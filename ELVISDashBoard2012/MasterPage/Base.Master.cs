﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.Data;
using Common.Function;
using BusinessLogic;


namespace ELVISDashBoard.MasterPage
{
    public partial class Base : System.Web.UI.MasterPage
    {

        protected UserData UserData
        {
            get { return Session["userData"] as UserData; }
        }

        protected string UserName
        {
            get
            {
                UserData u = UserData;
                if (u != null)
                    return u.USERNAME;
                else
                    return "";
            }
        }

        protected string FullName
        {
            get
            {
                UserData u = UserData;
                if (u != null)
                    return u.FIRST_NAME + " " + u.LAST_NAME;
                return "";
            }
        }


        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}