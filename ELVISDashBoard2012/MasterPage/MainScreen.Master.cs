﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using Common.Data;
using System.IO;
using System.Text;
using BusinessLogic;
using Common.Function;

namespace ELVISDashboard.MasterPage
{
    public partial class MainScreen : System.Web.UI.MasterPage
    {
        private UserData userData;
        //private string _ScreenID;
        private string _UserID;
        //private List<RoleAcessData> _RoleAcessList;
        GlobalResourceData _globalResxData = new GlobalResourceData();
        LoggingLogic _logging = new LoggingLogic();

        private UserData UserData
        {
            get
            {
                UserData u = Session["userData"] as UserData;
                return u ??
                       new UserData()
                       {
                           USERNAME = "",
                           DIV_CD = -1,
                           DIV_NAME = "",
                           TITLE = "",
                           REG_NO = 0,
                           FIRST_NAME = "",
                           LAST_NAME = "",
                           DEPARTMENT_ID = "",
                           DEPARTMENT_NAME = "",
                       };
            }
        }

        protected readonly String UMENU = "User.MenuList";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (UserData == null)
                {
                    Response.Redirect("~/Default.aspx");
                }
                else
                {
                    litUser.Text = UserData.FIRST_NAME + " " + UserData.LAST_NAME;
                }
                _UserID = UserData.USERNAME;

                userData = (UserData)Session["userData"];
                List<ScreenData> MenuList = null;
                //if (Session["MenuData"] == null)
                //{
                if (Session != null)
                {
                    MenuList = Session[UMENU] as List<ScreenData>;
                    if (MenuList == null)
                    {
                        MenuList = new BusinessLogic.MenuLogic().GetMenu(userData.USERNAME);

                        Session[UMENU] = MenuList;
                    }
                }
                else
                {
                    MenuList = new BusinessLogic.MenuLogic().GetMenu(userData.USERNAME);
                }
                if (MenuList != null)
                {
                    MenuIds = new List<string>();
                    Render(MenuList);
                }

                //}
                //else
                //{
                //    MenuList = (List<ScreenData>)Session["MenuData"];
                //}
                HiddenField hidPageTitle = ContentPengumuman.FindControl("hidPageTitle") as HiddenField;
                HiddenField hidScreenID = ContentPengumuman.FindControl("hidScreenID") as HiddenField;
                litPageTitle.Text = hidPageTitle.Value;              
            }
        }

        private List<string> MenuIds;
        private void Render(List<ScreenData> MenuList)
        {
            foreach (ScreenData _menuData in MenuList)
            {
                if (MenuIds.Contains(_menuData.MENU_ID))
                    continue;
                MenuIds.Add(_menuData.MENU_ID); 
                if (_menuData.PATH == "#")
                {
                    litMenu.Text += @"<li><span class=""folder"">" + _menuData.CAPTION + "</span>";
                }
                else
                {
                    litMenu.Text += "<li><span class=\"file\"><a href=\"javascript:openWin('" 
                        + ResolveClientUrl(_menuData.PATH) + "','" 
                        + _menuData.SCREEN_ID + "');\">" 
                        + _menuData.CAPTION + "</a></span>";

                }
                if (_menuData != null)
                {
                    if (_menuData.ChildList != null && _menuData.ChildList.Count > 0)
                    {
                        litMenu.Text += "<ul>";
                        Render(_menuData.ChildList);
                        litMenu.Text += "</ul>";
                    }
                    litMenu.Text += "</li>";
                }
            }
        }

        protected void Home_Click(object sender, ImageClickEventArgs e)
        {
            // Response.Redirect("Home.aspx");
        }

        protected void Logout_Click(object sender, ImageClickEventArgs e)
        {
            int cookieCount = Request.Cookies.Count;
            if (cookieCount > 0)
            {
                bool kode = false;
                for (int i = 0; i < cookieCount; i++)
                {
                    string keyName = Request.Cookies[i].Name;
                    string value = Request.Cookies[i].Value;
                    try
                    {
                        new Common.Function.GlobalResourceData().GetResxObject("ScreenID", keyName);
                        if (value == "1")
                        {
                            kode = true;
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                    }
                }
                if (kode == false)
                {
                    #region logging logout
                    try
                    {
                        int _processId = 0;
                        string _errMsg = string.Format(_globalResxData.GetResxObject("Message", "MSTD00005INF"), UserData.USERNAME);
                        _logging.Log("MSTD00005INF", _errMsg, "Logout Process", UserData.USERNAME, "", _processId);
                    }
                    catch
                    {
                    }
                    #endregion

                    Session.Clear();
                }
            }
            else
            {
                #region logging logout
                try
                {
                    int _processId = 0;
                    string _errMsg = string.Format(_globalResxData.GetResxObject("Message", "MSTD00005INF"), UserData.USERNAME);
                    _logging.Log("MSTD00005INF", _errMsg, "Logout Process", UserData.USERNAME, "", _processId);
                }
                catch
                {
                }
                #endregion

                Session.Clear();
            }

        }
    }
}
