﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELVISDashBoard
{
    public static class AppConfig
    {
        private static string version = Version(); 
        public static string VERSION
        {
            get { return version; }
        }

        public static string Version()
        {
            Version v = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            return v.Major.ToString() + "." + v.Minor.ToString().PadLeft(2, '0') + " " 
                + System.IO.File.GetLastWriteTime(System.Reflection.Assembly.GetExecutingAssembly().Location)
                .ToString("dd-MM-yyyy HH:mm");
        }
    }
}