﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="ELVISDashBoard.unit.Log" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../App_Themes/BMS_Theme/Script/jquery.min.js" type="text/javascript"></script>
    <link href="css/ui.jqgrid.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
    .entry
    {
        margin: 2px; 
        padding: 5px; 
        float: left;
        clear: none;
    }    
    </style>
    
    <script type="text/javascript" >
        function TryParseInt(str,defaultValue){
            var retValue = defaultValue;
            if(typeof str != 'undefined' && str!=null && str.length>0){
                if (!isNaN(str)){
                    retValue = parseInt(str);
                }
            }
            return retValue;
        }


        function GetLog(pid, start, limit, filter) {
            pid = TryParseInt(pid, -1);
            
            start = TryParseInt(start, -1);
            limit = TryParseInt(limit, -1);

            $("#oTab").jqGrid({
                url: '/ws.asmx/log',
                datatype: 'json',
                ajaxGridOptions: { contentType: 'application/json; charset=utf-8' },
                mtype: 'POST',
                jsonReader: {

                    total: function (obj) { return 1; },
                    page: function (obj) { return 0; },
                    records:
                        function (obj) {
                            return obj.d.length;
                        },

                    root:
                        function (obj) {
                            
                            
                            return obj.d;
                        },
                    rows:
                        function (obj) {
                            
                        
                            return obj.d;
                        },

                    id: 'ROWNUM'


                },
                gridview: true,
                postData: JSON.stringify({ 'id': pid, 'start': start, 'limit': limit, 'filter': filter}),
                colnames: ['Id', 'Seq', 'When', 'What', 'Where', 'Who', 'Remarks', 'MsgId', 'MsgType', 'Sts'],
                colModel: [
                    { name: 'Id', label: 'ID', index: 'Id', width: 50 },
                    { name: 'Seq', label: 'SEQ', index: 'Seq', width: 50 },
                    { name: 'When', label: 'When', index: 'When', width: 150 },
                    { name: 'What', label: 'What', index: 'What', width: 350 },
                    { name: 'Where', label: 'Where', index: 'Where', width: 200 },
                    { name: 'Who', label: 'Who', index: 'Who', width: 100 },
                    { name: 'Remarks', label: 'Remarks', index: 'Remarks', width: 100 },
                    { name: 'MsgId', label: 'msgId', index: 'MsgId', width: 80 },
                    { name: 'MsgType', label: 'Type', index: 'MsgType', width: 100 },
                    { name: 'Sts', label: 'Sts', index: 'Sts', width: 40 }
                ],
                rowNum: 10,
                rowList: [10],
                pager: '#oPage',
                sortname: 'RowNum',
                
                viewrecords: true

            });
            $("#oTab").jqGrid('navGrid', '#oPage', { edit: false, add: false, del: false });
      }

      function aGetLog(pid, start, limit, filter) {
          $.ajax({
              type: 'POST',
              url: '/ws.asmx/log',
              contentType: 'application/json; charset=utf-8',
              dataType: 'json',
              data: JSON.stringify({ 'pid': pid, 'start': start, 'limit': limit, 'filter': filter }),
              beforeSend: function (xhr) {
                  xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
              },
              success: function (dx) {
                  
                  var q = dx.d;

                  if (q != null) {

                      $('#oDiv').html('Ok');
                  } else {
                      $('#oDiv').html("NG");
                  }
              },

              error: function (r, er) {
                  $('#oDiv').html(r.responseText);
              }

          });
      }

        $(document).ready(function () {
            $('#GoButton').click(function (e) {
                e.preventDefault();

                GetLog($('#pidText').val(),
                    $('#startText').val(),
                    $('#limitText').val(),
                    $('#filterText').val());

                aGetLog($('#pidText').val(),
                    $('#startText').val(),
                    $('#limitText').val(),
                    $('#filterText').val());
            });
        });
    </script>
</head>
<body>
    <div id="oDiv">
    </div>

    <form id="LogForm" runat="server" clientidmode="Static">
         <div class="row">
            
            <span class="entry"> ID</span><span class="entry"> <input type="text" id="pidText"  size="10"/></span>
            <span class="entry"> Start </span> <span class="entry"><input type="text" id="startText"  size="5"/></span>
            <span class="entry"> Limit </span> <span class="entry"><input type="text" id="limitText" size="5" /></span> 
            <span class="entry"> Filter </span> <span class="entry"><input type="text" id="filterText" size="40" /></span>
            <span class="entry"><input type="button" id="GoButton" value="Go" size="10" /></span>
        </div>        
    </form>
    <table id="oTab" spacing="0" border="0" cellpadding="3" cellspacing="0">
    <thead>
        <tr>
            <td>Id</td>
            <td>Seq</td>
            <td>When</td>
            <td>What</td>
            <td>Location</td>
            <td>Who</td>
           
            <td>remarks</td>
            <td>msg_id</td>
            <td>msg_type</td>
            <td>process_sts</td>
        </tr>
    </thead>
    <tbody>
        <tr> 
            <td> </td>
            <td> </td>
        </tr>
        
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
    </table>
    <div id="oPage"></div>

</body>
</html>
