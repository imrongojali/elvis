﻿<%@ Page Title="Worklist Manager" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="WorklistMan.aspx.cs" Inherits="ELVISDashBoard.presentationHelper.WorklistMan" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        var gridAllSelected = false;
    </script>

<style type="text/css">
.box3
{
    display: block;
    clear: none;
    float: left;
    
}
.blank10
{
    display: block;
    clear: none; 
    float: left;
    width: 10px;
}    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:HiddenField runat="server" ID="hidPageTitle" Value="Worklist Manager" />
    <asp:Literal ID="litMessage" runat="server" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
<asp:UpdatePanel ID="mainPanel" runat="server">
    <Triggers>
    <asp:PostBackTrigger ControlID="DeleteButton" />
    <asp:PostBackTrigger ControlID="ApproveButton" />
    <asp:PostBackTrigger ControlID="RejectButton" />
    <asp:PostBackTrigger ControlID="RedirectButton" />
    </Triggers>
    <ContentTemplate>
    <asp:Literal ID="MessageLiteral" runat="server"></asp:Literal>
    
    <dx:ASPxGridView ID="g" runat="server" DataSourceID="dsWorklist" ClientInstanceName="g" ClientIDMode="Static" KeyFieldName="Folio" Width="995px" >
    <Settings VerticalScrollableHeight="600" VerticalScrollBarStyle="Standard" ShowVerticalScrollBar="true" />
    <SettingsPager RenderMode="Lightweight" PageSize="30" />
    <SettingsBehavior ColumnResizeMode="Control" />
    <Columns>
    <dx:GridViewCommandColumn ShowSelectCheckbox="true" Width="50px">
        <HeaderStyle HorizontalAlign="Center"/>
         <HeaderTemplate>
            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientInstanceName="SelectAllCheckBox"
                ClientSideEvents-CheckedChanged="function(s, e) { g.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }"
                CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
            </dx:ASPxCheckBox>
        </HeaderTemplate>
    </dx:GridViewCommandColumn>
    <dx:GridViewDataColumn Caption="UserID" FieldName="UserID" Width="120px"/>
    <dx:GridViewDataColumn Caption="Folio" FieldName="Folio" Width="120px"/>
    <dx:GridViewDataTextColumn Caption="SN" FieldName = "SN" Width="100px"/>
    <dx:GridViewDataDateColumn Caption="Date" FieldName = "StartDate" Width="125px">
        <PropertiesDateEdit DisplayFormatString="{0:yyyy.MM.dd HH:mm}">
        </PropertiesDateEdit>
    </dx:GridViewDataDateColumn>
    <dx:GridViewDataColumn Caption="FullName" FieldName="FullName" />
    </Columns>
    </dx:ASPxGridView>

    <div class="row" >
        <div class="rowButton">
            <div class="box3">
            Do: 
            <asp:DropDownList ID="ddMethod" runat="server" ClientIDMode="Static">
                <asp:ListItem Text="GetWorklistComplete" Value="0"/>
                <asp:ListItem Text="GetAllWorklistComplete"  Value="1"/>
                <asp:ListItem Text="GetWorklistCompleteByProcess" Value="3"/>
                <asp:ListItem Text="GetWorklistFiltered" Value="4" />
                <asp:ListItem Text="GetWorklistBy" Value="5" />
            </asp:DropDownList>
                <asp:Button ID="ImpersonateButton" runat="server" Text="Go" OnClick="ImpersonateButton_Click" />
            </div>
            <div class="blank10">&nbsp;</div>
            <div class="box3">
            <asp:Button ID="ApproveButton" runat="server" Text="Approve" OnClick="ApproveButton_Click" />
            <asp:Button ID="RejectButton" runat="server" Text="Reject" OnClick="RejectButton_Click" />
            </div>
            <div class="blank10">&nbsp;</div>
            <div class="box3">
            <asp:Button ID="RedirectButton" runat="server" Text="Redirect" OnClick="RedirectButton_Click" />
            <asp:Button ID="DeleteButton" runat="server" Text="Delete" OnClick="DeleteButton_Click" />
            </div>

        </div>
    </div>
    <div class="row">
        <div class="box3">
        As <asp:TextBox ID="UserToText" runat="server" />
        </div>
        <div class="blank10">&nbsp;</div>
        <div class="box3">
        Key <asp:TextBox ID="KeyText" runat="server" />
        </div>
        <div class="blank10">&nbsp;</div>
        <div class="box3">
        Value <asp:TextBox ID="ValueText" runat="server" />
        </div>
        
    </div>

    <asp:LinqDataSource ID="dsWorklist" runat="server" OnSelecting="dsWorklist_Selecting">   
    </asp:LinqDataSource>
    </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="content" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="taskList" runat="server">
</asp:Content>
