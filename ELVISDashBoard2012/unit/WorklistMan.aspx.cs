﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using K2.Helper;
using Common;
using Common.Data;
using DataLayer;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace ELVISDashBoard.presentationHelper
{
    public partial class WorklistMan : BaseCodeBehind
    {
        private string Persona
        {
            get
            {
                string p = UserName;
                if (Session["Persona"]!= null && !((Session["Persona"] as string).isEmpty())) 
                    p = Session["Persona"] as string;
                return p;
            }
            set
            {
                Session["Persona"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            lit = litMessage;
            g.Settings.VerticalScrollableHeight = 400;
            lit = MessageLiteral;
            if (!Page.IsCallback && !Page.IsPostBack) 
                g.DataBind();
        }


        protected List<string> GetSelectedRows()
        {
            List<string> r = new List<string>();
            for (int i = 0; i < g.VisibleRowCount; i++)
            {
                if (g.Selection.IsRowSelected(i))
                {
                    string a = g.GetRowValues(i, new string[] { "Folio" }).str();
                    if (a!=null && a.Length > 0)
                        r.Add(Convert.ToString(a));
                }
            }
            return r;
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            List<string> folios = GetSelectedRows();
            //K2Helper k = new K2Helper();
            try
            {
                logic.k2.Open();
                logic.k2.deleteprocess(folios);
            }
            catch (Exception ex)
            {
                LoggingLogic.say(UserName, "Worklist Delete: " + CommonFunction.CommaJoin(folios));
                LoggingLogic.say(UserName, LoggingLogic.Trace(ex));
            }
            finally
            {
               logic.k2.Close();
            }
            g.DataBind();
        }

        protected void K2Action(string act)
        {
            Nag("INF", act);
            List<string> folios = GetSelectedRows();

            try
            {
                logic.k2.Open();
                logic.k2.Impersonate(Persona);

                string _Command = resx.K2ProcID("Form_Registration_" + act);

                Dictionary<string, string> _DataField = new Dictionary<string, string>();
                for (int i = 0; i < folios.Count; i++)
                {
                    string _DocNo = folios[i];
                    _DataField.Add("REFF_NO", _DocNo);
                    _DataField.Add("CurrentPIC", Persona);
                    List<WorklistHelper> w = logic.k2.GetWorklistFiltered(Persona, "folio", _DocNo);
                    if (w != null && w.Count == 1)
                    {
                        int ProcID = logic.k2.GetProcID(w[0].SN);
                        logic.k2.Revert();
                        logic.k2.EditDatafield(_DataField, ProcID);
                        logic.k2.Impersonate(Persona);
                        logic.k2.Action(_Command, w[0].SN);
                    }
                }

                logic.k2.Revert();

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                Nag("MSTD00002ERR", ex.Message);
            }
        }

        protected void ApproveButton_Click(object sender, EventArgs e)
        {
            K2Action("Approval");
        }

        protected void RejectButton_Click(object sender, EventArgs e)
        {
            K2Action("Rejected");
        }

        protected void RedirectButton_Click(object sender, EventArgs e)
        {
            if (UserToText.Text.Length < 1) return;

            List<string> folios = GetSelectedRows();
            //K2Helper k = new K2Helper();
            try
            {
                logic.k2.Open();
                logic.k2.Redirect(folios, UserName, UserToText.Text);
            }
            catch (Exception ex)
            {
                LoggingLogic.say(UserName, "Worklist Redirect to : " + UserToText.Text + " " + CommonFunction.CommaJoin(folios));
                LoggingLogic.say(UserName, LoggingLogic.Trace(ex));
            }
            finally
            {
                logic.k2.Close();
            }
            g.DataBind();
        }

        protected void ImpersonateButton_Click(object sender, EventArgs e)
        {
            Persona = UserToText.Text;
            g.DataBind();
        }

        protected void dsWorklist_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string Op = "?";
            string k = KeyText.Text, v = ValueText.Text;
            if (Persona != null)
            {
                try
                {
                    logic.k2.Open();
                    switch (ddMethod.SelectedValue)
                    {
                        case "0":
                            Op = string.Format("GetWorklistComplete(\"{0}\")", Persona);
                            e.Result = logic.k2.GetWorklistComplete(Persona);
                            break;
                        case "1":
                            Op = string.Format("GetAllWorklistComplete(\"{0}\")", Persona);
                                
                            e.Result = logic.k2.GetAllWorklistComplete(Persona);
                            break;
                        case "3":
                            Op = string.Format("GetWorklistCompleteByProcess(\"{0}\", \"{1}\")", Persona, v);
                            e.Result = logic.k2.GetWorklistCompleteByProcess(Persona, v);
                            break;
                        case "4":
                            Op = string.Format("GetWorklistFiltered(\"{0}\", \"{1}\", \"{2}\")", Persona, k, v);
                            if (!k.isEmpty() && !v.isEmpty())
                                e.Result = logic.k2.GetWorklistFiltered(Persona, k, v);
                            else
                                throw new Exception("Key Value must be filled");
                            break;
                        case "5":
                            List<string> eCrit = new List<string>();
                            if (!Persona.isEmpty())
                                eCrit.Add("user=" + Persona);
                            
                            if (!k.isEmpty() && !v.isEmpty())
                            {
                                eCrit.Add(k + "=" + v);
                            }
                            string a = string.Join(";", eCrit);
                            e.Result = logic.k2.GetWorklistBy(a);
                            break;
                        default: break;

                    }
                    
                }
                catch (Exception ex)
                {
                    e.Result = new List<WorklistHelper>();
                    Nag("MSTD00002ERR", Op +  " error");
                    LoggingLogic.say(UserName, "Err :{0} {1}", Op, LoggingLogic.Trace(ex));
                }

                finally
                {
                    logic.k2.Close();
                }
                    
            }
            else
            {
                Nag("MSTD00002ERR", "Cannot retrieve worklist for " + Persona);
                e.Result = new List<WorklistHelper>();
            }

           
        }
    }
}