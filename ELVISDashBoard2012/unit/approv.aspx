﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="approv.aspx.cs" Inherits="ELVISDashBoard.unit.approv" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    
    <style type="text/css">
    .tab 
    {
        padding-left: 20px;
        display: block; 
    }
    td 
    {
        font-family: Consolas, Courier, Monospace;
        font-size: 12pt;        
    }
    td.val 
    {
        padding-left: 10px;
    }
    
    #SessionUserData 
    {
        width: 500px;
        display: block;
        clear: both;        
    }
    </style>	
    <link href="../approval.css" rel="stylesheet" type="text/css" />
    <script src="../App_Themes/BMS_Theme/Script/jquery.min.js" type="text/javascript"></script>    
    <script type="text/javascript">
        function Classy(stat) {
            switch (stat) {
                case 1:
                    return "Approved";
                case 2:
                    return "Rejected";
                default:
                    return "Waiting";
            }
        }

        function Distribusy(x) {
            switch (x) {
                case 1:
                    return "Skipped";
                case 2:
                    return "Concurred"
                default:
                    return "";
            }
        }
        function DateFmt() {
            this.dateMarkers = {
                d: ['getDate', function (v) { return ("0" + v).substr(-2, 2) } ],
                m: ['getMonth', function (v) { return ("0" + (v + 1)).substr(-2, 2) } ],
                n: ['getMonth', function (v) {
                    var mthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    return mthNames[v];
                } ],
                w: ['getDay', function (v) {
                    var dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
                    return dayNames[v];
                } ],
                y: ['getFullYear'],
                H: ['getHours', function (v) { return ("0" + v).substr(-2, 2) } ],
                M: ['getMinutes', function (v) { return ("0" + v).substr(-2, 2) } ],
                S: ['getSeconds', function (v) { return ("0" + v).substr(-2, 2) } ],
                i: ['toISOString', null]
            };

            this.format = function (date, fmt) {
                var dateMarkers = this.dateMarkers
                var dateTxt = fmt.replace(/%(.)/g, function (m, p) {
                    var rv = date[(dateMarkers[p])[0]]()

                    if (dateMarkers[p][1] != null) rv = dateMarkers[p][1](rv)

                    return rv
                });

                return dateTxt
            }
        }

        fmt = new DateFmt()
        v = fmt.format(new Date(), "%w %d:%n:%y - %H:%M:%S %i")

        function Decodate(x) {
            if (x != null) {
                var fmt = new DateFmt();

                var d = getDateFromAspString(x);
                var df = fmt.format(d, "%d %n %y %H:%M");
                return df;
            } else {
                return "";
            }
        }

        function GetApprov(docNo, docYear, level, userName, statusCd, gridName) {
            
            $.ajax({
                type: 'POST',
                url: '/ws.asmx/approvalOverview',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({ 'docNo': docNo, 'docYear': docYear, 'level': level, 'userName': userName, 'statusCode': statusCd }),
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8");
                },
                success: function (dx) {
                    
                    var q = dx.d;
                    if (q != null) {
                        // var d = new Date(parseInt(q.RegDate.substr(6)));
                        // var fmt = d.getDate() + '-' + d.getMonth() + '-' + d.getFullYear();
                        $("#" + gridName + " > tbody").html("");
                        for (var i = 0; i < q.length; i++) {
                            var qx = q[i];
                            $("#" + gridName + "> tbody:last").append('<tr><td>' + qx.CODE + '</td><td>' +
                                qx.FULL_NAME + '</td><td class="' + Classy(qx.STATUS) + '">' + qx.STATUS + '</td>' +
                                '<td class="' + Distribusy(qx.SKIP_FLAG) + '">' + Decodate(qx.ACTUAL_DT) + '</td></tr>');
                        }
                    } else {
                        $('#OverviewMsg').html("empty");
                    }
                },

                error: function (r, er) {
                    $('#OverviewMsg').html(r.responseText);
                
                }
            });

        }

        function getDateFromAspString(aspString) {
            var epochMilliseconds = aspString.replace(/^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/, '$1');
            if (epochMilliseconds != aspString) {
                return new Date(parseInt(epochMilliseconds));
            }
        }

        $(document).ready(function () {

            $("#showApprovalOverview").click(function (event) {
                event.preventDefault();

                var docNo = $('#docNo').val();
                var docYear = $('#docYear').val();
                var userName = $('#userName').val();
                var statusCd = $('#statusCd').val();

                GetApprov(docNo, docYear, 1, userName, statusCd, "gAppUser");
                GetApprov(docNo, docYear, 2, userName, statusCd, "gAppFinance");
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="Overview">
    <div id="OverviewMsg"></div>
        <div id="inputLine" style="display: block; width= 400px; clear: both; float:none;">
            
             <h3>Approval Overview</h3>
             
             <table id = "ApprovalOverviewInput">
             <thead>
             <tr>             
             <td>Doc No</td>
             <td>Doc Year</td>
             <td>userName</td>
             <td>statusCd</td> 
             <td>&nbsp;</td>
             </tr>
             </thead>
             <tbody>
             <tr>
             <td>
            <input type="text" id="docNo" maxlength="9" width="90" style="width:100px"/>
            </td>
            <td>
            <input type="text" id="docYear" maxlength="4" width="50" style="width:80px"/>
            </td>
            <td>
            <input type="text" id="userName" maxlength="20" style="width:100px"/>
            </td><td>
            <input type="text" id="statusCd" maxlength="2" width="50" style="width:20px" />
            </td>
            <td><input type="button" id="showApprovalOverview" value="Show" ></td>
            </tr>
            </tbody>
            </table>
        </div>
        <div id="lefty" style="display: inline-block; float: left; width: 450px">
            <table id="gAppUser">
            <thead>
            <tr>
                <td>
                    Code
                </td>
                <td>
                    Name
                </td>
                <td>
                    Status
                </td>
                <td>
                    Date
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            </tbody>
            </table>
        </div>
        <div id="righty" style="display:inline-block; float: left; width:450px;">
            <table id="gAppFinance">
            <thead>
            <tr>
                <td>
                    Position
                </td>
                <td>
                    Name
                </td>
                <td>
                    Status
                </td>
                <td>
                    Time
                </td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
            </tbody>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
