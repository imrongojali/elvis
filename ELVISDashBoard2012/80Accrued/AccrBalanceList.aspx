﻿<%@ Page Title="Accrued Balance Retrieval Screen" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="AccrBalanceList.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrBalanceList" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function hidePop() {
            var pop = $find("ConfirmationDeletePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }
    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />

              <div class="contentsection" style="width:987px !important;">
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                <tr>
                    <td valign="baseline" style="width: 220px">
                        Issuing Division
                    </td>
                    <td valign="top" colspan="3">
                        <dx:ASPxDropDownEdit ID="IssuingDivisionDropDown" ClientInstanceName="IssuingDivisionDropDown"
                            runat="server" Width="210px" AutoPostBack="True">
                            <DropDownWindowStyle BackColor="#EDEDED" />
                            <DropDownWindowTemplate>
                                <dx:ASPxListBox runat="server" ID="IssuingDivisionListBox" ClientInstanceName="IssuingDivisionListBox"
                                    SelectionMode="CheckColumn" Width="300px" Height="300px" TextField="Description" AutoPostBack="True"                                        
                                    ValueField="Code" DataSourceID="dsDivision">
                                    <Border BorderStyle="None" />
                                    <BorderBottom BorderStyle="Solid" BorderWidth="1px" BorderColor="#DCDCDC" />
                                    <ClientSideEvents  SelectedIndexChanged="function(s,e) {
                                            var selectedItems = s.GetSelectedItems();
                                            var selectedText = GetSelectedListItemsText(selectedItems);
                                            IssuingDivisionDropDown.SetText(selectedText);
                                    }" />
                                </dx:ASPxListBox>
                                <table style="width: 100%" cellspacing="0" cellpadding="4">
                                    <tr>
                                        <td align="right">
                                            <dx:ASPxButton ID="btSelectAllDiv" AutoPostBack="False" runat="server" Text="Select All"
                                                CssClass="display-inline-table" OnClick="btSelectAllDiv_OnClick">
                                                <%--<ClientSideEvents Click="function(s, e) { IssuingDivisionListBox.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }" />--%>
                                            </dx:ASPxButton>
                                            <%--<asp:Button ID="btSelectAllDiv" runat="server" Text="Search" OnClick="btSelectAllDiv_OnClick" OnClientClick="loading();" />--%>
                                            <dx:ASPxButton ID="btClearDivisionList" AutoPostBack="False" runat="server" Text="Clear"
                                                CssClass="display-inline-table" OnClick="btClearDivisionList_OnClick">
                                                <ClientSideEvents Click="function(s, e){ IssuingDivisionListBox.UnselectAll(); IssuingDivisionDropDown.SetText(''); }" />
                                            </dx:ASPxButton>
                                            <dx:ASPxButton ID="btCloseDivisionList" AutoPostBack="False" runat="server" Text="Close"
                                                CssClass="display-inline-table">
                                                <ClientSideEvents Click="function(s, e){ IssuingDivisionDropDown.HideDropDown(); }" />
                                            </dx:ASPxButton>
                                        </td>
                                    </tr>
                                </table>
                            </DropDownWindowTemplate>
                        </dx:ASPxDropDownEdit>


                        <asp:DropDownList ID="DropDownListIssuingDivision" runat="server" Width="240px" CssClass="hidden"/>
                    </td>
                    <td valign="baseline" style="width: 80px">
                        Open Status
                    </td>
                    <td valign="top" style="width: 120px">
                        <asp:DropDownList ID="ddlOpenStatus" runat="server" Width="80px" AutoPostBack="false" />
                    </td>
                    <td valign="baseline" style="width: 300px" >
                        WBS No. (Old)
                    </td>
                    <td valign="top" style="width: 720px">
                        <dx:ASPxGridLookup runat="server" ID="ddlWBSNoOld" ClientInstanceName="ddlWBSNoOld" 
                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="WbsNumber" TextFormatString="{0}"
                            AutoPostBack="False" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains" OnLoad="LookupWbsNoOld_Load">       
                            <GridViewProperties EnableCallBacks="false">
                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                <SettingsPager PageSize="7" />
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                <dx:GridViewDataTextColumn Caption="WBS No. (Old)" FieldName="WbsNumber" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridLookup>
                        <asp:LinqDataSource ID="LinqDataSource2" runat="server" OnSelecting="LinqDataSource2_Selecting">
                        </asp:LinqDataSource>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Booking No
                    </td>
                    <td valign="top" colspan="3">
                        <asp:TextBox ID="txtBookingNo" runat="server" Width="160px" MaxLength="20" onkeydown = "OnKeyDown"></asp:TextBox>
                    </td>

                    <td valign="baseline">
                        Extended
                    </td>
                    <td valign="top">
                        <asp:DropDownList runat="server" Width="80px" ID="ddlExtend" AutoPostBack="false"/>
                    </td>
                    <td valign="baseline" >
                        WBS No. (New)
                    </td>
                    <td valign="top">
                        <dx:ASPxGridLookup runat="server" ID="ddlWBSNoNew" ClientInstanceName="ddlWBSNoNew"
                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="WbsNumber" TextFormatString="{0}"
                            AutoPostBack="False" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains" OnLoad="LookupWbsNoNew_Load">       
                            <GridViewProperties EnableCallBacks="false">
                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                <SettingsPager PageSize="7" />
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                <dx:GridViewDataTextColumn Caption="WBS No. (New)" FieldName="WbsNumber" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridLookup>
                        <asp:LinqDataSource ID="LinqDataSource3" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            OnSelecting="LinqDataSource3_Selecting" TableName="vw_WBS">
                        </asp:LinqDataSource>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Doc. Type
                    </td>
                    <td valign="top" colspan="3">
                        <asp:DropDownList runat="server"  ID="ddlPvType" AutoPostBack="false"/>
                    </td>

                    <td valign="baseline">
                        Shifted
                    </td>
                    <td valign="top">
                        <asp:DropDownList runat="server" Width="80px" ID="ddlShifted" AutoPostBack="false"/>
                    </td>
                    <td valign="baseline" >
                        Suspense No. (Old)
                    </td>
                    <%--<td valign="top">
                        <asp:DropDownList runat="server" Width="90%"  ID="ddlSusNoOld" AutoPostBack="false"/>
                    </td>--%>
                     <td valign="top">
                        <dx:ASPxGridLookup runat="server" ID="ddlSusNoOld" ClientInstanceName="ddlSusNoOld"
                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="SuspenseNo" TextFormatString="{0}"
                            AutoPostBack="False" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains" OnLoad="LookupSusNoOld_Load">       
                            <GridViewProperties EnableCallBacks="false">
                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                <SettingsPager PageSize="7" />
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                <dx:GridViewDataTextColumn Caption="Suspense No. (Old)" FieldName="SuspenseNo" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="WBS No. (Old)" FieldName="WbsNumber" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridLookup>
                         <asp:LinqDataSource ID="LinqDataSource4" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            OnSelecting="LinqDataSource4_Selecting" TableName="vw_WBS">
                        </asp:LinqDataSource>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        Expiry Date
                    </td>
                    <td valign="top" style="width:75px">
                        <dx:ASPxDateEdit ID="dtExpiryDateFrom" runat="server" Width="100px" DisplayFormatString="dd.MM.yyyy" 
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                            AutoPostBack="false" ClientIDMode="Static" >
                            <ClientSideEvents DateChanged="OnDateChanged" />
                        </dx:ASPxDateEdit>
                    </td>
                    <td valign="top" style="width:24px">
                        To
                    </td>
                    <td valign="top" style="width:150px">
                        <dx:ASPxDateEdit ID="dtExpiryDateTo" runat="server" DisplayFormatString="dd.MM.yyyy"
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" Width="100px" 
                            AutoPostBack="false" ClientIDMode="Static">
                            <ClientSideEvents DateChanged="OnDateChanged" />
                        </dx:ASPxDateEdit>
                    </td>

                    <td valign="baseline" style="width: 175px">
                        Budget Year
                    </td>
                    <td valign="top" style="width: 380px">
                        <asp:TextBox ID="txtBDYear" runat="server" Width="50px" MaxLength="4" TabIndex="2"                            
                            ClientIDMode="Static" 
                            onchange="extractNumber (this, 0, false);"
							onblur = "extractNumber (this, 0, false);"
							onkeyup = "extractNumber (this, 0, false);"
							onkeypress = "return blockNonNumbers (this, event, false, false);"                            
                            onkeydown = "OnKeyDown"
                            >
                            
                            </asp:TextBox>
                    </td>
                    <td valign="baseline" >
                        Suspense No. (New)
                    </td>
                    <%--<td valign="top">
                        <asp:DropDownList runat="server" Width="90%"  ID="ddlSusNoNew" AutoPostBack="false"/>
                    </td>--%>
                    <td valign="top">
                        <dx:ASPxGridLookup runat="server" ID="ddlSusNoNew" ClientInstanceName="ddlSusNoNew"
                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="SuspenseNo" TextFormatString="{0}"
                            AutoPostBack="False" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains" OnLoad="LookupSusNoNew_Load">       
                            <GridViewProperties EnableCallBacks="false">
                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                <SettingsPager PageSize="7" />
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                <dx:GridViewDataTextColumn Caption="Suspense No. (New)" FieldName="SuspenseNo" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="WBS No. (Old)" FieldName="WbsNumber" Width="200px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                        </dx:ASPxGridLookup>
                        <asp:LinqDataSource ID="LinqDataSource5" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            OnSelecting="LinqDataSource5_Selecting" TableName="vw_WBS">
                        </asp:LinqDataSource>
                    </td>
                </tr>
                
                
             </table>
             <table width="100%">
                 <tr>
                    
                    <td align="right">
                        <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="loading();" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click"  OnClientClick="loading();" />
                    </td>  
                </tr>
             </table>
            </div>
            <div class="rowbtn" style="text-align: right;">
                <table width="100%">
                    <tr>
                        
                        
                    </tr>
                </table>
                
            </div>


        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued Balance Retrieval Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />



    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnDownload" />
        </Triggers>
        <ContentTemplate>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>                       
                    <td>
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" 
                            AutoGenerateColumns="False"
                            ClientInstanceName="gridGeneralInfo" 
                            KeyFieldName="BOOKING_NO" 
                            OnCustomCallback="grid_CustomCallback" 
                            OnCustomButtonCallback="grid_CustomButtonCallback"
                            EnableCallBacks="false"
                            Styles-AlternatingRow-CssClass="even">
                            <Columns>
                                <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px"
                                    CellStyle-VerticalAlign="Middle" FixedStyle="Left" >
                                    <EditButton Visible="false" Text=" ">
                                    </EditButton>
                                    <UpdateButton Visible="false" Text=" ">
                                    </UpdateButton>
                                    <NewButton Visible="false" Text=" ">
                                    </NewButton>
                                    <CancelButton Visible="false" Text=" ">
                                    </CancelButton>
                                    <CellStyle VerticalAlign="Middle">
                                    </CellStyle>
                                    <HeaderTemplate>
                                        <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientInstanceName="SelectAllCheckBox"
                                            ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); gridAllSelected = s.GetChecked(); }"
                                            CheckState="Unchecked" ClientIDMode="Static" Style="text-align: center">
                                        </dx:ASPxCheckBox>
                                    </HeaderTemplate>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewDataTextColumn Caption="Booking No." FieldName="BOOKING_NO" Width="135px"
                                    VisibleIndex="1" FixedStyle="Left">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Budget Year" FieldName="BUDGET_YEAR" Width="120px"
                                    VisibleIndex="1">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn Caption="Expiry Date" FieldName="EXPIRE_DT"
                                    VisibleIndex="2">
                                    <PropertiesDateEdit DisplayFormatString="{0:dd MMM yyyy}">
                                    </PropertiesDateEdit>
                                </dx:GridViewDataDateColumn>
                                <dx:GridViewDataTextColumn Caption="Open Status" FieldName="OPEN_STATUS" VisibleIndex="3"
                                    Width="80px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Doc. Type" FieldName="PV_TYPE_NAME" VisibleIndex="4"
                                    Width="100px">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewCommandColumn Caption="Activity" VisibleIndex="3" Width="85px" ButtonType="Button">
                                    <CellStyle VerticalAlign="Middle" />
                                    <CustomButtons>
                                        <dx:GridViewCommandColumnCustomButton ID="btnActivity" Text="View">
                                            <%--<Image ToolTip="View Activity" Url="~/App_Themes/BMS_Theme/Images/redbox.png" ></Image>--%>
                                        </dx:GridViewCommandColumnCustomButton>
                                    </CustomButtons>
                                </dx:GridViewCommandColumn>
                                <dx:GridViewBandColumn Caption="WBS No." VisibleIndex="5">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Old" FieldName="WBS_NO_OLD" VisibleIndex="0"
                                            Width="200px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="New" FieldName="WBS_NO_PR" VisibleIndex="1"
                                            Width="200px">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Suspense No." VisibleIndex="6">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Old" FieldName="SUSPENSE_NO_OLD" VisibleIndex="0"
                                            Width="100px">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="New" FieldName="SUSPENSE_NO_PR" VisibleIndex="1"
                                            Width="100px">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Balance" VisibleIndex="7" HeaderStyle-BackColor="LightYellow" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Initial" FieldName="INIT_AMT" VisibleIndex="0"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridInitialAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("INIT_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Shifted" FieldName="SHIFTED_AMT" VisibleIndex="1"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridShiftedAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SHIFTED_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Spent" FieldName="SPENT_AMT" VisibleIndex="2"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridSpentAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SPENT_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Available" FieldName="AVAILABLE_AMT" VisibleIndex="3"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridAvaiableAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("AVAILABLE_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Outstanding" FieldName="OUTSTANDING_AMT" VisibleIndex="4"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridOutstandingAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("OUTSTANDING_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Blocked" FieldName="BLOCKED_AMT" VisibleIndex="5"
                                            Width="120px" HeaderStyle-BackColor="LightYellow" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />                                             
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridBlockedAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("BLOCKED_AMT")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Remaining for" VisibleIndex="9" HeaderStyle-BackColor="LightGreen" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="PV & Shifting" FieldName="REMAIN_SHIFTING" VisibleIndex="0"
                                            Width="120px" HeaderStyle-BackColor="LightGreen" >
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridRemainShiftAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("REMAIN_SHIFTING")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Extend & Closing" FieldName="REMAIN_EXTEND" VisibleIndex="1"
                                            Width="120px" HeaderStyle-BackColor="LightGreen">
                                            <CellStyle HorizontalAlign="Right" Paddings-PaddingRight="10px" />
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" 
                                                    ID="litGridRemainExtAmountIDR" 
                                                    Text='<%# CommonFunction.Eval_Curr("IDR", Eval("REMAIN_EXTEND")) %>' />
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewBandColumn Caption="Closing" VisibleIndex="9" HeaderStyle-BackColor="LightBlue" >
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="SAP Doc. No." FieldName="SAP_DOC_NO_CLOSE" VisibleIndex="0"
                                            Width="120px" HeaderStyle-BackColor="LightBlue" >
                                            <CellStyle HorizontalAlign="Left" Paddings-PaddingRight="10px" />
                                            <DataItemTemplate>
                                                <asp:LinkButton runat="server" ID="linkSapDocNo" 
                                                    OnClick="LoadGridPopUpSapDocNo" 
                                                    Text='<%# Eval("SAP_DOC_NO_CLOSE") %>' 
                                                    CssClass="smallFont">
                                                </asp:LinkButton>
                                                <!-- <asp:Literal runat="server"  ID="litGridSapDocNo" Text='<%# Eval("SAP_DOC_NO_CLOSE")%>' /> -->
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                            <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <AlternatingRow CssClass="even">
                                </AlternatingRow>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle" CssClass="smallFont">
                                    <Paddings PaddingTop="1px" PaddingBottom="1px" />
                                </Cell>
                                <Row CssClass="doubleRow" />
                            </Styles>
                            <Settings ShowStatusBar="Visible" />
                            <SettingsPager Visible="false" />
                            <Templates>
                                <StatusBar>
                                    <div style="text-align: left;">
                                        Records per page: <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                            <option value="5" <%# WriteSelectedIndexInfo(5) %>>5</option>
                                            <option value="10" <%# WriteSelectedIndexInfo(10) %>>10</option>
                                            <option value="15" <%# WriteSelectedIndexInfo(15) %>>15</option>
                                            <option value="20" <%# WriteSelectedIndexInfo(20) %>>20</option>
                                            <option value="25" <%# WriteSelectedIndexInfo(25) %>>25</option>
                                            <option value="50" <%# WriteSelectedIndexInfo(50) %>>50</option>
                                            <option value="100" <%# WriteSelectedIndexInfo(100) %>>100</option>
                                            <option value="150" <%# WriteSelectedIndexInfo(150) %>>150</option>
                                            <option value="200" <%# WriteSelectedIndexInfo(200) %>>200</option>
                                            <option value="250" <%# WriteSelectedIndexInfo(250) %>>250</option>
                                            <option value="500" <%# WriteSelectedIndexInfo(500) %>>500</option>
                                      </select>&nbsp; <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">&lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a> &nbsp;
                                        Page <input type="text" onchange="if(!gridGeneralInfo.InCallback()) gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                            onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                            value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                            style="width: 20px" />of <%# gridGeneralInfo.PageCount%>&nbsp; <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%><a
                                            title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp; <a title="Last"
                                                href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
                                    </div>
                                </StatusBar>
                            </Templates>
                        </dx:ASPxGridView>
                        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            OnSelecting="LinqDataSource1_Selecting" TableName="vw_AccrBalance">
                        </asp:LinqDataSource>
                        <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting"/>

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td width="80px">
                        <asp:Button ID="btnDownload" runat="server" Text="Report" OnClick="btnDownload_Click" OnClientClick="loading();" />
                    </td>
                    <td valign="baseline" width="80px" >
                            <asp:Label ID="lbPostingDt" runat="server" Text="Posting Date"></asp:Label>
                        </td>
                        <td width="100px">
                            <dx:ASPxDateEdit ID="dtPostingDt" runat="server" Width="100px" DisplayFormatString="dd.MM.yyyy" 
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" 
                            AutoPostBack="false" ClientIDMode="Static" >
                            <ClientSideEvents DateChanged="OnDateChanged" />
                        </dx:ASPxDateEdit>
                        </td>
                        <td width="80px">
                            <asp:Button ID="btnCloseAccrued" OnClick="btnCloseAccrued_Click" runat="server" Text="Closing" width="80px" enabledefaultappearance="false" enabletheming="false" native="true" font-bold="true" backcolor="Blue" forecolor="WhiteSmoke" hoverstyle-backcolor="#000033"  OnClientClick="loading();" />
                        </td>
                        

                    <td>
                        <asp:Button ID="btnSimulate" runat="server" Text="Simulate" OnClick="btnSimulate_Click" OnClientClick="loading();" />
                    </td>
                    <td colspan="3" valign="top" style="width: 270px" align="right">
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="rowbtn">
        <asp:UpdatePanel runat="server" ID="updPnlLegend">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlLegendInfo" GroupingText="Legend" Width="983px">
                    <div style="float: left; width: 50%">
                        &nbsp;&nbsp;&nbsp;Work Flow Status : 
                        <table>
                            <tr>
                                <td> &nbsp; </td>
                                <td style="background-color: #ff0000; width: 20px"> &nbsp; </td>
                                <td style="width: 50px"> Delay </td>
                                <td> &nbsp; </td>
                                <td style="background-color: #00ff00; width: 20px"> &nbsp; </td>
                                <td style="width: 75px"> On time </td>
                                <td> &nbsp; </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>

        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete" OkControlID="BtnOkConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <div class="row">&nbsp;</div>
                        <div class="rowbtn buttonRight">
                            <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" 
                                OnClientClick="loading();HiddenDeleteOkButton.click();"/>
                                &nbsp; 
                            <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" 
                                OnClick="BtnCancelConfirmationDelete_Click" />
                        </div>
                            <asp:Button ID="HiddenDeleteOkButton" ClientIDMode="Static" Text=" " runat="server" Width="0px" Height="0px" 
                                OnClick="btnOkConfirmationDelete_Click" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Pop up SAP Doc No -->
    <asp:Button ID="ButtonHidSapDocNo" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="ModalPopupSapDocNo" runat="server" TargetControlID="ButtonHidSapDocNo"
        PopupControlID="panelSapDocNo" CancelControlID="ButtonCloseSapDocNo" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelSapDocNo" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="UpdatePanelSapDocNo">
                <ContentTemplate>
                    <asp:Literal runat="server" ID="LitMsgSapDocNo"></asp:Literal><asp:Literal runat="server"
                        ID="LitModalSapDocNo"></asp:Literal><table cellpadding="2px" cellspacing="0" border="0"
                            style="text-align: left" width="600px">
                            <tr>
                                <td colspan="3">
                                    <br />
                                    <dx:ASPxGridView ID="gridSapDocNo" runat="server" Width="800px" Visible="true" KeyFieldName="CURRENCY_CD"
                                        Styles-AlternatingRow-CssClass="even" AutoGenerateColumns="false" OnHtmlRowCreated="gridSapDocNo_HtmlRowCreated">
                                        <SettingsPager NumericButtonCount="10" PageSize="10" AlwaysShowPager="True">
                                        </SettingsPager>
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="NO" VisibleIndex="0" Width="50px" Name="NO">
                                                <DataItemTemplate>
                                                    <asp:Literal ID="litGridNoSapDocNo" runat="server"></asp:Literal></DataItemTemplate><HeaderStyle HorizontalAlign="Center" />
                                                <CellStyle HorizontalAlign="Center">
                                                </CellStyle>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Invoice No" VisibleIndex="0" FieldName="INVOICE_NO" Width="150px"/>
                                            <dx:GridViewDataTextColumn Caption="SAP Doc No" VisibleIndex="1" FieldName="SAP_DOC_NO" Width="150px" />                                            
                                            <dx:GridViewDataTextColumn Caption="Sap Clearing Doc No" VisibleIndex="2" FieldName="SAP_CLEARING_DOC_NO" Width="150px"/>                                                                           <dx:GridViewDataTextColumn Caption="Pay Prop Doc No" VisibleIndex="3" FieldName="PAYPROP_DOC_NO" Width="140px"/>
                                            <dx:GridViewDataTextColumn Caption="Pay Prop ID" VisibleIndex="4" FieldName="PAYPROP_ID" Width="100px"/>
                                        </Columns>
                                        
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False"  ColumnResizeMode="Control"/>
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="false"
                                            VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                            <Cell HorizontalAlign="Center">
                                            </Cell>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right" colspan="3">
                                    <br />
                                    <asp:Button runat="server" ID="ButtonCloseSapDocNo" ClientIDMode="Static" Text="Close" OnClick="ButtonClosePopUpSapDocNo_Click" />
                                </td>
                            </tr>
                        </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <asp:Button ID="hidBtnPopup" runat="server" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="popupActivity" runat="server" TargetControlID="hidBtnPopup"
        PopupControlID="panelActivity" CancelControlID="btnCloseActivity" BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" ID="panelActivity" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="upnModal">
                <ContentTemplate>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="400px">
                        <tr>
                            <td valign="middle" style="width:98px" class="td-layout-item">
                                Booking No. <span class="right-bold">:</span>
                            </td>
                            <td valign="middle" class="td-layout-item">
                                <dx:ASPxLabel runat="server" ID="lblBookingNo" Width="100px" CssClass="display-inline-table" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <dx:ASPxGridView ID="gridActivity" runat="server" Width="100%" ClientInstanceName="gridActivity"
                                    KeyFieldName="BOOKING_NO" AutoGenerateColumns="False">
                                    <Columns>
                                        <dx:GridViewDataColumn Caption="Activity Description" FieldName="ACTIVITY_DES" VisibleIndex="0">
                                        </dx:GridViewDataColumn>
                                    </Columns>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right; float:right">
                                <br />
                                <asp:Button runat="server" ID="btnCloseActivity" Text="Close" OnClick="btnCloseActivity_Click" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>
 
</asp:Content>
