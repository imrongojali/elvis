﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.AccruedForm;
using Common.Data;
using Common.Data._80Accrued;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Rendering;
using ELVISDashBoard.presentationHelper;
using System.Text;
using BusinessLogic.CommonLogic;
using System.IO;
using Common;
using BusinessLogic.VoucherForm;
using System.Web;
using BusinessLogic._80Accrued;
using AjaxControlToolkit;
using Common.Control;
using DevExpress.Web.Data;

namespace ELVISDashBoard._80Accrued
{
    public partial class AccrExtendForm : BaseAccrFormBehind
    {
        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_8012");
        private string _extendNo = null;

        private AccruedExtendData AccruedData
        {
            get
            {
                var _accruedData = (AccruedExtendData)ViewState["_AccruedData_ELVIS_Screen_8012"];
                string extNo = GetSessKey();
                if (_accruedData == null && !string.IsNullOrEmpty(extNo))
                {
                    _accruedData = logic
                                    .AccrExtend
                                    .Search(new AccrExtendSearchCriteria(extNo))
                                    .FirstOrDefault();

                    ViewState["_AccruedData_ELVIS_Screen_8012"] = _accruedData;
                }

                return _accruedData;
            }
            set
            {
                ViewState["_AccruedData_ELVIS_Screen_8012"] = value;
            }
        }
        //private List<>

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        public readonly string PAGE_SIZE = "accr_pagesize";
        public int PageSize
        {
            get
            {
                if (Session[PAGE_SIZE] != null)
                    return Convert.ToInt32(Session[PAGE_SIZE]);
                else
                    return 0;
            }

            set
            {
                Session[PAGE_SIZE] = value;
            }
        }

        public int EditIndex
        {
            get
            {
                if (Session["editIndex"] != null)
                    return Convert.ToInt32(Session["editIndex"]);
                else
                    return 0;
            }

            set
            {
                Session["editIndex"] = value;
            }
        }

        public bool IsEditActivity
        {
            get
            {
                if (Session["IseditAct"] != null)
                    return Convert.ToBoolean(Session["IseditAct"]);
                else
                    return false;
            }

            set
            {
                Session["IseditAct"] = value;
            }
        }

        public bool IsNewAccrued
        {
            get
            {
                if (Session["IsNewAccr"] != null)
                    return Convert.ToBoolean(Session["IsNewAccr"]);
                else
                    return false;
            }

            set
            {
                Session["IsNewAccr"] = value;
            }
        }

        public string ActivityBookingNo
        {
            get
            {
                if (Session["activityBookNo"] != null)
                    return Convert.ToString(Session["activityBookNo"]);
                else
                    return "";
            }

            set
            {
                Session["activityBookNo"] = value;
            }
        }
        public string IssuingDiv
        {
            get
            {
                if (Session["issueDiv"] != null)
                    return Convert.ToString(Session["issueDiv"]);
                else
                    return "";
            }

            set
            {
                Session["issueDiv"] = value;
            }
        }
        #endregion
       

        #region Getter Setter for Data List
        private List<AccruedExtendData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<AccruedExtendData>();
                }
                else
                {
                    return (List<AccruedExtendData>)ViewState["_listInquiry"];
                }
            }
        }
        private bool IsEditing
        {
            get
            {
                if (Session["isEditing"] == null)
                    return false;
                else
                    return Convert.ToBoolean(Session["isEditing"]);
            }
            set
            {
                Session["isEditing"] = value;
            }
        }

        protected int[] CanApproveReject = new int[] { 501 };

        Dictionary<string, List<string>> listActivity = new Dictionary<string, List<string>>();

        public static readonly string[] enabledFields = new string[]
        {
            "BOOKING_NO","EXTEND_AMT"
        };

        #endregion
        #region Getter Setter for Attachment-Note List

        public AttachmentTable AttachmentTable
        {
            get
            {
                string attSessKey = "_attachment_ELVIS_Screen_8012";

                if (Session[attSessKey] == null)
                {
                    Session[attSessKey] = new AttachmentTable();
                }

                return (AttachmentTable)Session[attSessKey];
            }
        }

        string attSessKey = "_note_ELVIS_Screen_8012";

        protected void ResetFooter()
        {
            Session[attSessKey] = null;
        }

        public List<FormNote> Notes
        {

            get
            {
                if (Session[attSessKey] == null)
                {
                    Session[attSessKey] = new List<FormNote>();
                }

                return (List<FormNote>)Session[attSessKey];
            }
            set
            {
                Session[attSessKey] = value;
            }
        }

        private Dictionary<string, List<string>> ListActivity 
        {
            set
            {
                Session["_listActivity"] = value;
            }
            get
            {
                if (Session["_listActivity"] == null)
                {
                    return new Dictionary<string, List<string>>();
                }
                else
                {
                    return (Dictionary<string, List<string>>)Session["_listActivity"];
                }
            }
        }
        private List<AccruedListDetail> PopUpActivity
        {
            set
            {
                Session["_popUpActivity"] = value;
            }
            get
            {
                if (Session["_popUpActivity"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)Session["_popUpActivity"];
                }
            }
        }
        private List<AccruedListDetail> TempPopUpActivity
        {
            set
            {
                Session["_tempPopUpActivity"] = value;
            }
            get
            {
                if (Session["_tempPopUpActivity"] == null)
                {
                    return new List<AccruedListDetail>();
                }
                else
                {
                    return (List<AccruedListDetail>)Session["_tempPopUpActivity"];
                }
            }
        }
        #endregion
        #endregion

        #region Event
        protected void Page_Init(object sender, EventArgs e)
        {
            base.Page_Init(sender, e);
        }
        protected override void BaseElementInit()
        {
            BaseScreenType = "Extend";
            BaseScreenID = _ScreenID;
            BaseAttachmentGrid = gridAttachment;
            BaseAttachmentUpload = uploadAttachment;
            BaseAttachmentPopup = popdlgUploadAttachment;
            BaseAttachmentCategoryDropDown = cboxAttachmentCategory;
            BaseNoteRecipientDropDown = ddlSendTo;
            BaseNoteTextBox = txtNotice;
            BaseNoteRepeater = rptrNotice;
            BaseNoteCommentContainer = noticeComment;
            BaseHiddenMessageExpandFlag = hdExpandFlag;
            BaseMessageControlLiteral = messageControl;
        }
        protected override void BaseDataInit()
        {
            BaseData = new BaseAccrData();
            BaseData.Notes = Notes;
            BaseData.AttachmentTable = AttachmentTable;

            if(GetSessKey().isNotEmpty())
                BaseData.AccruedNo = GetSessKey();

            if (AccruedData == null || AccruedData.CREATED_DT == null) 
                return;

            
            BaseData.AccruedNo = AccruedData.EXTEND_NO;
            BaseData.ReffNo = AccruedData.REFF_NO;
            BaseData.DivisionID = AccruedData.DIVISION_ID;
            BaseData.StatusCode = AccruedData.STATUS_CD;
            BaseData.CreatedDt = AccruedData.CREATED_DT;
        }
        protected void reloadFooter()
        {
            if (BaseData == null)
                return;

            reloadNote();
            reloadAttachment();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            hidScreenID.Value = _ScreenID;
            lit = litMessage;
            clearScreenMessage();

            if (!IsPostBack)
            {
                _extendNo = Request["extend_no"];
                SetSessKey(_extendNo);
                if (_extendNo.Equals("new"))
                {
                    IsNewAccrued = true;
                }
                else
                {
                    IsNewAccrued = false;
                }

                PrepareLogout();
                ResetHeader();
                PrepareDataHeader();
                BaseDataInit();

                SetFirstLoad();
                SetDataList();
                ResetFooter();
                reloadFooter();
                if(IsNewAccrued)
                    resetAttachment();

                ListActivity = null;

                #region Init Startup Script

                gridGeneralInfo.CancelEdit();

                if (_extendNo.Equals("new"))
                {
                    StartEditing(true);
                }

                SetButtonAvailability();
                #endregion

            }
        }

        private void loadApprovalHistory()
        {
            List<AccrFormHistory> data = logic.AccrForm.getApprovalHistory(AccruedData.EXTEND_NO, AccruedData.STATUS_CD, UserData);
            gridApprovalHistory.DataSource = data;
            gridApprovalHistory.DataBind();
        }
        protected void gridApprovalHistory_Load(object sender, EventArgs e)
        {
            if (!Request["extend_no"].Equals("new"))
                loadApprovalHistory();
        }

        protected void gridApprovalHistory_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                AccrFormHistory data = (AccrFormHistory)gridApprovalHistory.GetRow(e.VisibleIndex);
                if (data != null)
                {
                    Image imgStatus = gridApprovalHistory.FindRowCellTemplateControl(e.VisibleIndex, gridApprovalHistory.Columns["status"] as GridViewDataColumn, "imgStatus") as Image;

                    string img = "wait";
                    int stsCd = data.STATUS;
                    if (stsCd == 1)
                    {
                        img = "check";
                    }
                    else if (stsCd == 2)
                    {
                        img = "cross";
                    }


                    string logoPath = AppSetting.CompanyLogo.Substring(1, AppSetting.CompanyLogo.LastIndexOf("/"));
                    imgStatus.ImageUrl = logoPath + img + ".gif";
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
        }
        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                
            }

            #region rowtype edit
        
            if (e.RowType == GridViewRowType.InlineEdit)
            {
                #region init

                //Get Element by ID
                //ASPxTextBox txtGridBookingNo = gridGeneralInfo.FindEditRowCellTemplateControl(
                //    gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "txtGridBookingNo")
                //    as ASPxTextBox;



                ASPxGridLookup ddlGridBookingNo = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "ddlGridBookingNo")
                    as ASPxGridLookup;
                ASPxTextBox txtGridExtendAmt = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["EXTEND_AMT"] as GridViewDataColumn, "txtGridExtendAmt")
                    as ASPxTextBox;
                ASPxComboBox extendType = gridGeneralInfo.FindEditRowCellTemplateControl(
                    gridGeneralInfo.Columns["EXTEND_TYPE"] as GridViewDataColumn, "extendType")
                    as ASPxComboBox;

                #endregion

                #region edit
                if (PageMode == Common.Enum.PageMode.Edit)
                {

                    int _VisibleIndex = EditIndex;

                    if (_VisibleIndex == e.VisibleIndex)
                    {
                        #region fill data edit

                        //Get value Data Edited
                        string bookingNo = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "BOOKING_NO"));
                        string extendAmt = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "EXTEND_AMT"));

                        //Set SelectBox WBS No (New)
                        //List<WBSStructure> lstWbsNumber = logic.AccrWBS.getWbsNumbers();
                        //ddlGridWBSNoNew.DataSource = lstWbsNumber;
                        //ddlGridWBSNoNew.DataBind();

                        //Set Readonly fields
                        ddlGridBookingNo.Value = ListInquiry[_VisibleIndex].BOOKING_NO;
                        txtGridExtendAmt.Value = ListInquiry[_VisibleIndex].EXTEND_AMT;
                        extendType.Value = ListInquiry[_VisibleIndex].EXTEND_TYPE;
                        extendType.Text = ListInquiry[_VisibleIndex].EXTEND_TYPE;


                        #endregion
                    }
                }
                #endregion
            }
            
            #endregion
        }
        protected void gridGeneralInfo_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            int curRow = e.VisibleIndex;

            if (e.DataColumn == null) return;
            
            ListInquiry = GetSessData();
            if (e.DataColumn.FieldName == "EXTEND_AMT")
            {
                string extAmtStr = e.GetValue("EXTEND_AMT") == null ? "" : e.GetValue("EXTEND_AMT").ToString();
                decimal extAmt = 0;

                if (Decimal.TryParse(extAmtStr, out extAmt))
                {
                    var d = ListInquiry[curRow];
                    if (d.EXTEND_TYPE.isNotEmpty())
                    {
                        decimal? maxAmt = d.EXTEND_TYPE_CD == 0 ? d.RECLAIMABLE_AMT : d.EXTENDABLE_AMT;
                        if (maxAmt < extAmt)
                        {
                            e.Cell.Style.Add("background-color", red);
                        }
                    }
                }
            }
        }

        protected void DoSearch()
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
                return;
            }

            if (ValidateInputSearch())
            {
                SetDataList();
            }
        }


        #region Set Button Visible
        private void SetFirstLoad()
        {
            GridViewColumn columnDeletion = gridGeneralInfo.Columns["DeletionControl"];
            columnDeletion.Visible = false;
            IsEditing = false;
            PageMode = Common.Enum.PageMode.View;

            SetButtonAvailability();
        }
        #endregion

        #region Grid

        
        protected void SetDataList()
        {
            string AccrExtNo = GetSessKey();

            if (!AccrExtNo.isEmpty())
            {
                ListInquiry = logic.AccrExtend.SearchForm(AccrExtNo).ToList();

                //add by FID.Ridwan 04072018
                var q = (from vab in ListInquiry
                         select new AccruedExtendData()
                         {
                             EXTEND_NO = vab.EXTEND_NO,
                             BOOKING_NO = vab.BOOKING_NO,
                             BOOKING_NO_OLD = vab.BOOKING_NO,
                             PV_TYPE_NAME = vab.PV_TYPE_NAME,
                             WBS_NO_OLD = vab.WBS_NO_OLD,
                             WBS_DESC_OLD = vab.WBS_DESC_OLD,
                             SUSPENSE_NO_OLD = vab.SUSPENSE_NO_OLD,
                             WBS_NO_PR = vab.WBS_NO_PR,
                             WBS_DESC_PR = vab.WBS_DESC_PR,
                             SUSPENSE_NO_PR = vab.SUSPENSE_NO_PR,
                             RECLAIMABLE_AMT = vab.RECLAIMABLE_AMT,
                             EXTENDABLE_AMT = vab.EXTENDABLE_AMT,
                             EXTEND_TYPE = vab.EXTEND_TYPE,
                             EXTEND_TYPE_CD = vab.EXTEND_TYPE_CD,
                             EXTEND_AMT = vab.EXTEND_AMT,
                             CREATED_BY = vab.CREATED_BY,
                             CREATED_DT = vab.CREATED_DT,
                             CHANGED_BY = vab.CHANGED_BY,
                             CHANGED_DT = vab.CHANGED_DT
                         });

                List<AccruedExtendData> _list = null;
                _list = logic.AccrExtend.GetViewActivity(AccrExtNo);

                ListInquiry = (from a in q
                                join b in _list on new { EXTEND_NO = a.EXTEND_NO, BOOKING_NO = a.BOOKING_NO } equals new { EXTEND_NO = b.EXTEND_NO, BOOKING_NO = b.BOOKING_NO } 
                                into ab
                                from b in ab.DefaultIfEmpty()
                                select new AccruedExtendData()
                                {
                                    ACTIVITY_DES_VIEW = (b != null ? b.ACTIVITY_DES_VIEW : ""),
                                    EXTEND_NO = a.EXTEND_NO,
                                    BOOKING_NO = a.BOOKING_NO,
                                    BOOKING_NO_OLD = a.BOOKING_NO,
                                    PV_TYPE_NAME = a.PV_TYPE_NAME,
                                    WBS_NO_OLD = a.WBS_NO_OLD,
                                    WBS_DESC_OLD = a.WBS_DESC_OLD,
                                    SUSPENSE_NO_OLD = a.SUSPENSE_NO_OLD,
                                    WBS_NO_PR = a.WBS_NO_PR,
                                    WBS_DESC_PR = a.WBS_DESC_PR,
                                    SUSPENSE_NO_PR = a.SUSPENSE_NO_PR,
                                    RECLAIMABLE_AMT = a.RECLAIMABLE_AMT,
                                    EXTENDABLE_AMT = a.EXTENDABLE_AMT,
                                    EXTEND_TYPE = a.EXTEND_TYPE,
                                    EXTEND_TYPE_CD = a.EXTEND_TYPE_CD,
                                    EXTEND_AMT = a.EXTEND_AMT,
                                    CREATED_BY = a.CREATED_BY,
                                    CREATED_DT = a.CREATED_DT,
                                    CHANGED_BY = a.CHANGED_BY,
                                    CHANGED_DT = a.CHANGED_DT
                                }).ToList();

                int count = 1;
                foreach (var data in ListInquiry)
                {
                    data.DisplaySequenceNumber = count;
                    data.ACTIVITY_DES_VIEW = data.ACTIVITY_DES_VIEW.Replace(";", "<BR/>");
                    count++;
                }
            }
            else
            {
                ListInquiry = null;
            }

            SetSessData(ListInquiry);
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }

        #endregion
        #endregion

        #region Buttons
        protected void SetButtonAvailability()
        {
            bool hasWl = false;
            if (!IsNewAccrued)
            {
                hasWl = hasWorklist(AccruedData.EXTEND_NO);
            }

            btnSave.Visible = IsEditing;
            btnCancel.Visible = IsEditing && !IsNewAccrued;

            btnEditRow.Visible = !IsEditing
                                && AccruedData != null 
                                && (AccruedData.STATUS_CD == 0 || (AccruedData.STATUS_CD == 511 && hasWl))
                                && RoLo.isAllowedAccess("btnEditRow");
            btnDownload.Visible = RoLo.isAllowedAccess("btnDownload");

            btnSubmit.Visible = !IsEditing 
                                && AccruedData != null 
                                && (AccruedData.STATUS_CD == 0 || (AccruedData.STATUS_CD == 511 && hasWl))
                                && RoLo.isAllowedAccess("btnSubmit");

            btnApprove.Visible = !IsEditing
                                && AccruedData != null
                                && hasWl && CanApproveReject.Contains(AccruedData.STATUS_CD)
                                && RoLo.isAllowedAccess("btnApprove");

            ltExtendDt.Visible = btnApprove.Visible;
            
            var now = DateTime.Now.AddDays(-1);
            dtExtendDt.MinDate = now;
            dtExtendDt.Visible = btnApprove.Visible;

            btnRevise.Visible = !IsEditing
                                && AccruedData != null
                                && hasWl && CanApproveReject.Contains(AccruedData.STATUS_CD)
                                && RoLo.isAllowedAccess("btnRevise");

            if (IsEditing 
                || IsNewAccrued 
                || new int[] { 0, 511 }.Contains(AccruedData.STATUS_CD))
            {
                AttachmentTable.openEditing();
            }
            else
            {
                AttachmentTable.closeEditing();
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            logic.Say("btnSave_Click", "Save");
            bool valid = false;
            if(ValidateSave()){
                ListInquiry = GetSessData();
                string AccrExtNo = GetSessKey();
                if (IsNewAccrued)
                {
                    bool existExtNo = logic.AccrExtend.IsExist(GetSessKey());
                    if (existExtNo)
                    {
                        AccrExtNo = GetNewExtendNo();
                    }
                }
                
                valid = logic.AccrExtend.InsertUpdateDetail(AccrExtNo, ListInquiry, ListActivity, UserData);
                
            }

            if (valid)
            {
                bool isNew = false;
                if (IsNewAccrued)
                {
                    isNew = true;
                }

                IsNewAccrued = false;
                SetDataList();
                ResetHeader();
                PrepareDataHeader();
                SetFirstLoad();
                //gridGeneralInfo.UpdateEdit();
                gridGeneralInfo.CancelEdit();

                if (isNew)
                {
                    PostLater(ScreenMessage.STATUS_INFO, "MSTD00011INF", "Accrued Extend");
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_INFO, "MSTD00013INF", "Accrued Extend");
                }

                
            }
            else
            {
                if (IsNewAccrued)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00010ERR", "Accrued Extend");
                    ReloadGrid();
                    StartEditingTabel();
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00012ERR", "Accrued Extend");
                    ReloadGrid();
                    StartEditingTabel();
                }
                
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            PageMode = Common.Enum.PageMode.View;
            SetFirstLoad();
            SetDataList();

            ListActivity = null;

            #region Init Startup Script

            gridGeneralInfo.CancelEdit();

            #endregion

        }
        protected void btEdit_Click(object sender, EventArgs e)
        {
            logic.Say("btEdit_Click", "Edit");
            
            StartEditing();
        }

        protected void StartEditing(bool isNew = false)
        {
            if (isNew)
            {
                SetSessData(null);
                AddBlankColumn();
            }

            IsEditing = true;
            PageMode = Common.Enum.PageMode.Edit;

            SetButtonAvailability();
            //clearScreenMessage();

            ReloadGrid();
            StartEditingTabel();
        }

        protected void StartEditingTabel()
        {
            GridViewColumn column = gridGeneralInfo.Columns["DeletionControl"];
            column.Visible = true;


            gridGeneralInfo.CancelEdit();
            int cntData = ListInquiry.Count();
            if (cntData > 0)
            {
                ASPxGridLookup ddlGridBookingNo = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["BOOKING_NO"] as GridViewDataColumn, "ddlGridBookingNo")
                        as ASPxGridLookup;

                string bookingNo = Convert.ToString(gridGeneralInfo.GetRowValues(cntData - 1, "BOOKING_NO"));
                EditIndex = cntData - 1;
                //decimal maxAmt = ListInquiry[EditIndex].RECLAIMABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].RECLAIMABLE_AMT : 0;
                //if (ListInquiry[EditIndex].RECLAIMABLE_AMT < ListInquiry[EditIndex].EXTENDABLE_AMT)
                //{
                //    maxAmt = ListInquiry[EditIndex].EXTENDABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].EXTENDABLE_AMT : 0;
                //}
                //gridGeneralInfo.JSProperties["cpMaxAmt"] = maxAmt;
                gridGeneralInfo.StartEdit(cntData - 1);
                //ddlGridBookingNo.Value = bookingNo;

            }
            else
            {
                initFirstRow();
            }
        }
        public void initFirstRow()
        {
            AddBlankColumn(); // todo
            ReloadGrid();
            gridGeneralInfo.StartEdit(0);
            EditIndex = 0;
            gridGeneralInfo.SettingsBehavior.AllowFocusedRow = false;
        }

        private void AddBlankColumn(int idxNew = 1)
        {
            AccruedExtendData temp = new AccruedExtendData();
            temp.DisplaySequenceNumber = idxNew;
            temp.EXTEND_NO = GetSessKey();
            ListInquiry = GetSessData();
            ListInquiry.Add(temp);
            SetSessData(ListInquiry);
        }
        
        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnDownload_Click(object sender, EventArgs e)
        {
            UserData userdata = (UserData)Session["UserData"];
            try
            {
                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "AccruedExtendReportTemplate.xls");

                string excelName = "Accrued_Extend_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
                // fid.Hadid 20190315. wrap text issue on Spire. no need to convert to pdf, for temporary solution.
                //string pdfName = "Accrued_Extend_Report_" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
                string pdfName = "";


                string fullPath = logic.AccrExtend.GenerateExtendFormReport(
                            UserName,
                            templatePath,
                            excelName,
                            GetSessKey(),
                            IssuingDiv,
                            userdata,
                            pdfName
                        );
                Xmit.Transmit(HttpContext.Current, fullPath);

            }
            catch (Exception ex)
            {
                string xmsg = null;
                if (ex.InnerException != null)
                {
                    xmsg =
                        logic.Msg.WriteMessage(ProcessID,
                            "MSTD00042ERR", "Commit Transaction", UserName,
                            LoggingLogic.Trace(ex));
                }
                Nag("MSTD00042ERR", xmsg.isNotEmpty() ? xmsg : ex.Message);
            }

            //return fileName;
        
        }

        protected void evt_imgDeleteRow_clicked(object sender, EventArgs args)
        {
            KeyImageButton image = (KeyImageButton)sender;
            string sequenceNumber = image.Key;
            int numSequenceNumber = int.Parse(sequenceNumber.Trim());
            logic.Say("evt_imgDeleteRow_clicked", "seq={0}", numSequenceNumber);
            gridGeneralInfo.CancelEdit();
            int removedList = numSequenceNumber - 1;

            //string removedBookNo = ListInquiry[removedList].BOOKING_NO;
            //ListActivity.Remove(removedBookNo);

            ListInquiry = GetSessData();
            ListInquiry.RemoveAt(removedList);

            for (int idx = 0; idx < ListInquiry.Count; idx++)
            {
                ListInquiry[idx].DisplaySequenceNumber = idx + 1;
            }

            SetSessData(ListInquiry);
            ReloadGrid();
            if(EditIndex > removedList){
                EditIndex = EditIndex - 1;
            }
            gridGeneralInfo.StartEdit(EditIndex);
            
        }
        protected void evt_imgDeleteAllRow_clicked(object sender, EventArgs args)
        {
            logic.Say("evt_imgDeleteAllRow_clicked", "Delete All Row");

            ListActivity = null;
            ListInquiry = null;
            SetSessData(ListInquiry);
            EditIndex = 0;
            AddBlankColumn();
            ReloadGrid();
            gridGeneralInfo.StartEdit(0);
        }
        protected void evt_imgAddRow_clicked(object sender, EventArgs arg)
        {
            logic.Say("evt_imgAddRow_clicked", "Add Row {0}", ListInquiry.Count + 1);

            gridGeneralInfo.CancelEdit();
            //gridGeneralInfo.UpdateEdit();
            int NewDataIndex = ListInquiry.Count + 1;
            AddBlankColumn(NewDataIndex);
            EditIndex = ListInquiry.Count - 1;
            ReloadGrid();
            gridGeneralInfo.StartEdit(EditIndex);
        }
        public void AddNewRowToGrid()
        {
            
        }

        protected void evt_gridGeneralInfo_onCellEditorInitialize(object sender, ASPxGridViewEditorEventArgs arg)
        {
            
            string fieldName = arg.Column.FieldName;

            if (enabledFields.Contains(fieldName))
            {
                arg.Editor.Enabled = true;
            }
            else
            {
                arg.Editor.Enabled = false;
            }
        }

        protected void grid_CustomButtonCallback(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            string bookingNo = gridGeneralInfo.GetRowValues(e.VisibleIndex, "BOOKING_NO").ToString();

            ViewActivity(bookingNo);
        }
        

        #endregion

        #region Function

        public void ReloadGrid()
        {
            ListInquiry = GetSessData();
            ListInquiry = ListInquiry.OrderBy(x => x.BOOKING_NO).ToList();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
        }

        public void OnTabKeypress(bool taxCalculated)
        {
            gridGeneralInfo.UpdateEdit();
            //int idxSelected = Tabel.EditIndex;
            //int lastDataIndex = Tabel.DataList.Count - 1;
            //if (Tabel.EditIndex == lastDataIndex)
            //{
            //    Tabel.addNewRow(taxCalculated);
            //    idxSelected = Tabel.EditIndex + 1;
            //}
            //else
            //{
            //    idxSelected++;
            //}

            //ReloadGrid();
            int idxSelected = 1;
            gridGeneralInfo.StartEdit(idxSelected);
            //renderTotalAmount();
        }
        private bool ValidateInputSearch()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Handle(ex);
                throw ;
            }
            return true;
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;
            string field = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";

            int _VisibleIndex = EditIndex;

            ListInquiry = GetSessData();
            if (field.Equals("bookNo"))
            {
                string bookNo = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
                if (!string.IsNullOrEmpty(bookNo))
                {
                    if(!bookNo.Equals(ListInquiry[_VisibleIndex].BOOKING_NO)){

                    }
                    AccruedExtendData temp = logic.AccrExtend.GetBookingDetail(bookNo).FirstOrDefault();
                    if (temp != null)
                    {
                        //List<AccruedExtendData> bookingExist = ListInquiry.Where(p => p.BOOKING_NO == bookNo).ToList();
                        
                        ListInquiry[_VisibleIndex].BOOKING_NO = bookNo;
                        ListInquiry[_VisibleIndex].WBS_DESC_OLD = temp.WBS_DESC_OLD;
                        ListInquiry[_VisibleIndex].WBS_NO_OLD = temp.WBS_NO_OLD;
                        ListInquiry[_VisibleIndex].PV_TYPE_NAME = temp.PV_TYPE_NAME;
                        ListInquiry[_VisibleIndex].SUSPENSE_NO_OLD = temp.SUSPENSE_NO_OLD;
                        ListInquiry[_VisibleIndex].WBS_NO_PR = temp.WBS_NO_PR;
                        ListInquiry[_VisibleIndex].WBS_DESC_PR = temp.WBS_DESC_PR;
                        ListInquiry[_VisibleIndex].SUSPENSE_NO_PR = temp.SUSPENSE_NO_PR;
                        ListInquiry[_VisibleIndex].EXTENDABLE_AMT = temp.EXTENDABLE_AMT;
                        ListInquiry[_VisibleIndex].RECLAIMABLE_AMT = temp.RECLAIMABLE_AMT;
                        ListInquiry[_VisibleIndex].EXTEND_AMT = 0;
                    }
                    else
                    {
                        ListInquiry[_VisibleIndex].BOOKING_NO = "";
                        ListInquiry[_VisibleIndex].WBS_DESC_OLD = "";
                        ListInquiry[_VisibleIndex].WBS_NO_OLD = "";
                        ListInquiry[_VisibleIndex].PV_TYPE_NAME = "";
                        ListInquiry[_VisibleIndex].SUSPENSE_NO_OLD = null;
                        ListInquiry[_VisibleIndex].WBS_NO_PR = "";
                        ListInquiry[_VisibleIndex].WBS_DESC_PR = "";
                        ListInquiry[_VisibleIndex].SUSPENSE_NO_PR = null;
                        ListInquiry[_VisibleIndex].EXTENDABLE_AMT = null;
                        ListInquiry[_VisibleIndex].RECLAIMABLE_AMT = null;
                        ListInquiry[_VisibleIndex].EXTEND_AMT = 0;
                    }

                    //decimal maxAmt = ListInquiry[EditIndex].RECLAIMABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].RECLAIMABLE_AMT : 0;
                    //if (ListInquiry[EditIndex].RECLAIMABLE_AMT < ListInquiry[EditIndex].EXTENDABLE_AMT)
                    //{
                    //    maxAmt = ListInquiry[EditIndex].EXTENDABLE_AMT.HasValue ? (decimal)ListInquiry[EditIndex].EXTENDABLE_AMT : 0;
                    //}
                    //gridGeneralInfo.JSProperties["cpMaxAmt"] = maxAmt;
                    
                    
                }
            }
            else if (field.Equals("extAmount"))
            {
                decimal extAmount = (paramFracs != null && paramFracs.Length > 3) ? Convert.ToDecimal(paramFracs[3]) : 0;
                ListInquiry[_VisibleIndex].EXTEND_AMT = extAmount;
                //decimal maxAmt = (decimal)ListInquiry[_VisibleIndex].RECLAIMABLE_AMT;
                //if(ListInquiry[_VisibleIndex].RECLAIMABLE_AMT < ListInquiry[_VisibleIndex].EXTENDABLE_AMT){
                //    maxAmt = (decimal)ListInquiry[_VisibleIndex].EXTENDABLE_AMT;
                //}
                //if (extAmount <= maxAmt)
                //{
                //    ListInquiry[_VisibleIndex].EXTEND_AMT = extAmount;
                    
                //}
                //else
                //{
                //    //gridGeneralInfo.JSProperties["cpAlertMessage"] = "Amount Extend/Reclaim berlebih";
                //}
                
            }
            else if (field.Equals("extType"))
            {
                string extendType = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
                string extendTypeLabel = (paramFracs != null && paramFracs.Length > 4) ? paramFracs[4] : "";
                ListInquiry[_VisibleIndex].EXTEND_TYPE_CD = Convert.ToInt32(extendType);
                ListInquiry[_VisibleIndex].EXTEND_TYPE = extendTypeLabel;
            }
            ListInquiry = ListInquiry;
            SetSessData(ListInquiry);
            gridGeneralInfo.CancelEdit();
            gridGeneralInfo.DataSource = ListInquiry;
            gridGeneralInfo.DataBind();
            gridGeneralInfo.StartEdit(_VisibleIndex);
        }

        protected void gridActivity_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs arg)
        {
            ASPxGridView grid = sender as ASPxGridView;
            string param = arg.Parameters;
            if (param.isEmpty()) return;
            string[] paramFracs = param.Split(':');
            string prefix = (paramFracs != null && paramFracs.Length > 0) ? paramFracs[0] : "";
            if (!prefix.Equals("p")) return;
            string command = (paramFracs != null && paramFracs.Length > 1) ? paramFracs[1] : "";
            if (!command.Equals("update")) return;
            string field = (paramFracs != null && paramFracs.Length > 2) ? paramFracs[2] : "";
            Dictionary<string, List<string>> savedActivity = ListActivity;
            List<string> listSelectedAct = new List<string>();
            TempPopUpActivity = PopUpActivity;
            string index = (paramFracs != null && paramFracs.Length > 3) ? paramFracs[3] : "";
            if (!string.IsNullOrEmpty(index))
            {
                int idx = Convert.ToInt32(index);
                string selectedAct = PopUpActivity[idx].ACTIVITY_DES;
                
                if (field.Equals("active"))
                {
                    //user checking new item
                    TempPopUpActivity[idx].IS_CHECKED = true;
                }
                else
                {
                    //user unchecking exist item
                    TempPopUpActivity[idx].IS_CHECKED = false;
                }
                ListActivity = savedActivity;
            }

            gridActivity.DataSource = TempPopUpActivity;
            gridActivity.DataBind();
            
        }

        protected void evt_gridGeneralInfo_onRowUpdating(object sender, ASPxDataUpdatingEventArgs args)
        {
            //decimal amt = 0;
            //OnRowUpdating(sender, args);
        }

        protected void evt_gridGeneralInfo_onSelectionChanged(object sender, EventArgs args)
        {

            if (PageMode == Common.Enum.PageMode.Edit)
            {
                logic.Say("evt_gridGeneralInfo_onSelectionChanged", "Change Row");
                int i = getSelectedRow();
                gridGeneralInfo.CancelEdit();
                EditIndex = i;
                ReloadGrid();
                //decimal maxAmt = ListInquiry[EditIndex].RECLAIMABLE_AMT.HasValue?(decimal)ListInquiry[EditIndex].RECLAIMABLE_AMT:0;
                //if (ListInquiry[EditIndex].RECLAIMABLE_AMT < ListInquiry[EditIndex].EXTENDABLE_AMT)
                //{
                //    maxAmt = ListInquiry[EditIndex].EXTENDABLE_AMT.HasValue?(decimal)ListInquiry[EditIndex].EXTENDABLE_AMT:0;
                //}
                //gridGeneralInfo.JSProperties["cpMaxAmt"] = maxAmt;
                gridGeneralInfo.StartEdit(EditIndex);
            }
            else
            {
                return;
            }
            
            //int i = OnSelectionChanged(sender, args);
            //string cc = "", des = "", curr = "", gla = "";
            //decimal amt = 0;
            //int seq = 0;
            //if (i >= 0 && i < Tabel.DataList.Count)
            //{
            //    seq = Tabel.DataList[i].SequenceNumber;
            //    cc = Tabel.DataList[i].CostCenterCode;
            //    curr = Tabel.DataList[i].CurrencyCode;
            //    des = Tabel.DataList[i].Description;
            //    amt = Tabel.DataList[i].Amount;
            //    gla = Tabel.DataList[i].GlAccount;
            //}
            //logic.Say("evt_gridPVDetail_onSelectionChanged", "{0} OnSelectionChanged [{1}] @{2} - {3} '{4}' {5} {6} {7}",
            //    ScreenType, i, seq, cc, des, curr, amt, gla);
            
        }

        public int getSelectedRow()
        {
            ReloadGrid();
            WebDataSelection selection = gridGeneralInfo.Selection;
            int cntData = ListInquiry.Count;
            for (int i = 0; i < cntData; i++)
            {
                if (selection.IsRowSelected(i))
                {
                    return i;
                }
            }
            return 0;
        }

        protected void PrepareDataHeader()
        {
            if (AccruedData == null)
            {
                IssuingDiv = normalizeDivisionName(UserData.DIV_NAME);
                tboxIssueDiv.Text = IssuingDiv;
                if (IsNewAccrued)
                {
                    string AccrExtNo = GetNewExtendNo();
                    //tboxAccruedNo.Text = AccrExtNo;
                    tboxTotAmt.Text = "0";
                    
                }
            }
            else
            {
                IssuingDiv = normalizeDivisionName(logic.Vendor.GetDivisionName(AccruedData.DIVISION_ID));
                tboxIssueDiv.Text = IssuingDiv;
                tboxTotAmt.Text = AccruedData.TOT_AMOUNT.fmt();
                tboxAccruedNo.Text = AccruedData.EXTEND_NO;
                tboxPicCur.Text = AccruedData.PIC_CURRENT;
                tboxSubmSts.Text = AccruedData.SUBMISSION_STATUS;
                tboxUpdateDt.Text = AccruedData.CREATED_DT.ToString("dd MMM yyyy");
                tboxPicNext.Text = AccruedData.PIC_NEXT;
                tboxWFSts.Text = AccruedData.WORKFLOW_STATUS.HasValue ? logic.Sys.GetText("WORKFLOW_STATUS", AccruedData.WORKFLOW_STATUS.str()) : "";
            }

            var ExchRate = logic.Look.getExchangeRates();

            tboxUsdIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "USD")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);

            tboxJpyIdr.Text = ExchRate
                .Where(x => x.CurrencyCode == "JPY")
                .Select(x => x.Rate)
                .FirstOrDefault()
                .fmt(2);
        }

        private string GetNewExtendNo(){
            string extNo = "AE-" + IssuingDiv + "-" + DateTime.Now.Year.ToString() + "-";
            List<AccruedExtendData> listHeadAccr = logic.AccrExtend.GetListDivAccrExtend(extNo).ToList();
            int maxNumb = 0;
            foreach (AccruedExtendData temp in listHeadAccr)
            {
                string[] splitedExtNo = temp.EXTEND_NO.Split('-');
                int NumbInRow = Convert.ToInt32(splitedExtNo[splitedExtNo.Length - 1]);
                if (NumbInRow > maxNumb)
                {
                    maxNumb = NumbInRow;
                }
            }
            maxNumb++;
            string seqExtendNo = maxNumb.ToString();
            string AccrExtNo = extNo + seqExtendNo.PadLeft(2, '0');
            SetSessKey(AccrExtNo);
            return AccrExtNo;
        }

        protected string normalizeDivisionName(string divName)
        {
            if (divName != null)
            {
                if (divName.ToLower().EndsWith("head"))
                {
                    divName = divName.Substring(0, divName.IndexOf('-'));
                }
            }
            return divName;
        }

        protected int GetFirstSelectedRow()
        {
            int row = -1;
            for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
            {
                if (!gridGeneralInfo.Selection.IsRowSelected(i)) continue;

                row = i;
                break;
            }
            return row;
        }
        #endregion

        private int GetEditIndex()
        {
            int editIndex = 0;
            if (Session["editIndex"] != null)
            {
                editIndex = Convert.ToInt32(Session["editIndex"]);
            }
            return editIndex;
        }

        protected void LookupBookingNo_Load(object sender, EventArgs arg)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                return;
            }

            ASPxGridLookup lookup = (ASPxGridLookup)sender;
            string divId = UserData.DIV_CD.ToString();
            List<AccruedExtendData> lstBookingNo = logic.AccrExtend.GetDataBookingNo(divId)
                                                        .Distinct()
                                                        .OrderBy(x => x.BOOKING_NO)
                                                        .ToList();
            lookup.DataSource = lstBookingNo;
            lookup.DataBind();

        }

        protected void ResetHeader()
        {
            AccruedData = null;
        }

        protected void gridActivity_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region edit
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                if (gridActivity.VisibleRowCount > 0)
                {

                    if (IsEditActivity)
                    {
                        for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                        {
                            ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                                (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                            if(x != null)
                                x.ReadOnly = false;
                        }
                    }
                    else
                    {
                        for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                        {
                            ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                                (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                            if (x != null)
                                x.ReadOnly = true;
                        }
                    }
                }
            }
            else
            {
                for (int i = 0; i < gridActivity.VisibleRowCount; i++)
                {
                    ASPxCheckBox x = (ASPxCheckBox)gridActivity.FindRowCellTemplateControl(i,
                        (GridViewDataColumn)gridActivity.Columns["checkList"], "checkList");
                    if (x != null)
                        x.ReadOnly = true;
                }
            }
            #endregion
        }

        private void SetSessData(List<AccruedExtendData> p){
            ListInquiry = p;
            Session["dataGrid"] = p;
        }
        private List<AccruedExtendData> GetSessData()
        {
            List<AccruedExtendData> ret = (List<AccruedExtendData>)Session["dataGrid"];
            return ret;
        }

        private void SetSessKey(string extendNo)
        {
            Session["extendNo"] = extendNo;
        }
        private string GetSessKey()
        {
            return Convert.ToString(Session["extendNo"] ?? "");
        }

        private bool ValidateSave()
        {
            ListInquiry = GetSessData();
            
            // Mandatory
            bool isEmptyBn = ListInquiry.Any(x => x.BOOKING_NO.isEmpty());
            bool isEmptyType = ListInquiry.Any(x => x.EXTEND_TYPE.isEmpty());
            bool isEmptyAmt = ListInquiry.Any(x => (x.EXTEND_AMT ?? 0) == 0);

            // Business
            bool isValidBal = ValidateBalance();
            
            if (isEmptyBn)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Booking No.");

            if (isEmptyType)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Extend Type");

            if (isEmptyAmt)
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Extend Amount");

            return !isEmptyBn
                && !isEmptyType
                && !isEmptyAmt
                && isValidBal;
        }

        protected void btnActivity_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            IsEditActivity = false;
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                ViewActivity(s.CommandName,1);
            }
            else
            {
                ViewActivity(s.CommandName);
            }
            
        }

        protected void btSendUploadAttachment_Clicked(object sender, EventArgs e)
        {
            evt_btSendUploadAttachment_clicked(sender, e);
            ReloadGrid();
            if (IsEditing)
            {
                StartEditingTabel();
            }
        }

        protected void btnAddActivity_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            ListInquiry = GetSessData();
            IsEditActivity = true;
            ActivityBookingNo = ListInquiry[Convert.ToInt32(s.CommandName)].BOOKING_NO;
            ViewActivity(ActivityBookingNo, 1);
        }

        protected void ViewActivity(string bookingNo, int type = 0)
        {
            logic.Say("btnActivity_Click", "Activity");
            panelActivity.GroupingText = "Simulation";
            gridActivity.BeginUpdate();
            gridActivity.Visible = true;
            string extendNo = GetSessKey();
            List<AccruedListDetail> _list = null;
            List<string> _listActChecked = new List<string>();

            if (IsEditActivity)
            {
                btnSaveActivity.Visible = true;
                btnCancelActivity.Visible = true;
                btnCloseActivity.Visible = false;
            }
            else
            {
                btnSaveActivity.Visible = false;
                btnCancelActivity.Visible = false;
                btnCloseActivity.Visible = true;
            }

            if (type == 0)
            {
                _list = logic.AccrExtend.GetCurrentActivity(extendNo, bookingNo);
                foreach (AccruedListDetail temp in _list)
                {
                    _listActChecked.Add(temp.ACTIVITY_DES);
                }
            }
            else
            {
                if (ListActivity.ContainsKey(bookingNo))
                {
                    _listActChecked = ListActivity[bookingNo];
                }
                _list = logic.AccrExtend.GetCurrentActivity(extendNo, bookingNo);
                foreach (AccruedListDetail temp in _list)
                {
                    if (!_listActChecked.Contains(temp.ACTIVITY_DES))
                    {
                        _listActChecked.Add(temp.ACTIVITY_DES);
                    }
                    
                }
                ListActivity[bookingNo] = _listActChecked;
            }
            _list = logic.AccrExtend.GetActivityList(bookingNo);
            if (_list != null)
            {
                for (int i = 0; i < _list.Count; i++ )
                {
                    if (_listActChecked.Contains(_list[i].ACTIVITY_DES))
                    {
                        _list[i].IS_CHECKED = true;
                    }
                }
            }

            PopUpActivity = _list;
            gridActivity.DataSourceID = dsActivity.ID;
            gridActivity.DataBind();

            lblBookingNo.Text = bookingNo;

            gridActivity.DetailRows.ExpandAllRows();
            gridActivity.EndUpdate();
            (popupActivity as ModalPopupExtender).Show();
        }

        protected void dsActivity_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = PopUpActivity;
        }

        protected void btnCloseActivity_Click(object sender, EventArgs e)
        {
            lblBookingNo.Text = "";
            (popupActivity as ModalPopupExtender).Hide();
        }

        protected void btnSaveActivity_Click(object sender, EventArgs e)
        {
            Dictionary<string, List<string>> savedActivity = ListActivity;
            List<string> listSelectedAct = new List<string>();
            PopUpActivity = TempPopUpActivity;
            foreach (AccruedListDetail temp in PopUpActivity)
            {
                if (temp.IS_CHECKED)
                {
                    listSelectedAct.Add(temp.ACTIVITY_DES);
                }
            }
            if (savedActivity.ContainsKey(ActivityBookingNo))
            {
                savedActivity[ActivityBookingNo] = listSelectedAct;
            }
            else
            {
                savedActivity.Add(ActivityBookingNo, listSelectedAct);
            }
            TempPopUpActivity = null;
            PopUpActivity = null;
            (popupActivity as ModalPopupExtender).Hide();
            ReloadGrid();
            StartEditingTabel();
        }
        protected void btnCancelActivity_Click(object sender, EventArgs e)
        {
            TempPopUpActivity = null;
            PopUpActivity = null;
            (popupActivity as ModalPopupExtender).Hide();
            ReloadGrid();
            StartEditingTabel();
        }

        protected void ddlExtendType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i = 0;
        }

        protected void extendType_Load(object sender, EventArgs e)
        {
            ASPxComboBox co = (ASPxComboBox)sender;


            co.DataSource = logic.AccrExtend.getExtendType();
            co.DataBind();

            if (gridGeneralInfo.IsEditing)
            {
                int idxSelected = gridGeneralInfo.EditingRowVisibleIndex;
                ListInquiry = GetSessData();
                int cntData = ListInquiry.Count;
                if (cntData == 0)
                {
                    return;
                }

                if ((idxSelected >= 0) && (idxSelected < cntData))
                {
                    AccruedExtendData detail = ListInquiry[idxSelected];
                    co.Value = detail.EXTEND_TYPE_CD;
                }
            }

        }

        //fid) prastyo 10072018
        protected bool SendEmail(string linkEmail)
        {
            String emailTemplate = String.Format("{0}\\AccruedApproval.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));

            EmailFunction mail = new EmailFunction(emailTemplate);

            string[] mailTo = null;
            if (AccruedData.STATUS_CD != 105)
            {
                var recipient = logic.Role.getUserRoleHeader(AccruedData.PIC_NEXT);
                if (recipient == null)
                    return false;

                mailTo = new string[] { recipient.EMAIL };
            }
            else
            {
                var listRecipient = logic.Role.getUserWorklist(AccruedData.EXTEND_NO);
                if (listRecipient == null || listRecipient.Count == 0)
                    return false;

                mailTo = listRecipient.Select(x => x.EMAIL).ToArray();
            }
            logic.Say("SendEmail", "SendEmail Accrued Approval {0} to : {1}. [link:{2}]"
                , AccruedData.EXTEND_NO
                , string.Join(",", mailTo)
                , linkEmail);

            ErrorData err = null;
            if (mail.ComposeAccruedApproval_EMAIL(3, AccruedData.EXTEND_NO, mailTo, linkEmail, ref err))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            logic.Say("btSubmit_Click", "Submit");
            clearScreenMessage();

            try
            {
                bool valid = true;

                if (AccruedData.STATUS_CD != 0 && AccruedData.STATUS_CD != 511)
                {
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00218ERR", "Draft or Reject by Acc", "submitted");
                    valid = false;
                }
                if (!AttachmentTable.Attachments.Any(x => x.FileName.isNotEmpty()))
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Attachment");
                    valid = false;
                }

                valid &= ValidateBalance();

                if (valid)
                {
                    logic.Say("btSubmit_Click", "K2StartWorklist K2 {0}", AccruedData.EXTEND_NO);

                    K2DataField k2Data = new K2DataField();
                    k2Data.Command = resx.K2ProcID("Form_Registration_Submit_New");
                    k2Data.folio = AccruedData.EXTEND_NO;
                    k2Data.Reff_No = AccruedData.REFF_NO;
                    k2Data.ApprovalLevel = "1";
                    k2Data.ApproverClass = "1";
                    k2Data.ApproverCount = "1";
                    k2Data.CurrentDivCD = UserData.DIV_CD.ToString();
                    k2Data.CurrentPIC = UserData.USERNAME;
                    k2Data.EmailAddress = UserData.EMAIL;
                    k2Data.Entertain = "0";
                    k2Data.K2Host = "";
                    k2Data.LimitClass = "0";
                    k2Data.ModuleCD = "5";
                    k2Data.NextClass = "0";
                    k2Data.PIC = UserData.DIV_CD.ToString();
                    k2Data.RegisterDt = string.Format("{0:G}", DateTime.Now);
                    k2Data.RejectedFinance = "0";
                    k2Data.StatusCD = AccruedData.STATUS_CD.ToString();
                    k2Data.StepCD = "";
                    k2Data.VendorCD = "0";
                    k2Data.TotalAmount = ListInquiry.Sum(x => x.TOT_AMOUNT).ToString();

                    if (AccruedData.STATUS_CD != 0)
                    {
                        k2Data.isResubmitting = true;
                    }

                    bool result = logic.WorkFlow.K2StartWorklist(k2Data, UserData);

                    if (result)
                    {
                        //fid) prastyo 10072018
                        int newStatus = 0;
                        if (WaitForK2UpdateStatus(out newStatus, 200))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Submit");

                            ReloadGrid();
                            resetWorklist();

                            logic.AccrForm.SetCancelFlag(AccruedData.EXTEND_NO, UserData, 0);

                            string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                            if (!Common.AppSetting.NoticeMailUsePort)
                                port = "";
                            string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?accrno={4}&mode=view",
                                                Request.Url.Scheme,
                                                Request.ServerVariables["SERVER_NAME"],
                                                port,
                                                "/80Accrued/AccrForm.aspx",
                                                AccruedData.EXTEND_NO));

                            if (SendEmail(linkEmail))
                            {
                                logic.Say("SendEmail", "SendEmail Accrued {0} finished successfully", AccruedData.EXTEND_NO);
                            }
                            else
                            {
                                logic.Say("SendEmail", "SendEmail Accrued {0} finished with failed", AccruedData.EXTEND_NO);
                            }
                        }
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submit");
                    }

                    SetDataList();
                    ResetHeader();
                    PrepareDataHeader();
                    SetButtonAvailability();
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Submit");
                Handle(ex);
            }

            postScreenMessage();
            ReloadOpener();
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            string loc = "btnApprove_Click";
            logic.Say(loc, "Approve Extend {0} by {1} on status {2}", AccruedData.EXTEND_NO, UserData.USERNAME, AccruedData.STATUS_CD);

            if (dtExtendDt.Text.isEmpty())
            {
                PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Extend Date");
                return;
            }

            if (!ValidateBalance())
            {
                return;
            }

            AccruedData.EXTEND_DT = dtExtendDt.Date;

            // do validation here
            bool valid = true;

            if (valid)
            {
                string command = resx.K2ProcID("Form_Registration_Approval");
                // Call K2
                bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.EXTEND_NO);
                if (success)
                {
                    int newStatus = 0;
                    if (WaitForK2UpdateStatus(out newStatus))
                    {
                        logic.AccrExtend.UpdateExtendDate(ListInquiry, AccruedData.EXTEND_DT, UserData);

                        // add by FID.Ridwan 06072018
                        logic.AccrExtend.UpdateExtend(AccruedData.EXTEND_NO, ListInquiry, AccruedData.EXTEND_DT.str());

                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Approve");
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_WARNING, "MSTD00082WRN");
                    }

                    ResetHeader();
                    PrepareDataHeader();
                    SetButtonAvailability();
                }
                else
                {
                    logic.Say(loc, "Process approval to K2 failed");
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Approve");
                }
            }
            ReloadOpener();
        }

		// fid.pras 13072018 popup revise begin
        protected void btnRevise_Click(object sender, EventArgs e)
        {
            string extendNo = AccruedData.EXTEND_NO;
            if (!string.IsNullOrEmpty(extendNo))
            {
                // do validation here
                bool valid = true;
                if (valid)
                {
                    txtReviseExtendNo.Text = extendNo;
                    ddlReviseCategory.Items.Clear();
                    ddlReviseCategory.Items.Add("Reject");
                    ddlReviseCategory.SelectedIndex = 0;
                    txtReviseComment.Text = "";
                    popdlgRevise.Show();
                }
                else
                {
                    Nag("MSTD00230ERR", extendNo);
                }
            }
        //    string loc = "btnRevise_Click";
        //    logic.Say(loc, "Revise Extend {0} by {1} on status {2}", AccruedData.EXTEND_NO, UserData.USERNAME, AccruedData.STATUS_CD);

        //    // do validation here
        //    bool valid = true;

        //    if (valid)
        //    {
        //        string command = resx.K2ProcID("Form_Registration_Rejected");
        //        // Call K2
        //        bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.EXTEND_NO);
        //        if (success)
        //        {
        //            int newStatus = 0;
        //            if (WaitForK2UpdateStatus(out newStatus))
        //            {
        //                Nag("MSTD00060INF", "Revise");
        //            }
        //            else
        //            {
        //                Nag("MSTD00082WRN");
        //            }

        //            ResetHeader();
        //            PrepareDataHeader();
        //            SetButtonAvailability();
        //        }
        //        else
        //        {
        //            logic.Say(loc, "Process Revise to K2 failed");
        //            Nag("MSTD00062ERR", "Revise");
        //        }
        //    }
        }

        protected void btnReviseCancel_Click(object sender, EventArgs e)
        {
            popdlgRevise.Hide();
        }
        protected void evt_btnReviseProceed_Click(object sender, EventArgs e)
        {
            popdlgRevise.Hide();
            string loc = "evt_btnReviseProceed_Click";
            logic.Say(loc, "Revise Shifting {0} by {1} on status {2}", AccruedData.EXTEND_NO, UserData.USERNAME, AccruedData.STATUS_CD);

            bool success = postNotice();
            if (success)
            {
                doReject(loc);
            }
            ReloadOpener();
        }

        protected bool postNotice()
        {
            bool result = true;
            bool hasWorlist = hasWorklist(AccruedData.EXTEND_NO);
            clearScreenMessage();

            if (BaseNoteRecipientDropDown != null)
            {
                String recipient = Convert.ToString(UserData.USERNAME);
                string loggedUserRole = UserData.Roles.FirstOrDefault();
                if (loggedUserRole.Equals(USER_ADMIN_ROLE) && string.IsNullOrEmpty(recipient))
                {
                    PostNow(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Notice recipient");
                    return false;
                }
            }

            try
            {
                string strNotice = txtReviseComment.Text;
                if (!String.IsNullOrEmpty(strNotice))
                {
                    string lowerScreenType = BaseScreenType.ToLower();
                    String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR")));
                    String noticeLink = String.Format("{0}://{1}:{2}{3}?shiftingno={4}&mode=view",
                        Request.Url.Scheme,
                        Request.ServerVariables["SERVER_NAME"],
                        HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                        Request.ServerVariables["URL"],
                        AccruedData.EXTEND_NO);

                    EmailFunction mail = new EmailFunction(emailTemplate);
                    ErrorData err = new ErrorData();
                    List<RoleData> listRole = RoLo.getUserRole(UserName, ref err);

                    String userRole = "";
                    String userRoleTo = "";
                    String noticeTo = "";
                    String noticer = "";

                    if (listRole.Any())
                    {
                        userRole = listRole.FirstOrDefault().ROLE_NAME;
                    }

                    List<string> lstUser = null;
                    if (BaseNoteRecipientDropDown.Value != null)
                    {
                        noticeTo = UserData.USERNAME;

                        ErrorData _Err = new ErrorData(0, "", "");
                        List<RoleData> roleTos = RoLo.getUserRole(noticeTo, ref _Err);
                        userRoleTo = (roleTos != null && roleTos.Count > 0) ? roleTos[0].ROLE_ID : "?";
                        lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
                    }
                    else
                    {
                        noticeTo = UserName;
                        userRoleTo = "";

                        lstUser = logic.Notice.listNoticeRecipient(AccruedData.REFF_NO, "", UserName, noticeTo);
                    }

                    if (lstUser == null)
                    {
                        lstUser = new List<string>();
                    }
                    NoticeLogic.NoticeToFirstInList(noticeTo, lstUser);

                    logic.Notice.InsertNotice(AccruedData.REFF_NO.str(),
                                            AccruedData.CREATED_DT.Year.str(),
                                            UserName,
                                            userRole,
                                            noticeTo,
                                            userRoleTo,
                                            lstUser,
                                            strNotice,
                                            true, hasWorlist);

                    fetchNote();
                    List<FormNote> noticeList = BaseData.Notes;
                    BaseNoteRepeater.DataSource = noticeList;
                    BaseNoteRepeater.DataBind();

                    if (noticeList.Count > 0)
                    {
                        var q = noticeList.Where(a => a.Date != null);
                        if (q.Any())
                        {
                            noticer = q.Last().SenderName;
                        }
                        else
                        {
                            noticer = UserName;
                        }
                    }
                    else
                    {
                        noticer = UserName;
                    }

                    int cntSentMail = 0;
                    if (UserData.USERNAME != null)
                    {

                        if (mail.ComposeApprovalNotice_EMAIL(AccruedData.REFF_NO,
                                                             AccruedData.CREATED_DT.Year.str(),
                                                             strNotice, noticeLink, noticeTo,
                                                             UserData, noticer,
                                                             AccruedData.DIVISION_ID,
                                                             BaseScreenType, "Notice", ref err))
                        {
                            PostLater(ScreenMessage.STATUS_INFO, "MSTD00024INF");
                            cntSentMail++;
                        }

                        if (err.ErrID == 1)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, err.ErrMsgID, err.ErrMsg);
                            result = false;
                        }
                        else if (err.ErrID == 2)
                        {
                            PostLater(ScreenMessage.STATUS_WARNING, "MSTD00002ERR", err.ErrMsg);
                            result = false;
                        }
                    }
                }
                else
                {
                    PostLater(ScreenMessage.STATUS_WARNING, "MSTD00017WRN", "Notice");
                }
            }
            catch (Exception ex)
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                Handle(ex);
                result = false;
            }
            BaseNoteTextBox.Text = "";

            BaseNoteCommentContainer.Attributes.Remove("style");
            BaseNoteCommentContainer.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");

            postScreenMessage();
            return result;
        }

        protected void doReject(string loc)
        {
            // do validation here
            bool valid = true;

            if (valid)
            {
                string command = resx.K2ProcID("Form_Registration_Rejected");

                // Call K2
                bool success = logic.PVRVWorkflow.Go(AccruedData.REFF_NO, command, UserData, "N/A", false, AccruedData.EXTEND_NO);
                if (success)
                {
                    int newStatus = 0;
                    if (WaitForK2UpdateStatus(out newStatus, 200))
                    {
                        ResetHeader();
                        PrepareDataHeader();
                        resetWorklist();

                        string port = ":" + HttpContext.Current.Request.ServerVariables["SERVER_PORT"];
                        if (!Common.AppSetting.NoticeMailUsePort)
                            port = "";
                        string linkEmail = ResolveClientUrl(string.Format("{0}://{1}{2}{3}?shiftingno={4}&mode=view",
                                            Request.Url.Scheme,
                                            Request.ServerVariables["SERVER_NAME"],
                                            port,
                                            "/80Accrued/AccrShiftingForm.aspx",
                                            AccruedData.EXTEND_NO));

                        if (SendEmail(linkEmail))
                        {
                            logic.Say("SendEmail", "SendEmail Shifting {0} finished successfully", AccruedData.EXTEND_NO);
                        }
                        else
                        {
                            logic.Say("SendEmail", "SendEmail Shifting {0} finished with failed", AccruedData.EXTEND_NO);
                        }

                        btnApprove.Visible = false;
                        btnRevise.Visible = false;

                        PostLater(ScreenMessage.STATUS_INFO, "MSTD00060INF", "Revise");
                    }
                    else
                    {
                        PostLater(ScreenMessage.STATUS_ERROR, "MSTD00082WRN");
                    }
                }
                else
                {
                    logic.Say(loc, "Process approval to K2 failed");
                    PostLater(ScreenMessage.STATUS_ERROR, "MSTD00062ERR", "Revise");
                }
            }

            postScreenMessage();
        }
		// fid.pras 13072018 popup revise end

        private bool WaitForK2UpdateStatus(out int newStatus, int maxLoop = 200)
        {
            Ticker.In("WaitForK2UpdateStatus");
            int oldStatus = AccruedData.STATUS_CD;
            int i = 0;

            do
            {
                System.Threading.Thread.Sleep(100 + ((i * 20) % 250));
                newStatus = logic.AccrExtend.GetLatestStatus(AccruedData.EXTEND_NO);
                i++;
            } while (oldStatus == newStatus && i < maxLoop);

            return i < maxLoop;
        }

        private bool ValidateBalance()
        {
            bool valid = true;
            ListInquiry = GetSessData();

            // check amount
            if (ListInquiry.Any(x => x.EXTEND_TYPE_CD == 0 && x.RECLAIMABLE_AMT < x.EXTEND_AMT))
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00217ERR", "Reclaimable Amount");
                valid = false;
            }

            if (ListInquiry.Any(x => x.EXTEND_TYPE_CD == 1 && x.EXTENDABLE_AMT < x.EXTEND_AMT))
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00217ERR", "Extend Amount");
                valid = false;
            }

            var listBn = ListInquiry.Select(x => x.BOOKING_NO).ToList();
            var listBal = logic.AccrBalance.GetBalancesByBookingNo(listBn);

            // check status
            var closedBal = listBal.Where(x => (x.BOOKING_STS ?? 0) == 1).Select(x => x.BOOKING_NO).ToList();
            if (closedBal.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR"
                    , "These Booking No is Closed : " + CommonFunction.CommaJoin(closedBal));
                valid = false;
            }

            // check expired
            var expBal = listBal.Where(x => (x.EXPIRED_DT ?? DateTime.Now) < DateTime.Now).Select(x => x.BOOKING_NO).ToList();
            if (expBal.Any())
            {
                PostLater(ScreenMessage.STATUS_ERROR, "MSTD00002ERR"
                    , "These Booking No is Expired : " + CommonFunction.CommaJoin(expBal));
                valid = false;
            }

            return valid;
        }
    }
}