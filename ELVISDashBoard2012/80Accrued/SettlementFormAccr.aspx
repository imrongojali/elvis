﻿<%@ Page Title="Settlement Form Accrued Screen" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="SettlementFormAccr.aspx.cs" Inherits="ELVISDashBoard._80Accrued.SettlementFormAccr" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Import Namespace="Common.Function" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Src="../UserControl/UcDataFieldEntertainment.ascx" TagName="UcDataFieldEntertainment" TagPrefix="ucdfe" %>
<%@ Register Src="../UserControl/UcDataFieldPromotion.ascx" TagName="UcDataFieldPromotion" TagPrefix="ucdp" %>
<%@ Register Src="../UserControl/UcDataFieldDonation.ascx" TagName="UcDataFieldDonation" TagPrefix="ucdt" %>
<%@ Register Src="../UserControl/UcAttachmentType.ascx" TagName="UcAttachmentType" TagPrefix="ucatt" %>
<asp:Content ID="xx" ContentPlaceHolderID="pre" runat="server">
    <script type="text/javascript">
        function convertConfirmation() {
            var selection = confirm("Convert Settlement into PV and/or RV ?");
            if (selection) {
                $("#hidDialogConfirmation").val("y");
            } else {
                $("#hidDialogConfirmation").val("n");
            }
            loading();
            return selection;
        }

        function hidePop() {
            var pop = $find("ConfirmationDeletePopUp");
            pop.hide();
        }

        function assignPopClick() {
            if ($("#rvLink").length > 0) {
                $("#rvLink").click(function () {
                    if ($("#pvLink").length < 1) 
                        hidePop();
                });
            }

            if ($("#pvLink").length > 0) {
                $("#pvLink").click(function () {
                    if ($("#rvLink").length < 1)
                        hidePop();
                });
            }
        }

        function search() {
            var btn = $("#btnSearch");
            if (btn.length > 0) {
                btn.click();
            };
            return false;
        }

        function OnKeyDown(s, event) {
            if (event.keyCode == 13) {
                event.cancelBubble = true;
                event.returnValue = false;
                search(); 
                return false; 
            }
        }

        function OnSelectSuspense(s, e) {
            var sus = $("#ddlSuspenseNo"); 
            if (sus != null && sus.length > 0) {
                search();   
            } else {                
            }
        }

        function isNumberKey(evt) {
            var thestring = evt.inputElement.value;
            var thenum = thestring.replace(/\D/g, "");
            evt.inputElement.value = thenum;
            return true;
        }

    </script>

<style type="text/css">
   .boxDesc
   {
       border: 1px solid #afafaf;
       border-style: inset;
       background-color: #efefef;
       display: block;              
       overflow: auto;
       padding-left: 5px;
       height: 22px;
       float: left;
       vertical-align: middle;
   }

    .dxbButton {
            color: #000000;
            font: normal 12px Tahoma;
            font-weight: normal;
            font-size: 12px;
            vertical-align: middle;
            border: 1px solid #7F7F7F;
            background: none;
            background-color: rgb(224, 223, 223);
            padding: 1px;
            cursor: pointer;
        }
   
   #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 80px;
   }
   #divVendorName
   {
       width: 254px;
   }
</style>
</asp:Content>
<asp:Content ID="xPre" ContentPlaceHolderID="head" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
               <asp:PostBackTrigger ControlID="ddlSuspenseNo" />
                <asp:PostBackTrigger ControlID="txtPVYear" />
             <asp:PostBackTrigger ControlID="btnQuestionData" />
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
            <div id="divNeaderAtt1" runat="server" visible="true">
              <div class="contentsection" style="width:987px !important;">
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                <tr>
                    <td valign="baseline" style="width: 100px">
                        Suspense No
                    </td>
                    <td valign="top" style="width: 125px">
                        <dx:ASPxGridLookup runat="server" ID="ddlSuspenseNo" 
                            ClientInstanceName="ddlSuspenseNo" ClientIDMode="Static"  
                            DataSourceID="dsUnsettled"
                            KeyFieldName="REFF_NO" TextFormatString="{0}" 

                            Width="110px" CssClass="display-inline-table" 
                            SelectionMode="Single" 
                            DisplayFormatString="{0}"
                            IncrementalFilteringMode="Contains" 
                            IncrementalFilteringDelay="1000"
                            EnableClientSideAPI="true"
                            OnValueChanged="ddlSuspenseNo_ValueChanged">
                            <ClientSideEvents 
                                TextChanged="OnSelectSuspense"
                                CloseUp="OnSelectSuspense"
                                KeyDown="OnKeyDown"
                            />
                            <GridViewProperties >
                               
                                <Settings ShowFilterRow="true" ShowStatusBar="Hidden" UseFixedTableLayout="true" ShowColumnHeaders="true" />
                                <SettingsPager PageSize="15" />
                                
                            </GridViewProperties>
                            <Columns>
                                <dx:GridViewDataTextColumn Caption="Suspense Baru" FieldName="SUSPENSE_NO_NEW" Width="100px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Suspense Lama" FieldName="SUSPENSE_NO_OLD" Width="100px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption = "Activity" FieldName="ACTIVITY_DES" Width="150px" />
                                <%-- <dx:GridViewDataTextColumn Caption = "CATEGORY_CODE" FieldName="CATEGORY_CODE" Width="150px"  />--%>
                                                                    
                                <%--<dx:GridViewDataTextColumn Caption="PV No" FieldName="PV_NO" Width="85px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption = "Curr" FieldName="CURRENCY_CD" Width="50px" />
                                <dx:GridViewDataTextColumn Caption = "Amount" FieldName="TOTAL_AMOUNT" Width="100px"  >  
                                    <PropertiesTextEdit DisplayFormatString="{0:N2}"></PropertiesTextEdit>
                                    <HeaderStyle HorizontalAlign="Center" />
                                   <CellStyle HorizontalAlign="Right" >

                                   </CellStyle>
                                   
                                   
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" Width="120px" >
                                    <CellStyle CssClass="smallFont" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataDateColumn Caption="Activity Date End" FieldName="ACTIVITY_DATE_TO" Width="120px" >
                                    <HeaderStyle HorizontalAlign="Center" />
                                    <CellStyle HorizontalAlign="Center" />
                                    <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" />
                                </dx:GridViewDataDateColumn>    --%>                            
                            </Columns>
                        </dx:ASPxGridLookup>
                    </td>
                    <td valign="baseline" style="width: 100px">
                        Transaction Type
                    </td>
                    <td valign="top" >
                   
                         <table>
                                    <tr>
                                        <td>
                                            <div class="boxDesc" id="divTransactionType">
                                                <asp:Literal ID="litTransactionType" runat="server" />
                                                 <asp:Literal ID="litddlSuspenseNo" runat="server" Visible="false" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                    </td>
                </tr>
                <tr>
                    <td valign="baseline">
                        PV Year
                    </td>
                    <td valign="top">
                        <asp:TextBox ID="txtPVYear" runat="server" Width="50px" MaxLength="4" TabIndex="2"                            
                            ClientIDMode="Static" 
                            onchange="extractNumber (this, 0, false);"
							onblur = "extractNumber (this, 0, false);"
							onkeyup = "extractNumber (this, 0, false);"
							onkeypress = "return blockNonNumbers (this, event, true, false);"                            
                            onkeydown = "OnKeyDown"
                            >
                            
                            </asp:TextBox>
                    </td>

                    <td valign="baseline">
                        Vendor Code
                    </td>
                    <td valign="top">
                       <%-- <div class="boxDesc" id="divVendorCode">
                        <asp:Literal ID="litVendorCode" runat="server" />
                        </div>

                        <div class="lefty" style="width: 10px">&nbsp;</div>       
                                         
                        <div class="boxDesc" id="divVendorName">
                        <asp:Literal ID="litVendorName" runat="server" />
                        </div>--%>
                         <table>

                                    <tr>
                                        <td>
                                            <div class="boxDesc" id="divVendorCode">
                                                <asp:Literal ID="litVendorCode" runat="server" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="lefty" style="width: 10px">&nbsp;</div>
                                        </td>
                                        <td>
                                            <div class="boxDesc" id="divVendorName">
                                                <asp:Literal ID="litVendorName" runat="server" />
                                            </div>
                                        </td>
                                        <td>
                                            <div id="divQuestion" runat="server" clientidmode="Static" visible="true">


                                                <dx:ASPxButton runat="server" ID="btnQuestionData"
                                                    Text="Create Nominative List" OnClick="evt_Question_onClick" BackColor="#33CC33" Font-Bold="True" ForeColor="White">
                                                    <ClientSideEvents Click="function(s,e) {loading(); }" />
                                                </dx:ASPxButton>




                                            </div>
                                        </td>
                                    </tr>
                                </table>
                    </td>
                </tr>
                
             </table>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <div style="visibility:hidden; width:0px; height:0px;">
                            <asp:Button ID="btnSearch" runat="server" Text="Search" ClientIDMode="Static"
                                onclick="btnSearch_Click" OnClientClick="loading()"  />
                        </div>
                    </div>
                </div>
            </div>

                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Settlement Form Accrued" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hidDialogConfirmation" ClientIDMode="Static" />


    <div id="divNeaderAtt2" runat="server" visible="true">
    <div style="clear:both"></div>
    <div style="clear:both"></div>

    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
          
        </Triggers>
        <ContentTemplate>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" OnClientClick="loading()" />
                </div>
            </div>
            <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="100%">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td>Settlement Amount:</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Literal ID="litSettlementAmount" runat="server"></asp:Literal>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>    
                <tr>                       
                    <td colspan="3">
                        <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                            ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                            KeyFieldName="SEQ_NO" Styles-AlternatingRow-CssClass="even">
                            <Columns>
                                <dx:GridViewBandColumn Caption="Cost Center" VisibleIndex="1">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Code" VisibleIndex="0"
                                            Width="100px" FieldName="COST_CENTER_CD">
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Name" VisibleIndex="1" Width="200px" 
                                            FieldName="COST_CENTER_NAME">
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>

                                <dx:GridViewDataTextColumn Caption="Description" VisibleIndex="2" Width="246px" 
                                    FieldName="DESCRIPTION">
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Curr" FieldName="CURRENCY"
                                    VisibleIndex="3" Width="50px">
                                    <CellStyle HorizontalAlign="Left">
                                    </CellStyle>
                                    <DataItemTemplate>
                                        <asp:Literal runat="server" ID="litGridCurrency" Text='<%# Bind("CURRENCY")%>'></asp:Literal>
                                    </DataItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="Amount" VisibleIndex="4">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Suspense" FieldName="AMOUNT" VisibleIndex="0"
                                            Width="125px" CellStyle-HorizontalAlign="Right">
                                            <DataItemTemplate>
                                                <asp:Literal runat="server" ID="litGridAmount" Text='<%# CommonFunction.Eval_Curr(Eval("CURRENCY"), Eval("AMOUNT")) %>'>
                                                </asp:Literal>
                                                <asp:HiddenField runat="server" ID="hidGridAmount" Value='<%# Bind("AMOUNT")%>'>
                                                </asp:HiddenField>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Spent" FieldName="SPENT_AMOUNT" 
                                            VisibleIndex="1" Width="125px" CellStyle-HorizontalAlign="Right">
                                            <EditItemTemplate>
                                                
                                            </EditItemTemplate>
                                            <DataItemTemplate>
                                               <%-- <dx:ASPxTextBox runat="server" ID="txtGridSpentAmount" Width="110px" Text='<%# Bind("SPENT_AMOUNT") %>'
                                                    AutoPostBack="true" 
                                                    HorizontalAlign="Right"
                                                    OnTextChanged="txtGridSpentAmount_Changed">                                                    
                                                    <MaskSettings  Mask="<0..9999999999999g>.<00..99>" IncludeLiterals="DecimalSymbol"/>
                                                </dx:ASPxTextBox>--%>
                                                 <dx:ASPxTextBox runat="server" ID="txtGridSpentAmount" Width="110px" Text='<%# Bind("SPENT_AMOUNT")%>'
                                                        AutoPostBack="true"
                                                        HorizontalAlign="Right"
                                                        OnTextChanged="txtGridSpentAmount_Changed">
                                                        <MaskSettings Mask="<0..9999999999999g>.<00..99>" IncludeLiterals="DecimalSymbol" />
                                                       <%-- <MaskSettings Mask="<0..9999999999999g>.<00..99>" IncludeLiterals="DecimalSymbol" />--%>
                                                    </dx:ASPxTextBox>
                                                <asp:Literal runat="server" ID="litGridSpentAmount" Visible="false" 
                                                    Text='<%# CommonFunction.Eval_Curr(Eval("CURRENCY"), Eval("SPENT_AMOUNT")) %>'>
                                                </asp:Literal>
                                                <asp:HiddenField runat="server" ID="hidSpentAmount" Value='<%# Bind("SPENT_AMOUNT")%>'></asp:HiddenField>
                                                <asp:HiddenField runat="server" ID="hidSeqNo" Value='<%# Bind("SEQ_NO")%>'></asp:HiddenField>
                                                <asp:HiddenField runat="server" ID="hidPvSeqNo" Value='<%# Bind("PV_SEQ_NO")%>'></asp:HiddenField>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Settlement" VisibleIndex="2" Width="125px" 
                                            FieldName="SETTLEMENT_AMOUNT" CellStyle-HorizontalAlign="Right">
                                            <DataItemTemplate>
                                                <asp:HiddenField runat="server" ID="hidSettlementAmount" 
                                                    Value='<%# Bind("SETTLEMENT_AMOUNT")%>'></asp:HiddenField>
                                                <asp:Literal runat="server" ID="litSettlementAmount" 
                                                    Text='<%# CommonFunction.Eval_Curr(Eval("CURRENCY"), Eval("SETTLEMENT_AMOUNT")) %>'></asp:Literal>
                                            </DataItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" />
                            <SettingsLoadingPanel ImagePosition="Top" />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                                </Header>
                                <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                                </Cell>
                            </Styles>
                            <SettingsPager AlwaysShowPager="False" Mode="ShowAllRecords"></SettingsPager>
                            <Settings ShowStatusBar="Hidden" />
                            <SettingsPager Visible="false" />
                        </dx:ASPxGridView>
                        <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                            EntityTypeName="" Select="new (SEQ_NO, PV_SEQ_NO, PV_SUS_NO, PV_YEAR, COST_CENTER_NAME, CURRENCY, SETTLEMENT_AMOUNT, SPENT_AMOUNT, COST_CENTER_CD, DESCRIPTION, CURRENCY_CD, AMOUNT)"
                            OnSelecting="LinqDataSource1_Selecting" TableName="vw_Settlement_Detail" >
                        </asp:LinqDataSource>
                        <asp:LinqDataSource ID="dsUnsettled" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities" 
                        OnSelecting="dsUnsettled_Selecting" />

                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnButtons" runat="server">
        <ContentTemplate>
            <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
                <tr>
                    <td valign="top" style="width: 130px" align="left">
                        <asp:Button runat="server" ID="btnConvertPVRV" Text="Convert PV/RV" CssClass="xlongButton"
                            ClientIDMode="Static" 
                            SkinID="xlongButton" OnClick="btnConvertPVRV_Click" /></td>
                    <td valign="top" style="width: 470px" align="left" rowspan="2">
 
                    </td>
                    <td valign="top" style="width: 270px" align="right">
                        <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click" OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click" OnClientClick="loading()" />
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="3" valign="top" style="width: 270px" align="right">
                        <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                    </td>
                </tr>
                <tr>
                        <td valign="top" style="width: 130px" align="right">
                            <div id="divAttachmentType" runat="server" visible="false">
                                <asp:UpdatePanel runat="server" ID="pnupdateAttachmentGrid">
                                    <ContentTemplate>

                                        <ucatt:UcAttachmentType runat="server" ID="UcAttachmentType1" />


                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
    <%//********************region Success Confirmation**********************//%>
    <asp:UpdatePanel ID="upnlSuccessConfirmation" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnConvertPVRV" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnSuccessConfirmation" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnSuccessConfirmation"
                ClientIDMode="Static"
                PopupControlID="ConfirmationSuccessPanel" CancelControlID="BtnOkSuccessConfirmation" 
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationSuccessPanel" runat="server" CssClass="informationPopUpX">
                    <center>
                        <b>Congratulations!</b>
                        <br />
                        <asp:Label ID="lblSettlementNo" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Label ID="lblPVRVNo" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkSuccessConfirmation" Text="OK" runat="server" Width="60px" style="display:none;" />
                        <br />
                        <span style="font-style: italic; font-weight: bold; font-size:larger;">Please click the link above</span>
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%//*********************end region***********************//%>

  <dx:ASPxPanel runat="server" ID="PanelEntertainment" ClientInstanceName="PanelEntertainment"
        ClientVisible="false" Width="100%">
        <PanelCollection>
            <dx:PanelContent>

                <ucdfe:UcDataFieldEntertainment runat="server" ID="DataFieldEntertainment" />

            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>

    <dx:ASPxPanel runat="server" ID="PanelPromotion" ClientInstanceName="PanelPromotion"
        ClientVisible="false" Width="100%">
        <PanelCollection>
            <dx:PanelContent>
                <ucdp:UcDataFieldPromotion runat="server" ID="DataFieldPromotion" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>

    <dx:ASPxPanel runat="server" ID="PanelDonation" ClientInstanceName="PanelDonation"
        ClientVisible="false" Width="100%">
        <PanelCollection>
            <dx:PanelContent>
                <ucdt:UcDataFieldDonation runat="server" ID="DataFieldDonation" />
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
</asp:Content>


