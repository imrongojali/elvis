﻿<%@ Page Title="Accrued Form" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="True" CodeBehind="AccrForm.aspx.cs" Inherits="ELVISDashBoard._80Accrued.AccrForm" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<%@ Register Src="../UserControl/FormHint.ascx" TagName="FormHint" TagPrefix="fh" %>
<%@ Import Namespace="Common.Function" %>
<asp:Content ID="preX" ContentPlaceHolderID="pre" runat="server">
    <link href="../approval.css" rel="stylesheet" type="text/css" />
    <script src="../approval.js" type="text/javascript"></script>

    <script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>
    <script type="text/javascript">
      
        function AccrCurrencyCode_TextChanged(s, e) {
            var t = s.GetValue();
            gridAccrDetail.PerformCallback('p:update:CurrencyCode:' + t);
        }

        function isNumberKey(evt) {
            var thestring = evt.inputElement.value;
            var thenum = thestring.replace(/\D/g, "");
            evt.inputElement.value = thenum;
            return true;
        }

       

        function AccrPVType_TextChanged(s, e) {
            
            console.log(s);
            console.log(e);
            var v = s.GetValue();
            var t = s.GetText();
            gridAccrDetail.PerformCallback('p:update:PVType:' + v + '|' + t);
        }

        function WbsNoChanged(s, e) {
            var t = s.GetValue();
            gridAccrDetail.PerformCallback('p:update:WbsNumber:' + t);
        }

        function SuspenseNoChanged(s, e) {
            var t = s.GetValue();
            gridAccrDetail.PerformCallback('p:update:SuspenseNo:' + t);
        }

        function IsEmptyReason(s, e) {
            if ($("#txtReviseComment").val() == "") {
                alert("Please input reason first!");
                return false;
            }
            $("#popdlgRevise").hide();
            loading();
            return true;
        }

        $(document).ready(function () {

        });

        $(document).keydown(BlockNonIntendedBackspace);

        function hidePop() {
            var pop = $find("popUpRevise");
            pop.hide();
        }
    </script>
</asp:Content>
<%@ Register Src="../UserControl/UcQuestionaire.ascx" TagName="UcQuestionaire" TagPrefix="ucq" %>
<asp:Content ID="sectionHeader" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .otvlabel {
            float: left;
            width: 105px;
            font-size: 10px;
            display: block;
        }

        .col1 {
            width: 180px;
            float: left;
            display: block;
            clear: none;
        }

        .col2 {
            width: 160px;
            float: left;
            display: block;
            clear: none;
        }

        .btRight {
            float: right;
            width: 90%;
            text-align: right;
            vertical-align: middle;
            display: inline;
        }

        .dxbButton {
            color: #000000;
            font: normal 12px Tahoma;
            font-weight: normal;
            font-size: 12px;
            vertical-align: middle;
            border: 1px solid #7F7F7F;
            background: none;
            background-color: rgb(224, 223, 223);
            padding: 1px;
            cursor: pointer;
        }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageMode" />
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Accrued Form" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdLastFocusedID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdSelectedVendorCode" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdGridFocusedColumn" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdFinanceAccessFlag" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidReloadOpener" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidHasWorklist" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidOneTimeVendorValid" ClientIDMode="Static" />
    <br />
    <br />
    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false"
                OnLoad="evt_messageControl_onLoad" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <dx:ASPxPanel runat="server" ID="PanelMasterHeader" ClientInstanceName="PanelMasterHeader"
        ClientVisible="true" Width="100%">
        <PanelCollection>
            <dx:PanelContent>
                <asp:UpdatePanel runat="server" ID="pnupdateContentSection">
                    <ContentTemplate>
                        <div class="contentsection">
                            <dx:ASPxPageControl ID="PVFormListTabs" runat="server" ActiveTabIndex="0" EnableHierarchyRecreation="True"
                                Height="150px" Width="970px" BackColor="#EFEFFF">
                                <TabPages>
                                    <dx:TabPage Text="General Data" Name="GeneralData">
                                        <ContentCollection>
                                            <dx:ContentControl ID="GeneralDataCC" runat="server">
                                                <div class="contentsectionUnderTab">
                                                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 937px;"
                                                        class="control-layout-table">
                                                        <tr>
                                                            <td valign="middle" style="width: 75px" class="td-layout-item">Issuing Division <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxIssueDiv" Width="100px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">Exchange Rate
                                                            </td>
                                                            <td valign="middle" style="width: 150px" class="td-layout-item" colspan="2">PIC
                                                            </td>
                                                            <td valign="middle" style="width: 100px" class="td-layout-item">Total Amount <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 135px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxTotAmt" Width="130px" CssClass="display-inline-table" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle" style="width: 75px" class="td-layout-item">Accrued No <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxAccruedNo" Width="100px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td align="left" valign="middle" style="width: 100px" class="td-layout-item">USD to IDR <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 175px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxUsdIdr" Width="160px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td valign="middle" style="width: 80px" class="td-layout-item">Current <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 175px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxPicCur" Width="160px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td valign="middle" style="width: 100px" class="td-layout-item">Submission Status <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 135px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxSubmSts" Width="130px" CssClass="display-inline-table" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="middle" style="width: 75px" class="td-layout-item">Created Date <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 90px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxCreateDt" Width="100px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td align="left" valign="middle" style="width: 100px" class="td-layout-item">JPY to IDR <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 175px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxJpyIdr" Width="160px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td valign="middle" style="width: 80px" class="td-layout-item">Next <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 155px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxPicNext" Width="160px" CssClass="display-inline-table" />
                                                            </td>
                                                            <td valign="middle" style="width: 100px" class="td-layout-item">Workflow Status <span class="right-bold">:</span>
                                                            </td>
                                                            <td valign="middle" style="width: 135px" class="td-layout-value value-pv">
                                                                <dx:ASPxLabel runat="server" ID="tboxWFSts" Width="130px" CssClass="display-inline-table" />
                                                            </td>
                                                        </tr>


                                                    </table>
                                                    <table style="width: 20%; display: inline-table; float: right;">
                                                        <tr>
                                                            <td>
                                                                <table style="float: right">
                                                                    <tr>
                                                                        <td></td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </dx:ContentControl>
                                        </ContentCollection>
                                    </dx:TabPage>
                                </TabPages>
                            </dx:ASPxPageControl>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="pnupdateAmountList" runat="server">
                    <ContentTemplate>
                        <table>
                            <tr>
                                <td>
                                    <div class="totalAmount">
                                        <asp:Literal runat="server" ID="ltTotalAmount" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div id="ButtonRight" runat="server" style="float: right; text-align: right; width: 100%; margin-right: 0px;">
                    <asp:UpdatePanel runat="server" ID="pnlButtonRight">
                        <Triggers>
                        </Triggers>
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td style="float: right; text-align: right; width: 350px">
                                        <asp:Button ID="btEdit" runat="server" Text="Edit" OnClick="btEdit_Click" ClientIDMode="Static"
                                            OnClientClick="loading();" />
                                        <asp:Button ID="btCheckBudget" runat="server" Text="Check Budget" CssClass="xlongButton"
                                            SkinID="xlongButton" OnClick="evt_btCheckBudget_onClick" OnClientClick="loading()" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <div id="UploadDetailDiv" runat="server">
                    <asp:UpdatePanel runat="server" ID="pnupdateUpload">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnUploadTemplate" />
                            <asp:PostBackTrigger ControlID="btExport" />
                        </Triggers>
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td>
                                        <dx:ASPxHyperLink runat="server" ID="lnkDownloadTemplate" Visible="false" Text="Download Template"
                                            NavigateUrl="~/ExcelTemplate/Upload_Accrued_Template.xls" />
                                        <asp:Label runat="server" ID="ltDownloadTemplateTextSeparator" Visible="false">&nbsp; | &nbsp;</asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 550px">
                                        <asp:FileUpload ID="fuInvoiceList" runat="server" ClientIDMode="Static" />
                                        &nbsp;<asp:Button runat="server" ID="btnUploadTemplate" Text="Upload" OnClick="btnUploadTemplate_Click"
                                            OnClientClick="BeforeUpload_Click() ? loading() : return false;" />
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel runat="server" ID="pnupdateGrid">
                    <ContentTemplate>
                        <dx:ASPxGridView runat="server" ID="gridAccrDetail" Width="990px" ClientInstanceName="gridAccrDetail"
                            ClientIDMode="Static" KeyFieldName="DisplaySequenceNumber" AutoGenerateColumns="False"
                            OnRowUpdating="evt_gridAccrDetail_onRowUpdating"
                            OnSelectionChanged="evt_gridAccrDetail_onSelectionChanged"
                            OnCellEditorInitialize="evt_gridAccrDetail_onCellEditorInitialize"
                            OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                            OnHtmlDataCellPrepared="gridGeneralInfo_HtmlDataCellPrepared"
                            OnCustomCallback="evt_gridAccrDetail_CustomCallback">
                            <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="True"
                                AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                            <SettingsEditing Mode="Inline" />
                            <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                VerticalScrollableHeight="225" />
                            <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                            <Styles>
                                <AlternatingRow Enabled="True" CssClass="value-pv" />
                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                </Header>
                            </Styles>
                            <Columns>
                                <dx:GridViewDataColumn Width="30px" FieldName="DeletionControl">
                                    <CellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />
                                    <EditCellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />
                                    <HeaderTemplate>
                                        <common:KeyImageButton ID="imgDeleteAllRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png"
                                            OnClick="evt_imgDeleteAllRow_clicked" CssClass="pointed" />
                                    </HeaderTemplate>
                                    <DataItemTemplate>
                                        <common:KeyImageButton ID="imgDeleteRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png"
                                            Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgDeleteRow_clicked"
                                            CssClass="pointed" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <common:KeyImageButton ID="imgAddRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png"
                                            Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgAddRow_clicked" CssClass="pointed" />
                                    </EditItemTemplate>
                                </dx:GridViewDataColumn>
                                <dx:GridViewDataTextColumn Width="30px" FieldName="DisplaySequenceNumber" Caption="No">
                                    <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <DataItemTemplate>
                                        <dx:ASPxLabel ID="lblNo" runat="server" Text="<%# Container.VisibleIndex + 1 %>" />
                                        <dx:ASPxLabel ID="lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" Visible="false" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxLabel ID="edit_lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" Visible="false" />
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Booking No" FieldName="BookingNo" Width="120px"
                                    ReadOnly="true">
                                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <DataItemTemplate>
                                        <dx:ASPxLabel ID="data_lblBookingNo" runat="server" Text="<%# Bind('BookingNo') %>" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxLabel ID="edit_lblBookingNo" runat="server" Text="<%# Bind('BookingNo') %>" />
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="Budget">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="WBS No." Width="180px" FieldName="WbsNumber">
                                            <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="data_lblBudgetNo" runat="server" Text="<%# Bind('WbsNumber') %>" />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxGridLookup runat="server" ID="lkpgridBudgetNo" ClientInstanceName="lkpgridBudgetNo"
                                                    ClientIDMode="Static" SelectionMode="Single" KeyFieldName="WbsNumber" TextFormatString="{0}"
                                                    AutoPostBack="false" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains"
                                                    OnLoad="LookupBudgetNo_Load">
                                                    <ClientSideEvents TextChanged="WbsNoChanged" />
                                                    <GridViewProperties EnableCallBacks="false">
                                                        <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                        <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                                        <SettingsPager PageSize="7" />
                                                    </GridViewProperties>
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataTextColumn Caption="WBS Number" FieldName="WbsNumber" Width="200px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                </dx:ASPxGridLookup>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="WBS Description" Width="250px" FieldName="WbsDesc">
                                            <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="data_lblBudgetDesc" runat="server" Text="<%# Bind('WbsDesc') %>" />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxLabel ID="edit_lblBudgetDesc" runat="server" Text="<%# Bind('WbsDesc') %>" />
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Suspense No" Width="100px" FieldName="SuspenseNo">
                                            <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="data_lblSuspenseNo" runat="server" Text="<%# Bind('SuspenseNo') %>" />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxGridLookup runat="server" ID="lkpgridSuspenseNo" ClientInstanceName="lkpgridSuspenseNo"
                                                    ClientIDMode="Static" SelectionMode="Single" KeyFieldName="ReffNo" TextFormatString="{0}"
                                                    AutoPostBack="false" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains"
                                                    OnLoad="lkpgridSuspenseNo_Load">
                                                    <ClientSideEvents TextChanged="SuspenseNoChanged" />
                                                    <GridViewProperties EnableCallBacks="false">
                                                        <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                        <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" />
                                                        <SettingsPager PageSize="7" />
                                                    </GridViewProperties>
                                                    <Columns>
                                                        <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                        <dx:GridViewDataTextColumn Caption="Suspense No" FieldName="PVNo" Width="85px">
                                                            <Settings AutoFilterCondition="Contains" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="TotalAmount" Width="100px">
                                                            <PropertiesTextEdit DisplayFormatString="{0:N2}"></PropertiesTextEdit>
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Right"></CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VendorName" Width="120px">
                                                            <CellStyle CssClass="smallFont" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataDateColumn Caption="Activity Date End" FieldName="ActivityDateTo" Width="120px">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <CellStyle HorizontalAlign="Center" />
                                                            <PropertiesDateEdit DisplayFormatString="dd MMM yyyy" />
                                                        </dx:GridViewDataDateColumn>
                                                    </Columns>
                                                </dx:ASPxGridLookup>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewDataTextColumn Caption="Doc Type" Width="90px" FieldName="PVType">
                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Left">
                                    </EditCellStyle>
                                    <DataItemTemplate>
                                        <asp:Literal ID="litPVType" runat="server" Text="<%# Bind('PVType') %>" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxGridLookup runat="server" ID="ddlDocType" ClientInstanceName="ddlDocType"
                                            ClientIDMode="Static" SelectionMode="Single" KeyFieldName="Code" TextFormatString="{0}"
                                            AutoPostBack="false" Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains"
                                            OnLoad="ddlDocType_Load" >
                                               <ClientSideEvents TextChanged="AccrPVType_TextChanged" />
                                            <GridViewProperties EnableCallBacks="false">
                                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                <Settings ShowFilterRow="false" ShowStatusBar="Auto" UseFixedTableLayout="true"
                                                    ShowColumnHeaders="false" />
                                                <SettingsPager PageSize="7" />

                                            </GridViewProperties>
                                            <Columns>
                                                <dx:GridViewCommandColumn ShowSelectCheckbox="True" />
                                                <dx:GridViewDataTextColumn Caption="PV Description" FieldName="Description" Width="150px">
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Action" Width="180px" FieldName="ActionControl">
                                    <CellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />
                                    <EditCellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle" />

                                    <DataItemTemplate>
                                        <asp:Literal ID="litEnomCategory" runat="server" Text="<%# Bind('CategoryName') %>" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>

                                        

                                        <asp:UpdatePanel runat="server" ID="pnlQuestionData">
                                            <Triggers>
                                                <asp:PostBackTrigger ControlID="btnQuestionData" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <dx:ASPxButton runat="server" ID="btnQuestionData"
                                                    Text="Nominative Questionaire"   OnClick="evt_Question_onClick" BackColor="#33CC33" Font-Bold="True" ForeColor="White" ClientVisible="<%# Bind('ActionControl') %>">
                                                    <%--   <ClientSideEvents Click="function(s,e) {loading(); }" />--%>
                                                </dx:ASPxButton>

                                                <asp:Literal ID="litEditEnomCategory" runat="server" Text="<%# Bind('ReferenceNoDataField') %>" Visible="<%# Bind('LabelControl') %>" />


                                            </ContentTemplate>
                                        </asp:UpdatePanel>

                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Activity" Width="250px" FieldName="ActivityDescription">
                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Left">
                                    </EditCellStyle>
                                    <DataItemTemplate>
                                        <asp:Literal ID="litActivity" runat="server" Text="<%# Bind('ActivityDescription') %>" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxTextBox runat="server" ID="lblActivity" Width="98%" MaxLength="30" Text="<%# Bind('ActivityDescription') %>" />
                                    </EditItemTemplate>
                                    <EditItemTemplate>
                                        <common:TabBluredTextBox ID="tboxActivity" runat="server" ClientInstanceName="tboxActivity" MaxLength="50"
                                            Text="<%# Bind('ActivityDescription') %>" HorizontalAlign="Left" Width="98%" AutoPostBack="true"
                                            OnTabKeypress="evt_tboxActivity_TabKeypress" MaskHintStyle-Wrap="False"
                                            OnValueChanged="evt_tboxActivity_ValueChanged"
                                            OnLoad="evt_tboxActivity_Load">
                                            <%--<ClientSideEvents GotFocus="Amount_Focused" />--%>
                                            <%--<MaskSettings Mask="<0..9999999999999g>.<00..99>" IncludeLiterals="All" />--%>
                                        </common:TabBluredTextBox>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Cost Center" Width="110px" FieldName="CostCenterCode">
                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                    <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Left">
                                    </EditCellStyle>
                                    <DataItemTemplate>
                                        <asp:Literal ID="litCostCenter" runat="server" Text="<%# Bind('CostCenterCode') %>" />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <dx:ASPxGridLookup runat="server" ID="lkpgridCostCenterCode" ClientInstanceName="lkpgridCostCenterCode"
                                            SelectionMode="Single" KeyFieldName="Code" TextFormatString="{0}" Width="98%"
                                            CssClass="display-inline-table" OnLoad="lkpgridCostCenterCode_Load" IncrementalFilteringMode="Contains"
                                            OnValueChanged="lkpgridCostCenterCode_ValueChanged">
                                            <GridViewProperties>
                                                <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true" />
                                                <Settings ShowFilterRow="true" ShowStatusBar="Hidden" UseFixedTableLayout="true" />
                                                <SettingsPager PageSize="7" />
                                            </GridViewProperties>
                                            <%--<ClientSideEvents TextChanged="Detail_TextChanged" />--%>
                                            <Columns>
                                                <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Width="100px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="400px">
                                                    <Settings AutoFilterCondition="Contains" />
                                                </dx:GridViewDataTextColumn>
                                            </Columns>
                                        </dx:ASPxGridLookup>
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="Value">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Curr." FieldName="CurrencyCode" Width="60px">
                                            <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" />
                                            <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Left">
                                            </EditCellStyle>
                                            <DataItemTemplate>
                                                <asp:Literal ID="litCurrencyCode" runat="server" Text="<%# Bind('CurrencyCode') %>" />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <dx:ASPxComboBox runat="server" Width="98%" ID="coCurrencyCode" ClientIDMode="static"
                                                    ValueType="System.String" CssClass="display-inline-table" TextField="Code" ValueField="Code"
                                                    DropDownWidth="60px" DropDownStyle="DropDown" OnLoad="coCurrencyCode_Load">
                                                    <ClientSideEvents TextChanged="AccrCurrencyCode_TextChanged" />
                                                </dx:ASPxComboBox>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="Amount" FieldName="Amount" Width="93px">
                                            <CellStyle VerticalAlign="Middle" HorizontalAlign="Right">
                                            </CellStyle>
                                            <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Right">
                                            </EditCellStyle>
                                            <PropertiesTextEdit />
                                            <DataItemTemplate>
                                                <dx:ASPxLabel ID="lblAmount" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("Amount")) %>' Width="98%" CssClass="buttonRight"
                                                    HorizontalAlign="Right" />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <common:TabBluredTextBox ID="tboxAmount" runat="server" ClientInstanceName="tboxAmount"
                                                    Text="<%# Bind('Amount') %>" HorizontalAlign="Right" Width="88px" AutoPostBack="true"
                                                    OnTabKeypress="evt_tboxAmount_TabKeypress" MaskHintStyle-Wrap="False" CssClass="buttonRight"
                                                    ClientSideEvents-KeyUp="isNumberKey" ClientSideEvents-TextChanged="isNumberKey"
                                                    OnValueChanged="evt_tboxAmount_ValueChanged"
                                                    OnLoad="evt_tboxAmount_Load">
                                                    <%--<ClientSideEvents GotFocus="Amount_Focused" />--%>
                                                    <%--<MaskSettings Mask="<0..9999999999999g>.<00..99>" IncludeLiterals="All" />--%>
                                                </common:TabBluredTextBox>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                                <dx:GridViewDataTextColumn Caption="Amount in IDR" FieldName="AmountIdr" Width="100px">
                                    <CellStyle VerticalAlign="Middle" HorizontalAlign="Right" />
                                    <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Right">
                                    </EditCellStyle>
                                    <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                    <DataItemTemplate>
                                        <asp:Literal ID="litAmountIDR" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("AmountIdr")) %>' />
                                    </DataItemTemplate>
                                    <EditItemTemplate>
                                        <asp:Literal ID="edt_litAmountIDR" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("AmountIdr")) %>' />
                                    </EditItemTemplate>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewBandColumn Caption="SAP Balance">
                                    <Columns>
                                        <dx:GridViewDataTextColumn Caption="Available" FieldName="SapAmtAvailable" Width="100px" PropertiesTextEdit-DisplayFormatString="#,##0.00">
                                            <CellStyle VerticalAlign="Middle" HorizontalAlign="Right" />
                                            <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Right">
                                            </EditCellStyle>
                                            <PropertiesTextEdit DisplayFormatString="#,##0.00" />
                                            <DataItemTemplate>
                                                <asp:Literal ID="litSapAvailable" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SapAmtAvailable")) %>' />
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <asp:Literal ID="edt_litSapAvailable" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SapAmtAvailable")) %>' />
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                        <%--BackColor='<%# Convert.ToDouble(Eval("SapAmtRemaining")) < 0 ? "red" : "white" %>'--%>
                                        <dx:GridViewDataTextColumn Caption="Remaining" FieldName="SapAmtRemaining" Width="100px" PropertiesTextEdit-DisplayFormatString="#,##0.00">
                                            <CellStyle VerticalAlign="Middle" HorizontalAlign="Right" />
                                            <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Right">
                                            </EditCellStyle>
                                            <PropertiesTextEdit DisplayFormatString="#,##0.00">
                                            </PropertiesTextEdit>
                                            <DataItemTemplate>
                                                <span style='<%# "background-color:" + (Convert.ToDouble(Eval("SapAmtRemaining")) < 0 ? "red": "transparent") + ";" %>'>
                                                    <asp:Literal ID="litSapRemaining" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SapAmtRemaining")) %>' /></span>
                                            </DataItemTemplate>
                                            <EditItemTemplate>
                                                <span style='<%# "background-color:" + (Convert.ToDouble(Eval("SapAmtRemaining")) < 0 ? "red": "transparent") + ";" %>'>
                                                    <asp:Literal ID="edt_litSapRemaining" runat="server" Text='<%# CommonFunction.Eval_Curr("IDR", Eval("SapAmtRemaining")) %>' /></span>
                                            </EditItemTemplate>
                                        </dx:GridViewDataTextColumn>
                                    </Columns>
                                </dx:GridViewBandColumn>
                            </Columns>
                        </dx:ASPxGridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- Footer -->
                <table border="0" cellpadding="4" cellspacing="1" style="table-layout: fixed; width: 980px;">
                    <tr>
                        <td align="left" colspan="4">
                            <div class="display-inline-table">
                                <asp:UpdatePanel runat="server" ID="pnpudateFooterButtons">
                                    <ContentTemplate>
                                        <table border="0" cellpadding="4" cellspacing="1" style="table-layout: fixed; width: 993px;">
                                            <tr>
                                                <td width="350px">
                                                    <asp:Button ID="btExport" runat="server" Text="Report" OnClick="btExport_onClick" OnClientClick="loading()" />
                                                    <asp:Button ID="btSubmit" runat="server" Text="Submit" OnClick="btSubmit_Click" OnClientClick="loading()" />
                                                    <dx:ASPxButton ID="btnApprove" runat="server" Text="Approve" Width="80px"
                                                        EnableDefaultAppearance="false" EnableTheming="false" Native="true"
                                                        Font-Bold="true" BackColor="#00a800" ForeColor="WhiteSmoke" HoverStyle-BackColor="#003300"
                                                        OnClick="evt_btApprove_onClick">
                                                        <ClientSideEvents Click="function(sender, event) { loading() }" />
                                                    </dx:ASPxButton>
                                                </td>
                                                <td style="width: 100px" align="center">
                                                    <asp:Button ID="btSimulation" runat="server" Text="Simulate" OnClick="evt_btSimulation_onClick" OnClientClick="loading()" />
                                                </td>
                                                <td style="width: 350px" align="right">
                                                    <div class="display-inline-table">
                                                        <table width="100%">
                                                            <tr>
                                                                <td>
                                                                    <asp:Literal runat="server" ID="ltPostingDt">Posting Date</asp:Literal>
                                                                </td>
                                                                <td>
                                                                    <dx:ASPxDateEdit ID="dtPostingDt" runat="server" Width="100px" DisplayFormatString="dd.MM.yyyy"
                                                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true"
                                                                        AutoPostBack="false" ClientIDMode="Static">
                                                                        <ClientSideEvents DateChanged="OnDateChanged" />
                                                                    </dx:ASPxDateEdit>
                                                                </td>
                                                                <td>
                                                                    <asp:Button ID="btPostToSAP" runat="server" Text="Post To SAP" OnClick="evt_btPostToSAP_onClick" OnClientClick="loading()"
                                                                        CssClass="xlongButton" SkinID="xlongButton" />
                                                                    <asp:Button ID="btWBSUpdate" runat="server" Text="WBS Update" CssClass="xlongButton"
                                                                        SkinID="xlongButton" OnClick="evt_btWBSUpdate_onClick" OnClientClick="loading()" />
                                                                    <dx:ASPxButton ID="btReject" runat="server" Text="Revise" Width="80px"
                                                                        EnableDefaultAppearance="false" EnableTheming="false" Native="true"
                                                                        Font-Bold="true" BackColor="Red" ForeColor="WhiteSmoke" HoverStyle-BackColor="#800000"
                                                                        OnClick="evt_btRevise_onClick">
                                                                        <ClientSideEvents Click="function(sender, event) { loading() }" />
                                                                    </dx:ASPxButton>
                                                                    <asp:Button ID="btSave" runat="server" Text="Save" OnClick="btSave_Click" ClientIDMode="Static"
                                                                        OnClientClick="loading();" />
                                                                    <asp:Button ID="btCancel" runat="server" Text="Cancel" OnClientClick="loading();"
                                                                        OnClick="btCancel_Click" />
                                                                    <dx:ASPxButton ID="btnClose" runat="server" Text="Close" Width="80px"
                                                                        EnableDefaultAppearance="false" EnableTheming="false" Native="true"
                                                                        Font-Bold="true" BackColor="Blue" ForeColor="WhiteSmoke" HoverStyle-BackColor="#000033"
                                                                        OnClick="btClose_Click">
                                                                        <ClientSideEvents Click="function(sender, event) { closeWin() }" />
                                                                    </dx:ASPxButton>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>

                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 130px;">
                        <td align="left" style="width: 350px" colspan="2">
                            <asp:Panel runat="server" ID="pnAttachment" GroupingText="Attachment" Height="100px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel runat="server" ID="pnupdateAttachmentGrid">
                                                <ContentTemplate>
                                                    <div id="EntertainmentAttachDiv" runat="server" visible="false">
                                                        <a href="../Template/Excel/Template_Entertainment_Sheet.xls">Download Entertainment Template</a>
                                                    </div>
                                                    <dx:ASPxGridView runat="server" ID="gridAttachment" Width="446px" ClientInstanceName="gridAttachment"
                                                        ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
                                                        SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
                                                        SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
                                                        SettingsBehavior-AllowDragDrop="False" OnHtmlRowCreated="evt_gridAttachment_onHtmlRowCreated">
                                                        <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="True"
                                                            AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                                                        <SettingsEditing Mode="Inline" />
                                                        <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                                            VerticalScrollableHeight="263" />
                                                        <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                                                        <Styles>
                                                            <AlternatingRow Enabled="True" CssClass="value-pv" />
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn FieldName="SequenceNumber" Caption="No" Width="30px">
                                                                <CellStyle HorizontalAlign="Center" />
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel ID="lblNoAttachment" runat="server" Text="<%# Container.VisibleIndex + 1 %>" />
                                                                    <dx:ASPxLabel runat="server" ID="lblAttachmentNumber" Text="<%# Bind('SequenceNumberInString') %>" Visible="false" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Category" Width="130px">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblAttachmentCategory" Text="<%# Bind('CategoryName') %>" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="File Name" Width="200px">
                                                                <CellStyle HorizontalAlign="Left" />
                                                                <DataItemTemplate>
                                                                    <common:BlankTargetedHyperlink runat="server" ID="lblAttachmentFileName" Text="<%# Bind('FileName') %>"
                                                                        NavigateUrl="<%# Bind('Url') %>" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataColumn FieldName="#" Width="66px">
                                                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                <DataItemTemplate>
                                                                    <common:KeyImageButton runat="server" ID="imgAddAttachment" Key="<%# Bind('SequenceNumber') %>"
                                                                        CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png" OnClick="evt_imgAddAttachment_onClick"></common:KeyImageButton>
                                                                    <common:KeyImageButton runat="server" ID="imgDeleteAttachment" Key="<%# Bind('SequenceNumber') %>"
                                                                        CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png" OnClick="evt_imgDeleteAttachment_onClick" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataColumn>
                                                        </Columns>
                                                        <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />
                                                    </dx:ASPxGridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                        <td align="left" colspan="2" style="padding-left: 46px;">
                            <asp:Panel runat="server" ID="pnNotice" GroupingText="Notice" Width="350px">
                                <asp:UpdatePanel runat="server" ID="upnlNotice">
                                    <ContentTemplate>
                                        <dx:ASPxPanel runat="server" ID="pnNotes">
                                            <PanelCollection>
                                                <dx:PanelContent>
                                                    <div style="display: inline; float: left; width: 100%; border: 1px solid grey; background-color: #f6f6f6; padding-top: 10px;">
                                                        <div style="padding-left: 10px;">
                                                            <div style="width: 68%; float: left">
                                                                <table>
                                                                    <tr>
                                                                        <td style="width: 7px; vertical-align: middle;">To&nbsp;:&nbsp;&nbsp;
                                                                        </td>
                                                                        <td>
                                                                            <dx:ASPxComboBox ID="ddlSendTo" runat="server" EnableCallbackMode="true" IncrementalFilteringMode="Contains"
                                                                                ValueType="System.String" ValueField="Username" DropDownButton-Visible="true"
                                                                                AllowMouseWheel="true" OnLoad="evt_ddlSendTo_onLoad" Width="260px" CallbackPageSize="10"
                                                                                TextFormatString="{0}" AutoPostBack="true" MaxLength="61">
                                                                                <Columns>
                                                                                    <dx:ListBoxColumn FieldName="FULLNAME" Caption="Name" />
                                                                                    <dx:ListBoxColumn FieldName="Username" Caption="UserName" Visible="true" />
                                                                                </Columns>
                                                                            </dx:ASPxComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                        <div class="rowright" style="vertical-align: top !important; float: left; width: 100%; border-top: 1px dashed grey; margin: 5px 0px 5px 1px">
                                                            <table>
                                                                <tr>
                                                                    <td valign="middle">
                                                                        <asp:TextBox Enabled="true" runat="server" ID="txtNotice" Style="width: 300px; margin: 5px 5px 5px 5px; height: 60px;"
                                                                            TextMode="MultiLine" />
                                                                    </td>
                                                                    <td valign="middle">
                                                                        <dx:ASPxButton runat="server" ID="btSendNotice" Text="Send Notice" Height="60px"
                                                                            OnClick="evt_btSendNotice_onClick" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <asp:Literal runat="server" ID="litNoticeMessage"></asp:Literal>
                                                    </div>
                                                </dx:PanelContent>
                                            </PanelCollection>
                                        </dx:ASPxPanel>
                                        <div id="noticeWrap">
                                            <div id="noticeComment" runat="server" clientidmode="Static">
                                                <asp:Repeater runat="server" ID="rptrNotice" OnItemDataBound="evt_rptrNotice_onItemDataBound">
                                                    <ItemTemplate>
                                                        <asp:Literal runat="server" ID="litOpenDivNotice"></asp:Literal>
                                                        <asp:Literal runat="server" ID="litRptrNoticeComment"></asp:Literal>
                                                        <asp:Literal runat="server" ID="litCloseDivNotice"></asp:Literal>
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div class="row appovtopline">
                                <div class="appovbigtitle lefty">
                                    <div class="lefty">
                                        <div class="box Approved"></div>
                                        Approved
                                    </div>
                                    <div class="lefty">
                                        <div class="box Waiting"></div>
                                        Waiting
                                    </div>
                                    <div class="lefty">
                                        <div class="box Rejected"></div>
                                        Rejected
                                    </div>
                                    <div class="lefty">
                                        <div class="box Skipped"></div>
                                        Skip
                                    </div>
                                    <div class="lefty">
                                        <div class="box Concurred"></div>
                                        Concurrent
                                    </div>
                                    <div class="lefty">
                                        <div class=""></div>
                                    </div>
                                </div>
                                <div class="righty">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 300px;" colspan="2">
                            <asp:Panel runat="server" ID="Panel2" GroupingText="Approval History" Height="130px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel runat="server" ID="UpdatePanel1">
                                                <ContentTemplate>
                                                    <dx:ASPxGridView runat="server" ID="gridApprovalHistory" OnLoad="gridApprovalHistory_Load" Width="460px" ClientInstanceName="gridApprovalHistory"
                                                        ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
                                                        SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false" OnHtmlRowCreated="gridApprovalHistory_HtmlRowCreated"
                                                        SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
                                                        SettingsBehavior-AllowDragDrop="False">
                                                        <SettingsBehavior AllowFocusedRow="false" AllowDragDrop="False" AllowSelectByRowClick="false"
                                                            AllowSelectSingleRowOnly="True" AllowSort="False" ProcessSelectionChangedOnServer="True" />
                                                        <SettingsEditing Mode="Inline" />
                                                        <Settings ShowHorizontalScrollBar="true" ShowVerticalScrollBar="true" UseFixedTableLayout="True"
                                                            VerticalScrollableHeight="100" />
                                                        <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                                                        <Styles>
                                                            <AlternatingRow Enabled="True" CssClass="value-pv" />
                                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </Header>
                                                        </Styles>
                                                        <Columns>
                                                            <dx:GridViewDataTextColumn Caption="Role" Width="100px">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblHistoryRole" Text="<%# Bind('ROLE_NAME') %>" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Name" Width="150px">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblHistoryName" Text="<%# Bind('NAME') %>" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Status" Name="status" Width="100px">
                                                                <DataItemTemplate>
                                                                    <asp:Label runat="server" ID="lblHistoryStatusCd" Text="<%# Bind('STATUS') %>" Visible="false" />
                                                                    <asp:Image runat="server" ID="imgStatus" />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Date" Width="100px">
                                                                <DataItemTemplate>
                                                                    <dx:ASPxLabel runat="server" ID="lblHistoryDate" Text='<%# CommonFunction.Eval_Date(Eval("ACTUAL_DT"), true) %>' />
                                                                </DataItemTemplate>
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                        <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />
                                                    </dx:ASPxGridView>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>

                        <td rowspan="2">
                            <fh:FormHint runat="server" ID="FormHint1" />
                        </td>
                    </tr>
                </table>


                <!-- File Upload -->
                <asp:HiddenField ID="popUploadAttachment" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="popdlgUploadAttachment" BehaviorID="popdlgUploadAttachment"
                    runat="server" DropShadow="true" TargetControlID="popUploadAttachment" PopupControlID="pnpopUploadAttachment"
                    BackgroundCssClass="modalBackground" />
                <center>
                    <asp:Panel runat="server" ID="pnpopUploadAttachment" CssClass="speakerPopupList"
                        ClientIDMode="Static">
                        <asp:UpdatePanel runat="server" ID="pnupdateFileUploadPopup">
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btSendUploadAttachment" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="uploaddiv">
                                    <div class="row">
                                        <label>
                                            Category</label>
                                        <div class="uploadr">
                                            <dx:ASPxComboBox runat="server" ID="cboxAttachmentCategory" AutoPostBack="false"
                                                ClientIDMode="Static" ClientInstanceName="cboxAttachmentCategory" TextField="Description"
                                                ValueField="Code" ValueType="System.String" OnLoad="evt_cboxAttachmentCategory_onLoad" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <label>
                                            File</label>
                                        <div class="uploadr">
                                            <asp:FileUpload runat="server" ID="uploadAttachment" ClientIDMode="Static" />
                                        </div>
                                    </div>
                                    <div class="rowbtn">
                                        <div style="width: 1px; height: 30px; clear: both;">
                                            &nbsp
                                        </div>
                                        <div class="btnRightLong">
                                            <asp:Button ID="btSendUploadAttachment" runat="server" Text="Upload" OnClick="evt_btSendUploadAttachment_clicked"
                                                OnClientClick="return UploadAttachment_Click()" />
                                            <asp:Button ID="btCloseUploadAttachment" runat="server" Text="Close" OnClick="evt_btCloseUploadAttachment_clicked" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </center>

                <asp:HiddenField ID="popRevise" runat="server" />
                <ajaxToolkit:ModalPopupExtender ID="popdlgRevise" BehaviorID="popdlgRevise"
                    runat="server" DropShadow="true" TargetControlID="popRevise" PopupControlID="pnpopRevise"
                    BackgroundCssClass="modalBackground" />
                <center>
                    <asp:Panel runat="server" ID="pnpopRevise" CssClass="speakerPopupList" ClientIDMode="Static" Height="250px">
                        <asp:UpdatePanel runat="server" ID="pnupdateFileRevise">
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnReviseProceed" />
                            </Triggers>
                            <ContentTemplate>
                                <div class="uploaddiv">
                                    <div class="row">
                                        &nbsp;
                                <label>Accrued No</label>
                                        <asp:TextBox ID="txtReviseAccrNo" runat="server" Enabled="false" />
                                    </div>
                                    <div class="row">
                                        <div id="RejectDiv" runat="server">
                                            <label>Category</label>
                                            <asp:DropDownList ID="ddlReviseCategory" runat="server" />
                                        </div>
                                    </div>
                                    <div id="NoticeDiv" runat="server">
                                        <div id="WhatDiv">&nbsp;</div>
                                        <div class="row">
                                            <asp:Label runat="server" ID="lblNoticeLabel" Text=" Reason:" />
                                        </div>
                                        <div class="row">
                                            <asp:TextBox ID="txtReviseComment" ClientIDMode="Static" runat="server"
                                                Width="98%" Height="50px" TextMode="MultiLine" BackColor="#EEEEEE" />
                                        </div>
                                    </div>
                                    <div class="row" style="width: 500px">&nbsp;</div>
                                    <div class="rowbtn">
                                        <div class="btnRightLong">
                                            <asp:Button ID="btnReviseProceed" runat="server" Text="Proceed" OnClick="evt_btnReviseProceed_Click" OnClientClick="return IsEmptyReason()" />
                                            <asp:Button ID="btnReviseCancel" runat="server" Text="Cancel" OnClick="btnReviseCancel_Click" />
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </asp:Panel>
                </center>
            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
    <dx:ASPxPanel runat="server" ID="PanelQuestion" ClientInstanceName="PanelQuestion"
        ClientVisible="false" Width="100%">
        <PanelCollection>
            <dx:PanelContent>

                <ucq:UcQuestionaire runat="server" ID="UcQr" />


            </dx:PanelContent>
        </PanelCollection>
    </dx:ASPxPanel>
</asp:Content>
