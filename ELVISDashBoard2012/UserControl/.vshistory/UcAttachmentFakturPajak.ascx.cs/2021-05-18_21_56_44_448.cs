﻿using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Bytescout.BarCodeReader;
//using Bytescout.BarCodeReader;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxUploadControl;
using ELVISDashboard.MasterPage;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
//using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcAttachmentFakturPajak : System.Web.UI.UserControl
    {
        protected FtpLogic ftp = new FtpLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ShowLoadAttachmentFakturPajak()
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<AttachmentFakturPajak> _list = new List<AttachmentFakturPajak>();

            _list = def.formPersistence.GetAttachmentFakturPajak(def.FormData.ReferenceNoDataField);

            gridAttachmentFakturPajak.DataSource = _list;

            gridAttachmentFakturPajak.DataBind();

        }


        protected void evt_btAttFileFaktur_Click(object sender, EventArgs e)
        {
            popUploadFileFakturPajak.ShowOnPageLoad = true;
        }
        protected void evt_btSubmitFileFaktur_Click(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            if (uploadAttachmentFakturPajak.HasFile)
            {
                //foreach (HttpPostedFile uploadedFile2 in uploadAttachmentFakturPajak.PostedFile)
                //{
                //    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                //    listofuploadedfiles.Text += String.Format("{0}<br />", uploadedFile.FileName);
                //}
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile postedFile = Request.Files[i];
                    if (postedFile.ContentLength > 0)
                    {
                        string filename = CommonFunction.CleanFilename(postedFile.FileName);
                        FileInfo fi = new FileInfo(filename);
                        string ext = fi.Extension.Replace(".", "");
                        def.logic.Say("btSendUploadAttachment_Click", "FileName = {0}", postedFile.FileName);
                        string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                        int maxSize = int.Parse(strMaxSize);
                        maxSize = maxSize * 1024;
                        byte[] fileData = null;
                        var binaryReader = new BinaryReader(postedFile.InputStream);
                        fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                        if (fileData.Length > maxSize)
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());

                            return;
                        }
                        if (ext.ToUpper() != "PDF")
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "File Type Must Be PDF");
                            popUploadFileFakturPajak.ShowOnPageLoad = false;
                            return;

                        }

                        bool uploadSucceded = true;
                        string errorMessage = null;
                        AttachID a = def.GetAttachID();

                        string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
                        string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;

                        def.FormData.ReferenceNoDataField = _refno;
                        AttachmentFakturPajak attfileFaktur = new AttachmentFakturPajak();
                        attfileFaktur.REFERENCE_NO = _refno;
                        attfileFaktur.DIRECTORY = thn + "/" + def.ScreenType + "/" + _refno + "/FakturPajak";
                        attfileFaktur.FILE_NAME = filename;
                        attfileFaktur.STATUS = "";
                        attfileFaktur.URL = "";
                        attfileFaktur.ERR_MESSAGE = "";
                        // attfile.Blank = false;


                        attfileFaktur.CREATED_BY = def.UserData.USERNAME;
                        attfileFaktur.CREATED_DT = DateTime.Now;

                        ftp.ftpUploadFakturPajakBytes(thn, def.ScreenType,
                             _refno,
                             filename,
                             fileData,
                             ref uploadSucceded,
                             ref errorMessage);

                        Int64 _id = def.formPersistence.CreateAttachmentFakturPajak(attfileFaktur);

                        AttachmentFakturPajak attfileFakturedit = new AttachmentFakturPajak();


                        Reader barcodeReader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");


                        // barcodeReader.PDFRenderingResolution = 200;
                        barcodeReader.ImagePreprocessingFilters.AddScale(0.75d);


                        barcodeReader.BarcodeTypesToFind.QRCode = true;

                        barcodeReader.FastMode = true;
                       // FtpWebRequest reqFTP = null;

                       
                     string fileFaktur=  ftp.GetFileFtpFaktur(thn + "/" + def.ScreenType + "/" + _refno + "/FakturPajak", filename);
                        string testfile = @"D:\EFB_PATH\FakturPDF\Faktur_Source\340410_f7d1.pdf";
                        var _indexPages = barcodeReader.GetPdfPageCount(fileFaktur);
                        var indpages = _indexPages > 0 ? _indexPages - 1 : 0;
                        FoundBarcode[] barcodesDataPage;
                        var _page = 0;

                        for (int qi = indpages; qi >= 0; qi--)
                        {
                            barcodesDataPage = barcodeReader.ReadFrom(fileFaktur, qi, _indexPages);
                            if (barcodesDataPage.Count() > 0) //validasi kalo barcode nya gak kebaca
                            {
                                _page = barcodesDataPage[0].Page;
                                break;
                            }


                        }

                        FoundBarcode[] barcodes = barcodeReader.ReadFrom(testfile, _page, _indexPages);
                        string DJPLink;
                        if (barcodes.Count() > 0) //validasi kalo barcode nya gak kebaca
                        {
                            try
                            {
                                foreach (FoundBarcode barcode in barcodes)
                                {
                                    DJPLink = barcode.Value.ToString();

                                    if (DJPLink != "" || DJPLink != null)
                                    {
                                        attfileFakturedit.URL = DJPLink;
                                        attfileFakturedit.STATUS = "Valid";
                                        //var response = new HttpResponseMessage();
                                        //resValidateFakturPm VATInXML = new resValidateFakturPm();

                                        //using (HttpClient client = new HttpClient())
                                        //{
                                        //}
                                        // DJPLink = DJPLink.Replace(">TRIAL VERSION EXPIRES 90 DAYS AFTER INSTALLATION<", "");
                                        // ScanQRRepostitory.InsertQR(DJPLink, fileName, QRPDFFiles, fail, process);
                                    }
                                    else
                                    {
                                        attfileFakturedit.ERR_MESSAGE = "Fail to read file";

                                        attfileFakturedit.STATUS = "Invalid";

                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                attfileFakturedit.ERR_MESSAGE = ex.Message;
                                attfileFakturedit.STATUS = "Invalid";


                            }
                        }



                        attfileFakturedit.ID = _id;
                        attfileFakturedit.DESCRIPTION = "";


                        attfileFakturedit.CREATED_BY = def.UserData.USERNAME;
                        attfileFakturedit.CREATED_DT = DateTime.Now;
                        def.formPersistence.UpdateAttachmentFakturPajak(attfileFakturedit);




                    }
                }
            }

        }
    }
}