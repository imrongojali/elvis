﻿using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Bytescout.BarCodeReader;
using Common.Control;
using Common.Data;
//using Bytescout.BarCodeReader;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using ELVISDashboard.MasterPage;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
//using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
   
    public partial class UcAttachmentFakturPajak : System.Web.UI.UserControl
    {
      
        public FtpLogic ftp = new FtpLogic();
       
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ShowLoadAttachmentFakturPajak()
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<AttachmentFakturPajak> _list = new List<AttachmentFakturPajak>();

            _list = def.formPersistence.GetAttachmentFakturPajak(def.FormData.ReferenceNoDataField);

            gridAttachmentFakturPajak.DataSource = _list;

            gridAttachmentFakturPajak.DataBind();

        }

        
        protected void evt_btAttFileFaktur_Click(object sender, EventArgs e)
        {
            popUploadFileFakturPajak.ShowOnPageLoad = true;
        }

        protected void evt_btManualFaktur_Click(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.PnlMasterHeader.ClientVisible = false;
            def.PnlManualFaktur.ClientVisible = true;
        }
        protected void evt_btSubmitFileFaktur_Click(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            if (uploadAttachmentFakturPajak.HasFile)
            {
                //foreach (HttpPostedFile uploadedFile2 in uploadAttachmentFakturPajak.PostedFile)
                //{
                //    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                //    listofuploadedfiles.Text += String.Format("{0}<br />", uploadedFile.FileName);
                //}
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile postedFile = Request.Files[i];
                    if (postedFile.ContentLength > 0)
                    {
                        string filename = CommonFunction.CleanFilename(postedFile.FileName);
                        FileInfo fi = new FileInfo(filename);
                        string ext = fi.Extension.Replace(".", "");
                        def.logic.Say("btSendUploadAttachment_Click", "FileName = {0}", postedFile.FileName);
                        string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                        int maxSize = int.Parse(strMaxSize);
                        maxSize = maxSize * 1024;
                        byte[] fileData = null;
                        var binaryReader = new BinaryReader(postedFile.InputStream);
                        fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                        if (fileData.Length > maxSize)
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());

                            return;
                        }
                        if (ext.ToUpper() != "PDF")
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "File Type Must Be PDF");
                            
                            popUploadFileFakturPajak.ShowOnPageLoad = false;
                            return;

                        }

                        bool uploadSucceded = true;
                        string errorMessage = null;
                        AttachID a = def.GetAttachID();

                        string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
                        string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;

                        def.FormData.ReferenceNoDataField = _refno;
                        AttachmentFakturPajak attfileFaktur = new AttachmentFakturPajak();
                        attfileFaktur.REFERENCE_NO = _refno;
                        attfileFaktur.DIRECTORY = thn + "/" + def.ScreenType + "/" + _refno + "/FakturPajak";
                        attfileFaktur.FILE_NAME = filename;
                        attfileFaktur.DESCRIPTION = "";
                        attfileFaktur.STATUS = "";
                        attfileFaktur.URL = "";
                        attfileFaktur.ERR_MESSAGE = "";
                        // attfile.Blank = false;


                        attfileFaktur.CREATED_BY = def.UserData.USERNAME;
                        attfileFaktur.CREATED_DT = DateTime.Now;

                        ftp.ftpUploadFakturPajakBytes(thn, def.ScreenType,
                             _refno,
                             filename,
                             fileData,
                             ref uploadSucceded,
                             ref errorMessage);

                        string ValueQrCode = ftp.ftpGetValueBarcode(fileData);



                        Int64 _id = def.formPersistence.CreateAttachmentFakturPajak(attfileFaktur);
                        AttachmentFakturPajak attfileFakturedit = new AttachmentFakturPajak();

                        attfileFakturedit.ID = _id;
                        attfileFakturedit.DESCRIPTION = def.formPersistence.NoFakturXML(ValueQrCode);
                        try
                        {
                            var response = new HttpResponseMessage();
                            VATInManualInputView model = new VATInManualInputView();
                            resValidateFakturPm VATInXML = new resValidateFakturPm();
                           
                                if (!string.IsNullOrEmpty(ValueQrCode))
                                {
                                    using (HttpClient client = new HttpClient())
                                    {
                                        client.DefaultRequestHeaders.Accept.Clear();
                                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                                        Uri serviceUri = new Uri(ValueQrCode, UriKind.Absolute);

                                        XmlDocument doc = new XmlDocument();
                                        response = client.GetAsync(serviceUri).Result;

                                        var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                                        var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                                        using (var reader = XmlReader.Create(stream))
                                        {
                                            VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                                            if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                                            {
                                                throw new Exception("Invalid URL, Invoice not found");
                                            }
                                        }
                                        string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                        DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                                        DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);


                                        model.InvoiceNumber = VATInXML.nomorFaktur;
                                        model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                                        model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                                        model.FGPengganti = VATInXML.fgPengganti;
                                        model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                        model.ExpireDate = endOfMonth.ToShortDateString();
                                        model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                                        model.SupplierName = VATInXML.namaPenjual;

                                        model.SupplierAddress = VATInXML.alamatPenjual;

                                        model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                                        model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                                        model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                                        model.StatusApprovalXML = VATInXML.statusApproval;
                                        model.StatusFakturXML = VATInXML.statusFaktur;
                                        model.VATBaseAmount = VATInXML.jumlahDpp;
                                        model.VATAmount = VATInXML.jumlahPpn;
                                        model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                                        model.FakturType = "eFaktur";
                                        model.VATInDetails = new List<VATInDetailManualInputView>();

                                        foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                        {
                                            VATInDetailManualInputView detailModel = new VATInDetailManualInputView();

                                            detailModel.UnitName = detail.nama;
                                            detailModel.UnitPrice = detail.hargaSatuan;
                                            detailModel.Quantity = detail.jumlahBarang;
                                            detailModel.TotalPrice = detail.hargaTotal;
                                            detailModel.Discount = detail.diskon;
                                            detailModel.DPP = detail.dpp;
                                            detailModel.PPN = detail.ppn;
                                            detailModel.PPNBM = detail.ppnbm;
                                            detailModel.TarifPPNBM = detail.tarifPpnbm;

                                            model.VATInDetails.Add(detailModel);
                                        }
                                    }
                                }
                           
                            def.formPersistence.GenerateXML(model, ValueQrCode, _id);
                          
                            attfileFakturedit.STATUS = "Valid";
                            attfileFakturedit.ERR_MESSAGE = "";
                            popUploadFileFakturPajak.ShowOnPageLoad = false;

                        }
                        catch (Exception ex)
                        {
                          
                            attfileFakturedit.STATUS = "InValid";
                            string err = "";
                            if (ex.Message.Trim()=="Object reference not set to an instance of an object.")
                            {
                                err = "QR Code Tidak Terbaca";
                            }
                            else if (ex.Message.Trim() == "One or more errors occurred.")
                            {
                                err = "Server DJP tidak dapat terkoneksi";
                            }
                            
                            else 
                            {
                                err = ex.Message.Trim();
                            }

                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                            attfileFakturedit.ERR_MESSAGE = err;
                            popUploadFileFakturPajak.ShowOnPageLoad = false;

                        }
                        
                        attfileFakturedit.URL = ValueQrCode;
                       
                        def.formPersistence.UpdateAttachmentFakturPajak(attfileFakturedit);

                        ShowLoadAttachmentFakturPajak();


                    }
                }
            }

        }
        protected void evt_gridAttachmentFakturPajak_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            gridAttachmentFakturPajak = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            Int64 key = (Int64)arg.KeyValue;
            if (key > 0)
            {

             AttachmentFakturPajak _itemlist = new AttachmentFakturPajak();
            _itemlist = def.formPersistence.GetAttachmentFakturPajak(def.FormData.ReferenceNoDataField).Where(x => x.ID == key).SingleOrDefault();
            BlankTargetedHyperlink lblFILE_NAME = (BlankTargetedHyperlink)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblFILE_NAME");
             ASPxLabel lblManualFileFaktur = (ASPxLabel)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblManualFileFaktur");
            if (lblFILE_NAME != null)
             {
                   if (_itemlist.FILE_NAME.Trim()=="Manual")
                    {
                        lblFILE_NAME.Visible = false;
                        lblManualFileFaktur.Visible = true;
                    }
                    else
                    {
                        lblFILE_NAME.Visible = true;
                        lblManualFileFaktur.Visible = false;
                    }
             }
                   

            }
           

        }
        


    }
}