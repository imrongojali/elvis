﻿using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Bytescout.BarCodeReader;
using Common.Data;
//using Bytescout.BarCodeReader;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxUploadControl;
using ELVISDashboard.MasterPage;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
//using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
   
    public partial class UcAttachmentFakturPajak : System.Web.UI.UserControl
    {
      
        public FtpLogic ftp = new FtpLogic();
       
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ShowLoadAttachmentFakturPajak()
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<AttachmentFakturPajak> _list = new List<AttachmentFakturPajak>();

            _list = def.formPersistence.GetAttachmentFakturPajak(def.FormData.ReferenceNoDataField);

            gridAttachmentFakturPajak.DataSource = _list;

            gridAttachmentFakturPajak.DataBind();

        }

        
        protected void evt_btAttFileFaktur_Click(object sender, EventArgs e)
        {
            popUploadFileFakturPajak.ShowOnPageLoad = true;
        }

      
        protected void evt_btSubmitFileFaktur_Click(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.clearScreenMessage();
            if (uploadAttachmentFakturPajak.HasFile)
            {
                //foreach (HttpPostedFile uploadedFile2 in uploadAttachmentFakturPajak.PostedFile)
                //{
                //    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                //    listofuploadedfiles.Text += String.Format("{0}<br />", uploadedFile.FileName);
                //}
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile postedFile = Request.Files[i];
                    if (postedFile.ContentLength > 0)
                    {
                        string filename = CommonFunction.CleanFilename(postedFile.FileName);
                        FileInfo fi = new FileInfo(filename);
                        string ext = fi.Extension.Replace(".", "");
                        def.logic.Say("btSendUploadAttachment_Click", "FileName = {0}", postedFile.FileName);
                        string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                        int maxSize = int.Parse(strMaxSize);
                        maxSize = maxSize * 1024;
                        byte[] fileData = null;
                        var binaryReader = new BinaryReader(postedFile.InputStream);
                        fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                        if (fileData.Length > maxSize)
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());

                            return;
                        }
                        if (ext.ToUpper() != "PDF")
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "File Type Must Be PDF");
                            popUploadFileFakturPajak.ShowOnPageLoad = false;
                            return;

                        }

                        bool uploadSucceded = true;
                        string errorMessage = null;
                        AttachID a = def.GetAttachID();

                        string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
                        string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;

                        def.FormData.ReferenceNoDataField = _refno;
                        AttachmentFakturPajak attfileFaktur = new AttachmentFakturPajak();
                        attfileFaktur.REFERENCE_NO = _refno;
                        attfileFaktur.DIRECTORY = thn + "/" + def.ScreenType + "/" + _refno + "/FakturPajak";
                        attfileFaktur.FILE_NAME = filename;
                        attfileFaktur.DESCRIPTION = "";
                        attfileFaktur.STATUS = "";
                        attfileFaktur.URL = "";
                        attfileFaktur.ERR_MESSAGE = "";
                        // attfile.Blank = false;


                        attfileFaktur.CREATED_BY = def.UserData.USERNAME;
                        attfileFaktur.CREATED_DT = DateTime.Now;

                        ftp.ftpUploadFakturPajakBytes(thn, def.ScreenType,
                             _refno,
                             filename,
                             fileData,
                             ref uploadSucceded,
                             ref errorMessage);

                        string ValueQrCode = ftp.ftpGetValueBarcode(fileData);



                        Int64 _id = def.formPersistence.CreateAttachmentFakturPajak(attfileFaktur);
                        AttachmentFakturPajak attfileFakturedit = new AttachmentFakturPajak();

                        attfileFakturedit.ID = _id;
                        attfileFakturedit.DESCRIPTION = def.formPersistence.NoFakturXML(ValueQrCode);
                        try
                        {
                             def.formPersistence.GenerateXML(ValueQrCode, _id);
                          
                            attfileFakturedit.STATUS = "Valid";
                            attfileFakturedit.ERR_MESSAGE = "";

                        }
                        catch (Exception ex)
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", ex.Message);
                            attfileFakturedit.STATUS = "NoValid";
                            attfileFakturedit.ERR_MESSAGE = ex.Message;
                           
                        }
                        
                        attfileFakturedit.URL = ValueQrCode;
                       
                        def.formPersistence.UpdateAttachmentFakturPajak(attfileFakturedit);




                    }
                }
            }

        }
    }
}