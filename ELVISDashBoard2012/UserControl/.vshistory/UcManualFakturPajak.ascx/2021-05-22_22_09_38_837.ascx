﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcManualFakturPajak.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcManualFakturPajak" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<style type="text/css">
    .auto-style3 {
        width: 150px;
        height: 15px;
    }

    .auto-style4 {
        width: 300px;
        height: 15px;
    }
</style>
<%--<script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>--%>
<script type="text/javascript">

    function VATAmount_KeyPress(s, e) {
        spnEnomVATBaseAmount.SetValue(s.GetValue());  
    }

</script>



<asp:UpdatePanel runat="server" ID="pnManualFakturPajak">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmitManualFaktur" />
        <asp:PostBackTrigger ControlID="btnCancelManualFaktur" />
    </Triggers>
    <ContentTemplate>
        <table style="width: 100%">

            <tr>

                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Faktur Type / Tax Invoice Number <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                    <table>
                        <tr>
                            <td>
                                <dx:ASPxComboBox ID="cmbEnomFakturType" runat="server" AutoPostBack="True" Width="100px"
                                    DropDownWidth="100px">
                                    <Items>
                                        <dx:ListEditItem Text="Non eFaktur" Value="Non eFaktur" />
                                        <dx:ListEditItem Selected="True" Text="eFaktur" Value="eFaktur" />
                                    </Items>
                                </dx:ASPxComboBox>

                            </td>
                            <td></td>
                            <td>
                                <dx:ASPxTextBox ID="txtEnomTaxInvoiceNumber" runat="server" Width="150" ClientInstanceName="txtEnomTaxInvoiceNumber">
                                    <MaskSettings Mask="+1 (999) 000-0000" IncludeLiterals="None" />
                                   <%-- <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />--%>
                                    <ClientSideEvents />
                                </dx:ASPxTextBox>
                            </td>
                        </tr>
                    </table>


                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Tax Invoice Date <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">

                      <dx:ASPxDateEdit ID="dtEnomTaxInvoiceDate"  ClientInstanceName="dtEnomTaxInvoiceDate"
                                        ClientIDMode="static" runat="server" Width="120px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table">

                                      
                                         <CalendarProperties>
                                             <FastNavProperties Enabled="False" />
                                         </CalendarProperties>

                                      
                                        <%-- <ValidationSettings ValidationGroup="GroupPromotion" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True"  ErrorText="Name is required" />
                        </ValidationSettings>--%>
                                    </dx:ASPxDateEdit>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Supplier NPWP  <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                      <dx:ASPxTextBox ID="txtEnomSupplierNPWP" runat="server" Width="142"  ClientInstanceName="txtEnomSupplierNPWP">
                                        <MaskSettings Mask="99.999.999.9-999.999" IncludeLiterals="None" />
                                     <%--   <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>

                                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Supplier Name <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                      <dx:ASPxTextBox ID="txtEnomSupplierName" runat="server" Width="100%"  ClientInstanceName="txtEnomSupplierName">
                                      
                                     <%--   <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>

                                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Supplier Address <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                      <dx:ASPxTextBox ID="txtEnomSupplierAddress" runat="server" Width="100%"  ClientInstanceName="txtEnomSupplierAddress">
                                      
                                     <%--   <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>

                                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Buyer NPWP <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                     <dx:ASPxTextBox ID="txtEnomBuyerNPWP" runat="server" Width="142"  ClientInstanceName="txtEnomBuyerNPWP">
                                        <MaskSettings Mask="99.999.999.9-999.999" IncludeLiterals="None" />
                                     <%--   <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>

                                    </dx:ASPxTextBox>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">VAT Amount <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                       <dx:ASPxSpinEdit ID="spnEnomVATAmount" ClientInstanceName="spnEnomVATAmount" runat="server" DisplayFormatString="C2" Number="0" Width="172">
                                       <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>
                            <ClientSideEvents  KeyPress="VATAmount_KeyPress" />
                                    </dx:ASPxSpinEdit>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">VAT Base Amount <span class="right-bold">:</span>
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                    <dx:ASPxSpinEdit ID="spnEnomVATBaseAmount" ClientInstanceName="spnEnomVATBaseAmount" runat="server" DisplayFormatString="C2" Number="0" Width="172">
                                       <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>
                                    </dx:ASPxSpinEdit>
                </td>
            </tr>
            <tr>
                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Total PPnBM
                </td>
                <td valign="middle" class="td-layout-value value-pv">
                     <dx:ASPxSpinEdit ID="spnEnomTotalPPnBM" ClientInstanceName="spnEnomTotalPPnBM" runat="server" DisplayFormatString="C2" Number="0" Width="172">
                                       <%-- <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>--%>
                                    </dx:ASPxSpinEdit>
                </td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td align="right" valign="baseline">

                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnSubmitManualFaktur"
                        Text="Submit">
                        <ClientSideEvents Click="function(s,e) {loading(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnCancelManualFaktur"
                        Text="Cancel">
                        <ClientSideEvents Click="function(s,e) { popUploadFileFakturPajak.Hide(); }" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>

    </ContentTemplate>
</asp:UpdatePanel>



