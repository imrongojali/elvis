﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcManualFakturPajak.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcManualFakturPajak" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<style type="text/css">
    .auto-style2 {
        width: 267px;
    }
</style>
<%--<script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>--%>
<script type="text/javascript">
 


</script>
    

 
                        <asp:UpdatePanel runat="server" ID="pnManualFakturPajak">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btnSubmitManualFaktur" />
                             <asp:PostBackTrigger ControlID="btnCancelManualFaktur" />
                        </Triggers>
                        <ContentTemplate>
                           <table>
                              
                                <tr>
                                    <td style="margin-right: 10px;">&nbsp;File
                                    </td>
                                    <td>
                                        <asp:FileUpload runat="server" ID="FileUpload1" ClientIDMode="Static" AllowMultiple="true"  />
                                        <asp:Label ID="Label1" runat="server" />  
                                    </td>
                                </tr>
                                  <tr>
                                    <td style="margin-right: 10px;">
                                    </td>
                                    <td>
                                       

                                    </td>
                                </tr>
                                 <tr>
                                    <td style="margin-right: 10px;">
                                    </td>
                                    <td>
                                       

                                    </td>
                                </tr>
                                 <tr>
                                    <td style="margin-right: 10px;">
                                    </td>
                                    <td  align="right" valign="baseline">
                                       
                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnSubmitManualFaktur"
                                            Text="Submit" >
                                            <ClientSideEvents Click="function(s,e) {loading(); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnCancelManualFaktur"
                                            Text="Cancel">
                                            <ClientSideEvents Click="function(s,e) { popUploadFileFakturPajak.Hide(); }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>
                            
                        </ContentTemplate>
                    </asp:UpdatePanel>



