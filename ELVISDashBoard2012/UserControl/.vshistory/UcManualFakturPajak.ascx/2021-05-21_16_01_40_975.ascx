﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcManualFakturPajak.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcManualFakturPajak" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<style type="text/css">
    .auto-style2 {
        width: 267px;
    }
</style>
<%--<script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>--%>
<script type="text/javascript">



</script>



<asp:UpdatePanel runat="server" ID="pnManualFakturPajak">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmitManualFaktur" />
        <asp:PostBackTrigger ControlID="btnCancelManualFaktur" />
    </Triggers>
    <ContentTemplate>
        <table style="width: 100%">

            <tr>

                <td valign="middle" style="width: 300px" class="mandatory_label td-layout-item">&nbsp;Faktur Type / Tax Invoice Number <span class="right-bold">:</span>
                </td>
                <td valign="middle" style="width: 125px;" class="td-layout-value value-pv">
                                                                <dx:ASPxComboBox runat="server" Width="110px" ID="ddlDocType" ClientIDMode="static"
                                                                    ValueType="System.String" ValueField="Code" CssClass="display-inline-table" TextField="Description"
                                                                    Visible="false" OnLoad="ddlDocType_Load" AutoPostBack="true" OnSelectedIndexChanged="evt_ddlDocType_SelectedIndexChanged" />
                                                                <dx:ASPxLabel ID="lblDocType" runat="server" Visible="false" CssClass="display-inline-table" />
                                                            </td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Tax Invoice Date
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier NPWP
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier Name
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier Address
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Buyer NPWP
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;VAT Amount
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;">&nbsp;VAT Base Amount
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;">&nbsp;Total PPnBM
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td align="right" valign="baseline">

                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnSubmitManualFaktur"
                        Text="Submit">
                        <ClientSideEvents Click="function(s,e) {loading(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnCancelManualFaktur"
                        Text="Cancel">
                        <ClientSideEvents Click="function(s,e) { popUploadFileFakturPajak.Hide(); }" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>
        <table border="2" bordercolor="red" width="100%" cellspacing="0" cellpadding="0">
            <thead>
                <tr>
                    <th style="font-size: 13px; padding: 5px 9px 6px 9px; line-height: 1em;" align="left" bgcolor="#EAEAEA" width="300">Shipping Information:</th>
                    <th width="10">&nbsp;</th>
                    <th style="font-size: 13px; padding: 5px 9px 6px 9px; line-height: 1em;" align="left" bgcolor="#EAEAEA" width="300">Shipping Method:</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="font-size: 12px; padding: 7px 9px 9px 9px; border-left: 1px solid #EAEAEA; border-bottom: 1px solid #EAEAEA; border-right: 1px solid #EAEAEA;" valign="top">{{var order.getShippingAddress().format('html')}} &nbsp;</td>
                    <td>&nbsp;</td>
                    <td style="font-size: 12px; padding: 7px 9px 9px 9px; border-left: 1px solid #EAEAEA; border-bottom: 1px solid #EAEAEA; border-right: 1px solid #EAEAEA;" valign="top">{{var order.getShippingDescription()}} &nbsp;</td>
                </tr>
            </tbody>
        </table>
        <table border="2" bordercolor="red" width="600" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td>xx
        <p style="font-size: 12px; margin: 0 0 10px 0;">xx</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </ContentTemplate>
</asp:UpdatePanel>



