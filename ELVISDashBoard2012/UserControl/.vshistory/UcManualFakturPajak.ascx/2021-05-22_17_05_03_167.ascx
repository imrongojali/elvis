﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcManualFakturPajak.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcManualFakturPajak" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<style type="text/css">
    .auto-style3 {
        width: 150px;
        height: 15px;
    }
    .auto-style4 {
        width: 300px;
        height: 15px;
    }
</style>
<%--<script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>--%>
<script type="text/javascript">



</script>



<asp:UpdatePanel runat="server" ID="pnManualFakturPajak">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmitManualFaktur" />
        <asp:PostBackTrigger ControlID="btnCancelManualFaktur" />
    </Triggers>
    <ContentTemplate>
        <table style="width: 100%">

            <tr>

                <td valign="middle" style="width: 250px" class="mandatory_label td-layout-item">Faktur Type / Tax Invoice Number <span class="right-bold">:</span>
                </td>
                <td valign="baseline">
                    <table>
                        <tr>
                            <td> <dx:ASPxComboBox ID="cmbPromtChar" runat="server" AutoPostBack="True" Width="100px"
                                DropDownWidth="100px">
                                <Items>
                                    <dx:ListEditItem Text="Non eFaktur" Value="Non eFaktur" />
                                                                        <dx:ListEditItem Selected="True" Text="eFaktur" Value="eFaktur" />
                                </Items>
                            </dx:ASPxComboBox>

                            </td>
                             <td> </td>
                             <td>    <dx:ASPxTextBox ID="txtPhone" runat="server" Width="150" ClientInstanceName="clTxtPhone">
                            <MaskSettings Mask="+1 (999) 000-0000" IncludeLiterals="None" />
                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" />
                            <ClientSideEvents  />
                        </dx:ASPxTextBox></td>
                        </tr>
                    </table>
                                                        
                
                                                            </td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Tax Invoice Date
                </td>
                <td>


                </td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier NPWP
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier Name
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Supplier Address
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;Buyer NPWP
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px; width: 300px">&nbsp;VAT Amount
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;">&nbsp;VAT Base Amount
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;">&nbsp;Total PPnBM
                </td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td></td>
            </tr>
            <tr>
                <td style="margin-right: 10px;"></td>
                <td align="right" valign="baseline">

                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnSubmitManualFaktur"
                        Text="Submit">
                        <ClientSideEvents Click="function(s,e) {loading(); }" />
                    </dx:ASPxButton>
                    <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnCancelManualFaktur"
                        Text="Cancel">
                        <ClientSideEvents Click="function(s,e) { popUploadFileFakturPajak.Hide(); }" />
                    </dx:ASPxButton>
                </td>
            </tr>
        </table>

    </ContentTemplate>
</asp:UpdatePanel>



