﻿using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void EnomTaxInvoiceNumbereFaktur_Validation(object sender, ValidationEventArgs e)
        {
            if (cmbEnomFakturType.Value.ToString() == "eFaktur")
            {
                if (string.IsNullOrEmpty(e.Value.ToString()))
                {
                    e.IsValid = false;
                }
                else
                {
                    e.IsValid = true;
                }
            }
            else
            {
                e.IsValid = true;
            }
           

               
        }
        protected void EnomTaxInvoiceNumberNoneFaktur_Validation(object sender, ValidationEventArgs e)
        {
            if (cmbEnomFakturType.Value.ToString() == "Non eFaktur")
            {
                if (string.IsNullOrEmpty(e.Value.ToString()))
                {
                    e.IsValid = false;
                }
                else
                {
                    e.IsValid = true;
                }
            }
            else
            {
                e.IsValid = true;
            }



        }

        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;
        }

            //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
            //{

            //}
        }
}