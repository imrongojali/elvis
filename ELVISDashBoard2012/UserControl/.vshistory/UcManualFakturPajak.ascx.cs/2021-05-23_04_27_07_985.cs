﻿using BusinessLogic.VoucherForm;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
     

        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            AttachID a = def.GetAttachID();
            string fakturtype = cmbEnomFakturType.ToString();
            string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
            string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;
            def.FormData.ReferenceNoDataField = _refno;
            AttachmentFakturPajak manualfileFaktur = new AttachmentFakturPajak();
            manualfileFaktur.REFERENCE_NO = _refno;
            manualfileFaktur.DIRECTORY = "";
            manualfileFaktur.FILE_NAME = "Manual";
            if (fakturtype == "Non eFaktur")
            {
                manualfileFaktur.DESCRIPTION = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().FormatNomorManualFakturGabungan();
            }
            else
            {
                manualfileFaktur.DESCRIPTION = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan();
            }
            manualfileFaktur.DIRECTORY = "";
            manualfileFaktur.STATUS = "";
            manualfileFaktur.URL = "";
            manualfileFaktur.ERR_MESSAGE = "";
            manualfileFaktur.CREATED_BY = def.UserData.USERNAME;
            manualfileFaktur.CREATED_DT = DateTime.Now;
            Int64 _id = def.formPersistence.CreateAttachmentFakturPajak(manualfileFaktur);
          
            def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak();
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;

            //try
            //{
            //    if (fakturtype == "Non eFaktur")
            //    {
            //        model.InvoiceNumber = model.InvoiceNumberFull;
            //        model.FGPengganti = "0";
            //        model.InvoiceDate = DateTime.ParseExact(model.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
            //    }
            //    else
            //    {
            //        model.KDJenisTransaksi = model.InvoiceNumberFull.Substring(0, 2);
            //        model.FGPengganti = model.InvoiceNumberFull.Substring(2, 1);
            //        model.InvoiceNumber = model.InvoiceNumberFull.FormatNomorFakturPolos();
            //        model.InvoiceDate = DateTime.ParseExact(model.InvoiceDate, "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
            //    }
            //    result = VATInRepository.Instance.Insert(model, null, CurrentLogin.Username, DateTime.Now); //TODO: Later change  to real user
            //}
            //catch (Exception e)
            //{
            //    e.LogToApp("VATIn Manual Input Non eFaktur", MessageType.ERR, CurrentLogin.Username);
            //    result.ResultCode = false;
            //    result.ResultDesc = e.Message;
            //}
        }
        protected void evt_Cancel_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;
        }
        

        //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}