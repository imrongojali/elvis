﻿using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
     

        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            Int64 _id = 0;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            AttachID a = def.GetAttachID();
            string fakturtype = cmbEnomFakturType.ToString();
            string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
            string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;
            def.FormData.ReferenceNoDataField = _refno;
         
           
            VATInManualInputView VATInManualFaktur = new VATInManualInputView();
            VATInManualFaktur.FakturType = fakturtype;
          //  VATInManualFaktur.InvoiceNumberFull = manualfileFaktur.DESCRIPTION;
          //  VATInManualFaktur.InvoiceDate = dtEnomTaxInvoiceDate.Value.ToString().FormatSQLDate();
            VATInManualFaktur.SupplierNPWP = txtEnomSupplierNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.SupplierName = txtEnomSupplierName.Value.ToString();
            VATInManualFaktur.SupplierAddress = txtEnomSupplierAddress.Value.ToString();
            VATInManualFaktur.NPWPLawanTransaksi= txtEnomBuyerNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.VATBaseAmount =Convert.ToDecimal(spnEnomVATAmount.Value.ToString());
            VATInManualFaktur.VATAmount = Convert.ToDecimal(spnEnomVATBaseAmount.Value.ToString());
            VATInManualFaktur.JumlahPPnBM = Convert.ToDecimal(spnEnomTotalPPnBM.Value.ToString());
            try
            {
                string invno = "";
                if (fakturtype == "Non eFaktur")
                {
                    invno = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                    VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                    VATInManualFaktur.FGPengganti = "0";
                    VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                }
                else
                {
                    invno=txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim();
                    VATInManualFaktur.KDJenisTransaksi = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(0, 2);
                    VATInManualFaktur.FGPengganti = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(2, 1);
                    VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().Trim();
                    VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                }
                AttachmentFakturPajak manualfileFaktur = new AttachmentFakturPajak();
                manualfileFaktur.REFERENCE_NO = _refno;
                manualfileFaktur.DIRECTORY = "";
                manualfileFaktur.FILE_NAME = "Manual";
                manualfileFaktur.DESCRIPTION = invno;

                manualfileFaktur.STATUS = "Valid";
                manualfileFaktur.URL = "";
                manualfileFaktur.ERR_MESSAGE = "";
                manualfileFaktur.CREATED_BY = def.UserData.USERNAME;
                manualfileFaktur.CREATED_DT = DateTime.Now;
                _id = def.formPersistence.CreateAttachmentFakturPajak(manualfileFaktur);
                def.formPersistence.GenerateXML(VATInManualFaktur, null, _id);

                def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak();
                def.PnlMasterHeader.ClientVisible = true;
                def.PnlManualFaktur.ClientVisible = false;

            }
            catch (Exception ex)
            { 
                string err = "";
                err = ex.Message.Trim();
                def.formPersistence.DeleteFakturPajak(_id);
                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
            }
           
        }
        protected void evt_Cancel_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;
        }
        

        //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}