﻿using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
     

        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            AttachID a = def.GetAttachID();
            string fakturtype = cmbEnomFakturType.ToString();
            string thn = (def.FormData.PVYear ?? DateTime.Now.Year).str();
            string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;
            def.FormData.ReferenceNoDataField = _refno;
            AttachmentFakturPajak manualfileFaktur = new AttachmentFakturPajak();
            manualfileFaktur.REFERENCE_NO = _refno;
            manualfileFaktur.DIRECTORY = "";
            manualfileFaktur.FILE_NAME = "Manual";
            manualfileFaktur.DESCRIPTION = "";
          
            manualfileFaktur.STATUS = "";
            manualfileFaktur.URL = "";
            manualfileFaktur.ERR_MESSAGE = "";
            manualfileFaktur.CREATED_BY = def.UserData.USERNAME;
            manualfileFaktur.CREATED_DT = DateTime.Now;
            Int64 _id = def.formPersistence.CreateAttachmentFakturPajak(manualfileFaktur);
            AttachmentFakturPajak manualFakturedit = new AttachmentFakturPajak();
            manualFakturedit.ID = _id;
            if (fakturtype == "Non eFaktur")
            {
                manualFakturedit.DESCRIPTION = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString();
            }
            else
            {
                manualFakturedit.DESCRIPTION = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan();
            }
          
            VATInManualInputView VATInManualFaktur = new VATInManualInputView();
            VATInManualFaktur.FakturType = fakturtype;
          //  VATInManualFaktur.InvoiceNumberFull = manualfileFaktur.DESCRIPTION;
          //  VATInManualFaktur.InvoiceDate = dtEnomTaxInvoiceDate.Value.ToString().FormatSQLDate();
            VATInManualFaktur.SupplierNPWP = txtEnomSupplierNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.SupplierName = txtEnomSupplierName.Value.ToString();
            VATInManualFaktur.SupplierAddress = txtEnomSupplierAddress.Value.ToString();
            VATInManualFaktur.NPWPLawanTransaksi= txtEnomBuyerNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.VATBaseAmount =Convert.ToDecimal(spnEnomVATAmount.Value.ToString());
            VATInManualFaktur.VATAmount = Convert.ToDecimal(spnEnomVATBaseAmount.Value.ToString());
            VATInManualFaktur.JumlahPPnBM = Convert.ToDecimal(spnEnomTotalPPnBM.Value.ToString());
            try
            {
                if (fakturtype == "Non eFaktur")
                {
                    VATInManualFaktur.InvoiceNumber = manualFakturedit.DESCRIPTION;
                    VATInManualFaktur.FGPengganti = "0";
                    VATInManualFaktur.InvoiceDate = DateTime.ParseExact(dtEnomTaxInvoiceDate.Value.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
                }
                else
                {
                    VATInManualFaktur.KDJenisTransaksi = manualFakturedit.DESCRIPTION.Substring(0, 2);
                    VATInManualFaktur.FGPengganti = manualFakturedit.DESCRIPTION.Substring(2, 1);
                    VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumbereFaktur.Value.ToString();
                    VATInManualFaktur.InvoiceDate = DateTime.ParseExact(dtEnomTaxInvoiceDate.Value.ToString(), "dd-MM-yyyy", CultureInfo.InvariantCulture).FormatSQLDate();
                }
                def.formPersistence.GenerateXML(VATInManualFaktur, null, _id);

                manualFakturedit.STATUS = "Valid";
                manualFakturedit.ERR_MESSAGE = "";
               
            }
            catch (Exception ex)
            {
                manualFakturedit.STATUS = "NoValid";
                string err = "";
               
                    err = ex.Message.Trim();
                

                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                manualFakturedit.ERR_MESSAGE = err;
              
            }
            manualFakturedit.URL = "";

            def.formPersistence.UpdateAttachmentFakturPajak(manualFakturedit);
            def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak();
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;
        }
        protected void evt_Cancel_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;
        }
        

        //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}