﻿using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            Int64 _id = 0;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            AttachID a = def.GetAttachID();
            string fakturtype = cmbEnomFakturType.Value.ToString();
           
            string _refno = String.IsNullOrEmpty(def.FormData.ReferenceNoDataField) ? a.ReffNo : def.FormData.ReferenceNoDataField;
            def.FormData.ReferenceNoDataField = _refno;


            VATInManualInputView VATInManualFaktur = new VATInManualInputView();
            VATInManualFaktur.FakturType = fakturtype;
            //  VATInManualFaktur.InvoiceNumberFull = manualfileFaktur.DESCRIPTION;
            //  VATInManualFaktur.InvoiceDate = dtEnomTaxInvoiceDate.Value.ToString().FormatSQLDate();
            VATInManualFaktur.SupplierNPWP = txtEnomSupplierNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.SupplierName = txtEnomSupplierName.Value.ToString();
            VATInManualFaktur.SupplierAddress = txtEnomSupplierAddress.Value.ToString();
            VATInManualFaktur.NPWPLawanTransaksi = txtEnomBuyerNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.VATBaseAmount = Convert.ToDecimal(spnEnomVATAmount.Value.ToString());
            VATInManualFaktur.VATAmount = Convert.ToDecimal(spnEnomVATBaseAmount.Value.ToString());
            VATInManualFaktur.JumlahPPnBM = Convert.ToDecimal(spnEnomTotalPPnBM.Value.ToString());
            if (hidEventMode.Value == "NEW")
            {
                try
                {
                    string invno = "";
                    if (fakturtype == "Non eFaktur")
                    {
                        invno = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                        VATInManualFaktur.InvoiceNumberFull = invno;
                        VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                        VATInManualFaktur.FGPengganti = "0";
                        VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                    }
                    else
                    {
                        invno = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim();
                        VATInManualFaktur.InvoiceNumberFull = invno;
                        VATInManualFaktur.KDJenisTransaksi = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(0, 2);
                        VATInManualFaktur.FGPengganti = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(2, 1);
                        VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().Trim();
                        VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                    }
                    AttachmentFakturPajak manualfileFaktur = new AttachmentFakturPajak();
                    manualfileFaktur.REFERENCE_NO = _refno;
                    manualfileFaktur.DIRECTORY = "";
                    manualfileFaktur.FILE_NAME = "Manual";
                    manualfileFaktur.DESCRIPTION = invno;

                    manualfileFaktur.STATUS = "Valid";
                    manualfileFaktur.URL = "";
                    manualfileFaktur.ERR_MESSAGE = "";
                    manualfileFaktur.CREATED_BY = def.UserData.USERNAME;
                    manualfileFaktur.CREATED_DT = DateTime.Now;
                    _id = def.formPersistence.CreateAttachmentFakturPajak(manualfileFaktur);
                    def.formPersistence.GenerateXML(VATInManualFaktur, null, _id);

                    def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak();
                    clearmanualfaktur();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlManualFaktur.ClientVisible = false;

                }
                catch (Exception ex)
                {
                    string err = "";
                    err = ex.Message.Trim();
                    def.formPersistence.DeleteFakturPajak(_id);
                    def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                }
            }
            else
            {
                try
                {
                    Int64 _idEdit = Convert.ToInt64(hidEventModeId.Value);
                    VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                   
                    def.formPersistence.UpdateManual(VATInManualFaktur, _idEdit);

                    def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak();
                    clearmanualfaktur();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlManualFaktur.ClientVisible = false;

                }
                catch (Exception ex)
                {
                    string err = "";
                    err = ex.Message.Trim();
                    def.formPersistence.DeleteFakturPajak(_id);
                    def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                }
            }

        }

        public void clearmanualfaktur()
        {
            cmbEnomFakturType.Value = "eFaktur";
            txtEnomTaxInvoiceNumbereFaktur.Value = "";
            txtEnomTaxInvoiceNumberNoneFaktur.Value = "";
            dtEnomTaxInvoiceDate.Value = "";
            txtEnomSupplierNPWP.Value = "";
            txtEnomSupplierName.Value = "";
            txtEnomSupplierAddress.Value = "";
            txtEnomBuyerNPWP.Value = "";
            spnEnomVATAmount.Value = "0";
            spnEnomVATBaseAmount.Value = "0";
            spnEnomTotalPPnBM.Value = "0";
        }

        public void Viewdatamanual(Int64 _id)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            VATInManualInputView _VATInManualFakturView = new VATInManualInputView();
            _VATInManualFakturView= def.formPersistence.GetManualFakturPajak(_id);
            cmbEnomFakturType.Value = _VATInManualFakturView.FakturType;
            if (_VATInManualFakturView.FakturType == "eFaktur")
            {
                txtEnomTaxInvoiceNumbereFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = true;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = false;
            }
            else
            {
                txtEnomTaxInvoiceNumberNoneFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = false;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = true;
            }
            dtEnomTaxInvoiceDate.Date = Convert.ToDateTime(_VATInManualFakturView.InvoiceDate);
            txtEnomSupplierNPWP.Value = _VATInManualFakturView.SupplierNPWP;
            txtEnomSupplierName.Value = _VATInManualFakturView.SupplierName;
            txtEnomSupplierAddress.Value = _VATInManualFakturView.SupplierAddress;
            txtEnomBuyerNPWP.Value = _VATInManualFakturView.NPWPLawanTransaksi;
            spnEnomVATAmount.Value = _VATInManualFakturView.VATAmount;
            spnEnomVATBaseAmount.Value = _VATInManualFakturView.VATBaseAmount;
            spnEnomTotalPPnBM.Value = _VATInManualFakturView.JumlahPPnBM;
            EnabledManualFaktur();
        }

        public void EventMode(string _em)
        {
            hidEventMode.Value = _em;
        }

        public void ViewdatamanualEdit(Int64 _id)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            VATInManualInputView _VATInManualFakturView = new VATInManualInputView();
            hidEventModeId.Value = _id.ToString();
            _VATInManualFakturView = def.formPersistence.GetManualFakturPajak(_id);
            cmbEnomFakturType.Value = _VATInManualFakturView.FakturType;
            if (_VATInManualFakturView.FakturType == "eFaktur")
            {
                txtEnomTaxInvoiceNumbereFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = true;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = false;
            }
            else
            {
                txtEnomTaxInvoiceNumberNoneFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = false;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = true;
            }
            dtEnomTaxInvoiceDate.Date = Convert.ToDateTime(_VATInManualFakturView.InvoiceDate);
            txtEnomSupplierNPWP.Value = _VATInManualFakturView.SupplierNPWP;
            txtEnomSupplierName.Value = _VATInManualFakturView.SupplierName;
            txtEnomSupplierAddress.Value = _VATInManualFakturView.SupplierAddress;
            txtEnomBuyerNPWP.Value = _VATInManualFakturView.NPWPLawanTransaksi;
            spnEnomVATAmount.Value = _VATInManualFakturView.VATAmount;
            spnEnomVATBaseAmount.Value = _VATInManualFakturView.VATBaseAmount;
            spnEnomTotalPPnBM.Value = _VATInManualFakturView.JumlahPPnBM;
            DisabledManualFaktur();
        }

        public void EnabledManualFaktur()
        {
            cmbEnomFakturType.Enabled=false;
            txtEnomTaxInvoiceNumbereFaktur.Enabled = false;
            txtEnomTaxInvoiceNumberNoneFaktur.Enabled = false;
            dtEnomTaxInvoiceDate.Enabled = false;
            txtEnomSupplierNPWP.Enabled = false;
            txtEnomSupplierName.Enabled = false;
            txtEnomSupplierAddress.Enabled = false;
            txtEnomBuyerNPWP.Enabled = false;
            spnEnomVATAmount.Enabled = false;
            spnEnomVATBaseAmount.Enabled = false;
            spnEnomTotalPPnBM.Enabled = false;
            btnSubmitManualFaktur.Visible = false;

        }

        public void DisabledManualFaktur()
        {
            cmbEnomFakturType.Enabled = true;
            txtEnomTaxInvoiceNumbereFaktur.Enabled = true;
            txtEnomTaxInvoiceNumberNoneFaktur.Enabled = true;
            dtEnomTaxInvoiceDate.Enabled = true;
            txtEnomSupplierNPWP.Enabled = true;
            txtEnomSupplierName.Enabled = true;
            txtEnomSupplierAddress.Enabled = true;
            txtEnomBuyerNPWP.Enabled = true;
            spnEnomVATAmount.Enabled = true;
            spnEnomVATBaseAmount.Enabled = true;
            spnEnomTotalPPnBM.Enabled = true;
            btnSubmitManualFaktur.Visible = true;

        }

        protected void evt_Cancel_ManualFaktur(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            clearmanualfaktur();
            def.PnlMasterHeader.ClientVisible = true;
            def.PnlManualFaktur.ClientVisible = false;

        }


        //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}