﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAttachmentFakturPajak.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcAttachmentFakturPajak" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>


<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<style type="text/css">
    .auto-style2 {
        width: 267px;
    }
     

  

</style>
<%--<script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>--%>
<script type="text/javascript">
    function UploadAttachmentFakturPajak_Click(s, e) {

        var fn = $("#uploadAttachmentFakturPajak").val();
        if (fn == "" || fn == null) {
            alert("Browse file to Upload");
            return false;
        }
        if (fn.indexOf("&") >= 0 || fn.indexOf("+") >= 0 || fn.indexOf("%") >= 0 || fn.indexOf("?") >= 0 || fn.indexOf("*") >= 0) {
            return confirm("File name contains banned & + % * ? chars \r\n Rename, then press Cancel, select other(renamed) file\r\n or Proceed Upload (replacing banned chars with '_' ) ?");
        }
        return true;
    }


</script>


<%-- Loading Panel is used in this sample to visualize uploads performed via postbacks --%>
<dx:ASPxLoadingPanel ID="ASPxLoadingPanel1" runat="server"
    ContainerElementID="ASPxUploadControl1" ClientInstanceName="loadingPanel">
</dx:ASPxLoadingPanel>
<asp:UpdatePanel runat="server" ID="pnupdateAttachmentFakturPajak">
    <ContentTemplate>

        <asp:UpdatePanel runat="server" ID="pnupdateFileUploadFakturPajak">
            <Triggers>
                <asp:PostBackTrigger ControlID="btAttFileFaktur" />
                <asp:PostBackTrigger ControlID="btManualFaktur" />
              
                
            </Triggers>
            <ContentTemplate>
                <table>
                    <tr>
                        <td align="left" valign="baseline">

                            <dx:ASPxButton  runat="server" ID="btAttFileFaktur"
                                Text="Add Tax Invoice File"  OnClick="evt_btAttFileFaktur_Click" BackColor="#0066FF" Font-Bold="True" ForeColor="White">
                               <%-- <ClientSideEvents Click="function(s,e) { loading(); }" />--%>
                            </dx:ASPxButton>
                        </td>
                        <td align="right" valign="baseline">
                            <dx:ASPxButton runat="server"  ID="btManualFaktur"
                                Text="Add Manual Tax Invoice" OnClick="evt_btManualFaktur_Click" BackColor="#0066FF" Font-Bold="True" ForeColor="White">
                                <ClientSideEvents Click="function(s,e) { loading(); }" />
                                <%--  <ClientSideEvents Click="function(s,e) {UploadAttachmentEnom_Click(); loading(); }" />--%>
                            </dx:ASPxButton>
                        </td>
                    </tr>

                    <tr>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                    </tr>

                </table>

            </ContentTemplate>
        </asp:UpdatePanel>
       
      
        <asp:HiddenField runat="server" ID="hidReferenceNoFakturPajak" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidUserNameFakturPajak" ClientIDMode="Static" />
      
        <asp:HiddenField runat="server" ID="hidformtypeFakturPajak" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidScreenTypeFakturPajak" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidthnFakturPajak" ClientIDMode="Static" />
        <dx:ASPxGridView runat="server" ID="gridAttachmentFakturPajak" Width="340px" ClientInstanceName="gridAttachmentFakturPajak"
            ClientIDMode="Static" KeyFieldName="ID" EnableCallBacks="False" AutoGenerateColumns="False"
            SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
            SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
            OnHtmlRowCreated="evt_gridAttachmentFakturPajak_onHtmlRowCreated"
            SettingsBehavior-AllowDragDrop="False">
            <Styles>
                <AlternatingRow Enabled="True" />
                 <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                </Header>
            </Styles>
            <Columns>
                <dx:GridViewDataTextColumn FieldName="REF_SEQ_NO" Caption="No" Width="30px" Visible="false">
                    <CellStyle HorizontalAlign="Center" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lblREF_SEQ_NO" runat="server" Text='<%# Bind("REF_SEQ_NO") %>' />

                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="Sequence_number" Caption="No" Width="30px">
                    <CellStyle HorizontalAlign="Center" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lblSeq_no" runat="server" Text='<%# Bind("Sequence_number") %>' />

                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="FILE_NAME" Caption="Tax Invoice File" Width="170px">
                    <CellStyle HorizontalAlign="Left" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lblManualFileFaktur" runat="server" Text='<%# Bind("FILE_NAME") %>' />
                        <common:BlankTargetedHyperlink runat="server" ID="lblFILE_NAME" Text='<%# Bind("FILE_NAME") %>'
                            NavigateUrl='<%# Bind("URL_DIRECTORY") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="DESCRIPTION" Caption="Tax Invoice Number" Width="140px">
                    <DataItemTemplate>
                        <common:BlankTargetedHyperlink runat="server" ID="lblDESCRIPTION" Text='<%# Bind("DESCRIPTION") %>'
                            NavigateUrl='<%# Bind("URL") %>' />

                        <asp:LinkButton
                            OnClick="ViewManualFaktur"
                             ID="linkDESCRIPTION"
                            OnClientClick="loading();"
                            CommandArgument='<%# Container.VisibleIndex %>' 
                            runat="server" Text='<%# Bind("DESCRIPTION") %>'
                            >
                           
                        </asp:LinkButton>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="STATUS" Caption="Status" Width="65px">
                    <CellStyle HorizontalAlign="Center" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lblSTATUS" runat="server" Text='<%# Bind("STATUS") %>' />
                          <asp:LinkButton
                            OnClick="ViewManualFakturStatus"
                             ID="linkSTATUS"
                              OnClientClick="loading();"
                            CommandArgument='<%# Container.VisibleIndex %>' 
                            runat="server" Text='<%# Bind("STATUS") %>'
                            >
                               
                        </asp:LinkButton>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn FieldName="#" Width="60px" CellStyle-VerticalAlign="Middle">
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <DataItemTemplate>
                              <asp:UpdatePanel runat="server" ID="gridupdatepajak">
            <Triggers>
                <asp:PostBackTrigger ControlID="imgRefreshAttachmentEnomFakturPajak" />
                <asp:PostBackTrigger ControlID="imgEditAttachmentEnomFakturPajak" />
              
                 <asp:PostBackTrigger ControlID="imgDeleteAttachmentEnomFakturPajak" />
            </Triggers>
            <ContentTemplate> 

                  <table>

                                <tr>
                                    <td style="margin-right: 10px;">
                                         <common:KeyImageButton runat="server" ID="imgRefreshAttachmentEnomFakturPajak" Key='<%# Bind("ID") %>'
                            ImageUrl="~/App_Themes/BMS_Theme/Images/refresh-button.png" OnClick="evt_imgRefreshAAttachmentEnomFakturPajak_onClick" OnClientClick="loading();" ></common:KeyImageButton>
                                        <common:KeyImageButton runat="server" ID="imgEditAttachmentEnomFakturPajak" Key='<%# Bind("ID") %>'
                            ImageUrl="~/App_Themes/BMS_Theme/Images/b_edit.png" OnClick="evt_imgEditAttachmentEnomFakturPajak_onClick" OnClientClick="loading();"></common:KeyImageButton>
                                    </td>
                                    <td style="margin-right: 10px;">
                                        <common:KeyImageButton runat="server" ID="imgDeleteAttachmentEnomFakturPajak" Key='<%# Bind("ID") %>'
                            ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png" OnClick="evt_imgDeleteAttachmentEnomFakturPajak_onClick" OnClientClick="loading();" />
                                    </td>
                                </tr>
                      </table>
                       
                        
                     
                        
                </ContentTemplate>
                                  </asp:UpdatePanel>
                    </DataItemTemplate>
                
                </dx:GridViewDataColumn>
            </Columns>
            <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
            <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />
            <Settings VerticalScrollableHeight="150" VerticalScrollBarMode="Visible" />
            <Settings HorizontalScrollBarMode="Auto" />
        </dx:ASPxGridView>


        <dx:ASPxPopupControl ID="popUploadFileFakturPajak" ClientInstanceName="popUploadFileFakturPajak" runat="server"
            Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            Width="300px" EnableAnimation="false" HeaderText="Upload File" AllowDragging="false"
            AllowResize="false">
            <ContentCollection>
                <dx:PopupControlContentControl ID="popUploadFileFakturPajakContent" runat="server">
                    <asp:UpdatePanel runat="server" ID="pnupdateFileUploadPopupFakturPajak">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btSubmitFileFakturPajak" />
                        </Triggers>
                        <ContentTemplate>
                            <table>

                                <tr>
                                    <td style="margin-right: 10px;">&nbsp;File
                                    </td>
                                    <td>
                                        <asp:FileUpload runat="server" ID="uploadAttachmentFakturPajak" ClientIDMode="Static" AllowMultiple="true" />
                                        <asp:Label ID="listofuploadedfiles" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td align="right" valign="baseline">

                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btSubmitFileFakturPajak"
                                            Text="Submit" OnClick="evt_btSubmitFileFaktur_Click">
                                            <ClientSideEvents Click="function(s,e) {UploadAttachmentFakturPajak_Click(); loading(); }" />
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btCancelFileFakturPajak"
                                            Text="Cancel">
                                            <ClientSideEvents Click="function(s,e) { popUploadFileFakturPajak.Hide(); }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>

        <dx:ASPxPopupControl ID="popErrMsg" ClientInstanceName="popErrMsg" runat="server"
            Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            Width="450px" EnableAnimation="false" HeaderText="Info" AllowDragging="false"
            AllowResize="false">
            <ContentCollection>
                <dx:PopupControlContentControl ID="popErrMsgControl" runat="server">
                 
                    <table>
                          <tr>
                            <td></td>
                            <td>

                              <asp:TextBox id="txtErrMsg" TextMode="multiline" Columns="50" Rows="5" runat="server" Enabled="false" />
                            
                            </td>
                        </tr>
                          <tr>
                            <td></td>
                            <td>

                             
                            
                            </td>
                        </tr>
                            <tr>
                            <td></td>
                            <td>

                             
                            
                            </td>
                        </tr>
                       
                        <tr>
                            <td style="margin-right: 10px;"></td>
                            <td align="right" valign="baseline">

                             
                                <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btnCancelpopErrMsg"
                                    Text="Cancel" AutoPostBack="False">
                                    <ClientSideEvents Click="function(s,e) { popErrMsg.Hide(); }" />
                                </dx:ASPxButton>
                            </td>
                        </tr>
                    </table>



                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </ContentTemplate>
</asp:UpdatePanel>

