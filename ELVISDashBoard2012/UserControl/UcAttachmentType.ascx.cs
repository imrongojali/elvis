﻿using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Control;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{

    public partial class UcAttachmentType : System.Web.UI.UserControl
    {

        //public static string hidformtypeAtt.Value;
        //public static string hidCategoryCodeAtt.Value;
        //public static string hidReferenceNoAtt.Value;
        //public static string hidUserNameAtt.Value;
        //public static int? hidVendorGroupAtt.Value;
        //public static string hidScreenTypeAtt.Value;
        //public static string hidthnAtt.Value;
        protected readonly FormPersistence formPersistence = new FormPersistence();
        protected LogicFactory logic = new LogicFactory();
        protected FtpLogic ftp = new FtpLogic();
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        public void ShowLoadAttachmentCategoryNominative(string _formType,
            string _AttCategoryCode,
            string _AttReferenceNoDataField,
            string _userName,
            string _thn,
            string _screenType, int? _AttVendorGroupCode = 0)
        {

            hidformtypeAtt.Value = _formType;
            hidCategoryCodeAtt.Value = _AttCategoryCode;
            hidReferenceNoAtt.Value = _AttReferenceNoDataField;
            if (_AttVendorGroupCode==null)
            { hidVendorGroupAtt.Value = "0"; }
            else { hidVendorGroupAtt.Value = Convert.ToString(_AttVendorGroupCode);  }
            
            hidUserNameAtt.Value = _userName;
            hidScreenTypeAtt.Value = _screenType;
            hidthnAtt.Value = _thn;


            if (hidformtypeAtt.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;

                def.showRefData();
            }
            else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;

                defsettAccrfrom.showRefData();
            }


            //if (hidCategoryCodeAtt.Value != "")
            //{
                List<AttachmentCategoryNominative> _list = new List<AttachmentCategoryNominative>();


                _list = formPersistence.GetAttachmentCategoryNominative(hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidformtypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value));

                gridAttachmentEnom.DataSource = _list;

                gridAttachmentEnom.DataBind();
          //  }



        }

        public void IsVisibeleAttachment(bool IsVisible)
        {

            gridAttachmentEnom.Columns["#"].Visible = IsVisible;
        }
        protected void evt_cboxAttachmentCategoryEnom_onLoad(object sender, EventArgs args)
        {

            List<CodeConstant> a = formPersistence.getAttachmentCategories();
            cboxAttachmentCategoryEnom.DataSource = a;
            cboxAttachmentCategoryEnom.DataBind();
        }

        public void gridvalidation()
        {
            gridAttachmentEnom.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
        }
        public void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {


            ASPxLabel hidValidateVendorGroup = gridAttachmentEnom.FindRowCellTemplateControl(e.VisibleIndex, gridAttachmentEnom.Columns["CategoryName"] as GridViewDataColumn, "hidValidateVendorGroup") as ASPxLabel;

            if (Convert.ToBoolean(hidValidateVendorGroup.Value) == true)
            {
                if (e.DataColumn.FieldName == "SequenceNumber")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                }
                if (e.DataColumn.FieldName == "CategoryName")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                }
                if (e.DataColumn.FieldName == "FileName")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                }




            }

        }
        protected void evt_imgAddAttachmentEnom_onClick(object sender, EventArgs arg)
        {
            //   var def = this.Page as _30PvFormList.VoucherFormPage;
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            int keyNum = int.Parse(key);
            AttachmentCategoryNominative _itemAtt = new AttachmentCategoryNominative();

            _itemAtt = formPersistence.GetAttachmentCategoryNominative(hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidformtypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value)).Where(x => x.SequenceNumber == keyNum).SingleOrDefault();
            if (_itemAtt.IS_ENOMINATIVE)
            {
                lblIsnominative.Value = "1";
                lblCategoryName.Value = _itemAtt.CategoryName;
                lblAttId.Value = _itemAtt.ATTACH_CD;
                lblCategoryName.ClientVisible = true;
                cboxAttachmentCategoryEnom.ClientVisible = false;
                lblValidateFileType.Value = _itemAtt.ValidateFileType;
            }
            else
            {
                lblIsnominative.Value = "0";
                lblCategoryName.Value = "";
                lblAttId.Value = "";
                lblValidateFileType.Value = "";
                lblCategoryName.ClientVisible = false;
                cboxAttachmentCategoryEnom.ClientVisible = true;
            }
            popUploadAttEnom.ShowOnPageLoad = true;

            // AttachmentPopup.Show();
        }

        protected void evt_gridAttachmentEnom_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {

            try
            {


                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                gridAttachmentEnom = (ASPxGridView)sender;

                if (arg.RowType != GridViewRowType.Data)
                {
                    return;
                }

                int key = (int)arg.KeyValue;
                if (key > 0)
                {

                    AttachmentCategoryNominative _itemAtt = new AttachmentCategoryNominative();
                    _itemAtt = formPersistence.GetAttachmentCategoryNominative(hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidformtypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value)).Where(x => x.SequenceNumber == key).SingleOrDefault();

                    KeyImageButton imgAddEnom = (KeyImageButton)gridAttachmentEnom.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgAddAttachmentEnom");
                    if (imgAddEnom != null)
                    {
                        if (_itemAtt != null)
                        {
                            if (Convert.ToString(_itemAtt.ATTACH_CD) == "0")
                            {
                                imgAddEnom.Visible = false;
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(_itemAtt.FileName))
                                {
                                    imgAddEnom.Visible = true;
                                }
                                else
                                {
                                    imgAddEnom.Visible = false;
                                }
                            }
                        }
                    }

                    KeyImageButton imgDeleteEnom = (KeyImageButton)gridAttachmentEnom.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachmentEnom");
                    if (imgDeleteEnom != null)
                    {
                        if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                        {
                            int statusCd = def.FormData.StatusCode ?? 0;
                            bool isPV = def.ScreenType == SCREEN_TYPE_PV;
                            bool canDelAttach = statusCd == 0
                                              || statusCd == 31
                                              || (
                                                  (statusCd > 0 && def.FormData.InWorklist) &&
                                                  !(
                                                        ((statusCd > 20 && statusCd < 30) && isPV)
                                                     || ((statusCd > 60 && statusCd < 70) && !isPV)
                                                  )
                                                  );
                            if (_itemAtt != null)
                            {
                                if (string.IsNullOrEmpty(_itemAtt.FileName) || !canDelAttach)
                                {
                                    imgDeleteEnom.CssClass = "hidden";
                                }
                                else
                                {
                                    imgDeleteEnom.CssClass = "poited";
                                }
                                if (Convert.ToString(_itemAtt.ATTACH_CD) == "0")
                                {
                                    imgDeleteEnom.CssClass = "hidden";
                                }
                            }
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                        {
                            int statusCd = defsettAccrfrom.FormData.StatusCode ?? 0;
                            bool isPV = defsettAccrfrom.ScreenType == SCREEN_TYPE_PV;
                            bool canDelAttach = statusCd == 0
                                              || statusCd == 31
                                              || (
                                                  (statusCd > 0 && def.FormData.InWorklist) &&
                                                  !(
                                                        ((statusCd > 20 && statusCd < 30) && isPV)
                                                     || ((statusCd > 60 && statusCd < 70) && !isPV)
                                                  )
                                                  );
                            if (_itemAtt != null)
                            {
                                if (string.IsNullOrEmpty(_itemAtt.FileName) || !canDelAttach)
                                {
                                    imgDeleteEnom.CssClass = "hidden";
                                }
                                else
                                {
                                    imgDeleteEnom.CssClass = "poited";
                                }
                                if (Convert.ToString(_itemAtt.ATTACH_CD) == "0")
                                {
                                    imgDeleteEnom.CssClass = "hidden";
                                }
                            }
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                        {
                            if (_itemAtt.FileName == null || _itemAtt.FileName == "")
                            {
                                imgDeleteEnom.CssClass = "hidden";
                            }
                            else
                            {
                                imgDeleteEnom.CssClass = "poited";
                            }
                            if (Convert.ToString(_itemAtt.ATTACH_CD) == "0")
                            {
                                imgDeleteEnom.CssClass = "hidden";
                            }
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                        {
                            if (_itemAtt.FileName == null || _itemAtt.FileName == "")
                            {
                                imgDeleteEnom.CssClass = "hidden";
                            }
                            else
                            {
                                imgDeleteEnom.CssClass = "poited";
                            }
                            if (Convert.ToString(_itemAtt.ATTACH_CD) == "0")
                            {
                                imgDeleteEnom.CssClass = "hidden";
                            }
                        }


                    }
                }
            }
            catch (Exception)
            {
                KeyImageButton imgDeleteEnomErr = (KeyImageButton)gridAttachmentEnom.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgDeleteAttachmentEnom");
                if (imgDeleteEnomErr != null)
                {
                    imgDeleteEnomErr.CssClass = "hidden";
                }
            }
        }
        protected void evt_imgDeleteAttachmentEnom_onClick(object sender, EventArgs arg)
        {

            //  var def = this.Page as _30PvFormList.VoucherFormPage;
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            decimal keyNum = decimal.Parse(key);
            AttachmentCategoryNominative _itemAtt = new AttachmentCategoryNominative();
            _itemAtt = formPersistence.GetAttachmentCategoryNominative(hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidformtypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value)).Where(x => x.REF_SEQ_NO == keyNum).SingleOrDefault();
            logic.Say("evt_imgDeleteAttachment_onClick", "Key= {0}", keyNum);

            if ((_itemAtt != null))
            {
                ftp.Delete(_itemAtt.PATH, _itemAtt.FileName);
                formPersistence.DeleteAttachmenCategoryNominative(hidReferenceNoAtt.Value, keyNum);
                ShowLoadAttachmentCategoryNominative(hidformtypeAtt.Value, hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidUserNameAtt.Value, hidthnAtt.Value, hidScreenTypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value));
            }
        }
        protected void evt_btSubmitFileEnom_Click(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsett = this.Page as _60SettlementForm.SettlementFormList;
            var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            if (uploadAttachmentEnom.HasFile)
            {

                string filename = CommonFunction.CleanFilename(uploadAttachmentEnom.FileName);
                FileInfo fi = new FileInfo(filename);
                string ext = fi.Extension.Replace(".", "").ToUpper();
                if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                {

                    def.clearScreenMessage();
                }
                else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                {
                    defsettAccrfrom.clearScreenMessage();
                }


                logic.Say("btSendUploadAttachment_Click", "FileName = {0}", uploadAttachmentEnom.FileName);
                string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                int maxSize = int.Parse(strMaxSize);
                maxSize = maxSize * 1024;
                if (uploadAttachmentEnom.FileBytes.Length > maxSize)
                {
                    if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                    {

                        def.clearScreenMessage();
                        def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    }
                    else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                    {
                        defsett.ErrMsg = defsett.Nagging("MSTD00089ERR", (maxSize / 1024).ToString());
                    }
                    else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                    {
                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00089ERR", (maxSize / 1024).ToString());
                    }
                    else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                    {

                        defsettAccrfrom.clearScreenMessage();
                        defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                    }
                    popUploadAttEnom.ShowOnPageLoad = false;
                    return;
                }

                bool uploadSucceded = true;
                string errorMessage = null;
                ListEditItem item = cboxAttachmentCategoryEnom.SelectedItem;
                if (lblIsnominative.Value.ToString() == "1")
                {
                    string validatetype = lblValidateFileType.Value.ToString().Trim().ToUpper();

                    string[] authorsList = validatetype.Split(";".ToCharArray());
                    if (!authorsList.Contains(ext))
                    {
                        if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                        {

                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "File Must Be " + validatetype);
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                        {
                            defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "File Must Be " + validatetype);
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                        {
                            defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "File Must Be " + validatetype);
                        }
                        if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                        {

                            defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "File Must Be " + validatetype);
                        }

                        popUploadAttEnom.ShowOnPageLoad = false;
                        return;
                    }

                }
                else
                {
                    if (item == null)
                    {

                        if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                        {
                            def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                        {
                            defsett.ErrMsg = defsett.Nagging("MSTD00017WRN", "Attachment Category");
                        }
                        else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                        {
                            defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00017WRN", "Attachment Category");
                        }
                        if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                        {
                            defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                        }
                        popUploadAttEnom.ShowOnPageLoad = false;
                        return;
                    }
                    else
                    {

                        if (item.Value == null)
                        {
                            if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                            {
                                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                            }
                            else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00017WRN", "Attachment Category");
                            }
                            else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00017WRN", "Attachment Category");
                            }
                            else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00017WRN", "Attachment Category");
                            }

                            popUploadAttEnom.ShowOnPageLoad = false;
                            return;
                        }
                    }
                }

                //  AttachID a = def.GetAttachID();



                ftp.ftpUploadBytes(hidthnAtt.Value, hidScreenTypeAtt.Value,
                                hidReferenceNoAtt.Value,
                                filename,
                                uploadAttachmentEnom.FileBytes,
                                ref uploadSucceded,
                                ref errorMessage);

                AttachmentCategoryNominative AttEnom = new AttachmentCategoryNominative();
                AttEnom.ReferenceNumber = hidReferenceNoAtt.Value;
                AttEnom.FileName = filename;
                AttEnom.FormType = hidformtypeAtt.Value;
                if (lblIsnominative.Value.ToString() == "1")
                {
                    AttEnom.ATTACH_CD = lblAttId.Value.ToString();
                    AttEnom.Description = lblCategoryName.Value.ToString();
                    AttEnom.CategoryCode = hidCategoryCodeAtt.Value;
                    AttEnom.IS_ENOMINATIVE = true;
                }
                else
                {
                    AttEnom.ATTACH_CD = item.Value.ToString();
                    AttEnom.Description = item.Text;
                    AttEnom.CategoryCode = hidCategoryCodeAtt.Value;
                    AttEnom.IS_ENOMINATIVE = false;
                    if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                    {

                        def.FormData.ReferenceNoDataField = hidReferenceNoAtt.Value;
                        def.FormData.CategoryCode = hidCategoryCodeAtt.Value;
                    }
                    else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                    {

                        defsettAccrfrom.FormData.ReferenceNoDataField = hidReferenceNoAtt.Value;
                        defsettAccrfrom.FormData.CategoryCode = hidCategoryCodeAtt.Value;
                    }



                }

                AttEnom.PATH = hidthnAtt.Value + "/" + hidScreenTypeAtt.Value + "/" + hidReferenceNoAtt.Value;
                AttEnom.FILE_TYPE = ext;

                AttEnom.CreatedBy = hidUserNameAtt.Value;
                formPersistence.AddAttachmenCategoryNominative(AttEnom);
                ShowLoadAttachmentCategoryNominative(hidformtypeAtt.Value, hidCategoryCodeAtt.Value, hidReferenceNoAtt.Value, hidUserNameAtt.Value, hidthnAtt.Value, hidScreenTypeAtt.Value, Convert.ToInt32(hidVendorGroupAtt.Value));
                popUploadAttEnom.ShowOnPageLoad = false;

                //if (item.Value.ToString() == "3")
                //{
                //    string _directoryPath = logic.Settle.GetByConfigKeyAtt("ShareFolder10.85.40.199").ConfigValue;
                //    string _directoryUser = logic.Settle.GetByConfigKeyAtt("ShareFolder10.85.40.199User").ConfigValue;
                //    string _directoryPass = logic.Settle.GetByConfigKeyAtt("ShareFolder10.85.40.199Pass").ConfigValue;
                //    using (new NetworkConnection(_directoryPath, new NetworkCredential(_directoryUser, _directoryPass)))
                //    {
                       
                //        Stream stream = new MemoryStream(uploadAttachmentEnom.FileBytes);
                //        // File.Copy(localPath, _directoryPath);
                //        using (var fileStream = new FileStream(_directoryPath + filename, FileMode.Create, FileAccess.ReadWrite))
                //        {


                //            stream.CopyTo(fileStream);
                //            stream.Dispose();
                //            stream.Close();
                //            fileStream.Dispose();
                //            fileStream.Close();



                //        }

                //    }
                //}

                // gridvalidation();
            }
            else
            {
                if (hidformtypeAtt.Value == "ELVIS_PVFormList")
                {
                    def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", "File Harus di isi");
                }
                else if (hidformtypeAtt.Value == "ELVIS_SettlementFormList")
                {
                    defsett.ErrMsg = defsett.Nagging("MSTD00089ERR", "File Harus di isi");
                }
                else if (hidformtypeAtt.Value == "ELVIS_AccrSettForm")
                {
                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00089ERR", "File Harus di isi");
                }
                else if (hidformtypeAtt.Value == "ELVIS_AccrPVForm")
                {
                    defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", "File Harus di isi");

                }
                popUploadAttEnom.ShowOnPageLoad = false;
                return;

            }

        }

    }
}