﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDataFieldEntertainment.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcDataFieldEntertainment" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v12.2" %>
<script type="text/javascript">
    function OnNewClickEnter(s, e) {
        //ASPxGridViewEntertainment.AddNewRow();
        ASPxGridViewEntertainment.AddNewRow();
    }

    function OnNewDetailClickEnter(s, e) {

        gvDetailEnter.AddNewRow();
    }
    function BeforeUploadEntertainment_Click(s, e) {
        //var fn = $("#fuDataFieldEntertainmentList").val();
        var fn = $("#fuDataFieldEntertainmentList").val();

        if (!fn || fn.length() < 1) {
            alert("Select file");
            return false;
        }
        return true;
    }


    //function OnEditClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.StartEditRow(index);
    //}

    //function OnSaveClick(s, e) {
    //    grid.UpdateEdit();
    //}

    //function OnCancelClick(s, e) {
    //    grid.CancelEdit();
    //}

    //function OnDeleteClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.DeleteRow(index);
    //}  
</script>
<style type="text/css">
    div.inline {
        float: left;
    }

    .clearBoth {
        clear: both;
    }
    
   
    /*  #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 150px;
   }
   #divVendorName
   {
       width: 120px;
   }*/
</style>

<asp:UpdatePanel runat="server" ID="UpdatePanelEntertainment">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSaveEntertainment" />
        <asp:PostBackTrigger ControlID="btnCloseEntertainment" />
        <asp:PostBackTrigger ControlID="btnClearEntertainment" />

        <asp:PostBackTrigger ControlID="btnUploadTemplateEntertainment" />
    </Triggers>
    <ContentTemplate>


        <dx:aspxhyperlink runat="server" id="lnkDownloadTemplate" text="Download Template"
            navigateurl="~/Template/Excel/TemplateDataFieldEntertainment.xls" />
        <br />
        <asp:FileUpload ID="fuDataFieldEntertainmentList" runat="server" ClientIDMode="Static" />
        &nbsp;
        <asp:Button runat="server" ID="btnUploadTemplateEntertainment" Text="Upload" OnClick="btnUploadTemplateEntertainment_Click"
            OnClientClick="return BeforeUploadEntertainment_Click()" />
        <br />
        <div>&nbsp;&nbsp;</div>
        <dx:aspxpanel id="FormPanelEntertainment" runat="server" width="100%" style="border: 1px solid #000;">
        
            <PanelCollection>
                
                <dx:PanelContent runat="server">
                      
                    <div>&nbsp;&nbsp;</div>
                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                        <tr>
                            <td valign="baseline" style="width: 170px">Tahun Pajak
                            </td>
                            <td valign="top" style="width: 100px">
                                <dx:ASPxSpinEdit ID="spnTahunPajakEntertainment" runat="server" Number="0" Width="70" MaxLength="4" ClientInstanceName="spnTahunPajak" OnValidation="TahunPajak_Validation" NumberType="Integer">
                                  <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupEntertainment" SetFocusOnError="True" ErrorTextPosition="Bottom" ErrorText="Name must be at least two characters long">
                            <RequiredField IsRequired="True" ErrorText="Tahun Pajak is required" />
                        </ValidationSettings>
                                </dx:ASPxSpinEdit>
                            </td>

                            <td valign="baseline" style="width: 170px">Tmp. &  Tgl. Penandatangan
                            </td>
                            <td valign="top" style="width: 260px">
                                <div class="inline">
                                    <dx:ASPxTextBox runat="server" ID="txtTempatPenandatanganEntertainmentEntertainment" Width="120px">

                                         <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupEntertainment" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Tmp. is required" />
                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                                <div class="inline" style="width: 10px">&nbsp;&nbsp;</div>

                                <div class="inline">
                                    <dx:ASPxDateEdit ID="dtPenandaTanganEntertainment"  ClientInstanceName="dtPenandaTangan"
                                        ClientIDMode="static" runat="server" Width="120px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table">

                                      
                                         <CalendarProperties>
                                             <FastNavProperties Enabled="False" />
                                         </CalendarProperties>

                                      
                                         <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupEntertainment" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True"  ErrorText="Tanggal is required" />
                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline" style="width: 200px">Nama Direktur
                            </td>
                            <td valign="top" style="width: 200px">
                                <dx:ASPxTextBox runat="server" ID="txtNamaPenandatanganEntertainment" Width="300px">

                                   
                                         <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupEntertainment" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Nama Direktur is required" />
                        </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>

                            <td valign="baseline">Jabatan Penandatangan
                            </td>
                            <td valign="top">
                                <div>
                                    <dx:ASPxTextBox runat="server" ID="txtJabatanpenandatanagnEntertainment" Text="Direktur"   Width="250px">

                                         <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupEntertainment" ErrorTextPosition="Bottom" SetFocusOnError="True" >
                            <RequiredField IsRequired="True" ErrorText="Jabatan Penandatangan is required" />
                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                            </td>
                        </tr>

                    </table>
                    </dx:PanelContent>
                            </PanelCollection>
        </dx:aspxpanel>
        <div>&nbsp;&nbsp;</div>
        <dx:aspxbutton id="btnNewEntertainment" runat="server" text="Add" autopostback="false">
                        <ClientSideEvents Click="function (s, e) { OnNewClickEnter(s, e); }" />
                    </dx:aspxbutton>
        <br />
         <asp:HiddenField runat="server" ID="hidCategoryCodeEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidTransactionCodeEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidReferenceNoEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidUserNameEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorCodeEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorGroupEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidformtypeEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidScreenTypeEntertainment" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidthnEntertainment" ClientIDMode="Static" />
         <asp:HiddenField runat="server" ID="hidIsSaveEntertainment" ClientIDMode="Static" />
        <dx:aspxgridview id="ASPxGridViewEntertainment" runat="server"
            clientinstancename="ASPxGridViewEntertainment"
            autogeneratecolumns="False"
            onrowinserting="ASPxGridViewEntertainment_RowInserting"
            onrowupdating="ASPxGridViewEntertainment_RowUpdating"
            onrowdeleting="ASPxGridViewEntertainment_RowDeleting"
            onrowvalidating="ASPxGridViewEntertainment_RowValidating"

            oninitnewrow="ASPxGridViewEntertainment_InitNewRow"
            onparsevalue="ASPxGridViewEntertainment_ParseValue"
            OnStartRowEditing="ASPxGridViewEntertainment_StartRowEditing"
             OnCancelRowEditing="ASPxGridViewEntertainment_Cancel"
            clientidmode="AutoID" keyfieldname="SEQ_NO" width="100%"
            styles-alternatingrow-cssclass="even">
                        <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <SettingsEditing Mode="Inline" />
                        
                         <Styles>
                            <Header HorizontalAlign="Center"></Header> 
                        </Styles>

                        <Columns>
                            <dx:GridViewCommandColumn VisibleIndex="0" FixedStyle="Left" Width="63px" ButtonType="Image">

                                <EditButton Visible="True">
                                    <Image ToolTip="Edit" Url="~/App_Themes/BMS_Theme/Images/damage.png"></Image>
                                </EditButton>

                                <DeleteButton Visible="True">
                                    <Image ToolTip="Delete" Url="~/App_Themes/BMS_Theme/Images/svn_deleted.png"></Image>
                                </DeleteButton>
                                <CancelButton>
                                    <Image ToolTip="Cancel" Url="~/App_Themes/BMS_Theme/Images/svn_modified.png">
                                    </Image>
                                </CancelButton>
                                <UpdateButton>
                                    <Image ToolTip="Update" Url="~/App_Themes/BMS_Theme/Images/svn_normal.png">
                                    </Image>
                                </UpdateButton>
                               
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>

                                <%--EditCellStyle-HorizontalAlign="Right"--%>

                            </dx:GridViewCommandColumn>
                            <dx:GridViewDataTextColumn FieldName="NO" Caption="No" Width="40px" VisibleIndex="1">
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblSeqNo" Text='<%# Bind("NO")%>'></dx:ASPxLabel>
                                    <asp:HiddenField runat="server" ID="hidSeqNo" Value='<%# Bind("SEQ_NO") %>' />
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNo" Text='<%# Bind("NO") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn FieldName="TANGGAL" Caption="Tanggal Transaksi" Width="120px" VisibleIndex="2">
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxDateEdit ID="dtTanggaltransaksi" Value='<%# Bind("TANGGAL") %>' ClientInstanceName="dtTanggaltransaksi"
                                        ClientIDMode="static" runat="server" Width="112px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblTanggalTransaksi"  Text='<%# Bind("TANGGAL","{0:dd MMM yyyy}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                             <dx:GridViewDataTextColumn FieldName="TEMPAT" Caption="Tempat"  Width="200px" VisibleIndex="3" >
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox ID="txtTempat" runat="server" Width="192px" Value='<%#Bind("TEMPAT") %>' ClientInstanceName="txtAlamat" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                       
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>

                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                  
                                    <dx:ASPxLabel runat="server" ID="lblTempat" Text='<%# Bind("TEMPAT") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="ALAMAT" Caption="Alamat"  Width="200px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox ID="txtAlamat" runat="server" Width="192px" Value='<%#Bind("ALAMAT") %>' ClientInstanceName="txtAlamat">
                                       
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>

                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                  
                                    <dx:ASPxLabel runat="server" ID="lblAlamat" Text='<%# Bind("ALAMAT") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <dx:GridViewDataTextColumn FieldName="TIPE_ENTERTAINMENT" Caption="Bentuk dan Jenis Entertain" Width="200px" VisibleIndex="5">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtTipeEntertainment" Width="192" Value='<%# Bind("TIPE_ENTERTAINMENT") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblTipeEntertainment" Text='<%# Bind("TIPE_ENTERTAINMENT") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                                <dx:GridViewDataTextColumn FieldName="JUMLAH" Caption="Nilai" Width="180px" VisibleIndex="6">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                 
                                        <dx:ASPxSpinEdit ID="txtJumlah" runat="server" DisplayFormatString="Rp. {0:#,##,##.00}" Number="0" Width="172" Value='<%# Bind("JUMLAH") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblJumlah" Text='<%# String.Format("Rp. {0}", Convert.ToDecimal(Eval("JUMLAH")).ToString("#,##,##.00"))%>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            <%--batas--%>
                        </Columns>
                        
                     <SettingsDetail ShowDetailRow="True"  AllowOnlyOneMasterRowExpanded="true"/>
                        <Settings ShowVerticalScrollBar="false"
                           
                            ShowHorizontalScrollBar="True" />
                        <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
                        <Styles>
                            <AlternatingRow CssClass="even">
                            </AlternatingRow>
                        </Styles>
                       

                                           <Templates>
            <DetailRow>
                 
                 
                <dx:ASPxGridView ID="gvDetailEnter" ClientInstanceName="gvDetailEnter" runat="server" Width="1000" 
                   
                     OnInit="gvDetailEnter_Init"
                     AutoGenerateColumns="False"
                        OnRowInserting="gvDetailEnter_RowInserting"
                        OnRowUpdating="gvDetailEnter_RowUpdating"
                        OnRowDeleting="gvDetailEnter_RowDeleting"
                        OnRowValidating="gvDetailEnter_RowValidating"
                        OnStartRowEditing="gvDetailEnter_StartRowEditing"
                        OnCancelRowEditing="gvDetailEnter_Cancel"
                        OnInitNewRow="gvDetailEnter_InitNewRow"
                    Styles-AlternatingRow-CssClass="even"
                    KeyFieldName="SEQ_NO_DETAIL">
                       <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
                        <SettingsPager AlwaysShowPager="True">
                        </SettingsPager>
                        <SettingsEditing   Mode="Inline" />                    
                         <Styles>
                            <Header HorizontalAlign="Center"></Header> 
                        </Styles>

                    <Columns>
                         <dx:GridViewCommandColumn VisibleIndex="0" FixedStyle="Left" Width="63px" ButtonType="Image">

                                <EditButton Visible="True">
                                    <Image ToolTip="Edit" Url="~/App_Themes/BMS_Theme/Images/damage.png"></Image>
                                </EditButton>

                                <DeleteButton Visible="True">
                                    <Image ToolTip="Delete" Url="~/App_Themes/BMS_Theme/Images/svn_deleted.png"></Image>
                                </DeleteButton>
                                <CancelButton>
                                    <Image ToolTip="Cancel" Url="~/App_Themes/BMS_Theme/Images/svn_modified.png">
                                    </Image>
                                </CancelButton>
                                <UpdateButton>
                                    <Image ToolTip="Update" Url="~/App_Themes/BMS_Theme/Images/svn_normal.png">
                                    </Image>
                                </UpdateButton>
                              <HeaderTemplate><dx:ASPxButton ID="btnNewEntertainment" runat="server" Text="ADD" AutoPostBack="false">
                        <ClientSideEvents Click="function (s, e) { OnNewDetailClickEnter(s, e ); }" />
                    </dx:ASPxButton></HeaderTemplate>
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                </CellStyle>

                         </dx:GridViewCommandColumn>
                           <dx:GridViewDataTextColumn FieldName="NAMA_RELASI" Caption="Nama Relasi" Width="310px" VisibleIndex="1">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNamaRelasi" Width="302px" Value='<%# Bind("NAMA_RELASI") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblNamaRelasi" Text='<%# Bind("NAMA_RELASI") %>' />
                        
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                             <dx:GridViewDataTextColumn FieldName="JABATAN" Caption="Jabatan" Width="310px" VisibleIndex="2">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtJabatan" Width="302px" Value='<%# Bind("JABATAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblJabatan" Text='<%# Bind("JABATAN") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                            
                             <dx:GridViewDataTextColumn FieldName="ALAMAT_PERUSAHAAN" Caption="Alamat Perusahaan" Width="310px" VisibleIndex="3">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtAlamatPerusahaan" Width="302px" Value='<%# Bind("ALAMAT_PERUSAHAAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblAlamatPerusahaan" Text='<%# Bind("ALAMAT_PERUSAHAAN") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                               <dx:GridViewDataTextColumn FieldName="TIPE_BISNIS" Caption="Tipe Bisnis" Width="310px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtTipeBisnis" Width="302px" Value='<%# Bind("TIPE_BISNIS") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblTipeBisnis" Text='<%# Bind("TIPE_BISNIS") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>

                              <dx:GridViewDataTextColumn FieldName="KETERANGAN" Caption="Keterangan" Width="310px" VisibleIndex="5">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtKeterangan" Width="302px" Value='<%# Bind("KETERANGAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblKeterangan" Text='<%# Bind("KETERANGAN") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>
                         


                       
                    </Columns>
                    <Settings ShowVerticalScrollBar="false"
                            VerticalScrollableHeight="250"
                            ShowHorizontalScrollBar="True" />
                        <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
                        <Styles>
                            <AlternatingRow CssClass="even">
                            </AlternatingRow>
                        </Styles>
                </dx:ASPxGridView>
            </DetailRow>
        </Templates>
                            
                    </dx:aspxgridview>
        <br />
        <br />
        <%--<SettingsLoadingPanel ImagePosition="left" Mode="ShowAsPopup" />
            <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />--%>
        <%--<div class="inline">--%>

        <div style="position: relative; float: right">
            <table>
                <tr>
                 <td>
                    <dx:ASPxButton ID="btnClearEntertainment" runat="server" Text="Clear" OnClick="evt_Clear_Entertainment">
                        <ClientSideEvents Click="function(s,e) { loading(); ASPxClientEdit.ClearGroup('GroupEntertainment'); }" />
                    </dx:ASPxButton>
                </td>
                       <td>
                      <dx:ASPxButton ID="btnCloseEntertainment" runat="server" Text="Cancel" OnClick="evt_Close_Entertainment" >
                        <ClientSideEvents Click="function(s,e) { loading(); ASPxClientEdit.ClearGroup('GroupEntertainment'); }" />
                    </dx:ASPxButton>
                </td>
                      <td>
                        <dx:aspxbutton id="btnSaveEntertainment" runat="server" text="Save" onclick="evt_Save_Entertainment" validationgroup="GroupEntertainment" >
                      <ClientSideEvents Click="function(s,e) { loading(); }" />
                  </dx:aspxbutton>
                    </td>


                </tr>
            </table>
        </div>
     
    </ContentTemplate>

</asp:UpdatePanel>


