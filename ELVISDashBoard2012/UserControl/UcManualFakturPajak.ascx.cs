﻿using BusinessLogic.VoucherForm;
using Common.Data;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{
    public partial class UcManualFakturPajak : System.Web.UI.UserControl
    {
        protected readonly FormPersistence formPersistence = new FormPersistence();

        protected void Page_Load(object sender, EventArgs e)
        {
           // this.InitializeCulture();


          
            spnEnomVATAmount.IsValid = true;
            spnEnomVATBaseAmount.IsValid = true;
            spnEnomTotalPPnBM.IsValid = true;
        }

        protected void InitializeCulture()
        {
            
            CultureInfo newCulture = new System.Globalization.CultureInfo("id-ID");
            newCulture.NumberFormat.CurrencySymbol = "Rp. ";
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
        }
        protected void evt_Submit_ManualFaktur(object sender, EventArgs arg)
        {
            Int64 _id = 0;
           
            if (hidFormType.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.FormData.ReferenceNoDataField = hidReffNo.Value;
            }
            else if (hidFormType.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.FormData.ReferenceNoDataField = hidReffNo.Value;
            }

            string fakturtype = cmbEnomFakturType.Value.ToString();

           


            VATInManualInputView VATInManualFaktur = new VATInManualInputView();
            VATInManualFaktur.FakturType = fakturtype;
            //  VATInManualFaktur.InvoiceNumberFull = manualfileFaktur.DESCRIPTION;
            //  VATInManualFaktur.InvoiceDate = dtEnomTaxInvoiceDate.Value.ToString().FormatSQLDate();
            VATInManualFaktur.SupplierNPWP = txtEnomSupplierNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.SupplierName = txtEnomSupplierName.Value.ToString();
            VATInManualFaktur.SupplierAddress = txtEnomSupplierAddress.Value.ToString();
            VATInManualFaktur.NPWPLawanTransaksi = txtEnomBuyerNPWP.Value.ToString().FormatNPWP();
            VATInManualFaktur.VATBaseAmount = spnEnomVATBaseAmount.Number; 
            VATInManualFaktur.VATAmount = spnEnomVATAmount.Number; 
            VATInManualFaktur.JumlahPPnBM = spnEnomTotalPPnBM.Number;
            if (hidEventMode.Value == "NEW")
            {
                try
                {
                    string invno = "";
                    if (fakturtype == "Non eFaktur")
                    {
                        invno = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                        VATInManualFaktur.InvoiceNumberFull = invno;
                        VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumberNoneFaktur.Value.ToString().Trim();
                        VATInManualFaktur.FGPengganti = "0";
                        VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                    }
                    else
                    {
                        invno = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim();
                        VATInManualFaktur.InvoiceNumberFull = invno;
                        VATInManualFaktur.KDJenisTransaksi = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(0, 2);
                        VATInManualFaktur.FGPengganti = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().FormatNomorManualFakturGabungan().Trim().Substring(2, 1);
                        VATInManualFaktur.InvoiceNumber = txtEnomTaxInvoiceNumbereFaktur.Value.ToString().Trim().FormatNomorFakturPolos();
                        VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                    }
                    AttachmentFakturPajak manualfileFaktur = new AttachmentFakturPajak();
                    manualfileFaktur.REFERENCE_NO = hidReffNo.Value;
                    manualfileFaktur.DIRECTORY = "";
                    manualfileFaktur.FILE_NAME = "Manual";
                    manualfileFaktur.DESCRIPTION = invno;

                    manualfileFaktur.STATUS = "Valid";
                    manualfileFaktur.URL = "";
                    manualfileFaktur.ERR_MESSAGE = "";
                    manualfileFaktur.FormType = hidFormType.Value;
                    manualfileFaktur.CREATED_BY = hidusername.Value;
                    manualfileFaktur.CREATED_DT = DateTime.Now;
                    _id = formPersistence.CreateAttachmentFakturPajak(manualfileFaktur);
                    formPersistence.GenerateXML(VATInManualFaktur, null, _id);

                    if (hidFormType.Value == "ELVIS_PVFormList")
                    {
                        var def = this.Page as _30PvFormList.VoucherFormPage;
                        //def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00011INF");
                        def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak(hidFormType.Value, hidReffNo.Value, hidthn.Value, hidscreenType.Value, hidusername.Value);

                        def.PnlMasterHeader.ClientVisible = true;
                        def.PnlManualFaktur.ClientVisible = false;
                        
                        def.clearScreenMessage();
                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00251INF");
                    }
                    else if (hidFormType.Value == "ELVIS_AccrPVForm")
                    {
                        var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                        
                        defsettAccrfrom.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak(hidFormType.Value, hidReffNo.Value, hidthn.Value, hidscreenType.Value, hidusername.Value);

                        defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                        defsettAccrfrom.PnlManualFaktur.ClientVisible = false;
                        
                        defsettAccrfrom.clearScreenMessage();
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00251INF");
                    }
                   
                    clearmanualfaktur();

                }
                catch (Exception ex)
                {
                    string err = "";
                    err = ex.Message.Trim();
                    formPersistence.DeleteFakturPajak(_id);
                    if (hidFormType.Value == "ELVIS_PVFormList")
                    {
                        var def = this.Page as _30PvFormList.VoucherFormPage;
                        def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                        return;
                    }
                    else if (hidFormType.Value == "ELVIS_AccrPVForm")
                    {
                        var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                        defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                        return;
                    }
                  
                }
            }
            else
            {
                try
                {
                    Int64 _idEdit = Convert.ToInt64(hidEventModeId.Value);
                    VATInManualFaktur.InvoiceDate = Convert.ToDateTime(dtEnomTaxInvoiceDate.Value.ToString()).FormatSQLDate();
                   
                    formPersistence.UpdateManual(VATInManualFaktur, _idEdit);
                    if (hidFormType.Value == "ELVIS_PVFormList")
                    {
                        var def = this.Page as _30PvFormList.VoucherFormPage;
                        def.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak(hidFormType.Value, hidReffNo.Value, hidthn.Value, hidscreenType.Value, hidusername.Value);

                        def.PnlMasterHeader.ClientVisible = true;
                        def.PnlManualFaktur.ClientVisible = false;
                        def.clearScreenMessage();
                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00252INF");
                    }
                    else if (hidFormType.Value == "ELVIS_AccrPVForm")
                    {
                        var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                        defsettAccrfrom.ucAttachmentFakturPajak.ShowLoadAttachmentFakturPajak(hidFormType.Value, hidReffNo.Value, hidthn.Value, hidscreenType.Value, hidusername.Value);

                        defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                        defsettAccrfrom.PnlManualFaktur.ClientVisible = false;
                        defsettAccrfrom.clearScreenMessage();
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00252INF");
                    }
                   

                    clearmanualfaktur();

                }
                catch (Exception ex)
                {
                    string err = "";
                    err = ex.Message.Trim();
                    formPersistence.DeleteFakturPajak(_id);
                    if (hidFormType.Value == "ELVIS_PVFormList")
                    {
                        var def = this.Page as _30PvFormList.VoucherFormPage;
                        def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                    }
                    else if (hidFormType.Value == "ELVIS_AccrPVForm")
                    {
                        var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                        defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                    }
                }
            }

        }

        public void clearmanualfaktur()
        {
            cmbEnomFakturType.Value = "eFaktur";
            txtEnomTaxInvoiceNumbereFaktur.Value = "";
            txtEnomTaxInvoiceNumberNoneFaktur.Value = "";
            dtEnomTaxInvoiceDate.Value = "";
            txtEnomSupplierNPWP.Value = "";
            txtEnomSupplierName.Value = "";
            txtEnomSupplierAddress.Value = "";
            txtEnomBuyerNPWP.Value = "";
            spnEnomVATAmount.Number = 0;
            spnEnomVATBaseAmount.Number = 0;
            spnEnomTotalPPnBM.Number = 0;
        }

        public void Viewdatamanual(Int64 _id, string formid)
        {
            hidFormType.Value = formid;
            EnabledManualFaktur();
           
            VATInManualInputView _VATInManualFakturView = new VATInManualInputView();
            _VATInManualFakturView= formPersistence.GetManualFakturPajak(_id);
            cmbEnomFakturType.Value = _VATInManualFakturView.FakturType;
            if (_VATInManualFakturView.FakturType == "eFaktur")
            {
                txtEnomTaxInvoiceNumbereFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = true;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = false;
            }
            else
            {
                txtEnomTaxInvoiceNumberNoneFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = false;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = true;
            }
            dtEnomTaxInvoiceDate.Date = Convert.ToDateTime(_VATInManualFakturView.InvoiceDate);
            txtEnomSupplierNPWP.Value = _VATInManualFakturView.SupplierNPWP;
            txtEnomSupplierName.Value = _VATInManualFakturView.SupplierName;
            txtEnomSupplierAddress.Value = _VATInManualFakturView.SupplierAddress;
            txtEnomBuyerNPWP.Value = _VATInManualFakturView.NPWPLawanTransaksi;
            spnEnomVATAmount.Number = Convert.ToDecimal(_VATInManualFakturView.VATAmount);
            spnEnomVATBaseAmount.Number = Convert.ToDecimal(_VATInManualFakturView.VATBaseAmount);
            spnEnomTotalPPnBM.Number = Convert.ToDecimal(_VATInManualFakturView.JumlahPPnBM);
            
        }

        public void EventMode(string _em, string Formtype, string ReffNo, string thn, string screenType, string username)
        {
            hidEventMode.Value = _em;
            hidFormType.Value = Formtype;
            hidReffNo.Value = ReffNo;
            hidthn.Value = thn;
            hidscreenType.Value = screenType;
            hidusername.Value = username;

        }

        public void ViewdatamanualEdit(Int64 _id)
        {
            DisabledManualFaktur();
           

            VATInManualInputView _VATInManualFakturView = new VATInManualInputView();
            hidEventModeId.Value = _id.ToString();
            _VATInManualFakturView = formPersistence.GetManualFakturPajak(_id);
            cmbEnomFakturType.Value = _VATInManualFakturView.FakturType;
            cmbEnomFakturType.Enabled = false;
            if (_VATInManualFakturView.FakturType == "eFaktur")
            {
                txtEnomTaxInvoiceNumbereFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = true;
                txtEnomTaxInvoiceNumbereFaktur.Enabled = false;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = false;
            }
            else
            {
                txtEnomTaxInvoiceNumberNoneFaktur.Value = _VATInManualFakturView.InvoiceNumberFull;
                txtEnomTaxInvoiceNumberNoneFaktur.Enabled = false;
                txtEnomTaxInvoiceNumbereFaktur.ClientVisible = false;
                txtEnomTaxInvoiceNumberNoneFaktur.ClientVisible = true;
            }
            dtEnomTaxInvoiceDate.Date = Convert.ToDateTime(_VATInManualFakturView.InvoiceDate);
            txtEnomSupplierNPWP.Value = _VATInManualFakturView.SupplierNPWP;
            txtEnomSupplierName.Value = _VATInManualFakturView.SupplierName;
            txtEnomSupplierAddress.Value = _VATInManualFakturView.SupplierAddress;
            txtEnomBuyerNPWP.Value = _VATInManualFakturView.NPWPLawanTransaksi;
            spnEnomVATAmount.Number = Convert.ToDecimal(_VATInManualFakturView.VATAmount);
            spnEnomVATBaseAmount.Number = Convert.ToDecimal(_VATInManualFakturView.VATBaseAmount);
            spnEnomTotalPPnBM.Number = Convert.ToDecimal(_VATInManualFakturView.JumlahPPnBM);
            
        }

        public void EnabledManualFaktur()
        {
            cmbEnomFakturType.Enabled=false;
            txtEnomTaxInvoiceNumbereFaktur.Enabled = false;
            txtEnomTaxInvoiceNumberNoneFaktur.Enabled = false;
            dtEnomTaxInvoiceDate.Enabled = false;
            txtEnomSupplierNPWP.Enabled = false;
            txtEnomSupplierName.Enabled = false;
            txtEnomSupplierAddress.Enabled = false;
            txtEnomBuyerNPWP.Enabled = false;
            spnEnomVATAmount.Enabled = false;
            spnEnomVATBaseAmount.Enabled = false;
            spnEnomTotalPPnBM.Enabled = false;
            btnSubmitManualFaktur.Visible = false;

        }

        public void DisabledManualFaktur()
        {
            cmbEnomFakturType.Enabled = true;
            txtEnomTaxInvoiceNumbereFaktur.Enabled = true;
            txtEnomTaxInvoiceNumberNoneFaktur.Enabled = true;
            dtEnomTaxInvoiceDate.Enabled = true;
            txtEnomSupplierNPWP.Enabled = true;
            txtEnomSupplierName.Enabled = true;
            txtEnomSupplierAddress.Enabled = true;
            txtEnomBuyerNPWP.Enabled = true;
            spnEnomVATAmount.Enabled = true;
            spnEnomVATBaseAmount.Enabled = true;
            spnEnomTotalPPnBM.Enabled = true;
            btnSubmitManualFaktur.Visible = true;

        }

        protected void evt_Cancel_ManualFaktur(object sender, EventArgs arg)
        {
           
            clearmanualfaktur();
            spnEnomVATAmount.IsValid = true;
            spnEnomVATBaseAmount.IsValid = true;
            spnEnomTotalPPnBM.IsValid = true;

            if (hidFormType.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
              
                def.PnlMasterHeader.ClientVisible = true;
                def.PnlManualFaktur.ClientVisible = false;
                def.clearScreenMessage();
            }
            else if (hidFormType.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                defsettAccrfrom.PnlManualFaktur.ClientVisible = false;
                defsettAccrfrom.clearScreenMessage();
            }
          
        }


        //protected void evt_ddlTransType_SelectedIndexChanged(object sender, EventArgs e)
        //{

        //}
    }
}