﻿using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Bytescout.BarCodeReader;
using Common.Control;
using Common.Data;
//using Bytescout.BarCodeReader;
using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxUploadControl;
using ELVISDashboard.MasterPage;

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
//using System.Net.Http;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Serialization;
using static ELVISDashBoard._30PvFormList.VoucherFormPage;

namespace ELVISDashBoard.UserControl
{

    public partial class UcAttachmentFakturPajak : System.Web.UI.UserControl
    {
        protected readonly FormPersistence formPersistence = new FormPersistence();
        protected LogicFactory logic = new LogicFactory();
        public FtpLogic ftp = new FtpLogic();
       
       
        public static bool buttonactive;
        public static int? AttPajakVendorGroupCode;
        protected void Page_Load(object sender, EventArgs e)
        {


        }
        public void ShowLoadAttachmentFakturPajak(string formType, 
            string _AttPajakReferenceNoDataField,
            string _thn,
            string _screenType,
            string _userName)
        {



            hidformtypeFakturPajak.Value = formType;

            hidReferenceNoFakturPajak.Value = _AttPajakReferenceNoDataField;
            hidthnFakturPajak.Value = _thn;
            hidScreenTypeFakturPajak.Value= _screenType;
            hidUserNameFakturPajak.Value = _userName;

            if (formType == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.showRefData();
            }
          else  if (formType == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.showRefData();
            }
            List<AttachmentFakturPajak> _list = new List<AttachmentFakturPajak>();
          
            _list = formPersistence.GetAttachmentFakturPajak(hidReferenceNoFakturPajak.Value, hidformtypeFakturPajak.Value);

            gridAttachmentFakturPajak.DataSource = _list;

            gridAttachmentFakturPajak.DataBind();

        }


        protected void evt_btAttFileFaktur_Click(object sender, EventArgs e)
        {
            popUploadFileFakturPajak.ShowOnPageLoad = true;
        }

        protected void evt_btManualFaktur_Click(object sender, EventArgs e)
        {
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                def.PnlMasterHeader.ClientVisible = false;
                def.PnlManualFaktur.ClientVisible = true;

                def.ucManualFakturPajak.EventMode("NEW", hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
                def.ucManualFakturPajak.DisabledManualFaktur();
            }
           else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.clearScreenMessage();
                defsettAccrfrom.PnlMasterHeader.ClientVisible = false;
                defsettAccrfrom.PnlManualFaktur.ClientVisible = true;

                defsettAccrfrom.ucManualFakturPajak.EventMode("NEW", hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
                defsettAccrfrom.ucManualFakturPajak.DisabledManualFaktur();
            }
          
            
        }

        protected void evt_imgEditAttachmentEnomFakturPajak_onClick(object sender, EventArgs e)
        {
          
            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            Int64 keyNum = Int64.Parse(key);
          

            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                def.ucManualFakturPajak.EventMode("EDIT", hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
                def.ucManualFakturPajak.ViewdatamanualEdit(keyNum);
                def.PnlMasterHeader.ClientVisible = false;
                def.PnlManualFaktur.ClientVisible = true;
            }
           else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.clearScreenMessage();
                defsettAccrfrom.ucManualFakturPajak.EventMode("EDIT", hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
                defsettAccrfrom.ucManualFakturPajak.ViewdatamanualEdit(keyNum);
                defsettAccrfrom.PnlMasterHeader.ClientVisible = false;
                defsettAccrfrom.PnlManualFaktur.ClientVisible = true;
            }
        }

        protected void evt_imgDeleteAttachmentEnomFakturPajak_onClick(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;

            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                def.clearScreenMessage();
                def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00253INF");

            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                defsettAccrfrom.clearScreenMessage();
                defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00253INF");
            }

            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            Int64 keyNum = Int64.Parse(key);
            AttachmentFakturPajak deleteAttachmentFaktur = new AttachmentFakturPajak();
            deleteAttachmentFaktur = formPersistence.GetAttachmentFakturPajak(hidReferenceNoFakturPajak.Value, hidformtypeFakturPajak.Value).Where(x => x.ID == keyNum).SingleOrDefault();
            if (deleteAttachmentFaktur.FILE_NAME == "Manual")
            {
                formPersistence.DeleteFakturPajakFull(keyNum);
            }
            else
            {
                logic.Say("evt_imgDeleteAttachmentEnomFakturPajak_onClick", "Key= {0}", keyNum);

                if ((deleteAttachmentFaktur != null))
                {
                    ftp.Delete(deleteAttachmentFaktur.DIRECTORY, deleteAttachmentFaktur.FILE_NAME);
                    formPersistence.DeleteFakturPajakFull(keyNum);

                }
            }

            ShowLoadAttachmentFakturPajak(hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value,hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                def.ReloadGrid();
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                defsettAccrfrom.ReloadGrid();
            }
           


        }

        public void IsVisibeleFakturPajak(bool IsVisible)
        {
            buttonactive = IsVisible;
            btAttFileFaktur.ClientVisible = IsVisible;
            btManualFaktur.ClientVisible = IsVisible;
            gridAttachmentFakturPajak.Columns["#"].Visible = IsVisible;
        }

        protected void evt_imgRefreshAAttachmentEnomFakturPajak_onClick(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
               
                def.clearScreenMessage();
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
             
                defsettAccrfrom.clearScreenMessage();
            }
           

            KeyImageButton image = (KeyImageButton)sender;
            string key = image.Key;
            Int64 keyNum = Int64.Parse(key);
            AttachmentFakturPajak RefreshAttachmentFaktur = new AttachmentFakturPajak();
            RefreshAttachmentFaktur = formPersistence.GetAttachmentFakturPajak(hidReferenceNoFakturPajak.Value, hidformtypeFakturPajak.Value).Where(x => x.ID == keyNum).SingleOrDefault();
            AttachmentFakturPajak attfileFakturedit = new AttachmentFakturPajak();

            attfileFakturedit.ID = keyNum;
            attfileFakturedit.DESCRIPTION = "";
            logic.Say("evt_imgRefreshAAttachmentEnomFakturPajak_onClick", "Key= {0}", keyNum);

            if ((RefreshAttachmentFaktur != null))
            {
                try
                {
                    var response = new HttpResponseMessage();
                    VATInManualInputView model = new VATInManualInputView();
                    resValidateFakturPm VATInXML = new resValidateFakturPm();

                    if (!string.IsNullOrEmpty(RefreshAttachmentFaktur.URL))
                    {
                        var httpClientHandler = new HttpClientHandler
                        {
                            // Proxy = new WebProxy("http://localhost:8888", false),
                            Proxy = new WebProxy("proxy.toyota.astra.co.id", 3128),
                            UseProxy = true
                        };
                        using (HttpClient client = new HttpClient(httpClientHandler))
                        // using (HttpClient client = new HttpClient())
                        {



                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                            Uri serviceUri = new Uri(RefreshAttachmentFaktur.URL, UriKind.Absolute);

                            XmlDocument doc = new XmlDocument();




                            response = client.GetAsync(serviceUri).Result;



                            var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                            var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                            using (var reader = XmlReader.Create(stream))
                            {
                                VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                                if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                                {
                                    throw new Exception("Invalid URL, Invoice not found");
                                }
                            }
                            string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                            DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                            DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                            attfileFakturedit.DESCRIPTION = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);

                            model.InvoiceNumber = VATInXML.nomorFaktur;
                            model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                            model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                            model.FGPengganti = VATInXML.fgPengganti;
                            model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                            model.ExpireDate = endOfMonth.ToShortDateString();
                            model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                            model.SupplierName = VATInXML.namaPenjual;

                            model.SupplierAddress = VATInXML.alamatPenjual;

                            model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                            model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                            model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                            model.StatusApprovalXML = VATInXML.statusApproval;
                            model.StatusFakturXML = VATInXML.statusFaktur;
                            model.VATBaseAmount = VATInXML.jumlahDpp;
                            model.VATAmount = VATInXML.jumlahPpn;
                            model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                            model.FakturType = "eFaktur";
                            model.VATInDetails = new List<VATInDetailManualInputView>();

                            foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                            {
                                VATInDetailManualInputView detailModel = new VATInDetailManualInputView();

                                detailModel.UnitName = detail.nama;
                                detailModel.UnitPrice = detail.hargaSatuan;
                                detailModel.Quantity = detail.jumlahBarang;
                                detailModel.TotalPrice = detail.hargaTotal;
                                detailModel.Discount = detail.diskon;
                                detailModel.DPP = detail.dpp;
                                detailModel.PPN = detail.ppn;
                                detailModel.PPNBM = detail.ppnbm;
                                detailModel.TarifPPNBM = detail.tarifPpnbm;

                                model.VATInDetails.Add(detailModel);
                            }
                        }
                    }

                    formPersistence.GenerateXML(model, RefreshAttachmentFaktur.URL, keyNum);

                    attfileFakturedit.STATUS = "Valid";
                    attfileFakturedit.ERR_MESSAGE = "";


                }
                catch (Exception ex)
                {

                    attfileFakturedit.STATUS = "InValid";
                    string err = "";
                    if (ex.Message.Trim() == "Object reference not set to an instance of an object.")
                    {
                        err = "QR Code Tidak Terbaca";
                    }
                    else if (ex.Message.Trim() == "One or more errors occurred.")
                    {
                        err = "Server DJP tidak dapat terkoneksi";
                    }

                    else
                    {
                        err = ex.Message.Trim();
                    }
                    if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                    {
                        def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                    }
                    else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                    }
                    attfileFakturedit.ERR_MESSAGE = err;
                    popUploadFileFakturPajak.ShowOnPageLoad = false;

                }
                attfileFakturedit.URL = RefreshAttachmentFaktur.URL;

                formPersistence.UpdateAttachmentFakturPajak(attfileFakturedit);

                ShowLoadAttachmentFakturPajak(hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);

            }


            ShowLoadAttachmentFakturPajak(hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                def.ReloadGrid();
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                defsettAccrfrom.ReloadGrid();
            }
        


        }
        protected void evt_btSubmitFileFaktur_Click(object sender, EventArgs e)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {

                def.clearScreenMessage();
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {

                defsettAccrfrom.clearScreenMessage();
            }
          
            if (uploadAttachmentFakturPajak.HasFile)
            {
                //foreach (HttpPostedFile uploadedFile2 in uploadAttachmentFakturPajak.PostedFile)
                //{
                //    uploadedFile.SaveAs(System.IO.Path.Combine(Server.MapPath("~/Images/"), uploadedFile.FileName));
                //    listofuploadedfiles.Text += String.Format("{0}<br />", uploadedFile.FileName);
                //}
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFile postedFile = Request.Files[i];
                    if (postedFile.ContentLength > 0)
                    {
                        string filename = CommonFunction.CleanFilename(postedFile.FileName);
                        FileInfo fi = new FileInfo(filename);
                        string ext = fi.Extension.Replace(".", "");
                        logic.Say("btSendUploadAttachment_Click", "FileName = {0}", postedFile.FileName);
                        string strMaxSize = ConfigurationManager.AppSettings["MAX_ATTACHMENT_SIZE"];
                        int maxSize = int.Parse(strMaxSize);
                        maxSize = maxSize * 1024;
                        byte[] fileData = null;
                        var binaryReader = new BinaryReader(postedFile.InputStream);
                        fileData = binaryReader.ReadBytes(postedFile.ContentLength);
                        if (fileData.Length > maxSize)
                        {
                            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                            {

                                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                            }
                            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
                            {

                                defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00089ERR", (maxSize / 1024).ToString());
                            }
                           

                            return;
                        }
                        if (ext.ToUpper() != "PDF")
                        {
                          
                            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                            {

                                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "File Type Must Be PDF");
                            }
                            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
                            {

                                defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "File Type Must Be PDF");
                            }
                            popUploadFileFakturPajak.ShowOnPageLoad = false;
                            return;

                        }

                        bool uploadSucceded = true;
                        string errorMessage = null;
                      

                       
                       
                        AttachmentFakturPajak attfileFaktur = new AttachmentFakturPajak();
                        attfileFaktur.REFERENCE_NO = hidReferenceNoFakturPajak.Value;
                        attfileFaktur.DIRECTORY = hidthnFakturPajak.Value + "/" + hidScreenTypeFakturPajak.Value + "/" + hidReferenceNoFakturPajak.Value + "/FakturPajak";
                        attfileFaktur.FILE_NAME = filename;
                        attfileFaktur.DESCRIPTION = "";
                        attfileFaktur.STATUS = "";
                        attfileFaktur.URL = "";
                        attfileFaktur.ERR_MESSAGE = "";
                        attfileFaktur.FormType = hidformtypeFakturPajak.Value;

                        // attfile.Blank = false;


                        attfileFaktur.CREATED_BY = hidUserNameFakturPajak.Value;
                        attfileFaktur.CREATED_DT = DateTime.Now;

                        ftp.ftpUploadFakturPajakBytes(hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value,
                             hidReferenceNoFakturPajak.Value,
                             filename,
                             fileData,
                             ref uploadSucceded,
                             ref errorMessage);

                        string ValueQrCode = ftp.ftpGetValueBarcode(fileData);



                        Int64 _id = formPersistence.CreateAttachmentFakturPajak(attfileFaktur);
                        AttachmentFakturPajak attfileFakturedit = new AttachmentFakturPajak();
                        attfileFakturedit.DESCRIPTION = "";
                        attfileFakturedit.ID = _id;

                        if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                        {

                            def.clearScreenMessage();
                            // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                            def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00254INF");
                        }


                        try
                        {

                            var response = new HttpResponseMessage();
                            VATInManualInputView model = new VATInManualInputView();
                            resValidateFakturPm VATInXML = new resValidateFakturPm();

                            if (!string.IsNullOrEmpty(ValueQrCode))
                            {
                                var httpClientHandler = new HttpClientHandler
                                {
                                    // Proxy = new WebProxy("http://localhost:8888", false),
                                    Proxy = new WebProxy("proxy.toyota.astra.co.id", 3128),
                                    UseProxy = true
                                };
                                using (HttpClient client = new HttpClient(httpClientHandler))
                                // using (HttpClient client = new HttpClient())
                                {
                                    client.DefaultRequestHeaders.Accept.Clear();
                                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                                    Uri serviceUri = new Uri(ValueQrCode, UriKind.Absolute);

                                    XmlDocument doc = new XmlDocument();
                                    response = client.GetAsync(serviceUri).Result;

                                    var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                                    var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                                    using (var reader = XmlReader.Create(stream))
                                    {
                                        VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                                        if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                                        {
                                            throw new Exception("Invalid URL, Invoice not found");
                                        }
                                    }
                                    string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                                    attfileFakturedit.DESCRIPTION = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);

                                    model.InvoiceNumber = VATInXML.nomorFaktur;
                                    model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                                    model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                                    model.FGPengganti = VATInXML.fgPengganti;
                                    model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                                    model.ExpireDate = endOfMonth.ToShortDateString();
                                    model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                                    model.SupplierName = VATInXML.namaPenjual;

                                    model.SupplierAddress = VATInXML.alamatPenjual;

                                    model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                                    model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                                    model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                                    model.StatusApprovalXML = VATInXML.statusApproval;
                                    model.StatusFakturXML = VATInXML.statusFaktur;
                                    model.VATBaseAmount = VATInXML.jumlahDpp;
                                    model.VATAmount = VATInXML.jumlahPpn;
                                    model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                                    model.FakturType = "eFaktur";
                                    model.VATInDetails = new List<VATInDetailManualInputView>();

                                    foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                                    {
                                        VATInDetailManualInputView detailModel = new VATInDetailManualInputView();

                                        detailModel.UnitName = detail.nama;
                                        detailModel.UnitPrice = detail.hargaSatuan;
                                        detailModel.Quantity = detail.jumlahBarang;
                                        detailModel.TotalPrice = detail.hargaTotal;
                                        detailModel.Discount = detail.diskon;
                                        detailModel.DPP = detail.dpp;
                                        detailModel.PPN = detail.ppn;
                                        detailModel.PPNBM = detail.ppnbm;
                                        detailModel.TarifPPNBM = detail.tarifPpnbm;

                                        model.VATInDetails.Add(detailModel);
                                    }
                                }
                            }

                            formPersistence.GenerateXML(model, ValueQrCode, _id);

                            attfileFakturedit.STATUS = "Valid";
                            attfileFakturedit.ERR_MESSAGE = "";
                            popUploadFileFakturPajak.ShowOnPageLoad = false;

                        }
                        catch (Exception ex)
                        {

                            attfileFakturedit.STATUS = "InValid";
                            string err = "";
                            if (ex.Message.Trim() == "Object reference not set to an instance of an object.")
                            {
                                err = "QR Code Tidak Terbaca";
                            }
                            else if (ex.Message.Trim() == "One or more errors occurred.")
                            {
                                err = "Server DJP tidak dapat terkoneksi";
                            }

                            else
                            {
                                err = ex.Message.Trim();
                            }
                            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                            {
                                def.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                               
                            }
                            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.PostLaterOn(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", err);
                                
                            }
                           
                            attfileFakturedit.ERR_MESSAGE = err;
                            popUploadFileFakturPajak.ShowOnPageLoad = false;

                        }

                        attfileFakturedit.URL = ValueQrCode;

                        formPersistence.UpdateAttachmentFakturPajak(attfileFakturedit);

                        ShowLoadAttachmentFakturPajak(hidformtypeFakturPajak.Value, hidReferenceNoFakturPajak.Value, hidthnFakturPajak.Value, hidScreenTypeFakturPajak.Value, hidUserNameFakturPajak.Value);
                        if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
                        {
                            def.ReloadGrid();
                        }
                        else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
                        {
                            defsettAccrfrom.ReloadGrid();
                        }

                     }
                }
            }

        }

        protected void ViewManualFaktur(object sender, System.EventArgs e)
        {
            LinkButton bTN = (LinkButton)sender;
            Int64 lKey = Convert.ToInt64(gridAttachmentFakturPajak.GetRowValues(Convert.ToInt32(bTN.CommandArgument), "ID").ToString());

           

            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                def.ucManualFakturPajak.Viewdatamanual(lKey, "ELVIS_PVFormList");
                def.PnlMasterHeader.ClientVisible = false;
                def.PnlManualFaktur.ClientVisible = true;
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {

                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.clearScreenMessage();
                defsettAccrfrom.ucManualFakturPajak.Viewdatamanual(lKey, "ELVIS_AccrPVForm");
                defsettAccrfrom.PnlMasterHeader.ClientVisible = false;
                defsettAccrfrom.PnlManualFaktur.ClientVisible = true;
            }
        }

        protected void ViewManualFakturStatus(object sender, System.EventArgs e)
        {
            LinkButton bTN = (LinkButton)sender;
            Int64 lKey = Convert.ToInt64(gridAttachmentFakturPajak.GetRowValues(Convert.ToInt32(bTN.CommandArgument), "ID").ToString());
            if (hidformtypeFakturPajak.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
            }
            else if (hidformtypeFakturPajak.Value == "ELVIS_AccrPVForm")
            {
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                defsettAccrfrom.clearScreenMessage();
            }
                string errmsg = formPersistence.GetAttachmentFakturPajak(hidReferenceNoFakturPajak.Value, hidformtypeFakturPajak.Value).Where(x => x.ID == lKey).SingleOrDefault().ERR_MESSAGE;
            txtErrMsg.Text = errmsg;
            popErrMsg.ShowOnPageLoad = true;

        }
        protected void evt_gridAttachmentFakturPajak_onHtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs arg)
        {
           

            gridAttachmentFakturPajak = (ASPxGridView)sender;

            if (arg.RowType != GridViewRowType.Data)
            {
                return;
            }

            Int64 key = (Int64)arg.KeyValue;
            if (key > 0)
            {

                AttachmentFakturPajak _itemlist = new AttachmentFakturPajak();
                _itemlist = formPersistence.GetAttachmentFakturPajak(hidReferenceNoFakturPajak.Value, hidformtypeFakturPajak.Value).Where(x => x.ID == key).SingleOrDefault();
                BlankTargetedHyperlink lblFILE_NAME = (BlankTargetedHyperlink)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblFILE_NAME");
                ASPxLabel lblManualFileFaktur = (ASPxLabel)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblManualFileFaktur");
                KeyImageButton imgRefreshAttachmentEnomFakturPajak = (KeyImageButton)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgRefreshAttachmentEnomFakturPajak");
                KeyImageButton imgEditAttachmentEnomFakturPajak = (KeyImageButton)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "imgEditAttachmentEnomFakturPajak");
                BlankTargetedHyperlink lblDESCRIPTION = (BlankTargetedHyperlink)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblDESCRIPTION");
                LinkButton linkDESCRIPTION = (LinkButton)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "linkDESCRIPTION");
                LinkButton linkSTATUS = (LinkButton)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "linkSTATUS");
                ASPxLabel lblSTATUS = (ASPxLabel)gridAttachmentFakturPajak.FindRowCellTemplateControl(arg.VisibleIndex, null, "lblSTATUS");
                if (_itemlist != null)
                {
                    if (_itemlist.STATUS == "Valid")
                    {
                        linkSTATUS.Visible = false;
                        lblSTATUS.Visible = true;
                    }
                    else
                    {
                        linkSTATUS.Visible = true;
                        lblSTATUS.Visible = false;
                    }
                    if (lblFILE_NAME != null)
                    {
                        if (_itemlist.FILE_NAME.Trim() == "Manual")
                        {
                            lblFILE_NAME.Visible = false;
                            lblManualFileFaktur.Visible = true;
                            if (buttonactive)
                            {
                                imgEditAttachmentEnomFakturPajak.Visible = true;
                                imgRefreshAttachmentEnomFakturPajak.Visible = false;
                            }
                            lblDESCRIPTION.Visible = false;
                            linkDESCRIPTION.Visible = true;

                        }
                        else
                        {
                            lblFILE_NAME.Visible = true;
                            lblManualFileFaktur.Visible = false;
                            if (buttonactive)
                            {
                                imgEditAttachmentEnomFakturPajak.Visible = false;
                            }
                            lblDESCRIPTION.Visible = true;
                            linkDESCRIPTION.Visible = false;
                            if (_itemlist.ERR_MESSAGE.Trim() == "Server DJP tidak dapat terkoneksi")
                            {
                                if (buttonactive)
                                {
                                    imgRefreshAttachmentEnomFakturPajak.Visible = true;
                                }
                            }
                            else
                            {
                                if (buttonactive)
                                {
                                    imgRefreshAttachmentEnomFakturPajak.Visible = false;
                                }
                            }
                        }
                    }
                }

            }


        }



    }
}