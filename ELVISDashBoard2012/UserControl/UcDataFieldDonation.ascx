﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcDataFieldDonation.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcDataFieldDonation" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<%@ Register TagPrefix="dx" Namespace="DevExpress.Web.ASPxEditors" Assembly="DevExpress.Web.v12.2" %>
<script type="text/javascript">
    function OnNewDonationClick(s, e) {
        //ASPxGridViewDonation.AddNewRow();
        ASPxGridViewDonation.AddNewRow();
    }


    function BeforeUploadDonation_Click(s, e) {
        //var fn = $("#fuDataFieldDonationList").val();
        var fn = $("#fuDataFieldDonationList").val();

        if (!fn || fn.length() < 1) {
            alert("Select file");
            return false;
        }
        return true;
    }
    //$(document).ready(function () { ASPxClientEdit.ClearGroup('GroupDonation'); });
    function fn_AllowonlyNumeric(s, e) {
        var theEvent = e.htmlEvent || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
        var regex = /[0-9]/;
        if (!regex.test(key)) {
            theEvent.returnValue = false;
            if (theEvent.preventDefault)
                theEvent.preventDefault();
        }
    }


    //function OnEditClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.StartEditRow(index);
    //}

    //function OnSaveClick(s, e) {
    //    grid.UpdateEdit();
    //}

    //function OnCancelClick(s, e) {
    //    grid.CancelEdit();
    //}

    //function OnDeleteClick(s, e) {
    //    var index = grid.GetFocusedRowIndex();
    //    grid.DeleteRow(index);
    //}  
</script>
<style type="text/css">
    div.inline {
        float: left;
    }

    .clearBoth {
        clear: both;
    }

    /*  #divTransactionType
   {
       width: 350px;
   }
   #divVendorCode 
   {
       width: 150px;
   }
   #divVendorName
   {
       width: 120px;
   }*/
</style>

<asp:UpdatePanel runat="server" ID="UpdatePanelDonation">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSaveDonation" />
        <asp:PostBackTrigger ControlID="btnCloseDonation" />
        <asp:PostBackTrigger ControlID="btnClearDonation" />

        <asp:PostBackTrigger ControlID="btnUploadTemplateDonation" />
    </Triggers>
    <ContentTemplate>

        <table>
            <tr>
                <td>
                    <dx:ASPxHyperLink runat="server" ID="lnkDownloadTemplateDonation" Text="Download Template"
                        NavigateUrl="~/Template/Excel/TemplateDataFieldDonation.xls" />

                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:FileUpload ID="fuDataFieldDonationList" runat="server" ClientIDMode="Static" />

                </td>
                <td>&nbsp;&nbsp;&nbsp;</td>
                <td>

                    <asp:Button ID="btnUploadTemplateDonation" runat="server" Text="Upload" OnClick="btnUploadTemplateDonation_Click"
                        OnClientClick="return BeforeUploadDonation_Click()" />
                        
                        <%--<ClientSideEvents Click="function(s,e) {loading(); }" />--%>
                    <%--</dx:ASPxButton>--%>

                </td>
            </tr>

        </table>
        <div>&nbsp;&nbsp;</div>
        <dx:ASPxPanel ID="FormPanelDonation" runat="server" Width="100%" Style="border: 1px solid #000;">

            <PanelCollection>

                <dx:PanelContent runat="server">

                    <div>&nbsp;&nbsp;</div>
                    <table border="0" id="tableheaderdonation" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                        <tr>
                            <td valign="baseline" style="width: 170px">Tahun Pajak
                            </td>
                            <td valign="top" style="width: 100px">

                                <dx:ASPxSpinEdit ID="spnTahunPajakDonation" runat="server" Number="0" Width="70" MaxLength="4" ClientInstanceName="spnTahunPajakDonation" OnValidation="TahunPajakDonation_Validation" NumberType="Integer">

                                    <ValidationSettings ValidationGroup="GroupDonation" ErrorDisplayMode="Text" SetFocusOnError="True" ErrorTextPosition="Bottom" ErrorText="Name must be at least two characters long">
                                        <RequiredField IsRequired="True" ErrorText="Tahun Pajak is required" />
                                    </ValidationSettings>


                                </dx:ASPxSpinEdit>
                            </td>
                            <td valign="baseline" style="width: 170px">Tmp. &  Tgl. Penandatangan
                            </td>
                            <td valign="top" style="width: 260px">
                                <div class="inline">


                                    <dx:ASPxTextBox runat="server" ID="txtTempatPenandatanganDonation" Width="120px">

                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupDonation" ErrorTextPosition="Bottom" SetFocusOnError="True">
                                            <RequiredField IsRequired="True" ErrorText="Tmp.  is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                                <div class="inline" style="width: 10px">&nbsp;&nbsp;</div>

                                <div class="inline">


                                    <dx:ASPxDateEdit ID="dtPenandaTanganDonation" ClientInstanceName="dtPenandaTanganDonation"
                                        ClientIDMode="static" runat="server" Width="120px" DisplayFormatString="dd MMM yyyy"
                                        EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table">


                                        <CalendarProperties>
                                            <FastNavProperties Enabled="False" />
                                        </CalendarProperties>


                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupDonation" ErrorTextPosition="Bottom" SetFocusOnError="True">
                                            <RequiredField IsRequired="True" ErrorText="Tanggal is required" />
                                        </ValidationSettings>
                                    </dx:ASPxDateEdit>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline" style="width: 200px">Nama Penandatangan
                            </td>
                            <td valign="top" style="width: 200px">
                                <dx:ASPxTextBox runat="server" ID="txtNamaPenandatanganDonation" Width="300px">


                                    <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupDonation" ErrorTextPosition="Bottom" SetFocusOnError="True">
                                        <RequiredField IsRequired="True" ErrorText="Nama Penandatangan is required" />
                                    </ValidationSettings>
                                </dx:ASPxTextBox>
                            </td>

                            <td valign="baseline">Jabatan Penandatangan
                            </td>
                            <td valign="top">
                                <div>
                                    <dx:ASPxTextBox runat="server" ID="txtJabatanpenandatanagnDonation" Width="250px">

                                        <ValidationSettings ErrorDisplayMode="Text" ValidationGroup="GroupDonation" ErrorTextPosition="Bottom" SetFocusOnError="True">
                                            <RequiredField IsRequired="True" ErrorText="Jabatan Penandatangan is required" />
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </div>

                            </td>
                        </tr>

                    </table>
                </dx:PanelContent>
            </PanelCollection>
        </dx:ASPxPanel>
        <div>&nbsp;&nbsp;</div>
        <dx:ASPxButton ID="btnNewDonation" runat="server" Text="Add" AutoPostBack="false">
            <ClientSideEvents Click="function (s, e) { OnNewDonationClick(s, e); }" />
        </dx:ASPxButton>
        <br />
        <asp:HiddenField runat="server" ID="hidCategoryCodeDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidTransactionCodeDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidReferenceNoDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidUserNameDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorCodeDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorGroupDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidformtypeDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidScreenTypeDonation" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidthnDonation" ClientIDMode="Static" />
         <asp:HiddenField runat="server" ID="hidIsSaveDonation" ClientIDMode="Static" />
        <dx:ASPxGridView ID="ASPxGridViewDonation" runat="server"
            ClientInstanceName="ASPxGridViewDonation"
            AutoGenerateColumns="False"
            OnRowInserting="ASPxGridViewDonation_RowInserting"
            OnRowUpdating="ASPxGridViewDonation_RowUpdating"
            OnRowDeleting="ASPxGridViewDonation_RowDeleting"
            OnRowValidating="ASPxGridViewDonation_RowValidating"
            
            OnInitNewRow="ASPxGridViewDonation_InitNewRow"
            OnParseValue="ASPxGridViewDonation_ParseValue"
            OnStartRowEditing="ASPxGridViewDonation_StartRowEditing"
            OnCancelRowEditing="ASPxGridViewDonation_Cancel"
            ClientIDMode="AutoID" KeyFieldName="SEQ_NO" Width="100%"
            Styles-AlternatingRow-CssClass="even">
            <SettingsBehavior AllowDragDrop="False" AllowSort="False" />
            <SettingsPager AlwaysShowPager="True">
            </SettingsPager>
            <SettingsEditing Mode="Inline" />

            <Styles>
                <Header HorizontalAlign="Center"></Header>
            </Styles>
            <Columns>
                <dx:GridViewCommandColumn VisibleIndex="0" FixedStyle="Left" Width="63px" ButtonType="Image">

                    <EditButton Visible="True">
                        <Image ToolTip="Edit" Url="~/App_Themes/BMS_Theme/Images/damage.png"></Image>
                    </EditButton>

                    <DeleteButton Visible="True">
                        <Image ToolTip="Delete" Url="~/App_Themes/BMS_Theme/Images/svn_deleted.png"></Image>
                    </DeleteButton>
                    <CancelButton>
                        <Image ToolTip="Cancel" Url="~/App_Themes/BMS_Theme/Images/svn_modified.png">
                        </Image>
                    </CancelButton>
                    <UpdateButton>
                        <Image ToolTip="Update" Url="~/App_Themes/BMS_Theme/Images/svn_normal.png">
                        </Image>
                    </UpdateButton>

                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                    </CellStyle>

                </dx:GridViewCommandColumn>
                <dx:GridViewDataTextColumn FieldName="NO" Caption="No" Width="40px" VisibleIndex="1">
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblSeqNoDonation" Text='<%# Bind("NO")%>'></dx:ASPxLabel>
                        <asp:HiddenField runat="server" ID="hidSeqNoDonation" Value='<%# Bind("SEQ_NO") %>' />
                    </EditItemTemplate>
                    <DataItemTemplate>

                        <dx:ASPxLabel runat="server" ID="lblNoDonation" Text='<%# Bind("NO") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="JENIS_SUMBANGAN" Caption="Jenis Sumbangan" Width="300px" VisibleIndex="2">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtJenisSumbanganDonation" Width="292px" Value='<%# Bind("JENIS_SUMBANGAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">

                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>


                    </EditItemTemplate>
                    <DataItemTemplate>

                        <dx:ASPxLabel runat="server" ID="lblJenisSumbanganDonation" Text='<%# Bind("JENIS_SUMBANGAN") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="BENTUK_SUMBANGAN" Caption="Bentuk Sumbangan" Width="250px" VisibleIndex="3">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox ID="txtBentukSumbanganDonation" runat="server" Width="242" Value='<%#Bind("BENTUK_SUMBANGAN") %>' ClientInstanceName="txtNPWP" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">

                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>

                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>

                        <dx:ASPxLabel runat="server" ID="lblBentukSumbanganDonation" Text='<%# Bind("BENTUK_SUMBANGAN") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="NILAI_SUMBANGAN" Caption="Nilai Sumbangan" Width="180px" VisibleIndex="4">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>

                        <dx:ASPxSpinEdit ID="spnNilaiSumbanganDonation" runat="server"   DisplayFormatString="Rp. {0:#,##,##.00}" Number="0" Width="172" Value='<%# Bind("NILAI_SUMBANGAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxSpinEdit>
                    </EditItemTemplate>
                    <DataItemTemplate>

                        <dx:ASPxLabel runat="server" ID="lblNilaiSumbanganDonation" Text='<%# String.Format("Rp. {0}", Convert.ToDecimal(Eval("NILAI_SUMBANGAN")).ToString("#,##,##.00"))%>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>




                <dx:GridViewDataTextColumn FieldName="TANGGAL_SUMBANGAN" Caption="Tanggal Sumbangan" Width="120px" VisibleIndex="5">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxDateEdit ID="dtTanggalsumbanganDonation" Value='<%# Bind("TANGGAL_SUMBANGAN") %>' ClientInstanceName="dtTanggalsumbangan"
                            ClientIDMode="static" runat="server" Width="112px" DisplayFormatString="dd MMM yyyy"
                            EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">

                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </EditItemTemplate>
                    <DataItemTemplate>

                        <dx:ASPxLabel runat="server" ID="lblTanggalSumbanganDonation" Text='<%# Bind("TANGGAL_SUMBANGAN","{0:dd MMM yyyy}") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn FieldName="NAMA_LEMBAGA_PENERIMA" Caption="Nama Lembaga Penerima" Width="310px" VisibleIndex="6">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtNamaLembagaPenerimaDonation" Width="302px" Value='<%# Bind("NAMA_LEMBAGA_PENERIMA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblNamaLembagaPenerimaDonation" Text='<%# Bind("NAMA_LEMBAGA_PENERIMA") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="NPWP_LEMBAGA_PENERIMA" Caption="NPWP Lembaga Penerima" Width="150px" VisibleIndex="7">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>


                        <dx:ASPxTextBox ID="txtNPWPLembagaPenerimaDonation" runat="server" Width="142" Value='<%#Bind("NPWP_LEMBAGA_PENERIMA") %>' ClientInstanceName="txtNPWPLembagaPenerimaDonation" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <MaskSettings Mask="99.999.999.9-999.999" IncludeLiterals="None" />
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>

                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblLembagaPenerimaDonation" Text='<%# Bind("NPWP_LEMBAGA_PENERIMA") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="ALAMAT_LEMBAGA" Caption="Alamat Lembaga" Width="310px" VisibleIndex="8">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtAlamatLembagaDonation" Width="302px" Value='<%# Bind("ALAMAT_LEMBAGA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblAlamatLembagaDonation" Text='<%# Bind("ALAMAT_LEMBAGA") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>


                <dx:GridViewDataTextColumn FieldName="NO_TELP_LEMBAGA" Caption="No. Telp Lembaga" Width="150px" VisibleIndex="9">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtNO_TELP_LEMBAGADonation" Width="142px" Value='<%# Bind("NO_TELP_LEMBAGA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ClientSideEvents KeyPress="function(s,e){ fn_AllowonlyNumeric(s,e);}" />
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblNO_TELP_LEMBAGADonation" Text='<%# Bind("NO_TELP_LEMBAGA") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <%--        <dx:GridViewDataTextColumn FieldName="NO_TELP_LEMBAGA" Caption="No. Telp Lembaga" Width="180px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                 
                                        <dx:ASPxSpinEdit ID="txtNO_TELP_LEMBAGADonation" runat="server"  Number="0" Width="172" Value='<%# Bind("NO_TELP_LEMBAGA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNO_TELP_LEMBAGADonation" Text='<%# Bind("NO_TELP_LEMBAGA") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>--%>


                <dx:GridViewDataTextColumn FieldName="SARANA_PRASARANA_INFRASTRUKTUR" Caption="Sarana Prasarana Infrastruktur" Width="310px" VisibleIndex="10">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtSARANA_PRASARANA_INFRASTRUKTURDonation" Width="302px" Value='<%# Bind("SARANA_PRASARANA_INFRASTRUKTUR") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblSARANA_PRASARANA_INFRASTRUKTURDonation" Text='<%# Bind("SARANA_PRASARANA_INFRASTRUKTUR") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="LOKASI_INFRASTRUKTUR" Caption="Lokasi Infrastruktur" Width="310px" VisibleIndex="11">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtLOKASI_INFRASTRUKTURDonation" Width="302px" Value='<%# Bind("LOKASI_INFRASTRUKTUR") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblLOKASI_INFRASTRUKTURDonation" Text='<%# Bind("LOKASI_INFRASTRUKTUR") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="IZIN_MENDIRIKAN_BANGUNAN" Caption="Izin Mendirikan Bangunan" Width="310px" VisibleIndex="12">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtIZIN_MENDIRIKAN_BANGUNANRDonation" Width="302px" Value='<%# Bind("IZIN_MENDIRIKAN_BANGUNAN") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblIZIN_MENDIRIKAN_BANGUNANDonation" Text='<%# Bind("IZIN_MENDIRIKAN_BANGUNAN") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="NOMOR_REKENING" Caption="Nomor Rekening" Width="310px" VisibleIndex="13">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtNOMOR_REKENINGDonation" Width="302px" Value='<%# Bind("NOMOR_REKENING") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ClientSideEvents KeyPress="function(s,e){ fn_AllowonlyNumeric(s,e);}" />
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblNOMOR_REKENINGDonation" Text='<%# Bind("NOMOR_REKENING") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <%--  <dx:GridViewDataTextColumn FieldName="NOMOR_REKENING" Caption="Nomor Rekening" Width="310px" VisibleIndex="4">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                 
                                        <dx:ASPxSpinEdit ID="txtNOMOR_REKENINGDonation" runat="server"  Number="0" Width="302" Value='<%# Bind("NOMOR_REKENING") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxSpinEdit>
                                </EditItemTemplate>
                                <DataItemTemplate>

                                    <dx:ASPxLabel runat="server" ID="lblNOMOR_REKENINGDonation" Text='<%# Bind("NOMOR_REKENING") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>--%>


                <dx:GridViewDataTextColumn FieldName="NAMA_REKENING_PENERIMA" Caption="Nama Rekening Penerima" Width="310px" VisibleIndex="13">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtNAMA_REKENING_PENERIMADonation" Width="302px" Value='<%# Bind("NAMA_REKENING_PENERIMA") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblNAMA_REKENING_PENERIMADonation" Text='<%# Bind("NAMA_REKENING_PENERIMA") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <dx:GridViewDataTextColumn FieldName="NAMA_BANK" Caption="Nama Bank" Width="310px" VisibleIndex="15">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>
                        <dx:ASPxTextBox runat="server" ID="txtNAMA_BANKDonation" Width="302px" Value='<%# Bind("NAMA_BANK") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxTextBox>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblNAMA_BANKDonation" Text='<%# Bind("NAMA_BANK") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <%--      <dx:GridViewDataTextColumn FieldName="NO_KTP" Caption="Nomor KTP" Width="310px" VisibleIndex="16">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                                    Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <EditItemTemplate>
                                    <dx:ASPxTextBox runat="server" ID="txtNO_KTPDonation" Width="302px" Value='<%# Bind("NO_KTP") %>' DisplayFormatString="Rp. {0:#,##,##.00}" Number="0" ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                                       <ClientSideEvents KeyPress="function(s,e){ fn_AllowonlyNumeric(s,e);}" />
                                        <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                                        </ValidationSettings>
                                    </dx:ASPxTextBox>
                                </EditItemTemplate>
                                <DataItemTemplate>
                                    <dx:ASPxLabel runat="server" ID="lblNO_KTPDonation" Text='<%# Bind("NO_KTP","{0:C2}") %>' />
                                </DataItemTemplate>
                            </dx:GridViewDataTextColumn>--%>

                <dx:GridViewDataTextColumn FieldName="BIAYA_PEMBANGUNAN_INFRASTRUKTUR" Caption="Biaya Pembangunan Infrastruktur" Width="200px" VisibleIndex="17">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true" />
                    <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px"
                        Wrap="true">
                        <Paddings PaddingLeft="5px" />
                    </EditCellStyle>
                    <EditItemTemplate>

                        <dx:ASPxSpinEdit ID="spnBIAYA_PEMBANGUNAN_INFRASTRUKTURDonation" runat="server" DisplayFormatString="Rp. {0:#,##,##.00}" Number="0" Width="192" Value='<%# Bind("BIAYA_PEMBANGUNAN_INFRASTRUKTUR") %>' ValidationSettings-ValidationGroup="<%# Container.ValidationGroup %>">
                            <ValidationSettings Display="Dynamic" ErrorDisplayMode="None">
                            </ValidationSettings>
                        </dx:ASPxSpinEdit>
                    </EditItemTemplate>
                    <DataItemTemplate>
                        <dx:ASPxLabel runat="server" ID="lblBIAYA_PEMBANGUNAN_INFRASTRUKTURDonation" Text='<%# String.Format("Rp. {0}", Convert.ToDecimal(Eval("BIAYA_PEMBANGUNAN_INFRASTRUKTUR")).ToString("#,##,##.00"))%>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>

                <%--batas--%>
            </Columns>

            <Settings ShowVerticalScrollBar="false"
                ShowHorizontalScrollBar="True" />
            <SettingsText CommandDelete=" " CommandEdit=" " CommandNew=" " />
            <Styles>
                <AlternatingRow CssClass="even">
                </AlternatingRow>
            </Styles>



        </dx:ASPxGridView>



        <br />
        <br />

        <div style="position: relative; float: right">
            <table>
                <tr>
                    <td>
                        <dx:ASPxButton ID="btnClearDonation" runat="server" Text="Clear" OnClick="evt_Clear_Donation">
                            <ClientSideEvents Click="function(s,e) { loading(); ASPxClientEdit.ClearGroup('GroupDonation');}" />
                        </dx:ASPxButton>
                    </td>
                    <td>
                        <dx:ASPxButton ID="btnCloseDonation" runat="server" Text="Cancel" OnClick="evt_Close_Donation">
                            <ClientSideEvents Click="function(s,e) { loading();  ASPxClientEdit.ClearGroup('GroupDonation');}" />
                        </dx:ASPxButton>
                    </td>


                    <td>
                        <dx:ASPxButton ID="btnSaveDonation" runat="server" Text="Save" OnClick="evt_Save_Donation" ValidationGroup="GroupDonation">
                            <ClientSideEvents Click="function(s,e) { loading(); }" />
                        </dx:ASPxButton>
                    </td>
                </tr>
            </table>
        </div>

    </ContentTemplate>

</asp:UpdatePanel>

