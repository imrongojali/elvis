﻿
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;

using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using ELVISDashBoard._30PvFormList;
using Microsoft.Reporting.WebForms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace ELVISDashBoard.UserControl
{


    public partial class UcDataFieldDonation : System.Web.UI.UserControl
    {
        protected readonly GlobalResourceData resx = new GlobalResourceData();
        //public static string hidCategoryCodeDonation.Value;
        //public static string hidTransactionCodeDonation.Value;
        //public static string hidReferenceNoDonation.Value;
        //public static string hidUserNameDonation.Value;
        //public static string hidVendorCodeDonation.Value;
        //public static string hidVendorGroupDonation.Value;
        //public static string _formtypeDon;
        //public static string hidScreenTypeDonation.Value;
        //public static string hidthnDonation.Value;
        protected readonly FormPersistence formPersistence = new FormPersistence();
        protected LogicFactory logic = new LogicFactory();
        //public static bool IsSaveDonation { get; set; }
        protected FtpLogic ftp = new FtpLogic();



        protected void Page_Load(object sender, EventArgs e)
        {

           // this.InitializeCultureDonation();
            if (hidCategoryCodeDonation.Value != "")
            {
                List<Donation> _listData = new List<Donation>();

                _listData = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
                if (_listData.Count() > 0)
                {
                    ASPxGridViewDonation.DataSource = _listData;
                    ASPxGridViewDonation.DataBind();
                }
            }
            //    ASPxEdit.ValidateEditorsInContainer(this);
            //  spnTahunPajakDonation.Number = DateTime.Now.Year;
            // dtPenandaTanganDonation.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));


        }

       

        protected void InitializeCultureDonation()
        {
            //CultureInfo Culture = new System.Globalization.CultureInfo("id-ID");
            //Thread.CurrentThread.CurrentCulture = Culture;
            //Thread.CurrentThread.CurrentUICulture = Culture;
            //CultureInfo newCulture = (CultureInfo)CultureInfo.CurrentCulture.Clone();

            CultureInfo newCulture = new System.Globalization.CultureInfo("id-ID");
            newCulture.NumberFormat.CurrencySymbol = "Rp. ";
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
        }


        public void DonationList(string _formtype,
            string CategoryCodeProm,
            string TransactionCodeProm,
            string ReferenceNoProm,
            string UserNameProm,
            string VendorCode,
            string VendorGroup,
            string ScreenType,
            string thn)
        {



            hidformtypeDonation.Value = _formtype;
            hidCategoryCodeDonation.Value = CategoryCodeProm;
            hidTransactionCodeDonation.Value = TransactionCodeProm;
            hidReferenceNoDonation.Value = ReferenceNoProm;
            hidUserNameDonation.Value = UserNameProm;
            hidVendorCodeDonation.Value = VendorCode;
            hidVendorGroupDonation.Value = VendorGroup;
            hidScreenTypeDonation.Value = ScreenType;
            hidthnDonation.Value = thn;

            bool categorysignature = formPersistence.GetCategorySignature(CategoryCodeProm);
            txtJabatanpenandatanagnDonation.Enabled = categorysignature;

            if (!categorysignature) {
                txtJabatanpenandatanagnDonation.Text = " ";
            }
            else
            {
                txtJabatanpenandatanagnDonation.Text = "";
            }
            //
            DataValidDonation();
            formPersistence.GetDeleteIsActiveDonation(CategoryCodeProm, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            List<Donation> _listData = new List<Donation>();
            _listData = formPersistence.GetDonation(CategoryCodeProm, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            ASPxGridViewDonation.DataSource = _listData;
            ASPxGridViewDonation.DataBind();


            if (_listData.Count > 0)
            {
                int thn_pjk_Donation = _listData.Select(x => x.THN_PAJAK).FirstOrDefault();
                DateTime PenandaTanganDonation = _listData.Select(x => x.TGL_PENANDATANGAN).FirstOrDefault();
                string TempatPenandatanganDonation = _listData.Select(x => x.TMP_PENANDATANGAN).FirstOrDefault();
                string NamaPenandaTanganDonation = _listData.Select(x => x.NAMA_PENANDATANGAN).FirstOrDefault();
                string JabatanpenandatanagnDonation = _listData.Select(x => x.JABATAN_PENANDATANGAN).FirstOrDefault();
                spnTahunPajakDonation.Number = thn_pjk_Donation;
                txtTempatPenandatanganDonation.Text = TempatPenandatanganDonation;
                dtPenandaTanganDonation.Date = Convert.ToDateTime(PenandaTanganDonation.ToString("MM/dd/yyyy"));
                txtNamaPenandatanganDonation.Text = NamaPenandaTanganDonation;
                txtJabatanpenandatanagnDonation.Text = JabatanpenandatanagnDonation;
            }
            else
            {
                spnTahunPajakDonation.Number = DateTime.Now.Year;
                dtPenandaTanganDonation.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            }

            if (hidformtypeDonation.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = hidTransactionCodeDonation.Value;
                def.FormData.ReferenceNoDataField = hidReferenceNoDonation.Value;
            }

            else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
            {
                var def = this.Page as _80Accrued.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = hidTransactionCodeDonation.Value;
                def.FormData.ReferenceNoDataField = hidReferenceNoDonation.Value;
            }
            displayheaderdonation();

            string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldDonationTemp.xls");
            string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldDonation.xls");


            FileStream file = new FileStream(templatePathSource, FileMode.OpenOrCreate, FileAccess.Read);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            // sheet.GetRow(3).GetCell(0).StringCellValue.ToString().Trim();

            int i = 0;
            List<DataFieldTemplate> dataField = new List<DataFieldTemplate>();
            dataField = formPersistence.getDataFieldNameTemplate(hidCategoryCodeDonation.Value);
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRow = sheet.GetRow(2);
                    ICell cell = dataRow.GetCell(i);
                    cell.SetCellValue(item.FieldNameByExcel);

                    IRow dataRowValue = sheet.GetRow(3);
                    ICell cellValue = dataRowValue.GetCell(i);

                    EnomVendor _VendorFirst = new EnomVendor();
                    _VendorFirst = formPersistence.GetEnomVendor(hidVendorCodeDonation.Value).FirstOrDefault();
                    if (cellValue != null)
                    {
                        if (i == 4)
                        {
                            cellValue.SetCellValue(_VendorFirst.VENDOR_NAME);
                        }
                        if (i == 5)
                        {
                            cellValue.SetCellValue(RemoveSpecialCharacters(_VendorFirst.NPWP));
                        }
                        if (i == 6)
                        {
                            cellValue.SetCellValue(_VendorFirst.NpwpAddress);
                        }

                    }

                    var _vg = (hidVendorGroupDonation.Value == "") ? "0" : hidVendorGroupDonation.Value;
                    bool isValidation = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, item.FieldNameByTable, Convert.ToInt32(_vg));
                    if (isValidation == true)
                    {
                        stylexlsDonation(hssfwb, cell);
                    }
                    i++;
                }
            }
            using (FileStream fs = new FileStream(templatePath, FileMode.Create, FileAccess.Write))
            {

                hssfwb.Write(fs);
                fs.Close();
                file.Close();
            }

            int r = 0;
            //string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonationTemp.xls");
            //string templatePathXls = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonation.xls");

            string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonationTemp.xls");
            string templatePathXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonation.xls");
            FileStream filexls = new FileStream(templatePathSourceXls, FileMode.OpenOrCreate, FileAccess.Read);
            HSSFWorkbook hssfwbxls = new HSSFWorkbook(filexls);
            ISheet sheetxls = hssfwbxls.GetSheet("Default");
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRowXls = sheetxls.GetRow(0);
                    ICell cellXls = dataRowXls.GetCell(r);
                    cellXls.SetCellValue(item.FieldNameByExcel);
                    stylexlsDonation(hssfwbxls, cellXls);
                    r++;

                }
            }
            using (FileStream fsXls = new FileStream(templatePathXls, FileMode.Create, FileAccess.Write))
            {

                hssfwbxls.Write(fsXls);
                fsXls.Close();
                filexls.Close();
            }


        }
        public void DataValidDonation()
        {
            spnTahunPajakDonation.IsValid = true;
            txtTempatPenandatanganDonation.IsValid = true;
            dtPenandaTanganDonation.IsValid = true;
            txtNamaPenandatanganDonation.IsValid = true;
            txtJabatanpenandatanagnDonation.IsValid = true;
        }
        public void stylexlsDonation(HSSFWorkbook hssfwb, ICell cell)
        {
            ICellStyle style = hssfwb.CreateCellStyle();

            //style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
            style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
            style.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font = hssfwb.CreateFont();
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            cell.CellStyle = style;
        }

        protected void evt_Close_Donation(object sender, EventArgs arg)
        {
            try
            {


                DataValidDonation();
                formPersistence.GetDeleteIsActiveDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
                if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                {

                    var def = this.Page as _30PvFormList.VoucherFormPage;
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlDonation.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                {
                    var def = this.Page as _80Accrued.VoucherFormPage;
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlDonation.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                {
                    var defsett = this.Page as _60SettlementForm.SettlementFormList;
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlDonation = false;
                }
                else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                {
                    var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlDonation = false;
                }

            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlDonation.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (defaccr != null)
                {
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlDonation.ClientVisible = false;
                    defaccr.clearScreenMessage();
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlDonation = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlDonation = false;
                }
            }
        }


        protected void evt_Save_Donation(object sender, EventArgs arg)
        {
            try
            {


                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                if (hidIsSaveDonation.Value=="1")
                {
                    if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                    {

                        def.clearScreenMessage();
                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                    {
                        defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                    {
                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.clearScreenMessage();
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                        return;

                    }
                }
                else
                {
                    if (ASPxEdit.AreEditorsValid(FormPanelDonation))
                    {
                        try
                        {

                            DataFieldHeader dataFieldHeader = new DataFieldHeader();
                            dataFieldHeader.CATEGORY_CODE = IsNullString(hidCategoryCodeDonation.Value);
                            dataFieldHeader.TRANSACTION_CD = IsNullInt(hidTransactionCodeDonation.Value);
                            dataFieldHeader.REFERENCE_NO = IsNullString(hidReferenceNoDonation.Value);
                            dataFieldHeader.THN_PAJAK = IsNullInt(spnTahunPajakDonation.Value);
                            dataFieldHeader.TMP_PENANDATANGAN = IsNullString(txtTempatPenandatanganDonation.Value);
                            dataFieldHeader.TGL_PENANDATANGAN = IsNullDateTime(dtPenandaTanganDonation.Value);
                            dataFieldHeader.NAMA_PENANDATANGAN = IsNullString(txtNamaPenandatanganDonation.Value);
                            dataFieldHeader.JABATAN_PENANDATANGAN = IsNullString(txtJabatanpenandatanagnDonation.Value);
                            bool IsValid = formPersistence.UpdateHeaderDonation(dataFieldHeader);
                            if (IsValid == false)
                            {
                                if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                                {

                                    def.clearScreenMessage();
                                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                                {

                                    defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                                {
                                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                                {
                                    defsettAccrfrom.clearScreenMessage();
                                    defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                            }
                            else
                            {

                                string reportPath = Server.MapPath("~/Report/RDLC/DataFieldDonation.rdlc");
                                string categoryname = formPersistence.GetCategoryNominative(hidCategoryCodeDonation.Value);
                                string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonation.xls");
                                // string templatePathSource = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldDonation.xls");
                                // string templateDoctPathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDonation.docx");
                                byte[] filedoctDonation = formPersistence.ExportDoctDonation(hidCategoryCodeDonation.Value, hidTransactionCodeDonation.Value, hidReferenceNoDonation.Value, reportPath, hidformtypeDonation.Value);
                                byte[] filexlsDonation = formPersistence.ExportXlsDonation(hidCategoryCodeDonation.Value, hidTransactionCodeDonation.Value, hidReferenceNoDonation.Value, templatePathSource, hidformtypeDonation.Value);

                                string docxName = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".pdf";
                                string pdfNamexls = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".xls";


                                bool uploadSucceded = true;
                                string errorMessage = null;


                                ftp.ftpUploadBytes(hidthnDonation.Value, hidScreenTypeDonation.Value,
                                                IsNullString(hidReferenceNoDonation.Value.Replace("|", "")),
                                                docxName,
                                                filedoctDonation,
                                                ref uploadSucceded,
                                                ref errorMessage);
                                AttachmentCategoryNominative Donationdocx = new AttachmentCategoryNominative();
                                Donationdocx.ReferenceNumber = hidReferenceNoDonation.Value;
                                Donationdocx.FileName = docxName;
                                Donationdocx.ATTACH_CD = "0";
                                Donationdocx.Description = categoryname;
                                Donationdocx.PATH = hidthnDonation.Value + "/" + hidScreenTypeDonation.Value + "/" + hidReferenceNoDonation.Value.Replace("|", "");
                                Donationdocx.FILE_TYPE = "DOC";
                                Donationdocx.CategoryCode = hidCategoryCodeDonation.Value;
                                Donationdocx.IS_ENOMINATIVE = true;
                                Donationdocx.FormType = hidformtypeDonation.Value;
                                Donationdocx.CreatedBy = hidUserNameDonation.Value;
                                formPersistence.AddAttachmenCategoryNominative(Donationdocx);

                                ftp.ftpUploadBytes(hidthnDonation.Value, hidScreenTypeDonation.Value,
                                              IsNullString(hidReferenceNoDonation.Value.Replace("|", "")),
                                              pdfNamexls,
                                              filexlsDonation,
                                              ref uploadSucceded,
                                              ref errorMessage);

                                AttachmentCategoryNominative DonationXLS = new AttachmentCategoryNominative();
                                DonationXLS.ReferenceNumber = hidReferenceNoDonation.Value;
                                DonationXLS.FileName = pdfNamexls;
                                DonationXLS.ATTACH_CD = "0";
                                DonationXLS.Description = categoryname;
                                DonationXLS.PATH = hidthnDonation.Value + "/" + hidScreenTypeDonation.Value + "/" + hidReferenceNoDonation.Value.Replace("|", "");
                                DonationXLS.FILE_TYPE = "XLS";
                                DonationXLS.CategoryCode = hidCategoryCodeDonation.Value;
                                DonationXLS.IS_ENOMINATIVE = true;
                                DonationXLS.FormType = hidformtypeDonation.Value;
                                DonationXLS.CreatedBy = hidUserNameDonation.Value;
                                formPersistence.AddAttachmenCategoryNominative(DonationXLS);
                                if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                                {

                                    def.FormData.CategoryCode = hidCategoryCodeDonation.Value;
                                    def.FormData.TransactionCodeDataField = hidTransactionCodeDonation.Value;
                                    def.FormData.ReferenceNoDataField = hidReferenceNoDonation.Value;
                                }
                                else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                                {

                                    defsettAccrfrom.FormData.CategoryCode = hidCategoryCodeDonation.Value;
                                    defsettAccrfrom.FormData.TransactionCodeDataField = hidTransactionCodeDonation.Value;
                                    defsettAccrfrom.FormData.ReferenceNoDataField = hidReferenceNoDonation.Value;
                                }

                            }
                            if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                            {

                                def.clearScreenMessage();
                                // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                                def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                                def.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeDonation.Value, hidCategoryCodeDonation.Value, hidReferenceNoDonation.Value, hidUserNameDonation.Value, hidthnDonation.Value, hidScreenTypeDonation.Value, Convert.ToInt32(hidVendorGroupDonation.Value));
                                def.PnlMasterHeader.ClientVisible = true;
                                def.PnlDonation.ClientVisible = false;

                            }
                            else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                                defsett.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeDonation.Value, hidCategoryCodeDonation.Value, hidReferenceNoDonation.Value, hidUserNameDonation.Value, hidthnDonation.Value, hidScreenTypeDonation.Value, Convert.ToInt32(hidVendorGroupDonation.Value));
                                defsett.DivMasterStt1 = true;
                                defsett.DivMasterStt2 = true;
                                defsett.PnlDonation = false;
                            }
                            else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);

                                defsettAccr.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeDonation.Value, hidCategoryCodeDonation.Value, hidReferenceNoDonation.Value, hidUserNameDonation.Value, hidthnDonation.Value, hidScreenTypeDonation.Value, Convert.ToInt32(hidVendorGroupDonation.Value));
                                defsettAccr.DivMasterStt1 = true;
                                defsettAccr.DivMasterStt2 = true;
                                defsettAccr.PnlDonation = false;
                            }
                            else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.clearScreenMessage();
                                // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                                defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                                defsettAccrfrom.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeDonation.Value, hidCategoryCodeDonation.Value, hidReferenceNoDonation.Value, hidUserNameDonation.Value, hidthnDonation.Value, hidScreenTypeDonation.Value, Convert.ToInt32(hidVendorGroupDonation.Value));
                                defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                                defsettAccrfrom.PnlDonation.ClientVisible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                            {

                                def.clearScreenMessage();
                                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);

                            }
                            else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", ex.Message);
                                return;
                            }
                            else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", ex.Message);
                                return;
                            }
                            else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.clearScreenMessage();
                                defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                            }

                        }
                    }
                }
            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlDonation.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (defaccr != null)
                {
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlDonation.ClientVisible = false;
                    defaccr.clearScreenMessage();
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlDonation = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlDonation = false;
                }
            }
        }

        protected void ASPxGridViewDonation_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {


            ASPxTextBox txtJenisSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["JENIS_SUMBANGAN"] as GridViewDataColumn, "txtJenisSumbanganDonation") as ASPxTextBox;
            ASPxTextBox txtBentukSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BENTUK_SUMBANGAN"] as GridViewDataColumn, "txtBentukSumbanganDonation") as ASPxTextBox;
            ASPxSpinEdit txtNilaiSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NILAI_SUMBANGAN"] as GridViewDataColumn, "spnNilaiSumbanganDonation") as ASPxSpinEdit;
            ASPxDateEdit txtTanggalSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["TANGGAL_SUMBANGAN"] as GridViewDataColumn, "dtTanggalsumbanganDonation") as ASPxDateEdit;
            ASPxTextBox txtNamaLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNamaLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtNPWPLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NPWP_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNPWPLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtAlamatLembaga = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["ALAMAT_LEMBAGA"] as GridViewDataColumn, "txtAlamatLembagaDonation") as ASPxTextBox;
            ASPxTextBox txtNO_TELP_LEMBAGA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_TELP_LEMBAGA"] as GridViewDataColumn, "txtNO_TELP_LEMBAGADonation") as ASPxTextBox;
            ASPxTextBox txtSARANA_PRASARANA_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["SARANA_PRASARANA_INFRASTRUKTUR"] as GridViewDataColumn, "txtSARANA_PRASARANA_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtLOKASI_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["LOKASI_INFRASTRUKTUR"] as GridViewDataColumn, "txtLOKASI_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtIZIN_MENDIRIKAN_BANGUNAN = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["IZIN_MENDIRIKAN_BANGUNAN"] as GridViewDataColumn, "txtIZIN_MENDIRIKAN_BANGUNANRDonation") as ASPxTextBox;
            ASPxTextBox txtNOMOR_REKENING = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNOMOR_REKENINGDonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_REKENING_PENERIMA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNAMA_REKENING_PENERIMADonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_BANK = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNAMA_BANKDonation") as ASPxTextBox;
            //ASPxTextBox txtNO_KTP = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_KTP"] as GridViewDataColumn, "txtNO_KTPDonation") as ASPxTextBox;
            ASPxSpinEdit txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BIAYA_PEMBANGUNAN_INFRASTRUKTUR"] as GridViewDataColumn, "spnBIAYA_PEMBANGUNAN_INFRASTRUKTURDonation") as ASPxSpinEdit;
            Donation Donation = new Donation();
            Donation.CATEGORY_CODE = hidCategoryCodeDonation.Value;
            Donation.TRANSACTION_CD = Convert.ToInt32(hidTransactionCodeDonation.Value);
            Donation.REFERENCE_NO = hidReferenceNoDonation.Value;
            Donation.NO = 1;

            Donation.JENIS_SUMBANGAN = IsNullString(txtJenisSumbangan.Value);
            Donation.BENTUK_SUMBANGAN = IsNullString(txtBentukSumbangan.Value);
            Donation.NILAI_SUMBANGAN = Convert.ToDecimal(txtNilaiSumbangan.Number);
            Donation.TANGGAL_SUMBANGAN = Convert.ToDateTime(txtTanggalSumbangan.Date);
            Donation.NAMA_LEMBAGA_PENERIMA = IsNullString(txtNamaLembagaPenerima.Value);
            Donation.NPWP_LEMBAGA_PENERIMA = FormatNPWP(IsNullString(txtNPWPLembagaPenerima.Value));
            Donation.ALAMAT_LEMBAGA = IsNullString(txtAlamatLembaga.Value);
            Donation.NO_TELP_LEMBAGA = IsNullString(txtNO_TELP_LEMBAGA.Value);
            Donation.SARANA_PRASARANA_INFRASTRUKTUR = IsNullString(txtSARANA_PRASARANA_INFRASTRUKTUR.Value);
            Donation.LOKASI_INFRASTRUKTUR = IsNullString(txtLOKASI_INFRASTRUKTUR.Value);
            Donation.IZIN_MENDIRIKAN_BANGUNAN = IsNullString(txtIZIN_MENDIRIKAN_BANGUNAN.Value);
            Donation.NOMOR_REKENING = IsNullString(txtNOMOR_REKENING.Value);
            Donation.NAMA_REKENING_PENERIMA = IsNullString(txtNAMA_REKENING_PENERIMA.Value);
            Donation.NAMA_BANK = IsNullString(txtNAMA_BANK.Value);
            //Donation.NO_KTP = IsNullString(txtNO_KTP.Value);
            Donation.BIAYA_PEMBANGUNAN_INFRASTRUKTUR = Convert.ToDecimal(txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR.Number);
            Donation.FormType = hidformtypeDonation.Value;
            Donation.CREATED_DT = DateTime.Now;
            Donation.CREATED_BY = IsNullString(hidUserNameDonation.Value);
            //  e.Cancel = true;

            formPersistence.AddDonation(Donation);
            e.Cancel = true;
            ASPxGridViewDonation.CancelEdit();

            List<Donation> _listSave = new List<Donation>();

            _listSave = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            ASPxGridViewDonation.DataSource = _listSave;
            ASPxGridViewDonation.DataBind();

        }

        protected void ASPxGridViewDonation_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            HiddenField hidSeqNo = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO"] as GridViewDataColumn, "hidSeqNoDonation") as HiddenField;
            ASPxTextBox txtJenisSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["JENIS_SUMBANGAN"] as GridViewDataColumn, "txtJenisSumbanganDonation") as ASPxTextBox;
            ASPxTextBox txtBentukSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BENTUK_SUMBANGAN"] as GridViewDataColumn, "txtBentukSumbanganDonation") as ASPxTextBox;
            ASPxSpinEdit txtNilaiSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NILAI_SUMBANGAN"] as GridViewDataColumn, "spnNilaiSumbanganDonation") as ASPxSpinEdit;
            ASPxDateEdit txtTanggalSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["TANGGAL_SUMBANGAN"] as GridViewDataColumn, "dtTanggalsumbanganDonation") as ASPxDateEdit;
            ASPxTextBox txtNamaLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNamaLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtNPWPLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NPWP_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNPWPLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtAlamatLembaga = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["ALAMAT_LEMBAGA"] as GridViewDataColumn, "txtAlamatLembagaDonation") as ASPxTextBox;
            ASPxTextBox txtNO_TELP_LEMBAGA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_TELP_LEMBAGA"] as GridViewDataColumn, "txtNO_TELP_LEMBAGADonation") as ASPxTextBox;
            ASPxTextBox txtSARANA_PRASARANA_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["SARANA_PRASARANA_INFRASTRUKTUR"] as GridViewDataColumn, "txtSARANA_PRASARANA_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtLOKASI_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["LOKASI_INFRASTRUKTUR"] as GridViewDataColumn, "txtLOKASI_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtIZIN_MENDIRIKAN_BANGUNAN = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["IZIN_MENDIRIKAN_BANGUNAN"] as GridViewDataColumn, "txtIZIN_MENDIRIKAN_BANGUNANRDonation") as ASPxTextBox;
            ASPxTextBox txtNOMOR_REKENING = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNOMOR_REKENINGDonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_REKENING_PENERIMA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNAMA_REKENING_PENERIMADonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_BANK = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNAMA_BANKDonation") as ASPxTextBox;
            //ASPxTextBox txtNO_KTP = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_KTP"] as GridViewDataColumn, "txtNO_KTPDonation") as ASPxTextBox;
            ASPxSpinEdit txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BIAYA_PEMBANGUNAN_INFRASTRUKTUR"] as GridViewDataColumn, "spnBIAYA_PEMBANGUNAN_INFRASTRUKTURDonation") as ASPxSpinEdit;
            Donation Donation = new Donation();
            Donation.SEQ_NO = IsNullInt64(hidSeqNo.Value);




            Donation.JENIS_SUMBANGAN = IsNullString(txtJenisSumbangan.Value);
            Donation.BENTUK_SUMBANGAN = IsNullString(txtBentukSumbangan.Value);
            Donation.NILAI_SUMBANGAN = Convert.ToDecimal(txtNilaiSumbangan.Number);
            Donation.TANGGAL_SUMBANGAN = Convert.ToDateTime(txtTanggalSumbangan.Date);
            Donation.NAMA_LEMBAGA_PENERIMA = IsNullString(txtNamaLembagaPenerima.Value);
            Donation.NPWP_LEMBAGA_PENERIMA = IsNullString(txtNPWPLembagaPenerima.Value);
            Donation.ALAMAT_LEMBAGA = IsNullString(txtAlamatLembaga.Value);
            Donation.NO_TELP_LEMBAGA = IsNullString(txtNO_TELP_LEMBAGA.Value);
            Donation.SARANA_PRASARANA_INFRASTRUKTUR = IsNullString(txtSARANA_PRASARANA_INFRASTRUKTUR.Value);
            Donation.LOKASI_INFRASTRUKTUR = IsNullString(txtLOKASI_INFRASTRUKTUR.Value);
            Donation.IZIN_MENDIRIKAN_BANGUNAN = IsNullString(txtIZIN_MENDIRIKAN_BANGUNAN.Value);
            Donation.NOMOR_REKENING = IsNullString(txtNOMOR_REKENING.Value);
            Donation.NAMA_REKENING_PENERIMA = IsNullString(txtNAMA_REKENING_PENERIMA.Value);
            Donation.NAMA_BANK = IsNullString(txtNAMA_BANK.Value);
            //Donation.NO_KTP = IsNullString(txtNO_KTP.Value);
            Donation.BIAYA_PEMBANGUNAN_INFRASTRUKTUR = Convert.ToDecimal(txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR.Number);

            Donation.CREATED_DT = DateTime.Now;
            Donation.CREATED_BY = IsNullString(hidUserNameDonation.Value);

            formPersistence.EditDonation(Donation);
            e.Cancel = true;
            ASPxGridViewDonation.CancelEdit();

            List<Donation> _listEdit = new List<Donation>();

            _listEdit = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            ASPxGridViewDonation.DataSource = _listEdit;
            ASPxGridViewDonation.DataBind();

        }

        protected void ASPxGridViewDonation_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var keydata = IsNullInt64(e.Keys["SEQ_NO"]);

            formPersistence.DeleteDonation(keydata);
            e.Cancel = true;


            List<Donation> _listDelete = new List<Donation>();

            _listDelete = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            ASPxGridViewDonation.DataSource = _listDelete;
            ASPxGridViewDonation.DataBind();
        }

        protected void ASPxGridViewDonation_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {

            string msgValidation = "";
            int countValidation = 0;
            var def = this.Page as _30PvFormList.VoucherFormPage;
            ASPxTextBox txtJenisSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["JENIS_SUMBANGAN"] as GridViewDataColumn, "txtJenisSumbanganDonation") as ASPxTextBox;
            ASPxTextBox txtBentukSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BENTUK_SUMBANGAN"] as GridViewDataColumn, "txtBentukSumbanganDonation") as ASPxTextBox;
            ASPxSpinEdit txtNilaiSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NILAI_SUMBANGAN"] as GridViewDataColumn, "spnNilaiSumbanganDonation") as ASPxSpinEdit;
            ASPxDateEdit txtTanggalSumbangan = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["TANGGAL_SUMBANGAN"] as GridViewDataColumn, "dtTanggalsumbanganDonation") as ASPxDateEdit;
            ASPxTextBox txtNamaLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNamaLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtNPWPLembagaPenerima = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NPWP_LEMBAGA_PENERIMA"] as GridViewDataColumn, "txtNPWPLembagaPenerimaDonation") as ASPxTextBox;
            ASPxTextBox txtAlamatLembaga = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["ALAMAT_LEMBAGA"] as GridViewDataColumn, "txtAlamatLembagaDonation") as ASPxTextBox;
            ASPxTextBox txtNO_TELP_LEMBAGA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_TELP_LEMBAGA"] as GridViewDataColumn, "txtNO_TELP_LEMBAGADonation") as ASPxTextBox;
            ASPxTextBox txtSARANA_PRASARANA_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["SARANA_PRASARANA_INFRASTRUKTUR"] as GridViewDataColumn, "txtSARANA_PRASARANA_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtLOKASI_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["LOKASI_INFRASTRUKTUR"] as GridViewDataColumn, "txtLOKASI_INFRASTRUKTURDonation") as ASPxTextBox;
            ASPxTextBox txtIZIN_MENDIRIKAN_BANGUNAN = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["IZIN_MENDIRIKAN_BANGUNAN"] as GridViewDataColumn, "txtIZIN_MENDIRIKAN_BANGUNANRDonation") as ASPxTextBox;
            ASPxTextBox txtNOMOR_REKENING = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNOMOR_REKENINGDonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_REKENING_PENERIMA = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNAMA_REKENING_PENERIMADonation") as ASPxTextBox;
            ASPxTextBox txtNAMA_BANK = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNAMA_BANKDonation") as ASPxTextBox;
            //ASPxTextBox txtNO_KTP = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["NO_KTP"] as GridViewDataColumn, "txtNO_KTPDonation") as ASPxTextBox;
            ASPxSpinEdit txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR = ASPxGridViewDonation.FindEditRowCellTemplateControl(ASPxGridViewDonation.Columns["BIAYA_PEMBANGUNAN_INFRASTRUKTUR"] as GridViewDataColumn, "spnBIAYA_PEMBANGUNAN_INFRASTRUKTURDonation") as ASPxSpinEdit;

            bool IS_JENIS_SUMBANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "JENIS_SUMBANGAN", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_BENTUK_SUMBANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "BENTUK_SUMBANGAN", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NILAI_SUMBANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NILAI_SUMBANGAN", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_TANGGAL_SUMBANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "TANGGAL_SUMBANGAN", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NAMA_LEMBAGA_PENERIMA = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NAMA_LEMBAGA_PENERIMA", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NPWP_LEMBAGA_PENERIMA = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NPWP_LEMBAGA_PENERIMA", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_ALAMAT_LEMBAGA = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "ALAMAT_LEMBAGA", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NO_TELP_LEMBAGA = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NO_TELP_LEMBAGA", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_SARANA_PRASARANA_INFRASTRUKTUR = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "SARANA_PRASARANA_INFRASTRUKTUR", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_LOKASI_INFRASTRUKTUR = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "LOKASI_INFRASTRUKTUR", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_IZIN_MENDIRIKAN_BANGUNAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "IZIN_MENDIRIKAN_BANGUNAN", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NOMOR_REKENING = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NOMOR_REKENING", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NAMA_REKENING_PENERIMA = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NAMA_REKENING_PENERIMA", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_NAMA_BANK = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NAMA_BANK", Convert.ToInt32(hidVendorGroupDonation.Value));
            //bool IS_NO_KTP = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "NO_KTP", Convert.ToInt32(hidVendorGroupDonation.Value));
            bool IS_BIAYA_PEMBANGUNAN_INFRASTRUKTUR = formPersistence.IsValidationVendorGroup(hidCategoryCodeDonation.Value, "BIAYA_PEMBANGUNAN_INFRASTRUKTUR", Convert.ToInt32(hidVendorGroupDonation.Value));

            if ((IsNullString(txtNamaLembagaPenerima.Value) == "") && IS_NAMA_LEMBAGA_PENERIMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NAMA_LEMBAGA_PENERIMA").FirstOrDefault();
                txtNamaLembagaPenerima.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if ((IsNullString(txtJenisSumbangan.Value) == "") && IS_JENIS_SUMBANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "JENIS_SUMBANGAN").FirstOrDefault();
                txtJenisSumbangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtBentukSumbangan.Value) == "") && IS_BENTUK_SUMBANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "BENTUK_SUMBANGAN").FirstOrDefault();
                txtBentukSumbangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNilaiSumbangan.Value) == "") && IS_NILAI_SUMBANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NILAI_SUMBANGAN").FirstOrDefault();
                txtNilaiSumbangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullDateTime(txtTanggalSumbangan.Date) == Convert.ToDateTime("1900-01-01")) && IS_TANGGAL_SUMBANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "TANGGAL_SUMBANGAN").FirstOrDefault();
                txtTanggalSumbangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if ((IsNullString(txtNPWPLembagaPenerima.Value) == "") && IS_NPWP_LEMBAGA_PENERIMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NPWP_LEMBAGA_PENERIMA").FirstOrDefault();
                txtNPWPLembagaPenerima.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }


            if ((IsNullString(txtNPWPLembagaPenerima.Value) == "") && IS_NPWP_LEMBAGA_PENERIMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NPWP_LEMBAGA_PENERIMA").FirstOrDefault();
                txtNPWPLembagaPenerima.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if (IS_NPWP_LEMBAGA_PENERIMA)
            {
                if (txtNPWPLembagaPenerima.Value.ToString().Trim().Length != 15)
                {
                    string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NPWP_LEMBAGA_PENERIMA").FirstOrDefault();
                    txtNPWPLembagaPenerima.IsValid = false;
                    msgValidation += _caption + " Format must be 15 characters, ";
                    countValidation++;
                }
            }
            else
            {
                if (IsNullString(txtNPWPLembagaPenerima.Value) != "")
                {
                    if (txtNPWPLembagaPenerima.Value.ToString().Trim().Length != 15)
                    {
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NPWP_LEMBAGA_PENERIMA").FirstOrDefault();
                        txtNPWPLembagaPenerima.IsValid = false;
                        msgValidation += _caption + " Format must be 15 characters, ";
                        countValidation++;
                    }
                }
            }

            if ((IsNullString(txtAlamatLembaga.Value) == "") && IS_ALAMAT_LEMBAGA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "ALAMAT_LEMBAGA").FirstOrDefault();
                txtAlamatLembaga.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNO_TELP_LEMBAGA.Value) == "") && IS_NO_TELP_LEMBAGA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NO_TELP_LEMBAGA").FirstOrDefault();
                txtNO_TELP_LEMBAGA.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtSARANA_PRASARANA_INFRASTRUKTUR.Value) == "") && IS_SARANA_PRASARANA_INFRASTRUKTUR)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "SARANA_PRASARANA_INFRASTRUKTUR").FirstOrDefault();
                txtSARANA_PRASARANA_INFRASTRUKTUR.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtLOKASI_INFRASTRUKTUR.Value) == "") && IS_LOKASI_INFRASTRUKTUR)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "LOKASI_INFRASTRUKTUR").FirstOrDefault();
                txtLOKASI_INFRASTRUKTUR.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtIZIN_MENDIRIKAN_BANGUNAN.Value) == "") && IS_IZIN_MENDIRIKAN_BANGUNAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "IZIN_MENDIRIKAN_BANGUNAN").FirstOrDefault();
                txtIZIN_MENDIRIKAN_BANGUNAN.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if ((IsNullString(txtNOMOR_REKENING.Value) == "") && IS_NOMOR_REKENING)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NOMOR_REKENING").FirstOrDefault();
                txtNOMOR_REKENING.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNAMA_REKENING_PENERIMA.Value) == "") && IS_NAMA_REKENING_PENERIMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NAMA_REKENING_PENERIMA").FirstOrDefault();
                txtNAMA_REKENING_PENERIMA.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNAMA_BANK.Value) == "") && IS_NAMA_BANK)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NAMA_BANK").FirstOrDefault();
                txtNAMA_BANK.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            //if ((IsNullString(txtNO_KTP.Value) == "") && (IS_NO_KTP = true))
            //{
            //    string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "NO_KTP").FirstOrDefault();
            //    txtNO_KTP.IsValid = false;
            //    msgValidation += _caption + ", ";
            //    countValidation++;
            //}

            if ((IsNullString(txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR.Value) =="") && IS_BIAYA_PEMBANGUNAN_INFRASTRUKTUR)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, "BIAYA_PEMBANGUNAN_INFRASTRUKTUR").FirstOrDefault();
                txtBIAYA_PEMBANGUNAN_INFRASTRUKTUR.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }


        protected void ASPxGridViewDonation_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            hidIsSaveDonation.Value = "1";
            List<Donation> _listSeqNo = new List<Donation>();
            _listSeqNo = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);

            e.NewValues["NO"] = _listSeqNo.Count() + 1;
            e.NewValues["SEQ_NO"] = _listSeqNo.Count() + 1;

            EnomVendor _Vendor = new EnomVendor();
            _Vendor = formPersistence.GetEnomVendor(hidVendorCodeDonation.Value).FirstOrDefault();

            if (_Vendor != null)
            {
                e.NewValues["NILAI_SUMBANGAN"] = 0;
                e.NewValues["TANGGAL_SUMBANGAN"] = DateTime.Now;
                e.NewValues["NAMA_LEMBAGA_PENERIMA"] = _Vendor.VENDOR_NAME;
                e.NewValues["NPWP_LEMBAGA_PENERIMA"] = _Vendor.NPWP;
                e.NewValues["ALAMAT_LEMBAGA"] = _Vendor.NpwpAddress;
                e.NewValues["BIAYA_PEMBANGUNAN_INFRASTRUKTUR"] = 0;
            }
            else
            {
                e.NewValues["NILAI_SUMBANGAN"] = 0;
                e.NewValues["TANGGAL_SUMBANGAN"] = DateTime.Now;
                e.NewValues["NAMA_LEMBAGA_PENERIMA"] = "";
                e.NewValues["ALAMAT_LEMBAGA"] = "";
                e.NewValues["BIAYA_PEMBANGUNAN_INFRASTRUKTUR"] = 0;
            }


        }

        protected void ASPxGridViewDonation_ParseValue(object sender, DevExpress.Web.Data.ASPxParseValueEventArgs ev)
        {

            List<Donation> _listSeqNo = new List<Donation>();

            _listSeqNo = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);

            if (ev.FieldName == "NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }

            if (ev.FieldName == "SEQ_NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("SEQ_NO number");
                }

        }
        public void displayheaderdonation()
        {
            if (IsNullString(hidCategoryCodeDonation.Value) != "")
            {




                foreach (GridViewColumn row in ASPxGridViewDonation.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodeDonation.Value, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            ASPxGridViewDonation.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }
        }
       

        public string IsNullString(object data)
        {
            string returndata = "";
            if (data == null)
            {
                returndata = "";
            }
            else
            {
                returndata = data.ToString();
            }
            return returndata;
        }

        public decimal IsNullDecimal(object data)
        {
            decimal returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToDecimal(data);
            }
            return returndata;
        }

        public Int32 IsNullInt(object data)
        {
            Int32 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt32(data);
            }
            return returndata;
        }

        public Int64 IsNullInt64(object data)
        {
            Int64 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt64(data);
            }
            return returndata;
        }

        public DateTime IsNullDateTime(object data)
        {
            DateTime returndata = Convert.ToDateTime("1900-01-01");
            if (data == null)
            {
                returndata = Convert.ToDateTime("1900-01-01");
            }
            else
            {
                returndata = Convert.ToDateTime(data);
            }
            return returndata;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string FormatNPWP(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string a = value.Substring(0, 2);
                string b = value.Substring(2, 3);
                string c = value.Substring(5, 3);
                string d = value.Substring(8, 1);
                string e = value.Substring(9, 3);
                string f = value.Substring(12, 3);

                return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
            }
            else
            {
                return "";
            }

        }

        protected void TahunPajakDonation_Validation(object sender, ValidationEventArgs e)
        {

            if (e.Value.ToString().Length != 4)

                e.IsValid = false;
        }


        #region Upload
        protected void btnUploadTemplateDonation_Click(object sender, EventArgs e)
        {
            hidIsSaveDonation.Value = "0";
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsett = this.Page as _60SettlementForm.SettlementFormList;
            var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            //  var def = this.Page as _30PvFormList.VoucherFormPage;
            logic.Say("BeforeUploadDonation_Click", "Upload : {0}", fuDataFieldDonationList.FileName);
            if (hidformtypeDonation.Value == "ELVIS_PVFormList")
            {

                def.clearScreenMessage();
            }
            else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
            {

                defsettAccrfrom.clearScreenMessage();
            }
            try
            {


                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(fuDataFieldDonationList.FileName).ToString().ToLower();
                string _strFileName = fuDataFieldDonationList.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                    {

                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;

                    }
                    return;
                }
                string _strFunctionId = "";
                _strFunctionId = resx.FunctionId("FCN_" + hidScreenTypeDonation.Value + "_FORM_UPLOAD");
                if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                {

                    _processId = def.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                {

                    _processId = defsett.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                {

                    _processId = defsettAccr.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                {

                    _processId = defsettAccrfrom.DoStartLog(_strFunctionId, "");
                }
                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                fuDataFieldDonationList.SaveAs(xlsFile);

                string[][] listOfErrors = new string[1][];

                bool Ok = formPersistence.UploadFileTemplateDonation(
                        hidCategoryCodeDonation.Value,
                        Convert.ToInt32(hidTransactionCodeDonation.Value),
                        hidReferenceNoDonation.Value,
                        hidVendorGroupDonation.Value,
                        DateTime.Now,
                        IsNullString(hidUserNameDonation.Value),
                        xlsFile,
                         hidformtypeDonation.Value);
                if (!Ok)
                {
                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), "TemplateDataFieldDonation.xls"
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);
                    if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                    {


                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    return;
                }
                else
                {
                    List<Donation> _listUpload = new List<Donation>();

                    _listUpload = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
                    ASPxGridViewDonation.DataSource = _listUpload;
                    ASPxGridViewDonation.DataBind();
                    if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                    }
                    else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                    {


                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                    }



                }
            }
            catch (Exception ex)
            {

                if (hidformtypeDonation.Value == "ELVIS_PVFormList")
                {

                    def.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldDonationList.Dispose();
                    return;
                }
                else if (hidformtypeDonation.Value == "ELVIS_SettlementFormList")
                {

                    defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldDonationList.Dispose();
                    return;
                }
                else if (hidformtypeDonation.Value == "ELVIS_AccrSettForm")
                {

                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldDonationList.Dispose();
                    return;
                }

                else if (hidformtypeDonation.Value == "ELVIS_AccrPVForm")
                {



                    defsettAccrfrom.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldDonationList.Dispose();
                    return;
                }
                LoggingLogic.err(ex);
            }
            fuDataFieldDonationList.Dispose();

        }

        #endregion Upload


        protected void evt_Clear_Donation(object sender, EventArgs arg)
        {
            hidIsSaveDonation.Value = "0";
            formPersistence.ClearDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            List<Donation> _listClear = new List<Donation>();

            _listClear = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
            ASPxGridViewDonation.DataSource = _listClear;
            ASPxGridViewDonation.DataBind();

            spnTahunPajakDonation.Number = DateTime.Now.Year;
            dtPenandaTanganDonation.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            txtTempatPenandatanganDonation.Value = "";
            txtNamaPenandatanganDonation.Value = "";
            txtJabatanpenandatanagnDonation.Value = "";
            DataValidDonation();

            //errtextValidation();

        }



        protected void ASPxGridViewDonation_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {

            hidIsSaveDonation.Value = "1";

        }

        protected void ASPxGridViewDonation_Cancel(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSaveDonation.Value = "0";
        }



    }
}
