﻿using BusinessLogic.VoucherForm;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxPanel;

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ELVISDashBoard.UserControl
{


    public partial class UcQuestionaire : System.Web.UI.UserControl
    {
        // public static _30PvFormList.VoucherFormPage VFervice;
        protected readonly FormPersistence formPersistence = new FormPersistence();

        //public static string hidformtypeQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string hidCategoryCodeQuestion.Value
        //{
        //    get;
        //    set;
        //}
        //public static string hidTransactionCodeQuestion.Value
        //{
        //    get;
        //    set;
        //}
        //public static string hidReferenceNoQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string hidUserNameQuestion.Value
        //{
        //    get;
        //    set;
        //}
        //public static string hidDIV_CDQuestion.Value
        //{
        //    get;
        //    set;
        //}



        //public static int hidPageSelectedCategoryQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static int hidCountSelectedCategoryQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string hidVendorCodeQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string hidVendorGroupQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string NominativeType
        //{
        //    get;
        //    set;
        //}

        //public static string hidthnQuestion.Value
        //{
        //    get;
        //    set;
        //}

        //public static string hidScreenTypeQuestion.Value
        //{
        //    get;
        //    set;
        //}
        public static Int32[] ArraySeqCategoryType
        {
            get;
            set;
        }

        //public static int hidindexCategory.Value
        //{
        //    get;
        //    set;
        //}

        protected void Page_Load(object sender, EventArgs e)
        {


        }


        //public class NavigationList<T> : List<T>
        //{
        //    private int _currentIndex = 0;
        //    public int CurrentIndex
        //    {
        //        get
        //        {
        //            if (_currentIndex > Count - 1) { _currentIndex = Count - 1; }
        //            if (_currentIndex < 0) { _currentIndex = 0; }
        //            return _currentIndex;
        //        }
        //        set { _currentIndex = value; }
        //    }

        //    public T MoveNext
        //    {
        //        get { _currentIndex++; return this[CurrentIndex]; }
        //    }

        //    public T MovePrevious
        //    {
        //        get { _currentIndex--; return this[CurrentIndex]; }
        //    }

        //    public T Current
        //    {
        //        get { return this[CurrentIndex]; }
        //    }
        //}
        public void QuestionList(string _formtype,
            string _CategoryCode,
            string _TransactionCode,
            string _ReferenceNo,
            int _PageSelectedCategory,
            int _CountSelectedCategory,
            string _UserName,
            string _DIV_CD,
            string _VendorCode,
            string _VendorGroup,
            int[] _ArraySeqCategoryType,
            string _thn,
            string _screenType,
            string gl
            )
        {



            hidformtypeQuestion.Value = _formtype;
            hidCategoryCodeQuestion.Value = _CategoryCode;
            hidTransactionCodeQuestion.Value = _TransactionCode;
            hidReferenceNoQuestion.Value = _ReferenceNo;
            hidPageSelectedCategoryQuestion.Value = Convert.ToString(_PageSelectedCategory);
            hidCountSelectedCategoryQuestion.Value = Convert.ToString(_CountSelectedCategory);
            hidUserNameQuestion.Value = _UserName;
            hidDIV_CDQuestion.Value = _DIV_CD;
            hidVendorCodeQuestion.Value = _VendorCode;
            hidVendorGroupQuestion.Value = _VendorGroup;
            ArraySeqCategoryType = _ArraySeqCategoryType;
            //Arraytest.Text = _ArraySeqCategoryType.ToString();
            hidthnQuestion.Value = _thn;
            hidglQuestion.Value = gl;
            hidScreenTypeQuestion.Value = _screenType;


            List<Question> _list = new List<Question>();
            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                var def = this.Page as _80Accrued.AccruedFormPage;
                def.clearScreenMessage();
            }

            _list = formPersistence.GetQuestion(hidCategoryCodeQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value), hidReferenceNoQuestion.Value, hidformtypeQuestion.Value);
            ASPxGridViewQuestion.DataSource = _list;
            string categoryname = formPersistence.GetCategoryNominative(hidCategoryCodeQuestion.Value);
            ASPxGridViewQuestion.Columns["DescriptionQuestion"].Caption = "Questionaire " + categoryname;
            ASPxGridViewQuestion.DataBind();

            int pageselected = Convert.ToInt32(hidPageSelectedCategoryQuestion.Value);
            hidindexCategory.Value = Array.FindIndex(ArraySeqCategoryType, x => x == pageselected).ToString();



            if (Convert.ToInt32(hidindexCategory.Value) > 0)
            {
                btnBack.ClientVisible = true;
            }
            else
            {
                btnBack.ClientVisible = false;
            }

        }

        void grid_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {


            ASPxCheckBox boxYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + e.VisibleIndex) as ASPxCheckBox;
            HiddenField hidchkYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidchkYes") as HiddenField;
            HiddenField hidWarningStatusYes = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidWarningStatusYes") as HiddenField;
            bool _hidchkYes = Convert.ToBoolean(hidchkYes.Value);
            if (boxYes.Checked == true && hidWarningStatusYes.Value == "OQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.Yellow;
                }
            }
            else if (boxYes.Checked == true && _hidchkYes == false)
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red;
                }
            }
            if (boxYes.Checked == true && hidWarningStatusYes.Value == "BQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedYes")
                {
                    e.Cell.BackColor = System.Drawing.Color.LightSeaGreen;
                }
            }
            ASPxCheckBox boxNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + e.VisibleIndex) as ASPxCheckBox;
            HiddenField hidchkNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidchkNo") as HiddenField;
            HiddenField hidWarningStatusNo = ASPxGridViewQuestion.FindRowCellTemplateControl(e.VisibleIndex, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidWarningStatusNo") as HiddenField;
            bool _hidchkNo = Convert.ToBoolean(hidchkNo.Value);

            if (boxNo.Checked == true && hidWarningStatusNo.Value == "OQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.Yellow; // or the color you want
                }
            }
            else if (boxNo.Checked == true && _hidchkNo == false)
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.Red; // or the color you want
                }
            }
            if (boxNo.Checked == true && hidWarningStatusNo.Value == "BQ")
            {
                if (e.DataColumn.FieldName == "ValueCheckedNo")
                {
                    e.Cell.BackColor = System.Drawing.Color.LightSeaGreen; // or the color you want
                }
            }



            if (boxYes.Checked == false && boxNo.Checked == false)
            {
                e.Cell.BackColor = System.Drawing.Color.Red;
            }

        }

        protected void evt_Save(object sender, EventArgs arg)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defacc = this.Page as _80Accrued.AccruedFormPage;


            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {

                def.clearScreenMessage();
            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {

                defacc.clearScreenMessage();
            }

            ASPxGridViewQuestion.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
            IList<QuestionDetail> questionDetail = new List<QuestionDetail>();

            int IsValidateCheck = 0;
            int IsValidateData = 0;
            int countdata = 0;

            for (int i = 0; i < ASPxGridViewQuestion.VisibleRowCount; i++)
            {
                countdata++;
                ASPxCheckBox boxYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + i) as ASPxCheckBox;
                ASPxCheckBox boxYesNext = ASPxGridViewQuestion.FindRowCellTemplateControl((i + 1), ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "boxYes" + (i + 1)) as ASPxCheckBox;
                HiddenField hidchkYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidchkYes") as HiddenField;
                HiddenField hidWarningStatusYes = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedYes"] as GridViewDataColumn, "hidWarningStatusYes") as HiddenField;
                bool _hidchkYes = Convert.ToBoolean(hidchkYes.Value);

                ASPxCheckBox boxNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + i) as ASPxCheckBox;
                ASPxCheckBox boxNoNext = ASPxGridViewQuestion.FindRowCellTemplateControl((i + 1), ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "boxNo" + (i + 1)) as ASPxCheckBox;
                HiddenField hidchkNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidchkNo") as HiddenField;
                HiddenField hidWarningStatusNo = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["ValueCheckedNo"] as GridViewDataColumn, "hidWarningStatusNo") as HiddenField;
                bool _hidchkNo = Convert.ToBoolean(hidchkNo.Value);

                if (boxYes.Checked == false && boxNo.Checked == false)
                {
                    IsValidateData += 1;
                }

                if ((boxNo.Checked == true && _hidchkNo == false) || (boxYes.Checked == true && _hidchkYes == false))
                {
                    IsValidateCheck += 1;
                }

                bool IsCheck = false;
                if (boxYes.Checked == true && boxNo.Checked == false)
                {
                    IsCheck = true;
                }
                else if (boxYes.Checked == false && boxNo.Checked == true)
                {
                    IsCheck = false;
                }

                HiddenField hidQuestionId = ASPxGridViewQuestion.FindRowCellTemplateControl(i, ASPxGridViewQuestion.Columns["RowNumber"] as GridViewDataColumn, "hidQuestionId") as HiddenField;
                questionDetail.Add(new QuestionDetail { QuestionId = Convert.ToInt64(hidQuestionId.Value), IsYes = IsCheck });


                if (boxYes.Checked == true || boxNo.Checked == true)
                {
                    boxYes.ClientEnabled = true;
                    boxNo.ClientEnabled = true;
                    if (countdata < ASPxGridViewQuestion.VisibleRowCount)
                    {
                        boxYesNext.ClientEnabled = true;
                        boxNoNext.ClientEnabled = true;
                    }
                }
            }
            if (IsValidateData > 0)
            {

                if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
                {
                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Questionaire harus di isi");
                    def.PnlQuestion.ClientVisible = true;
                    def.PnlMasterHeader.ClientVisible = false;
                }
                else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
                {
                    defacc.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", " Questionaire harus di isi");

                    defacc.PnlQuestion.ClientVisible = true;
                    defacc.PnlMasterHeader.ClientVisible = false;
                }



            }
            else if (IsValidateCheck > 0)
            {



                popmsgnext.ShowOnPageLoad = true;

                if (Convert.ToInt32(hidindexCategory.Value) > 0)
                {
                    btnBack.ClientVisible = true;
                }
                else
                {
                    btnBack.ClientVisible = false;
                }


                if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
                {
                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", " Answer Does Not Match with Category");
                    def.PnlQuestion.ClientVisible = true;
                    def.PnlMasterHeader.ClientVisible = false;
                }
                else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
                {

                    defacc.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", " Answer Does Not Match with Category");
                    defacc.PnlQuestion.ClientVisible = true;
                    defacc.PnlMasterHeader.ClientVisible = false;

                }




            }
            else if (IsValidateData == 0 && IsValidateCheck == 0)
            {
                formPersistence.AddQuestion(hidCategoryCodeQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value), hidReferenceNoQuestion.Value, questionDetail, hidformtypeQuestion.Value, DateTime.Now, hidUserNameQuestion.Value);

                if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
                {
                    def.FormData.CategoryCode = hidCategoryCodeQuestion.Value;
                    var tipe_category = hidCategoryCodeQuestion.Value.ToUpper();
                    def.PnlQuestion.ClientVisible = false;
                    def.PnlMasterHeader.ClientVisible = false;
                    def.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeQuestion.Value, hidCategoryCodeQuestion.Value, hidReferenceNoQuestion.Value, hidUserNameQuestion.Value, hidthnQuestion.Value, hidScreenTypeQuestion.Value, Convert.ToInt32(hidVendorGroupQuestion.Value));
                    if (def.FormData.PVTypeCode == 2)
                    {
                        def.PnlQuestion.ClientVisible = false;
                        def.PnlMasterHeader.ClientVisible = true;
                        def.FormData.CategoryCode = hidCategoryCodeQuestion.Value;
                        def.FormData.TransactionCodeDataField = hidTransactionCodeQuestion.Value;
                        def.FormData.ReferenceNoDataField = hidReferenceNoQuestion.Value;
                    }
                    else
                    {
                        if (tipe_category == "ENOM1")
                        {
                            def.PnlEntertainment.ClientVisible = true;
                            def.ucDataFieldEntertainment.EntertainmentList(hidformtypeQuestion.Value,
                                hidCategoryCodeQuestion.Value,
                                hidTransactionCodeQuestion.Value,
                                hidReferenceNoQuestion.Value,
                                hidUserNameQuestion.Value,
                                hidVendorCodeQuestion.Value,
                                hidVendorGroupQuestion.Value,
                                hidthnQuestion.Value,
                                hidScreenTypeQuestion.Value);
                        }
                        else if (tipe_category == "ENOM2")
                        {
                            def.PnlPromotion.ClientVisible = true;
                            def.ucDataFieldPromotion.PromotionList(hidformtypeQuestion.Value,
                                hidCategoryCodeQuestion.Value,
                                hidTransactionCodeQuestion.Value,
                                hidReferenceNoQuestion.Value,
                                hidUserNameQuestion.Value,
                                hidVendorCodeQuestion.Value,
                                hidVendorGroupQuestion.Value,
                                 
                                hidScreenTypeQuestion.Value,
                                 hidthnQuestion.Value);
                        }
                        else if (tipe_category == "ENOM3")
                        {
                            def.PnlDonation.ClientVisible = true;
                            def.ucDataFieldDonation.DonationList(hidformtypeQuestion.Value,
                                hidCategoryCodeQuestion.Value,
                                hidTransactionCodeQuestion.Value,
                                hidReferenceNoQuestion.Value,
                                hidUserNameQuestion.Value,
                                hidVendorCodeQuestion.Value,
                                hidVendorGroupQuestion.Value,
                                
                                hidScreenTypeQuestion.Value,
                                hidthnQuestion.Value
                                );
                        }
                    }
                }
                else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
                {
                    defacc.PostNow(ScreenMessage.STATUS_INFO, "MSTD00001INF", "Posting finish successfully");

                    //def.FormData.TransactionCodeDataField = hidTransactionCodeQuestion.Value;
                    //def.FormData.ReferenceNoDataField = hidReferenceNoQuestion.Value;
                    // string[] seqNumber = hidReferenceNoQuestion.Value.Split('|');
                    defacc.PnlQuestion.ClientVisible = false;
                    defacc.PnlMasterHeader.ClientVisible = true;
                    Int32 row = Convert.ToInt32(hidVendorCodeQuestion.Value);
                    defacc.Tabel.rows[row].CategoryCode = hidCategoryCodeQuestion.Value;
                    defacc.Tabel.rows[row].IsCategory = true;
                    defacc.ReloadGrid();
                    defacc.DetailGrid.StartEdit(row);
                    //  var datatable = defacc.Tabel.DataList.Where(x => x.ReferenceNoDataField.Equals(hidReferenceNoQuestion.Value));
                    var data = defacc.Tabel.DataList.ToList();
                    try
                    {

                        var datatable = from docs in data where docs.ReferenceNoDataField == hidReferenceNoQuestion.Value && docs.CategoryCode == hidCategoryCodeQuestion.Value.ToString() select docs;

                        if (datatable.Count()>0)
                        {
                            var _PVTypeCd = datatable != null ? datatable.Select(e => e.PVTypeCd).FirstOrDefault() : null;
                            var _PWbsNumber = datatable != null ? datatable.Select(e => e.WbsNumber).FirstOrDefault() : null;
                            if (datatable != null)
                            {
                                foreach (var item in defacc.Tabel.DataList)
                                {
                                    if (item.PVTypeCd == _PVTypeCd && item.WbsNumber == _PWbsNumber)
                                    {
                                        //bool IsAction = true;

                                        item.CategoryCode = hidCategoryCodeQuestion.Value;
                                        
                                        if (item.ReferenceNoDataField == hidReferenceNoQuestion.Value)
                                        {
                                            item.ActionControl = true;
                                        }
                                        else
                                        {
                                            item.ActionControl = false;
                                        }

                                    }

                                }
                            }
                        }
                    }
                    catch (Exception)
                    {


                    }

                    //ASPxPanel cb = sender as ASPxPanel;
                    //defacc.test(cb);

                }



            }


        }



        protected void CheckBoxYes_Init(object sender, EventArgs e)
        {
            var box = sender as ASPxCheckBox;
            var container = box.NamingContainer as GridViewDataItemTemplateContainer;
            var index = container.VisibleIndex;
            box.ID = "boxYes" + index;
            box.ClientInstanceName = "boxYes" + index;

            box.ClientSideEvents.CheckedChanged = "function(s,e){OnCheckedYesChanged(s,e," + index + ") }";
        }

        protected void CheckBoxNo_Init(object sender, EventArgs e)
        {
            var box = sender as ASPxCheckBox;
            var container = box.NamingContainer as GridViewDataItemTemplateContainer;
            var index = container.VisibleIndex;
            box.ID = "boxNo" + index;
            box.ClientInstanceName = "boxNo" + index;

            box.ClientSideEvents.CheckedChanged = "function(s,e){OnCheckedNoChanged(s,e," + index + ") }";
        }
        protected void evt_Cose(object sender, EventArgs arg)
        {
            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                def.PnlQuestion.ClientVisible = false;
                def.PnlMasterHeader.ClientVisible = true;
            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                var defacc = this.Page as _80Accrued.AccruedFormPage;
                defacc.clearScreenMessage();
                defacc.PnlQuestion.ClientVisible = false;
                defacc.PnlMasterHeader.ClientVisible = true;
                defacc.ReloadGrid();
            }
        }

        protected void evt_Back(object sender, EventArgs arg)
        {
            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();

                def.PnlQuestion.ClientVisible = true;
                def.PnlMasterHeader.ClientVisible = false;
            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                var defacc = this.Page as _80Accrued.AccruedFormPage;
                defacc.clearScreenMessage();
                defacc.PnlQuestion.ClientVisible = true;
                defacc.PnlMasterHeader.ClientVisible = false;
            }
            Int32 indexdata = Convert.ToInt32(hidindexCategory.Value);
            indexdata--;

            // hidindexCategory.Value = hidindexCategory.Value - 1;
            string groupid = getCategoryCode(ArraySeqCategoryType[indexdata]);
            hidindexCategory.Value = indexdata.ToString();
            QuestionListByCategoryCode(groupid);
            if (Convert.ToInt32(hidindexCategory.Value) > 0)
            {
                btnBack.ClientVisible = true;
            }
            else
            {
                btnBack.ClientVisible = false;
            }



        }

        protected void evt_btnext_Click(object sender, EventArgs arg)
        {
            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                def.PnlQuestion.ClientVisible = true;
                def.PnlMasterHeader.ClientVisible = false;
            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                var defacc = this.Page as _80Accrued.AccruedFormPage;
                defacc.clearScreenMessage();
                defacc.PnlQuestion.ClientVisible = true;
                defacc.PnlMasterHeader.ClientVisible = false;
            }

            Int32 indexdata = Convert.ToInt32(hidindexCategory.Value);
            indexdata++;
            //  hidindexCategory.Value = hidindexCategory.Value + 1;
            if (ArraySeqCategoryType.Length == indexdata)
            {
                indexdata = 0;
            }
            string groupid = getCategoryCode(ArraySeqCategoryType[indexdata]);
            QuestionListByCategoryCode(groupid);
            hidindexCategory.Value = indexdata.ToString();
            if (Convert.ToInt32(hidindexCategory.Value) > 0)
            {
                btnBack.ClientVisible = true;
            }
            else
            {
                btnBack.ClientVisible = false;
            }



        }
        protected void evt_cancel_Click(object sender, EventArgs arg)
        {
            PagingButton();
            ASPxGridViewQuestion.HtmlDataCellPrepared += new ASPxGridViewTableDataCellEventHandler(grid_HtmlDataCellPrepared);
        }

        protected void evt_Clear_Click(object sender, EventArgs arg)
        {

            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();

            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                var defacc = this.Page as _80Accrued.AccruedFormPage;
                defacc.clearScreenMessage();

            }

            formPersistence.DeleteQuestion(hidCategoryCodeQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value), hidReferenceNoQuestion.Value, hidformtypeQuestion.Value);
            List<Question> _list = new List<Question>();
            _list = formPersistence.GetQuestion(hidCategoryCodeQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value), hidReferenceNoQuestion.Value, hidformtypeQuestion.Value);
            ASPxGridViewQuestion.DataSource = _list;
            ASPxGridViewQuestion.DataBind();


        }



        public void QuestionListByCategoryCode(string _CategoryCode)
        {

            hidCategoryCodeQuestion.Value = _CategoryCode;



            List<Question> _list = new List<Question>();


            _list = formPersistence.GetQuestion(hidCategoryCodeQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value), hidReferenceNoQuestion.Value, hidformtypeQuestion.Value);
            ASPxGridViewQuestion.DataSource = _list;
            ASPxGridViewQuestion.DataBind();

            string categoryname = formPersistence.GetCategoryNominative(hidCategoryCodeQuestion.Value);
            ASPxGridViewQuestion.Columns["DescriptionQuestion"].Caption = "Questionaire " + categoryname;


        }

        public string getCategoryCode(int seqNumber)
        {

            string _category = "";
            if (hidformtypeQuestion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.clearScreenMessage();
                List<SelectedCategory> _list = new List<SelectedCategory>();

                _list = formPersistence.GetSelectedCategory(Convert.ToInt32(hidDIV_CDQuestion.Value), hidglQuestion.Value, Convert.ToInt32(hidTransactionCodeQuestion.Value));
                var datacat = _list.Where(x => x.GroupSeq == seqNumber).FirstOrDefault();
                if (datacat != null)
                {
                    // hidCategoryCode.Value = datacat.hidCategoryCodeQuestion.Value;
                    _category = datacat.CategoryCode.ToString();
                }
                else
                {
                    _category = "";
                }

            }
            else if (hidformtypeQuestion.Value == "ELVIS_AccrForm")
            {
                string[] wbsno = hidReferenceNoQuestion.Value.ToString().Split('|');
                string[] glacc = wbsno[0].ToString().Split('-');
                var defacc = this.Page as _80Accrued.AccruedFormPage;
                defacc.clearScreenMessage();
                List<SelectedCategoryAccrued> _list = new List<SelectedCategoryAccrued>();
                _list = formPersistence.GetSelectedCategoryAccrued(glacc[5].ToString().Trim());
                var datacat = _list.Where(x => x.GroupSeq == seqNumber).FirstOrDefault();
                if (datacat != null)
                {
                    // hidCategoryCode.Value = datacat.hidCategoryCodeQuestion.Value;
                    _category = datacat.CategoryCode.ToString();
                }
                else
                {
                    _category = "";
                }

            }

            return _category;
        }

        public void PagingButton()
        {


            //hidindexCategory.Value = Array.FindIndex(ArraySeqCategoryType, x => x == hidPageSelectedCategoryQuestion.Value);

            //string groupid = getCategoryCode(ArraySeqCategoryType[hidindexCategory.Value]);

            //QuestionListByCategoryCode(groupid);

            if (Convert.ToInt32(hidindexCategory.Value) > 0)
            {
                btnBack.ClientVisible = true;
            }
            else
            {
                btnBack.ClientVisible = false;
            }


        }


    }
}