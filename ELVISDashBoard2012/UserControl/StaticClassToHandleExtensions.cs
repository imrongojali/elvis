﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ELVISDashBoard.UserControl
{
    public static class StaticClassToHandleExtensions
    {
        public static Control FindSiblingControl(this Control control, string id)
        {
            Control parent = control.Parent;
            while (parent.GetType() != typeof(ContentPlaceHolder) && parent.GetType() != typeof(Page))
                parent = parent.Parent;

            return parent.FindControl(id);
        }
    }
}