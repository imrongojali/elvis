﻿
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;

using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.Data;
using Microsoft.Reporting.WebForms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace ELVISDashBoard.UserControl
{

    public partial class UcDataFieldPromotion : System.Web.UI.UserControl
    {
        protected readonly GlobalResourceData resx = new GlobalResourceData();
        //public static string hidCategoryCodePromotion.Value;
        //public static string hidTransactionCodePromotion.Value;
        //public static string hidReferenceNoPromotion.Value;
        //public static string hidUserNamePromotion.Value;
        //public static string hidVendorCodePromotion.Value;
        //public static string hidVendorGroupPromotion.Value;
        //public static string hidformtypePromotion.Value;
        //public static string hidScreenTypePromotion.Value;
        //public static string hidthnPromotion.Value;
        //public static bool IsSave { get; set; }
        protected readonly FormPersistence formPersistence = new FormPersistence();
        protected LogicFactory logic = new LogicFactory();

        protected FtpLogic ftp = new FtpLogic();






        protected void Page_Load(object sender, EventArgs e)
        {


           // this.InitializeCulture();

            if (hidCategoryCodePromotion.Value != "")
            {
                List<Promotion> _listData = new List<Promotion>();

                _listData = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
                //_listData = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
                if (_listData.Count() > 0)
                {

                    ASPxGridViewPromotion.DataSource = _listData;
                    ASPxGridViewPromotion.DataBind();
                }
            }



            spnTahunPajak.Number = DateTime.Now.Year;
            dtPenandaTangan.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));


        }

        protected void InitializeCulture()
        {
            CultureInfo newCulture = new System.Globalization.CultureInfo("id-ID");
            newCulture.NumberFormat.CurrencySymbol = "Rp. ";
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
        }


        public void PromotionList(string formType,
            string CategoryCodeProm,
            string TransactionCodeProm,
            string ReferenceNoProm,
            string UserNameProm,
            string VendorCode,
            string VendorGroup,
            string ScreenType,
            string thn)
        {
            hidformtypePromotion.Value = formType;
            hidCategoryCodePromotion.Value = CategoryCodeProm;
            hidTransactionCodePromotion.Value = TransactionCodeProm;
            hidReferenceNoPromotion.Value = ReferenceNoProm;
            hidUserNamePromotion.Value = UserNameProm;
            hidVendorCodePromotion.Value = VendorCode;
            hidVendorGroupPromotion.Value = VendorGroup;
            hidScreenTypePromotion.Value = ScreenType;
            hidthnPromotion.Value = thn;

            bool categorysignature = formPersistence.GetCategorySignature(CategoryCodeProm);
            txtJabatanpenandatanagn.Enabled = categorysignature;

            if (!categorysignature)
            {
                txtJabatanpenandatanagn.Text = " ";
            }
            else
            {
                txtJabatanpenandatanagn.Text = "";
            }

            DataValidPromotion();

            hidIsSavePromotion.Value = "0";

            if (hidformtypePromotion.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = TransactionCodeProm;
                def.FormData.ReferenceNoDataField = ReferenceNoProm;
            }

            else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
            {
                var def = this.Page as _80Accrued.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = TransactionCodeProm;
                def.FormData.ReferenceNoDataField = ReferenceNoProm;
            }
            formPersistence.GetDeleteIsActivePromotion(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm, hidformtypePromotion.Value);

            List<Promotion> _listData = new List<Promotion>();
            _listData = formPersistence.GetPromotion(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm, hidformtypePromotion.Value);
            ASPxGridViewPromotion.DataSource = _listData;
            ASPxGridViewPromotion.DataBind();


            if (_listData.Count > 0)
            {
                int thn_pjk_Donation = _listData.Select(x => x.THN_PAJAK).FirstOrDefault();
                DateTime PenandaTanganDonation = _listData.Select(x => x.TGL_PENANDATANGAN).FirstOrDefault();
                string TempatPenandatanganDonation = _listData.Select(x => x.TMP_PENANDATANGAN).FirstOrDefault();
                string NamaPenandaTanganDonation = _listData.Select(x => x.NAMA_PENANDATANGAN).FirstOrDefault();
                string JabatanpenandatanagnDonation = _listData.Select(x => x.JABATAN_PENANDATANGAN).FirstOrDefault();
                spnTahunPajak.Number = thn_pjk_Donation;
                txtTempatPenandatangan.Text = TempatPenandatanganDonation;
                dtPenandaTangan.Date = Convert.ToDateTime(PenandaTanganDonation.ToString("MM/dd/yyyy"));
                txtNamaPenandatangan.Text = NamaPenandaTanganDonation;
                txtJabatanpenandatanagn.Text = JabatanpenandatanagnDonation;
            }
            else
            {
                spnTahunPajak.Number = DateTime.Now.Year;
                dtPenandaTangan.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            }
            displayheaderPromotion();

            string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldPromotionTemp.xls");
            string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldPromotion.xls");
            //string excelName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
            //string pdfName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
            FileStream file = new FileStream(templatePathSource, FileMode.OpenOrCreate, FileAccess.Read);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            // sheet.GetRow(3).GetCell(0).StringCellValue.ToString().Trim();

            int i = 0;
            List<DataFieldTemplate> dataField = new List<DataFieldTemplate>();
            dataField = formPersistence.getDataFieldNameTemplate(hidCategoryCodePromotion.Value);
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRow = sheet.GetRow(5);
                    ICell cell = dataRow.GetCell(i);
                    cell.SetCellValue(item.FieldNameByExcel);

                    IRow dataRowValue = sheet.GetRow(6);
                    ICell cellValue = dataRowValue.GetCell(i);

                    EnomVendor _VendorFirst = new EnomVendor();
                    _VendorFirst = formPersistence.GetEnomVendor(hidVendorCodePromotion.Value).FirstOrDefault();
                    if (cellValue != null)
                    {
                        if (i == 0)
                        {
                            cellValue.SetCellValue(1);
                        }
                        if (i == 1)
                        {
                            cellValue.SetCellValue(_VendorFirst.VENDOR_NAME);
                        }
                        if (i == 2)
                        {
                            cellValue.SetCellValue(RemoveSpecialCharacters(_VendorFirst.NPWP));
                        }
                        if (i == 3)
                        {
                            cellValue.SetCellValue(_VendorFirst.NpwpAddress);
                        }
                    }
                    //if (i == 4)
                    //{
                    //    cellValue.SetCellValue(DateTime.Now.ToString("dd.MM.yyyy"));
                    //}

                    var _vg = (hidVendorGroupPromotion.Value == "") ? "0" : hidVendorGroupPromotion.Value;
                    bool isValidation = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, item.FieldNameByTable, Convert.ToInt32(_vg));
                    if (isValidation == true)
                    {
                        stylexls(hssfwb, cell);
                    }
                    i++;
                }
            }
            using (FileStream fs = new FileStream(templatePath, FileMode.Create, FileAccess.Write))
            {

                hssfwb.Write(fs);
                fs.Close();
                file.Close();
            }

            int r = 0;
            string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotionTemp.xls");
            string templatePathXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotion.xls");
            FileStream filexls = new FileStream(templatePathSourceXls, FileMode.OpenOrCreate, FileAccess.Read);
            HSSFWorkbook hssfwbxls = new HSSFWorkbook(filexls);
            ISheet sheetxls = hssfwbxls.GetSheet("Default");
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRowXls = sheetxls.GetRow(0);
                    ICell cellXls = dataRowXls.GetCell(r);
                    cellXls.SetCellValue(item.FieldNameByExcel);
                    stylexls(hssfwbxls, cellXls);
                    r++;

                }
            }
            using (FileStream fsXls = new FileStream(templatePathXls, FileMode.Create, FileAccess.Write))
            {

                hssfwbxls.Write(fsXls);
                fsXls.Close();
                filexls.Close();
            }


        }

        public void stylexls(HSSFWorkbook hssfwb, ICell cell)
        {
            ICellStyle style = hssfwb.CreateCellStyle();

            //style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
            style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
            style.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font = hssfwb.CreateFont();
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            cell.CellStyle = style;
        }

        protected void evt_Close_Promotion(object sender, EventArgs arg)
        {
            try
            {
                DataValidPromotion();
                formPersistence.GetDeleteIsActivePromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);

                if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                {
                    var def = this.Page as _30PvFormList.VoucherFormPage;
                    def.clearScreenMessage();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlPromotion.ClientVisible = false;
                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                {
                    var def = this.Page as _80Accrued.VoucherFormPage;
                    def.clearScreenMessage();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlPromotion.ClientVisible = false;
                }
                else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                {
                    var defsett = this.Page as _60SettlementForm.SettlementFormList;
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlPromotion = false;

                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                {
                    var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlPromotion = false;
                }
            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.clearScreenMessage();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlPromotion.ClientVisible = false;
                }
                else if (defaccr != null)
                {
                    defaccr.clearScreenMessage();
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlPromotion.ClientVisible = false;
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlPromotion = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlPromotion = false;
                }
            }
        }

        protected void evt_Clear_Promotion(object sender, EventArgs arg)
        {
            hidIsSavePromotion.Value = "0";
            formPersistence.ClearPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
            List<Promotion> _listClear = new List<Promotion>();
            _listClear = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
            ASPxGridViewPromotion.DataSource = _listClear;
            ASPxGridViewPromotion.DataBind();
            spnTahunPajak.Number = DateTime.Now.Year;
            dtPenandaTangan.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            txtTempatPenandatangan.Value = "";
            txtNamaPenandatangan.Value = "";
            txtJabatanpenandatanagn.Value = "";
            DataValidPromotion();

        }

        protected void evt_Save_Promotion(object sender, EventArgs arg)
        {
            try
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
                if (hidIsSavePromotion.Value=="1")
                {


                    if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                    {

                        def.clearScreenMessage();
                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                    {
                        defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                    {
                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "InValid Save");
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.clearScreenMessage();
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                        return;

                    }
                }
                else
                {

                    if (ASPxEdit.AreEditorsValid(FormPanelPromotion))
                    {
                        try
                        {

                            DataFieldHeader dataFieldHeader = new DataFieldHeader();
                            dataFieldHeader.CATEGORY_CODE = IsNullString(hidCategoryCodePromotion.Value);
                            dataFieldHeader.TRANSACTION_CD = IsNullInt(hidTransactionCodePromotion.Value);
                            dataFieldHeader.REFERENCE_NO = IsNullString(hidReferenceNoPromotion.Value);
                            dataFieldHeader.THN_PAJAK = IsNullInt(spnTahunPajak.Value);
                            dataFieldHeader.TMP_PENANDATANGAN = IsNullString(txtTempatPenandatangan.Value);
                            dataFieldHeader.TGL_PENANDATANGAN = IsNullDateTime(dtPenandaTangan.Value);
                            dataFieldHeader.NAMA_PENANDATANGAN = IsNullString(txtNamaPenandatangan.Value);
                            dataFieldHeader.JABATAN_PENANDATANGAN = IsNullString(txtJabatanpenandatanagn.Value);
                            bool IsValid = formPersistence.UpdateHeaderPromotion(dataFieldHeader);
                            if (IsValid == false)
                            {
                                if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                                {

                                    def.clearScreenMessage();
                                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                                {
                                    defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                                {
                                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                    return;
                                }
                                else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                                {
                                    defsettAccrfrom.clearScreenMessage();
                                    defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                    return;

                                }


                            }
                            else
                            {

                                string categoryname = formPersistence.GetCategoryNominative(hidCategoryCodePromotion.Value);

                                string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotion.xls");
                                string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotionTest.xls");
                                string reportPath = Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc");
                                // urlrdlc.Text = reportPath + " " + templatePathSource + " " + templatePath + " " + reportPath;
                                byte[] filexlspromotion = formPersistence.ExportXlsPromotion(hidCategoryCodePromotion.Value, hidTransactionCodePromotion.Value, hidReferenceNoPromotion.Value, templatePathSource, templatePath, hidformtypePromotion.Value);
                                string pdfName = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".pdf";
                                string pdfNamexls = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".xls";
                                byte[] filepdfpromotion = formPersistence.ExportPdfPromotion(hidCategoryCodePromotion.Value, hidTransactionCodePromotion.Value, hidReferenceNoPromotion.Value, reportPath, hidformtypePromotion.Value);


                                bool uploadSucceded = true;
                                string errorMessage = null;


                                ftp.ftpUploadBytes(hidthnPromotion.Value, hidScreenTypePromotion.Value,
                                            IsNullString(hidReferenceNoPromotion.Value.Replace("|", "")),
                                            pdfName,
                                            filepdfpromotion,
                                            ref uploadSucceded,
                                            ref errorMessage);

                                AttachmentCategoryNominative promotionPdf = new AttachmentCategoryNominative();
                                promotionPdf.ReferenceNumber = hidReferenceNoPromotion.Value;
                                promotionPdf.FileName = pdfName;
                                promotionPdf.ATTACH_CD = "0";
                                promotionPdf.Description = categoryname;
                                promotionPdf.PATH = hidthnPromotion.Value + "/" + hidScreenTypePromotion.Value + "/" + hidReferenceNoPromotion.Value.Replace("|", "");
                                promotionPdf.FILE_TYPE = "PDF";
                                promotionPdf.CategoryCode = hidCategoryCodePromotion.Value;
                                promotionPdf.IS_ENOMINATIVE = true;
                                promotionPdf.FormType = hidformtypePromotion.Value;
                                promotionPdf.CreatedBy = hidUserNamePromotion.Value;
                                formPersistence.AddAttachmenCategoryNominative(promotionPdf);

                                ftp.ftpUploadBytes(hidthnPromotion.Value, hidScreenTypePromotion.Value,
                                          IsNullString(hidReferenceNoPromotion.Value.Replace("|", "")),
                                          pdfNamexls,
                                          filexlspromotion,
                                          ref uploadSucceded,
                                          ref errorMessage);


                                AttachmentCategoryNominative promotionXLS = new AttachmentCategoryNominative();
                                promotionXLS.ReferenceNumber = hidReferenceNoPromotion.Value;
                                promotionXLS.FileName = pdfNamexls;
                                promotionXLS.ATTACH_CD = "0";
                                promotionXLS.Description = categoryname;
                                promotionXLS.PATH = hidthnPromotion.Value + "/" + hidScreenTypePromotion.Value + "/" + hidReferenceNoPromotion.Value.Replace("|", "");
                                promotionXLS.FILE_TYPE = "XLS";
                                promotionXLS.CategoryCode = hidCategoryCodePromotion.Value;
                                promotionXLS.IS_ENOMINATIVE = true;
                                promotionXLS.FormType = hidformtypePromotion.Value;
                                promotionXLS.CreatedBy = hidUserNamePromotion.Value;
                                formPersistence.AddAttachmenCategoryNominative(promotionXLS);
                                if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                                {

                                    def.FormData.CategoryCode = hidCategoryCodePromotion.Value;
                                    def.FormData.TransactionCodeDataField = hidTransactionCodePromotion.Value;
                                    def.FormData.ReferenceNoDataField = hidReferenceNoPromotion.Value;
                                }
                                else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                                {

                                    defsettAccrfrom.FormData.CategoryCode = hidCategoryCodePromotion.Value;
                                    defsettAccrfrom.FormData.TransactionCodeDataField = hidTransactionCodePromotion.Value;
                                    defsettAccrfrom.FormData.ReferenceNoDataField = hidReferenceNoPromotion.Value;
                                }


                            }



                            if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                            {

                                def.clearScreenMessage();
                                // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                                def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");

                                def.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypePromotion.Value, hidCategoryCodePromotion.Value, hidReferenceNoPromotion.Value, hidUserNamePromotion.Value, hidthnPromotion.Value, hidScreenTypePromotion.Value, Convert.ToInt32(hidVendorGroupPromotion.Value));
                                def.PnlMasterHeader.ClientVisible = true;
                                def.PnlPromotion.ClientVisible = false;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                                defsett.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypePromotion.Value, hidCategoryCodePromotion.Value, hidReferenceNoPromotion.Value, hidUserNamePromotion.Value, hidthnPromotion.Value, hidScreenTypePromotion.Value, Convert.ToInt32(hidVendorGroupPromotion.Value));
                                defsett.DivMasterStt1 = true;
                                defsett.DivMasterStt2 = true;
                                defsett.PnlPromotion = false;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                                defsettAccr.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypePromotion.Value, hidCategoryCodePromotion.Value, hidReferenceNoPromotion.Value, hidUserNamePromotion.Value, hidthnPromotion.Value, hidScreenTypePromotion.Value, Convert.ToInt32(hidVendorGroupPromotion.Value));
                                defsettAccr.DivMasterStt1 = true;
                                defsettAccr.DivMasterStt2 = true;
                                defsettAccr.PnlPromotion = false;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                            {


                                defsettAccrfrom.clearScreenMessage();
                                // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                                defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");

                                defsettAccrfrom.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypePromotion.Value, hidCategoryCodePromotion.Value, hidReferenceNoPromotion.Value, hidUserNamePromotion.Value, hidthnPromotion.Value, hidScreenTypePromotion.Value, Convert.ToInt32(hidVendorGroupPromotion.Value));
                                defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                                defsettAccrfrom.PnlPromotion.ClientVisible = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                            {

                                def.clearScreenMessage();
                                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                                return;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", ex.Message);
                                return;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", ex.Message);
                                return;
                            }
                            else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.clearScreenMessage();
                                defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                                return;

                            }
                        }
                    }
                }
            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.clearScreenMessage();
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlPromotion.ClientVisible = false;
                }
                else if (defaccr != null)
                {
                    defaccr.clearScreenMessage();
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlPromotion.ClientVisible = false;
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlPromotion = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlPromotion = false;
                }
            }
        }
        protected void ASPxGridViewPromotion_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {


            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;

            Promotion promotion = new Promotion();
            promotion.CATEGORY_CODE = hidCategoryCodePromotion.Value;
            promotion.TRANSACTION_CD = Convert.ToInt32(hidTransactionCodePromotion.Value);
            promotion.REFERENCE_NO = hidReferenceNoPromotion.Value;
            promotion.NO = 1;

            promotion.NAMA = IsNullString(txtNama.Value);
            promotion.NPWP = FormatNPWP(IsNullString(txtNPWP.Value));
            promotion.ALAMAT = IsNullString(txtAlamat.Value);
            promotion.FormType = IsNullString(hidformtypePromotion.Value);

            promotion.CREATED_DT = DateTime.Now;
            promotion.CREATED_BY = IsNullString(hidUserNamePromotion.Value);
            //  e.Cancel = true;

            formPersistence.AddPromotion(promotion);
            e.Cancel = true;
            ASPxGridViewPromotion.CancelEdit();

            List<Promotion> _listSave = new List<Promotion>();

            _listSave = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
            ASPxGridViewPromotion.DataSource = _listSave;
            ASPxGridViewPromotion.DataBind();


        }

        protected void ASPxGridViewPromotion_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            HiddenField hidSeqNo = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NO"] as GridViewDataColumn, "hidSeqNo") as HiddenField;
            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;

            Promotion promotion = new Promotion();
            promotion.SEQ_NO = IsNullInt64(hidSeqNo.Value);



            promotion.NAMA = IsNullString(txtNama.Value);
            promotion.NPWP = FormatNPWP(IsNullString(txtNPWP.Value));
            promotion.ALAMAT = IsNullString(txtAlamat.Value);

            promotion.CREATED_DT = DateTime.Now;
            promotion.CREATED_BY = IsNullString(hidUserNamePromotion.Value);

            formPersistence.EditPromotion(promotion);
            e.Cancel = true;
            ASPxGridViewPromotion.CancelEdit();

            List<Promotion> _listEdit = new List<Promotion>();

            _listEdit = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
            ASPxGridViewPromotion.DataSource = _listEdit;
            ASPxGridViewPromotion.DataBind();


        }

        protected void ASPxGridViewPromotion_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var keydata = IsNullInt64(e.Keys["SEQ_NO"]);

            formPersistence.DeletePromotion(keydata);
            e.Cancel = true;


            List<Promotion> _listDelete = new List<Promotion>();

            _listDelete = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
            ASPxGridViewPromotion.DataSource = _listDelete;
            ASPxGridViewPromotion.DataBind();

        }

        protected void ASPxGridViewPromotion_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {

            string msgValidation = "";
            int countValidation = 0;

            ASPxTextBox txtNama = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NAMA"] as GridViewDataColumn, "txtNama") as ASPxTextBox;
            ASPxTextBox txtNPWP = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["NPWP"] as GridViewDataColumn, "txtNPWP") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewPromotion.FindEditRowCellTemplateControl(ASPxGridViewPromotion.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;

            bool IS_NAMA = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NAMA", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_NPWP = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NPWP", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_ALAMAT = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "ALAMAT", Convert.ToInt32(hidVendorGroupPromotion.Value));

            if ((IsNullString(txtNama.Value) == "") && IS_NAMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NAMA").FirstOrDefault();
                txtNama.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if ((IsNullString(txtNPWP.Value) == "") && IS_NPWP)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NPWP").FirstOrDefault();
                txtNPWP.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if (IS_NPWP)
            {
                if (txtNPWP.Value.ToString().Trim().Length != 15)
                {
                    string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NPWP").FirstOrDefault();
                    txtNPWP.IsValid = false;
                    msgValidation += _caption + " Format must be 15 characters, ";
                    countValidation++;
                }
            }
            else
            {
                if (IsNullString(txtNPWP.Value) != "")
                {
                    if (txtNPWP.Value.ToString().Trim().Length != 15)
                    {
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NPWP").FirstOrDefault();
                        txtNPWP.IsValid = false;
                        msgValidation += _caption + " Format must be 15 characters, ";
                        countValidation++;
                    }
                }
            }
            if ((IsNullString(txtAlamat.Value) == "") && IS_ALAMAT)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "ALAMAT").FirstOrDefault();
                txtAlamat.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }


            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }
        protected void ASPxGridViewPromotion_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSavePromotion.Value = "1";
        }

        protected void gvDetail_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSavePromotion.Value = "1";
        }
        protected void ASPxGridViewPromotion_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            hidIsSavePromotion.Value = "1";
            List<Promotion> _listSeqNo = new List<Promotion>();
            _listSeqNo = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);

            e.NewValues["NO"] = _listSeqNo.Count() + 1;
            e.NewValues["SEQ_NO"] = _listSeqNo.Count() + 1;

            EnomVendor _Vendor = new EnomVendor();
            _Vendor = formPersistence.GetEnomVendor(hidVendorCodePromotion.Value).FirstOrDefault();
            if (_Vendor != null)
            {
                
                e.NewValues["NAMA"] = _Vendor.VENDOR_NAME;
                e.NewValues["NPWP"] = _Vendor.NPWP;
                e.NewValues["ALAMAT"] = _Vendor.NpwpAddress;
            }
            else
            {
                e.NewValues["NAMA"] = "";
                e.NewValues["NPWP"] = "";
                e.NewValues["ALAMAT"] = "";
            }



        }

        protected void ASPxGridViewPromotion_ParseValue(object sender, DevExpress.Web.Data.ASPxParseValueEventArgs ev)
        {

            List<Promotion> _listSeqNo = new List<Promotion>();

            _listSeqNo = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);

            if (ev.FieldName == "NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }
            if (ev.FieldName == "NAMA")
                try
                {
                    ev.Value = Convert.ToString(ev.Value);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }
            if (ev.FieldName == "SEQ_NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("SEQ_NO number");
                }

        }

        public void displayheaderPromotion()
        {
            if (IsNullString(hidCategoryCodePromotion.Value) != "")
            {




                foreach (GridViewColumn row in ASPxGridViewPromotion.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            ASPxGridViewPromotion.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }
        }

       

        public string IsNullString(object data)
        {
            string returndata = "";
            if (data == null)
            {
                returndata = "";
            }
            else
            {
                returndata = data.ToString();
            }
            return returndata;
        }

        public decimal IsNullDecimal(object data)
        {
            decimal returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToDecimal(data);
            }
            return returndata;
        }

        public Int32 IsNullInt(object data)
        {
            Int32 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt32(data);
            }
            return returndata;
        }

        public Int64 IsNullInt64(object data)
        {
            Int64 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt64(data);
            }
            return returndata;
        }

        public DateTime IsNullDateTime(object data)
        {
            DateTime returndata = Convert.ToDateTime("1900-01-01");
            if (data == null)
            {
                returndata = Convert.ToDateTime("1900-01-01");
            }
            else
            {
                returndata = Convert.ToDateTime(data);
            }
            return returndata;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string FormatNPWP(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string a = value.Substring(0, 2);
                string b = value.Substring(2, 3);
                string c = value.Substring(5, 3);
                string d = value.Substring(8, 1);
                string e = value.Substring(9, 3);
                string f = value.Substring(12, 3);

                return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
            }
            else
            {
                return "";
            }

        }

        protected void TahunPajak_Validation(object sender, ValidationEventArgs e)
        {

            if (e.Value.ToString().Length != 4)

                e.IsValid = false;
        }


        #region Upload
        protected void btnUploadTemplatePromotion_Click(object sender, EventArgs e)
        {
            hidIsSavePromotion.Value = "0";
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsett = this.Page as _60SettlementForm.SettlementFormList;
            var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            logic.Say("BeforeUploadPromotion_Click", "Upload : {0}", fuDataFieldPromotionList.FileName);

            if (hidformtypePromotion.Value == "ELVIS_PVFormList")
            {

                def.clearScreenMessage();
            }
            else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
            {

                defsettAccrfrom.clearScreenMessage();
            }
            try
            {


                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(fuDataFieldPromotionList.FileName).ToString().ToLower();
                string _strFileName = fuDataFieldPromotionList.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                    {



                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;

                    }
                    return;
                }
                string _strFunctionId = "";
                _strFunctionId = resx.FunctionId("FCN_" + hidScreenTypePromotion.Value + "_FORM_UPLOAD");

                if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                {

                    _processId = def.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                {

                    _processId = defsett.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                {

                    _processId = defsettAccr.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                {

                    _processId = defsettAccrfrom.DoStartLog(_strFunctionId, "");
                }
                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                fuDataFieldPromotionList.SaveAs(xlsFile);

                string[][] listOfErrors = new string[1][];

                bool Ok = formPersistence.UploadFileTemplatePromotion(
                        hidCategoryCodePromotion.Value,
                        Convert.ToInt32(hidTransactionCodePromotion.Value),
                        hidReferenceNoPromotion.Value,
                        hidVendorGroupPromotion.Value,
                        DateTime.Now,
                        IsNullString(hidUserNamePromotion.Value),
                        xlsFile,
                        hidformtypePromotion.Value);
                if (!Ok)
                {
                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), "TemplateDataFieldPromotion.xls"
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);


                    if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    return;
                }
                else
                {
                    List<Promotion> _listUpload = new List<Promotion>();

                    _listUpload = formPersistence.GetPromotion(hidCategoryCodePromotion.Value, Convert.ToInt32(hidTransactionCodePromotion.Value), hidReferenceNoPromotion.Value, hidformtypePromotion.Value);
                    ASPxGridViewPromotion.DataSource = _listUpload;
                    ASPxGridViewPromotion.DataBind();
                    if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);

                    }
                    else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");


                    }

                }

            }
            catch (Exception ex)
            {
                if (hidformtypePromotion.Value == "ELVIS_PVFormList")
                {

                    def.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldPromotionList.Dispose();
                    return;
                }
                else if (hidformtypePromotion.Value == "ELVIS_SettlementFormList")
                {

                    defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldPromotionList.Dispose();
                    return;
                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrSettForm")
                {

                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldPromotionList.Dispose();
                    return;
                }
                else if (hidformtypePromotion.Value == "ELVIS_AccrPVForm")
                {
                    defsettAccrfrom.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldPromotionList.Dispose();
                    return;

                }
                LoggingLogic.err(ex);
            }
            fuDataFieldPromotionList.Dispose();

        }

        #endregion Upload

        //protected void gvMaster_DetailRowGetButtonVisibility(object sender, ASPxGridViewDetailRowButtonEventArgs e)
        //{
        //    User currentUser = Users.Find(u => u.ID == (int)gvMaster.GetRowValues(e.VisibleIndex, "ID"));

        //    if (!currentUser.HasProjects())
        //        e.ButtonState = GridViewDetailRowButtonState.Hidden;
        //}
        protected void gvDetail_Init(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            //var container = grid.NamingContainer as GridViewDataItemTemplateContainer;
            //var index = container.VisibleIndex;

            var def = this.Page as _30PvFormList.VoucherFormPage;
            if (IsNullString(hidCategoryCodePromotion.Value) != "")
            {




                foreach (GridViewColumn row in grid.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            grid.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }

            Int64 seqNo = (Int64)grid.GetMasterRowKeyValue();

            List<PromotionDetail> _listDetail = new List<PromotionDetail>();

            _listDetail = formPersistence.GetPromotionDetail(seqNo);

            grid.DataSource = _listDetail.ToList();

        }

        protected void gvDetail_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            ASPxGridView gridDetail = sender as ASPxGridView;

            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            PromotionDetail promotionDetail = new PromotionDetail();
            Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();

            promotionDetail.SEQ_NO = seqNo;
            promotionDetail.TANGGAL = IsNullDateTime(dtTanggal.Value);
            promotionDetail.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            promotionDetail.JUMLAH = IsNullDecimal(spnJumlah.Value);
            promotionDetail.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            promotionDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            promotionDetail.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            promotionDetail.JENIS_PPH = IsNullString(txtJenisPph.Value);
            promotionDetail.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            promotionDetail.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            promotionDetail.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            promotionDetail.NAMA_BANK = IsNullString(txtNamaBank.Value);
            promotionDetail.NO_KTP = IsNullString(txtNoKtp.Value);
            promotionDetail.CREATED_DT = DateTime.Now;
            promotionDetail.CREATED_BY = IsNullString(hidUserNamePromotion.Value);
            var def = this.Page as _30PvFormList.VoucherFormPage;
            formPersistence.AddPromotionDetail(promotionDetail);
            e.Cancel = true;
            gridDetail.CancelEdit();

            List<PromotionDetail> _listSaveDetail = new List<PromotionDetail>();

            _listSaveDetail = formPersistence.GetPromotionDetail(seqNo);
            gridDetail.DataSource = _listSaveDetail;
            gridDetail.DataBind();

        }

        protected void gvDetail_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;


            var keydata = IsNullInt64(e.Keys["SEQ_NO_DETAIL"]);

            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;
            PromotionDetail promotionDetail = new PromotionDetail();
            promotionDetail.SEQ_NO_DETAIL = Convert.ToInt64(keydata);
            promotionDetail.TANGGAL = IsNullDateTime(dtTanggal.Value);
            promotionDetail.BENTUK_DAN_JENIS_BIAYA = IsNullString(txtBentukDanJenisBiaya.Value);
            promotionDetail.JUMLAH = IsNullDecimal(spnJumlah.Value);
            promotionDetail.JUMLAH_GROSS_UP = IsNullDecimal(spnJumlahGrossUp.Value);
            promotionDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            promotionDetail.JUMLAH_PPH = IsNullDecimal(spnJumlahPph.Value);
            promotionDetail.JENIS_PPH = IsNullString(txtJenisPph.Value);
            promotionDetail.JUMLAH_NET = IsNullDecimal(spnJumlahNet.Value);
            promotionDetail.NOMOR_REKENING = IsNullString(txtNoRekening.Value);
            promotionDetail.NAMA_REKENING_PENERIMA = IsNullString(txtNamaRekeningPenreima.Value);
            promotionDetail.NAMA_BANK = IsNullString(txtNamaBank.Value);
            promotionDetail.NO_KTP = IsNullString(txtNoKtp.Value);
            promotionDetail.CREATED_DT = DateTime.Now;
            promotionDetail.CREATED_BY = IsNullString(hidUserNamePromotion.Value);

            formPersistence.EditPromotionDetail(promotionDetail);
            e.Cancel = true;
            gridDetail.CancelEdit();
            Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();
            List<PromotionDetail> _listDetailEdit = new List<PromotionDetail>();

            _listDetailEdit = formPersistence.GetPromotionDetail(seqNo);
            gridDetail.DataSource = _listDetailEdit;
            gridDetail.DataBind();


        }

        protected void gvDetail_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;
            // Int64 seqNo = (Int64)gridDetail.GetMasterRowKeyValue();
            var keydata = IsNullInt64(e.Keys["SEQ_NO_DETAIL"]);

            Int64 seqnoheader = formPersistence.DeletePromotionDetail(keydata);
            e.Cancel = true;
            List<PromotionDetail> _listDetailDelete = new List<PromotionDetail>();
            _listDetailDelete = formPersistence.GetPromotionDetail(seqnoheader);
            gridDetail.DataSource = _listDetailDelete;
            gridDetail.DataBind();

        }
        public void DataValidPromotion()
        {
            spnTahunPajak.IsValid = true;
            txtTempatPenandatangan.IsValid = true;
            dtPenandaTangan.IsValid = true;
            txtNamaPenandatangan.IsValid = true;
            txtJabatanpenandatanagn.IsValid = true;
        }
        protected void gvDetail_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            ASPxGridView gridDetail = sender as ASPxGridView;
            string msgValidation = "";
            int countValidation = 0;

            ASPxDateEdit dtTanggal = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggal") as ASPxDateEdit;
            ASPxTextBox txtBentukDanJenisBiaya = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["BENTUK_DAN_JENIS_BIAYA"] as GridViewDataColumn, "txtBentukDanJenisBiaya") as ASPxTextBox;
            ASPxSpinEdit spnJumlah = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH"] as GridViewDataColumn, "spnJumlah") as ASPxSpinEdit;
            ASPxSpinEdit spnJumlahGrossUp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_GROSS_UP"] as GridViewDataColumn, "spnJumlahGrossUp") as ASPxSpinEdit;
            ASPxTextBox txtKeterangan = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            ASPxSpinEdit spnJumlahPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_PPH"] as GridViewDataColumn, "spnJumlahPph") as ASPxSpinEdit;
            ASPxTextBox txtJenisPph = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JENIS_PPH"] as GridViewDataColumn, "txtJenisPph") as ASPxTextBox;
            ASPxSpinEdit spnJumlahNet = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["JUMLAH_NET"] as GridViewDataColumn, "spnJumlahNet") as ASPxSpinEdit;
            ASPxTextBox txtNoRekening = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NOMOR_REKENING"] as GridViewDataColumn, "txtNoRekening") as ASPxTextBox;
            ASPxTextBox txtNamaRekeningPenreima = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_REKENING_PENERIMA"] as GridViewDataColumn, "txtNamaRekeningPenreima") as ASPxTextBox;
            ASPxTextBox txtNamaBank = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NAMA_BANK"] as GridViewDataColumn, "txtNamaBank") as ASPxTextBox;
            ASPxTextBox txtNoKtp = gridDetail.FindEditRowCellTemplateControl(gridDetail.Columns["NO_KTP"] as GridViewDataColumn, "txtNoKtp") as ASPxTextBox;

            bool IS_TANGGAL = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "TANGGAL", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_BENTUK_DAN_JENIS_BIAYA = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "BENTUK_DAN_JENIS_BIAYA", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_JUMLAH = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "JUMLAH", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_JUMLAH_GROSS_UP = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "JUMLAH_GROSS_UP", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_KETERANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "KETERANGAN", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_JUMLAH_PPH = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "JUMLAH_PPH", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_JENIS_PPH = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "JENIS_PPH", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_JUMLAH_NET = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "JUMLAH_NET", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_NOMOR_REKENING = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NOMOR_REKENING", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_NAMA_REKENING_PENERIMA = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NAMA_REKENING_PENERIMA", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_NAMA_BANK = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NAMA_BANK", Convert.ToInt32(hidVendorGroupPromotion.Value));
            bool IS_NO_KTP = formPersistence.IsValidationVendorGroup(hidCategoryCodePromotion.Value, "NO_KTP", Convert.ToInt32(hidVendorGroupPromotion.Value));

            if ((IsNullDateTime(dtTanggal.Value) == Convert.ToDateTime("1900-01-01")) && IS_TANGGAL)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "TANGGAL").FirstOrDefault();
                dtTanggal.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtBentukDanJenisBiaya.Value) == "") && IS_BENTUK_DAN_JENIS_BIAYA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault();
                txtBentukDanJenisBiaya.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(spnJumlah.Value) == "") && (IS_JUMLAH))
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "JUMLAH").FirstOrDefault();
                spnJumlah.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(spnJumlahGrossUp.Value) == "") && IS_JUMLAH_GROSS_UP)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "JUMLAH_GROSS_UP").FirstOrDefault();
                spnJumlahGrossUp.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtKeterangan.Value) == "") && IS_KETERANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "KETERANGAN").FirstOrDefault();
                txtKeterangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(spnJumlahPph.Value) == "") && IS_JUMLAH_PPH)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "JUMLAH_PPH").FirstOrDefault();
                spnJumlahPph.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtJenisPph.Value) == "") && IS_JENIS_PPH)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "JENIS_PPH").FirstOrDefault();
                txtJenisPph.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(spnJumlahNet.Value) == "") && IS_JUMLAH_NET)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "JUMLAH_NET").FirstOrDefault();
                spnJumlahNet.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNoRekening.Value) == "") && IS_NOMOR_REKENING)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NOMOR_REKENING").FirstOrDefault();
                txtNoRekening.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNamaRekeningPenreima.Value) == "") && IS_NAMA_REKENING_PENERIMA)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NAMA_REKENING_PENERIMA").FirstOrDefault();
                txtNamaRekeningPenreima.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNamaBank.Value) == "") && IS_NAMA_BANK)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NAMA_BANK").FirstOrDefault();
                txtNamaBank.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtNoKtp.Value) == "") && IS_NO_KTP)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NO_KTP").FirstOrDefault();
                txtNoKtp.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if (IS_NO_KTP)
            {
                if (txtNoKtp.Value.ToString().Trim().Length != 16)
                {
                    string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NO_KTP").FirstOrDefault();
                    txtNoKtp.IsValid = false;
                    msgValidation += _caption + " Format must be 16 characters, ";
                    countValidation++;

                }
            }
            else
            {
                if (IsNullString(txtNoKtp.Value) != "")
                {
                    if (txtNoKtp.Value.ToString().Trim().Length != 16)
                    {
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodePromotion.Value, "NO_KTP").FirstOrDefault();
                        txtNoKtp.IsValid = false;
                        msgValidation += _caption + " Format must be 16 characters, ";
                        countValidation++;

                    }
                }
            }

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }

        protected void gvDetail_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            hidIsSavePromotion.Value = "1";
            e.NewValues["TANGGAL"] = DateTime.Now;
            e.NewValues["JUMLAH"] = 0;
            e.NewValues["JUMLAH_GROSS_UP"] = 0;
            e.NewValues["JUMLAH_PPH"] = 0;
            e.NewValues["JUMLAH_NET"] = 0;

        }



        protected void ASPxGridViewPromotion_Cancel(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {

            hidIsSavePromotion.Value = "0";

        }

        protected void gvDetail_Cancel(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSavePromotion.Value = "0";
        }

    }

}
