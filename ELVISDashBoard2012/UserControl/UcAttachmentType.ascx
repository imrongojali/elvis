﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UcAttachmentType.ascx.cs" Inherits="ELVISDashBoard.UserControl.UcAttachmentType" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxHiddenField" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallbackPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<script type="text/javascript">
    //function UploadAttachmentEnom_Click(s, e) {

    //    var fn = $("#uploadAttachmentEnom").val();
    //    if (fn == "" || fn == null) {
    //        alert("Browse file to Upload");
    //        return false;
    //    }
    //    if (fn.indexOf("&") >= 0 || fn.indexOf("+") >= 0 || fn.indexOf("%") >= 0 || fn.indexOf("?") >= 0 || fn.indexOf("*") >= 0) {
    //        return confirm("File name contains banned & + % * ? chars \r\n Rename, then press Cancel, select other(renamed) file\r\n or Proceed Upload (replacing banned chars with '_' ) ?");
    //    }
    //    return true;
    //}
</script>
<asp:UpdatePanel runat="server" ID="pnupdateAttachmentGridEnom">
    <ContentTemplate>
         <asp:HiddenField runat="server" ID="hidCategoryCodeAtt" ClientIDMode="Static" />
      
        <asp:HiddenField runat="server" ID="hidReferenceNoAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidUserNameAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorCodeAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidVendorGroupAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidformtypeAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidScreenTypeAtt" ClientIDMode="Static" />
        <asp:HiddenField runat="server" ID="hidthnAtt" ClientIDMode="Static" />

        <dx:ASPxGridView runat="server" ID="gridAttachmentEnom" Width="320px" ClientInstanceName="gridAttachmentEnom"
            ClientIDMode="Static" KeyFieldName="SequenceNumber" EnableCallBacks="false" AutoGenerateColumns="false"
            SettingsBehavior-AllowSort="false" SettingsBehavior-AllowSelectByRowClick="false"
            SettingsBehavior-AllowSelectSingleRowOnly="false" SettingsBehavior-AllowFocusedRow="false"
            SettingsBehavior-AllowDragDrop="False" OnHtmlRowCreated="evt_gridAttachmentEnom_onHtmlRowCreated">
            <Styles>
                <AlternatingRow Enabled="True" />
                 <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                </Header>
            </Styles>
            <Columns>
                <dx:GridViewDataTextColumn FieldName="SequenceNumber" Caption="No" Width="30px">
                    <CellStyle HorizontalAlign="Center" />
                    <DataItemTemplate>
                        <dx:ASPxLabel ID="lblNoAttachmentEnom" runat="server" Text='<%# Bind("SequenceNumber") %>' />
                        <%-- <dx:ASPxLabel runat="server" ID="lblAttachmentNumber" Text="<%# Bind('SequenceNumberInString') %>" Visible="false" />--%>
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="CategoryName" Caption="Category" Width="130px">

                    <DataItemTemplate>
                        
                        <dx:ASPxLabel runat="server" ID="lblAttachmentCategoryEnom" Text='<%# Bind("CategoryName") %>' />
                           <dx:ASPxLabel runat="server" ID="hidValidateVendorGroup" Text='<%# Bind("ValidateVendorGroup") %>' Visible="false" />

                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataTextColumn FieldName="FileName" Caption="File Name" Width="130px">
                    <CellStyle HorizontalAlign="Left" />
                    <DataItemTemplate>
                        <common:BlankTargetedHyperlink runat="server" ID="lblAttachmentFileNameEnom" Text='<%# Bind("FileName") %>'
                            NavigateUrl='<%# Bind("Url") %>' />
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                   <dx:GridViewDataTextColumn FieldName="FILE_TYPE" Caption="File Type" Width="130px" Visible="false">

                    <DataItemTemplate>
                        
                        <dx:ASPxLabel runat="server" ID="lblFileTypeEnom" Text='<%# Bind("FILE_TYPE") %>' />
                              
                    </DataItemTemplate>
                </dx:GridViewDataTextColumn>
                <dx:GridViewDataColumn FieldName="#" Width="30px">
                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <DataItemTemplate>
                        <common:KeyImageButton runat="server" ID="imgAddAttachmentEnom" Key='<%# Bind("SequenceNumber") %>'
                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png" OnClick="evt_imgAddAttachmentEnom_onClick"></common:KeyImageButton>
                        <common:KeyImageButton runat="server" ID="imgDeleteAttachmentEnom" Key='<%# Bind("REF_SEQ_NO") %>'
                            CssClass="pointed" ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png" OnClick="evt_imgDeleteAttachmentEnom_onClick" />
                    </DataItemTemplate>
                </dx:GridViewDataColumn>
            </Columns>
            <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false" />

            <Settings VerticalScrollableHeight="150" VerticalScrollBarMode="Visible" />
            <Settings HorizontalScrollBarMode="Auto" />
        </dx:ASPxGridView>
        <dx:ASPxPopupControl ID="popUploadAttEnom" ClientInstanceName="popUploadAttEnom" runat="server"
            Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter"
            Width="300px" EnableAnimation="false" HeaderText="Upload File" AllowDragging="false"
            AllowResize="false">
            <ContentCollection>
                <dx:PopupControlContentControl ID="popUploadFileAttEnomContent" runat="server">
                    <asp:UpdatePanel runat="server" ID="pnupdateFileUploadPopupEnom">
                        <Triggers>
                            <asp:PostBackTrigger ControlID="btSubmitFileEnom" />
                        </Triggers>
                        <ContentTemplate>
                            <table>

                                <tr>
                                    <td style="margin-right: 10px;">&nbsp;Category
                                    </td>
                                    <td>
                                        <dx:ASPxLabel ID="lblIsnominative" ClientInstanceName="lblIsnominative" runat="server" ClientVisible="false" />
                                        <dx:ASPxLabel ID="lblCategoryName" runat="server" ClientVisible="false" />
                                        <dx:ASPxLabel ID="lblAttId" runat="server" ClientVisible="false" />
                                        <dx:ASPxLabel ID="lblValidateFileType" runat="server" ClientVisible="false" />
                                        <dx:ASPxComboBox runat="server" ID="cboxAttachmentCategoryEnom" AutoPostBack="false"
                                            ClientIDMode="Static" ClientInstanceName="cboxAttachmentCategoryEnom" TextField="Description"
                                            ValueField="Code" ValueType="System.String" OnLoad="evt_cboxAttachmentCategoryEnom_onLoad" ClientVisible="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;">&nbsp;File
                                    </td>
                                    <td>
                                        <asp:FileUpload runat="server" ID="uploadAttachmentEnom" ClientIDMode="Static" AllowMultiple="false" />

                                    </td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td style="margin-right: 10px;"></td>
                                    <td align="right" valign="baseline">

                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btSubmitFileEnom"
                                            Text="Submit" OnClick="evt_btSubmitFileEnom_Click">
                                             <ClientSideEvents Click="function(s,e) { loading(); }" />
                                            <%--<ClientSideEvents Click="function(s,e) {UploadAttachmentEnom_Click(); loading(); }" />--%>
                                        </dx:ASPxButton>
                                        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btCancelFileEnom"
                                            Text="Cancel">
                                            <ClientSideEvents Click="function(s,e) { popUploadAttEnom.Hide(); }" />
                                        </dx:ASPxButton>
                                    </td>
                                </tr>
                            </table>

                        </ContentTemplate>
                    </asp:UpdatePanel>

                </dx:PopupControlContentControl>
            </ContentCollection>
        </dx:ASPxPopupControl>
    </ContentTemplate>
</asp:UpdatePanel>
