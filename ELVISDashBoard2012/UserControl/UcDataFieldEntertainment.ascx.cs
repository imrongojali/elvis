﻿
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;

using Common.Function;
using Common.Messaging;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxHiddenField;
using DevExpress.Web.Data;
using Microsoft.Reporting.WebForms;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;


using QRCoder;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace ELVISDashBoard.UserControl
{

    public partial class UcDataFieldEntertainment : System.Web.UI.UserControl
    {
        protected readonly GlobalResourceData resx = new GlobalResourceData();
        //public static string hidCategoryCodeEntertainment.Value;
        //public static string hidTransactionCodeEntertainment.Value;
        //public static string hidReferenceNoEntertainment.Value;
        //public static string hidUserNameEntertainment.Value;
        //public static string hidVendorCodeEntertainment.Value;
        //public static string hidVendorGroupEntertainment.Value;
        //public static string hidformtypeEntertainment.Value;
        //public static string hidScreenTypeEntertainment.Value;
        //public static string hidthnEntertainment.Value;
        //public static bool IsSaveEntertainment { get; set; }
        protected readonly FormPersistence formPersistence = new FormPersistence();
        protected LogicFactory logic = new LogicFactory();

        protected FtpLogic ftp = new FtpLogic();

       
        protected void Page_Load(object sender, EventArgs e)
        {

           // this.InitializeCultureEntertainment();

            if (hidCategoryCodeEntertainment.Value != "")
            {
                List<Entertainment> _listData = new List<Entertainment>();

                _listData = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
                //_listData = formPersistence.GetDonation(hidCategoryCodeDonation.Value, Convert.ToInt32(hidTransactionCodeDonation.Value), hidReferenceNoDonation.Value, hidformtypeDonation.Value);
                if (_listData.Count() > 0)
                {

                    ASPxGridViewEntertainment.DataSource = _listData;
                    ASPxGridViewEntertainment.DataBind();
                }
            }

            spnTahunPajakEntertainment.Number = DateTime.Now.Year;
            dtPenandaTanganEntertainment.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
        }
        
        protected void InitializeCultureEntertainment()
        {
           

            CultureInfo newCulture = new System.Globalization.CultureInfo("id-ID");
            newCulture.NumberFormat.CurrencySymbol = "Rp. ";
            System.Threading.Thread.CurrentThread.CurrentCulture = newCulture;
            System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture;
        }


        public void EntertainmentList(string formtype, 
            string CategoryCodeProm, 
            string TransactionCodeProm, 
            string ReferenceNoProm, 
            string UserNameProm, 
            string VendorCode, 
            string VendorGroup,
            string _thn,
            string _screenType)
        {
            hidformtypeEntertainment.Value = formtype;

            hidCategoryCodeEntertainment.Value = CategoryCodeProm;
            hidTransactionCodeEntertainment.Value = TransactionCodeProm;
            hidReferenceNoEntertainment.Value = ReferenceNoProm;
            hidUserNameEntertainment.Value = UserNameProm;
            hidVendorCodeEntertainment.Value = VendorCode;
            hidVendorGroupEntertainment.Value = VendorGroup;
            hidScreenTypeEntertainment.Value = _screenType;
            hidthnEntertainment.Value = _thn;

            DataValidEntertainment();
            hidIsSaveEntertainment.Value = "0";
            bool categorysignature = formPersistence.GetCategorySignature(CategoryCodeProm);
            txtJabatanpenandatanagnEntertainment.Enabled = categorysignature;
            //List<Entertainment> _listData = new List<Entertainment>();
            //_listData = formPersistence.GetEntertainment(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm, hidformtypeEntertainment.Value);


            //ASPxGridViewEntertainment.DataSource = _listData;
            //ASPxGridViewEntertainment.DataBind();
            if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = TransactionCodeProm;
                def.FormData.ReferenceNoDataField = ReferenceNoProm;
            }
            else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
            {
                var def = this.Page as _80Accrued.VoucherFormPage;
                def.FormData.CategoryCode = CategoryCodeProm;
                def.FormData.TransactionCodeDataField = TransactionCodeProm;
                def.FormData.ReferenceNoDataField = ReferenceNoProm;
            }

            formPersistence.GetDeleteIsActiveEntertainment(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm, hidformtypeEntertainment.Value);

            List<Entertainment> _listData = new List<Entertainment>();
            _listData = formPersistence.GetEntertainment(CategoryCodeProm, Convert.ToInt32(TransactionCodeProm), ReferenceNoProm, hidformtypeEntertainment.Value);
            ASPxGridViewEntertainment.DataSource = _listData;
            ASPxGridViewEntertainment.DataBind();

            if (_listData.Count > 0)
            {
                int thn_pjk_Donation = _listData.Select(x => x.THN_PAJAK).FirstOrDefault();
                DateTime PenandaTanganDonation = _listData.Select(x => x.TGL_PENANDATANGAN).FirstOrDefault();
                string TempatPenandatanganDonation = _listData.Select(x => x.TMP_PENANDATANGAN).FirstOrDefault();
                string NamaPenandaTanganDonation = _listData.Select(x => x.NAMA_PENANDATANGAN).FirstOrDefault();
                string JabatanpenandatanagnDonation = _listData.Select(x => x.JABATAN_PENANDATANGAN).FirstOrDefault();
                spnTahunPajakEntertainment.Number = thn_pjk_Donation;
                txtTempatPenandatanganEntertainmentEntertainment.Text = TempatPenandatanganDonation;
                dtPenandaTanganEntertainment.Date = Convert.ToDateTime(PenandaTanganDonation.ToString("MM/dd/yyyy"));
                txtNamaPenandatanganEntertainment.Text = NamaPenandaTanganDonation;
                txtJabatanpenandatanagnEntertainment.Text = JabatanpenandatanagnDonation;
            }
            else
            {
                spnTahunPajakEntertainment.Number = DateTime.Now.Year;
                dtPenandaTanganEntertainment.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            }
            displayEnterheader();
            string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldEntertainmentTemp.xls");
            string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "TemplateDataFieldEntertainment.xls");
           
            FileStream file = new FileStream(templatePathSource, FileMode.OpenOrCreate, FileAccess.Read);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
       

            int i = 0;
            List<DataFieldTemplate> dataField = new List<DataFieldTemplate>();
            dataField = formPersistence.getDataFieldNameTemplate(hidCategoryCodeEntertainment.Value);
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRow = sheet.GetRow(5);
                    ICell cell = dataRow.GetCell(i);
                    cell.SetCellValue(item.FieldNameByExcel);

                    //IRow dataRowValue = sheet.GetRow(6);
                    //ICell cellValue = dataRowValue.GetCell(i);

                    //EnomVendor _VendorFirst = new EnomVendor();
                    //_VendorFirst = formPersistence.GetEnomVendor(hidVendorCodeEntertainment.Value).FirstOrDefault();
                    //if (i == 0)
                    //{
                    //    cellValue.SetCellValue(1);
                    //}
                    //if (i == 1)
                    //{
                    //    cellValue.SetCellValue(_VendorFirst.VENDOR_NAME);
                    //}
                    //if (i == 2)
                    //{
                    //    cellValue.SetCellValue(RemoveSpecialCharacters(_VendorFirst.NPWP));
                    //}


                    var _vg = (hidVendorGroupEntertainment.Value == "") ? "0" : hidVendorGroupEntertainment.Value;
                    bool isValidation = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, item.FieldNameByTable, Convert.ToInt32(_vg));
                    if (isValidation == true)
                    {
                        stylexlseEntertainment(hssfwb, cell);
                    }
                    i++;
                }
            }
            using (FileStream fs = new FileStream(templatePath, FileMode.Create, FileAccess.Write))
            {

                hssfwb.Write(fs);
                fs.Close();
                file.Close();
            }

            int r = 0;
            //string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainmentTemp.xls");
            //string templatePathXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainment.xls");

            string templatePathSourceXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainmentTemp.xls");
            string templatePathXls = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainment.xls");
            FileStream filexls = new FileStream(templatePathSourceXls, FileMode.OpenOrCreate, FileAccess.Read);
            HSSFWorkbook hssfwbxls = new HSSFWorkbook(filexls);
            ISheet sheetxls = hssfwbxls.GetSheet("Default");
            if (dataField.Count() > 0)
            {
                foreach (DataFieldTemplate item in dataField.ToList())
                {
                    IRow dataRowXls = sheetxls.GetRow(0);
                    ICell cellXls = dataRowXls.GetCell(r);
                    cellXls.SetCellValue(item.FieldNameByExcel);
                    stylexlseEntertainment(hssfwbxls, cellXls);
                    r++;

                }
            }
            using (FileStream fsXls = new FileStream(templatePathXls, FileMode.Create, FileAccess.Write))
            {

                hssfwbxls.Write(fsXls);
                fsXls.Close();
                filexls.Close();
            }


        }

        public void DataValidEntertainment()
        {
            spnTahunPajakEntertainment.IsValid = true;
            txtTempatPenandatanganEntertainmentEntertainment.IsValid = true;
            dtPenandaTanganEntertainment.IsValid = true;
            txtNamaPenandatanganEntertainment.IsValid = true;
            txtJabatanpenandatanagnEntertainment.IsValid = true;
        }
        public void stylexlseEntertainment(HSSFWorkbook hssfwb, ICell cell)
        {
            ICellStyle style = hssfwb.CreateCellStyle();

            //style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.GREY_25_PERCENT.index;
            style.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.RED.index;
            style.FillPattern = FillPatternType.SOLID_FOREGROUND;
            style.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            style.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;

            IFont font = hssfwb.CreateFont();
            font.Color = NPOI.HSSF.Util.HSSFColor.BLACK.index;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            style.SetFont(font);
            style.Alignment = HorizontalAlignment.CENTER;
            style.VerticalAlignment = VerticalAlignment.CENTER;

            cell.CellStyle = style;
        }

        protected void evt_Close_Entertainment(object sender, EventArgs arg)
        {
            try
            {
                formPersistence.GetDeleteIsActiveEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);

            if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
            {
                var def = this.Page as _30PvFormList.VoucherFormPage;
                def.PnlMasterHeader.ClientVisible = true;
                def.PnlEntertainment.ClientVisible = false;
                def.clearScreenMessage();
            }
           else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
            {
                var def = this.Page as _80Accrued.VoucherFormPage;
                def.PnlMasterHeader.ClientVisible = true;
                def.PnlEntertainment.ClientVisible = false;
                def.clearScreenMessage();
            }
            else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
            {
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                defsett.DivMasterStt1 = true;
                defsett.DivMasterStt2 = true;
                defsett.PnlEntertainment = false;
            }
            else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
            {
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                defsettAccr.DivMasterStt1 = true;
                defsettAccr.DivMasterStt2 = true;
                defsettAccr.PnlEntertainment = false;
            }
            DataValidEntertainment();
            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlEntertainment.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (defaccr != null)
                {
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlEntertainment.ClientVisible = false;
                    defaccr.clearScreenMessage();
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlEntertainment = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlEntertainment = false;
                }
            }
        }
        protected void evt_Clear_Entertainment(object sender, EventArgs arg)
        {
            hidIsSaveEntertainment.Value = "0";
            formPersistence.ClearEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
            List<Entertainment> _listClear = new List<Entertainment>();
            _listClear = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
            ASPxGridViewEntertainment.DataSource = _listClear;
            ASPxGridViewEntertainment.DataBind();
            spnTahunPajakEntertainment.Number = DateTime.Now.Year;
            dtPenandaTanganEntertainment.Date = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy"));
            txtTempatPenandatanganEntertainmentEntertainment.Value = "";
            txtNamaPenandatanganEntertainment.Value = "";
            txtJabatanpenandatanagnEntertainment.Value = "";
            DataValidEntertainment();

        }

        protected void evt_Save_Entertainment(object sender, EventArgs arg)
        {
            try
            {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsett = this.Page as _60SettlementForm.SettlementFormList;
            var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            if (hidIsSaveEntertainment.Value=="1")
                {


                if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                {

                    def.clearScreenMessage();
                    def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                {
                    defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "InValid Save");
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                {
                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "InValid Save");
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                {
                    defsettAccrfrom.clearScreenMessage();
                    defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "InValid Save");
                    return;

                }
            }
            else
            {
                if (ASPxEdit.AreEditorsValid(FormPanelEntertainment))
                {
                    try
                    {
                        DataFieldHeader dataFieldHeader = new DataFieldHeader();
                        dataFieldHeader.CATEGORY_CODE = IsNullString(hidCategoryCodeEntertainment.Value);
                        dataFieldHeader.TRANSACTION_CD = IsNullInt(hidTransactionCodeEntertainment.Value);
                        dataFieldHeader.REFERENCE_NO = IsNullString(hidReferenceNoEntertainment.Value);
                        dataFieldHeader.THN_PAJAK = IsNullInt(spnTahunPajakEntertainment.Value);
                        dataFieldHeader.TMP_PENANDATANGAN = IsNullString(txtTempatPenandatanganEntertainmentEntertainment.Value);
                        dataFieldHeader.TGL_PENANDATANGAN = IsNullDateTime(dtPenandaTanganEntertainment.Value);
                        dataFieldHeader.NAMA_PENANDATANGAN = IsNullString(txtNamaPenandatanganEntertainment.Value);
                        dataFieldHeader.JABATAN_PENANDATANGAN = IsNullString(txtJabatanpenandatanagnEntertainment.Value);

                        bool IsValid = formPersistence.UpdateHeaderEntertainment(dataFieldHeader);
                        if (IsValid == false)
                        {
                            if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                            {

                                def.clearScreenMessage();
                                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                return;
                            }
                            else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                            {
                                defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                return;
                            }
                            else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                            {
                                defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", "Nominative list cannot empty");
                                return;
                            }
                            else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                            {
                                defsettAccrfrom.clearScreenMessage();
                                def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", "Nominative list cannot empty");
                                return;

                            }
                        }
                        else
                        {

                            string categoryname = formPersistence.GetCategoryNominative(hidCategoryCodeEntertainment.Value);
                            string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainment.xls");
                            string templatePath = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldEntertainmentTest.xls");
                            string reportPath = Server.MapPath("~/Report/RDLC/DataFieldEntertainment.rdlc");
                            byte[] filexlsEntertainment = formPersistence.ExportXlsEntertainment(hidCategoryCodeEntertainment.Value, hidTransactionCodeEntertainment.Value, hidReferenceNoEntertainment.Value, templatePathSource, templatePath, hidformtypeEntertainment.Value);
                            string pdfName = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".pdf";
                            string pdfNamexls = categoryname + " " + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".xls";
                            byte[] filepdfEntertainment = formPersistence.ExportPdfEntertainment(hidCategoryCodeEntertainment.Value, hidTransactionCodeEntertainment.Value, hidReferenceNoEntertainment.Value, reportPath, hidformtypeEntertainment.Value);


                            bool uploadSucceded = true;
                            string errorMessage = null;


                            ftp.ftpUploadBytes(hidthnEntertainment.Value, hidScreenTypeEntertainment.Value,
                                 IsNullString(hidReferenceNoEntertainment.Value.Replace("|", "")),
                                     pdfName,
                                            filepdfEntertainment,
                                            ref uploadSucceded,
                                            ref errorMessage);

                            AttachmentCategoryNominative EntertainmentPdf = new AttachmentCategoryNominative();
                            EntertainmentPdf.ReferenceNumber = hidReferenceNoEntertainment.Value;
                            EntertainmentPdf.FileName = pdfName;
                            EntertainmentPdf.ATTACH_CD = "0";
                            EntertainmentPdf.Description = categoryname;
                            EntertainmentPdf.PATH = hidthnEntertainment.Value + "/" + hidScreenTypeEntertainment.Value + "/" + hidReferenceNoEntertainment.Value.Replace("|", "");
                            EntertainmentPdf.FILE_TYPE = "PDF";
                            EntertainmentPdf.CategoryCode = hidCategoryCodeEntertainment.Value;
                            EntertainmentPdf.IS_ENOMINATIVE = true;
                            EntertainmentPdf.FormType = hidformtypeEntertainment.Value;
                            EntertainmentPdf.CreatedBy = hidUserNameEntertainment.Value;
                            formPersistence.AddAttachmenCategoryNominative(EntertainmentPdf);


                            ftp.ftpUploadBytes(hidthnEntertainment.Value, hidScreenTypeEntertainment.Value,
                                     IsNullString(hidReferenceNoEntertainment.Value.Replace("|", "")),
                                         pdfNamexls,
                                          filexlsEntertainment,
                                          ref uploadSucceded,
                                          ref errorMessage);

                            AttachmentCategoryNominative EntertainmentXLS = new AttachmentCategoryNominative();
                            EntertainmentXLS.ReferenceNumber = hidReferenceNoEntertainment.Value;
                            EntertainmentXLS.FileName = pdfNamexls;
                            EntertainmentXLS.ATTACH_CD = "0";
                            EntertainmentXLS.Description = categoryname;
                            EntertainmentXLS.PATH = hidthnEntertainment.Value + "/" + hidScreenTypeEntertainment.Value + "/" + hidReferenceNoEntertainment.Value.Replace("|", "");
                            EntertainmentXLS.FILE_TYPE = "XLS";
                            EntertainmentXLS.CategoryCode = hidCategoryCodeEntertainment.Value;
                            EntertainmentXLS.IS_ENOMINATIVE = true;
                            EntertainmentXLS.FormType = hidformtypeEntertainment.Value;
                            EntertainmentXLS.CreatedBy = hidUserNameEntertainment.Value;
                            formPersistence.AddAttachmenCategoryNominative(EntertainmentXLS);

                            if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                            {

                                def.FormData.CategoryCode = hidCategoryCodeEntertainment.Value;
                                def.FormData.TransactionCodeDataField = hidTransactionCodeEntertainment.Value;
                                def.FormData.ReferenceNoDataField = hidReferenceNoEntertainment.Value;
                            }
                            else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                            {

                                defsettAccrfrom.FormData.CategoryCode = hidCategoryCodeEntertainment.Value;
                                defsettAccrfrom.FormData.TransactionCodeDataField = hidTransactionCodeEntertainment.Value;
                                defsettAccrfrom.FormData.ReferenceNoDataField = hidReferenceNoEntertainment.Value;
                            }

                        }

                        if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                        {

                            def.clearScreenMessage();
                            // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                            def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");

                            def.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeEntertainment.Value, hidCategoryCodeEntertainment.Value, hidReferenceNoEntertainment.Value, hidUserNameEntertainment.Value, hidthnEntertainment.Value, hidScreenTypeEntertainment.Value, Convert.ToInt32(hidVendorGroupEntertainment.Value));
                            def.PnlMasterHeader.ClientVisible = true;
                            def.PnlEntertainment.ClientVisible = false;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                        {
                            defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                            defsett.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeEntertainment.Value, hidCategoryCodeEntertainment.Value, hidReferenceNoEntertainment.Value, hidUserNameEntertainment.Value, hidthnEntertainment.Value, hidScreenTypeEntertainment.Value, Convert.ToInt32(hidVendorGroupEntertainment.Value));
                            defsett.DivMasterStt1 = true;
                            defsett.DivMasterStt2 = true;
                            defsett.PnlEntertainment = false;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                        {
                            defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                            defsettAccr.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeEntertainment.Value, hidCategoryCodeEntertainment.Value, hidReferenceNoEntertainment.Value, hidUserNameEntertainment.Value, hidthnEntertainment.Value, hidScreenTypeEntertainment.Value, Convert.ToInt32(hidVendorGroupEntertainment.Value));
                            defsettAccr.DivMasterStt1 = true;
                            defsettAccr.DivMasterStt2 = true;
                            defsettAccr.PnlEntertainment = false;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                        {


                            defsettAccrfrom.clearScreenMessage();
                            // def.PostNow(ScreenMessage.STATUS_INFO, "Data Field Harus di isi");
                            defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");

                            defsettAccrfrom.ucAttachmentType.ShowLoadAttachmentCategoryNominative(hidformtypeEntertainment.Value, hidCategoryCodeEntertainment.Value, hidReferenceNoEntertainment.Value, hidUserNameEntertainment.Value, hidthnEntertainment.Value, hidScreenTypeEntertainment.Value, Convert.ToInt32(hidVendorGroupEntertainment.Value));
                            defsettAccrfrom.PnlMasterHeader.ClientVisible = true;
                            defsettAccrfrom.PnlEntertainment.ClientVisible = false;
                        }
                    }
                    catch (Exception ex)
                    {

                        if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                        {

                            def.clearScreenMessage();
                            def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                            return;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                        {
                            defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", ex.Message);
                            return;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                        {
                            defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", ex.Message);
                            return;
                        }
                        else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                        {
                            defsettAccrfrom.clearScreenMessage();
                            defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00002ERR", ex.Message);
                            return;

                        }
                    }

                }
            }
            }
            catch (Exception)
            {

                var def = this.Page as _30PvFormList.VoucherFormPage;
                var defaccr = this.Page as _80Accrued.VoucherFormPage;
                var defsett = this.Page as _60SettlementForm.SettlementFormList;
                var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
                if (def != null)
                {
                    def.PnlMasterHeader.ClientVisible = true;
                    def.PnlEntertainment.ClientVisible = false;
                    def.clearScreenMessage();
                }
                else if (defaccr != null)
                {
                    defaccr.PnlMasterHeader.ClientVisible = true;
                    defaccr.PnlEntertainment.ClientVisible = false;
                    defaccr.clearScreenMessage();
                }
                else if (defsett != null)
                {
                    defsett.DivMasterStt1 = true;
                    defsett.DivMasterStt2 = true;
                    defsett.PnlEntertainment = false;
                }
                else if (defsettAccr != null)
                {
                    defsettAccr.DivMasterStt1 = true;
                    defsettAccr.DivMasterStt2 = true;
                    defsettAccr.PnlEntertainment = false;
                }
            }
        }

        protected void ASPxGridViewEntertainment_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            ASPxDateEdit dtTanggaltransaksi = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggaltransaksi") as ASPxDateEdit;
            ASPxTextBox txtTempat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TEMPAT"] as GridViewDataColumn, "txtTempat") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
            ASPxTextBox txtTipeEntertainment = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TIPE_ENTERTAINMENT"] as GridViewDataColumn, "txtTipeEntertainment") as ASPxTextBox;
            ASPxSpinEdit txtJumlah = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["JUMLAH"] as GridViewDataColumn, "txtJumlah") as ASPxSpinEdit;
          
            Entertainment Entertainment = new Entertainment();
            Entertainment.CATEGORY_CODE = hidCategoryCodeEntertainment.Value;
            Entertainment.TRANSACTION_CD = Convert.ToInt32(hidTransactionCodeEntertainment.Value);
            Entertainment.REFERENCE_NO = hidReferenceNoEntertainment.Value;
            Entertainment.NO = 1;

            Entertainment.TANGGAL = IsNullDateTime(dtTanggaltransaksi.Value);
            Entertainment.TEMPAT = IsNullString(IsNullString(txtTempat.Value));
            Entertainment.ALAMAT = IsNullString(IsNullString(txtAlamat.Value));
            Entertainment.TIPE_ENTERTAINMENT = IsNullString(txtTipeEntertainment.Value);
            Entertainment.JUMLAH = IsNullDecimal(txtJumlah.Value);

            Entertainment.CREATED_DT = DateTime.Now;
            Entertainment.CREATED_BY = IsNullString(hidUserNameEntertainment.Value);
            Entertainment.FormType = hidformtypeEntertainment.Value;
         
            formPersistence.AddEntertainment(Entertainment);
            e.Cancel = true;
            ASPxGridViewEntertainment.CancelEdit();

            List<Entertainment> _listSave = new List<Entertainment>();

            _listSave = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
            ASPxGridViewEntertainment.DataSource = _listSave;
            ASPxGridViewEntertainment.DataBind();

        }

        protected void ASPxGridViewEntertainment_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            HiddenField hidSeqNo = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["NO"] as GridViewDataColumn, "hidSeqNo") as HiddenField;
            ASPxDateEdit dtTanggaltransaksi = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggaltransaksi") as ASPxDateEdit;
            ASPxTextBox txtTempat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TEMPAT"] as GridViewDataColumn, "txtTempat") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
            ASPxTextBox txtTipeEntertainment = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TIPE_ENTERTAINMENT"] as GridViewDataColumn, "txtTipeEntertainment") as ASPxTextBox;
            ASPxSpinEdit txtJumlah = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["JUMLAH"] as GridViewDataColumn, "txtJumlah") as ASPxSpinEdit;
         
            Entertainment Entertainment = new Entertainment();
            Entertainment.SEQ_NO = IsNullInt64(hidSeqNo.Value);

            Entertainment.TANGGAL = IsNullDateTime(dtTanggaltransaksi.Value);
            Entertainment.TEMPAT = IsNullString(IsNullString(txtTempat.Value));
            Entertainment.ALAMAT = IsNullString(IsNullString(txtAlamat.Value));
            Entertainment.TIPE_ENTERTAINMENT = IsNullString(txtTipeEntertainment.Value);
            Entertainment.JUMLAH = IsNullDecimal(txtJumlah.Value);
           
            Entertainment.CREATED_DT = DateTime.Now;
            Entertainment.CREATED_BY = IsNullString(hidUserNameEntertainment.Value);
           
            formPersistence.EditEntertainment(Entertainment);
            e.Cancel = true;
            ASPxGridViewEntertainment.CancelEdit();

            List<Entertainment> _listEdit = new List<Entertainment>();

            _listEdit = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
            ASPxGridViewEntertainment.DataSource = _listEdit;
            ASPxGridViewEntertainment.DataBind();

        }

        protected void ASPxGridViewEntertainment_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            var keydata = IsNullInt64(e.Keys["SEQ_NO"]);
           
            formPersistence.DeleteEntertainment(keydata);
            e.Cancel = true;


            List<Entertainment> _listDelete = new List<Entertainment>();

            _listDelete = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
            ASPxGridViewEntertainment.DataSource = _listDelete;
            ASPxGridViewEntertainment.DataBind();
        }

        protected void ASPxGridViewEntertainment_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {

            string msgValidation = "";
            int countValidation = 0;
         
            ASPxDateEdit dtTanggaltransaksi = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TANGGAL"] as GridViewDataColumn, "dtTanggaltransaksi") as ASPxDateEdit;
            ASPxTextBox txtTempat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TEMPAT"] as GridViewDataColumn, "txtTempat") as ASPxTextBox;
            ASPxTextBox txtAlamat = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["ALAMAT"] as GridViewDataColumn, "txtAlamat") as ASPxTextBox;
            ASPxTextBox txtTipeEntertainment = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["TIPE_ENTERTAINMENT"] as GridViewDataColumn, "txtTipeEntertainment") as ASPxTextBox;
            ASPxSpinEdit txtJumlah = ASPxGridViewEntertainment.FindEditRowCellTemplateControl(ASPxGridViewEntertainment.Columns["JUMLAH"] as GridViewDataColumn, "txtJumlah") as ASPxSpinEdit;
          

            bool IS_TANGGAL = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "TANGGAL", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_TEMPAT= formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "TEMPAT", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_ALAMAT = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "ALAMAT", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_TIPE_ENTERTAINMENT = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "TIPE_ENTERTAINMENT", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            //bool IS_TANGGAL_SUMBANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "TANGGAL_SUMBANGAN", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_JUMLAH = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "JUMLAH", Convert.ToInt32(hidVendorGroupEntertainment.Value));

            if ((IsNullDateTime(dtTanggaltransaksi.Value) == Convert.ToDateTime("1900-01-01")) && IS_TANGGAL)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "TANGGAL").FirstOrDefault();
                dtTanggaltransaksi.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            
            if ((IsNullString(txtTempat.Value) == "") && IS_TEMPAT)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "TEMPAT").FirstOrDefault();
                txtAlamat.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtAlamat.Value) == "") && IS_ALAMAT)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "ALAMAT").FirstOrDefault();
                txtAlamat.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtTipeEntertainment.Value) == "") && IS_TIPE_ENTERTAINMENT)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "TIPE_ENTERTAINMENT").FirstOrDefault();
                txtTipeEntertainment.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtJumlah.Value) == "") && IS_JUMLAH)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "JUMLAH").FirstOrDefault();
                txtJumlah.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }

        protected void ASPxGridViewEntertainment_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {
            hidIsSaveEntertainment.Value = "1";
            List<Entertainment> _listSeqNo = new List<Entertainment>();
            _listSeqNo = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);

            e.NewValues["NO"] = _listSeqNo.Count() + 1;
            e.NewValues["SEQ_NO"] = _listSeqNo.Count() + 1;
            e.NewValues["TANGGAL"] = DateTime.Now;
            e.NewValues["JUMLAH"] = 0;

        }

        protected void ASPxGridViewEntertainment_ParseValue(object sender, DevExpress.Web.Data.ASPxParseValueEventArgs ev)
        {
            var def = this.Page as _30PvFormList.VoucherFormPage;
            List<Entertainment> _listSeqNo = new List<Entertainment>();

            _listSeqNo = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);

            if (ev.FieldName == "NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("NO number");
                }
           
            if (ev.FieldName == "SEQ_NO")
                try
                {
                    ev.Value = Convert.ToInt64(_listSeqNo.Count() + 1);
                }
                catch (Exception)
                {

                    throw new Exception("SEQ_NO number");
                }

        }


        public void displayEnterheader()
        {
            if (IsNullString(hidCategoryCodeEntertainment.Value) != "")
            {




                foreach (GridViewColumn row in ASPxGridViewEntertainment.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            ASPxGridViewEntertainment.Columns[namecolums].Caption = _caption;
                        }


                    }


                }
            }
        }
      

        public string IsNullString(object data)
        {
            string returndata = "";
            if (data == null)
            {
                returndata = "";
            }
            else
            {
                returndata = data.ToString();
            }
            return returndata;
        }

        public decimal IsNullDecimal(object data)
        {
            decimal returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToDecimal(data);
            }
            return returndata;
        }

        public Int32 IsNullInt(object data)
        {
            Int32 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt32(data);
            }
            return returndata;
        }

        public Int64 IsNullInt64(object data)
        {
            Int64 returndata = 0;
            if (data == null)
            {
                returndata = 0;
            }
            else
            {
                returndata = Convert.ToInt64(data);
            }
            return returndata;
        }

        public DateTime IsNullDateTime(object data)
        {
            DateTime returndata = Convert.ToDateTime("1900-01-01");
            if (data == null)
            {
                returndata = Convert.ToDateTime("1900-01-01");
            }
            else
            {
                returndata = Convert.ToDateTime(data);
            }
            return returndata;
        }

        public static string RemoveSpecialCharacters(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach (char c in str)
            {
                if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'))
                {
                    sb.Append(c);
                }
            }
            return sb.ToString();
        }

        public static string FormatNPWP(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                string a = value.Substring(0, 2);
                string b = value.Substring(2, 3);
                string c = value.Substring(5, 3);
                string d = value.Substring(8, 1);
                string e = value.Substring(9, 3);
                string f = value.Substring(12, 3);

                return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
            }
            else
            {
                return "";
            }

           
        }

        protected void TahunPajak_Validation(object sender, ValidationEventArgs e)
        {

            if (e.Value.ToString().Length != 4)

                e.IsValid = false;
        }

        #region Upload
        protected void btnUploadTemplateEntertainment_Click(object sender, EventArgs e)
        {
            hidIsSaveEntertainment.Value = "0";

            var def = this.Page as _30PvFormList.VoucherFormPage;
            var defsett = this.Page as _60SettlementForm.SettlementFormList;
            var defsettAccr = this.Page as _80Accrued.SettlementFormAccr;
            var defsettAccrfrom = this.Page as _80Accrued.VoucherFormPage;
            logic.Say("BeforeUploadEntertainment_Click", "Upload : {0}", fuDataFieldEntertainmentList.FileName);
            if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
            {
                def.clearScreenMessage();
            }
            else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
            {
                defsettAccrfrom.clearScreenMessage();
            }
            try
            {


                int _processId = 0;
                CommonExcelUpload _CommonExcelUpload = new CommonExcelUpload();
                string _strFileType = Path.GetExtension(fuDataFieldEntertainmentList.FileName).ToString().ToLower();
                string _strFileName = fuDataFieldEntertainmentList.FileName;

                if (!_CommonExcelUpload.IsExcelFormat(_strFileType))
                {
                    if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }

                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00033ERR", _strFileName);
                        return;

                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00033ERR", _strFileName);
                        return;
                       

                    }
                    return;
                }
                string _strFunctionId = "";
                //_strFunctionId = resx.FunctionId("FCN_" + def.ScreenType + "_FORM_UPLOAD");
                _strFunctionId = resx.FunctionId("FCN_" + hidScreenTypeEntertainment.Value + "_FORM_UPLOAD");
                if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                {

                    _processId = def.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                {

                    _processId = defsett.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                {

                    _processId = defsettAccr.DoStartLog(_strFunctionId, "");
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                {

                    _processId = defsettAccrfrom.DoStartLog(_strFunctionId, "");
                }

                string _strTempDir = LoggingLogic.GetTempPath("", _processId);

                string xlsFile = Util.GetTmp(_strTempDir, Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(_strFileName)) + "_" + _processId.str(), _strFileType);
                LoggingLogic.ForceDirectories(Path.GetDirectoryName(xlsFile));
                fuDataFieldEntertainmentList.SaveAs(xlsFile);

                string[][] listOfErrors = new string[1][];

                bool Ok = formPersistence.UploadFileTemplateEntertainment(
                        hidCategoryCodeEntertainment.Value,
                        Convert.ToInt32(hidTransactionCodeEntertainment.Value),
                        hidReferenceNoEntertainment.Value,
                        hidVendorGroupEntertainment.Value,
                        DateTime.Now,
                        IsNullString(hidUserNameEntertainment.Value),
                        xlsFile,
                        hidformtypeEntertainment.Value);
                if (!Ok)
                {
                    string sourceFile = Server.MapPath(
                                            String.Format(
                                                    logic.Sys.GetText("TEMPLATE.FMT", "ERR"), "TemplateDataFieldEntertainment.xls"
                                                )
                                            );
                    string targetFile = Server.MapPath(Common.AppSetting.ErrorUploadFilePath);

                    string fileName = formPersistence.DownloadError(_processId, _strFileName, sourceFile, targetFile, listOfErrors);

                    String linkErrorFile = String.Format("<a href='{0}://{1}:{2}{3}{4}' target='_blank'>{5}</a>",
                                                            Request.Url.Scheme,
                                                            Request.ServerVariables["SERVER_NAME"],
                                                            Request.ServerVariables["SERVER_PORT"],
                                                            Common.AppSetting.ErrorUploadFilePath.Replace("~", ""),
                                                            fileName,
                                                            fileName);

                    if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00002ERR", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_ERROR, "MSTD00001INF", String.Format("Upload failed. download error message here : {0}", linkErrorFile));
                        return;

                    }
                    return;
                }
                else
                {
                    List<Entertainment> _listUpload = new List<Entertainment>();

                    _listUpload = formPersistence.GetEntertainment(hidCategoryCodeEntertainment.Value, Convert.ToInt32(hidTransactionCodeEntertainment.Value), hidReferenceNoEntertainment.Value, hidformtypeEntertainment.Value);
                    ASPxGridViewEntertainment.DataSource = _listUpload;
                    ASPxGridViewEntertainment.DataBind();
                    if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                    {

                        def.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                    {

                        defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                    {

                        defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ScreenMessage.STATUS_INFO);
                    }
                    else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                    {
                        defsettAccrfrom.PostNow(ScreenMessage.STATUS_INFO, "MSTD00020INF");
                       
                    }
                }
            }
            catch (Exception ex)
            {
                if (hidformtypeEntertainment.Value == "ELVIS_PVFormList")
                {

                    def.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldEntertainmentList.Dispose();
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_SettlementFormList")
                {

                    defsett.ErrMsg = defsett.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldEntertainmentList.Dispose();
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrSettForm")
                {

                    defsettAccr.ErrMsg = defsettAccr.Nagging("MSTD00020INF", ex.Message);
                    fuDataFieldEntertainmentList.Dispose();
                    return;
                }
                else if (hidformtypeEntertainment.Value == "ELVIS_AccrPVForm")
                {
                    defsettAccrfrom.PostNowPush(ScreenMessage.STATUS_ERROR, "", ex.Message);
                    fuDataFieldEntertainmentList.Dispose();
                    return;

                   
                }
                LoggingLogic.err(ex);
            }
            fuDataFieldEntertainmentList.Dispose();

        }

        #endregion Upload

    
        protected void gvDetailEnter_Init(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            //var container = grid.NamingContainer as GridViewDataItemTemplateContainer;
            //var index = container.VisibleIndex;

            var def = this.Page as _30PvFormList.VoucherFormPage;
            if (IsNullString(hidCategoryCodeEntertainment.Value) != "")
            {
                foreach (GridViewColumn row in grid.Columns)
                {
                    GridViewDataColumn dataRow = row as GridViewDataColumn;

                    if (dataRow == null) continue;
                    if (dataRow.FieldName != null)
                    {
                        string namecolums = dataRow.FieldName;
                        string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, namecolums).FirstOrDefault();
                        if (_caption != null)
                        {
                            grid.Columns[namecolums].Caption = _caption;
                        }
                    }
                }
            }
            Int64 seqNo = (Int64)grid.GetMasterRowKeyValue();

            List<EntertainmentDetail> _listDetail = new List<EntertainmentDetail>();

            _listDetail = formPersistence.GetEntertainmentDetail(seqNo);

            grid.DataSource = _listDetail.ToList();

        }

        protected void gvDetailEnter_RowInserting(object sender, DevExpress.Web.Data.ASPxDataInsertingEventArgs e)
        {

            ASPxGridView gvDetailEnter = sender as ASPxGridView;

            ASPxTextBox txtNamaRelasi = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["NAMA_RELASI"] as GridViewDataColumn, "txtNamaRelasi") as ASPxTextBox;
            ASPxTextBox txtJabatan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["JABATAN"] as GridViewDataColumn, "txtJabatan") as ASPxTextBox;
            ASPxTextBox txtAlamatPerusahaan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["ALAMAT_PERUSAHAAN"] as GridViewDataColumn, "txtAlamatPerusahaan") as ASPxTextBox;
            ASPxTextBox txtTipeBisnis = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["TIPE_BISNIS"] as GridViewDataColumn, "txtTipeBisnis") as ASPxTextBox;
            ASPxTextBox txtKeterangan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            EntertainmentDetail EntertainmentDetail = new EntertainmentDetail();
            Int64 seqNo = (Int64)gvDetailEnter.GetMasterRowKeyValue();

            EntertainmentDetail.SEQ_NO = seqNo;
            EntertainmentDetail.NAMA_RELASI = IsNullString(txtNamaRelasi.Value);
            EntertainmentDetail.JABATAN = IsNullString(txtJabatan.Value);
            EntertainmentDetail.ALAMAT_PERUSAHAAN = IsNullString(txtAlamatPerusahaan.Value);
            EntertainmentDetail.TIPE_BISNIS = IsNullString(txtTipeBisnis.Value);
            EntertainmentDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            EntertainmentDetail.CREATED_DT = DateTime.Now;
            EntertainmentDetail.CREATED_BY = IsNullString(hidUserNameEntertainment.Value);
           
            formPersistence.AddEntertainmentDetail(EntertainmentDetail);
            e.Cancel = true;
            gvDetailEnter.CancelEdit();

            List<EntertainmentDetail> _listSaveDetail = new List<EntertainmentDetail>();

            _listSaveDetail = formPersistence.GetEntertainmentDetail(seqNo);
            gvDetailEnter.DataSource = _listSaveDetail;
            gvDetailEnter.DataBind();
        }

        protected void gvDetailEnter_RowUpdating(object sender, DevExpress.Web.Data.ASPxDataUpdatingEventArgs e)
        {
            ASPxGridView gvDetailEnter = sender as ASPxGridView;

            var keydata = IsNullInt64(e.Keys["SEQ_NO_DETAIL"]);
            //ASPxTextBox hiddenFieldseqdetail = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["SEQ_NO_DETAIL"] as GridViewDataColumn, "hiddenFieldseqdetail") as ASPxTextBox;
            ASPxTextBox txtNamaRelasi = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["NAMA_RELASI"] as GridViewDataColumn, "txtNamaRelasi") as ASPxTextBox;
            ASPxTextBox txtJabatan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["JABATAN"] as GridViewDataColumn, "txtJabatan") as ASPxTextBox;
            ASPxTextBox txtAlamatPerusahaan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["ALAMAT_PERUSAHAAN"] as GridViewDataColumn, "txtAlamatPerusahaan") as ASPxTextBox;
            ASPxTextBox txtTipeBisnis = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["TIPE_BISNIS"] as GridViewDataColumn, "txtTipeBisnis") as ASPxTextBox;
            ASPxTextBox txtKeterangan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;
            
            // create new object 
            EntertainmentDetail EntertainmentDetail = new EntertainmentDetail();

            // fill the object based on value from page
            EntertainmentDetail.SEQ_NO_DETAIL = Convert.ToInt64(keydata);
            //EntertainmentDetail.SEQ_NO_DETAIL = IsNullInt64(hiddenFieldseqdetail.Value);
            EntertainmentDetail.NAMA_RELASI = IsNullString(txtNamaRelasi.Value);
            EntertainmentDetail.JABATAN = IsNullString(txtJabatan.Value);
            EntertainmentDetail.ALAMAT_PERUSAHAAN = IsNullString(txtAlamatPerusahaan.Value);
            EntertainmentDetail.TIPE_BISNIS = IsNullString(txtTipeBisnis.Value);
            EntertainmentDetail.KETERANGAN = IsNullString(txtKeterangan.Value);
            EntertainmentDetail.CREATED_DT = DateTime.Now;
            EntertainmentDetail.CREATED_BY = IsNullString(hidUserNameEntertainment.Value);
          

            // update data
            formPersistence.EditEntertainmentDetail(EntertainmentDetail);
            e.Cancel = true;
            gvDetailEnter.CancelEdit();
            Int64 seqNo = (Int64)gvDetailEnter.GetMasterRowKeyValue();
            List<EntertainmentDetail> _listDetailEdit = new List<EntertainmentDetail>();

            _listDetailEdit = formPersistence.GetEntertainmentDetail(seqNo);
            gvDetailEnter.DataSource = _listDetailEdit;
            gvDetailEnter.DataBind();

        }

        protected void gvDetailEnter_RowDeleting(object sender, DevExpress.Web.Data.ASPxDataDeletingEventArgs e)
        {
            ASPxGridView gvDetailEnter = sender as ASPxGridView;
            //Int64 seqNo = (Int64)gvDetailEnter.GetMasterRowKeyValue();
            var keydata = IsNullInt64(e.Keys["SEQ_NO_DETAIL"]);

           Int64 seqnoheader= formPersistence.DeleteEntertainmentDetail(keydata);
            e.Cancel = true;
            List<EntertainmentDetail> _listDetailDelete = new List<EntertainmentDetail>();
            _listDetailDelete = formPersistence.GetEntertainmentDetail(seqnoheader);
            gvDetailEnter.DataSource = _listDetailDelete;
            gvDetailEnter.DataBind();
        }

        protected void gvDetailEnter_RowValidating(object sender, DevExpress.Web.Data.ASPxDataValidationEventArgs e)
        {
            ASPxGridView gvDetailEnter = sender as ASPxGridView;
            string msgValidation = "";
            int countValidation = 0;
            
            ASPxTextBox txtNamaRelasi = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["NAMA_RELASI"] as GridViewDataColumn, "txtNamaRelasi") as ASPxTextBox;
            ASPxTextBox txtJabatan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["JABATAN"] as GridViewDataColumn, "txtJabatan") as ASPxTextBox;
            ASPxTextBox txtAlamatPerusahaan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["ALAMAT_PERUSAHAAN"] as GridViewDataColumn, "txtAlamatPerusahaan") as ASPxTextBox;
            ASPxTextBox txtTipeBisnis = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["TIPE_BISNIS"] as GridViewDataColumn, "txtTipeBisnis") as ASPxTextBox;
            ASPxTextBox txtKeterangan = gvDetailEnter.FindEditRowCellTemplateControl(gvDetailEnter.Columns["KETERANGAN"] as GridViewDataColumn, "txtKeterangan") as ASPxTextBox;

            bool IS_NAMA_RELASI = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "NAMA_RELASI", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_JABATAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "JABATAN", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_ALAMAT_PERUSAHAAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "ALAMAT_PERUSAHAAN", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_TIPE_BISNIS = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "TIPE_BISNIS", Convert.ToInt32(hidVendorGroupEntertainment.Value));
            bool IS_KETERANGAN = formPersistence.IsValidationVendorGroup(hidCategoryCodeEntertainment.Value, "KETERANGAN", Convert.ToInt32(hidVendorGroupEntertainment.Value));

            if ((IsNullString(txtNamaRelasi.Value) == "") && IS_NAMA_RELASI)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "NAMA_RELASI").FirstOrDefault();
                txtNamaRelasi.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtJabatan.Value) == "") && IS_JABATAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "JABATAN").FirstOrDefault();
                txtJabatan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtAlamatPerusahaan.Value) == "") && IS_ALAMAT_PERUSAHAAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "ALAMAT_PERUSAHAAN").FirstOrDefault();
                txtAlamatPerusahaan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtTipeBisnis.Value) == "") && IS_TIPE_BISNIS)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "TIPE_BISNIS").FirstOrDefault();
                txtTipeBisnis.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }
            if ((IsNullString(txtKeterangan.Value) == "") && IS_KETERANGAN)
            {
                string _caption = formPersistence.getDataFieldName(hidCategoryCodeEntertainment.Value, "KETERANGAN").FirstOrDefault();
                txtKeterangan.IsValid = false;
                msgValidation += _caption + ", ";
                countValidation++;
            }

            if (countValidation > 0)
            {
                e.RowError = msgValidation + " Is Validation.";
            }
        }

        protected void gvDetailEnter_InitNewRow(object sender, DevExpress.Web.Data.ASPxDataInitNewRowEventArgs e)
        {

            hidIsSaveEntertainment.Value = "1";



        }

        protected void ASPxGridViewEntertainment_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSaveEntertainment.Value = "1";
        }

        protected void gvDetailEnter_StartRowEditing(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSaveEntertainment.Value = "1";
        }

        protected void ASPxGridViewEntertainment_Cancel(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {

            hidIsSaveEntertainment.Value = "0";

        }

        protected void gvDetailEnter_Cancel(object sender, DevExpress.Web.Data.ASPxStartRowEditingEventArgs e)
        {
            hidIsSaveEntertainment.Value = "0";
        }

    }
}
