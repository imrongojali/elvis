﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/MainScreen.Master" AutoEventWireup="true"
    CodeBehind="Home.aspx.cs" Inherits="ELVISDashBoard.Home" EnableSessionState="True"%>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxEditors" tagprefix="dx" %>
<%@ Register assembly="DevExpress.Web.v12.2" namespace="DevExpress.Web.ASPxGridView" tagprefix="dx" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxCallback" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxLoadingPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<style type="text/css">
    .confirm
    {
        width: 500px;
        display: table;
    }
    .confirmLabel
    {
        width: 25%;
        display: table-cell;
        clear: none;
        vertical-align: top;
    }
    .confirmValue
    {
        width: 75%;
        text-align: left;
        display: table-cell;
        clear: none;
        vertical-align: top;
    }
    .confirmButton
    {
        width: 100%;
        text-align: right;
        display: block;
        clear: none;
        margin: 3px 0px 3px 0px;
    }
    .confirmText
    {
        width: 98%;
        height: 50px;
        text-align: left;
    }
</style>
<script type="text/javascript">
    var RefreshDelay = 30000; /// 3 seconds delay between each refresh is considered long enough - user complained not being rershed
    var LastRefresh = new Date();

    function RefreshSession() {
        var delays = (new Date()).getTime() - LastRefresh.getTime();
        if (delays < RefreshDelay)
            return;
        LastRefresh = new Date();

        $.post("Home.aspx", {}, function () {}, "text");
    }
    var intervalId;

    $(window).focus(function () {
        RefreshSession();
        if (needReload) {
            window.location.reload();
            window.needReload = false;
        }
        if (!intervalId) {
            intervalId = setInterval(RefreshSession, RefreshDelay);
        }
    });
    $(window).blur(function () {
        clearInterval(intervalId);
        intervalId = 0;
    });

    function loading() {
       
        if (typeof Callback != 'undefined')
            Callback.PerformCallback();
        else
            console.log('Callback undefined');
        if (typeof LoadingPanel != 'undefined')
            LoadingPanel.Show();
        else
            console.log('LoadingPanel undefined');
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPengumuman" runat="server">
    
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Main Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:ListView ID="ListView1" runat="server" DataSourceID="LinqDataSource1">
        <ItemTemplate>
            <table class="Information">
                <tr runat="server">
                    <td>
                        <asp:Label ID="titleLabel" runat="server" Text='<%#Eval("INFORMATION_TITLE") %>'
                            CssClass="titleInfo" />
                        <br />
                        by :
                        <asp:Label ID="createdByLabel" runat="server" Text='<%#Eval("CREATED_BY") %>' CssClass="authorInfo"></asp:Label>
                        <asp:Label ID="createdDateLabel" runat="server" Text='<%#Eval("CREATED_DT") %>' CssClass="authorInfo"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 20px">
                        <asp:Label ID="informationLabel" runat="Server" Text='<%#Eval("INFORMATION_DESC") %>'
                            CssClass="contentInfo" />
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:ListView>
    
    <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
        OnSelecting="LinqDataSource1_Selecting">
    </asp:LinqDataSource>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentTaskList" runat="server">
    <asp:UpdatePanel runat="server" ID="upnWorklist">
    <ContentTemplate>
    <dx:ASPxGridView ID="gvWorklist" runat="server" KeyFieldName="Folio" 
        AutoGenerateColumns="False"  ClientInstanceName="gvWorklist"                
        OnHtmlRowPrepared="gvWorklist_HtmlRowPrepared" 
        Width="100%" OnHtmlRowCreated="gvWorklist_HtmlRowCreated">
        <Columns>
            <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px" Name="cmd">
                <EditButton Visible="false" Text=" ">
                </EditButton>
                <UpdateButton Visible="false" Text=" ">
                </UpdateButton>
                <NewButton Visible="false" Text=" ">
                </NewButton>
                <CancelButton Visible="false" Text=" ">
                </CancelButton>
                <HeaderTemplate>
                    <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientSideEvents-CheckedChanged="function(s, e) { gvWorklist.SelectAllRowsOnPage(s.GetChecked()); }"
                        CheckState="Unchecked" Style="text-align: center">
                    </dx:ASPxCheckBox>
                </HeaderTemplate>
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </HeaderStyle>
            </dx:GridViewCommandColumn>

            <dx:GridViewDataTextColumn Caption="No." Name="colNo" ReadOnly="True" 
                VisibleIndex="0" Width="50px" Visible="false">
                <dataitemtemplate>
                    <asp:Literal ID="litNo" runat="server"></asp:Literal>
                </dataitemtemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Visible="false" Name="Rejected" FieldName="REJECTED">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Visible="false" Name="notices" FieldName="Notices"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Visible="false" Name="worklistNotice" FieldName="WorklistNotice"></dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Notice" Name="notice" ReadOnly="True"
                VisibleIndex="1" FieldName="NeedReply" Width="50px">
                <DataItemTemplate><center>
                    <asp:Image ID="imgStatus" runat="server" Height="18px" Width="18px" ImageUrl="~/App_Themes/BMS_Theme/Images/void.png" />
					</center>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            
             <dx:GridViewDataTextColumn Caption="Accrued" Name="Type" ReadOnly="True" VisibleIndex="2" FieldName="Type" Width="60px">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Document No" Name="colDetail" 
                VisibleIndex="3"
                Width="120px" >
                <DataItemTemplate>
                    <asp:HyperLink runat="server" ID="hypDetail" Text="Detail" NavigateUrl="#"/>
                    <sub><asp:Label runat="server" ID="lblNotices" Text="(n)"  Visible="false" CssClass="notices"/></sub>
                    <asp:Label runat="server" ID="lblHoldFlag" Text="*" Visible="false" CssClass="holdflag"/>
                </DataItemTemplate>
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
           
             <dx:GridViewDataTextColumn Caption="Document No." Name="colDocNo" ReadOnly="True"
                VisibleIndex="4" FieldName="DocNumber" Width="100px" Visible="False">
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" Name="colDesc" VisibleIndex="5"
                ReadOnly="true" FieldName="Description" Width="245px" 
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Division" Name="DivName" VisibleIndex="6" FieldName="DivName"
                HeaderStyle-HorizontalAlign="Center"
                Width="55px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Amount (IDR)" Name="colTotal" VisibleIndex="7"
                Width="125px"
                HeaderStyle-HorizontalAlign="Right"
                ReadOnly="true" FieldName="TotalAmount">
                <PropertiesTextEdit DisplayFormatString="N0"/>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Doc Age" Name="colYear" VisibleIndex="8"
                ReadOnly="true" FieldName="WorklistTme" Width="65px" 
                HeaderStyle-HorizontalAlign="Center">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Folio" Name="colFolio" VisibleIndex="9"
                ReadOnly="true" FieldName="Folio" Visible="False">
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Created Date" Name="colStartDate" 
                ReadOnly="true" FieldName="CreatedDate" Visible="true" VisibleIndex="10" 
                Width="85px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            
            <dx:GridViewDataTextColumn Caption="Status" Name="StatusName" 
                ReadOnly="true" FieldName="StatusName" Visible="true" VisibleIndex="11" 
                Width="115px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="PV Type" Name="PvTypeName" 
                ReadOnly="true" FieldName="PvTypeName" Visible="true" VisibleIndex="12" 
                Width="85px" >
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="Activity Dt Fr" Name="ActivityDateFrom" 
                ReadOnly="true" FieldName="ActivityDateFrom" Visible="true" VisibleIndex="13" 
                Width="95px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Activity Dt To" Name="ActivityDateFrom" 
                ReadOnly="true" FieldName="ActivityDateTo" Visible="true" VisibleIndex="14" 
                Width="95px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Pay Method" Name="PayMethodName" 
                ReadOnly="true" FieldName="PayMethodName" Visible="true" VisibleIndex="15" 
                Width="85px" >
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Budget No" Name="BudgetNo" 
                ReadOnly="true" FieldName="BudgetNo" Visible="true" VisibleIndex="16" 
                Width="165px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle VerticalAlign="Middle" />
            </dx:GridViewDataTextColumn>

            <dx:GridViewDataTextColumn Caption="DATE"
              Name="DocDateCol" ReadOnly="true" FieldName="DocDate" Visible="false" VisibleIndex="99">
              </dx:GridViewDataTextColumn>

        </Columns>
        <Settings ShowVerticalScrollBar="True" 
            VerticalScrollableHeight = "20"
            ShowHorizontalScrollBar="true" />
        <SettingsPager Mode="ShowAllRecords" />
        <SettingsBehavior ColumnResizeMode="Control" />
        <Styles>
            <Cell VerticalAlign="Middle" CssClass="smallFont">
            </Cell>
        </Styles>
    </dx:ASPxGridView>
    <div id="divButtonApprove" runat="server" Visible="false">
 			<div style="margin: 4px 0 0 0; width: 50%; float: left; display: block;">
                <asp:Button ID="btnApprove" runat="server" 
                     CssClass="approveButton" SkinID="approveButton"
                     Text="Approve" 
                     Height="24px" Width="100px" 
                     OnClick="btnApprove_Click" OnClientClick="loading()" />

                </div>
                <div style="margin: 4px 0 0 0; width: 50%; float: right; display:block;" class="buttonRight">

                  <asp:Button ID="btnReject" runat="server"  SkinID="rejectButton" CssClass="rejectButton" 
                    Text="Reject"
                    Height="24px" Width="100px"                                        
                    OnClick="btnReject_Click"/>                   

                </div>
        
    
    </div>
    </ContentTemplate>
    </asp:UpdatePanel>

    <%//********************* region Approval popup  ***********************%>
    <asp:UpdatePanel ID="upnApproval" runat="server">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidButtonApproval" runat="server" style="display:none;" />
            <cc1:ModalPopupExtender ID="popUpApproval" runat="server"
            TargetControlID="hidButtonApproval"
            PopupControlID="pnlApproval"
            CancelControlID="btnApprovalCancel"
            BackgroundCssClass="modalBackground" />    
            <center>
            <asp:Panel ID="pnlApproval" runat="server" CssClass="speakerPopupList">
                    <div class="confirm">
                        <div class="confirmLabel">
                            <label>
                                Document No</label></div>
                        <div class="confirmValue">
                            <asp:TextBox ID="txtApprovalDocNo" CssClass="confirmText" runat="server" Enabled="false"
                                TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="confirm">
                        <div class="confirmLabel">
                            <label>
                                Notes</label></div>
                        <div class="confirmValue">
                            <asp:TextBox ID="txtNotes" CssClass="confirmText" runat="server" Enabled="true" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="confirmButton">
                        <asp:Button ID="btnApprovalProceed" runat="server" Text="Proceed" OnClick="btnApprovalProceed_Click"
                            OnClientClick="window.scrollTo(0,0);loading()" />&nbsp;
                        <asp:Button ID="btnApprovalCancel" runat="server" Text="Cancel" OnClientClick="window.scrollTo(0,0);loading()" />
                    </div>
            </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%//********************* end region Approval popup  ***********************%>

    <%//Region Error %>
    <asp:UpdatePanel ID="upnlError" runat="server">
        <ContentTemplate>
        <asp:Button ID="hidBtnError" runat="server" style="display:none;" />
        <cc1:ModalPopupExtender ID="ErrorPopUp" runat="server"
        TargetControlID="hidBtnError"
        PopupControlID="ErrorPanel"
        CancelControlID="BtnCancel"
        BackgroundCssClass="modalBackground" />    
            <center>
                <asp:Panel ID="ErrorPanel" runat="server" CssClass="informationPopUp">
                    <center>
                    <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblErorData" runat="server"></asp:Label>
                    <br />
                    <asp:Button ID="BtnCancel" Text="OK" runat="server"  
                            Width="60px" />        
                    </center>                     
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%//Region Error %>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentNoticeList" runat="server">
    <asp:UpdatePanel runat="server" ID="upnNotice">
    <ContentTemplate>
    <dx:ASPxGridView ID="gridNoticeList" runat="server" KeyFieldName="DOC_NO" 
        AutoGenerateColumns="False" ClientInstanceName="gridNoticeList"        
        Width="100%" OnHtmlRowCreated="gridNoticeList_HtmlRowCreated" 
        DataSourceID="LinqDataSource2">
        <Columns>
            <dx:GridViewDataTextColumn Caption="No." Name="colNo" ReadOnly="True" 
                HeaderStyle-HorizontalAlign="Center" VisibleIndex="0" Width="40px">
                <dataitemtemplate>
                    <asp:Literal ID="litNo" runat="server"></asp:Literal>
                </dataitemtemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Action" Name="colDetail" VisibleIndex="1" 
                Width="80px" FieldName="DOC_NO">
                <DataItemTemplate>
                    <asp:Button runat="server" ID="btnDetail" Text="View"/>
                    <%-- <asp:HyperLink runat="server" ID="hypDetail" Text="" NavigateUrl="#"/> --%>
                </DataItemTemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Notice" Name="status" VisibleIndex="2"
                Width="50px" FieldName="REPLY_FLAG">                
                <dataitemtemplate>
                    <center>
                        <asp:Image ID="imgStatus" runat="server" Height="15px" Width="15px" />
                        <%--<asp:Literal ID="litStatus" runat="server" Text=""></asp:Literal>--%>
                    </center>
                </dataitemtemplate>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="To" Name="noticeTo" VisibleIndex="3"
                ReadOnly="true" FieldName="NOTICE_TO_NAME" Width="100px">
                <HeaderStyle HorizontalAlign="Left" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Description" Name="description" VisibleIndex="4" 
                ReadOnly="true" FieldName="DESCRIPTION" Width="245px">
                <%--<dataitemtemplate>
                    <asp:Literal ID="litNotice" runat="server"></asp:Literal>
                </dataitemtemplate>--%>
                <HeaderStyle HorizontalAlign="Left" />
                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Division" Name="divName" VisibleIndex="5" 
                ReadOnly="true" FieldName="DIVISION_NAME" Width="55px">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Total Amount (IDR)" Name="colTotal" VisibleIndex="6"
                Width="125px" FieldName="TOTAL_AMOUNT_IDR"
                ReadOnly="true">
                <PropertiesTextEdit DisplayFormatString="N0"/>
                <HeaderStyle HorizontalAlign="Right" />
                <CellStyle HorizontalAlign="Right" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataTextColumn Caption="Doc No." Name="colDoc" VisibleIndex="7" 
                Width="90px" FieldName="DOC_NO">
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataTextColumn>
            <dx:GridViewDataDateColumn Caption="Created Date" FieldName="NOTICE_DT" 
                Name="createdDt" ReadOnly="True" VisibleIndex="11" Width="120px">
                <PropertiesDateEdit DisplayFormatString="dd/MM/yyyy HH:mm" EditFormat="Custom">
                </PropertiesDateEdit>
                <HeaderStyle HorizontalAlign="Center" />
                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                </CellStyle>
            </dx:GridViewDataDateColumn>
        </Columns>
        <Settings ShowVerticalScrollBar="True" VerticalScrollableHeight="100" ShowHorizontalScrollBar="true" />
        <SettingsPager Mode="ShowAllRecords" />
        <SettingsBehavior ColumnResizeMode="Control" />
        <Styles>
            <Cell VerticalAlign="Middle" CssClass="smallFont">
            </Cell>
        </Styles>
    </dx:ASPxGridView>
	 <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="Common.Data"
                EntityTypeName="" 
                TableName="NoticeHomeData" OnSelecting="LinqDataSource2_Selecting" OrderBy="NOTICE_DT descending">
            </asp:LinqDataSource>
    </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="BtnLoading" runat="server" Style="display: none;" />
    <asp:UpdatePanel runat="server" ID="upnLoadingPanel">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BtnLoading" />
        </Triggers>
        <ContentTemplate>
            <!-- LoadingPanel BEGIN -->
            <div id="loadingPanelPre" style="display: none;">
            </div>
            <dx:ASPxLoadingPanel ID="LoadingPanel" runat="server" ClientInstanceName="LoadingPanel"
                ClientIDMode="Static" Text="" LoadingDivStyle-Border-BorderColor="Transparent"
                Border-BorderColor="Transparent" BackColor="Transparent" Image-Url="~/App_Themes/BMS_Theme/Images/ajax-loader.gif"
                Modal="true">
            </dx:ASPxLoadingPanel>
            <dx:ASPxCallback ID="ASPxCallback1" ClientInstanceName="Callback" runat="server"
                ClientIDMode="Static">
                <ClientSideEvents CallbackComplete="function(s, e) { LoadingPanel.Hide(); }" />
            </dx:ASPxCallback>
            <div id="loadingPanelPost" style="display: none;">
            </div>
            <!-- LoadingPanel END -->
        </ContentTemplate>
    </asp:UpdatePanel>
    <dx:ASPxLoadingPanel ID="ASPxGridLoadingPanel" ClientInstanceName="gridLoadingPanel"
        runat="server" Modal="true">
    </dx:ASPxLoadingPanel>
</asp:Content>

