﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using AjaxControlToolkit;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using Common.Data;
using Common.Data._40RVFormList;
using Common.Function;
using DataLayer.Model;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;
using Common; 

namespace ELVISDashboard._40RvFormList
{
    public partial class RVApproval : BaseCodeBehind
    {

        #region Init
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_4003");

        private ArrayList listMsg = new ArrayList();
        
        private const int PRE_RECEIVE = 60; /// was "67"
        private const int PRE_CHECK = 65;   /// was "68"

        #region Getter Setter for Data List
        private List<RVApprovalData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<RVApprovalData>();
                }
                else
                {
                    return (List<RVApprovalData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        protected bool reloadData = false;
        protected RVApprovalData _data = null;
        protected RVApprovalData data
        {
            get
            {
                if (_data == null || reloadData)
                {
                    return reload();
                }
                return _data;
            }
        }

        private RoleLogic _rolo = null;
        protected RoleLogic rolo
        {
            get
            {
                if (_rolo == null)
                {
                    _rolo = new RoleLogic(UserName, _ScreenID);
                }
                return _rolo;
            }
        }

        protected RVApprovalData reload()
        {
            logic.Say("reload", "RVApproval({0},{1})", txtRVNo.Text, txtRVYear.Text);
            _data = logic.RVApproval.Search(txtRVNo.Text, txtRVYear.Text);
            return _data;
        }
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            this.Master.ProceedButtonClicked += new EventHandler(btnApprovalProceed_Click);
            lit = litMessage;

            if (!IsPostBack)
            {
                PrepareLogout();
                hidScreenID.Value = _ScreenID;
                hidUserName.Value = UserName;
                if (!IsPostBack)
                {
                    #region Init Parameter
                    String varRVNo = Request.QueryString["rvno"];
                    String varRVYear = Request.QueryString["rvyear"];
                    #endregion

                    #region Init Startup Script
                    noticeComment.Attributes.Add("style", "display: inline; float: left; width: 100%;");
                    #endregion

                    #region Init button
                    setFirstLoad();
                    #endregion

                    if (!String.IsNullOrEmpty(varRVNo) && !String.IsNullOrEmpty(varRVYear))
                    {
                        txtNotice.Focus();

                        txtRVYear.Text = varRVYear;
                        txtRVNo.Text = varRVNo;

                        txtRVNo.Enabled = false;
                        txtRVYear.Enabled = false;

                        #region Search data
                        if (validateSearchCriteria())
                        {
                            loadData();
                        }
                        #endregion
                    }
                    else
                    {
                        if (txtRVNo.Enabled)
                        {
                            txtRVNo.Focus();
                        }

                        clearFields();
                    }
                }
            }
            
            foreach (var msg in listMsg)
            {
                litMessage.Text += msg;
            }
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            try
            {
                ListInquiry = logic.RVApproval.SearchDetail(txtRVNo.Text, txtRVYear.Text);
                e.Result = ListInquiry;

                if (ListInquiry.Any())
                {
                    litTotalAmount.Text = "<table width=\"100%\" id=\"TotalAmountTab\"><tr>";
                    Dictionary<String, Decimal> amounts = new Dictionary<String, Decimal>();
                    foreach (var data in ListInquiry)
                    {
                        if (amounts.ContainsKey(data.CURRENCY_CD))
                        {
                            amounts[data.CURRENCY_CD] = amounts[data.CURRENCY_CD] + (data.AMOUNT_VALUE??0);
                        }
                        else
                        {
                            amounts.Add(data.CURRENCY_CD, data.AMOUNT_VALUE??0);
                        }
                    }

                    foreach (var data in amounts)
                    {
                        if (data.Key == "IDR" || data.Key == "JPY")
                        {
                            litTotalAmount.Text += String.Format("<td class='style5'>{0} {1:N0}</td>", data.Key, data.Value);
                        }
                        else
                        {
                            litTotalAmount.Text += String.Format("<td class='style5'>{0} {1:N2}</td>", data.Key, data.Value);
                        }
                    }
                    litTotalAmount.Text += "</tr></table>";
                }
                else
                {
                    litTotalAmount.Text = "";
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }

        }

        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            try
            {
                var data = logic.RVApproval.SearchAttachmentInqury(txtRVNo.Text, txtRVYear.Text);
                e.Result = data;
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        protected void gridApprovalList_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                Literal litGridNoGeneralInfo = gridApprovalList.FindRowCellTemplateControl(
                    e.VisibleIndex, gridApprovalList.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                if (litGridNoGeneralInfo != null)
                {
                    litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
                }

                Literal litGridStdWording = gridApprovalList.FindRowCellTemplateControl(
                    e.VisibleIndex, gridApprovalList.Columns["DESCRIPTION"] as GridViewDataColumn, "litStdWording") as Literal;
                if (litGridStdWording != null)
                {
                    litGridStdWording.Text = hidStdWording.Value;
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        protected void gridAttachment_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                Literal litGridNoGeneralInfo = gridAttachment.FindRowCellTemplateControl(
                    e.VisibleIndex, gridAttachment.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                if (litGridNoGeneralInfo != null)
                {
                    litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
                }

                HyperLink lnkFile = gridAttachment.FindRowCellTemplateControl(
                    e.VisibleIndex, gridAttachment.Columns["FILE_NAME"] as GridViewDataColumn, "lnkFile") as HyperLink;
                if (lnkFile != null)
                {
                    lnkFile.Text = e.GetValue("FILE_NAME").str();
                    lnkFile.NavigateUrl = System.Web.HttpUtility.UrlPathEncode(e.GetValue("URL").str());
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        protected void rptrNotice_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            RepeaterItem item = e.Item;
            if (item.ItemType == ListItemType.AlternatingItem || item.ItemType == ListItemType.Item)
            {
                Literal litOpenDivNotice = (Literal)item.FindControl("litOpenDivNotice");
                Literal litRptrNoticeComment = (Literal)item.FindControl("litRptrNoticeComment");
                Literal litCloseDivNotice = (Literal)item.FindControl("litCloseDivNotice");
                String dateHeader;

                var data = (NoticeData) item.DataItem;

                if (data.NOTICE_DT.HasValue)
                {
                    litOpenDivNotice.Text = "<div class='noticeLeft'>";
                    dateHeader = ((DateTime)data.NOTICE_DT).ToString("d MMMM yyyy hh:mm:ss");
                }
                else
                {
                    litOpenDivNotice.Text = "<div class='noticeRight'>";
                    dateHeader = ((DateTime)data.REPLY_DT).ToString("d MMMM yyyy hh:mm:ss");
                }

                litRptrNoticeComment.Text = String.Format(
                    "<span class='noteSender'>{0}</span> {1}<br>To: <span class='noteSender'>{2}</span><br>{3}"
                    , data.NOTICE_FROM_NAME
                    , dateHeader
                    , data.NOTICE_TO_NAME
                    , data.NOTICE_COMMENT);

                litCloseDivNotice.Text = "</div>";
            }
        }

        /*Modified by Nia - Filling dropdown list user data in notice*/
        #region DropDown AutoComplete Notice To
        #region cbGridPrevPartNumber_ItemRequestedByValue
        protected void cbNoticeTo_ItemRequestedByValue(object source, EventArgs e)
        {
            _m.SetMessage("", "");

            ASPxComboBox cbNoticeTo = (ASPxComboBox)source;

            cbNoticeTo.DataSource = logic.Notice.GetNoticeUser(txtRVNo.Text, txtRVYear.Text);
            cbNoticeTo.DataBind();
        }
        #endregion
        #endregion
        /*End modified*/

        #endregion

        #region Button

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            logic.Say("btnSearch_Click", "Search {0} {1}", txtRVNo.Text, txtRVYear.Text);
            litMessage.Text = "";
            listMsg = new ArrayList();
            try
            {
                #region Search data
                if (validateSearchCriteria())
                {
                    loadData();
                    initButton();
                }
                #endregion
            }
            catch (Exception ex)
            {
                listMsg.Add(
                    _m.SetMessage(
                        string.Format(
                            resx.Message( "MSTD00002ERR")
                        , ex.Message)
                        , "ERR"));
                LoggingLogic.err(ex);
            }
            foreach (var msg in listMsg)
            {
                litMessage.Text += msg;
            }
        }

        protected void btnReleaseNotice_Click(object sender, EventArgs e)
        {
            logic.Say("btnReleaseNotice_Click", "releaseNotice({0}, {1}, {2}, {3})", txtRVNo.Text, txtRVYear.Text, txtNoticeNo.Text, UserData.USERNAME);
            ErrorData err = new ErrorData();
            rptrNotice.DataSource = logic.Notice.releaseNotice(txtRVNo.Text, txtRVYear.Text, txtNoticeNo.Text, UserData.USERNAME, ref err);
            rptrNotice.DataBind();

            if (err.ErrID == 1)
            {
                litMessage.Text = Nagging(err.ErrMsgID, err.ErrMsg);
            }
            else if (err.ErrID == 0)
            {
                Nag("MSTD00023INF");
            }
        }

        protected void btnNoticeSave_Click(object sender, EventArgs e)
        {
            logic.Say("btnNoticeSave_Click", " ");
            litMessage.Text = "";
            String noticeTo = "";
            try
            {
                if (txtNotice.Text.isEmpty())
                {
                    Nag("MSTD00017WRN", "Notice");
                    return;
                }
                if (txtSendTo.Value.str().isEmpty())
                {
                    Nag("MSTD00017WRN", "Notice To");
                    return;
                }

                String noticeMessage = "";
                noticeTo = txtNotice.Text;

                noticeMessage = txtNotice.Text;

                InsertNotice(noticeMessage, "Comment");
                
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
            txtNotice.Text = "";
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            logic.Say("btnApprove_Click", "Approve {0} {1}", txtRVNo.Text, txtRVYear.Text);
            Master.ApprovalType = Common.Enum.ApprovalEnum.Approve;
            // Master.btnApprove_Click(sender, e, txtRVNo.Text, txtRVYear.Text);
            btnApprovalProceed_Click(sender, e);
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {
            logic.Say("btnReject_Click", "Reject {0} {1}", txtRVNo.Text, txtRVYear.Text);
            Master.btnReject_Click(sender, e, txtRVNo.Text, txtRVYear.Text);
            
        }

        protected void btnReceived_Click(object sender, EventArgs e)
        {
            logic.Say("btnReceived_Click", "Receive {0} {1}", txtRVNo.Text, txtRVYear.Text);
            litMessage.Text = "";

            bool Ok = false;

            string selectedConfirmation = hidDialogConfirmation.Value;
            if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
            {
                int pid = 0;
                pid = log.Log("INF", "btnReceived_Click", "btnReceive", UserName, "");
                try
                {
                    List<string> rvs = new string[] { txtRVNo.Text + ":" + txtRVYear.Text }.ToList();
                    string msg = "";

                    Ok = logic.RVList.RvCheck(rvs, pid, ref msg, UserName, 1);
                    if (Ok)
                    {
                        #region Get --> Command
                        string _Command = resx.K2ProcID( "Form_Registration_Approval");
                        #endregion
                        string _comment = "";
                        #region Do --> Approve-Reject
                        string reff = txtRVNo.Text + txtRVYear.Text;
                        Ok = logic.PVRVWorkflow.Go(reff, _Command, UserData, _comment);
                        if (Ok)
                        {
                            logic.RVApproval.SetReceivedDate(txtRVNo.Text.Int(), txtRVYear.Text.Int());
                            logic.WorkList.Remove(reff);
                        }
                        
                        #endregion
                    } 
                    Nag((Ok) ? "MSTD00060INF" : "MSTD00062ERR", "Receive");

                }
                catch (Exception Ex)
                {
                    string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                    litMessage.Text += _m.SetMessage(_ErrMsg, "ERR");
                    LoggingLogic.err(Ex);
                }

                if (WaitForit(Ok))
                {
                    loadData();
                    initButton();
                }
            }
        }

        protected void btnDirectToMe_clicked(object sender, EventArgs e)
        {
            logic.Say("btnDirectToMe_Click", "Direct To Me");
            litMessage.Text = "";
            if (MessageBox.Show("Are you sure you wish to redirect this RV No?", "Confirm Redirect", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                bool success = false;

                try
                {
                    string byPassUser = logic.Look.GetLastUser(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME, "Direct");

                    #region Do --> Approve-Reject
                    string reff = txtRVNo.Text + txtRVYear.Text;
                    if (logic.PVRVWorkflow.Redirect(reff, UserData, byPassUser))
                    {
                        success = true;
                        Nag("MSTD00060INF", "Redirect");
                        logic.WorkList.Remove(reff);
                    }
                    else
                    {
                        Nag("MSTD00062ERR", "Redirect");
                    }
                    #endregion
                }
                catch (Exception Ex)
                {
                    string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                    litMessage.Text += _m.SetMessage(_ErrMsg, "ERR");
                    LoggingLogic.err(Ex);
                }

                if (success)
                {
                    int timeOut = 0;
                    while (logic.WorkList.isUserHaveWorklist(txtRVNo.Text + txtRVYear.Text))
                    {
                        timeOut++;
                        if (timeOut == 1200)
                        {
                            Nag("MSTD00082WRN");
                            break;
                        }
                    }

                    if (timeOut < 1200)
                    {
                        loadData();
                    }
                }
            }
        }

        protected void btnChecks_Click(object sender, EventArgs e)
        {
            logic.Say("btnChecks_Click", "Check");
            litMessage.Text = "";
            reload();
            string selectedConfirmation = hidDialogConfirmation.Value;
            if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
            {
                bool Ok = false;
                string _Comment = "";
                System.Web.UI.WebControls.TextBox txtApprovalComment = Master.FindControl("txtApprovalComment") as System.Web.UI.WebControls.TextBox;
                _Comment = "";
                int pid = log.Log("INF", "CHECK RV", "btnChecks", UserName, "", 0);

                try
                {
                    string msg = "";
                    Ok = logic.RVList.RvCheck(new string[] { txtRVNo.Text + ":" + txtRVYear.Text }.ToList(), pid, ref msg, UserName);

                    // if (false)
                    if (Ok)
                    {
                        #region Get --> Command
                        // string _Command = _globalResource.GetResxObject("K2ProcID", "DOC_CHECKED_BY_ACCOUNTING");
                        string _Command = resx.K2ProcID( "Form_Registration_Approval");
                        #endregion

                        #region Do --> Approve-Reject

                        Ok = logic.PVRVWorkflow.Go(txtRVNo.Text + txtRVYear.Text, _Command, UserData, _Comment);

                        #endregion
                        Nag((Ok) ? "MSTD00069INF" : "MSTD00070ERR");
                    }
                    else
                    {
                        Nag("MSTD00062ERR", "Check");
                    }

                }
                catch (Exception Ex)
                {
                    string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                    litMessage.Text += _m.SetMessage(_ErrMsg, "ERR");
                    LoggingLogic.err(Ex);
                }

                if (WaitForit(Ok))
                {
                    logic.RVApproval.UpdateSettlement(txtRVNo.Text, txtRVYear.Text);

                    loadData();
                }
            }
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            logic.Say("btnVerify_Click", "Verify {0} {1}", txtRVNo.Text, txtRVYear.Text);
            string loc = "Verify";
            int no = txtRVNo.Text.Int(0);
            int yy = txtRVYear.Text.Int(0);

            litMessage.Text = "";

            string selectedConfirmation = hidDialogConfirmation.Value;
            if (!string.IsNullOrEmpty(selectedConfirmation) && (selectedConfirmation.ToLower().Equals("y")))
            {
                bool Ok = false;
                try
                {
                    int pid = log.Log("MSTD000001INF", "Verified", "btnVerify_Click()", (UserData != null) ? UserName : "", "", 0);
                    Ok = logic.RVApproval.Verified(no, yy, UserData);
                    Nag((Ok) ? "MSTD00060INF" : "MSTD00062ERR", loc);
                    loadData();
                    logic.WorkList.Remove(txtRVNo.Text + txtRVYear.Text);
                    initButton();
                }
                catch (Exception ex)
                {
                    litMessage.Text += MessageLogic.Wrap(ex.Message, "ERR");
                    Handle(ex);
                }
            }
        }

        private bool WaitForit(bool success)
        {
            if (success)
            {
                string[] newStatus = logic.RVApproval.GetNewStatus(txtRVNo.Text, txtRVYear.Text);

                int timeOut = 0;

                while (litStatus.Text == newStatus[1])
                {
                    newStatus = logic.RVApproval.GetNewStatus(txtRVNo.Text, txtRVYear.Text);
                    System.Threading.Thread.Sleep(100 + ((timeOut * 20) % 250));
                    timeOut++;
                    if (timeOut >= 1200)
                    {
                        Nag("MSTD00082WRN");
                        break;
                    }
                }
                return (timeOut < 1200);
            }
            else
                return false;
        }

        protected void btnApprovalProceed_Click(object sender, EventArgs e)
        {
            log.Say("btnApprovalProceed_Click", "Approve {0} {1}", txtRVNo.Text, txtRVYear.Text);
            litMessage.Text = "";
            string _Comment = "";
            System.Web.UI.WebControls.TextBox txtApprovalComment = Master.FindControl("txtApprovalComment") as System.Web.UI.WebControls.TextBox;

            ModalPopupExtender popUpApproval = Master.FindControl("popUpApproval") as ModalPopupExtender;

            bool success = false;

            try
            {
                if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve || (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject && txtApprovalComment.Text != ""))
                {
                    #region Get --> Comment
                    if (string.IsNullOrEmpty(txtApprovalComment.Text))
                    {
                        _Comment = "N/A";
                    }
                    else
                    {
                        _Comment = txtApprovalComment.Text;
                    }
                    #endregion

                    #region Get --> Command
                    string _Command = "";
                    if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        _Command = resx.K2ProcID( "Form_Registration_Approval");
                    else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        _Command = resx.K2ProcID( "Form_Registration_Rejected");
                    #endregion
                    string reffNo = txtRVNo.Text + txtRVYear.Text;
                    #region Do --> Approve-Reject
                    if (logic.PVRVWorkflow.Go(reffNo, _Command, UserData, _Comment))
                    //if (true)
                    {
                        reload();
                        success = true;
                        if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            Nag("MSTD00060INF", "Approve");
                        }
                        else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            Nag("MSTD00060INF", "Reject");
                        }
                        logic.WorkList.Remove(reffNo);
                        ReloadOpener();
                    }
                    else
                    {
                        if (Master.ApprovalType == Common.Enum.ApprovalEnum.Approve)
                        {
                            Nag("MSTD00062ERR", "Approve");
                        }
                        else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                        {
                            Nag("MSTD00062ERR", "Reject");
                        }
                    }
                    #endregion
                }
                else if (txtApprovalComment.Text != "" && (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold || Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold))
                {
                    reload();
                    String userName = UserData.USERNAME;

                    ErrorData err = new ErrorData();
                    bool flag = logic.RVApproval.ChangeHoldValue(txtRVNo.Text, txtRVYear.Text, userName, ref err);

                    if (flag)
                    {
                        Nag("MSTD00060INF", "Un-Hold");
                    }
                    else
                    {
                        Nag("MSTD00060INF", "Hold");
                    }

                    if (txtApprovalComment.Text != "")
                    {
                        InsertNotice(txtApprovalComment.Text, "Hold");
                    }

                    logic.RVApproval.InsertTableHistory(txtRVNo.Text, txtRVYear.Text);
                    ReloadOpener();
                }
                else
                {
                    if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                    {
                        Nag("MSTD00088ERR", "Hold");
                    }
                    else if (Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
                    {
                        Nag("MSTD00088ERR", "Un-Hold");
                    }
                    else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                    {
                        Nag("MSTD00059ERR");
                    }
                }
            }
            catch (Exception Ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                litMessage.Text += _m.SetMessage(_ErrMsg, "ERR");
                LoggingLogic.err(Ex);
            }

            txtApprovalComment.Text = "";

            if (WaitForit(success))
            {
                if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                {
                    InsertNotice(_Comment, "Reject");

                    // added by FID.Ridwan 12072018 update outstanding balance
                    string[] newStatus = new string[3] { "", "", "" };
                    int no, yy;
                    Int32.TryParse(txtRVNo.Text, out no);
                    Int32.TryParse(txtRVYear.Text, out yy);

                    var rvData = logic.RVForm.search(no, yy);
                    if (rvData.TransactionCode == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                    {
                        //var stsUpdBal = logic.Sys.GetArray("DISTRIBUTION_STATUS_PARAM", "PVRV_BALANCE_ON_REJECT");

                        //logic.Say("btApprovalProceed_Click", "status update balance : {0}, new status : {1}"
                            //, CommonFunction.CommaJoin(stsUpdBal.ToList()), newStatus[0]);

                        //if (stsUpdBal.Contains(newStatus[0]))
                            logic.RVForm.updateBalanceOutstanding(rvData, UserData, false);

                    }
                    // end of added
                }
                loadData();
                initButton();
            }

        }
        #endregion

        #region Function
        private void loadData()
        {
            try
            {
                // header = logic.RVApproval.SearchHeaderInqury(txtRVNo.Text, txtRVYear.Text);
                reload();
                initButton();

                if (data != null)
                {
                    #region Data found
                    bool distributed = logic.Look.DistributedToMe(txtRVNo.Text, txtRVYear.Text, UserName);
                    bool canApprove = logic.User.IsAuthorizedFinance
                        || distributed
                        || logic.PoA.AttorneyGrantorOOFCount(UserName) > 0;
                    bool canShowData = canApprove ||  UserData.isInDivision(data.DIVISION_ID);
                    if (canShowData)
                    {
                        litRVDate.Text = data.RV_DATE.ToString("dd/MM/yyyy");
                        litStatus.Text = data.STATUS_NAME;
                        litTransactionType.Text = data.TRANSACTION_TYPE;
                        litVendorCode.Text = data.VENDOR_CD;
                        litPaymentMethod.Text = data.PAYMENT_METHOD;
                        litRVType.Text = data.RV_TYPE;
                        litVendorName.Text = data.VENDOR_NAME;
                        litIssuingDivision.Text = data.ISSUING_DIVISION;
                        //litTotalAmounIDR.Text = data.TOTAL_AMOUNT.ToString();
                        if (data.DUE_DATE != null)
                        {
                            litDueDate.Text = DateTime.Parse(data.DUE_DATE.ToString()).ToString("dd/MM/yyyy HH:mm");
                            txtDueDate.Text = DateTime.Parse(data.DUE_DATE.ToString()).ToString("MM/dd/yyyy HH:mm");
                        }
                        hidStatusCD.Value = data.STATUS_CD.ToString();
                        hidIssuingDivisionID.Value = data.DIVISION_ID;
                        hidStdWording.Value = data.STD_WORDING;

                        List<NoticeData> listNotice = logic.Notice.SearchNoticeInquiry(txtRVNo.Text, txtRVYear.Text);
                        rptrNotice.DataSource = listNotice;
                        rptrNotice.DataBind();

                        if (listNotice.Count > 0)
                        {
                            noticeComment.Attributes.Remove("style");
                            noticeComment.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");
                        }

                        gridApprovalList.DataSourceID = LinqDataSource1.ID;
                        gridApprovalList.DataBind();

                        gridAttachment.DataSourceID = LinqDataSource2.ID;
                        gridAttachment.DataBind();


                    }
                    if (canApprove)
                    {
                        btnApprove.Enabled = true;
                        btnReject.Enabled = true;
                        btnNoticeSave.Enabled = true;
                    }
                    else if (!canShowData) 
                    {
                        clearFields();
                        Nag("MSTD00045ERR");
                    }
                    #endregion
                }
                else
                {
                    #region No data found
                    clearFields();
                    Nag("MSTD00018INF");
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Nag("MSTD00002ERR", ex.Message);
                LoggingLogic.err(ex);
            }
        }

        private void InsertNotice(string _Comment, string mode)
        {
            String emailTemplate = String.Format("{0}\\NoticeCommentNotification.txt", Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE","DIR")));
            String noticeLink = String.Format("http://{0}:{1}{2}?rvno={3}&rvyear={4}",
                Request.ServerVariables["SERVER_NAME"],
                HttpContext.Current.Request.ServerVariables["SERVER_PORT"],
                Request.ServerVariables["URL"],
                txtRVNo.Text, txtRVYear.Text);
            EmailFunction mail = new EmailFunction(emailTemplate);
            ErrorData _Err = new ErrorData();
            String noticer = "";
            List<RoleData> listRole = rolo.getUserRole(UserData.USERNAME, ref _Err);
            List<RoleData> listRoleTo = null;
            String userRole = "";
            String userRoleTo = "";
            String noticeTo = "";
            String appType = "";
            if (listRole.Any())
            {
                userRole = listRole.FirstOrDefault().ROLE_NAME;
            }

            if (mode == "Comment")
            {
                if (txtSendTo.Value != null)
                {
                    {
                        noticeTo = txtSendTo.Value.ToString();
                        listRoleTo = rolo.getUserRole(noticeTo, ref _Err);

                        if (listRoleTo.Any())
                        {
                            userRoleTo = listRoleTo.FirstOrDefault().ROLE_NAME;
                        }
                    }
                }
                appType = "Notice";
            }
            else
            {
                noticeTo = logic.Look.GetLastUser(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME, mode);

                if (Master.ApprovalType == Common.Enum.ApprovalEnum.Reject)
                {
                    appType = "Reject";
                }
                else if (Master.ApprovalType == Common.Enum.ApprovalEnum.Hold)
                {
                    appType = "Hold";
                }
                else if (Master.ApprovalType == Common.Enum.ApprovalEnum.UnHold)
                {
                    appType = "Un-Hold";
                }

                _Comment = String.Format("{0} Reason:<br>{1}", appType, _Comment);
            }

            bool hasWorklist = logic.WorkList.isUserHaveWorklist(txtRVNo.Text + txtRVYear.Text);
            List<string> lstUser = logic.Notice.listNoticeRecipient(txtRVNo.Text,
                                      txtRVYear.Text, UserName, noticeTo);
            logic.Notice.InsertNotice(txtRVNo.Text,
                                      txtRVYear.Text,
                                      UserData.USERNAME,
                                      userRole,
                                      noticeTo,
                                      userRoleTo,
                                      lstUser,
                                      _Comment,
                                      mode != "Hold", hasWorklist);

            List<NoticeData> noticeList = logic.Notice.SearchNoticeInquiry(txtRVNo.Text, txtRVYear.Text);

            rptrNotice.DataSource = noticeList;
            rptrNotice.DataBind();

            //noticer = noticeList.Count > 0 ? noticeList.Where(a => a.NOTICE_DT != null).Last().CREATED_BY : UserData.USERNAME;

            if (mail.ComposeApprovalNotice_EMAIL(txtRVNo.Text, txtRVYear.Text,
                    _Comment, noticeLink, noticeTo, UserData, noticer, hidIssuingDivisionID.Value,
                    "RV", appType, ref _Err))
            {
                if (!mode.Equals("Reject"))
                    Nag("MSTD00024INF");
            }

            if (_Err.ErrID == 1)
            {
                litMessage.Text += _m.SetMessage(string.Format(resx.Message( _Err.ErrMsgID), _Err.ErrMsg), "WRN");
            }
            else if (_Err.ErrID == 2)
            {
                Nag("MSTD00002ERR", _Err.ErrMsg);
            }

            noticeComment.Attributes.Remove("style");
            
            noticeComment.Attributes.Add("style", "display: inline; float: left; overflow: scroll; overflow-x: hidden; height: 150px; width: 100%;");
        }

        private bool validateSearchCriteria()
        {
            Int32 rvNo;
            Int16 rvYear;

            #region RV No. or RV Year is not filled / invalid value
            if (String.IsNullOrEmpty(txtRVNo.Text))
            {
                listMsg.Add(Nagging("MSTD00017WRN", "RV No."));
            }
            else if (!Int32.TryParse(txtRVNo.Text, out rvNo))
            {
                listMsg.Add(Nagging("MSTD00018WRN", "RV No."));
            }

            if (String.IsNullOrEmpty(txtRVYear.Text))
            {
                listMsg.Add(Nagging("MSTD00017WRN", "RV Year"));
            }
            else if (!Int16.TryParse(txtRVYear.Text, out rvYear))
            {
                listMsg.Add(Nagging("MSTD00018WRN", "RV Year"));
            }
            else if (txtRVYear.Text.Length != 4)
            {
                listMsg.Add(Nagging("MSTD00019WRN", "RV Year", "4"));
            }
            #endregion

            return listMsg.Count == 0;

        }

        private void clearFields()
        {
            rptrNotice.DataSource = null;
            rptrNotice.DataBind();

            gridApprovalList.DataSourceID = null;
            gridApprovalList.DataBind();

            gridAttachment.DataSourceID = null;
            gridAttachment.DataBind();

            litRVDate.Text = "";
            litStatus.Text = "";
            litTransactionType.Text = "";
            litVendorCode.Text = "";
            litPaymentMethod.Text = "";
            litRVType.Text = "";
            litVendorName.Text = "";
            litIssuingDivision.Text = "";
            litDueDate.Text = "";
            txtDueDate.Text = "";

            litTotalAmount.Text = "";

            btnApprove.Enabled = false;
            btnReject.Enabled = false;
            btnNoticeSave.Enabled = false;
            btnReceived.Enabled = false;
            btnCheck.Enabled = false;
            //btnHold.Enabled = false;
            //btnDirectToMe.Enabled = false;

            noticeComment.Attributes.Remove("style");
            noticeComment.Attributes.Add("style", "display: inline; float: left; width: 100%;");
        }

        private void initButton()
        {
            reload();
            //RoleLogic rolo = new RoleLogic(UserData.USERNAME, _ScreenID);
            //bool status = logic.RVApproval.approvalStatus(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME);

            bool status = logic.WorkList.isUserHaveWorklist(txtRVNo.Text + txtRVYear.Text);
            int statusCd = hidStatusCD.Value.Int(-1);

            btnApprove.Visible = true & rolo.isAllowedAccess("btnApprove") && status;
            btnReject.Visible = true & rolo.isAllowedAccess("btnReject") && status;

            //seharusnya tidak main di status tapi hasworklist
            btnReceived.Visible = true & rolo.isAllowedAccess("btnReceived") && status; // (statusCd == PRE_RECEIVE);
            btnReceived.Enabled = btnReceived.Visible;

            btnCheck.Visible = true & rolo.isAllowedAccess("btnCheck") && (statusCd == PRE_CHECK);
            if (btnCheck.Visible)
            {
                btnCheck.Enabled = statusCd == PRE_CHECK;
            }

            btnVerify.Visible = rolo.isAllowedAccess("btnVerify")
                  && ((data.COUNTER_FLAG ?? 0) == 0 || data.HOLD_FLAG == 1);

            divVerified.Visible = !btnVerify.Visible && (data.COUNTER_FLAG ?? 0) == 1;
            //btnHold.Enabled = _RoleLogic.isAllowedAccess("btnHold") && logic.RVApproval.isCanHold(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME, hidStatusCD.Value);

            bool canDirect = logic.RVApproval.isCanDirect(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME) && !status;
            //btnDirectToMe.Enabled = canDirect && _RoleLogic.isAllowedAccess("btnDirectToMe");
            //btnDirectToMe.Visible = btnDirectToMe.Enabled;
        }

        private void setFirstLoad()
        {
            //RoleLogic rolo = new RoleLogic(UserData.USERNAME, _ScreenID);

            btnReceived.Visible = true && rolo.isAllowedAccess("btnReceived");
            btnCheck.Visible = true && rolo.isAllowedAccess("btnCheck");
            btnVerify.Visible = rolo.isAllowedAccess("btnVerify");

            btnApprove.Visible = true && rolo.isAllowedAccess("btnApprove");
            btnReject.Visible = true && rolo.isAllowedAccess("btnReject");

            //btnHold.Enabled = _RoleLogic.isAllowedAccess("btnHold") && logic.RVApproval.isCanHold(txtRVNo.Text, txtRVYear.Text, UserData.USERNAME, hidStatusCD.Value);
            //btnDirectToMe.Visible = _RoleLogic.isAllowedAccess("btnDirectToMe");

            btnNoticeSave.Visible = true;

            btnSearch.Visible = true && rolo.isAllowedAccess("btnSearch");

            txtRVNo.Enabled = true && rolo.isAllowedAccess("btnSearch");
            txtRVYear.Enabled = true && rolo.isAllowedAccess("btnSearch");

        }
        #endregion

        #region paging
        // const string GridCustomPageSizeName = "gridCustomPageSize";
        const string GridPageSize = "PVRV_GRIDSIZE";
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridPageSize] != null)
            {
                gridApprovalList.SettingsPager.PageSize = (int)Session[GridPageSize];
            }
            else
            {
                gridApprovalList.SettingsPager.PageSize = 5;
                Session[GridPageSize] = 5;
            }
        }
        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridApprovalList.SettingsPager.PageSize = newPageSize;
            Session[GridPageSize] = newPageSize;
            if (gridApprovalList.VisibleRowCount > 0)
                gridApprovalList.DataSourceID = LinqDataSource1.ID;
            gridApprovalList.DataBind();
        }
        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridApprovalList.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        protected string GetShowingOnPage()
        {
            int pageSize = gridApprovalList.SettingsPager.PageSize;
            if (gridApprovalList.PageIndex < 0)
                gridApprovalList.PageIndex = 0;
            int startIndex = gridApprovalList.PageIndex * pageSize + 1;
            int endIndex = (gridApprovalList.PageIndex + 1) * pageSize;
            if (endIndex > gridApprovalList.VisibleRowCount)
            {
                endIndex = gridApprovalList.VisibleRowCount;
            }
            return string.Format("Showing {0}-{1} of {2}", startIndex, endIndex, gridApprovalList.VisibleRowCount);
        }
        protected void gridApprovalList_PageIndexChanged(object sender, EventArgs e)
        {
            gridApprovalList.DataBind();
        }
        #endregion

    }
}