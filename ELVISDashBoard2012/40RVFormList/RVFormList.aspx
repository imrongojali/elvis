﻿<%@ Page Title="RV Form" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="RVFormList.aspx.cs" Inherits="ELVISDashboard._40RVFormList.RVFormList" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %> 
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>

<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common"%>
<%@ Register src="../UserControl/FormHint.ascx" tagname="FormHint" tagprefix="fh" %>
<asp:Content ContentPlaceHolderID="pre" ID="xPre" runat="server"> 
    <script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>   
    <script type="text/javascript">
        $(document).keydown(function () {
            if (e.which === 8 && !$(e.target).is("input, textarea")) {
                e.preventDefault();
            }
        });
    </script>
</asp:Content>

<asp:Content ID="sectionHeader" ContentPlaceHolderID="head" runat="server">
    
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HiddenField runat="server" ID="hidPageMode" />
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="RV Form" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static"/>
    <asp:HiddenField runat="server" ID="hdLastFocusedID" ClientIDMode="Static"/>
    <asp:HiddenField runat="server" ID="hdSelectedVendorCode" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdGridFocusedColumn" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdFinanceAccessFlag" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidReloadOpener" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidHasWorklist" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidOneTimeVendorValid" ClientIDMode="Static" />
    <br /><br />
    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>                 
            <asp:Literal runat="server" ID="messageControl" Visible="false" EnableViewState="false" OnLoad="evt_messageControl_onLoad" />            
        </ContentTemplate>
    </asp:UpdatePanel>   
    <br />
    <asp:UpdatePanel runat="server" ID="pnupdateContentSection">
        <ContentTemplate>
            <div class="contentsection">
                <dx:ASPxPageControl ID="PVFormListTabs" runat="server" ActiveTabIndex="0" EnableHierarchyRecreation="True" Height="170px" Width="970px" BackColor="#FFFFEF">
                    <TabPages>
                        <dx:TabPage Text="General Data" Name="GeneralData">
                            <ContentCollection>
                                <dx:ContentControl ID="GeneralDataCC" runat="server">
                                    <div class="contentsectionUnderTab">
                                        <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 937px;" class="control-layout-table">
                                            <tr>
                                                <td id="tdDocDate" valign="middle" style="width: 100px" class="td-layout-item">
                                                    RV Date <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px;" class="td-layout-value value-rv"> 
                                                    <dx:ASPxLabel runat="server" ID="tboxDocDate" Width="110px" CssClass="display-inline-table"/>
                                                </td>
                                                <td valign="middle" style="width: 100px" class="td-layout-item">
                                                    RV No <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">
                                                    <dx:ASPxLabel ID="tboxDocNo" runat="server" Width="110px" CssClass="display-inline-table"/>
                                                </td>
                                                <td valign="middle" style="width: 100px" class="td-layout-item">
                                                    Issuing Division <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">
                                                    <dx:ASPxLabel ID="tboxIssueDiv" runat="server" Width="110px" CssClass="display-inline-table"/>
                                                </td>
                                                <td valign="middle" style="width: 100px" class="td-layout-item">
                                                    Status <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">
                                                    <dx:ASPxLabel ID="tboxStatus" runat="server" Width="110px" CssClass="display-inline-table"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td valign="middle" style="width: 100px" class="mandatory_label td-layout-item">
                                                    RV Type <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px;" class="td-layout-value value-rv">
                                                    <dx:ASPxComboBox runat="server" Width="110px" ID="ddlDocType" ClientIDMode="static" 
                                                                     ValueType="System.String" ValueField="Code" CssClass="display-inline-table"
                                                                     TextField="Description" Visible="false" OnLoad="ddlDocType_Load" 
                                                                     OnSelectedIndexChanged="evt_ddlDocType_SelectedIndexChanged"/>
                                                    <dx:ASPxLabel ID="lblDocType" runat="server" Visible="false" CssClass="display-inline-table"/>
                                                </td>
                                                <td valign="middle" class="mandatory_label td-layout-item">
                                                    Activity Date <span class="right-bold">:</span>
                                                </td>
                                                <td class="td-layout-value value-rv" colspan="3">
                                                <div class="lefty">
                                                    <dx:ASPxDateEdit ID="dtActivityDateFrom" ClientInstanceName="dtActivityDateFrom" 
                                                        ClientIDMode="static" runat="server" Width="100px" 
                                                        DisplayFormatString="dd MMM yyyy" EditFormatString="dd/MM/yyyy" 
                                                        UseMaskBehavior="true" CssClass="display-inline-table">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic"/>
                                                            <ClientSideEvents DateChanged="OnDateChanged" Validation="OnDateValid" />
                                                    </dx:ASPxDateEdit>
                                                    <dx:ASPxLabel ID="lblActivityDate" runat="server" Visible="false" CssClass="display-inline-table"/>
                                                </div>
                                                <div class="to">
                                                    <dx:ASPxLabel ID="lblTo" runat="server" Text="  to  " CssClass="display-inline-table"/> 
                                                </div>
                                                <div class="lefty">
                                                    <dx:ASPxLabel ID="lblActivityDateTo" runat="server" Visible="false" CssClass="display-inline-table"/>                                                           
                                                    <dx:ASPxDateEdit ID="dtActivityDateTo" ClientInstanceName="dtActivityDateTo" ClientIDMode="static" 
                                                                    runat="server" Width="100px" DisplayFormatString="dd MMM yyyy" 
                                                                    EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" CssClass="display-inline-table">
                                                            <ValidationSettings ErrorDisplayMode="ImageWithTooltip" Display="Dynamic"/>
                                                            <ClientSideEvents DateChanged="OnDateChanged" Validation="OnDateValid"/>
                                                    </dx:ASPxDateEdit>
                                                </div>
                                                </td>                                               
                                                <td valign="middle" style="width: 100px" class="mandatory_label td-layout-item">
                                                    <div style="height: 100%; width: 55%; float: left;">Payment Method</div>
                                                    <div class="right-bold">:</div>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">
                                                    <dx:ASPxComboBox runat="server" Width="110px" ID="ddlPaymentMethod" CssClass="display-inline-table"
                                                                     ClientInstanceName="ddlPaymentMethod" ClientIDMode="static"
                                                                     ValueType="System.String" ValueField="Code" TextField="Description"
                                                                     OnLoad="ddlPaymentMethod_Load" AutoPostBack="false"
                                                                     OnSelectedIndexChanged="evt_ddlPaymentMethod_onSelectedIndexChanged"/>
                                                    <asp:HiddenField ID="hidPaymentMethod" runat="server" ClientIDMode="Static" />
                                                    <asp:Literal ID="lblPaymentMethod" runat="server" Visible="true"/>
                                                </td>
                                                <td valign="middle" style="width: 100px" class="td-layout-item"></td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv"></td>
                                            </tr>
                                            <tr>
                                                <td id="Td1" valign="middle" style="width: 100px" runat="server" class="mandatory_label td-layout-item">
                                                    Customer Code  <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">        
                                                 <div class="lefty">
                                                    <dx:ASPxGridLookup runat="server" 
                                                        ID="lkpgridVendorCode" 
                                                        ClientInstanceName="lkpgridVendorCode" 
                                                        SelectionMode="Single" KeyFieldName="VENDOR_CD" TextFormatString="{0}"
                                                        Width="95px" OnLoad="lkpgridVendorCode_Load"
                                                        CssClass="display-inline-table" 
                                                        IncrementalFilteringMode="Contains"
                                                        IncrementalFilteringDelay="1000">
                                                        <GridViewProperties EnableCallBacks="false">
                                                            <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true"/>
                                                            <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"/>
                                                            <SettingsPager PageSize="7"/>
                                                        </GridViewProperties>
                                                        <ClientSideEvents 
                                                            TextChanged="function(s,e) {__doPostBack('lkpgridVendorCode',lkpgridVendorCode.GetGridView().GetFocusedRowIndex()); }"/>                                              
                                                        <Columns>
                                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="20px"/>
                                                            <dx:GridViewDataTextColumn Caption="Customer Code" FieldName="VENDOR_CD" Width="100px">
                                                                <Settings AutoFilterCondition="Contains"/>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Search Terms" FieldName="SEARCH_TERMS" Width="200px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                            </dx:GridViewDataTextColumn>    
                                                             <dx:GridViewDataTextColumn Caption="Customer Name" FieldName="VENDOR_NAME" Width="150px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                            </dx:GridViewDataTextColumn>                                                            
                                                        </Columns>
                                                    </dx:ASPxGridLookup>
                                                    <dx:ASPxLabel ID="lblVendorCode"  runat="server" CssClass="display-inline-table" OnLoad="evt_lblVendorCode_onLoad"/>           

                                                   </div> 

                                                    <div class="lefty">
                                                    <dx:ASPxButton runat="server" ID="btAddNewVendor" 
                                                                    CssClass="vendor-code-addition-button"
                                                                    Image-Url="~/App_Themes/BMS_Theme/Images/green-plus.png"
                                                                    EnableDefaultAppearance="false" EnableTheming="false"
                                                                    Cursor="pointer">
                                                        <ClientSideEvents Click="function(s,e) { popAddVendor.Show(); }" />                
                                                        <Image Url="~/App_Themes/BMS_Theme/Images/green-plus.png">
                                                        </Image>
                                                    </dx:ASPxButton>   
                                                    </div>                                      
                                                </td>
                                                <td  class="td-layout-value value-rv" colspan="2">
                                                    <dx:ASPxLabel ID="lblVendorDesc" ClientInstanceName="lblVendorDesc" runat="server" 
                                                    CssClass="display-inline-table" OnLoad="evt_lblVendorDesc_onLoad"/>
                                                </td>
                                                <td valign="middle" style="width: 100px" class="td-layout-item">
                                                    Vendor Group <span class="right-bold">:</span>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv">
                                                    <dx:ASPxLabel runat="server" ID="lblVendorGroup" CssClass="display-inline-table"/>
                                                     <asp:HiddenField runat="server" ID="hidVendorGroup" ClientIDMode="Static" OnLoad="hidVendorGroup_Load" />
                                                </td>
                                                <td class="td-layout-item" colspan="2">
                                                    <dx:ASPxCheckBox runat="server" ID="chkCalculateTax" AutoPostBack="true" 
                                                        Text=" Calculate Tax" 
                                                        OnCheckedChanged="evt_chkCalculateTax_onCheckedChanged" Visible="false"/>
                                                </td>                                                                                                        
                                            </tr>
                                            <tr>
                                                <td valign="middle" class="mandatory_label td-layout-item">
                                                    <div style="height: 100%; width: 70%; float: left;">Transaction Type</div>
                                                    <div class="right-bold">:</div>
                                                </td>
                                                <td valign="middle" style="width: 125px" class="td-layout-value value-rv"> 
                                                   <dx:ASPxGridLookup runat="server" ID="ddlTransactionType" 
                                                                       ClientInstanceName="ddlTransactionType" 
                                                                       SelectionMode="Single" KeyFieldName="Code" 
                                                                       TextFormatString="{0}" ClientIDMode="Static"
                                                                       Width="110px" OnLoad="ddlTransactionType_Load" 
                                                                       CssClass="display-inline-table"
                                                                       IncrementalFilteringMode="Contains"
                                                                       IncrementalFilteringDelay="1000">
                                                        <GridViewProperties EnableCallBacks="false">
                                                            <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true"/>
                                                            <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true" ShowColumnHeaders="false"/>
                                                            <SettingsPager PageSize="7"/>
                                                        </GridViewProperties>      
                                                        <ClientSideEvents TextChanged="TransactionType_TextChanged" />                                    
                                                        <Columns>
                                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True" Width="30px"/>
                                                            <dx:GridViewDataTextColumn Caption="Transaction Name" FieldName="Name" Width="200px">
                                                                <Settings AutoFilterCondition="Contains"/>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Standard Wording" FieldName="StandardWording" Visible="false" Width="200px"/>
                                                        </Columns>
                                                    </dx:ASPxGridLookup>

                                                    <dx:ASPxLabel ID="lblTransactionType" runat="server" Visible="false" CssClass="display-inline-table"/>
                                                </td>
                                                <td valign="middle" class="td-layout-item">
                                                     <dx:ASPxLabel runat="server" ID="ltBudgeted" Text="Budget No" 
                                                        CssClass="display-inline-table"/>
                                                    <div class="right-bold">:</div>
                                                </td>   
                                                <td style="width: 125px" colspan="2" valign="middle" class="td-layout-value value-rv">                                                    
                                                    <dx:ASPxGridLookup runat="server" ID="lkpgridBudgetNo" ClientInstanceName="lkpgridBudgetNo" 
                                                        SelectionMode="Single" KeyFieldName="WbsNumber" TextFormatString="{0}" AutoPostBack="true"
                                                        Width="95%" CssClass="display-inline-table" IncrementalFilteringMode="Contains"
                                                        OnLoad="LookupBudgetNo_Load" 
                                                        OnTextChanged="LookUpBudgetNo_TextChanged">
                                                        <GridViewProperties EnableCallBacks="false">
                                                            <SettingsBehavior   AllowSelectByRowClick="true" EnableRowHotTrack="true"/>
                                                            <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"/>
                                                            <SettingsPager PageSize="7"/>
                                                        </GridViewProperties>                                 
                                                        <Columns>
                                                            <dx:GridViewCommandColumn ShowSelectCheckbox="True"/>
                                                            <dx:GridViewDataTextColumn Caption="WBS Number" FieldName="WbsNumber" Width="200px">
                                                                <Settings AutoFilterCondition="Contains"/>
                                                            </dx:GridViewDataTextColumn>
                                                            <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="350px">
                                                                <Settings AutoFilterCondition="Contains" />
                                                            </dx:GridViewDataTextColumn>
                                                        </Columns>
                                                    </dx:ASPxGridLookup>
                                                    <dx:ASPxLabel runat="server" ID="lblBudgetNo" CssClass="display-inline-table" />
                                                    <dx:ASPxHyperLink ID="lnkBudgetNo" ClientInstanceName="lnkBudgetNo" Text="more" runat="server" 
                                                                      CssClass="budgetNumber-more-link">
                                                        <ClientSideEvents Click="function(sender, evt) { pnpopMultipleBudgetNumber.Show(); }" />
                                                    </dx:ASPxHyperLink>
                                                </td>                                             
                                            </tr>
                                        </table>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                        <dx:TabPage Text="Finance Data" Name="FinanceData">
                            <ContentCollection>
                                <dx:ContentControl ID="FinanceDataCC" runat="server" Width="970px">
                                    <div class="contentsectionUnderTab">
                                        <table style="width: 60%; display: inline-table;">
                                            <tr>
                                                <td style="width: 100px;" class="td-layout-item">
                                                    Posting Date <span class="right-bold">:</span>
                                                </td>
                                                <td style="width: 150px;" class="td-layout-value value-rv">
                                                    <dx:ASPxDateEdit ID="dtPostingDate" ClientIDMode="static" runat="server" 
                                                                     Width="130px" DisplayFormatString="dd MMM yyyy" CssClass="display-inline-table"
                                                                     EditFormatString="dd/MM/yyyy" UseMaskBehavior="true" />
                                                    <dx:ASPxLabel ID="lblPostingDate" runat="server" Visible="false" CssClass="display-inline-table" />
                                                </td>          
                                                <td style="width: 150px;" class="td-layout-item">
                                                    <div class="hidden">
                                                        Planning Payment Date <span class="right-bold">:</span>
                                                    </div>
                                                </td>
                                                <td style="width: 150px;" class="td-layout-item">
                                                    <div class="hidden">
                                                        <dx:ASPxDateEdit ID="dtPlanningPaymentDate" ClientIDMode="static" runat="server" 
                                                                         DisplayFormatString="dd MMM yyyy" EditFormatString="dd/MM/yyyy" 
                                                                         UseMaskBehavior="true" Width="130px" CssClass="display-inline-table" />
                                                        <dx:ASPxLabel ID="lblPlanningPaymentDate" runat="server" 
                                                            Visible="false" CssClass="display-inline-table" />
                                                    </div>
                                                </td>                                                           
                                            </tr>
                                            <tr>
                                                <td class="td-layout-item">
                                                <div class="hidden">
                                                    Bank Type<div class="right-bold">:</div>
                                                    </div>
                                                </td>
                                                <td class="td-layout-item">       
                                                                                             
                                               
                                                  
                                                </td>                                             
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <br /><br />
                                                    <asp:LinkButton ID="lnkSAPDocNumber" runat="server" Text="SAP Document No." OnClick="evt_lnkSAPDocNumber_clicked"/>
                                                </td>
                                            </tr>                                                        
                                        </table>
                                        <table style="width: 20%; display: inline-table; float: right;">
                                            <tr>
                                                <td>
                                                    <table style="float: right">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btReverse" runat="server" Text="Reverse" 
                                                                    CssClass="xlongButton" skinID ="xlongButton"
                                                                    OnClick="evt_btReverse_onClick"
                                                                    OnClientClick="loading()"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btSimulation" runat="server" Text="Simulate" 
                                                                    CssClass="xlongButton" skinID ="xlongButton" 
                                                                    OnClick="evt_btSimulation_onClick" 
                                                                    OnClientClick="loading()"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btPostToSAP" runat="server" Text="Post to SAP" 
                                                                    CssClass="xlongButton" skinID ="xlongButton"
                                                                    OnClick="evt_btPostToSAP_onClick"
                                                                    OnClientClick="loading()"/>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </dx:ContentControl>
                            </ContentCollection>
                        </dx:TabPage>
                    </TabPages>
                </dx:ASPxPageControl>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel> 
    <asp:UpdatePanel ID="pnupdateAmountList" runat="server">
        <ContentTemplate>
            <table>
                <tr>
                    <td>
                        <div class="totalAmount">
                            <asp:Literal runat="server" ID="ltTotalAmount" />
                        </div>  
                    </td>
                </tr>
            </table>                      
        </ContentTemplate>
    </asp:UpdatePanel> 
    <div id="UploadDetailDiv" runat="server"> 
        <asp:UpdatePanel runat="server" ID="pnupdateUpload">
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUploadTemplate" />
            </Triggers>
            <ContentTemplate>
                <table>
                    <tr>
                        <td>
                            <dx:ASPxHyperLink runat="server" ID="lnkDownloadTemplate" Visible="false" Text="Download Template" NavigateUrl="~/ExcelTemplate/Doc_Detail_Template.xls" />                  
                            <asp:Label runat="server" ID="ltDownloadTemplateTextSeparator" Visible="false">&nbsp; | &nbsp;</asp:Label>
                            
                        </td>
                    </tr>
                    <tr>
                        <td>                            
                            <asp:FileUpload ID="fuInvoiceList" runat="server"/>
                            &nbsp;<asp:Button runat="server" ID="btnUploadTemplate" Text="Upload" OnClick="btnUploadTemplate_Click" />   
                        </td>
                    </tr>
                </table>      
            </ContentTemplate>
        </asp:UpdatePanel>              
    </div>
    <asp:UpdatePanel runat="server" ID="pnupdateGrid">
        <ContentTemplate>
            <dx:ASPxGridView runat="server" ID="gridPVDetail" Width="990px"                
                    ClientInstanceName="gridPVDetail"
                    ClientIDMode="Static"
                    KeyFieldName="DisplaySequenceNumber"
                    AutoGenerateColumns="false"
                    EnableCallBacks="false"
                    OnRowUpdating="evt_gridPVDetail_onRowUpdating"
                    OnSelectionChanged="evt_gridPVDetail_onSelectionChanged" 
                    OnCellEditorInitialize="evt_gridPVDetail_onCellEditorInitialize"                    
                    OnCustomCallback="evt_gridPVDetail_CustomCallback">
                 <Styles>                
                    <AlternatingRow Enabled="True" CssClass="value-rv"/>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle">
                    </Header>                    
                </Styles>
                <SettingsBehavior 
                    AllowFocusedRow="false"
                    AllowDragDrop="False" 
                    AllowSelectByRowClick="True" 
                    AllowSelectSingleRowOnly="True" 
                    AllowSort="False" 
                    ProcessSelectionChangedOnServer="True" />    
                <SettingsEditing Mode="Inline" />
                <Settings 
                    ShowHorizontalScrollBar="false" 
                    ShowVerticalScrollBar="true" 
                    VerticalScrollableHeight="145"
                    
                    UseFixedTableLayout="True" />
                <SettingsText CommandCancel=" " CommandEdit=" " CommandUpdate="  " />
                <Columns>               
                    <dx:GridViewDataColumn Width="30px" FieldName="DeletionControl">
                        <CellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle"/>
                        <EditCellStyle HorizontalAlign="Center" Cursor="pointer" VerticalAlign="Middle"/>
                        <HeaderTemplate>
                            <common:KeyImageButton ID="imgDeleteAllRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png" 
                                                   OnClick="evt_imgDeleteAllRow_clicked" CssClass="pointed"/>   
                        </HeaderTemplate>                        
                        <DataItemTemplate>
                            <common:KeyImageButton ID="imgDeleteRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/b_delete.png" 
                                                   Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgDeleteRow_clicked" CssClass="pointed"/>
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <common:KeyImageButton ID="imgAddRow" runat="server" ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png" 
                                                   Key="<%# Bind('DisplaySequenceNumber') %>" OnClick="evt_imgAddRow_clicked" CssClass="pointed"/>
                        </EditItemTemplate>
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn Width="30px" FieldName="DisplaySequenceNumber" Caption="No">
                        <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        <DataItemTemplate>
                            <dx:ASPxLabel ID="lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" />                       
                        </DataItemTemplate>      
                        <EditItemTemplate>
                            <dx:ASPxLabel ID="edit_lblSequenceNumber" runat="server" Text="<%# Bind('DisplaySequenceNumber') %>" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Cost Center">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Code" FieldName="CostCenterCode" Width="80px"> 
                                <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                                <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                               
                                <EditItemTemplate>
                                <dx:ASPxGridLookup runat="server" ID="lkpgridCostCenterCode" 
                                                       ClientInstanceName="lkpgridCostCenterCode" 
                                                       SelectionMode="Single" KeyFieldName="Code" 
                                                       TextFormatString="{0}" Width="95%"
                                                       CssClass="display-inline-table"
                                                       OnLoad="lkpgridCostCenterCode_Load"
                                                       IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000">
                                        <GridViewProperties>
                                            <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true"/>
                                            <Settings ShowFilterRow="true" ShowStatusBar="Hidden" UseFixedTableLayout="true"/>
                                            <SettingsPager PageSize="7"/>
                                        </GridViewProperties>
                                        <ClientSideEvents TextChanged="Detail_TextChanged" />
                                        <Columns>
                                            <dx:GridViewDataTextColumn Caption="Code" FieldName="Code" Width="100px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                            <dx:GridViewDataTextColumn Caption="Description" FieldName="Description" Width="400px">
                                                <Settings AutoFilterCondition="Contains"/>
                                            </dx:GridViewDataTextColumn>
                                        </Columns>
                                    </dx:ASPxGridLookup>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="Name" FieldName="CostCenterName" Width="120px">
                                <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Wrap="true"/>
                                <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" 
                                    Paddings-PaddingLeft="5px" Wrap="true">
                                    <Paddings PaddingLeft="5px" />
                                </EditCellStyle>
                                <DataItemTemplate>
                                    <dx:ASPxLabel ID="lblCostCenterName" runat="server" 
                                                  Text="<%# Bind('CostCenterName') %>" Width="110px"
                                                  OnLoad="evt_lblCostCenterName_onLoad" CssClass="pvform-costCenterName"/>                                    
                                </DataItemTemplate>
                                <EditItemTemplate>
                                    <dx:ASPxLabel ID="tboxCostCenterName" runat="server" ClientInstanceName="tboxCostCenterName"
                                                  Text="<%# Bind('CostCenterName') %>" Width="110px"
                                                  OnLoad="evt_lblCostCenterName_onLoad" CssClass="pvform-costCenterName"/>                                   
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn Caption="Description" Width="250px" FieldName="Description"> 
                        <CellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="True"/>
                        <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Left" Wrap="True"/>
                        <DataItemTemplate>
                            <dx:ASPxLabel ID="lblStandardDescriptionWording" runat="server" Text="<%# Bind('StandardDescriptionWording') %>" 
                                          Width="90px" CssClass="display-inline-table pvform-standardWording"/> &nbsp;&nbsp;
                            <dx:ASPxLabel ID="lblDescription" runat="server" Text="<%# Bind('Description') %>" 
                                          CssClass="display-inline-table"/>
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <table>
                                <tr>
                                    <td valign="middle" align="left">
                                        <dx:ASPxLabel ID="edit_lblDescription" runat="server" Text="<%# Bind('StandardDescriptionWording') %>" 
                                        Width="150px" CssClass="pvform-standardWording"/>
                                    </td>
                                    <td valign="middle" align="right">
                                        <dx:ASPxLabel ID="lblSpace0" runat="server" Text="  " CssClass="display-inline-table"/>
                                        <dx:ASPxTextBox ID="tboxDescription" 
                                                        Width="180px"
                                                        CssClass="display-inline-table"
                                                        ClientInstanceName="tboxDescription"
                                                        runat="server" Text="<%# Bind('Description') %>"
                                                        OnLoad="evt_tboxDescription_onLoad">
                                        <ClientSideEvents GotFocus="function(s,e) { s.SelectAll(); }" />
                                        </dx:ASPxTextBox>
                                    </td>
                                </tr>
                            </table>                          
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                     <dx:GridViewBandColumn Caption="Amount">
                        <Columns>                             
                            <dx:GridViewDataTextColumn Caption="Curr" FieldName="CurrencyCode" Width="60px">
                               <CellStyle VerticalAlign="Middle" HorizontalAlign="Center"/>
                                <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Center"> 
                                </EditCellStyle>
                                <DataItemTemplate>
                                <asp:Literal ID="litCurrencyCode" runat="server" Text="<%# Bind('CurrencyCode') %>" />
                                </DataItemTemplate>
                                <EditItemTemplate>
                                    <dx:ASPxComboBox runat="server" Width="99%"
                                        ClientInstanceName="coCurrencyCode" ID="coCurrencyCode"  
                                        OnLoad="coCurrencyCode_Load"  
                                        DropDownStyle="DropDown"                                        
                                        TextField="Code" ValueField="Code" 
                                        DropDownWidth="99%" 
                                         TextFormatString="{0}">                                        
                                        <ClientSideEvents TextChanged="CurrencyCode_TextChanged"/>
                                    </dx:ASPxComboBox>
                                </EditItemTemplate>
                            </dx:GridViewDataTextColumn>                                     
                            <dx:GridViewDataTextColumn Caption="Value" FieldName="Amount" Width="90px"> 
                                <CellStyle VerticalAlign="Middle" HorizontalAlign="Right" 
                                           Paddings-PaddingLeft="5px" Paddings-PaddingRight="10px">
                                    <Paddings PaddingLeft="5px" PaddingRight="10px" />
                                </CellStyle>
                                <EditCellStyle VerticalAlign="Middle" HorizontalAlign="Right" 
                                               Paddings-PaddingLeft="5px" Paddings-PaddingRight="10px">                                 
                                    <Paddings PaddingLeft="5px" PaddingRight="10px" />
                                </EditCellStyle>
                                <PropertiesTextEdit />
                                <DataItemTemplate>
                                    <dx:ASPxLabel ID="lblAmount" 
                                                  runat="server" 
                                                  Text="<%# Bind('AmountDisplay') %>" 
                                                  Width="80px" 
                                                  HorizontalAlign="Right"/>
                                </DataItemTemplate> 
                                <EditItemTemplate>
                                    <common:TabBluredTextBox ID="tboxAmount" runat="server"
                                        ClientInstanceName ="tboxAmount"
                                        Text="<%# Bind('Amount') %>"
                                        HorizontalAlign="Right" Width="110px"
                                        OnLoad="evt_tboxAmount_onLoad"
                                        OnTabKeypress="evt_tboxAmount_TabKeypress">
                                        <ClientSideEvents GotFocus="Amount_Focused"/>
                                        <MaskSettings  Mask="<0..9999999999999g>.<0..99>" IncludeLiterals="DecimalSymbol"/>
                                    </common:TabBluredTextBox>
                                </EditItemTemplate> 
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    <dx:GridViewDataTextColumn Caption="Invoice No" FieldName="InvoiceNumber" Width="100px" ReadOnly="true" Visible="false">
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
                        <EditCellStyle HorizontalAlign="Center" VerticalAlign="Middle"/>
<%--                        <DataItemTemplate>
                              <common:BlankTargetedHyperlink ID="lblInvoiceNumber" runat="server" 
                                                           Text="<%# Bind('InvoiceNumber') %>" 
                                                           NavigateUrl="<%# Bind('InvoiceNumberUrl') %>" 
                                                           Width="80px"/>
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <dx:ASPxHyperLink ID="edit_lblInvoiceNumber" runat="server" 
                                Text="<%# Bind('InvoiceNumber') %>" 
                                NavigateUrl="<%# Bind('InvoiceNumberUrl') %>" 
                                Target="_blank" Width="80px"/>
                        </EditItemTemplate>  
--%>                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Tax Code" FieldName="TaxCode" Width="60px"  HeaderStyle-Wrap="True" ReadOnly="true">
                        <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" 
                                    Paddings-PaddingLeft="5px" Wrap="true">
                               
                                </EditCellStyle>
                               <EditItemTemplate>
                                 <div class="cellbox">
                                    <asp:Literal ID="litTaxCode" runat="server" Text="<%# Bind('TaxCode') %>" />
                                 </div>
                               </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Tax No" FieldName="TaxNumber" Width="100px" ReadOnly="true" Visible="false">
                    <CellStyle HorizontalAlign="Left" VerticalAlign="Middle" Paddings-PaddingLeft="5px" Paddings-PaddingRight="5px"/>
                     <EditCellStyle HorizontalAlign="Left" VerticalAlign="Middle" 
                                    Paddings-PaddingLeft="5px" Wrap="true">
                               
                                </EditCellStyle>
                                <EditItemTemplate><div class="cellbox"><asp:Literal ID="litTaxNumber" runat="server" Text="<%# Bind('TaxNumber') %>" /></div></EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                
            </dx:ASPxGridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!-- Footer -->    
    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed; width: 980px;">
        <tr>
            <td align="left">
                 <div class="display-inline-table">
                    <asp:UpdatePanel runat="server" ID="pnpudateLeftFooterButtons">
                        <ContentTemplate>                       
                            <asp:Button ID="btSubmit" runat="server" Text="Submit" Visible="false" 
                                            OnClick="btSubmit_Click" OnClientClick="loading()"/>
                            <asp:Button ID="btReject" runat="server" Text="Reject" 
                                        OnClick="evt_btReject_onClick"/>
                            <asp:Button ID="btHold" runat="server" Text="Hold" 
                                        OnClick="evt_btHold_onClick" OnClientClick="loading()"/>
                            <asp:Button ID="btUnHold" runat="server" Text="Un-Hold" 
                                        OnClick="evt_btUnHold_onClick" OnClientClick="loading()"/>
                            <asp:Button ID="btSummary" runat="server" Text="Generate Summary" CssClass="longButton" SkinID="xlongButton"
                                        OnClick="evt_btSummary_onClick" OnClientClick="loading()" />
                        </ContentTemplate>
                    </asp:UpdatePanel>                
                </div>         
            </td>
            <td></td>
            <td align="right">
                <div class="display-inline-table">
                    <asp:UpdatePanel runat="server" ID="pnpudateRightFooterButtons">
                        <ContentTemplate>
                            <asp:Button ID="btEdit" runat="server" Text="Edit"
                                        OnClick="btEdit_Click" ClientIDMode="Static"/>
                            <asp:Button ID="btSave" runat="server" Text="Save Draft" 
                                        OnClick="btSave_Click" ClientIDMode="Static" OnClientClick="loading();"
                                        CssClass="xlongButton" SkinID="xlongButton"/>
                            <asp:Button ID="btCancel" runat="server" Text="Cancel"
                                        OnClick="btCancel_Click"/>				
                            <asp:Button ID="btClose" runat="server" Text="Close" UseSubmitBehavior="false" 
                                        OnClientClick="closeWin()" OnClick="btClose_Click"/>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>     
            </td>
        </tr>
        <tr style="height: 170px;">
            <td align="left" style="width: 100%">
                <asp:panel runat="server" ID="pnAttachment" GroupingText="Attachment" Height="200px">                  
                    <table>
                        <tr>
                            <td>                      
                                <asp:UpdatePanel runat="server" ID="pnupdateAttachmentGrid">
                                    <ContentTemplate>

                                        <div id="EntertainmentAttachDiv" runat="server" visible="false">
                                            <a href="../Template/Excel/Template_Entertainment_Sheet.xls">Download Entertainment Template</a>
                                        </div>

                                        <dx:ASPxGridView runat="server" ID="gridAttachment" Width="300px"
                                            ClientInstanceName="gridAttachment"
                                            KeyFieldName="SequenceNumber"
                                            EnableCallBacks="false"
                                            AutoGenerateColumns="false"                    
                                            SettingsBehavior-AllowSort="false"
                                            SettingsBehavior-AllowSelectByRowClick="false"
                                            SettingsBehavior-AllowSelectSingleRowOnly="false"
                                            SettingsBehavior-AllowFocusedRow="false"
                                            SettingsBehavior-AllowDragDrop="False"
                                            OnHtmlRowCreated="evt_gridAttachment_onHtmlRowCreated">
                                            <Styles>
                                                <AlternatingRow Enabled="True" />
                                            </Styles>
                                            <Columns>   
                                                <dx:GridViewDataTextColumn FieldName="SequenceNumber" Caption="No" Width="30px">
                                                    <CellStyle HorizontalAlign="Center" />
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblAttachmentNumber" Text="<%# Bind('SequenceNumberInString') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="Category">
                                                    <DataItemTemplate>
                                                        <dx:ASPxLabel runat="server" ID="lblAttachmentCategory" Text="<%# Bind('CategoryName') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataTextColumn Caption="File Name">
                                                    <CellStyle HorizontalAlign="Left" />
                                                    <DataItemTemplate>
                                                         <common:BlankTargetedHyperlink runat="server" ID="lblAttachmentFileName" Text="<%# Bind('FileName') %>" 
                                                        NavigateUrl="<%# Bind('Url') %>" />
                                                    </DataItemTemplate>
                                                </dx:GridViewDataTextColumn>
                                                <dx:GridViewDataColumn FieldName="#">
                                                    <CellStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <DataItemTemplate>
                                                        <common:KeyImageButton runat="server" ID="imgAddAttachment" 
                                                                              Key="<%# Bind('SequenceNumber') %>" 
                                                                              CssClass="pointed"
                                                                              ImageUrl="~/App_Themes/BMS_Theme/Images/green-plus.png"
                                                                              OnClick="evt_imgAddAttachment_onClick">                                  
                                                        </common:KeyImageButton>
                                                        <common:KeyImageButton runat="server" ID="imgDeleteAttachment" 
                                                                              Key="<%# Bind('SequenceNumber') %>" 
                                                                              CssClass="pointed"
                                                                              ImageUrl="~/App_Themes/BMS_Theme/Images/red-cross.png"
                                                                              OnClick="evt_imgDeleteAttachment_onClick"/>  
                                                    </DataItemTemplate>
                                                </dx:GridViewDataColumn>
                                            </Columns>
                                            <SettingsPager PageSize="10" Mode="ShowAllRecords" AlwaysShowPager="false"/>
                                        </dx:ASPxGridView> 
                                    </ContentTemplate>
                                </asp:UpdatePanel>    
                            </td>
                        </tr>
                    </table>                            
                </asp:panel>
            </td>
            <td align="center">
                <asp:Panel runat="server" ID="pnNotice" GroupingText="Notice" Width="250px">
                    <asp:UpdatePanel runat="server" ID="upnlNotice">
                        <ContentTemplate>
                            <dx:ASPxPanel runat="server" ID="pnNotes">
                                <PanelCollection>
                                    <dx:PanelContent>
                                        <div style="display:inline; float:left; width:100%; border:1px solid grey; background-color:#f6f6f6; padding-top: 10px;">            
                                            <div style="padding-left:10px;">
                                                <div style="width:68%; float:left">
                                                    <table>
                                                        <tr>
                                                            <td style="width:7px;vertical-align:middle;">
                                                                To&nbsp;:&nbsp;&nbsp;
                                                            </td>
                                                            <td>
                                                                <dx:ASPxComboBox ID="ddlSendTo" runat="server"
                                                                        EnableCallbackMode="true"
                                                                        IncrementalFilteringMode="StartsWith" DropDownStyle="DropDown"
                                                                        ValueType="System.String" ValueField="Username" 
                                                                        DropDownButton-Visible="true" AllowMouseWheel="true"
                                                                        OnLoad="evt_ddlSendTo_onLoad" 
                                                                        width="260px" CallbackPageSize="10" TextFormatString="{0}"
                                                                        AutoPostBack="false" MaxLength="61">
                                                                    <Columns>
                                                                        <dx:ListBoxColumn FieldName="FULLNAME" Caption="Name"/>
                                                                        <dx:ListBoxColumn FieldName="Username"  Visible="false" />
                                                                    </Columns>
                                                                </dx:ASPxComboBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="rowright" style="vertical-align:top !important; float:left; width:100%; border-top:1px dashed grey; margin: 5px 0px 5px 1px">
                                                <table>
                                                    <tr> 
                                                        <td valign="middle">
                                                            <asp:TextBox Enabled="true" runat="server" ID="txtNotice" style="width:300px; margin: 5px 5px 5px 5px; height: 60px;" TextMode="MultiLine"/>
                                                        </td>
                                                        <td valign="middle">
                                                            <dx:ASPxButton runat="server" ID="btSendNotice" Text="Send Notice"
                                                                           Height="60px" OnClick="evt_btSendNotice_onClick"/>
                                                        </td>
                                                    </tr>
                                                </table>                                    
                                            </div>
                                            <asp:Literal runat="server" ID="litNoticeMessage"></asp:Literal>
                                        </div>							            
                                    </dx:PanelContent>
                                </PanelCollection>
                            </dx:ASPxPanel>
                            <div id="noticeWrap">			
                                <div id="noticeComment" runat="server" clientidmode="Static">
                                    <asp:Repeater runat="server" ID="rptrNotice" 
                                                    OnItemDataBound="evt_rptrNotice_onItemDataBound" >
                                        <ItemTemplate>
                                            <asp:Literal runat="server" ID="litOpenDivNotice"></asp:Literal>
                                            <asp:Literal runat="server" ID="litRptrNoticeComment"></asp:Literal>
                                            <asp:Literal runat="server" ID="litCloseDivNotice"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:Repeater>   
                                </div>
                            </div>				
                        </ContentTemplate>
                    </asp:UpdatePanel>                 
                </asp:Panel>
            </td>
             <td>
                <fh:FormHint runat="server" ID="FormHint1" />               
            </td>
        </tr>         
    </table>        

    <!-- File Upload -->
    <asp:HiddenField ID="popUploadAttachment" runat="server"/>
    <ajaxToolkit:ModalPopupExtender ID="popdlgUploadAttachment" BehaviorID="popdlgUploadAttachment" 
                                    runat="server" DropShadow="true" 
                                    TargetControlID="popUploadAttachment" PopupControlID="pnpopUploadAttachment"
                                    BackgroundCssClass="modalBackground"/>
    <center>
        <asp:Panel runat="server" ID="pnpopUploadAttachment" CssClass="speakerPopupList" ClientIDMode="Static">
            <asp:UpdatePanel runat="server" ID="pnupdateFileUploadPopup">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btSendUploadAttachment" />
                </Triggers>
                <ContentTemplate>
                <div class="uploaddiv">
                    <div class="row">
                        <label>Category</label>
                        <div class="uploadr">
                            <dx:ASPxComboBox runat="server" ID="cboxAttachmentCategory" AutoPostBack="false"
                                ClientInstanceName="cboxAttachmentCategory" ClientIDMode="Static"
                                TextField="Description" ValueField="Code" ValueType="System.String"
                                OnLoad="evt_cboxAttachmentCategory_onLoad"/>                   
                        </div>
                    </div>
                    <div class="row">
                        <label>File</label>
                        <div class="uploadr">
                            <asp:FileUpload runat="server" ID="uploadAttachment" ClientIDMode="Static"/>
                        </div>
                    </div>    
                    <div class="rowbtn">
                        <div style="width:1px;height:30px;clear:both;">&nbsp</div>
                        <div class="btnRightLong">
                            <asp:Button ID="btSendUploadAttachment" runat="server" Text="Upload" 
                                OnClick="evt_btSendUploadAttachment_clicked"
                                OnClientClick="return UploadAttachment_Click()"
                                />
                            <asp:Button ID="btCloseUploadAttachment" runat="server" Text="Close" OnClick="evt_btCloseUploadAttachment_clicked"/>
                       </div>
                    </div>
                </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

    <!-- Multiple BudgetNumber Popup -->
    <dx:ASPxPopupControl runat="server" ID="pnpopMultipleBudgetNumber" ClientInstanceName="pnpopMultipleBudgetNumber"
                         Modal="true" PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" HeaderText="">
        <ContentCollection>
            <dx:PopupControlContentControl>
                <asp:Panel runat="server" ID="Panel1" CssClass="speakerPopupList">
                    <asp:UpdatePanel runat="server" ID="pnupdateMultipleBudgetNumber">
                        <ContentTemplate>
                            <dx:ASPxListBox runat="server" Width="250px" 
                                ID="lstboxMultipleBudgetNumber" 
                                ClientInstanceName="lstboxMultipleBudgetNumber">
                                <Columns>
                                    <dx:ListBoxColumn Caption="WBS Numbers" FieldName="WbsNumber" />
                                </Columns>
                            </dx:ASPxListBox>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </asp:Panel>
            </dx:PopupControlContentControl>
        </ContentCollection>
    </dx:ASPxPopupControl>

    <!-- SAP Document Number Popup -->
    <asp:HiddenField ID="popSAPDocNumberTarget" runat="server"/>
    <ajaxToolkit:ModalPopupExtender ID="popSAPDocNumber" runat="server" DropShadow="true" 
                                    TargetControlID="popSAPDocNumberTarget" PopupControlID="pnpopSAPDocNumber"
                                    BackgroundCssClass="modalBackground"/>
    <center>
        <asp:Panel runat="server" ID="pnpopSAPDocNumber" CssClass="speakerPopupList">
            <asp:UpdatePanel runat="server" ID="pnupdateSAPDocNumber">
                <ContentTemplate>
                    <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="700px">                        
                        <tr>
                            <td colspan="3">
                                <br />
                                <dx:ASPxGridView ID="gridpopSAPDocNumber" runat="server" Width="700px" Visible="true" 
                                                 AutoGenerateColumns="false" KeyFieldName="InvoiceNumber">
                                    <SettingsPager Mode="ShowAllRecords"/>
                                    <SettingsBehavior AllowSelectByRowClick="true" AllowSelectSingleRowOnly="true"/>
                                    <Columns>  
                                        <dx:GridViewDataTextColumn Caption="Currency Code" FieldName="CurrencyCode" Width="50px">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <DataItemTemplate>
                                                <asp:Label ID="lblCurrencyCode" runat="server" Text="<%# Bind('CurrencyCode') %>"/>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn> 
                                        <dx:GridViewDataTextColumn Caption="Invoice No." FieldName="InvoiceNumber" Width="100px">
                                            <DataItemTemplate>
                                                <asp:Label ID="lblSAPInvoiceNumber" runat="server" Text="<%# Bind('InvoiceNumber') %>"/>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn> 
                                        <dx:GridViewDataTextColumn Caption="SAP Document No." FieldName="SAPDocNumber" Width="110px">
                                            <DataItemTemplate>
                                                <asp:Label ID="lblSAPDocNumber" runat="server" Text="<%# Bind('SAPDocNumber') %>"/>
                                            </DataItemTemplate>
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn>
                                        <dx:GridViewDataTextColumn Caption="SAP Clearing Doc. No." FieldName="ClearingDocNumber" Width="120px">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <DataItemTemplate>
                                                <asp:Label ID="lblClearingDocNumber" runat="server" Text="<%# Bind('ClearingDocNumber') %>"/>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn>                
                                        
                                        <dx:GridViewDataTextColumn Caption="Pay Prop Doc. No." FieldName="PayPropDocNo" Width="100px">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <DataItemTemplate>
                                                <asp:Label ID="lblPayPropDocNo" runat="server" Text="<%# Bind('PayPropDocNo') %>"/>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn>    
                                        <dx:GridViewDataTextColumn Caption="Pay Prop ID" FieldName="PayPropId" Width="70px">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <DataItemTemplate>
                                                <asp:Label ID="lblPayPropId" runat="server" Text="<%# Bind('PayPropId') %>"/>
                                            </DataItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <CellStyle HorizontalAlign="Center"/>
                                        </dx:GridViewDataTextColumn>  
                                                            
                                    </Columns>
                                    <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                    <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                        VerticalScrollableHeight="200" VerticalScrollBarStyle="Standard" />
                                    <Styles>
                                        <Header HorizontalAlign="Center" VerticalAlign="Middle"/>
                                    </Styles>
                                </dx:ASPxGridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" colspan="3">
                                <br />
                                <asp:Button runat="server" ID="btSAPDocPopupClose" Text="Close" OnClick="evt_btSAPDocPopupClose_clicked" />
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </asp:Panel>
    </center>

     <!-- Added by Akhmad Nuryanto -->
    <asp:Button ID="hidBtnDetail" runat="server" style="display:none;" />
    <ajaxToolkit:ModalPopupExtender ID="popUpSimulation" runat="server"
        TargetControlID="hidBtnDetail"
        PopupControlID="panelSimulation"
        CancelControlID="btnCloseSimulation"
        BackgroundCssClass="modalBackground" />
    <center>
        <asp:Panel runat="server" id="panelSimulation" CssClass="speakerPopupList">
        <asp:UpdatePanel runat="server" ID="upnModal">       
        <ContentTemplate>
            <asp:Literal runat="server" ID="LitPopUpModal"></asp:Literal> 
            <table cellpadding="2px" cellspacing="0" border="0" style="text-align:left" width="900px">
                <tr>
                    <td>
                        <dx:ASPxGridView ID="gridSimulation" runat="server" Width="100%" ClientInstanceName="gridSimulation"
                            KeyFieldName="KEYS" AutoGenerateColumns="False">
                            <Columns>
                                <dx:GridViewDataColumn Caption="Simulation" FieldName="KEYS" VisibleIndex="0">
                                </dx:GridViewDataColumn>
                            </Columns>
                            <SettingsPager AlwaysShowPager="false" Mode="ShowAllRecords">
                            </SettingsPager>
                            <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                            <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                VerticalScrollableHeight="470" VerticalScrollBarStyle="Standard"  />
                            <Styles>
                                <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                </Header>
                                <Cell HorizontalAlign="Center" Border-BorderStyle="None">
                                    <Border BorderStyle="None" />
                                </Cell>
                            </Styles>
                            <Templates>
                                <DetailRow>
                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align:left" width="870px">
                                        <colgroup>
                                            <col width="120px" />
                                            <col width="100px" />
                                            <col width="120px" />
                                            <col width="100px" />
                                            <col width="120px" />
                                            <col width="100px" />
                                        <//colgroup>
                                        <tr>
                                            <td>Transaction Type:</td>
                                            <td><%# Eval("TRANSACTION_NAME")%></td>
                                            <td>Company Code: </td>
                                            <td><%# Eval("COMPANY_CODE")%></td>
                                            <td>Fiscal Year: </td>
                                            <td><%# Eval("FISCAL_YEAR")%></td>
                                        </tr>
                                        <tr>
                                            <td>Doc. Date: </td>
                                            <td><%# string.Format("{0:dd/MM/yyyy}", Eval("PV_DATE")) %></td>
                                            <td>Posting Date: </td>
                                            <td><%# string.Format("{0:dd/MM/yyyy}", Eval("POSTING_DATE")) %></td>
                                            <td>Period: </td>
                                            <td><%# Eval("PERIOD")%></td>
                                        </tr>
                                        <tr>
                                            <td>Ref. Doc.: </td>
                                            <td><%# Eval("REF_DOC")%></td>
                                            <td>Witholding Tax: </td>
                                            <td><%# Eval("WITHOLDING_TAX")%></td>
                                        </tr>
                                        <tr>
                                            <td>Doc. Currency: </td>
                                            <td><%# Eval("CURRENCY_CODE")%></td>
                                            <td colspan="4" style='<%# Eval("WARNING")==null ? "" : "font-weight: bold; background-color:Yellow"%>' ><%# Eval("WARNING")%></td>
                                        </tr>
                                    </table>
                                    <br />
                                    <dx:ASPxGridView ID="gridPopUpSimulation" runat="server" Width="835px" 
                                        KeyFieldName ="SEQ_NO" 
                                        OnBeforePerformDataSelect="gridPopUpSimulation_DataSelect"
                                        AutoGenerateColumns="false" ClientInstanceName="gridPopUpSimulation"
                                        OnCustomColumnDisplayText="grid_CustomColumnDisplayText"> 
                                        <SettingsPager Mode="ShowAllRecords" AlwaysShowPager="false">
                                        </SettingsPager>
                                        
                                        <Columns>                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="0" Width="40px" FieldName="SEQ_NO" Caption="Itm" />
                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="1" FieldName="ACCOUNT" Caption ="Account" Width="110px"/>
                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="2" FieldName="ACCOUNT_SHORT" Caption ="Account short text" />
                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="3" FieldName="TX" Caption ="Tx" Width="50px" 
                                                CellStyle-HorizontalAlign="Center"/>
                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="4" FieldName="AMOUNT" Caption="Amount" Width="110px"
                                                CellStyle-HorizontalAlign="Right" />
                                            
                                            <dx:GridViewDataTextColumn VisibleIndex="5" FieldName="TEXT" Caption ="Text"/>
                                            
                                        </Columns>
                                        <SettingsBehavior AllowDragDrop="False" AllowGroup="False" AllowSort="False" />
                                        <Settings ShowStatusBar="Hidden" UseFixedTableLayout="True" 
                                                ShowVerticalScrollBar="True" VerticalScrollableHeight="200" 
                                                VerticalScrollBarStyle="Standard" />
                                        <Styles>
                                            <Header HorizontalAlign="Center" VerticalAlign="Middle">
                                            </Header>
                                        </Styles>
                                    </dx:ASPxGridView>
                                </DetailRow>
                            </Templates>
                            <SettingsDetail ShowDetailRow="true" />
                            <Settings ShowGroupPanel="false" />
                            <SettingsCustomizationWindow Enabled="True" />
                        </dx:ASPxGridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:right" colspan="3">
                    <br />
                        <asp:Button runat="server" ID="btnCloseSimulation" text="Close" onclick="btnCloseSimulation_Click"/>
                    </td>
                </tr>
            </table>      
        </ContentTemplate>
        </asp:UpdatePanel>
        </asp:Panel>
    </center>


<!-- Vendor addition popup -->
<dx:ASPxPopupControl 
    id="popAddVendor" ClientInstanceName="popAddVendor" runat="server" Modal="true"
    PopupHorizontalAlign="WindowCenter" PopupVerticalAlign="WindowCenter" Width="300px"
    EnableAnimation="false" HeaderText="Add New Vendor" AllowDragging="false" AllowResize="false">
<ContentCollection>
    <dx:PopupControlContentControl ID="popAddVendorContent" runat="server">
        <table>
            <tr>
                <td style="margin-right: 10px;">Employee Name</td>
                <td>
                    <dx:ASPxTextBox runat="server" ID="tboxNewVendorName" MaxLength="40"/>
                </td>
            </tr>
            <tr>
                <td style="margin-right: 10px;">&nbsp;Reg No</td>
                <td>
                    <dx:ASPxTextBox runat="server" ID="tboxNewVendorSearchTerm" MaxLength="7" Size="7" />
                </td>
            </tr>
        </table>
        <br />
        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btSubmitNewVendor" 
                        Text="Submit" OnClick="evt_btSubmitNewVendor_Click">
            <ClientSideEvents Click="function(s,e) { popAddVendor.Hide(); loading(); }" />
        </dx:ASPxButton>
        <dx:ASPxButton runat="server" CssClass="display-inline-table" ID="btCancelNewVendorAddition" Text="Cancel">
            <ClientSideEvents Click="function(s,e) { popAddVendor.Hide(); }" />
        </dx:ASPxButton>
    </dx:PopupControlContentControl>
</ContentCollection>
</dx:ASPxPopupControl>

    
</asp:Content>