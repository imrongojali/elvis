﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Reflection;
using System.Diagnostics;
using Common;
using Common.Data;
using BusinessLogic;

namespace ELVISDashBoard
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            SystemSettingLogic _set = new SystemSettingLogic();

            string rc = _set.GetText("INVOICE_UPLOAD", "NO_FRACTION_CURRENCY");
            if (string.IsNullOrEmpty(rc))
            {
                AppSetting.RoundCurrency = new string[] { "IDR", "JPY" };
            }
            else
            {
                AppSetting.RoundCurrency = rc.Split(',');
            }

            LoggingLogic.say("_App", "Start");

            Ticker.In("App {0}", "start");
              
        }
        void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 60;
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            string errorInfo = "";
            HttpApplication h = sender as HttpApplication;
            if (h != null)
            {
                HttpRequest r = (sender as HttpApplication).Context.Request;
                if (r!=null) 
                    errorInfo =  string.Format("\r\n\tRequest\r\n\tRawUrl: {0} \r\n\tUrl: {1} \r\n\tUrlReferrer: {2}\r\n", r.RawUrl, r.Url, r.UrlReferrer);
            }
            LoggingLogic.say("_App", LoggingLogic.Trace(Server.GetLastError()) + errorInfo);
        }


        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            LogicFactory logic = LogicFactory.Be;
            if (logic != null)
            {
                logic.Dispose();                
            }
        }

        protected void Application_End(object sender, EventArgs e)
        {
            HttpRuntime r = typeof(HttpRuntime).InvokeMember("_theRuntime", 
                   BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.GetField, 
                   null, null, null) as HttpRuntime;

            if (r == null)
                return;

            string shutDownMessage = (string)r.GetType()
                .InvokeMember("_shutDownMessage",
                    BindingFlags.NonPublic| BindingFlags.Instance| BindingFlags.GetField,
                    null,r,null);

            string shutDownStack = (string)r.GetType().InvokeMember("_shutDownStack",
                    BindingFlags.NonPublic| BindingFlags.Instance| BindingFlags.GetField,
                    null,r,null);

            LoggingLogic.say("_App", "\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}",
                                         shutDownMessage, shutDownStack);
            Ticker.Out("App {0}", "stop");
            //if (!EventLog.SourceExists(".NET Runtime"))
            //{
            //    EventLog.CreateEventSource(".NET Runtime", "Application");
            //}

            //EventLog log = new EventLog();
            //log.Source = ".NET Runtime";
            //log.WriteEntry(String.Format("\r\n\r\n_shutDownMessage={0}\r\n\r\n_shutDownStack={1}",
            //                             shutDownMessage,
            //                             shutDownStack),
            //                             EventLogEntryType.Error);


        }
            
            
        
    }
}