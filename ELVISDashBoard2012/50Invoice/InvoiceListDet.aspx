﻿<%@ Page Title="Invoice List Detail" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="InvoiceListDet.aspx.cs" Inherits="ELVISDashBoard._50Invoice.InvoiceListDet" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .largeFont
        {
            font-size: x-large !important;
        }
        .style1
        {
            height: 40px;
        }
        .style2
        {
            height: 80px;
        }
        .style3
        {
            height: 23px;
        }
        .style4
        {
            height: 15px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="bodyContent" style="width: 100%; height: auto;">
        <asp:UpdatePanel runat="server" ID="upnLitMessage">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
                <asp:HiddenField runat="server" ID="hidPageMode" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField runat="server" ID="hidPageTitle" Value="Invoice List Detail" />
        <asp:HiddenField runat="server" ID="hidScreenID" />
        <asp:HiddenField runat="server" ID="hidInvoiceNo" />
        <asp:HiddenField runat="server" ID="hidInvoiceDate" />
        <asp:HiddenField runat="server" ID="hidVendorCode" />
        <asp:HiddenField runat="server" ID="hidVendorName" />
        <asp:HiddenField runat="server" ID="hidDivision" />
        <asp:HiddenField runat="server" ID="hidStatus" />
        <asp:HiddenField runat="server" ID="hidPvNo" />
        <asp:HiddenField runat="server" ID="hidPvYear" />
        <asp:HiddenField runat="server" ID="hidTransactionType" />
        <asp:UpdatePanel runat="server" ID="upnContentSection">
            <Triggers>
            </Triggers>
            <ContentTemplate>
                <div class="contentsection" style="width: 967px !important;">
                    <table border="0" cellpadding="3" cellspacing="1" style="table-layout: fixed;">
                        <tr>
                            </td>
                            <td valign="baseline" style="width: 108px">
                                <asp:Label ID="lblInvoiceNoLabel" runat="server">Invoice No.</asp:Label>
                            </td>
                            <td valign="top" style="width: 105px; background-color: #EEEEEE;">
                                <asp:Literal ID="litInvoiceNo" runat="server"></asp:Literal>
                            </td>
                            <td style="width: 80px">
                            <td valign="baseline" style="width: 108px">
                                <asp:Label ID="lblInvoiceDateLabel" runat="server">Invoice Date</asp:Label>
                            </td>
                            <td valign="top" style="width: 130px; background-color: #EEEEEE;">
                                <asp:Literal ID="litInvoiceDate" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline">
                                <asp:Label ID="lblVendorCodeLabel" runat="server">Vendor Code</asp:Label>
                            </td>
                            <td valign="top" style="background-color: #EEEEEE;" width="200px">
                                <asp:Literal ID="litVendorCode" runat="server"></asp:Literal>
                            </td>
                            <td style="width: 80px">
                            </td>
                            <td valign="baseline">
                                <asp:Label ID="lblAssignDivisionLabel" runat="server">Issuing Division</asp:Label>
                            </td>
                            <td valign="top" style="background-color: #EEEEEE;">
                                <asp:Literal ID="litAssignDivision" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline">
                                <asp:Label ID="lblVendorNameLabel" runat="server">Vendor Name</asp:Label>
                            </td>
                            <td valign="top" style="background-color: #EEEEEE;">
                                <asp:Literal ID="litVendorName" runat="server"></asp:Literal>
                            </td>
                            <td style="width: 80px">
                            </td>
                            <td valign="baseline">
                                <asp:Label ID="lblInvoiceStatusLabel" runat="server">Invoice Status</asp:Label>
                            </td>
                            <td valign="top" style="background-color: #EEEEEE;" width="200px">
                                <asp:Literal ID="litInvoiceStatus" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <tr>
                            <td valign="baseline">
                                <asp:Label ID="Label1" runat="server">Transaction Type</asp:Label>
                            </td>
                            <td valign="top" style="background-color: #EEEEEE;">
                                <asp:Literal ID="lblTransactionType" runat="server"></asp:Literal>
                            </td>
                            <td style="width: 80px">
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <table>
            <tr>
                <td>
                    <center>
                        <asp:Panel runat="server" ID="panelModal" CssClass="speakerPopupList">
                            <asp:UpdatePanel runat="server" ID="upnModal">
                                <ContentTemplate>
                                    <table cellpadding="1px" cellspacing="0" border="0" style="text-align: left" width="100%">
                                        <tr>
                                            <td class="style1">
                                                <table width="100%">
                                                    <tr>
                                                        <td class="style4">
                                                            Total Amount:
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="style3">
                                                            <asp:Literal ID="litTotalAmount" runat="server"></asp:Literal>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" class="style2">
                                                <dx:ASPxGridView ID="gridInvoiceDetailList" runat="server" AutoGenerateColumns="False"
                                                    Width="100%" ClientInstanceName="gridInvoiceDetailList" OnHtmlRowCreated="gridInvoiceDetailList_HtmlRowCreated"
                                                    KeyFieldName="SEQ_NO" Style="text-align: center" 
                                                    Styles-AlternatingRow-CssClass="even">
                                                    <Columns>
                                                        <dx:GridViewDataTextColumn Caption="No" VisibleIndex="0" Width="30px">
                                                            <DataItemTemplate>
                                                                <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                                                            </DataItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Center" VerticalAlign="Middle">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Item Transaction" FieldName="ITEM_TRANSACTION"
                                                            VisibleIndex="1" Width="150px" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Item Description" 
                                                            FieldName="ITEM_DESCRIPTION" ReadOnly="True"
                                                            VisibleIndex="2" Width="250px">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            <CellStyle HorizontalAlign="Left" VerticalAlign="Middle">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewBandColumn Caption="Amount" VisibleIndex="3">
                                                            <Columns>
                                                                <dx:GridViewDataTextColumn Caption="Curr" FieldName="CURRENCY_CD" ReadOnly="True"
                                                                    VisibleIndex="0" Width="50px" CellStyle-HorizontalAlign="Center">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <CellStyle HorizontalAlign="Left">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                                <dx:GridViewDataTextColumn Caption="Value" FieldName="AMOUNT" ReadOnly="True" VisibleIndex="1"
                                                                    Width="100px" CellStyle-HorizontalAlign="Right">
                                                                    <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                    <CellStyle HorizontalAlign="Right">
                                                                    </CellStyle>
                                                                </dx:GridViewDataTextColumn>
                                                            </Columns>
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewBandColumn>
                                                        <dx:GridViewDataTextColumn Caption="Cost Center Code" FieldName="COST_CENTER_CD"
                                                            VisibleIndex="4" Width="70px" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                                            <CellStyle HorizontalAlign="Center">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="WBS No." FieldName="WBS_NO" VisibleIndex="5"
                                                            Width="125px" Visible="False">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                                        </dx:GridViewDataTextColumn>
                                                        <dx:GridViewDataTextColumn Caption="Tax Code" FieldName="TAX_CD" VisibleIndex="6"
                                                            Width="35px">
                                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True" />
                                                            <CellStyle HorizontalAlign="Center" Wrap="True">
                                                            </CellStyle>
                                                        </dx:GridViewDataTextColumn>
                                                    </Columns>
                                                    <SettingsPager Visible="False" Mode="ShowAllRecords" PageSize="0">
                                                    </SettingsPager>
                                                    <Settings UseFixedTableLayout="True" ShowVerticalScrollBar="True" VerticalScrollableHeight="190" />
                                                    <Styles>
                                                        <AlternatingRow CssClass="even">
                                                        </AlternatingRow>
                                                    </Styles>
                                                </dx:ASPxGridView>
                                                <asp:LinqDataSource ID="LinqDataSource2" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                                                    EntityTypeName="" OnSelecting="LinqDataSource2_Selecting"
                                                    Select="new (SEQ_NO, INVOICE_NO, ITEM_TRANSACTION_CD, ITEM_TRANSACTION, ITEM_DESCRIPTION, CURRENCY_CD, AMOUNT, COST_CENTER_CD, WBS_NO,TAX_CD)"
                                                    TableName="TB_R_INVOICE_D">
                                                </asp:LinqDataSource>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </asp:Panel>
                    </center>
                </td>
            </tr>
        </table>
        <div class="rowbtn">
            <div class="btnright">
                <asp:Button runat="server" ID="btnClose" Text="Close" OnClientClick="closeWin()" />
            </div>
        </div>
    </div>
</asp:Content>
