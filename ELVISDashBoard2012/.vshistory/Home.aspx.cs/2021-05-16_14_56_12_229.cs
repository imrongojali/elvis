﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Linq;
using BusinessLogic;
using Common;
using Common.Data;
using Common.Function;
using DataLayer;
using DevExpress.Web.ASPxGridView;

namespace ELVISDashBoard
{
    public partial class Home : System.Web.UI.Page
    {
        #region Init
        private static string _ScreenID = "ELVIS_MainScreen";
        private UserData UserData
        {
            get { return Session["userData"] as UserData; }
        }

        private string UserName
        {
            get
            {
                return (UserData != null) ? UserData.USERNAME : "";
            }
        }

        private string AppInit
        {
            get
            {
                object o = System.Web.HttpContext.Current.Cache[_ScreenID];
                return o as string;
            }

            set
            {
                string v = Heap.Get<string>(_ScreenID + "_");
                if (string.Compare(v, value, true) == 0)
                {

                }
                else
                {
                    System.Web.HttpContext.Current.Cache.Insert(_ScreenID, value, null, DateTime.UtcNow.AddDays(2), System.Web.Caching.Cache.NoSlidingExpiration);
                    Heap.Set<string>(_ScreenID, value, Heap.Duration * 10000);
                }
            }
        }

        private List<RoleData> userRolesList = null;
        private List<RoleData> UserRoles
        {
            get
            {
                if (userRolesList == null)
                {
                    userRolesList = Session["userRolesList"] as List<RoleData>;
                    if (userRolesList == null)
                    {
                        ErrorData _Err = new ErrorData();
                        userRolesList = rolo.getUserRole(UserName, ref _Err);
                    }
                }
                return userRolesList;
            }
        }

        private readonly string REFRESHED = "REFRESH_COUNT";
        private int RefreshCount
        {
            get
            {
                int? r = 0;
                Int32 v = Heap.Get<Int32>(REFRESHED + "." + UserName);
                if (v != null)
                {
                    r = v as int?;
                    r += 1;
                }
                else
                {
                    r = 1;
                }
                v = Convert.ToInt32(r);
                Heap.Set<Int32>(REFRESHED + "." + UserName, v);

                return (int)r;
            }
        }


        private Common.Enum.ApprovalEnum ApprovalType
        {
            set
            {
                Session["PVApprovalType"] = (int)value;
            }
            get
            {
                Common.Enum.ApprovalEnum e = Common.Enum.ApprovalEnum.Idle;
                if (Session["PVApprovalType"] != null)
                    e = (Common.Enum.ApprovalEnum)(Convert.ToInt32(Session["PVApprovalType"]));
                return e;
            }
        }


        private List<ApproveRejectData> _listApproveRejectData
        {
            get
            {
                var v = Heap.Get<List<ApproveRejectData>>("approveRejectList." + UserName);
                if (v == null)
                    return new List<ApproveRejectData>();
                else
                    return v;
            }
            set
            {
                Heap.Set<List<ApproveRejectData>>("approveRejectList." + UserName, value, Heap.Duration * 10);
            }
        }

        private LogicFactory logic = LogicFactory.Get();

        private GlobalResourceData resx = new GlobalResourceData();

        private int userRoleClass = 0;

        private RoleLogic role = null;
        private RoleLogic rolo
        {
            get
            {
                if (role == null)
                {
                    role = new RoleLogic(UserName, _ScreenID);
                }
                return role;
            }
        }

        #endregion

        public Home()
        {
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Ticker.In("Home Load");
            if (!IsPostBack)
            {

                if (UserData == null)
                {
                    Response.Redirect("~/Default.aspx", true);
                    return;
                }

                if (!string.IsNullOrEmpty(AppInit))
                {
                    string v = AppInit;
                    if (string.Compare(v, Heap.Get<string>(_ScreenID + "_"), true) != 0)
                    {
                        LoggingLogic.say("_App", "recycled {0}", v);
                        AppInit = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                    }
                }
                else
                {
                    AppInit = DateTime.Now.ToString("yyyyMMdd_HHmmss");
                }

                string[] linkClass = new string[3];
                linkClass[0] = logic.Sys.GetText("HomeScreenLink", "Form");
                linkClass[1] = logic.Sys.GetText("HomeScreenLink", "List");
                linkClass[2] = logic.Sys.GetText("HomeScreenLink", "Approval");
                string currentUserRole = "";
                if (UserData.Roles.Count > 0)
                    currentUserRole = UserData.Roles[0];
                for (int i = 0; i < linkClass.Length; i++)
                {
                    if (("," + linkClass[i] + ",").Contains("," + currentUserRole + ","))
                    {
                        userRoleClass = i + 1;
                        break;
                    }
                }

                bool hasApprovalAccess = rolo.isAllowedAccess("btnApprove");
                divButtonApprove.Visible = hasApprovalAccess;
                btnApprove.Visible = hasApprovalAccess;
                btnReject.Visible = hasApprovalAccess;

                int h1 = 230;
                int h2 = h1 - 41;
                gvWorklist.Settings.VerticalScrollableHeight = (hasApprovalAccess) ? h2 : h1;

                GridViewColumn column = gvWorklist.Columns["cmd"];
                column.Visible = hasApprovalAccess;
            }
            // TODO : K2 Skip
            LoadGrid(1);
            // ------------
            Ticker.Out("Home Load");
        }

        private void CopyWorklistData(WorklistData a, WorklistData b)
        {
            a.Description = b.Description;
            a.HypValue = b.HypValue;
            a.TotalAmount = b.TotalAmount;
            a.PaidDate = b.PaidDate;
            a.ReceivedDate = b.ReceivedDate;
            a.HoldFlag = b.HoldFlag;
            a.Notices = b.Notices;
            a.NeedReply = b.NeedReply;
            a.DocDate = b.DocDate;
            a.DivName = b.DivName;
            a.Rejected = b.Rejected;
            a.STATUS_CD = b.STATUS_CD;
            a.StatusName = b.StatusName;
            a.PvTypeName = b.PvTypeName;
            a.ActivityDateFrom = b.ActivityDateFrom;
            a.ActivityDateTo = b.ActivityDateTo;
            a.PayMethodName = b.PayMethodName;
            a.BudgetNo = b.BudgetNo;
            a.WorklistNotice = b.WorklistNotice;
        }

        #region Load --> Grid
        private void LoadGrid(int byPageLoad = 0)
        {
            Ticker.In("LoadGrid");
            var lstAllWorklist = new List<WorklistData>();
            string hkey = "Worklist." + UserName;
            List<WorklistData> w = Heap.Get<List<WorklistData>>(hkey);

            if (byPageLoad != 0)
            {
                w = null;
                Ticker.Spin("Refreshed");
            }

            if (!(w != null && w.Count > 0))
                try
                {

                    if (UserData == null) return;
                    logic.k2.Open();
                    logic.Say("Home", "LoadGrid({0})", UserName);
                    Ticker.In("GetWorklistCompleteByProcess(" + UserName + ")");
                    var lstOrigWorklist = logic.k2.GetWorklistCompleteByProcess(UserName, "");
                    Ticker.Out();
                    Ticker.In("GetGrantorByAttorney");
                    var lstGrantor = logic.PoA.GetGrantorByAttorney(UserName, false);
                    Ticker.Out();
                    #region get --> ProjectName
                    string[] _ArrProjectName = resx.K2ProcID("Form_Registration_Submit_New").Split('\\');
                    string _ProjectName = _ArrProjectName[0];
                    #endregion

                    foreach (var kvp in lstOrigWorklist)
                    {

                        if (kvp.FullName.Contains(_ProjectName))
                        {
                            WorklistData _WorklistData = new WorklistData();
                            _WorklistData.Folio = kvp.Folio;
                            _WorklistData.SN = kvp.SN;
                            _WorklistData.CreatedDate = kvp.StartDate.ToString("dd MMM yyyy");
                            _WorklistData.WorklistTme = _WorklistData.WorklistTme = CalculateTime(kvp.StartDate);
                            TimeSpan ts = DateTime.Now.Subtract(kvp.StartDate);
                            _WorklistData.DocAgeSeconds = Convert.ToInt32(Math.Round(ts.TotalSeconds));

                            #region Get --> Document Number and Document Year
                            // fid 20180619. define whether worklist is Accrued or PV/RV
                            long _isnumericFolic;
                            if (!Int64.TryParse(kvp.Folio, out _isnumericFolic))
                            {
                                _WorklistData.IsAccrued = true;
                                _WorklistData.Type = "Accrued";
                                _WorklistData.DocNumber = kvp.Folio;
                                _WorklistData.DocYear = "";
                            }
                            else
                            {
                                _WorklistData.IsAccrued = false;
                                _WorklistData.Type = "";
                                _WorklistData.DocNumber = kvp.Folio.Substring(0, 9);
                                _WorklistData.DocYear = kvp.Folio.Substring(9, 4);
                            }
                            #endregion

                            _WorklistData.DivName = UserData.DIV_NAME;
                            //will be get from each of header data
                            _WorklistData.DocStatus = "";

                            // Add by FID.Arri on May 11, 2018 for new Dashboard
                            //_WorklistData.TransType = kvp.TransType;

                            lstAllWorklist.Add(_WorklistData);
                        }
                    }

                    if (lstGrantor != null)
                    {
                        foreach (var item in lstGrantor)
                        {
                            logic.Say("Home", "k2.GetWorklistCompleteByProcess({0})", item.GRANTOR);
                            Ticker.In("GetWorklistCompleteByProcess(" + item.GRANTOR + ")");
                            var grantorWorklist = logic.k2.GetWorklistCompleteByProcess(item.GRANTOR, "");
                            Ticker.Out();

                            foreach (var kvp in grantorWorklist)
                            {
                                if (kvp.FullName.Contains(_ProjectName))
                                {
                                    string _DocumentNo = "";
                                    string _DocumentYear = "";
                                    WorklistData _WorklistData = new WorklistData();

                                    _WorklistData.Folio = kvp.Folio;
                                    _WorklistData.SN = kvp.SN;
                                    _WorklistData.CreatedDate = string.Format("{0:dd/MM/yyyy HH:mm}", kvp.StartDate);
                                    _WorklistData.WorklistTme = CalculateTime(kvp.StartDate);

                                    #region Get --> Document Number and Document Year
                                    _DocumentNo = kvp.Folio.Substring(0, 9);
                                    _DocumentYear = kvp.Folio.Substring(9, 4);
                                    #endregion

                                    _WorklistData.DocNumber = _DocumentNo;
                                    _WorklistData.DocYear = _DocumentYear;

                                    if (UserData != null && UserData.DIV_NAME != null)
                                    {
                                        if (UserData.DIV_NAME.IndexOf('-') > 0)
                                            _WorklistData.DivName = UserData.DIV_NAME.Substring(0, UserData.DIV_NAME.IndexOf('-'));
                                        else
                                            _WorklistData.DivName = UserData.DIV_NAME;
                                    }
                                    //will be get from each of header data
                                    _WorklistData.DocStatus = "";

                                    // Add by FID.Arri on May 11, 2018 for new Dashboard
                                    //_WorklistData.TransType = kvp.TransType;

                                    lstAllWorklist.Add(_WorklistData);
                                }
                            }
                        }
                    }

                    if (lstAllWorklist != null && lstAllWorklist.Count > 0)
                    {
                        Ticker.In("FillWorklistData");
                        logic.Persist.FillWorklistData(lstAllWorklist);

                        Ticker.Out();
                    }

                    Ticker.In("Sort WorklistData by Doc Age");
                    lstAllWorklist.Sort(delegate (WorklistData a, WorklistData b)
                    {
                        int r = 0;
                        r = b.Rejected.CompareTo(a.Rejected);
                        if (r == 0)
                        {
                            r = b.DocAgeSeconds.CompareTo(a.DocAgeSeconds);
                        }
                        return r;
                    });

                    w = lstAllWorklist;
                    Ticker.Spin("Set [{0}]: {1} rows", hkey, w.Count);
                    Heap.Set(hkey, lstAllWorklist);

                    Ticker.Out();
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }
                finally
                {
                    logic.k2.Close();
                }
            else
            {
                Ticker.Spin("Get [{0}]: {1} rows", hkey, w.Count);
                lstAllWorklist = w;
            }

            gvWorklist.DataSource = lstAllWorklist;
            Ticker.Spin("Bind");
            gvWorklist.DataBind();

            Ticker.Out("LoadGrid");

        }
        #endregion


        protected void gvWorklist_HtmlRowPrepared(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {

                int Rejected = 0;

                try
                {
                    Rejected = Convert.ToInt32(e.GetValue("Rejected"));
                }
                catch (Exception x)
                {
                    Rejected = 0;
                }


                if (Rejected > 0)
                {

                    e.Row.Cells[0].CssClass = ((Rejected > 0) ? "rejected" : "");
                }

            }
        }

        #region gvWorklist --> HtmlRowCreated
        protected void gvWorklist_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {

                try
                {

                    WorklistData _WorklistData = (WorklistData)gvWorklist.GetRow(e.VisibleIndex);
                    if (_WorklistData == null) return;
                    HyperLink hypDetail = (HyperLink)gvWorklist.FindRowCellTemplateControl(e.VisibleIndex, gvWorklist.Columns["colDetail"] as GridViewDataColumn, "hypDetail");
                    Label lblHoldFlag = gvWorklist.FindRowCellTemplateControl(e.VisibleIndex, gvWorklist.Columns["colDetail"] as GridViewDataColumn, "lblHoldFlag") as Label;
                    Label lblNotices = gvWorklist.FindRowCellTemplateControl(e.VisibleIndex, gvWorklist.Columns["colDetail"] as GridViewDataColumn, "lblNotices") as Label;
                    Image imgStatus = gvWorklist.FindRowCellTemplateControl(e.VisibleIndex, gvWorklist.Columns["notice"] as GridViewDataColumn, "imgStatus") as Image;


                    ErrorData _Err = new ErrorData();


                    hypDetail.NavigateUrl = GetUrl(userRoleClass, _WorklistData);
                    hypDetail.Text = _WorklistData.DocNumber;

                    lblHoldFlag.Visible = (_WorklistData.HoldFlag != null) && (_WorklistData.HoldFlag > 0);
                    lblNotices.Visible = (_WorklistData.Notices > 0);
                    lblNotices.Text = "(" + _WorklistData.Notices.ToString() + ")";
                    int wh = 1; string img = "void";



                    if (_WorklistData.Notices > 0 && _WorklistData.WorklistNotice)
                    {
                        if (_WorklistData.NeedReply > 0)
                        {

                            img = "qmark";
                        }
                        else
                        {

                            img = "ytrig";
                        }
                        imgStatus.ImageUrl = "~/App_Themes/BMS_Theme/Images/" + img + ".png";
                    }
                }
                catch (Exception Ex)
                {
                    LoggingLogic.err(Ex);
                }
                finally
                {

                }
            }
        }
        #endregion

        private string GetUrl(int roleClass, WorklistData wl)
        {
            string detailURL = "";
            string docNumber = wl.DocNumber;
            string docYear = wl.DocYear;
            int TransCd = wl.TransCd;
            DateTime d = wl.DocDate;

            if (wl.IsAccrued)
            {
                if (docNumber.isEmpty()) return detailURL;

                if (docNumber.StartsWith("LA"))
                {
                    // List Accrued
                    //var sts = logic.AccrForm.GetLatestStatus(docNumber);
                    var sts = wl.STATUS_CD;
                    if (sts != 104)
                    {
                        detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrForm.aspx?accrno={0}&mode=view", docNumber));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8005"));
                    }
                    else
                    {
                        detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrWBS.aspx?accr_no={0}", docNumber));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8015"));
                    }
                }
                else if (docNumber.StartsWith("AE"))
                {
                    // Extend
                    detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrExtendForm.aspx?extend_no={0}&mode=view", docNumber));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8012"));
                }
                else if (docNumber.StartsWith("BS"))
                {
                    // Shifting
                    detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrShiftingForm.aspx?shiftingno={0}&mode=view", docNumber));
                    detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8014"));
                }
            }
            else
            {
                if (docNumber.isEmpty() || docYear.isEmpty()) return detailURL;

                if (docNumber.Int() < 600000000)
                {
                    if (roleClass == 1)
                    {
                        // modified by FID.Ridwan 11072018 add logic transcd
                        if (TransCd == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                        {
                            detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrPVForm.aspx?pv_no={0}&pv_year={1}", docNumber, docYear));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                        }
                        else
                        {
                            detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVFormList.aspx?pv_no={0}&pv_year={1}", docNumber, docYear));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                        }
                    }
                    else if (roleClass == 2)
                    {
                        detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVList.aspx?date={0}&doc_no={1}", d.ToString("yyyy-MM-dd"), docNumber));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3002"));
                    }
                    else
                    {
                        detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVApproval.aspx?pvno={0}&pvyear={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3003"));
                    }
                }
                else
                {
                    if (roleClass == 1)
                    {
                        // modified by FID.Ridwan 11072018 add logic transcd
                        if (TransCd == logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
                        {
                            detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrRVForm.aspx?rv_no={0}&rv_year={1}", docNumber, docYear));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4001"));
                        }
                        else
                        {
                            detailURL = ResolveClientUrl(string.Format("~/40RvFormList/RVFormList.aspx?rv_no={0}&rv_year={1}", docNumber, docYear));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4001"));
                        }
                    }
                    else if (roleClass == 2)
                    {

                        detailURL = ResolveClientUrl(string.Format("~/40RvFormList/RVList.aspx?date={0}&doc_no={1}", d.ToString("yyyy-MM-dd"), docNumber));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4002"));
                    }
                    else
                    {
                        detailURL = ResolveClientUrl(string.Format("~/40RvFormList/RVApproval.aspx?rvno={0}&rvyear={1}", docNumber, docYear));
                        detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4003"));
                    }
                }
            }
            return detailURL;
        }

        #region Approval and rejection

        #region btnApprove_Click
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            logic.Say("Home", "btnApprove_Click");
            if (gvWorklist.Selection.Count >= 1)
            {
                _listApproveRejectData = GetSelectedRowValues(gvWorklist);
                ApprovalType = Common.Enum.ApprovalEnum.Approve;
                txtNotes.Text = "";
                Act();
                // TODO : K2 Skip
                LoadGrid();
                //---------------
            }
            else if (gvWorklist.Selection.Count == 0)
            {
                #region Error
                lblErrorMessage.Text = resx.Message("MSTD00009WRN");
                ErrorPopUp.Show();
                #endregion
            }
        }
        #endregion

        #region btnReject_Click
        protected void btnReject_Click(object sender, EventArgs e)
        {
            if (gvWorklist.Selection.Count >= 1)
            {
                _listApproveRejectData = GetSelectedRowValues(gvWorklist);
                ApprovalType = Common.Enum.ApprovalEnum.Reject;
                SetPopUpApproval();
                popUpApproval.Show();
            }
            else if (gvWorklist.Selection.Count == 0)
            {
                #region Error
                lblErrorMessage.Text = resx.Message("MSTD00009WRN");
                ErrorPopUp.Show();
                #endregion
            }
        }
        #endregion

        #region SetPopUpApproval
        private void SetPopUpApproval()
        {
            if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
            {
                pnlApproval.GroupingText = "Approval Confirmation";
                btnApprovalProceed.Text = "Approve";
                btnApprovalProceed.BackColor = System.Drawing.Color.Green;
            }
            else if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
            {
                pnlApproval.GroupingText = "Rejection Confirmation";
                btnApprovalProceed.Text = "Reject";
                btnApprovalProceed.BackColor = System.Drawing.Color.Red;
            }
            else
            {
                pnlApproval.GroupingText = "Confirmation";
                btnApprovalProceed.Text = "Go";
                btnApprovalProceed.BackColor = System.Drawing.Color.Gray;
            }
            string listFolio = "";
            if (_listApproveRejectData.Count > 0)
            {
                foreach (ApproveRejectData item in _listApproveRejectData)
                {
                    listFolio += item.StrDocYear + " - " + item.StrDocNo + "; ";
                }
                listFolio = listFolio.Substring(0, listFolio.Length - 2);
            }
            txtApprovalDocNo.Text = listFolio;
        }
        #endregion

        protected void Act()
        {
            try
            {
                if (ApprovalType == Common.Enum.ApprovalEnum.Approve || (ApprovalType == Common.Enum.ApprovalEnum.Reject))
                {
                    #region Get --> Comment
                    string _Comment = txtNotes.Text;

                    #endregion

                    if (_listApproveRejectData.Count > 0)
                    {
                        string reffNos = string.Join(",", _listApproveRejectData.Select(a => a.Folio).Distinct().ToArray());
                        int act = 0;
                        _listApproveRejectData = logic.Base.GetStatusCd(_listApproveRejectData);
                        string statuses = logic.Sys.GetText("MASS_ACT", "STATUS", "");
                        int[] allowedStatus = new int[] { };
                        if (statuses.Length > 2)
                            allowedStatus = statuses.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(a => a.Int()).ToArray();

                        act = (int)ApprovalType;

                        int massActId = logic.MassAct.Do(reffNos, _Comment, act, UserName, 0);

                        bool _ApprovalError = false;
                        List<string> _ListApprovalError = new List<string>();
                        int DocType = 0;
                        string[] DocTypeStr = new string[] { "", "PV", "RV" };
                        foreach (ApproveRejectData _approveRejectData in _listApproveRejectData)
                        {
                            bool Ok = false;
                            bool proceed = ((_approveRejectData.StatusCd != null)
                                 && (allowedStatus.Contains(_approveRejectData.StatusCd ?? -1)
                                    || allowedStatus.Length == 0
                                    )
                                 );

                            if (proceed)
                            {
                                DocType = (_approveRejectData.Folio.Substring(1, 1).Int() < 6) ? 1 : 2;
                                #region This block is PV
                                if (_approveRejectData.Folio.StartsWith("1") || _approveRejectData.Folio.StartsWith("2")
                                    || _approveRejectData.Folio.StartsWith("3") || _approveRejectData.Folio.StartsWith("4")
                                    || _approveRejectData.Folio.StartsWith("5"))
                                {
                                    #region Get --> Command
                                    string _Command = "";
                                    if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
                                        _Command = resx.K2ProcID("Form_Registration_Approval");
                                    else if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
                                        _Command = resx.K2ProcID("Form_Registration_Rejected");
                                    #endregion

                                    #region Do --> Approve-Reject
                                    if (!logic.PVRVWorkflow.Go(_approveRejectData.Folio, _Command, UserData, _Comment))
                                    {
                                        _ListApprovalError.Add(_approveRejectData.Folio);
                                        _ApprovalError = true;

                                        logic.Say("Home", "error {0}: {1}", ApprovalType.ToString(), _approveRejectData.Folio);
                                    }
                                    //added by Akhmad Nuryanto, send email
                                    else
                                    {
                                        logic.MassAct.Done(_approveRejectData.Folio, massActId, UserName);
                                        sendEmail(_approveRejectData.StrDocNo, _approveRejectData.StrDocYear);
                                        Ok = true;
                                    }
                                    //end of addition by Akhmad Nuryanto
                                    #endregion
                                }
                                #endregion
                                #region This Block is RV
                                else
                                {
                                    #region Get --> Command
                                    string _Command = "";
                                    if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
                                        _Command = resx.K2ProcID("Form_Registration_Approval");
                                    else if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
                                        _Command = resx.K2ProcID("Form_Registration_Rejected");
                                    #endregion

                                    #region Do --> Approve-Reject
                                    if (!logic.PVRVWorkflow.Go(_approveRejectData.Folio, _Command, UserData, _Comment))
                                    {
                                        _ListApprovalError.Add(_approveRejectData.Folio);
                                        _ApprovalError = true;
                                        logic.Say("Home", "error {0}: {1}", ApprovalType.ToString(), _approveRejectData.Folio);
                                    }
                                    //added by Akhmad Nuryanto, send email
                                    else
                                    {
                                        logic.MassAct.Done(_approveRejectData.Folio, massActId, UserName);
                                        sendEmail(_approveRejectData.StrDocNo, _approveRejectData.StrDocYear);
                                        Ok = true;
                                    }
                                    //end of addition by Akhmad Nuryanto
                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                Ok = false;
                                _ListApprovalError.Add(_approveRejectData.Folio);
                                _ApprovalError = true;
                                logic.Say("Home", "{0} status {1} not allowed for Mass Action", _approveRejectData.Folio, _approveRejectData.StatusCd);
                            }
                            logic.Say("Home", "Approve {0}# {1} {2} {3}",
                                    DocTypeStr[DocType],
                                    _approveRejectData.StrDocNo,
                                    _approveRejectData.StrDocYear,
                                    (Ok ? "OK" : "ERR"));

                        }
                        if (_ApprovalError)
                        {
                            if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
                            {
                                lblErrorMessage.Text = String.Format(resx.Message("MSTD00062ERR"), "Approve");
                                lblErorData.Text = getStringFromList(_ListApprovalError);
                                ErrorPopUp.Show();
                            }
                            else if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
                            {
                                lblErrorMessage.Text = String.Format(resx.Message("MSTD00062ERR"), "Reject");
                                lblErorData.Text = getStringFromList(_ListApprovalError);
                                ErrorPopUp.Show();
                            }
                        }
                        else
                        {
                            if (ApprovalType == Common.Enum.ApprovalEnum.Approve)
                            {
                                lblErrorMessage.Text = String.Format(resx.Message("MSTD00060INF"), "Approve");
                                ErrorPopUp.Show();
                            }
                            else if (ApprovalType == Common.Enum.ApprovalEnum.Reject)
                            {
                                lblErrorMessage.Text = String.Format(resx.Message("MSTD00060INF"), "Reject");
                                ErrorPopUp.Show();
                            }
                        }
                    }
                }
                else
                {
                    #region Error
                    lblErrorMessage.Text = resx.Message("MSTD00059ERR");
                    ErrorPopUp.Show();
                    #endregion
                }
                Heap.Set<List<WorklistData>>("Worklist." + (UserName ?? ""), null);
            }
            catch (Exception Ex)
            {
                string _ErrMsg = !string.IsNullOrEmpty(Convert.ToString(Ex.InnerException)) ? Convert.ToString(Ex.InnerException) : Ex.Message;
                Handle(Ex);
                lblErrorMessage.Text = _ErrMsg;
                ErrorPopUp.Show();
            }
        }


        #region btnApproval_Click
        protected void btnApprovalProceed_Click(object sender, EventArgs e)
        {
            popUpApproval.Hide();
            Act();
            // TODO : K2 Skip
           LoadGrid();
           //------------------
        }
        #endregion

        #endregion

        #region Function

        private List<ApproveRejectData> GetSelectedRowValues(ASPxGridView gridView)
        {
            List<ApproveRejectData> listData = new List<ApproveRejectData>();
            ApproveRejectData data;
            string folio;

            string[] valueToGet = { "Folio" };
            bool canAccroed = (logic.Sys.GetNum("MASS_ACT/ACCRUED", 0) > 0);

            for (int i = 0; i < gridView.VisibleRowCount; i++)
            {
                if (gridView.Selection.IsRowSelected(i))
                {
                    data = new ApproveRejectData();
                    folio = gridView.GetRowValues(i, "Folio").ToString();

                    if (folio != null && !folio.Equals(""))
                    {
                        data.Folio = folio;
                        data.StrDocNo = data.Folio.Substring(0, 9);
                        data.StrDocYear = data.Folio.Substring(9, 4);
                        data.IntDocNo = Convert.ToInt32(data.StrDocNo);
                        data.IntDocYear = Convert.ToInt32(data.StrDocYear);
                    }
                    else
                        data.Folio = "";
                    int foli = 0;

                    if (!int.TryParse(folio, out foli)
                        && canAccroed)
                        folio = "";

                    if (!folio.isEmpty())
                        listData.Add(data);
                }
            }

            return listData;
        }

        protected static string CalculateTime(DateTime dtCreated)
        {
            string result = "";
            TimeSpan diff = DateTime.Now.Subtract(dtCreated);
            int days = (int)diff.Days;
            int hours = (int)diff.Hours;
            int minutes = ((int)diff.Minutes) + (diff.Seconds > 30 ? 1 : 0);

            if (days > 0)
            {
                result = days + (days == 1 ? " Day" : " Days");
            }
            else if (hours > 0)
            {
                result = hours + (hours == 1 ? " Hour" : " Hours");
            }
            else if (minutes > 0)
            {
                result = minutes + (minutes == 1 ? " Minute" : " Minutes");
            }
            return result;
        }

        protected static string getStringFromList(List<string> _ListString)
        {
            string returnString = "";
            if (_ListString.Count > 0)
            {
                foreach (string strItem in _ListString)
                {
                    returnString += strItem + ", ";
                }
                returnString = returnString.Substring(0, returnString.Length - 2);
            }
            return returnString;
        }
        #endregion

        protected void LinqDataSource2_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            Ticker.In("Home Notice");
            e.Result = logic.Notice.Search(UserName);
            Ticker.Out("Home Notice");
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            Ticker.In("Home Info");
            e.Result = logic.Announce.SearchInquiryHome(UserData);
            Ticker.Out("Home Info");
        }

        protected void gridNoticeList_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            if (e.RowType == GridViewRowType.Data)
            {
                try
                {
                    if (UserData == null) return;
                    Literal litNo = gridNoticeList.FindRowCellTemplateControl(e.VisibleIndex, gridNoticeList.Columns["NO"] as GridViewDataColumn, "litNo") as Literal;
                    Image imgStatus = gridNoticeList.FindRowCellTemplateControl(e.VisibleIndex, gridNoticeList.Columns["status"] as GridViewDataColumn, "imgStatus") as Image;
                    Button btnDetail = gridNoticeList.FindRowCellTemplateControl(e.VisibleIndex, gridNoticeList.Columns["colDetail"] as GridViewDataColumn, "btnDetail") as Button;

                    litNo.Text = (e.VisibleIndex + 1).ToString();

                    string detailURL = "";

                    ErrorData _Err = new ErrorData();
                    List<String> roles = new List<string>();

                    foreach (RoleData data in UserRoles)
                    {
                        roles.Add(data.ROLE_ID.ToLower().Replace('_', ' '));
                    }

                    string DocNo = e.GetValue("DOC_NO").ToString();
                    string DocYear = e.GetValue("DOC_YEAR").ToString();
                    string AccrNo = e.GetValue("ACCRUED_NO").ToString();
                    int status = Convert.ToInt32(e.GetValue("REPLY_FLAG") ?? "0");

                    if (AccrNo.isNotEmpty())
                    {
                        if (AccrNo.StartsWith("LA"))
                        {
                            // List Accrued
                            detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrForm.aspx?accrno={0}&mode=view", AccrNo));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8005"));
                        }
                        else if (AccrNo.StartsWith("AE"))
                        {
                            // Extend
                            detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrExtendForm.aspx?extend_no={0}&mode=view", AccrNo));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8012"));
                        }
                        else if (AccrNo.StartsWith("BS"))
                        {
                            // Shifting
                            detailURL = ResolveClientUrl(string.Format("~/80Accrued/AccrShiftingForm.aspx?shiftingno={0}&mode=view", AccrNo));
                            detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_8014"));
                        }
                    }
                    else
                    {
                        if (Decimal.Parse(DocNo) < 600000000)
                        {
                            if (roles.Contains("elvis user admin") || roles.Contains("elvis fd staff"))
                            {
                                detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVFormList.aspx?pv_no={0}&pv_year={1}", DocNo, DocYear));
                                detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3001"));
                            }
                            else
                            {
                                detailURL = ResolveClientUrl(string.Format("~/30PvFormList/PVApproval.aspx?pvno={0}&pvyear={1}", DocNo, DocYear));
                                detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_3003"));
                            }
                        }
                        else
                        {
                            if (roles.Contains("elvis user admin") || roles.Contains("elvis fd staff"))
                            {
                                detailURL = ResolveClientUrl(string.Format("~/40RvFormList/RVFormList.aspx?rv_no={0}&rv_year={1}", DocNo, DocYear));
                                detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4001"));
                            }
                            else
                            {
                                detailURL = ResolveClientUrl(string.Format("~/40RvFormList/RVApproval.aspx?rvno={0}&rvyear={1}", DocNo, DocYear));
                                detailURL = string.Format("javascript:openWin('{0}', '{1}')", detailURL, resx.Screen("ELVIS_Screen_4003"));
                            }
                        }
                    }

                    if (status > 0)
                    {
                        imgStatus.ImageUrl = "~/App_Themes/BMS_Theme/Images/qmark.png";
                    }
                    else
                    {

                        imgStatus.ImageUrl = "~/App_Themes/BMS_Theme/Images/ytrig.png";
                    }

                    btnDetail.OnClientClick = detailURL;

                }
                catch (Exception Ex)
                {
                    Handle(Ex);
                }
            }
        }


        private void sendEmail(String PVNo, String PVYear)
        {
        }

        private void Handle(Exception ex)
        {
            logic.Log.Log("MSTD00002ERR", LoggingLogic.Trace(ex),
                "Home", UserName);
        }
    }
}
