﻿-- GetWbsDistinct
select WbsNumber
	, Year
	, EOA
	, Division
	, Description
from
(
	select WbsNumber
		 , Year
		 , EOA
		 , Division
		 , Description
		 , ROW_NUMBER() OVER (PARTITION BY WbsNumber ORDER BY Year DESC) RN
	from vw_WBS
	where WbsNumber != '' and WbsNumber is not null
) a
where a.RN = 1