﻿declare 
	  @@vendorCd varchar(10) = @code
	, @@group int = @group
	, @@search VARCHAR(10) = @terms
	, @@name varchar(40) = @name
	, @@likeS int = 0
	, @@likeC int = 0
	, @@likeN int = 0
;
		
set @@vendorCd = REPLACE(@@vendorCd, '*', '%');
set @@search= REPLACE(@@search, '*', '%');
set @@name= REPLACE(@@name, '*', '%');
IF CHARINDEX('%', @@vendorCd) > 0 
BEGIN
	set @@likeC = 1;
END; 

IF CHARINDEX('%', @@search) > 0 
BEGIN
	set @@likeS = 1;
END; 

IF CHARINDEX('%', @@name) > 0 
BEGIN
	set @@likeN = 1;
END; 


select 
	 a.vendor_cd
	,v.vendor_name
	,v.vendor_group_cd
	,g.vendor_group_name
	,v.search_terms
	,a.name_of_bank
	,a.bank_account
	,case when vb.bank_account is null then 'On Request'
		  else 'Registered'
	end [status]
	,a.created_by
	,a.created_dt
	,a.beneficiaries
    ,a.directory
    ,a.file_name
    ,a.guid_filename
	,'/AttachFile/'+a.directory+'/'+a.guid_filename [url]
from tb_m_vendor_bank a
  join vw_vendor v on a.vendor_cd = v.vendor_cd
  left join sap_db.dbo.tb_m_vendor_bank vb on vb.vendor_cd = a.vendor_cd
		and a.bank_key = vb.bank_key
		and a.bank_account = vb.bank_account
  left join tb_m_vendor_group g on g.vendor_group_cd = v.vendor_group_cd
WHERE (NULLIF(LTRIM(RTRIM(@@vendorCd)),'') IS NULL 
	   OR (@@likeC = 0 AND v.VENDOR_CD = @@vendorCd )
	   OR (@@likeC = 1 AND v.VENDOR_CD LIKE @@vendorCd )
	    )
	    
  AND (NULLIF(LTRIM(RTRIM(@@name)),'') IS NULL 
       OR (@@likeN = 0 AND v.VENDOR_NAME = @@name )
       OR (@@likeN = 1 AND v.VENDOR_NAME LIKE @@name )
       )
       
  AND (NULLIF(LTRIM(RTRIM(@@search)),'') IS NULL 
       OR (@@likeS = 0 AND v.SEARCH_TERMS = @@search )
       OR (@@likeS = 1 AND v.SEARCH_TERMS LIKE @@search )
       )

  AND (NULLIF(@@group,0) IS NULL 
       OR (v.VENDOR_GROUP_CD = @@group )
       )