-- GetLastUploadAccr
select PROCESS_ID as [Pid]
	, SEQ_NO as [SequenceNumber]
	, ACTIVITY_DES as [ActivityDescription]
	, WBS_NO_OLD as [WbsNumber]
	, PV_TYPE_CD as [PVTypeCd]
	, PV_TYPE_NAME as [PVType]
	, COST_CENTER as [CostCenterCode]
	, CURRENCY_CD as [CurrencyCode]
	, AMOUNT as [Amount]
	, AMOUNT_IDR as [AmountIdr]
	, SUSPENSE_NO_OLD as [SuspenseNo]
	, BOOKING_NO as [BookingNo]
from TB_T_ACCR_LIST_D
WHERE PROCESS_ID = @pid