-- GetCostCenters

SELECT 
ISNULL(LTRIM(RTRIM(cc.COST_CENTER)),'') AS [COST_CENTER_CD],
ISNULL(LTRIM(RTRIM(cc.DESCRIPTION)), '') AS [COST_CENTER_NAME],
ISNULL(LTRIM(RTRIM(cc.DIVISION)), '') AS [DIVISION],                
cc.PRODUCTION_FLAG
FROM vw_CostCenter AS cc 