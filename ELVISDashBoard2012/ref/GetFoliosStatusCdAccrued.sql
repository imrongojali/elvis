-- GetFoliosStatusCd

DECLARE @@folios VARCHAR(MAX) = ISNULL(@folios,'');

SELECT D.REFF_NO [Folio]
    , D.DOC_NO [StrDocNo]
    , D.DOC_YEAR [StrDocYear]
    , D.DOC_CD [DocCd]
    , CASE WHEN D.DOC_CD IN (1,2) THEN CONVERT(INT, D.DOC_NO) ELSE 0 END [IntDocNo]
    , CASE WHEN D.DOC_CD IN (1,2) THEN CONVERT(INT, D.DOC_YEAR) ELSE 0 END [IntDocYear]
    , CASE WHEN DOC_CD = 1 THEN P.STATUS_CD
      WHEN DOC_CD = 2 THEN R.STATUS_CD
      WHEN DOC_CD = 0 THEN A.STATUS_CD
      ELSE NULL
      END [StatusCd]
FROM (	
    select DISTINCT REFF_NO, DOC_NO, DOC_YEAR, ISNULL(N.DOC_CD , 0) DOC_CD
    FROM
    (
    SELECT X.REFF_NO , CASE WHEN ISNUMERIC(X.REFF_NO) =1 THEN dbo.fn_DocNoFromReff(x.REFF_NO) ELSE NULL END DOC_NO
    , CASE WHEN ISNUMERIC(REFF_NO) =1 THEN dbo.fn_DocYearFromReff(x.REFF_NO) ELSE NULL END DOC_YEAR
    FROM
    (SELECT items REFF_NO FROM DBO.FN_EXPLODE(',',@@folios) X) X
    ) Z
    LEFT join (
    SELECT DOC_CD, [FROM], [TO] FROM TB_M_NUMBER_RANGE
    ) N ON DOC_NO BETWEEN N.[FROM] AND N.[TO]
) D
LEFT JOIN TB_R_PV_H P ON P.PV_NO = D.DOC_NO AND P.PV_YEAR = D.DOC_YEAR AND D.DOC_CD = 1
LEFT JOIN TB_R_RV_H R ON R.RV_NO = D.DOC_NO AND R.RV_YEAR = D.DOC_YEAR AND D.DOC_CD = 2
LEFT JOIN TB_R_ACCR_LIST_H A ON A.ACCRUED_NO = D.REFF_NO AND D.DOC_CD = 0
