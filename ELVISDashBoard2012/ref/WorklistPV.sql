-- WorklistPV
SELECT
 [Folio]             = CONVERT(VARCHAR(15), dbo.fn_DocNoYearToReff(h.PV_NO, h.PV_YEAR)) ,
 [DocNumber]         = h.PV_NO,
 [DocYear]           = h.PV_YEAR,
 [DocDate]           = h.PV_DATE,
 [HypValue]          = CONVERT(VARCHAR(10), h.PV_NO)
                     + (CASE WHEN (ISNULL(nc.NO_OF_NOTICE,0) > 0) THEN '(' + CONVERT(VARCHAR(15),ISNULL(nc.NO_OF_NOTICE,0)) + ')' ELSE '' END)
                     + (CASE WHEN  ISNULL(nc.REPLY_FLAG,0)> 0 THEN '*' ELSE '' END) ,
 [Description]       =  (SELECT TRANSACTION_NAME
                           FROM dbo.TB_M_TRANSACTION_TYPE AS m
                          WHERE (TRANSACTION_CD = h.TRANSACTION_CD)) + '-' + v.VENDOR_NAME ,

 [TotalAmount]       = aidr.TOTAL_AMOUNT_IDR,
 [Notices]           = ISNULL(nc.NO_OF_NOTICE,0),
 [PaidDate]          = h.PAID_DATE ,
 [HoldFlag]          = h.HOLD_FLAG ,
 [NeedReply]         = ISNULL(nc.REPLY_FLAG,0) ,
 [DivName]           = CASE
                       WHEN CHARINDEX ('-',REVERSE (m.DIVISION_NAME)) > 0
                       THEN RTRIM (SUBSTRING(m.DIVISION_NAME, 0, LEN (m.DIVISION_NAME) -
                            CHARINDEX ('-',REVERSE (m.DIVISION_NAME)) +1))
                       ELSE m.DIVISION_NAME
                       END ,
 [Rejected]          = CASE
                       WHEN h.STATUS_CD IN (SELECT STATUS_CD FROM dbo.fn_GetRejectedStatus (1) RS)
                       THEN 1
                       ELSE 0
                       END ,
 h.STATUS_CD,
 [StatusName]        = st.status_name ,
 [PvTypeName]        = pt.pv_type_name ,
 [ActivityDateFrom]  = CONVERT(VARCHAR(20), h.ACTIVITY_DATE_FROM, 106) ,
 [ActivityDateTo]    = CONVERT(VARCHAR(20), h.ACTIVITY_DATE_TO, 106) ,
 [PayMethodName]     = pm.PAY_METHOD_NAME ,
 [BudgetNo]          = h.BUDGET_NO ,
 [WorklistNotice]    = CONVERT(BIT, CASE WHEN r.NO_NOTICE_WORKLIST > 0 THEN 1 ELSE 0 END)  ,
 [TransCd]           = h.TRANSACTION_CD

FROM (
{0}
) g
     JOIN dbo.TB_R_PV_H AS h on h.PV_NO = g.DOC_NO
     JOIN TB_M_STATUS st ON st.STATUS_CD = h.STATUS_CD
     JOIN TB_M_PV_TYPE pt ON pt.PV_TYPE_CD = H.PV_TYPE_CD
     JOIN TB_M_PAYMENT_METHOD pm ON pm.PAY_METHOD_CD = h.PAY_METHOD_CD
LEFT JOIN SAP_DB.dbo.TB_M_VENDOR v ON V.VENDOR_CD = h.VENDOR_CD
LEFT JOIN TMMIN_ROLE.dbo.TB_M_DIVISION m ON m.DIVISION_ID = h.DIVISION_ID
LEFT JOIN
    (SELECT DOC_NO,
            DOC_YEAR,
            COUNT(1) AS NO_OF_NOTICE,
            SUM(REPLY_FLAG) AS REPLY_FLAG
    FROM dbo.TB_R_NOTICE AS n
    WHERE (USER_ROLE_TO IS NOT NULL)
    AND   (LEN(USER_ROLE_TO) > 0)
    OR    (NOTICE_DT IS NOT NULL)
    GROUP BY DOC_YEAR,
            DOC_NO)
    [nc]
    ON nc.DOC_NO = h.PV_NO AND nc.DOC_YEAR = h.PV_YEAR
LEFT JOIN
(   SELECT n.DOC_NO,
            n.DOC_YEAR,
            COUNT(dist.NO_NOTICE_WORKLIST) AS NO_NOTICE_WORKLIST
    FROM dbo.TB_R_NOTICE AS n
        LEFT  JOIN (SELECT REFF_NO,
                                MAX(ACTUAL_DT) AS ACTUAL_DT,
                                COUNT(1) AS NO_NOTICE_WORKLIST
                        FROM dbo.TB_R_DISTRIBUTION_STATUS AS a
                        WHERE (ACTUAL_DT IS NOT NULL)
                        GROUP BY REFF_NO) AS dist
                        ON CONVERT (VARCHAR,dist.REFF_NO) = dbo.fn_DocNoYearToReff(n.DOC_NO,n.DOC_YEAR)
    WHERE (n.NOTICE_DT IS NULL)
    OR    (dist.ACTUAL_DT < n.NOTICE_DT)
    OR    (n.REPLY_DT IS NULL)
    OR    (dist.ACTUAL_DT < n.REPLY_DT)
    GROUP BY n.DOC_YEAR, n.DOC_NO
)   [r]
    ON r.DOC_NO = h.PV_NO AND r.DOC_YEAR = h.PV_YEAR
LEFT JOIN (
    SELECT a.DOC_YEAR,
            a.DOC_NO,
            ROUND(
                SUM(ISNULL (a.EXCHANGE_RATE,ISNULL (x.EXCHANGE_RATE,0))
                    *a.TOTAL_AMOUNT)
                ,2) AS TOTAL_AMOUNT_IDR
    FROM dbo.TB_R_PVRV_AMOUNT [a]
    LEFT  JOIN (
        SELECT
        c.CURRENCY_CD,
        ISNULL (
            (SELECT TOP 1 r.EXCHANGE_RATE
                FROM SAP_DB.dbo.TB_M_EXCHANGE_RATE r
                WHERE r.CURR_CD = c.CURRENCY_CD
                  AND SYSDATETIME () >= r.VALID_FROM
            ORDER BY r.VALID_FROM DESC),
            ISNULL (x.EXCHANGE_RATE,1)) EXCHANGE_RATE,
        ISNULL(X.VALID_FROM, CONVERT(DATE, '1900-01-01')) VALID_FROM
    FROM TB_M_CURRENCY [c]
    LEFT JOIN (
        SELECT
            er.CURR_CD,
            er.VALID_FROM,
            er.EXCHANGE_RATE
        FROM SAP_DB.dbo.TB_M_EXCHANGE_RATE er
        WHERE er.VALID_FROM =
            (SELECT MAX(VALID_FROM)
            FROM SAP_DB.dbo.TB_M_EXCHANGE_RATE r
            WHERE CURR_CD = er.CURR_CD)) AS x
        ON c.CURRENCY_CD = x.CURR_CD
    ) [x]
    ON x.CURRENCY_CD = a.CURRENCY_CD
    GROUP BY a.DOC_YEAR,a.DOC_NO
)   [aidr]
    ON aidr.DOC_YEAR = h.PV_YEAR AND aidr.DOC_NO = h.PV_NO
