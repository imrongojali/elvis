-- GetSuspenseAmountBySettlement
-- DECLARE @no INT= 0, @year INT = 0; 
SELECT
	su.TOTAL_AMOUNT 
	-- , se.PV_NO, se.PV_YEAR, se.SUSPENSE_NO, se.SUSPENSE_YEAR, su.PV_TYPE_CD, se.PV_TYPE_CD, se.TOTAL_AMOUNT
FROM TB_R_PV_H su 
JOIN TB_R_PV_H se ON se.SUSPENSE_NO = su.PV_NO AND se.SUSPENSE_YEAR = su.PV_YEAR
WHERE se.PV_NO = @no and se.PV_YEAR = @year 