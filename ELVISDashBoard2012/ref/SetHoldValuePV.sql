DECLARE @@nm varchar(50) = 'SetHoldValuePV '

--	, @docno int = 500140141
--	, @docyear int = 2018
--	, @holdFlag int = null 
--	, @holdValue int = 1
--	, @username varchar(20) = 'yesse' 
--select pv_no, pv_year , hold_by, hold_flag, CHANGED_BY, CHANGED_DATE
--from tb_r_pv_h h where h.pv_no = @docno and h.pv_year = @docyear 

DECLARE 
@@NEW_HOLD_FLAG INT = NULL, 
@@DOC_NO INT = @docNo , 
@@DOC_YEAR INT = @docYear, 
@@HoldFlag int = @holdFlag, 
@@HoldValue int = @holdValue, 
@@UNAME VARCHAR(50) = @username


SELECT @@NEW_HOLD_FLAG = 
	CASE WHEN X.HOLD_FLAG IS NULL 
	     THEN 
		       CASE WHEN x.OLD_HOLD_FLAG = 0 
				    THEN CASE WHEN X.HOLD_VALUE > 0 THEN X.HOLD_VALUE ELSE 1 END 
			        WHEN x.OLD_HOLD_FLAG != 0 
  			        THEN ISNULL(x.OLD_HOLD_FLAG, 0)* -1
			        END 
					 
		ELSE 			 
			 CASE WHEN X.HOLD_FLAG > 0 
				  THEN CASE WHEN x.HOLD_VALUE > 0 
							THEN CASE WHEN HOLD_VALUE > 0 THEN HOLD_VALUE ELSE 1 END 
						    END 
				  ELSE 
						ISNULL(x.OLD_HOLD_FLAG, 0) * -1
			 END 
		END
FROM 
(
	SELECT TOP 1 ISNULL(f.HOLD_FLAG,0) OLD_HOLD_FLAG, f.PV_NO, f.PV_YEAR, f.HOLD_BY, X.HOLD_FLAG, X.HOLD_VALUE, X.DOC_NO, X.DOC_YEAR 
	FROM TB_R_PV_H f 
	JOIN (SELECT @@DOC_NO DOC_NO, @@DOC_YEAR DOC_YEAR, @@HoldFlag HOLD_FLAG, @@HoldValue HOLD_VALUE) X 
	  ON f.PV_NO = X.DOC_NO AND f.PV_YEAR = X.DOC_YEAR 
) X 

UPDATE TB_R_PV_H SET 
  HOLD_FLAG = @@NEW_HOLD_FLAG 
, HOLD_BY = @@UNAME 
, CHANGED_BY = @@UNAME
, CHANGED_DATE = GETDATE() 
WHERE PV_NO = @@DOC_NO AND PV_YEAR = @@DOC_YEAR 

SELECT CASE WHEN ISNULL(@@NEW_HOLD_FLAG ,0) > 0 THEN 0 ELSE 1 END [FLAG]; 