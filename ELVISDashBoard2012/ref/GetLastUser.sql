-- GetLastUser
/*
DECLARE 
	@reffNo NUMERIC(20),
	@userName VARCHAR(20),
	@mode VARCHAR(50);
*/	
SELECT TOP 1 
	PROCCED_BY 
FROM TB_R_DISTRIBUTION_STATUS a 
WHERE REFF_NO =  @reffNo
AND (
		(
		   @mode = 'Reject' AND (a.PROCCED_BY <> @userName) and (ACTUAL_DT IS NULL)
		)
	OR 
		(
			@mode = 'Direct' AND (ACTUAL_DT IS NULL)
		
		)
	OR 
		(	
			@mode= 'Hold' AND (PLAN_DT IS NULL)
		)
	OR 
		(
			@mode = 'Cancel'
		)
	)
ORDER BY STATUS_CD;	 