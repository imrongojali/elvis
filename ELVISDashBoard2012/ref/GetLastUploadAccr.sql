-- GetLastUploadAccr
select PROCESS_ID as [Pid]
	, SEQ_NO as [SequenceNumber]
	, CASE WHEN PV_TYPE_CD != '2' THEN ACTIVITY_DES
		ELSE LEFT(ACTIVITY_DES, 35) + '... (' + CAST(SUSPENSE_NO_OLD AS VARCHAR) + ')'
	END [ActivityDescription]
	, WBS_NO_OLD as [WbsNumber]
	, w.[Description] as [WbsDesc]
	, PV_TYPE_CD as [PVTypeCd]
	, PV_TYPE_NAME as [PVType]
	, COST_CENTER as [CostCenterCode]
	, CURRENCY_CD as [CurrencyCode]
	, AMOUNT as [Amount]
	, AMOUNT_IDR as [AmountIdr]
	, SUSPENSE_NO_OLD as [SuspenseNo]
	, BOOKING_NO as [BookingNo]
from TB_T_ACCR_LIST_D d
left join (select WbsNumber
				, [Description]
				, ROW_NUMBER() over (PARTITION BY WbsNumber ORDER BY [YEAR] desc, [Description]) RN
			from vw_WBS) w
	on d.WBS_NO_OLD = w.WbsNumber and w.RN = 1
WHERE PROCESS_ID = @pid