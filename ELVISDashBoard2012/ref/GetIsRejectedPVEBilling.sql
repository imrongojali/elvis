﻿-- GetIsRejectedPVEBilling wo.apiyudin 05-06-2020
if exists (
	select top 1 1  from tb_r_pv_h 
	where PV_NO = @PV_NO
		and status_cd in ('17','42','43')
		and is_ebilling='Y'
)
begin 
	select 1
end 
else
begin
	select 0
end