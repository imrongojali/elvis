-- SetClusterTransactionClean

DECLARE @@filterID int = @filterID;

WITH S (K, VAL, ID) AS
(
    SELECT SYSTEM_VALUE_NUM K, SYSTEM_VALUE_TXT  VAL, ID
    FROM TB_M_SYSTEM
    WHERE SYSTEM_TYPE ='CLUSTER_TRANSACTION'
)
UPDATE M
SET SYSTEM_VALUE_TXT =
	(SELECT SUBSTRING(W.X, 2, DATALENGTH(W.X)-1) WX
        FROM
        (
            SELECT
            (
                SELECT ','+y.K  [text()]
                FROM (
                    SELECT DISTINCT CONVERT(VARCHAR(10), ITEMS) K 
					FROM dbo.fn_explode(',', S.VAL)
                ) y
                left join (
                    SELECT DISTINCT CONVERT(VARCHAR(10), ITEMS) K
                    FROM dbo.fn_explode(',', (
                        select VAL FROM S where K = @@filterID )) x
                    ) z
                    ON z.K = y.K
                WHERE z.K is null
				ORDER BY CASE WHEN ISNUMERIC(y.k) = 1 THEN CONVERT(int, y.k) ELSE 0 END 
                FOR XML PATH('')
            ) X
        ) W
    )
FROM S
JOIN TB_M_SYSTEM M ON M.ID = S.ID
WHERE S.K != @@filterID