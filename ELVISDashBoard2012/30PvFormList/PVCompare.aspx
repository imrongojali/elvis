﻿<%@ Page Title="PV Compare" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="PVCompare.aspx.cs" Inherits="ELVISDashBoard._30PvFormList.PVCompare" %>

<%@ MasterType VirtualPath="~/MasterPage/GeneralMaster.Master" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxUploadControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Data.Linq" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="Common" Namespace="Common.Control" TagPrefix="common" %>
<%@ Register Src="../UserControl/FormHint.ascx" TagName="FormHint" TagPrefix="fh" %>
<asp:Content ID="preX" ContentPlaceHolderID="pre" runat="server">

    <script src="../App_Themes/BMS_Theme/Script/VoucherForm.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshParent() {
           <%-- var openerWindow = window.opener.location.toString();
            var pv_no = document.getElementById("<%= lblPVNo.ClientID %>");
            var pv_year = document.getElementById("<%= lblPVYear.ClientID %>");

            if (openerWindow.indexOf('pv_no') <= -1) {
                openerWindow = openerWindow + '?pv_no=' + pv_no.value + '&pv_year=' + pv_year.value;
                alert(openerWindow);
                window.opener.location.href = openerWindow;
            } else {
                alert(openerWindow);
                window.opener.location.href = window.opener.location;
            }--%>
        }
    </script>
</asp:Content>
<asp:Content ID="sectionHeader" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .otvlabel {
            float: left;
            width: 105px;
            font-size: 10px;
            display: block;
        }

        .col1 {
            width: 180px;
            float: left;
            display: block;
            clear: none;
        }

        .col2 {
            width: 160px;
            float: left;
            display: block;
            clear: none;
        }

        .btRight {
            float: right;
            width: 90%;
            text-align: right;
            vertical-align: middle;
            display: inline;
        }

        .alert {
            padding: 20px;
            background-color: greenyellow;
            color: black;
        }
    </style>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:HiddenField runat="server" ID="hidPageMode" />
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="PV Form" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:HiddenField runat="server" ID="hdExpandFlag" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdLastFocusedID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdSelectedVendorCode" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdGridFocusedColumn" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hdFinanceAccessFlag" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidReloadOpener" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidHasWorklist" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hidOneTimeVendorValid" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="hinPVNo" />
    <asp:HiddenField runat="server" ID="hidVendorCode" />
   <%-- <asp:HiddenField runat="server" ID="hidTotalgriddetail" />--%>
    <br />

    <asp:UpdatePanel runat="server" ID="pnupdateLitMessage">
        <ContentTemplate>
            <div class="alert">
                <strong>MATCHED!</strong> your PV and invoice does not contain taxable transaction, please continue by clicking [Save Comparison] to <strong>enable submit process</strong>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="pnupdateGrid">
        <ContentTemplate>
            <asp:Literal runat="server" ID="Literal1"></asp:Literal>
            <div>
                <dx:ASPxLabel ID="ASPxLabel5" runat="server" Text="PV Number : "></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblPVNo" runat="server" Width="80px" Font-Bold="true"></dx:ASPxLabel>
                &nbsp;
                        <dx:ASPxLabel ID="ASPxLabel7" runat="server" Text="Vendor Name : "></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblVendorName" runat="server" Font-Bold="true"></dx:ASPxLabel>

                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
                        <dx:ASPxLabel ID="lblVal" runat="server" Text="Validation Summary : "></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblInvoice" runat="server" Text="INVOICE " Font-Bold="true"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblTotInvoice" runat="server" Font-Bold="true"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lbl1" runat="server" Text="|"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblpas" runat="server" Text="MATCHED " Font-Bold="true"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblTotPassed" runat="server" Font-Bold="true"></dx:ASPxLabel>
                <dx:ASPxLabel ID="ASPxLabel1" runat="server" Text="|"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblIssue" runat="server" Text="UNMATCHED " Font-Bold="true"></dx:ASPxLabel>
                <dx:ASPxLabel ID="lblTotIssue" runat="server" Font-Bold="true"></dx:ASPxLabel>

                <dx:ASPxLabel ID="lblPVYear" runat="server" Font-Bold="true" Visible="false"></dx:ASPxLabel>
            </div>
            <table cellpadding="2px" cellspacing="0" border="0" style="text-align: left" width="100%">
                <tr>
                    <td>
                        <dx:ASPxGridView ID="gridCompare"
                            runat="server"
                            Width="100%"
                            ClientInstanceName="gridCompare"
                            KeyFieldName="ROW_NUMBER" AutoGenerateColumns="False"
                            OnHtmlRowCreated="gridCompare_HtmlRowCreated"
                            OnCustomCallback="grid_CustomCallback"
                            EnableCallBacks="false"
                            Styles-AlternatingRow-CssClass="even"
                            OnPageIndexChanged="gridCompare_PageIndexChanged">
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="VALIDASI_DATA" Caption="Validation" Width="90px" VisibleIndex="1">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="INVOICE_NO" Caption="Invoice Number" Width="110px" VisibleIndex="2">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="TAX_NO" Caption="Tax Invoice" Width="150px" VisibleIndex="3">
                                    <DataItemTemplate>
                                        <asp:HyperLink runat="server" ID="hyptax1" NavigateUrl="#" />
                                        <asp:Label runat="server" ID="lbltax0" />
                                        <asp:Label runat="server" ID="lbltaxSparator" />
                                        <asp:HyperLink runat="server" ID="hyptax2" NavigateUrl="#" />
                                        <asp:Label runat="server" ID="lbltax1" />
                                    </DataItemTemplate>
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="DPP" Caption="DPP Value" VisibleIndex="4" Width="100px" PropertiesTextEdit-DisplayFormatString="###,###,###.##">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="PPN" Caption="PPN Value" VisibleIndex="5" Width="100px" PropertiesTextEdit-DisplayFormatString="###,###,###.##">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="PPH" Caption="PPH Value" VisibleIndex="6" Width="100px" PropertiesTextEdit-DisplayFormatString="###,###,###.##">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn FieldName="VALIDASI_MESSAGE" Caption="Validation Message" VisibleIndex="7" Width="150px">
                                    <Settings AutoFilterCondition="Contains" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <SettingsBehavior AllowDragDrop="true" AllowSort="true"
                                ColumnResizeMode="Control" />
                            <SettingsEditing Mode="Inline" />
                            <Settings
                                UseFixedTableLayout="True" ShowVerticalScrollBar="True"
                                VerticalScrollableHeight="360" />
                            <Border BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px" />
                            <Styles Header-BackColor="#d2e4eb">
                                <Header BackColor="#D2E4EB" HorizontalAlign="Center" VerticalAlign="Middle"></Header>
                            </Styles>
                            <Settings ShowStatusBar="Visible" />
                            <SettingsPager Visible="false" />
                            <Settings ShowFilterRow="true" />
                            <Templates>
                                <StatusBar>
                                    <div style="text-align: left;">
                                        Records per page: 
											<select onchange="gridCompare.PerformCallback(this.value);">
                                                <option value="5" <%# WriteSelectedIndexCompare(5) %>>5</option>
                                                <option value="10" <%# WriteSelectedIndexCompare(10) %>>10</option>
                                                <option value="15" <%# WriteSelectedIndexCompare(15) %>>15</option>
                                                <option value="20" <%# WriteSelectedIndexCompare(20) %>>20</option>
                                                <option value="25" <%# WriteSelectedIndexCompare(25) %>>25</option>
                                            </select>&nbsp;														
												<a title="First" href="JavaScript:gridCompare.GotoPage(0);">&lt;&lt;</a> &nbsp; 
												<a title="Prev" href="JavaScript:gridCompare.PrevPage();">&lt;</a> &nbsp;
													Page
                                        <input type="text" onchange="gridCompare.GotoPage(parseInt(this.value, 10) - 1);"
                                            onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridCompare.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                            value="<%# (gridCompare.PageIndex >= 0)? gridCompare.PageIndex + 1 : 1 %>"
                                            style="width: 20px" />
                                        of <%# gridCompare.PageCount%> &nbsp;
                                                        <%# (gridCompare.VisibleRowCount > 1)? "(" + gridCompare.VisibleRowCount + " Items)&nbsp;": "" %>
                                        <a title="Next" href="JavaScript:gridCompare.NextPage();">&gt;</a> &nbsp;
												<a title="Last" href="JavaScript:gridCompare.GotoPage(<%# gridCompare.PageCount - 1 %>);">&gt;&gt;</a> &nbsp; 
                                    </div>
                                </StatusBar>
                            </Templates>
                        </dx:ASPxGridView>

                        <asp:LinqDataSource ID="LinqPVCompare" runat="server"
                            ContextTypeName="DataLayer.Model.ELVIS_DBEntities" EntityTypeName=""
                            OnSelecting="LinqPVCompare_Selecting"
                            Select="new (ID, PV_NO, VALIDASI_DATA, INVOICE_NO, TAX_NO, DPP, PPN, PPH, VALIDASI_MESSAGE, ROW_NUMBER)"
                            TableName="vw_efb_compare_faktur_list">
                        </asp:LinqDataSource>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="text-align: right">
                        <br />
                        <%--<asp:Button runat="server" ID="btRefreshCompare" Text="Refresh" OnClientClick="loading();" OnClick="evt_btRefresh_onClick" />--%>
                        <asp:Button runat="server" ID="btRefreshCompare" Text="Refresh" OnClientClick="loading();" OnClick="btRefreshCompare_Click" />
                    </td>
                    <td style="text-align: right">
                        <br />
                        <asp:Button runat="server" ID="btnSaveCompare" Text="Save Comparison" OnClientClick="loading();" OnClick="btnSaveCompare_Click" CssClass="xlongButton" SkinID="xlongButton" />
                    </td>
                    <td style="text-align: right">
                        <br />
                        <asp:Button runat="server" ID="btnSaveRemoveIssue" Text="Remove Invoice with Unmatched then Save Comparison" OnClick="btnSaveRemoveIssue_Click" OnClientClick="if(!confirm('Are you sure you Remove this issue?')) return false;loading();" CssClass="x3longButton" SkinID="x3longButton" />
                    </td>
                    <td style="text-align: right">
                        <br />
                        <asp:Button runat="server" ID="btnCloseCompare" Text="Close" OnClick="btnCloseCompare_Click" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
