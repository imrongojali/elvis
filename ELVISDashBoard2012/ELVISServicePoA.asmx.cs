﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using BusinessLogic.PoA;

namespace ELVISDashBoard
{
    /// <summary>
    /// Summary description for WebService1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ELVISServicePoA : System.Web.Services.WebService
    {
        private PoALogic _logic = new PoALogic();
        public const string MESG_SUCCESS = PoALogic.WS_MESG_SUCCESS;
        public const string MESG_ERROR = PoALogic.WS_MESG_ERROR;

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public string RedirectWorklist(string POANumber, string username)
        {
            string errMesg = MESG_SUCCESS;
            try
            {
                _logic.RedirectWorklist(POANumber, username);
            }
            catch (Exception ex)
            {
                errMesg = MESG_ERROR + " : " + ex.Message;
            }

            return errMesg;
        }

        [WebMethod]
        public string ProcessAllUnprocessPoA(string username)
        {
            string errMesg = MESG_SUCCESS;
            try
            {
                
                _logic.ProcessAllUnprocessPoA(username);
            }
            catch (Exception ex)
            {
                errMesg = MESG_ERROR + " : " + ex.Message;
            }

            return errMesg;
        }

    }
}
