﻿using System.Collections.Generic;
using System.Text;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using Common.Messaging;

namespace ELVISDashBoard.presentationHelper
{
    public class ScreenHelper
    {
        public const byte INF = 0;
        public const byte WRN = 1;
        public const byte ERR = 2;
        private LogicFactory logic = LogicFactory.Get();
        protected readonly Common.Function.GlobalResourceData _globalResource = new Common.Function.GlobalResourceData();
        protected readonly MessageLogic _m = LogicFactory.Get().Msg;
        private string ScreenName;
        private HttpSessionState som;
        protected Literal MessageControlLiteral { set; get; }
        protected HiddenField HiddenMessageExpandFlag { set; get; }
        
        public ScreenHelper(string name, HttpSessionState session, Control lit, Control hiddenExpand)
        {
            ScreenName = name;
            MessageControlLiteral = (Literal) lit;
            HiddenMessageExpandFlag = (HiddenField)hiddenExpand;
            som = session;
        }

        public List<ScreenMessage> Messages
        {
            get
            {
                string n = ScreenName + typeof(ScreenMessage);
                List<ScreenMessage> lstMessages = (List<ScreenMessage>) som[n];
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    som.Add(n, lstMessages);
                    HiddenMessageExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }

        protected void postScreenMessage(ScreenMessage[] messages)
        {
            postScreenMessage(messages, false, true);
        }
        protected void postScreenMessage(ScreenMessage[] messages, bool instantDisplay)
        {
            postScreenMessage(messages, false, instantDisplay);
        }
        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = Messages;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    lstMessage.Insert(0, messages[i]);
                }
            }
            updateScreenMessages();
        }

        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = Messages;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                ScreenMessage msg = lstMessages[0];
                StringBuilder b = new StringBuilder();
                string expandFlag = HiddenMessageExpandFlag.Value;

                string messageBoxClass = "message-info";
                string messageBoxSpanClass = "message-span-info";
                if (msg.Status == ScreenMessage.STATUS_WARNING)
                {
                    messageBoxClass = "message-warning";
                    messageBoxSpanClass = "message-span-warning";
                }
                else if (msg.Status == ScreenMessage.STATUS_ERROR)
                {
                    messageBoxClass = "message-error";
                    messageBoxSpanClass = "message-span-error";
                }

                if (expandFlag.Equals("true"))
                {
                    messageBoxClass += " expanded-message-box";
                }
                b.AppendFormat("<div id='messageBox' class='{0}'>\r\n", messageBoxClass);
                b.AppendFormat("      <span class='{0}'>{1}</span>\r\n", messageBoxSpanClass, msg.Message);

                if (cntMessages > 1)
                {
                    string txtMore = "more";
                    if (expandFlag.Equals("true"))
                    {
                        txtMore = "close";
                    }
                    b.AppendLine("      <span class='message-span-more'>");
                    b.AppendFormat("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore);
                    b.AppendLine("      </span>");
                }

                b.AppendLine("</div>");

                if (expandFlag.Equals("false"))
                {
                    b.AppendLine("<div id='messageBox-all' style='display: none'>");
                }
                else
                {
                    b.AppendLine("<div id='messageBox-all'>");
                }

                b.AppendLine("  <ul>");
                string listClass;
                for (int i = 1; i < cntMessages; i++)
                {
                    msg = lstMessages[i];
                    listClass = "message-detail-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        listClass = "message-detail-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        listClass = "message-detail-error";
                    }

                    b.AppendFormat("<li class='{0}'>", listClass);
                    b.AppendLine("      <span>" + msg.Message + "</span>");
                    b.AppendLine("</li>");
                }
                b.AppendLine("  </ul>");
                b.AppendLine("</div>");

                MessageControlLiteral.Text = b.ToString();
                MessageControlLiteral.Visible = true;
            }
            else
            {
                MessageControlLiteral.Text = "";
                MessageControlLiteral.Visible = false;
            }
        }

        public void clearScreenMessage()
        {
            Messages.Clear();
            updateScreenMessages();
        }

        private void PostMsg(bool instant, bool clearfirst, byte sts, string id, params object[] x)
        {
            string m = _globalResource.GetResxObject("Message", id);
            if (x != null && x.Length > 0)
            {
                m = string.Format(m, x);
            }
            postScreenMessage(
                new ScreenMessage[] 
                {
                    new ScreenMessage(sts, m)
                }, clearfirst, instant);
        }

        public void PostLaterOn(byte sts, string id, params object[] x)
        {
            PostMsg(false, true, sts, id, x);
        }

        public void PostNowPush(byte sts, string id, params object[] x)
        {
            PostMsg(true, false, sts, id, x);
        }

        public void PostNow(byte sts, string id, params object[] x)
        {
            PostMsg(true, true, sts, id, x);
        }

        public void PostLater(byte sts, string id, params object[] x)
        {
            PostMsg(false, false, sts, id, x);
        }

        public void Now(string id, params object[] x)
        {
            
        }
        public void Later(string id, params object[] x)
        {
        }
        public void Push(string id, params object[] x)
        {
        }
        public void Put(string id, params object[] x)
        {
        }
    }
}