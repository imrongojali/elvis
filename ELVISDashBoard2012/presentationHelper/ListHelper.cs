﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;

namespace ELVISDashBoard.presentationHelper
{
    public static class ListHelper
    {

        public static void Colorific(ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "STATUS")
            {
                ListHelper.Classy(e);
            }
            else if (e.DataColumn.FieldName == "ACTUAL_DT")
            {
                ListHelper.DistriBusy(e, e.GetValue("SKIP_FLAG").Int());
            }
        }

        public static void Classy(ASPxGridViewTableDataCellEventArgs e)
        {
            int v = e.CellValue.Int(0);
            switch (v)
            {
                case 1: e.Cell.CssClass = "Approved"; break;
                case 2: e.Cell.CssClass = "Rejected"; break;
                default: e.Cell.CssClass = "Waiting"; break;
            }
        }

        public static void DistriBusy(ASPxGridViewTableDataCellEventArgs e, int skip_flag = 0)
        {
            switch (skip_flag)
            {
                case 1:
                    e.Cell.CssClass = "Skipped"; 
                    break;
                case 2:
                    e.Cell.CssClass = "Concurred";
                    break;
            }
        }
    }

    public class SelectableDropDownHelper 
    {
        private ASPxDropDownEdit dx = null;
        private string box = "";
        
        public SelectableDropDownHelper(ASPxDropDownEdit e, string InnerListBox)
        {
            dx = e;
            box = InnerListBox;
        }

        public void Clear()
        {
            ASPxListBox divBox = (ASPxListBox)dx.FindControl(box);
            if (divBox == null) return;
            divBox.UnselectAll();
            dx.Text = String.Empty;
        }
        
        public string SelectedValues(string defaultDiv="", bool isFinance=false)
        {
            ASPxListBox divBox =  dx.FindControl(box) as ASPxListBox;
            if (divBox == null) return defaultDiv;
            SelectedItemCollection selectedItems = divBox.SelectedItems;
            List<string> l = new List<string>();
            if ((selectedItems != null) && (selectedItems.Count > 0))
            {
                ListEditItem item;
                if (selectedItems.Count == divBox.Items.Count && isFinance)
                {

                }
                else
                {
                    for (int i = selectedItems.Count - 1; i >= 0; i--)
                    {
                        item = selectedItems[i];
                        l.Add(item.Value.str());
                    }
                }
            } else {
                if (!isFinance)
                {
                    return defaultDiv;
                }
            }
            return CommonFunction.CommaJoin(l, ";");
        }

        public void SelectAll()
        {
            ASPxListBox divBox = dx.FindControl(box) as ASPxListBox;
            if (divBox == null) return;
            divBox.SelectAll();
            string s = "";
            for (int i = 0; i < divBox.SelectedItems.Count; i++)
            {
                s = s + ((i > 0) ? ";" : "") + divBox.SelectedItems[i].Text;

            }
            dx.Text = s;
        }


    }
}