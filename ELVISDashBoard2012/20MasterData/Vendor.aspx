﻿<%@ Page Title="Vendor" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="Vendor.aspx.cs" Inherits="ELVISDashBoard._20MasterData.Vendor" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <script src="../App_Themes/BMS_Theme/Script/validateNumber.js" type="text/javascript"></script>
    <style type="text/css">
        .searchRow 
        {
            margin: 0px 0px 0px 0px;
            padding: 2px 0px 0px 0px;
            width: 100%;
            display: block;
            float: none;
            clear: both;
        }
        .readOnlyFields
        {
            background-image: none;
            background-color: #DCDCDC;
            color: Black;
        }
        .crit  
        {
            margin: 0px 0px 0px 0px;
            padding: 0px 0px 0px 0px;
            width: 49%;
            text-align: left;
        }
        .label 
        {
            width: 100px;
            display: inline-block;
            float: left;
        }
        
    </style>
    <script type="text/javascript">
        function IsNotEmpty(s, e) {
            if (!s) {
                return false
            } else {
                return true
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="Vendor Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <div class="searchRow">
                    <div class="lefty crit">
                        <div class="label">Vendor Code</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtVendor" MaxLength="10" Columns="10" AutoPostBack="false" />
                        </div>
                    </div>
                    <div class="righty crit">
                        <div class="label">Vendor Name</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtVendorName" MaxLength="40" Columns="40" AutoPostBack="false" />
                        </div>
                    </div>
                </div>
                <div class="searchRow">
                    <div class="lefty crit">
                        <div class="label">
                            Vendor Group</div>
                        <div class="rowwarphalfLeft">
                            <asp:DropDownList runat="server" ID="ddlVendorGroupCode" AutoPostBack="false">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="righty crit">
                        <div class="label">
                            Search Terms</div>
                        <div class="rowwarphalfLeft">
                            <asp:TextBox runat="server" ID="txtSearchTerms" MaxLength="10" Columns="10" AutoPostBack="false" />
                        </div>
                    </div>
                </div>
                <div class="searchRow">
                    <div class="lefty crit">
                        <div class="label">
                            Division</div>
                        <div class="lefty">
                            <dx:ASPxComboBox runat="server" ID="cbDivision" 
                                DataSourceID="dsDivision"
                                TextFormatString="{0}"
                                EnableViewState="true"
                                IncrementalFilteringMode="Contains"  TextField="COMBO_NAME" ValueField="COMBO_CD" >
                                <Columns>
                                    <dx:ListBoxColumn FieldName="COMBO_NAME" Caption="Name" Width="200px"/>
                                    <dx:ListBoxColumn FieldName="COMBO_CD" Caption="Code" Width="60px"/>
                                </Columns   >
                            </dx:ASPxComboBox>
                        </div>
                    </div>
                    <div class="righty crit" id="suspenseCountDiv" runat="server" clientidmode="Static">
                        <div class="label">Suspense Count</div>
                        <div class="rowwarphalfLeft">
                            <div class="lefty">
                                <asp:TextBox runat="server" ID="txtSuspenseCount" MaxLength="9" Columns="5" />
                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSuspenseCount" FilterType="Numbers" ID="filSusFrom" Enabled="true" />
                            </div>
                            <div class="lefty" style="padding-left:10px;padding-right:10px;"> to </div>
                            <div class="lefty">
                                <asp:TextBox runat="server" ID="txtSuspenseCountTo" MaxLength="9" Columns="5" />
                                <cc1:FilteredTextBoxExtender runat="server" TargetControlID="txtSuspenseCountTo" FilterType="Numbers" ID="filSusTo" Enabled="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" />
                    </div>
                </div>
            </div>
            <div class="rowbtn" id="SaveCancelButtons">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>

            
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False" 
                Settings-VerticalScrollableHeight="320"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                KeyFieldName="VENDOR_CD" EnableCallBacks="false"
                OnPageIndexChanged="gridGeneralInfo_PageIndexChanged" Styles-AlternatingRow-CssClass="even">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="30px">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vendor Code" FieldName="VENDOR_CD" VisibleIndex="2" 
                        Width="90px">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridVendorCode" Width="88px" Column="10" MaxLength="10"
                            ReadOnly="true"
                                ReadOnlyStyle-Border-BorderStyle="None"/>                                
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vendor Name" FieldName="VENDOR_NAME" Width="325px"
                        VisibleIndex="3" CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridVendorName" Width="323px" Column="30" MaxLength="40">
                            </dx:ASPxTextBox>
                        </EditItemTemplate>
                        <CellStyle HorizontalAlign="Left">
                        </CellStyle>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vendor Group" VisibleIndex="4" Width="140px"
                        FieldName="VENDOR_GROUP_NAME">
                        <EditItemTemplate />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Search Terms" VisibleIndex="5" Width="75px" FieldName="SEARCH_TERMS">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridSearchTerms" Width="72px" Column="10" MaxLength="10">
                            </dx:ASPxTextBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Vendor Group" VisibleIndex="6" FieldName="VENDOR_GROUP_CD"
                        Visible="false">
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Division" VisibleIndex="7" Width="200px" FieldName="CONTACT_PERSON">
                        <CellStyle HorizontalAlign="Left" />
                        <DataItemTemplate>
                            <asp:Literal ID="litDivisionName" runat="server" />
                        </DataItemTemplate>
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridDivision" Width="196px" 
                                TextFormatString="{0}"
                                IncrementalFilteringMode="Contains"  >
                                <Columns>
                                <dx:ListBoxColumn FieldName="COMBO_NAME" Caption="Name" Width="200px"/>
                                <dx:ListBoxColumn FieldName="COMBO_CD" Caption="Code" Width="60px"/>
                                </Columns>
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Suspense Count" VisibleIndex="8" Width="80px" FieldName="SUSPENSE_COUNT" Visible="false">
                        <CellStyle HorizontalAlign="Right"/>
                        <EditItemTemplate />
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewBandColumn Caption="Created" VisibleIndex="10">
                        <Columns>
                            <dx:GridViewDataTextColumn Caption="Date" FieldName="CREATED_DT" VisibleIndex="0"
                                Width="135px">
                                <PropertiesTextEdit DisplayFormatString="{0:dd MMM yyyy HH:mm}"/>
                                <EditItemTemplate/>
                            </dx:GridViewDataTextColumn>
                            <dx:GridViewDataTextColumn Caption="By" FieldName="CREATED_BY" VisibleIndex="1" Width="100px">
                                <EditItemTemplate/>
                            </dx:GridViewDataTextColumn>
                        </Columns>
                    </dx:GridViewBandColumn>
                    
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <Cell HorizontalAlign="Center" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div id="divPaging" style="text-align: left;">
                            Records per page:
                            <asp:DropDownList ID="RecordPerPageSelect" runat="server" ClientIDMode="Static" 
                                OnInit="RecordPerPageSelect_Init"
                                AutoPostBack="true"
                                OnSelectedIndexChanged="RecordPerPageSelect_SelectedIndexChanged"/>
                            &nbsp;
                            <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">
                                &lt;&lt;</a> &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a>
                            &nbsp; Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>" style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp; 
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%> 
                            <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp;
                            <a title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>

            
            <asp:LinqDataSource ID="LinqDataSource1" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" 
                OnSelecting="LinqDataSource1_Selecting" >
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="dsDivision" runat="server" OnSelecting="dsDivision_Selecting" />
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click" OnClientClick="loading()"/>
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
