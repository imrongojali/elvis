﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using BusinessLogic._20MasterData;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridLookup;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._20MasterData
{
    public partial class User : BaseCodeBehind
    {
        #region Init
        private const string mySelf = "User";
        readonly UserRoleLogic myLogic = new UserRoleLogic();

        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2003");
        
        //readonly MessageLogic _m = new MessageLogic();
        
        bool IsEditing;

        private SortedDictionary<string, object> grid = new SortedDictionary<string, object>();

        private static readonly string[] aFields = {"REG_NO", "USERNAME", "TITLE", "FIRST_NAME", "LAST_NAME", 
                                "EMAIL", "POSITION_ID", "SECTION_ID", "DEPARTMENT_ID", 
                                "DIVISION_ID", "GROUP_CD", "ROLE_ID"};

        private static readonly string[] aColumns = {"txtGridRegNo", "txtGridUserName", "txtGridTitle", "txtGridFirstName", "txtGridLastName", 
                                  "txtGridEmail", "ddlGridPosition", "ddlGridSection", "ddlGridDepartment",
                                  "ddlGridDivision", "ddlGridGroup", "loGridRoleId"};        

       

        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }

        #region Getter Setter for Data List
        private List<UserRoleData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<UserRoleData>();
                }
                else
                {
                    return (List<UserRoleData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion


        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }

            hidScreenID.Value = _ScreenID;
            lit = litMessage;
            
            if (!IsPostBack)
            {
                PrepareLogout();

                gridGeneralInfo.DataSourceID = null;

                GenerateComboData(ddlPosition, "POSITION", true);
                GenerateComboData(ddlSection, "SECTION", true);
                GenerateComboData(ddlDivision, "DIVISION", true);
                GenerateComboData(ddlDepartment, "DEPARTMENT", true);
                
                SetFirstLoad();

                AddEnterComponent(txtUserName);
                AddEnterComponent(txtRegNo);
                AddEnterComponent(ddlPosition);
                AddEnterComponent(ddlSection);
                AddEnterComponent(ddlDepartment);
                AddEnterComponent(ddlDivision);
                AddEnterComponent(txtRoleId);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }

        protected void FillCombo(DropDownList d, List<ComboClassData> l)
        {
            d.DataSource = l;
            d.DataTextField = "COMBO_NAME";
            d.DataValueField = "COMBO_CD";
            d.DataBind();
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);

            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            btnClose.Visible = true;
        }
        #endregion

        #region Grid

        private string gv(int row, string fieldname)
        {
            return Convert.ToString(gridGeneralInfo.GetRowValues(row, fieldname));
        }

        private UserRoleData Gather(int row)
        {

            UserRoleData r = new UserRoleData();
            if (row >= 0)
            {
                string k = gv(row, "USER_DIV_POS");
                string u = "";
                string d = "";
                string p = "";
                SplitKeys(k,ref u, ref d, ref p);

                r.REG_NO = gv(row, "REG_NO");
                r.USERNAME = gv(row, "USERNAME");
                r.TITLE = gv(row, "TITLE");
                r.FIRST_NAME = gv(row, "FIRST_NAME");
                r.LAST_NAME = gv(row, "LAST_NAME");
                r.EMAIL = gv(row, "EMAIL");
                r.POSITION_ID = p; 
                r.SECTION_ID = gv(row, "SECTION_ID");
                r.DIVISION_ID = d; 
                r.DEPARTMENT_ID = gv(row, "DEPARTMENT_ID");
                r.GROUP_CD = Convert.ToInt32(gridGeneralInfo.GetRowValues(row, "GROUP_CD"));
                r.ROLE_ID = gv(row, "ROLE_ID");
            }
            else
            {
                r.REG_NO = (grid["REG_NO"] as ASPxTextBox).Text;
                r.USERNAME = (grid["USERNAME"] as ASPxTextBox).Text;
                r.TITLE = (grid["TITLE"] as ASPxTextBox).Text;
                r.FIRST_NAME = (grid["FIRST_NAME"] as ASPxTextBox).Text;
                r.LAST_NAME = (grid["LAST_NAME"] as ASPxTextBox).Text;
                r.EMAIL = (grid["EMAIL"] as ASPxTextBox).Text;
                r.POSITION_ID = Convert.ToString((grid["POSITION_ID"] as ASPxComboBox).Value);
                r.SECTION_ID = Convert.ToString((grid["SECTION_ID"] as ASPxComboBox).Value);
                r.DIVISION_ID = Convert.ToString((grid["DIVISION_ID"] as ASPxComboBox).Value);
                r.DEPARTMENT_ID = Convert.ToString((grid["DEPARTMENT_ID"] as ASPxComboBox).Value);
                r.GROUP_CD = Convert.ToInt32((grid["GROUP_CD"] as ASPxComboBox).Value);
                r.ROLE_ID = Convert.ToString( (grid["ROLE_ID"] as ASPxGridLookup).Value);
            }          

            return r;
        }

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data

            
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoGeneralInfo = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex, gridGeneralInfo.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));
            }
            #endregion
            #region rowtype edit
            if (!IsEditing || !(e.RowType == GridViewRowType.InlineEdit)) return;

            #region init

            Mark();
            ASPxTextBox first = (grid["REG_NO"] as ASPxTextBox);
            
            #endregion

            GenerateComboData(grid["POSITION_ID"] as ASPxComboBox, "POSITION", false);
            GenerateComboData(grid["SECTION_ID"] as ASPxComboBox, "SECTION", false);
            GenerateComboData(grid["DEPARTMENT_ID"] as ASPxComboBox, "DEPARTMENT", false);
            GenerateComboData(grid["DIVISION_ID"] as ASPxComboBox, "DIVISION", false);
            GenerateComboData(grid["GROUP_CD"] as ASPxComboBox, "GROUP_CD", false);
            
            switch (PageMode)
            {
                case Common.Enum.PageMode.Add:
                    first.Focus();
                    break;
                case Common.Enum.PageMode.Edit:
                    int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                    if (_VisibleIndex == e.VisibleIndex)
                    {
                        UserRoleData u = Gather(_VisibleIndex);
                        
                        (grid["USERNAME"] as ASPxTextBox).Text = u.USERNAME;
                        (grid["REG_NO"] as ASPxTextBox).Text = u.REG_NO;
                        (grid["USERNAME"] as ASPxTextBox).Text = u.USERNAME;
                        (grid["TITLE"] as ASPxTextBox).Text = u.TITLE;
                        (grid["FIRST_NAME"] as ASPxTextBox).Text = u.FIRST_NAME;
                        (grid["LAST_NAME"] as ASPxTextBox).Text = u.LAST_NAME;
                        (grid["EMAIL"] as ASPxTextBox).Text = u.EMAIL;

                        (grid["POSITION_ID"] as ASPxComboBox).Value = u.POSITION_ID;
                        (grid["SECTION_ID"] as ASPxComboBox).Value = u.SECTION_ID;
                        (grid["DEPARTMENT_ID"] as ASPxComboBox).Value = u.DEPARTMENT_ID;
                        (grid["DIVISION_ID"] as ASPxComboBox).Value = u.DIVISION_ID;
                        (grid["GROUP_CD"] as ASPxComboBox).Value = u.GROUP_CD;
                        (grid["ROLE_ID"] as ASPxGridLookup).Value = u.ROLE_ID;
                        
                        ASPxTextBox key = (grid["USERNAME"] as ASPxTextBox);

                        key.ReadOnly = true;
                        first.Focus();
                    }
                    break;
                default:
                    break;
            }

            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            gridGeneralInfo.DataBind();
        }

        protected void linqDs_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string code = txtUserName.Text;
            
            ListInquiry = myLogic.SearchInqury(txtUserName.Text, txtRegNo.Text, 
                ddlPosition.Value as string,
                 ddlSection.Value as string, 
                 ddlDepartment.Value as string, 
                 ddlDivision.Value as string, txtRoleId.Text);
            e.Result = ListInquiry;
        }

        protected void dsRoles_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            e.Result = myLogic.RoleList();
        }

        #endregion
        #endregion


        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtUserName.Text = "";
            txtRegNo.Text = "";
            txtRoleId.Text = "";

            ddlDepartment.SelectedIndex = 0;
            ddlPosition.SelectedIndex = 0;
            ddlSection.SelectedIndex = 0;
            ddlDivision.SelectedIndex = 0;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count > 0)
            {
                lblConfirmationDelete.Text = Nagging("MSTD00008CON", "delete"); 
                
                ConfirmationDeletePopUp.Show();
            }
            else 
            {
                #region Error
                
                Nag("MSTD00009WRN");
                #endregion
            }
        }

        private bool SplitKeys(string k, ref string username, ref string div, ref string pos)
        {
            string[] a;
            div = "";
            pos = "";

            if (k != null)
            {
                a = k.Split(';');
                if (a.Length > 2)
                {
                    username = a[0];
                    div = a[1];
                    pos = a[2];
                    return true;
                }
            }
            return false;
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                string bPOS = "";
                string bDIV = "";
                string bUser = "";
                List<UserRoleData> l = new List<UserRoleData>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i)) { 
                        string u = Convert.ToString( gridGeneralInfo.GetRowValues(i, "USER_DIV_POS"));
                        SplitKeys(u,ref bUser, ref bDIV, ref bPOS);
                        
                        l.Add( new UserRoleData() 
                        { 
                            USERNAME = Convert.ToString( gridGeneralInfo.GetRowValues(i, "USERNAME")),
                            DIVISION_ID = bDIV,
                            POSITION_ID = bPOS
                        });
                    }
                }

                if (l.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = myLogic.DeleteRow(l);
                    if (!result)
                    {
                        Nag("MSTD00014ERR");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        Nag("MSTD00049INF", mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = linqDs.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            Mark();
            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    UserRoleData u = Gather(-1);

                    if (!LdapAuthentication.Has(u.USERNAME))
                    {
                        Nag("MSTD00099ERR", u.USERNAME);
                        return;
                    }

                    bool result = myLogic.AddRow(u, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = linqDs.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        Nag("MSTD00011INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                    }
                    else
                    {
                        string msg="", msgCode="";
                        GetLastErrMessages(myLogic.LastErr, mySelf, u.USERNAME, "Add", ref msg, ref msgCode);
                        litMessage.Text = _m.SetMessage(msg, msgCode);
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    int index = Convert.ToInt32(Session["visibleIndex"]);
                    
                    UserRoleData _u = Gather(index);
                    UserRoleData u = Gather(-1);
                    
                    bool result = myLogic.EditRow(u, _u, UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        Nag("MSTD00013INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                    }
                    else
                    {
                        Nag("MSTD00012ERR", mySelf);
                    }
                }
                #endregion
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = linqDs.ID;
                
                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            PageMode = Common.Enum.PageMode.Edit;
                            gridGeneralInfo.SettingsBehavior.AllowSort = false;
                            IsEditing = true;                            
                            Session["visibleIndex"] = i;
                            gridGeneralInfo.StartEdit(i);
                            gridGeneralInfo.Selection.UnselectAll();
                            SetEditDetail();
                            break;
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    Nag("MSTD00009WRN");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    Nag("MSTD00016WRN");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                try
                {
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName =  mySelf.ToUpper() + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("Content-Disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    try
                    {
                        this.Response.BinaryWrite(myLogic.Get(UserName, imagePath, ListInquiry));
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);
                        litMessage.Text = "Cannot get file: " + ex.Message;
                    } 
                    this.Response.End();
                   
                }
                catch (Exception Ex)
                {
                    litMessage.Text = Ex.ToString();
                    // throw Ex;
                }
            }
            else
                Nag("MSTD00018INF");
        }
        #endregion

        protected void setSearchEnabled(bool can = true)
        {
            txtUserName.Enabled = can;
            txtRegNo.Enabled = can;
            ddlPosition.Enabled = can;
            ddlSection.Enabled = can;
            ddlDivision.Enabled = can;
            ddlDepartment.Enabled = can;
            txtRoleId.Enabled = can;               
        }

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;
            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
            setSearchEnabled(false);
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref Err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
            setSearchEnabled();
        }

        #endregion

        #region Function

        private void Mark()
        {
            grid.Clear();
            for (int i = 0; i < (aFields.Length); i++)
            {
                Object o = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns[aFields[i]] as GridViewDataColumn, aColumns[i]);

                if (o == null)
                    logic.Say("Mark", "ERR: cannot find field {0} in grid of column {1}", aFields[i], aColumns[i]);
                else
                    grid.Add(aFields[i], o);
            }
        }

        private bool ValidateInputSave()
        {
            bool aOk = true;

            StringBuilder s = new StringBuilder("");

            //REGION FOR VALIDATING INPUT SAVE
            
            string[] checkIfEmpty = { "REG_NO", "USERNAME", "FIRST_NAME" };
            string[] checkDesc = { "Reg No", "User Name", "First Name" };
            
            string[] checkSelected = {"POSITION_ID", // "SECTION_ID", "DEPARTMENT_ID", 
                                         "DIVISION_ID", "GROUP_CD"};
            string[] checkSelDec = { "Position", // "Section", "Department",
                                       "Division", "Group"};

            for (int i = 0; i < checkIfEmpty.Length; i++)
            {
                if (String.IsNullOrEmpty( (grid[checkIfEmpty[i]] as ASPxTextBox).Text))
                {
                    s.AppendLine( Nagging("MSTD00017WRN",checkDesc[i]));
                }
            }

            if (String.IsNullOrEmpty( (grid["ROLE_ID"] as ASPxGridLookup).Value as string))
            {
                s.AppendLine(Nagging("MSTD00017WRN", "Role"));
            }

            if (aOk)
                for (int i = 0; i < checkSelected.Length; i++)
                {
                    if ((grid[checkSelected[i]] as ASPxComboBox).Value==null)
                    {
                        s.AppendLine(Nagging("MSTD00017WRN", checkSelDec[i]));
                    }
                }
            int _regNo = 0;
            if (!Int32.TryParse((grid["REG_NO"] as ASPxTextBox).Text, out _regNo)) {
                s.AppendLine(Nagging("MSTD00018WRN", "Reg No"));
            }
            
            if (s.Length > 0) {
                litMessage.Text = s.ToString();
            }
            return (s.Length == 0);
        }


        private bool ValidateInputSearch()
        {
            bool aOk = true;
            
            return aOk;
        }
        #endregion

        #region paging
        // const string GridCustomPageSizeName = "gridCustomPageSize";
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }

        protected void grid_Unbound(object sender, ASPxGridViewColumnDataEventArgs e)
        {
            if (e.Column.FieldName == "USER_DIV_POS")
            {
                e.Value = Convert.ToString(e.GetListSourceFieldValue("USERNAME")) 
                        + ";"
                        + Convert.ToString(e.GetListSourceFieldValue("DIVISION_ID"))
                        + ";"
                        + Convert.ToString(e.GetListSourceFieldValue("POSITION_ID"));
            }
        }

        private void Scatter(UserRoleData u, bool editing = false)
        {
            (grid["TITLE"] as ASPxTextBox).Text = u.TITLE;
            (grid["REG_NO"] as ASPxTextBox).Text = u.REG_NO;
            (grid["FIRST_NAME"] as ASPxTextBox).Text = u.FIRST_NAME;
            (grid["LAST_NAME"] as ASPxTextBox).Text = u.LAST_NAME;
            (grid["EMAIL"] as ASPxTextBox).Text = u.EMAIL;
            if (!editing) 
            {
                (grid["USERNAME"] as ASPxTextBox).Text = u.USERNAME;
                (grid["POSITION_ID"] as ASPxComboBox).Value = u.POSITION_ID;
                (grid["SECTION_ID"] as ASPxComboBox).Value = u.SECTION_ID;
                (grid["DEPARTMENT_ID"] as ASPxComboBox).Value = u.DEPARTMENT_ID;
                (grid["DIVISION_ID"] as ASPxComboBox).Value = u.DIVISION_ID;                
            }
            (grid["GROUP_CD"] as ASPxComboBox).Value = u.GROUP_CD;
            (grid["ROLE_ID"] as ASPxGridLookup).Value = u.ROLE_ID;
        }

        protected void txtGridUserName_ValueChanged(object sender, EventArgs e)
        {
            ASPxTextBox t = sender as ASPxTextBox;
            int check = 1;
            UserRoleData u = myLogic.FindUser(t.Text, ref check);
            if (u != null)
            {
                Mark();
                Scatter(u, true);
            }
            if (check > 0) Nag("MSTD00056ERR", null, "User", t.Text);
        }

        protected void grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            int newPageSize;
            if (!int.TryParse(e.Parameters, out newPageSize)) return;
            gridGeneralInfo.SettingsPager.PageSize = newPageSize;
            Session[GridCustomPageSizeName] = newPageSize;
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = linqDs.ID;
            gridGeneralInfo.DataBind();
        }

        protected string WriteSelectedIndex(int pageSize)
        {
            return pageSize == gridGeneralInfo.SettingsPager.PageSize ? "selected='selected'" : string.Empty;
        }
        
        #endregion

        protected void ddSection_Callback(object sender, CallbackEventArgsBase e)
        {
            GenerateComboData(sender as ASPxComboBox, "SECTION", e.Parameter.ToString(), true);
        }

        protected void ddDepartment_Callback(object sender, CallbackEventArgsBase e)
        {
            GenerateComboData(sender as ASPxComboBox, "DEPARTMENT", e.Parameter.ToString(), true);
        }

        protected void ddlGridSection_Callback(object sender, CallbackEventArgsBase e)
        {
            GenerateComboData(sender as ASPxComboBox, "SECTION", e.Parameter.ToString());
        }

        protected void ddlGridDepartment_Callback(object sender, CallbackEventArgsBase e)
        {
            GenerateComboData(sender as ASPxComboBox, "DEPARTMENT", e.Parameter.ToString());
        }



    }
}