﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using BusinessLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Function;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxGridView;
using ELVISDashBoard.presentationHelper;

namespace ELVISDashBoard._20MasterData
{
    public partial class Vendor : BaseCodeBehind
    {
        #region Init
        private const string mySelf = "Vendor";
        protected new string[] pages = "5,10,25,50,100,250,500".Split(',');
        private readonly string _ScreenID = new Common.Function.GlobalResourceData().GetResxObject("ScreenID", "ELVIS_Screen_2002");
        bool IsEditing;
        

        #region Getter Setter for modal button if any
        private Common.Enum.ModalButton ModalButton
        {
            set
            {
                ViewState["ModalButton"] = value;
            }
            get
            {
                return (Common.Enum.ModalButton)ViewState["ModalButton"];
            }
        }
        #endregion

        #region Getter Setter for Data List
        private List<VendorData> ListInquiry
        {
            set
            {
                ViewState["_listInquiry"] = value;
            }
            get
            {
                if (ViewState["_listInquiry"] == null)
                {
                    return new List<VendorData>();
                }
                else
                {
                    return (List<VendorData>)ViewState["_listInquiry"];
                }
            }
        }
        #endregion

        #endregion

        #region Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            lit = litMessage;
            hidScreenID.Value = _ScreenID;


            if (!IsPostBack)
            {
                PrepareLogout();

                GenerateComboData(ddlVendorGroupCode, "VENDOR_GROUP_ALL", true);
                //GenerateComboData(cbDivision, "DIVISION_LIST", true);

                gridGeneralInfo.DataSourceID = null;
                SetFirstLoad();
                AddEnterComponent(txtVendor);
                AddEnterComponent(txtVendorName);
                AddEnterComponent(ddlVendorGroupCode);
                AddEnterComponent(txtSearchTerms);
                AddEnterComponent(cbDivision);
                AddEnterComponent(txtSuspenseCount);
                AddEnterComponent(txtSuspenseCountTo);
                AddEnterAsSearch(btnSearch, _ScreenID);

                if (UserData.isFinanceDivision)
                {
                    gridGeneralInfo.Columns["SUSPENSE_COUNT"].Visible = true;
                }
                else
                {
                    suspenseCountDiv.Attributes.CssStyle.Add("display", "none");
                }
            }
        }


        private void FillKeyCombo(List<VendorData> l, DropDownList dd)
        {
            if (l==null) return;
            dd.DataSource = l;
            dd.DataValueField = "VENDOR_CD";
            dd.DataTextField = "VENDOR_NAME";
            dd.DataBind();
        }

        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);
           
            btnSearch.Visible = true && _RoleLogic.isAllowedAccess("btnSearch");
            btnClear.Visible = true;

            btnAdd.Visible = true && _RoleLogic.isAllowedAccess("btnAdd");
            btnEdit.Visible = false;
            btnDelete.Visible = false;

            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;

            btnExportExcel.Visible = false;
            btnClose.Visible = true;
        }
        #endregion

        #region Grid

        protected void gridGeneralInfo_HtmlRowCreated(object sender, ASPxGridViewTableRowEventArgs e)
        {
            #region rowtype data
            if (e.RowType == GridViewRowType.Data)
            {
                Literal litGridNoGeneralInfo = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex, 
                    gridGeneralInfo.Columns["NO"] as GridViewDataColumn, "litGridNo") as Literal;
                litGridNoGeneralInfo.Text = Convert.ToString((e.VisibleIndex + 1));

                Literal litDivisionName = gridGeneralInfo.FindRowCellTemplateControl(e.VisibleIndex,
                    gridGeneralInfo.Columns["CONTACT_PERSON"] as GridViewDataColumn, "litDivisionName") as Literal;
                string _divcode = Convert.ToString(gridGeneralInfo.GetRowValues(e.VisibleIndex, "CONTACT_PERSON"));
                litDivisionName.Text = logic.Vendor.GetDivisionName(_divcode);
            }
            #endregion
            #region rowtype edit
            if (IsEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    #region init
                    ASPxTextBox txtGridVendorCode = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["VENDOR_CD"] as GridViewDataColumn, "txtGridVendorCode") 
                        as ASPxTextBox;
                    ASPxTextBox txtGridVendorName = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["VENDOR_NAME"] as GridViewDataColumn, "txtGridVendorName") 
                        as ASPxTextBox;
                    ASPxTextBox txtGridSearchTerms = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["SEARCH_TERMS"] as GridViewDataColumn, "txtGridSearchTerms") 
                        as ASPxTextBox;
                    ASPxComboBox ddlGridDivision = gridGeneralInfo.FindEditRowCellTemplateControl(
                        gridGeneralInfo.Columns["CONTACT_PERSON"] as GridViewDataColumn, "ddlGridDivision") 
                        as ASPxComboBox;

                    #endregion
                    #region add
                    if (PageMode == Common.Enum.PageMode.Add)
                    {
                        txtGridVendorCode.ReadOnly = true;
                        if (UserData.DIV_CD == 5)
                        {
                            GenerateComboData(ddlGridDivision, "DIVISION_LIST");
                        }
                        else
                        {
                            ddlGridDivision.Visible = false;
                        }
                    }
                    #endregion
                    #region edit
                    else if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        int _VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        if (_VisibleIndex == e.VisibleIndex)
                        {
                            #region fill data edit
                            string _VendorCode = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "VENDOR_CD"));
                            string _VendorName = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "VENDOR_NAME"));
                            string _VendorGroupName = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "VENDOR_GROUP_NAME"));
                            int _VendorGroupCd = Convert.ToInt32(gridGeneralInfo.GetRowValues(_VisibleIndex, "VENDOR_GROUP_CD"));
                            string _SearchTerms = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "SEARCH_TERMS"));
                            string _DivisionCd = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "DIVISION_ID"));
                            string _Division = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "CONTACT_PERSON"));
                            txtGridVendorName.Focus();
                            txtGridVendorName.Text = _VendorName;
                            if (UserData.isFinanceDivision)
                            {
                                GenerateComboData(ddlGridDivision, "DIVISION_LIST");
                                ddlGridDivision.Value = _DivisionCd;
                                ddlGridDivision.Text = _Division;
                            }
                            else
                            {
                                ddlGridDivision.Visible = false;
                            }
                            txtGridVendorCode.Text = _VendorCode;
                            txtGridVendorCode.ReadOnly = true;
                            txtGridSearchTerms.Text = _SearchTerms;
                            #endregion
                        }
                    }
                    #endregion
                }
            }
            #endregion
        }

        protected void gridGeneralInfo_PageIndexChanged(object sender, EventArgs e)
        {
            gridGeneralInfo.DataBind();
        }

        protected void LinqDataSource1_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            int _vendorGroupCd = 0;
            if (!Int32.TryParse(ddlVendorGroupCode.SelectedValue,out _vendorGroupCd)) {
                _vendorGroupCd = 0;
            }
            ListInquiry = logic.Vendor.Search(txtVendor.Text, 
                _vendorGroupCd, txtSearchTerms.Text, txtVendorName.Text, 
                cbDivision.Value.str(), 
                txtSuspenseCount.Text, txtSuspenseCountTo.Text);
            e.Result = ListInquiry;
        }

        protected void dsDivision_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            string query = resx.Combo("DIVISION_LIST");
            e.Result = logic.Combo.getComboData(query, true, "DIVISION_LIST");
        }

        #endregion
        #endregion

        #region Buttons

        protected void btnClose_Click(object sender, EventArgs e)
        {
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.DataSourceID = null;
            gridGeneralInfo.DataBind();
            btnExportExcel.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            txtVendor.Text = "";
            txtVendorName.Text = "";
            txtSearchTerms.Text = "";
            ddlVendorGroupCode.SelectedValue = null;
            cbDivision.SelectedIndex = 0;
            txtSuspenseCount.Text = "";
            txtSuspenseCountTo.Text = "";            
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete");
                ConfirmationDeletePopUp.Show();
            }
            else if (gridGeneralInfo.Selection.Count == 0)
            {
                #region Error
                Nag("MSTD00009WRN");
                #endregion
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            bool result = false;
            if (gridGeneralInfo.Selection.Count >= 1)
            {
                List<string> listVendorCode = new List<string>();
                for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                {
                    if (gridGeneralInfo.Selection.IsRowSelected(i))
                    {
                        string VENDOR_CD = "";
                        VENDOR_CD = gridGeneralInfo.GetRowValues(i, "VENDOR_CD").ToString();
                        listVendorCode.Add(VENDOR_CD);
                    }
                }

                if (listVendorCode.Count > 0)
                {
                    PageMode = Common.Enum.PageMode.View;
                    result = logic.Vendor.Delete(listVendorCode);
                    if (!result)
                    {
                        Nag("MSTD00014ERR");
                        gridGeneralInfo.Selection.UnselectAll();
                    }
                    else
                    {
                        Nag("MSTD00049INF", mySelf);
                        gridGeneralInfo.Selection.UnselectAll();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                    }
                }
            }
        }

        protected void btnSaveDetail_Click(object sender, EventArgs e)
        {
            ASPxTextBox txtGridVendorName = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["VENDOR_NAME"] as GridViewDataColumn
                , "txtGridVendorName") as ASPxTextBox;
            ASPxTextBox txtGridSearchTerms = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["SEARCH_TERMS"] as GridViewDataColumn, "txtGridSearchTerms") as ASPxTextBox;
            ASPxComboBox ddlGridDivision = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["CONTACT_PERSON"] as GridViewDataColumn, "ddlGridDivision") as ASPxComboBox;
            string _VendorCode = "";
            string _division = "";
            string _divName = "";
            if (UserData.isFinanceDivision)
            {
                _division = ddlGridDivision.Value.str();
                _divName = logic.Vendor.GetDivisionName(_division);
            }
            else
            {
                _division = UserData.DIV_CD.str();
                _divName = UserData.DIV_NAME;
            }

            if (PageMode == Common.Enum.PageMode.Add)
            {
                #region Add
                if (ValidateInputSave())
                {
                    bool result = logic.Vendor.InsertVendor(new VendorData() {
                        VENDOR_CD = _VendorCode, 
                        VENDOR_NAME = txtGridVendorName.Text,
                        SEARCH_TERMS = txtGridSearchTerms.Text, 
                        DIVISION_ID = _division, 
                        DIVISION_NAME = _divName}                      
                        , UserData);

                    if (result)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                        gridGeneralInfo.DataBind();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        Nag("MSTD00011INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;

                    }
                    else
                    {
                        string msg = "", msgCode = "";
                        GetLastErrMessages(logic.Vendor.LastErr, mySelf, _VendorCode, "Add", ref msg, ref msgCode);
                        litMessage.Text = _m.SetMessage(msg, msgCode); 
                        
                    }
                }
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSave())
                {
                    int i = Convert.ToInt32(Session["visibleIndex"]);
                    _VendorCode = Convert.ToString(gridGeneralInfo.GetRowValues(i, "VENDOR_CD"));
                    bool result = logic.Vendor.EditVendor(
                        new VendorData() {
                        VENDOR_CD = _VendorCode, 
                        VENDOR_NAME = txtGridVendorName.Text,
                        SEARCH_TERMS = txtGridSearchTerms.Text,
                        DIVISION_ID = _division,
                        DIVISION_NAME = _divName
                        }
                        , UserData);

                    if (result == true)
                    {
                        gridGeneralInfo.CancelEdit();
                        gridGeneralInfo.DataBind();
                        gridGeneralInfo.Selection.UnselectAll();
                        SetFirstLoad();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        Nag("MSTD00013INF", mySelf);
                        gridGeneralInfo.SettingsBehavior.AllowSort = true;
                    }
                    else
                    {
                        Nag("MSTD00012ERR", mySelf);
                    }
                }
                #endregion
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            if (ValidateInputSearch())
            {
                gridGeneralInfo.Selection.UnselectAll();
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
                
                gridGeneralInfo.DataBind();
                PageMode = Common.Enum.PageMode.View;
                SetCancelEditDetail();
            }

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.AddNewRow();
            PageMode = Common.Enum.PageMode.Add;
            IsEditing = true;
            SetEditDetail();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (PageMode == Common.Enum.PageMode.View)
            {
                if (gridGeneralInfo.Selection.Count == 1)
                {
                    int _VisibleIndex = -1;
                    for (int i = 0; i < gridGeneralInfo.VisibleRowCount; i++)
                    {
                        if (gridGeneralInfo.Selection.IsRowSelected(i))
                        {
                            _VisibleIndex = i;
                            int _vendorGroup = Convert.ToInt32(gridGeneralInfo.GetRowValues(_VisibleIndex, "VENDOR_GROUP_CD"));
                            if (_vendorGroup == 3)
                            {
                                Nag("MSTD00092ERR");
                                break;
                            }
                            else
                            {
                                string _DivisionStr = Convert.ToString(gridGeneralInfo.GetRowValues(_VisibleIndex, "DIVISION_ID"));
                                if (!UserData.isFinanceDivision)
                                {
                                    if (!String.IsNullOrWhiteSpace(_DivisionStr))
                                    {
                                        decimal _DivisionCd = Convert.ToDecimal(_DivisionStr);
                                        if (!UserData.isInDivision(_DivisionStr))
                                        {
                                            Nag("MSTD00091ERR");
                                            break;
                                        }
                                    }
                                    else
                                    {
                                        Nag("MSTD00091ERR");
                                        break;
                                    }
                                }
                                
                                PageMode = Common.Enum.PageMode.Edit;
                                gridGeneralInfo.SettingsBehavior.AllowSort = false;
                                IsEditing = true;
                                Session["visibleIndex"] = i;
                                gridGeneralInfo.StartEdit(i);
                                gridGeneralInfo.Selection.UnselectAll();
                                SetEditDetail();
                                break;
                            }
                        }
                    }
                }
                else if (gridGeneralInfo.Selection.Count == 0)
                {
                    Nag("MSTD00009WRN");
                }
                else if (gridGeneralInfo.Selection.Count > 0)
                {
                    Nag("MSTD00016WRN");
                }
            }
        }

        protected void btnCancelDetail_Click(object sender, EventArgs e)
        {
            gridGeneralInfo.CancelEdit();
            btnSaveDetail.Visible = false;
            btnCancelDetail.Visible = false;
            SetFirstLoad();
            SetCancelEditDetail();
            PageMode = Common.Enum.PageMode.View;
        }

        #region export to excel
        protected void btnExportExcel_Click(object sender, EventArgs e)
        {
            if (ListInquiry != null)
            {
                try
                {
                    string imagePath = Server.MapPath(Common.AppSetting.CompanyLogo);
                    UserData userdata = (UserData)Session["UserData"];
                    string _FileName = mySelf.ToUpper() + DateTime.Now.ToString("ddMMyyyy_HHmmss");
                    this.Response.Clear();
                    this.Response.AddHeader("content-disposition", String.Format("attachment;filename={0}.xls", _FileName));
                    this.Response.Charset = "";
                    this.Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    this.Response.ContentType = "application/vnd.ms-excel";
                    bool hasSuspenseCount = gridGeneralInfo.Columns["SUSPENSE_COUNT"].Visible;
                    try
                    {
                        this.Response.BinaryWrite(logic.Vendor.Get(UserName, imagePath, ListInquiry, hasSuspenseCount));
                    }
                    catch (Exception ex)
                    {
                        LoggingLogic.err(ex);                        
                    }
                    this.Response.End();
                }
                catch (Exception Ex)
                {
                    litMessage.Text = Ex.ToString();
                    // throw Ex;
                }
            }
            else Nag("MSTD00018INF");
        }
        #endregion

        protected void setSearchEnabled(bool can = true)
        {
            txtSearchTerms.ReadOnly = !can;
            txtSearchTerms.CssClass = (!can) ? "disableFields" : "";
            ddlVendorGroupCode.Enabled = can;
            ddlVendorGroupCode.CssClass = (!can) ? "disableFields": "";
            txtVendor.ReadOnly = !can;
            txtVendor.CssClass = (!can) ? "disableFields" : "";
            txtVendorName.ReadOnly = !can;
            txtVendorName.CssClass = (!can) ? "disableFields" : "";
        }

        protected void SetEditDetail()
        {
            btnSaveDetail.Visible = true;
            btnCancelDetail.Visible = true;
            btnAdd.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnExportExcel.Visible = false;
            btnClose.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            gridGeneralInfo.SettingsBehavior.AllowSort = false;
            setSearchEnabled(false);

            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Hidden;
        }

        protected void SetCancelEditDetail()
        {
            if (gridGeneralInfo.PageCount > 0)
            {
                RoleLogic role = new RoleLogic(UserName, _ScreenID);
                ErrorData Err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref Err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
                btnExportExcel.Visible = role.isAllowedAccess("btnExportExcel");
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
                btnExportExcel.Visible = false;
            }
            setSearchEnabled();           

            gridGeneralInfo.Settings.ShowStatusBar = GridViewStatusBarMode.Visible;
        }

        #endregion

        #region Function

        private bool ValidateInputSave()
        {
            //bool code = true;
            StringBuilder s = new StringBuilder("");

            //REGION FOR VALIDATING INPUT SAVE
            ASPxTextBox txtGridVendorName = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["VENDOR_NAME"] as GridViewDataColumn, "txtGridVendorName") as ASPxTextBox;
            ASPxTextBox txtGridSearchTerms = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["SEARCH_TERMS"] as GridViewDataColumn, "txtGridSearchTerms") as ASPxTextBox;
            ASPxComboBox ddlGridDivision = gridGeneralInfo.FindEditRowCellTemplateControl(
                gridGeneralInfo.Columns["CONTACT_PERSON"] as GridViewDataColumn, "ddlGridDivision") as ASPxComboBox;
            
            if (String.IsNullOrWhiteSpace(txtGridVendorName.Text))
            {
                s.AppendLine(_m.Message("MSTD00017WRN", "Vendor Name"));
            }

            if (ddlGridDivision != null && ddlGridDivision.Visible)
            {
                if (String.IsNullOrWhiteSpace(ddlGridDivision.Text))
                {
                    s.AppendLine(_m.Message("MSTD00017WRN", "Division"));
                }
            }

            if (String.IsNullOrWhiteSpace(txtGridSearchTerms.Text))
            {
                s.AppendLine(_m.Message("MSTD00017WRN", "Search Terms"));
            }
            if (s.Length > 0) {
                litMessage.Text = s.ToString();
            }
            return s.Length == 0;
        }

        
        private bool ValidateInputSearch()
        {
            return true;
        }
        #endregion

        #region paging
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session[GridCustomPageSizeName] != null)
            {
                gridGeneralInfo.SettingsPager.PageSize = (int)Session[GridCustomPageSizeName];
            }
        }

        protected override void Rebind()
        {
            if (gridGeneralInfo.VisibleRowCount > 0)
                gridGeneralInfo.DataSourceID = LinqDataSource1.ID;
            gridGeneralInfo.DataBind();
        }
        protected void RecordPerPageSelect_Init(object sender, EventArgs e)
        {
            RecordPerPage_Init(sender, e, gridGeneralInfo);
        }
        protected void RecordPerPageSelect_SelectedIndexChanged(object sender, EventArgs e)
        {
            RecordPerPage_Selected(sender, e, gridGeneralInfo);
        }
        #endregion

    }
}