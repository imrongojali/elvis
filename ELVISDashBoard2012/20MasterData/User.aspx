﻿<%@ Page Title="User" Language="C#" MasterPageFile="~/MasterPage/GeneralMaster.Master"
    AutoEventWireup="true" CodeBehind="User.aspx.cs" Inherits="ELVISDashBoard._20MasterData.User" %>

<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView.Export" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPanel" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxPopupControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxTabControl" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxClasses" TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxTabControl"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2" Namespace="DevExpress.Web.ASPxClasses"
    TagPrefix="dx" %>
<%@ Register Assembly="DevExpress.Web.v12.2, Version=12.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridLookup" TagPrefix="dx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="xPre" ContentPlaceHolderID="pre" runat="server">
    <style type="text/css">
        .ddl
        {
            width: 220px;
        }
        .ddlgrid
        {
            width: 220px;
        }
    </style>
    <script type="text/javascript">

        var say;
        if (window.console && typeof console.log === "function") {
            say = function () { console.log.apply(console, arguments); };
        } else {
            say = function () { return; }
        }

        function IsNotEmpty(s, e) {
            if (!s) {
                return false
            } else {
                return true
            }
        }

        function OnPreChanged(s, x) {
            if (x.GetValue()) {
                say(s.name + " [" + x.name + "] =" + x.GetValue());
                s.PerformCallback(x.GetValue().toString());
            }
            else {
                s.PerformCallback('');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel runat="server" ID="upnLitMessage">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <asp:Literal runat="server" ID="litMessage" EnableViewState="false"></asp:Literal>
            <asp:HiddenField runat="server" ID="hidPageMode" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField runat="server" ID="hidPageTitle" Value="User Master Screen" />
    <asp:HiddenField runat="server" ID="hidScreenID" />
    <asp:UpdatePanel runat="server" ID="upnContentSection">
        <Triggers>
        </Triggers>
        <ContentTemplate>
            <div class="contentsection">
                <!-- Search Criteria button -->
                <div class="row">
                    <label>
                        User Name</label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtUserName" MaxLength="20" Columns="20" />
                    </div>
                    
                    <label>
                        Division</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlDivision" AutoResizeWithContainer="true" Width="300px"
                            ClientIDMode="Static" ClientInstanceName="ddlDivision">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) {OnPreChanged(ddlDepartment, s)}" />
                        </dx:ASPxComboBox>
                    </div>  
                </div>
                <div class="row">
                    <label>
                        Reg No.</label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtRegNo" MaxLength="10" Columns="10" />
                    </div>
                    <label>
                        Department</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlDepartment" AutoResizeWithContainer="true"
                            ClientIDMode="Static" Width="300px" ClientInstanceName="ddlDepartment" OnCallback="ddDepartment_Callback">
                            <ClientSideEvents SelectedIndexChanged="function(s,e) {OnPreChanged(ddlSection, s)}" />
                        </dx:ASPxComboBox>
                    </div>
                    
                </div>
                <div class="row">
                    <label>
                        Role Id</label>
                    <div class="rowwarphalfLeft">
                        <asp:TextBox runat="server" ID="txtRoleId" MaxLength="20" Columns="15" />
                    </div>
                    <label>
                        Section</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlSection" AutoResizeWithContainer="true" Width="300px" 
                            ClientIDMode="Static" ClientInstanceName="ddlSection" OnCallback="ddSection_Callback" />
                    </div>
                </div>
                <div class="row">
                    <label>
                        Position</label>
                    <div class="rowwarphalfLeft">
                        <dx:ASPxComboBox runat="server" ID="ddlPosition" AutoResizeWithContainer="true" CssClass="inquirycombo"
                            Width="230px" />
                    </div>
                    <!-- Search Clear Button-->
                    <div class="rowwarphalfRight2">
                        <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" />
                    </div>
                </div>
            </div>
            <div class="rowbtn">
                <div class="btnright">
                    <asp:Button runat="server" ID="btnAdd" Text="Add" OnClick="btnAdd_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnEdit" Text="Edit" OnClick="btnEdit_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnDelete" Text="Delete" OnClick="btnDelete_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="upnGrid" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnSearch" />
            <asp:PostBackTrigger ControlID="btnExportExcel" />
        </Triggers>
        <ContentTemplate>
            <dx:ASPxGridView ID="gridGeneralInfo" runat="server" Width="990px" AutoGenerateColumns="False"
                ClientInstanceName="gridGeneralInfo" OnHtmlRowCreated="gridGeneralInfo_HtmlRowCreated"
                OnCustomCallback="grid_CustomCallback" OnPageIndexChanged="gridGeneralInfo_PageIndexChanged"
                OnCustomUnboundColumnData="grid_Unbound" 
                KeyFieldName="USER_DIV_POS" EnableCallBacks="false" Styles-AlternatingRow-CssClass="even"  
                Settings-VerticalScrollableHeight="290">
                <Columns>
                    <dx:GridViewCommandColumn ShowSelectCheckbox="true" VisibleIndex="0" Width="35px" FixedStyle="Left">
                        <EditButton Visible="false" Text=" ">
                        </EditButton>
                        <UpdateButton Visible="false" Text=" ">
                        </UpdateButton>
                        <NewButton Visible="false" Text=" ">
                        </NewButton>
                        <CancelButton Visible="false" Text=" ">
                        </CancelButton>
                        <HeaderTemplate>
                            <dx:ASPxCheckBox ID="SelectAllCheckBox" runat="server" ClientSideEvents-CheckedChanged="function(s, e) { gridGeneralInfo.SelectAllRowsOnPage(s.GetChecked()); }"
                                CheckState="Unchecked" Style="text-align: center">
                            </dx:ASPxCheckBox>
                        </HeaderTemplate>
                    </dx:GridViewCommandColumn>
                    <dx:GridViewDataColumn FieldName="USER_DIV_POS" Visible="false" UnboundType="String">
                    </dx:GridViewDataColumn>
                    <dx:GridViewDataTextColumn Caption="No" VisibleIndex="1" Width="50px" FixedStyle="Left" CellStyle-HorizontalAlign="Right">
                        <DataItemTemplate>
                            <asp:Literal runat="server" ID="litGridNo"></asp:Literal>
                        </DataItemTemplate>
                        <EditItemTemplate>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="User Name" FieldName="USERNAME" VisibleIndex="2" FixedStyle="Left" 
                        Width="122px">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridUserName" Width="120px" MaxLength="20" OnValueChanged="txtGridUserName_ValueChanged" AutoPostBack="true" >
                            <ReadOnlyStyle Border-BorderStyle="None" Border-BorderWidth="0" />
                                <ClientSideEvents Validation="IsNotEmpty" />
                            </dx:ASPxTextBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Reg No" FieldName="REG_NO" Width="80px" VisibleIndex="3" 
                        CellStyle-HorizontalAlign="Right">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridRegNo" Width="78px" MaxLength="10">
                                <ClientSideEvents Validation="IsNotEmpty" />
                            </dx:ASPxTextBox>
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    
                    <dx:GridViewDataTextColumn Caption="Title" FieldName="TITLE" VisibleIndex="4" Width="50px" CellStyle-HorizontalAlign="Center">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridTitle" Width="48px" MaxLength="5" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="First Name" FieldName="FIRST_NAME" VisibleIndex="5"
                        Width="100px">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridFirstName" Width="98px" MaxLength="30" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Last Name" FieldName="LAST_NAME" VisibleIndex="6"
                        Width="100px">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridLastName" Width="98px" MaxLength="30" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                    <dx:GridViewDataTextColumn Caption="Email" FieldName="EMAIL" VisibleIndex="7" Width="150px">
                        <EditItemTemplate>
                            <dx:ASPxTextBox runat="server" ID="txtGridEmail" Width="148px" MaxLength="200" />
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>

                    <dx:GridViewDataDropDownEditColumn Caption="Position" FieldName="POSITION_NAME" Width="222px"
                        VisibleIndex="8">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridPosition" CssClass="ddlgrid" />
                        </EditItemTemplate>
                    </dx:GridViewDataDropDownEditColumn>

                    <dx:GridViewDataDropDownEditColumn Caption="Division" FieldName="DIVISION_NAME" Width="222px"
                        VisibleIndex="9">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridDivision" CssClass="ddlgrid" 
                            ClientInstanceName="ddlGridDivision">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnPreChanged(ddlGridDepartment, s); }"  />
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataDropDownEditColumn>

                    <dx:GridViewDataDropDownEditColumn Caption="Department" FieldName="DEPARTMENT_NAME"
                        Width="222px" VisibleIndex="10">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridDepartment" CssClass="ddlgrid"
                            ClientInstanceName="ddlGridDepartment" OnCallback="ddlGridDepartment_Callback">
                            <ClientSideEvents SelectedIndexChanged="function(s, e) { OnPreChanged(ddlGridSection, s); }"  />
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataDropDownEditColumn>

                    <dx:GridViewDataDropDownEditColumn Caption="Section" FieldName="SECTION_NAME" Width="222px"
                        VisibleIndex="11">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridSection" CssClass="ddlgrid" 
                            ClientInstanceName="ddlGridSection" OnCallback="ddlGridSection_Callback">
                            </dx:ASPxComboBox>
                        </EditItemTemplate>
                    </dx:GridViewDataDropDownEditColumn>

                    <dx:GridViewDataDropDownEditColumn Caption="Group" FieldName="GROUP_NAME" Width="222px"
                        VisibleIndex="12">
                        <EditItemTemplate>
                            <dx:ASPxComboBox runat="server" ID="ddlGridGroup" CssClass="ddlgrid" />
                        </EditItemTemplate>
                    </dx:GridViewDataDropDownEditColumn>

                    <dx:GridViewDataTextColumn Caption="Role ID" FieldName="ROLE_ID" Width="222px" VisibleIndex="13"
                        CellStyle-HorizontalAlign="Left">
                        <EditItemTemplate>
                             <dx:ASPxGridLookup runat="server" ID="loGridRoleId" Width="220px" DataSourceID="dsRoles"
                            ClientInstanceName="loGridRoleId" SelectionMode="Single" KeyFieldName="COMBO_CD" TextFormatString="{0}"
                            IncrementalFilteringMode="Contains" IncrementalFilteringDelay="1000">
                                <GridViewProperties EnableCallBacks="false" >
                                    <SettingsBehavior AllowSelectByRowClick="true" EnableRowHotTrack="true"/>
                                    <Settings ShowFilterRow="true" ShowStatusBar="Auto" UseFixedTableLayout="true"  />
                                    <SettingsPager PageSize="12"/>
                                </GridViewProperties>
                                <Columns>
                                <dx:GridViewDataTextColumn Caption="Role ID" FieldName="COMBO_CD" Width="140px" VisibleIndex="0">
                                  <Settings AutoFilterCondition="Contains"/>
                                </dx:GridViewDataTextColumn>
                                <dx:GridViewDataTextColumn Caption="Name" FieldName="COMBO_NAME" Width="200px" VisibleIndex="1">
                                  <Settings AutoFilterCondition="Contains"/>
                                </dx:GridViewDataTextColumn>
                                </Columns>
                            </dx:ASPxGridLookup>
                            
                        </EditItemTemplate>
                    </dx:GridViewDataTextColumn>
                </Columns>
                <SettingsBehavior AllowDragDrop="true" AllowSort="true" ColumnResizeMode="Control" />
                <SettingsEditing Mode="Inline" />
                <Settings ShowHorizontalScrollBar="True" ShowVerticalScrollBar="true" />
                <SettingsLoadingPanel ImagePosition="Top" />
                <Styles>
                    <Header HorizontalAlign="Center" VerticalAlign="Middle" Wrap="True">
                    </Header>
                    <Cell HorizontalAlign="Left" VerticalAlign="Middle">
                    </Cell>
                </Styles>
                <Settings ShowStatusBar="Visible" />
                <SettingsPager Visible="false" />
                <Templates>
                    <StatusBar>
                        <div style="text-align: left;">
                            Records per page:
                            <select onchange="gridGeneralInfo.PerformCallback(this.value);">
                                <option value="5" <%# WriteSelectedIndex(5) %>>5</option>
                                <option value="10" <%# WriteSelectedIndex(10) %>>10</option>
                                <option value="15" <%# WriteSelectedIndex(15) %>>15</option>
                                <option value="20" <%# WriteSelectedIndex(20) %>>20</option>
                                <option value="25" <%# WriteSelectedIndex(25) %>>25</option>
                            </select>&nbsp; <a title="First" href="JavaScript:gridGeneralInfo.GotoPage(0);">&lt;&lt;</a>
                            &nbsp; <a title="Prev" href="JavaScript:gridGeneralInfo.PrevPage();">&lt;</a> &nbsp;
                            Page
                            <input type="text" onchange="gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1)"
                                onkeydown="if (event.keyCode == 13) { event.cancelBubble=true; event.returnValue = false; gridGeneralInfo.GotoPage(parseInt(this.value, 10) - 1); return false; }"
                                value="<%# (gridGeneralInfo.PageIndex >= 0)? gridGeneralInfo.PageIndex + 1 : 1 %>"
                                style="width: 20px" />
                            of
                            <%# gridGeneralInfo.PageCount%>&nbsp;
                            <%# (gridGeneralInfo.VisibleRowCount > 1) ? "(" + gridGeneralInfo.VisibleRowCount + " Items)&nbsp;" : ""%>
                            <a title="Next" href="JavaScript:gridGeneralInfo.NextPage();">&gt;</a> &nbsp; <a
                                title="Last" href="JavaScript:gridGeneralInfo.GotoPage(<%# gridGeneralInfo.PageCount - 1 %>);">
                                &gt;&gt;</a> &nbsp;
                        </div>
                    </StatusBar>
                </Templates>
                <SettingsPager AlwaysShowPager="true">
                </SettingsPager>
            </dx:ASPxGridView>
            <asp:LinqDataSource ID="linqDs" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities"
                EntityTypeName="" Select="new (REG_NO, USERNAME, TITLE, FIRST_NAME, LAST_NAME, EMAIL, GROUP_CD, GROUP_NAME, ROLE_ID, CLASS, POSITION_ID, POSITION_NAME, DIVISION_ID, DIVISION_NAME, DEPARTMENT_ID, DEPARTMENT_NAME, SECTION_ID, SECTION_NAME)"
                OnSelecting="linqDs_Selecting" TableName="vw_User">
            
            </asp:LinqDataSource>
            <asp:LinqDataSource ID="dsRoles" runat="server" ContextTypeName="DataLayer.Model.ELVIS_DBEntities" OnSelecting="dsRoles_Selecting"
            EntityTypeName="" Select="" TableName="TB_M_SYSTEM"  />
            <div class="rowbtn">
                <div class="btnleft">
                    <asp:Button runat="server" ID="btnExportExcel" Text="Download" CssClass="xlongButton"
                        SkinID="xlongButton" OnClick="btnExportExcel_Click" />
                </div>
                <div class="btnright">
                    <asp:Button ID="btnSaveDetail" runat="server" Text="Save" OnClick="btnSaveDetail_Click" />
                    &nbsp;
                    <asp:Button ID="btnCancelDetail" runat="server" Text="Cancel" OnClick="btnCancelDetail_Click" />
                    &nbsp;
                    <asp:Button runat="server" ID="btnClose" Text="Close" OnClick="btnClose_Click" OnClientClick="closeWin()" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--Panel for pop up list and confirmation-->
    <asp:UpdatePanel ID="upnlConfirmationDelete" runat="server">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnDelete" />
        </Triggers>
        <ContentTemplate>
            <asp:Button ID="hidBtnConfirmationDelete" runat="server" Style="display: none;" />
            <cc1:ModalPopupExtender ID="ConfirmationDeletePopUp" runat="server" TargetControlID="hidBtnConfirmationDelete"
                PopupControlID="ConfirmationDeletePanel" CancelControlID="BtnCancelConfirmationDelete"
                BackgroundCssClass="modalBackground" />
            <center>
                <asp:Panel ID="ConfirmationDeletePanel" runat="server" CssClass="informationPopUp">
                    <center>
                        <asp:Label ID="lblConfirmationDelete" runat="server" Text=""></asp:Label>
                        <br />
                        <asp:Button ID="BtnOkConfirmationDelete" Text="OK" runat="server" Width="60px" OnClick="btnOkConfirmationDelete_Click"
                            OnClientClick="loading()" />
                        &nbsp;
                        <asp:Button ID="BtnCancelConfirmationDelete" Text="Cancel" runat="server" Width="60px" />
                    </center>
                </asp:Panel>
            </center>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!--End of panel for pop up list and confirmation-->
</asp:Content>
