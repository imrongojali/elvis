﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ELVISDashBoard.presentationHelper;
using BusinessLogic._00Administration;
using Common.Messaging;
using System.Text;
using Common.Http;
using Common.Function;
using DevExpress.Web.Data;

namespace ELVISDashBoard._00Administration
{
    public partial class AnnouncementForm : BaseCodeBehind
    {        
        private AnnouncementPersistence persistence;
        private string _ScreenID;

        public AnnouncementForm()
        {
            persistence = new AnnouncementPersistence();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            _ScreenID = resx.Screen("ELVIS_Screen_0006").ToString();
            hidScreenID.Value = _ScreenID;
            clearScreenMessage();
            if (!IsPostBack)
            {
                PrepareLogout();
                searchAnnouncement();
            }
        }

        public AnnouncementMaster PageForm
        {
            get
            {
                AnnouncementMaster form = Session["AnnounceForm"] as AnnouncementMaster;
                if (form == null)
                {
                    form = new AnnouncementMaster();
                    form.UserData = UserData;
                    Session["AnnounceForm"] = form;
                }

                return form;
            }
        }

        private void fetchScreenData()
        {
            AnnouncementMaster form = PageForm;
            form.Title = tboxTitle.Text.Trim();
            form.Description = mmDescription.Text.Trim();
            
            evt_ddlPosition_onLoad(null, null);
            List<object> selectedPosition = ddlPosition.GridView.GetSelectedFieldValues("PositionID");
            if ((selectedPosition == null) || (!selectedPosition.Any()))
            {
                ddlPosition.GridView.Selection.SelectAll();
                selectedPosition = ddlPosition.GridView.GetSelectedFieldValues("PositionID");
            }
            form.PositionID = selectedPosition;

            evt_ddlPosition_onLoad(null, null);
            List<object> selectedDivision = ddlDivision.GridView.GetSelectedFieldValues("DivisionID");
            if ((selectedDivision == null) || (!selectedDivision.Any()))
            {
                ddlDivision.GridView.Selection.SelectAll();
                selectedDivision = ddlDivision.GridView.GetSelectedFieldValues("DivisionID");
            }
            form.DivisionID = selectedDivision;

            form.ReleaseDate = null;
            if (dtReleaseDate.Value != null)
            {
                form.ReleaseDate = (DateTime) dtReleaseDate.Value;
            }

            form.ExpireDate = null;
            if (dtExpireDate.Value != null)
            {
                form.ExpireDate = (DateTime) dtExpireDate.Value;
            }
        }
        private void dumpFormData()
        {
            AnnouncementMaster form = PageForm;
            tboxTitle.Text = form.Title;
            mmDescription.Text = form.Description;
            List<object> selectedDivision = form.DivisionID;
            if ((selectedDivision == null) || (!selectedDivision.Any()))
            {
                ddlDivision.GridView.Selection.UnselectAll();
            }
            else
            {
                WebDataSelection selection = ddlDivision.GridView.Selection;
                foreach (object ob in selectedDivision)
                {
                    selection.SelectRowByKey(ob);
                }
            }

            List<object> selectedPosition = form.PositionID;
            if ((selectedPosition == null) || (!selectedPosition.Any()))
            {
                ddlPosition.GridView.Selection.UnselectAll();
            }
            else
            {
                WebDataSelection selection = ddlPosition.GridView.Selection;
                foreach (object ob in selectedPosition)
                {
                    selection.SelectRowByKey(ob);
                }
            }   

            dtReleaseDate.Value = form.ReleaseDate;
            dtExpireDate.Value = form.ExpireDate;
        }
        
        private void searchAnnouncement()
        {
            gridSearchResult.DataSource = persistence.search();
            gridSearchResult.DataBind();
        }

        private bool inputIsValid()
        {
            string msgNullOrEmpty = _m.Message("MSTD00017WRN");
            clearScreenMessage();
            AnnouncementMaster form = PageForm;
            bool valid = true;
            if (string.IsNullOrEmpty(form.Title))
            {
                valid = false;
                postScreenMessage(new ScreenMessage[] {
                    new ScreenMessage(ScreenMessage.STATUS_ERROR, msgNullOrEmpty, new string[] { "Title" })
                }, false);
            }
            if (string.IsNullOrEmpty(form.Description))
            {
                valid = false;
                postScreenMessage(new ScreenMessage[] {
                    new ScreenMessage(ScreenMessage.STATUS_ERROR, msgNullOrEmpty, new string[] { "Description" })
                }, false);
            }
            if (form.ReleaseDate == null)
            {
                valid = false;
                postScreenMessage(new ScreenMessage[] {
                    new ScreenMessage(ScreenMessage.STATUS_ERROR, msgNullOrEmpty, new string[] { "Release Date" })
                }, false);
            }
            if (form.ExpireDate == null)
            {
                valid = false;
                postScreenMessage(new ScreenMessage[] {
                    new ScreenMessage(ScreenMessage.STATUS_ERROR, msgNullOrEmpty, new string[] { "Expire Date" })
                }, false);
            }         

            return valid;
        }

        protected void evt_btSave_Clicked(object sender, EventArgs arg)
        {
            bool saved = false;
            fetchScreenData();
            if (inputIsValid())
            {
                AnnouncementMaster form = PageForm;
                persistence.save(form);
                form.reset();
                dumpFormData();
                saved = true;
            }

            if (saved)
            {
                postScreenMessage(new ScreenMessage[] {
                    new ScreenMessage(ScreenMessage.STATUS_INFO, resx.GetResxObject("Message", "MSTD00055INF"))
                }, true, true);
            }

            searchAnnouncement();
        }

        protected override void clearSession()
        {
            base.clearSession();
            Session["AnnounceMsgList"] = null;
            Session["AnnounceForm"] = null;
        }

        protected void evt_btClose_onClick(object sender, EventArgs arg)
        {            
            clearSession();
        }

        protected override string getScreenName() 
        {
            return "AnnouncementForm";
        }

        protected void evt_ddlDivision_onLoad(object sender, EventArgs arg)
        {            
            ddlDivision.DataSource = persistence.listDivision();
            ddlDivision.DataBind();
        }

        protected void evt_ddlPosition_onLoad(object sender, EventArgs arg)
        {
            ddlPosition.DataSource = persistence.listUserPosition();
            ddlPosition.DataBind();
        }

        protected void evtSearchResultTable_PageIndexChanged(object sender, EventArgs arg)
        {
            searchAnnouncement();
        }

        #region Screen Message

        protected List<ScreenMessage> ScreenMessageList
        {
            get
            {
                //List<ScreenMessage> lstMessages = (List<ScreenMessage>)SessionManager.get(ScreenType + "MsgList");
                List<ScreenMessage> lstMessages = Session["AnnounceMsgList"] as List<ScreenMessage>;
                if (lstMessages == null)
                {
                    lstMessages = new List<ScreenMessage>();
                    //SessionManager.add(ScreenType + "MsgList", lstMessages);
                    Session["AnnounceMsgList"] = lstMessages;
                    hdExpandFlag.Value = "false";
                }
                return lstMessages;
            }
        }

        protected void postScreenMessage(ScreenMessage[] messages)
        {
            postScreenMessage(messages, false, true);
        }
        protected void postScreenMessage(ScreenMessage[] messages, bool instantDisplay)
        {
            postScreenMessage(messages, false, instantDisplay);
        }
        protected void postScreenMessage(ScreenMessage[] messages, bool clearFirst, bool instantDisplay)
        {
            List<ScreenMessage> lstMessage = ScreenMessageList;
            if (clearFirst)
            {
                lstMessage.Clear();
            }

            if (messages != null)
            {
                lstMessage.AddRange(messages);
            }
            updateScreenMessages();
        }
        protected void updateScreenMessages()
        {
            List<ScreenMessage> lstMessages = ScreenMessageList;
            int cntMessages = lstMessages.Count;
            if (cntMessages > 0)
            {
                if (cntMessages > 0)
                {
                    ScreenMessage msg = lstMessages[0];
                    StringBuilder stringBuilder = new StringBuilder();
                    string expandFlag = hdExpandFlag.Value;

                    string messageBoxClass = "message-info";
                    string messageBoxSpanClass = "message-span-info";
                    if (msg.Status == ScreenMessage.STATUS_WARNING)
                    {
                        messageBoxClass = "message-warning";
                        messageBoxSpanClass = "message-span-warning";
                    }
                    else if (msg.Status == ScreenMessage.STATUS_ERROR)
                    {
                        messageBoxClass = "message-error";
                        messageBoxSpanClass = "message-span-error";
                    }

                    if (expandFlag.Equals("true"))
                    {
                        messageBoxClass += " expanded-message-box";
                    }
                    stringBuilder.AppendLine(string.Format("<div id='messageBox' class='{0}'>", messageBoxClass));
                    stringBuilder.AppendLine(string.Format("      <span class='{0}'>{1}</span>", messageBoxSpanClass, msg.Message));

                    if (cntMessages > 1)
                    {
                        string txtMore = "more";
                        if (expandFlag.Equals("true"))
                        {
                            txtMore = "close";
                        }
                        stringBuilder.AppendLine("      <span class='message-span-more'>");
                        stringBuilder.AppendLine(string.Format("          <a id='message-detail-link' href='#' onclick=\"expandScreenMessage()\">{0}</a>", txtMore));
                        stringBuilder.AppendLine("      </span>");
                    }

                    stringBuilder.AppendLine("</div>");
                    if (expandFlag.Equals("false"))
                    {
                        stringBuilder.AppendLine("<div id='messageBox-all' style='display: none'>");
                    }
                    else
                    {
                        stringBuilder.AppendLine("<div id='messageBox-all'>");
                    }

                    stringBuilder.AppendLine("  <ul>");
                    string listClass;
                    for (int i = 1; i < cntMessages; i++)
                    {
                        msg = lstMessages[i];
                        listClass = "message-detail-info";
                        if (msg.Status == ScreenMessage.STATUS_WARNING)
                        {
                            listClass = "message-detail-warning";
                        }
                        else if (msg.Status == ScreenMessage.STATUS_ERROR)
                        {
                            listClass = "message-detail-error";
                        }

                        stringBuilder.AppendLine(string.Format("<li class='{0}'>", listClass));
                        stringBuilder.AppendLine("      <span>" + msg.Message + "</span>");
                        stringBuilder.AppendLine("</li>");
                    }
                    stringBuilder.AppendLine("  </ul>");
                    stringBuilder.AppendLine("</div>");

                    messageControl.Text = stringBuilder.ToString();
                    messageControl.Visible = true;
                }
            }
            else
            {
                messageControl.Visible = false;
            }
        }
        protected void clearScreenMessage()
        {
            ScreenMessageList.Clear();
            updateScreenMessages();
        }
        protected void evt_messageControl_onLoad(object sender, EventArgs args)
        {
            updateScreenMessages();
        }
        #endregion Screen Message
    }
}