﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using Common.Data;
using BusinessLogic;
using BusinessLogic.PoA;
using BusinessLogic.CommonLogic;
using Common.Function;
using DataLayer;
using System.Data.Common;
using System.Data.Entity;
using ELVISDashBoard.presentationHelper;
using System.Diagnostics;

namespace ELVISDashBoard.Administration
{
    public partial class PoA : BaseCodeBehind
    {
        bool isEditing;
        private string _ScreenID;

        protected void Page_Load(object sender, EventArgs e)
        {
            lit = litMessage;
            if (UserData == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            _ScreenID = resx.Screen("ELVIS_Screen_0005").ToString();
            hidScreenID.Value = _ScreenID;
            logic.PoA.emailTemplateDir = Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE","DIR"));

            if (!IsPostBack)
            {
                List<string> attorneys = logic.PoA.getAttorneyUsernamePoa(UserName);
                attorneys.Insert(0, "");
                ddlAttorney.DataSource = attorneys;
                // btnClose.OnClientClick = @"closeWindow('0')";
                ddlAttorney.DataBind();
                ASPxGridView1.DataSourceID = null;
                SetFirstLoad();

                AddEnterComponent(txtPOANumber);
                AddEnterComponent(txtGrantorOfAttorney);
                AddEnterComponent(ddlAttorney);
                AddEnterComponent(ddlOfficeStatus);
                AddEnterAsSearch(btnSearch, _ScreenID);
            }
        }
        #region SetFirstLoad
        private void SetFirstLoad()
        {
            RoleLogic _RoleLogic = new RoleLogic(UserName, _ScreenID);

            btnSearch.Visible = true;
            btnAdd.Visible = _RoleLogic.isAllowedAccess("btnAdd");

            btnSave.Visible = false;
            btnEdit.Visible = false;
            btnDelete.Visible = false;
            btnCancel.Visible = false;
        }
        #endregion

        protected void SetSearch(bool b)
        {
            txtPOANumber.Enabled = b;
            txtGrantorOfAttorney.Enabled = b;
            ddlAttorney.Enabled = b;
            ddlOfficeStatus.Enabled = b;
            btnSearch.Enabled = b;
            btnClear.Enabled = b;
            if (b)
                fromToDiv.Attributes.CssStyle.Remove("display");
            else
                fromToDiv.Attributes.CssStyle.Add("display", "none");

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            setMessage("");

            logic.Say("PoA.Add_Click", UserName);
            if (logic.PoA.CheckOOFStatus(UserName))
            {
                setMessage("MSTD00025ERR");
                LoadGrid();
                return;
            }

            if (!logic.PoA.canChangeGrantor(UserName) && logic.PoA.FuturePoACount(UserName) > 0)
            {
                setMessage("MSTD00141ERR");
                LoadGrid();
                return;
            }

            PageMode = Common.Enum.PageMode.Add;
            isEditing = true;

            ASPxGridView1.AddNewRow();
            SetEditMode();
        }

        private void SetEditMode()
        {
            btnSave.Visible = true;
            btnCancel.Visible = true;
            btnEdit.Visible = false;
            btnAdd.Visible = false;
            btnClose.Visible = false;
            btnDelete.Visible = false;
            btnSearch.Visible = false;
            btnClear.Visible = false;
            SetSearch(false);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            ASPxGridView1.CancelEdit();
            //ErrorData Err = null;
            setMessage("");
            logic.Say("PoA.Edit_Click", UserName);
            if (ASPxGridView1.Selection.Count == 1)
            {
                int visibleIndex = -1;

                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        //bool _oof = (bool)ASPxGridView1.GetRowValues(i, "OFFICE_STATUS");
                        string poa = ASPxGridView1.GetRowValues(i, "POA_NUMBER").str();
                        string gran = ASPxGridView1.GetRowValues(i, "GRANTOR").str();
                        if (!(gran.Equals(UserName) ||  PoALogic.roleAllowedEdit(LoginUserRole)))
                        {
                            setMessage("MSTD00002ERR", "Only Grantor, secretary or Admin can edit");
                            break;
                        }

                        if (logic.PoA.canEdit(poa))
                        {
                            visibleIndex = i;
                            isEditing = true;
                            SetEditMode();
                            PageMode = Common.Enum.PageMode.Edit;
                            ASPxGridView1.StartEdit(i);
                            Session["visibleIndex"] = i;
                            break;
                        }
                        else
                        {
                            setMessage("MSTD00002ERR", "Cannot Edit Data");
                        }
                    }
                }

            }
            else if (ASPxGridView1.Selection.Count == 0)
            {
                setMessage("MSTD00009WRN");
            }
            else if (ASPxGridView1.Selection.Count > 1)
            {
                setMessage("MSTD00016WRN");
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            logic.Say("PoA.Clear_Click", UserName);
            Clear();
            btnEdit.Visible = false;
            btnDelete.Visible = false;
        }

        public void Clear()
        {
            txtPOANumber.Text = "";
            txtGrantorOfAttorney.Text = "";
            ddlAttorney.SelectedIndex = 0;
            ddlOfficeStatus.SelectedValue = "";
            dtValidFrom.Text = "";
            dtValidTo.Text = "";
            setMessage("");
            ASPxGridView1.DataSourceID = null;
            ASPxGridView1.DataBind();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            setMessage("");
            logic.Say("PoA.Search_Click", UserName);
            LoadGrid();
            SetCancelEditDetail();
        }

        private Common.Enum.PageMode PageMode
        {
            set
            {
                ViewState["PageMode"] = value;
                if ((Common.Enum.PageMode)value == Common.Enum.PageMode.View)
                {
                    hidPageMode.Value = "0";
                    btnClose.OnClientClick = @"closeWindow('0')";
                    _ScreenID = resx.Screen("ELVIS_Screen_0005").ToString();
                    Response.Cookies[_ScreenID].Value = "0";
                }
                else
                {
                    hidPageMode.Value = "1";
                    btnClose.OnClientClick = @"closeWindow('1')";
                    Response.Cookies[_ScreenID].Value = "0";
                }
            }
            get
            {
                if (ViewState["PageMode"] == null)
                {
                    return Common.Enum.PageMode.View;
                }
                else
                {
                    return (Common.Enum.PageMode)ViewState["PageMode"];
                }
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            logic.Say("PoA.Delete_Click", UserName);
            if (ASPxGridView1.Selection.Count == 1)
            {
                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (!ASPxGridView1.Selection.IsRowSelected(i))
                        continue;
                    string gran = ASPxGridView1.GetRowValues(i, "GRANTOR").str();
                    if (!(gran.Equals(UserName) || PoALogic.roleAllowedEdit(LoginUserRole)))
                    {
                        setMessage("MSTD00002ERR", "Only Grantor, secretary or Admin can edit");
                        return;
                    }
                }

                lblConfirmationDelete.Text = _m.Message("MSTD00008CON", "delete PoA");
                ConfirmationDeletePopUp.Show();
            }
            else if (ASPxGridView1.Selection.Count == 0)
            {
                setMessage("MSTD00009WRN");
            }
            else if (ASPxGridView1.Selection.Count > 1)
            {
                setMessage("MSTD00016WRN");
            }
        }

        private Literal litGridPOANumber;
        private TextBox txtGridReason;
        private ASPxComboBox dgAttorney, ddlGrantor;
        private ASPxDateEdit dtGridValidFrom;
        private ASPxDateEdit dtGridValidTo;

        private void Mark()
        {
            litGridPOANumber = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["POA_NUMBER"] as GridViewDataColumn, "litGridPOANumber") as Literal;
            txtGridReason = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["REASON"] as GridViewDataColumn, "txtGridReason") as TextBox;
            dgAttorney = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["ATTORNEY"] as GridViewDataColumn, "dgAttorney") as ASPxComboBox;
            ddlGrantor = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["GRANTOR"] as GridViewDataColumn, "ddlGrantor") as ASPxComboBox;

            //dgOfficeStatus = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["OFFICE_STATUS"] as GridViewDataColumn, "dgOfficeStatus") as DropDownList;
            dtGridValidFrom = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["VALID_FROM"] as GridViewDataColumn, "dtGridValidFrom") as ASPxDateEdit;
            dtGridValidTo = ASPxGridView1.FindEditRowCellTemplateControl(ASPxGridView1.Columns["VALID_TO"] as GridViewDataColumn, "dtGridValidTo") as ASPxDateEdit;
        }

        private bool ValidateInputSaveADD()
        {
            bool ok = true;
            Mark();
            if (ddlGrantor.Value.str() == "")
            {
                setMessage("MSTD00017WRN", "Grantor");
                ok = false;
            }
            else if (dgAttorney.SelectedIndex == 0)
            {
                setMessage("MSTD00017WRN", "Attorney");
                ok = false;
            }
            else if (txtGridReason.Text.isEmpty())
            {
                setMessage("MSTD00017WRN", "Reason");
                ok = false;
            }
            else if (String.Compare(Convert.ToString(ddlGrantor.Value), Convert.ToString(dgAttorney.Value), false) == 0)
            {
                setMessage("MSTD00098ERR", "Grantor", "Attorney");
                ok = false;
            }
            else if (dtGridValidFrom.Text.Trim().isEmpty())
            {
                setMessage("MSTD00017WRN", "Valid From");
                ok = false;
            }
            else if (dtGridValidTo.Text.Trim().isEmpty())
            {

                setMessage("MSTD00017WRN", "Valid To");
                ok = false;
            }
            else if (dtGridValidFrom.Date.CompareTo(dtGridValidTo.Date) > 0)
            {

                setMessage("MSTD00094ERR", "Valid To", "Valid From");
                ok = false;
            }

            return ok;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            setMessage("");
            ErrorData Err = null;
            string loc = "PoA.Save_Click";
            logic.Say(loc, UserName);
            Mark();

            
            string gran = ddlGrantor.Value.str();
            string ator = dgAttorney.Value.str();
            string poaNum = "";
            DateTime dateTimeValidFrom = dtGridValidFrom.Date;
            DateTime dateTimeValidTo = dtGridValidTo.Date;
            DateTime currDate = DateTime.Now;
            currDate = new DateTime(currDate.Year, currDate.Month, currDate.Day);

            if (PageMode == Common.Enum.PageMode.Add)
            {

                #region Add
                if (ValidateInputSaveADD())
                {
                    bool ok = false;

                    ok = logic.PoA.AddPOA(
                            ref poaNum,
                            gran,
                            ator,
                            txtGridReason.Text,
                            "1",
                            dateTimeValidFrom,
                            dateTimeValidTo,
                            UserName, ref Err);
                    if (ok)
                    {
                        isEditing = false;

                        setMessage("MSTD00011INF", "PoA");
                        ASPxGridView1.CancelEdit();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;

                        if (dateTimeValidFrom <= currDate && dateTimeValidTo >= currDate)
                        {
                            logic.Say(loc, "Current Date inside Valid Date, Execute Redirect Worklist...");
                            try
                            {
                                logic.PoA.RedirectWorklist(poaNum, UserName, false);
                            }
                            catch (Exception ex)
                            {
                                Nag("MSTD00002ERR", ex.Message);
                                logic.Say(loc, "Error Execute Redirect Worklist : " + ex.Message);

                                // Delete PoA [START]
                                bool isDeleteSuccess = logic.PoA.DeletePOA(poaNum, ref Err);
                                if (!isDeleteSuccess)
                                {
                                    NagAdd("MSTD00062ERR", "Delete PoA");
                                    logic.Say(loc, "Delete PoA {0} Failed", poaNum);
                                }
                                else
                                {
                                    logic.Say(UserName, "Delete PoA {0} Success", poaNum);
                                }
                                // Delete PoA [END]
                            }
                        }
                        else
                        {
                            logic.Say(loc, "Current Date not inside Valid Date, Not Execute Redirect Worklist");
                            bool isEditSuccess = logic.PoA.EditStatusPOA(poaNum, true, ref Err, UserData);
                            if (!isEditSuccess)
                            {
                                NagAdd("MSTD00062ERR", "Edit Status PoA to In Office");
                                logic.Say(loc, "Edit Status PoA {0} to In Office Failed", poaNum);
                            }
                            else
                            {
                                logic.Say(loc, "Edit Status PoA {0} to In Office Success", poaNum);
                            }
                            // Edit Status PoA to In Office [END]
                        }

                        LoadGrid();
                    }
                    else
                    {
                        #region Error
                        if (Err != null)
                        {
                            setMessage(Err.ErrMsgID, Err.Data);
                        }
                        #endregion
                    }
                }
                isEditing = true;
                #endregion
            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                #region Edit
                if (ValidateInputSaveADD())
                {
                    poaNum = litGridPOANumber.Text;

                    PoAData d = new PoAData()
                    {
                        POA_NUMBER = poaNum,
                        GRANTOR = gran,
                        ATTORNEY = ator,
                        VALID_FROM = dateTimeValidFrom,
                        VALID_TO = dateTimeValidTo
                    };
                    bool ok = logic.PoA.EditPOA(d, ref Err, UserData);
                    if (ok)
                    {
                        setMessage("MSTD00013INF", "PoA");
                        ASPxGridView1.CancelEdit();
                        ASPxGridView1.DataBind();
                        ASPxGridView1.Selection.UnselectAll();
                        SetCancelEditDetail();
                        PageMode = Common.Enum.PageMode.View;
                        bool activePoa = PoALogic.isActive(d.VALID_FROM, d.VALID_TO, DateTime.Now, null);
                        
                        logic.Say(loc, "PoA.RedirectWorklist({0}, {1}, inOffice={2})", poaNum, UserName, !activePoa);
                        try
                        {
                            logic.PoA.RedirectWorklist(poaNum, UserName, !activePoa);
                        }
                        catch (Exception ex)
                        {
                            Nag("MSTD00002ERR", ex.Message);
                            logic.Say(loc, "Error RedirectWorklist : " + ex.Message);
                        }
                        LoadGrid();
                    }
                    else
                    {
                        setMessage(Err.ErrMsgID, Err.Data);
                    }
                }
                #endregion
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            setMessage("");
            if (PageMode != Common.Enum.PageMode.View)
            {
                setMessage("MSTD00027ERR");
            }
        }

        protected void grid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            Mark();

            if (e.RowType == GridViewRowType.Data)
            {
                Literal litOfficeStatus = ASPxGridView1.FindRowCellTemplateControl(e.VisibleIndex,
                    (GridViewDataColumn)ASPxGridView1.Columns["OFFICE_STATUS"] as GridViewDataColumn, "litOfficeStatus") as Literal;
                bool _officeStatus = (bool)ASPxGridView1.GetRowValues(e.VisibleIndex, "OFFICE_STATUS");
                litOfficeStatus.Text = ((_officeStatus) ? "In" : "Out Of") + " Office";
            }

            if (isEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    if (PageMode == Common.Enum.PageMode.Add)
                    {

                    }
                    else if (PageMode == Common.Enum.PageMode.Edit)
                    {
                        int VisibleIndex = Convert.ToInt32(Session["visibleIndex"]);
                        if (VisibleIndex == e.VisibleIndex)
                        {
                            // ErrorData Err = null;

                            var _poaNumber = ASPxGridView1.GetRowValues(VisibleIndex, "POA_NUMBER");
                            var _grantor = ASPxGridView1.GetRowValues(VisibleIndex, "GRANTOR");
                            var _attorney = ASPxGridView1.GetRowValues(VisibleIndex, "ATTORNEY");
                            var _reason = ASPxGridView1.GetRowValues(VisibleIndex, "REASON");

                            string poaNo = _poaNumber.ToString();

                            litGridPOANumber.Text = poaNo;
                            ddlGrantor.Value = _grantor.ToString();
                            dgAttorney.Value = _attorney.ToString();
                            ddlGrantor.ReadOnly = true;
                            dgAttorney.Enabled = false;
                            txtGridReason.Text = _reason.ToString();
                            txtGridReason.Enabled = false;
                            //dgOfficeStatus.SelectedValue = "1";
                            //dgOfficeStatus.Enabled = false;
                            bool canEditValid = logic.PoA.isFuturePoA(poaNo) || PoALogic.roleAllowedEdit(LoginUserRole);
                            dtGridValidFrom.ReadOnly = !canEditValid;
                            dtGridValidFrom.ClientEnabled = canEditValid;
                            if (ASPxGridView1.GetRowValues(VisibleIndex, "VALID_FROM") != null)
                            {
                                DateTime validFrom = (DateTime)ASPxGridView1.GetRowValues(VisibleIndex, "VALID_FROM");
                                dtGridValidFrom.Date = validFrom;
                            }

                            dtGridValidTo.ReadOnly = !canEditValid;
                            dtGridValidTo.ClientEnabled = canEditValid;
                            if (ASPxGridView1.GetRowValues(VisibleIndex, "VALID_TO") != null)
                            {
                                DateTime validTo = (DateTime)ASPxGridView1.GetRowValues(VisibleIndex, "VALID_TO");
                                dtGridValidTo.Date = validTo;
                            }
                        }
                    }
                }
            }
        }

        protected void btnOkConfirmationDelete_Click(object sender, EventArgs e)
        {
            ConfirmationDeletePopUp.Hide();
            if (ASPxGridView1.Selection.Count >= 1)
            {
                int visibleIndex = -1;
                bool ok = false;

                for (int i = 0; i < ASPxGridView1.VisibleRowCount; i++)
                {
                    if (ASPxGridView1.Selection.IsRowSelected(i))
                    {
                        string POANumber = ASPxGridView1.GetRowValues(i, "POA_NUMBER").ToString();
                        logic.Say("PoA.ConfirmationDelete_Click", "PoA {0}", POANumber);
                        visibleIndex = i;
                        //isEditing = false;
                        PageMode = Common.Enum.PageMode.View;
                        Session["visibleIndex"] = i;
                        //ok = logic.PoA.DeletePOA(POANumber, ref Err);
                        ok = logic.PoA.DeactivatePoa(POANumber, UserData.USERNAME);
                        if (!ok)
                        {
                            setMessage("MSTD00014ERR");
                            ASPxGridView1.Selection.UnselectAll();
                            break;
                        }
                    }
                }

                if (ok)
                {
                    setMessage("MSTD00015INF");
                    ASPxGridView1.DataBind();
                    ASPxGridView1.Selection.UnselectAll();
                }
            }
            else if (ASPxGridView1.Selection.Count == 0)
            {
                setMessage("MSTD00009WRN");
            }
        }

        protected void btnCancel_Click1(object sender, EventArgs e)
        {
            PageMode = Common.Enum.PageMode.View;
            ASPxGridView1.CancelEdit();
            setMessage("");
            SetCancelEditDetail();
        }

        private void SetCancelEditDetail()
        {
            RoleLogic role = new RoleLogic(UserName, _ScreenID);
            SetSearch(true);
            btnSave.Visible = false;
            btnCancel.Visible = false;
            btnClose.Visible = true;
            btnSearch.Visible = true;
            btnClear.Visible = true;
            btnAdd.Visible = role.isAllowedAccess("btnAdd");
            if (ASPxGridView1.VisibleRowCount > 0)
            {
                ErrorData Err = null;
                btnEdit.Visible = role.isAllowedAccess("btnEdit", ref Err);
                btnDelete.Visible = role.isAllowedAccess("btnDelete", ref Err);
            }
            else
            {
                btnEdit.Visible = false;
                btnDelete.Visible = false;
            }
        }

        private void LoadGrid()
        {
            ErrorData err = null;
            ASPxGridView1.Selection.UnselectAll();
            ASPxGridView1.DataSource = logic.PoA.searchPoA(txtPOANumber.Text.Trim(),
                    txtGrantorOfAttorney.Text.Trim(), ddlAttorney.SelectedValue.Trim(),
                    ddlOfficeStatus.SelectedValue.Trim(), (dtValidFrom.Text.Trim().isEmpty()) ? (DateTime?)null : dtValidFrom.Date,
                    (dtValidTo.Text.Trim().isEmpty()) ? (DateTime?)null : dtValidTo.Date, UserName, ref err);
            if (err != null)
                setMessage(err.ErrMsg, err.ErrMsgID);
            SetCancelEditDetail();
            ASPxGridView1.DataBind();
        }

        protected void ASPxGridView1_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            if (isEditing)
            {
                if (e.RowType == GridViewRowType.InlineEdit)
                {
                    e.Row.BackColor = System.Drawing.Color.LightGray;
                }
            }
        }

        protected void ASPxGridView1_HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
        {
            if (e.DataColumn.FieldName == "OFFICE_STATUS")
            {
                if ((bool)e.CellValue)
                    e.Cell.BackColor = System.Drawing.Color.Green;
                else
                    e.Cell.BackColor = System.Drawing.Color.Red;
            }
        }

        private void setMessage(string MessageID, params object[] x)
        {
            if (string.IsNullOrEmpty(MessageID))
                litMessage.Text = "";
            else
            {
                if (x == null)
                    litMessage.Text = _m.SetMessage(resx.Message(MessageID), MessageID);
                else
                    litMessage.Text = _m.SetMessage(string.Format(resx.Message(MessageID), x), MessageID);
            }
        }

        protected void ASPxGridView1_PageIndexChanged(object sender, EventArgs e)
        {
            LoadGrid();
        }

        protected void dsAttorney_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            List<string> a = logic.PoA.getAttorneyUsernamePoa(UserName);
            a.Insert(0, "");
            e.Result = logic.PoA.UserNameFull(a);
        }

        protected void dsGrantor_Selecting(object sender, LinqDataSourceSelectEventArgs e)
        {
            List<string> a = logic.PoA.getGrantorUsernamePoa(UserName);
            a.Insert(0, "");
            e.Result = logic.PoA.UserNameFull(a);
        }

        protected void ddlGrantor_Init(object sender, EventArgs e)
        {
            ASPxComboBox o = sender as ASPxComboBox;
            if (PageMode == Common.Enum.PageMode.Add)
            {
                o.ReadOnly = false;
                o.Text = PoALogic.roleAllowedEdit(LoginUserRole) 
                            ? "" : UserName; // only secretary can grant for other 
                o.ReadOnly = !logic.PoA.canChangeGrantor(UserName);
                o.Enabled = !o.ReadOnly;

            }
            else if (PageMode == Common.Enum.PageMode.Edit)
            {
                o.ReadOnly = true;
                o.Enabled = false;
            }

        }

        protected void dgAttorney_Init(object sender, EventArgs e)
        {
            ASPxComboBox o = sender as ASPxComboBox;
            if (PageMode == Common.Enum.PageMode.Add)
            {
                o.Enabled = true;
            }
            if (PageMode == Common.Enum.PageMode.Edit)
            {
                o.Enabled = false;
            }

        }

    }

}