﻿function Classy(stat) {
    switch (stat) {
        case 1:
            return "Approved";
        case 2:
            return "Rejected";
        default:
            return "Waiting";
    }
}

function Distribusy(x) {
    switch (x) {
        case 1:
            return "Skipped";
        case 2:
            return "Concurred"
        default:
            return "";
    }
}

function DateFmt() {
    this.dateMarkers = {
        d: ['getDate', function (v) { return ("0" + v).substr(-2, 2) } ],
        m: ['getMonth', function (v) { return ("0" + (v + 1)).substr(-2, 2) } ],
        n: ['getMonth', function (v) {
            var mthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
            return mthNames[v];
        } ],
        w: ['getDay', function (v) {
            var dayNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
            return dayNames[v];
        } ],
        y: ['getFullYear'],
        H: ['getHours', function (v) { return ("0" + v).substr(-2, 2) } ],
        M: ['getMinutes', function (v) { return ("0" + v).substr(-2, 2) } ],
        S: ['getSeconds', function (v) { return ("0" + v).substr(-2, 2) } ],
        i: ['toISOString', null]
    };

    this.format = function (date, fmt) {
        var dateMarkers = this.dateMarkers
        var dateTxt = fmt.replace(/%(.)/g, function (m, p) {
            var rv = date[(dateMarkers[p])[0]]()

            if (dateMarkers[p][1] != null) rv = dateMarkers[p][1](rv)

            return rv
        });

        return dateTxt
    }
}

function Decodate(x) {
    if (x != null) {
        var fmt = new DateFmt();

        var d = getDateFromAspString(x);
        var df = fmt.format(d, "%d %n %y %H:%M");
        return df;
    } else {
        return "";
    }
}

function getDateFromAspString(aspString) {
    var epochMilliseconds = aspString.replace(/^\/Date\(([0-9]+)([+-][0-9]{4})?\)\/$/, '$1');
    if (epochMilliseconds != aspString) {
        return new Date(parseInt(epochMilliseconds));
    }
}

function GetApprov(docNo, docYear, level, userName, statusCd, gridName) {
    
    if (docNo == null || docYear == null || docNo.length < 1 || docYear.length < 1 || userName.length < 1 || gridName.length < 1)
        return;
    if (parseInt(docNo) == NaN || parseInt(docYear) == NaN || parseInt(statusCd) == NaN)
        return;
    if (docNo.length > 10 || docYear.length > 4)
        return;

    $.ajax({
        type: 'POST',
        url: '/ws.asmx/approvalOverview',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        data: JSON.stringify({ 'docNo': docNo, 'docYear': docYear, 'level': level, 'userName': userName, 'statusCode': statusCd }),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-type",
                           "application/json; charset=utf-8"); 
        },
        success: function (dx) {
            
            var q = dx.d;
            if (q != null) {
                $("#" + gridName + " > tbody").html("");
                for (var i = 0; i < q.length; i++) {
                    var qx = q[i];
                    $("#" + gridName + "> tbody:last").append('<tr class="dxgvDataRow"><td class="dxgv CodeRow">' + qx.CODE + '</td><td class="dxgv">' +
                                qx.FULL_NAME + '</td><td class="dxgv ' + Classy(qx.STATUS) + '">' + qx.STATUS + '</td>' +
                                '<td class="dxgv DateCol ' + Distribusy(qx.SKIP_FLAG) + '">' + Decodate(qx.ACTUAL_DT) + '</td></tr>');
                }
            } else {
                $('#OverviewMsg').html("");
            }
        },

        error: function (r, er) {
          
            $('#OverviewMsg').html("");
        }
    });
}

function isElementVisible(aname) {
    var x = $('#' + aname);
    if (x == null) return 0;

    var pos = x.offset(),
            wX = $(window).scrollLeft(), wY = $(window).scrollTop(),
            wH = $(window).height(), wW = $(window).width(),
            oH = x.outerHeight(), oW = x.outerWidth();

    // check the edges
    // left, top and right, bottom are in the viewport
    if (pos.left >= wX && pos.top >= wY &&
            oW + pos.left <= wX + wW && oH + pos.top <= wY + wH)
        return 1; // alert('Div #' + $(this).attr('id') + ' is fully visible');
    else // partially visible   
        if (((pos.left <= wX && pos.left + oW > wX) ||
             (pos.left >= wX && pos.left <= wX + wW)) &&
            ((pos.top <= wY && pos.top + oH > wY) ||
             (pos.top >= wY && pos.top <= wY + wH)))
            return 1; // alert('Div #' + $(this).attr('id') + ' is partially visible');
        else // not visible 
            return 0; // alert('Div #' + $(this).attr('id') + ' is not visible');
    }

    function findArea(id) {
        var ele = "#" + id;
        var ot = $(ele).offset().top;
        var wt = $(window).scrollTop();
        var ob = ot + $(ele)[0].scrollHeight;
        var wb = $(window).scrollTop() + $(window).height();
        if (wt > ob) {
            return -2; // alert("Element hidden (above viewable area)");
        }
        else if (wb < ot) {
            return 2; // alert("Element hidden (below viewable area)");
        }
        else if (ot >= wt && ob <= wb) {
            return 0; //alert("Element visible! (within viewable area)");
        }
        else {
            return 1;
        }
    }
    function isOnScreen(element) {
        var curPos = element.offset();
        var curTop = curPos.top;
        var screenHeight = $(window).height();
        return (curTop > screenHeight) ? false : true;
    }
