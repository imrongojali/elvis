var loadedWindows = new Array();
var width = 1024;
var height = 690;
var winleft = (screen.width - width) / 2;
var wintop = (screen.height - height) / 2;
var winleftattach = (screen.width - 700) / 2;
var wintopattach = (screen.height - 400) / 2;

// Popup Home
function openHome() {
    popupWindow = window.open("../../../Home.aspx", "Home", 
        'scrollbars=1,menubar=0,resizable=0,status=0,directories=0,location=0,' +  
        'toolbar=0,width=' + width + ',height=' + height + ',left=' + winleft);
    popupWindow.registerID = name;
    loadedWindows[loadedWindows.length] = popupWindow;
    popupWindow.focus(); /// <reference path="../../../Home.aspx" />
}
    
    // Popup window
function openWin(url, name) {
    //alert("aduh2");
    popupWindow = window.open(url, name, 
        'scrollbars=1,menubar=0,resizable=0,status=0,directories=0,location=0,' + 
        'toolbar=0,width='+width+',height='+height+',left='+winleft+'');
    popupWindow.registerID = name;
    loadedWindows[loadedWindows.length] = popupWindow;
    popupWindow.focus();
}
// Popup Window Attachment Budget Planning Detail
function openWinAttachment(url, name) {
    //alert("aduh3");
    popupWindow = window.open(url, name, 
        'scrollbars=1,menubar=0,resizable=0,status=0,directories=0,location=0,'+
        'toolbar=0,width=700,height=400,left=' + winleftattach + 
        ',top=' + wintopattach + '');
    popupWindow.registerID = name;
    loadedWindows[loadedWindows.length] = popupWindow;
    popupWindow.focus();
}

// Popup Window From Login Button
function openWinLogin(url, name) {
    //alert("aduh4");
    popupWindow = window.open(url, name, 
        'scrollbars=1,menubar=0,resizable=0,status=0,directories=0,location=0,' +
        'toolbar=0,width=' + width + ',height=' + height + ',left=' + winleft + '');
    popupWindow.focus();
    window.open("", "_self");
    window.close();
}

function Logout() {
    var i = 0; // 0 itu bisa close, 1 itu ga bisa close (edit mode)
    for (var j = 0; j < loadedWindows.length; j++) {
        try {
            loadedWindows[j].close();
        } catch (err) {
        }
    }
    if (i == 0) {
        var _p; 
        _p = opener;
        if (_p == null) {
            self.close();
        } else {
            try
            {
                _p.Logout();
            } catch(err){}
        }
        self.close();
            
    }
}

//read cookie
function getCookie(c_name) {
    var i, x, y, ARRcookies = document.cookie.split(";");
    for (i = 0; i < ARRcookies.length; i++) {
        x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
        y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
        x = x.replace(/^\s+|\s+$/g, "");
        if (x == c_name) {
            return unescape(y);
        }
    }
}

//close child Window, close button
function closeWin() {
    var i = 0; // 0 itu bisa close, 1 itu ga bisa close (edit mode)
    for (var x = 0; x < loadedWindows.length; x++) {
        try {
            for (var y = 0; y < loadedWindows[x].loadedWindows.length; y++) {
                try {
                    if (getCookie(loadedWindows[x].loadedWindows[y].registerID) == 1) {
                        i = 1;
                        break;
                    }
                }
                catch (err) { }
            }
            if (getCookie(loadedWindows[x].registerID) == 1) {
                i = 1;
                break;
            }
        } catch (err2) { }
    }
    if (i == 0) {
        for (var x = 0; x < loadedWindows.length; x++) {
            try {
                for (var y = 0; y < loadedWindows[x].loadedWindows.length; y++) {
                    try {
                        loadedWindows[x].loadedWindows[y].close();
                    }
                    catch (err) { }
                }
                loadedWindows[x].close();
            } catch (err2) { }
        }
        self.close();
    }
}

