﻿var __selectedListText_SEPARATOR = ";";

function GetSelectedListItemsText(items) {
    var texts = [];
    for (var i = 0; i < items.length; i++) {
        texts.push(items[i].text);
    }   
            
    return texts.join(__selectedListText_SEPARATOR + ' ');
}

function formatDate(date, format) {
     if (!format)
        format = "MM/dd/yyyy";
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    var m_names = new Array("January", "February", "March",
    "April", "May", "June", "July", "August", "September",
    "October", "November", "December");

    var m_short = new Array("Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
    "Oct", "Nov", "Dec");
    format = format.replace("MMMM", m_names[month - 1]);
    format = format.replace("MMM", m_short[month -1]);

    format = format.replace("MM", padL(month.toString(), 2, "0"));
    if (format.indexOf("yyyy") > -1)
        format = format.replace("yyyy", year.toString());
    else if (format.indexOf("yy") > -1)
        format = format.replace("yy", year.toString().substr(2, 2));
    format = format.replace("dd", padL(date.getDate().toString(), 2, "0"));

    var hours = date.getHours();
    if (format.indexOf("t") > -1) {
        if (hours > 11)
            format = format.replace("t", "pm")
        else
            format = format.replace("t", "am")
    }
    if (format.indexOf("HH") > -1)
        format = format.replace("HH", padL(hours.toString(), 2, "0"));

    if (format.indexOf("hh") > -1) {
        if (hours > 12) hours - 12;
        if (hours == 0) hours = 12;
        format = format.replace("hh", padL(hours.toString(), 2, "0"));
    }

    if (format.indexOf("mm") > -1)
        format = format.replace("mm", padL(date.getMinutes().toString(), 2, "0"));

    if (format.indexOf("ss") > -1)
        format = format.replace("ss", padL(date.getSeconds().toString(), 2, "0"));
    return format;
}

repeat = function (chr, count) {
    var str = "";
    for (var x = 0; x < count; x++) { str += chr };
    return str;
}

padL = function (s, width, pad) {
    if (!width || width < 1)
        return s;

    if (!pad) pad = " ";
    var length = width - s.length
    if (length < 1) return s.substr(0, width);
    return (repeat(pad, length) + s).substr(0, width);
}

padR = function (s, width, pad) {
    if (!width || width < 1)
        return s;
    if (!pad) pad = " ";
    var length = width - s.length
    if (length < 1) this.substr(0, width);

    return (s + String.repeat(pad, length)).substr(0, width);
}

function closeWindow(hidPageMode) {
    //var PageMode = document.getElementById(hidPageMode).value;
    if (hidPageMode == 0) {
        var answer = confirm("Do you want to close this page?");
        if (answer) {
            closeWin();
        }
    }
}
function ReloadOpener() {
    if (opener != undefined && opener.location != undefined)
        opener.needReload = true;
    // opener.location.reload();
}
function loading() {
   
    if (typeof Callback != 'undefined') {
        
        Callback.PerformCallback();
    }
  
    else {
        console.log('No Callback');
    }
    if (typeof LoadingPanel != 'undefined') {
      
        LoadingPanel.Show();
    }
    else {
        console.log('No LoadingPanel');
    }
}

function clearComment() {
    document.getElementById("txtApprovalComment").value = "";
}

function approvalProceed() {
    loading();

    var btn = document.getElementById('btnApprovalProceedServer');
    btn.click();
    return false;
}

function HideBody() {
    $("#mainbody").addClass("hidden");
}
function OnDateValid(s, e) {
    if (e.value) {
        var d = e.value;
        e.isValid = (d >= new Date(1900, 1, 1) && d <= new Date(9999, 12, 31));

    } else
        e.isValid = true;
}

function OnDateChanged(s, e) {

    var myname = s.name;
    var mydate = s.date;
    if (!mydate) { return; }

    var yourname = "";
    var isFrom = myname.lastIndexOf("From");
    if (isFrom > 0) {
        yourname = myname.substr(0, isFrom) + "To";
    }
    else {
        yourname = myname.substr(0, myname.length - 2) + "From";
    }

    var oPair = eval(yourname);
    var yourdate = oPair.GetDate();

    if (yourdate == null) {
        oPair.SetDate(mydate);
        return;
    }
    if ((isFrom > 0 && mydate > yourdate) || (mydate < yourdate && isFrom < 0)) {
        var d = yourdate;

        oPair.SetDate(mydate);
        s.SetDate(d);
    }
}

function updateTime() {
    var now = new Date();

    var timelabel = document.getElementById("minuteTimer");
    if (timelabel) timelabel.innerHTML = moment().format("DD MMM YYYY HH:mm") + "&nbsp;&nbsp;|&nbsp;";
}

function OnWindow_Load() {
    var fullnameLabel = document.getElementById("litUser");
    var hasFullname = document.getElementById("hidFullName");
    if (fullnameLabel && hasFullname) {
        if (hasFullname.value)
            fullnameLabel.innerHTML = hasFullname.value;
        else {
            fullnameLabel.innerHTML = "&nbsp;"
        }
    } else {
        
    }
    updateTime();
    window.setInterval(updateTime, 7559);
}

if (window.addEventListener) // W3C standard
{
    window.addEventListener('load', OnWindow_Load, false); 
}
else if (window.attachEvent) // Microsoft
{
    window.attachEvent('onload', OnWindow_Load);
}

