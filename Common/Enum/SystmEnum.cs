﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Enum
{
    [Serializable]
    public enum PageMode
    {
        View,
        Add,
        Edit
    }

    public enum ModalButton
    {
        TransactionType,
        BudgetNo,
        VendorCd,
        CostCenterCd,
    }

    public enum EmailType
    {
        BudgetPlaningHeader,
        BudgetPlaningItem,
        EoA
    }
    [Serializable]
    public enum ApprovalEnum
    {
        Approve=1,
        Reject=2,
        Hold=3,
        UnHold=4, 
        Idle=0
    }
    public enum SAPMode
    { 
        Post,
        Check,
    }
    public enum DataSAPMode
    { 
        GL,
        Vendor
    }
}
