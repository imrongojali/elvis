﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Messaging
{
    [Serializable]
    public class ScreenMessage
    {        
        public const byte STATUS_INFO = 0;
        public const byte STATUS_WARNING = 1;
        public const byte STATUS_ERROR = 2;

        public ScreenMessage(byte status, string message) : this(status, message, null)
        {
        }
        public ScreenMessage(byte status, string message, string[] arguments)
        {
            this.status = status;
            this.message = message;
            this.arguments = arguments;
        }

        private string message;
        public string Message
        {
            set
            {
                message = value;
            }

            get
            {
                if (arguments != null)
                {
                    return string.Format(message, arguments);
                }   
                return message;
            }
        }

        private byte status;
        public byte Status
        {
            set
            {
                status = value;
            }

            get
            {
                return status;
            }
        }

        private string[] arguments;
        public string[] Arguments
        {
            set
            {
                arguments = value;
            }

            get
            {
                return arguments;
            }
        }
    }
}
