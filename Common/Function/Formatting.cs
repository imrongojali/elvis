﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Common.Function
{
    public static class Formatting
    {
        public const string VIEW_CURRENCY = "#,##0.00";
        public const string VIEW_CURRENCYIna = "#,##0";
        public const string VIEW_DateIna = "dd-MMMM-yyyy";
        public const string VIEW_DATE = "dd-MM-yyyy";
        public const string SQL_DATE = "yyyy-MM-dd";
        public const string SQL_DATETIME = "yyyy-MM-dd hh:mm:ss tt";
        public const string COUNTER_DATE = "MMM yyyy";
        public const string FULL_DATE = "dddd, dd MMM yyyy HH:mm";
        public const string VIEW_TIME = "h:mm tt";
        public const string DEFAULT_DATE_FROM = "01-01-1900";
        public const string DEFAULT_DATE_TO = "31-12-9999";
        public const string DEFAULT_TIME_FROM = "0:00 AM";
        public const string DEFAULT_TIME_TO = "11:59 PM";
        public const string FILE_TIMESTAMP = "yyyyMMdd_hhmmsstt_";
        public const string FULL_DAY = "dd MMMM yyyy";

        public const string NOT_AVAILABLE = "N/A";


        public static string FormatDefaultDateWhenNullOrEmpty(this string value, string paramType = "FROM")
        {
            return string.IsNullOrEmpty(value) ? (paramType.ToUpper().Equals("FROM") ? DEFAULT_DATE_FROM : DEFAULT_DATE_TO) : value;
        }

        /// <summary>
        /// Default <b>Time</b> String when value passed is NULL or empty string
        /// </summary>
        /// <param name="value"></param>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public static string FormatDefaultTimeWhenNullOrEmpty(this string value, string paramType = "FROM")
        {
            return string.IsNullOrEmpty(value) ? (paramType.ToUpper().Equals("FROM") ? DEFAULT_TIME_FROM : DEFAULT_TIME_TO) : value;
        }

        public static string FormatViewCurrency(this decimal value, string decimalformat = VIEW_CURRENCY)
        {
            return value.ToString(decimalformat, new CultureInfo("en-US"));
        }

        public static string FormatViewCurrencyiNA(this decimal value, string decimalformat = VIEW_CURRENCYIna)
        {
            return value.ToString(decimalformat, new CultureInfo("id-id"));

            //return String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
        }

        public static string FormatdatetimeiNA(this DateTime value, string dateformat = VIEW_DateIna)
        {
            return value.ToString(dateformat, new CultureInfo("id-id"));

            //return String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
        }

        public static string FormatViewCurrencyNullable(this Nullable<decimal> value, string decimalformat = VIEW_CURRENCY)
        {
            if (value.HasValue)
            {
                return string.Format("{0:" + decimalformat + "}", value);
            }
            else
            {
                return "";
            }
        }

        public static string FormatViewTime(this DateTime datum, string dateformat = VIEW_TIME)
        {
            return datum.ToString(dateformat, new CultureInfo("en-US"));
        }

        public static string FormatViewDate(this DateTime datum, string dateformat = VIEW_DATE)
        {
            string value = datum.ToString(dateformat);
            return value.Contains("0001") ? string.Empty : value;
        }

        public static string FormatViewDate(this string datum, string dateformat = VIEW_DATE)
        {
            try
            {
                if (datum.Contains("-"))
                {
                    datum = datum.Replace("-", "/");
                }
                DateTime d = DateTime.ParseExact(datum, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                return d.ToString(dateformat);
            }
            catch (Exception)
            {

            }
            return datum;

        }
        public static string FormatNumberIna(this string value, string decimalformat = VIEW_CURRENCYIna)
        {
            return decimal.Parse(value).ToString(decimalformat, new CultureInfo("en-US"));
        }
        public static string FormatNumber(this string value, string decimalformat = VIEW_CURRENCY)
        {
            return decimal.Parse(value).ToString(decimalformat, new CultureInfo("en-US"));
        }

        public static string FormatSQLDate(this DateTime datum, string dateformat = SQL_DATE)
        {
            return datum.ToString(dateformat);
        }

        public static string FormatSQLDateTime(this DateTime datum, string dateformat = SQL_DATETIME)
        {
            return datum.ToString(dateformat, CultureInfo.InvariantCulture);
        }

        public static string FormatSQLDate(this string datum, string dateformat = SQL_DATE)
        {
            DateTime d = new DateTime(1900, 1, 1);
            string v = "";

            if (DateTime.TryParseExact(datum,
                                       VIEW_DATE,
                                       System.Globalization.CultureInfo.InvariantCulture,
                                       System.Globalization.DateTimeStyles.None,
                                       out d))
            { v = d.ToString(dateformat); }
            else
            {
                v = "";
            }

            return v;
        }

        public static string FormatSQLDateTime(this string datum, string dateformat = SQL_DATETIME)
        {
            DateTime d = new DateTime(1900, 1, 1);
            string v = "";

            if (DateTime.TryParseExact(datum,
                                       VIEW_DATE,
                                       System.Globalization.CultureInfo.InvariantCulture,
                                       System.Globalization.DateTimeStyles.None,
                                       out d))
            { v = d.ToString(dateformat); }
            else
            {
                v = "";
            }

            return v;
        }

        public static string FormatFullDate(this DateTime datum, string dateformat = FULL_DATE)
        {
            return datum.ToString(dateformat);
        }

        //add by rg 07102020
        public static string FormatFullDay(this DateTime datum, string dateformat = FULL_DAY)
        {
            return datum.ToString(dateformat, new CultureInfo("id-ID"));
        }



        public static string FormatCounterDate(this string datum, string dateformat = COUNTER_DATE)
        {
            DateTime d = Convert.ToDateTime(datum);
            return d.ToString(dateformat);
        }
        public static string FormatNomorManualFakturGabungan(this string value)
        {
            value = value.Replace(".", "").Replace("-", "");
            double x;
            if (value.Length != 16)
            {
                throw new ApplicationException("Nomor Faktur Format must be 13 characters (plain)");
            }

            if (!double.TryParse(value, out x))
            {
                throw new ApplicationException("Nomor Faktur Format must be numeric characters");
            }
           

            //111.222-33.44444444 010.000-17.00365379
            string a = value.Substring(0, 3);
            string b = value.Substring(3, 3);
            string c = value.Substring(6, 2);
            string d = value.Substring(8, 8);

            return a + "." + b + "-" + c + "." + d;
        }
        public static string FormatNomorFakturGabungan(this string value, string KDJenisTransaksi = "", string FGPengganti = "")
        {
            value = value.Replace(".", "").Replace("-", "");
            double x;
            if (value.Length != 13)
            {
                throw new ApplicationException("Nomor Faktur Format must be 13 characters (plain)");
            }

            if (!double.TryParse(value, out x))
            {
                throw new ApplicationException("Nomor Faktur Format must be numeric characters");
            }
            value = KDJenisTransaksi + FGPengganti + value;

            //111.222-33.44444444
            string a = value.Substring(0, 3);
            string b = value.Substring(3, 3);
            string c = value.Substring(6, 2);
            string d = value.Substring(8, 8);

            return a + "." + b + "-" + c + "." + d;
        }

        /// <summary>
        /// Stripping off comma and dash symbols, 
        /// and delete 3 first characters from NomorFakturGabungan
        /// to get plain NomorFaktur
        /// </summary>
        /// <param name="value"></param>
        /// <returns>NomorFaktur</returns>
        public static string FormatNomorFakturPolos(this string value)
        {
            value = value.Replace(".", "").Replace("-", ""); //strip-off symbols
            value = value.Substring(3, value.Length - 3); //delete 3 first characters (KDJenisTransaksi & FGPengganti)

            return value;
        }

        public static string FormatNPWP(this string value)
        {
            value = value.Replace(".", "").Replace("-", ""); //strip-off symbols
            double x = 0;
            if (value.Length != 15)
            {
                throw new ApplicationException("NPWP Format must be 15 characters (plain) or 20 characters (with symbol)");
            }

            if (!double.TryParse(value, out x))
            {
                throw new ApplicationException("NPWP Format must be numeric characters");
            }

            //11.222.333.4-555.666
            string a = value.Substring(0, 2);
            string b = value.Substring(2, 3);
            string c = value.Substring(5, 3);
            string d = value.Substring(8, 1);
            string e = value.Substring(9, 3);
            string f = value.Substring(12, 3);

            return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
        }

    }
       
}
