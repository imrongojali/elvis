﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Resources;
using System.Reflection;

namespace Common.Function
{
    public class GlobalResourceData
    {
        private ResourceManager _rescManager = null;
        private string _base = "Message"; 

        public GlobalResourceData()
        {
            _rescManager = new ResourceManager("Common.App_GlobalResource." + _base, Assembly.GetExecutingAssembly());
        }

        public string GetResxObject(string ResourceKey)
        {
            return _rescManager.GetString(ResourceKey);
        }

        public string GetResxObject(string ClassName, string ResourceKey)
        {
            // string _strBaseName = "Common.App_GlobalResource." + ClassName;
            // ResourceManager _rescManager = new ResourceManager(_strBaseName, Assembly.GetExecutingAssembly());
            if (!_base.Equals(ClassName))
            {
                _base = ClassName;
                _rescManager = new ResourceManager("Common.App_GlobalResource." + _base, Assembly.GetExecutingAssembly());
            }
            return _rescManager.GetString(ResourceKey);
        }

        public List<object> getFunctionList()
        {
            List<object> retVal = null;
            string _strBaseName = "Common.App_GlobalResource.Function";
            ResourceManager _rescManager = new ResourceManager(_strBaseName, Assembly.GetExecutingAssembly());
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            var res = _rescManager.GetResourceSet(culture, true, true);
            var resList = res.Cast<object>().ToList();
            retVal = (List<object>)resList;
            return retVal;
        }

        public string Combo(string query)
        {
            return GetResxObject("ComboQuery", query);
        }

        public string FunctionId(string functionName)
        {
            return GetResxObject("Function", functionName);
        }

        public string K2ProcID(string name)
        {
            return GetResxObject("K2ProcID", name);
        }

        public string Message(string id)
        {
            return GetResxObject("Message", id);
        }

        public string Screen(string id)
        {
            return GetResxObject("ScreenID", id);
        }

        public static string ScreenID(string ID)
        {
            return new GlobalResourceData().Screen(ID);
        }

        public string this[string index]
        {
            get
            {
                return _rescManager.GetString(index);
            }
        }
    }
}
