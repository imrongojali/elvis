﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Common
{
    public class Util
    {
        public static string Base52(long l)
        {
            const byte BASE = 52;
            const string a = "etaoinshrdlucmfwypvbgkjqxzETAOINSHRDLUCMFWYPVBGKJQXZ";
            StringBuilder x = new StringBuilder("");
            byte n;

            do
            {
                n = (byte)(l % BASE);
                x.Insert(0, a.Substring(n, 1));
                l = (l - n) / BASE;
            }
            while (l > 0);
            return x.ToString();
        }

        public static string Base26(long l)
        {
            string x = "";
            long n;
            while (l > 0)
            {
                n = (l - 1) % 26;
                x = Convert.ToChar(n + 65).ToString() + x;
                l = (l - n) / 26;
            }
            return x;
        }

        public static string Base26(int i)
        {
            string x = "";
            int n;
            while (i > 0)
            {
                n = (i - 1) % 26;
                x = Convert.ToChar(n + 65).ToString() + x;
                i = (i - n) / 26;
            }
            return x;
        }

        public static string GetTmp(string path, string prefix, string ext)
        {
            string fn = "";
            string addi = "";
            int i = 0;
            fn = Path.Combine(ExpandTimeVars(path), ExpandTimeVars(prefix) + addi + ext);
            while (File.Exists(fn) && i < 52)
            {
                i++;
                addi = Base26(i);
                fn = Path.Combine(ExpandTimeVars(path), ExpandTimeVars(prefix) + addi + ext);
            }
            return fn;
        }

        public static string ExpandTimeVars(string s)
        {
            s = s.Replace("%DATE%", DateTime.Now.ToString("ddMMyyyy"));
            s = s.Replace("%TIME%", DateTime.Now.ToString("HHmmss"));
            s = s.Replace("%MS%", DateTime.Now.Millisecond.ToString("000"));
            return s;
        }

        public static void ReMoveFile(string n)
        {
            if (File.Exists(n))
            {
                int i = 0;
                string newname;
                do
                {
                    string ss = "_"
                        + Base52(
                            (int)Math.Round(DateTime.Now.TimeOfDay.TotalSeconds))
                        + Base52(
                            (int)Math.Round((double)DateTime.Now.Millisecond / 38)
                            )
                        + Base52(i++);
                    newname = Path.Combine(Path.GetDirectoryName(n), Path.GetFileNameWithoutExtension(n) + ss + Path.GetExtension(n));
                } while (File.Exists(newname));
                File.Move(n, newname);
            }
        }

        public static string[] Chop(string x, int MaxLength)
        {
            List<string> r = new List<string>();

            while (x.Length > MaxLength)
            {
                int k = MaxLength;
                while (k > 0 && !char.IsWhiteSpace(x[k - 1]))
                    --k;
                if (k <= 0 && x.Length > MaxLength)
                {
                    k = MaxLength;
                }
                r.Add(x.Substring(0, k));
                x = x.Substring(k);
            }
            // when string is shorter  than max length, then return string 
            if (x.Length > 0)
            {
                r.Add(x);
            }
            return r.ToArray();
        }

        public static string Hash(string data, int method = 2)
        {
            byte[] x = GetBytes(data);
            UInt32 a = 0;
            switch (method)
            {
                case 3:
                    a = MurMur.MurmurHash3(x);
                    break;
                default:
                    a = MurMur.MurmurHash2(x);
                    break;
            }
            return Base26((long) a);
        }

        public static byte[] GetBytes(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}
