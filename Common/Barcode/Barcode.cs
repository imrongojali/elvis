﻿using QRCoder;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Common.Barcode
{
    public class Barcode
    {
        public string GenerateQrCode(string text)
        {
            byte[] imgArray;

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            using (MemoryStream imageStream = new MemoryStream())
            {
                qrCodeImage.Save(imageStream, ImageFormat.Png);
                imgArray = new byte[imageStream.Length];
                imageStream.Seek(0, SeekOrigin.Begin);
                imageStream.Read(imgArray, 0, (int)imageStream.Length);
            }

            return Convert.ToBase64String(imgArray);
        }
    }
}
