﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Common.Control
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ExtendedImage runat=\"server\"></{0}:ExtendedImage>")]
    public class ExtendedImage : DevExpress.Web.ASPxEditors.ASPxImage, IPostBackEventHandler
    {
        public ExtendedImage() : base()
        {
        }
        
        public string Key { get; set; }

        public delegate void dlgOnClick(object sender, EventArgs args);
        public event dlgOnClick Click;

        public void RaisePostBackEvent(string eventArgument)
        {
            if(Click != null)
            {
                Click(this, EventArgs.Empty);
            }
        }        

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            base.Attributes.Add("onclick", String.Format("__doPostBack(\"{0}\",'')", UniqueID));
        }
    }
}
