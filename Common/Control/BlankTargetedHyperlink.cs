﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Common.Control
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:BlankTargetedHyperlink runat=\"server\"></{0}:BlankTargetedHyperlink>")]
    public class BlankTargetedHyperlink : Label, IPostBackEventHandler
    {
        public BlankTargetedHyperlink() : base()
        {
        }

        [Bindable(true)]
        public string NavigateUrl
        {
            set
            {
                ViewState["NavigateUrl"] = value;
            }
            get
            {
                string url = (string)ViewState["NavigateUrl"];
                return (url == null) ? String.Empty : url;
            }
        }

        public void RaisePostBackEvent(string argument) {}

        protected override void AddAttributesToRender(HtmlTextWriter writer)
        {
            writer.AddAttribute(HtmlTextWriterAttribute.Onclick, String.Format("openWin(\"{0}\",\"{1}\")", NavigateUrl, ""));
            writer.AddAttribute(HtmlTextWriterAttribute.Style, "text-decoration: underline; cursor: pointer; color: #A36565;");
            base.AddAttributesToRender(writer);
        }
    }
}
