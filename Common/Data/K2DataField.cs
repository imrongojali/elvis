﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;

namespace Common.Data
{
    public class K2DataField
    {
        public string Command { get; set; }
        public string ApplicationID { get { return AppSetting.ApplicationID; } }
        public string ApprovalLevel { get; set; }
        public string ApproverClass { get; set; }
        public string ApproverCount { get; set; }
        public string CurrentDivCD { get; set; }
        public string CurrentPIC { get; set; }
        public string EmailAddress { get; set; }
        public string Entertain { get; set; }
        public string K2Host { get; set; }
        public string LimitClass { get; set; }
        public string ModuleCD { get; set; }
        public string NextClass { get; set; }
        public string PIC { get; set; }
        public string Reff_No { get; set; }
        public string RegisterDt { get; set; }
        public string RejectedFinance { get; set; }
        public string StatusCD { get; set; }
        public string StepCD { get; set; }
        public string VendorCD { get; set; }
        public string folio { get; set; }

        private string _totalAmount = "0";
        public string TotalAmount
        { 
            get
            {
                return _totalAmount;
            }
            set 
            { 
                decimal tot = 0;
                if (!decimal.TryParse(value, out tot))
                    tot = 0;

                if (tot > int.MaxValue) tot = int.MaxValue;
                if (tot < int.MinValue) tot = int.MinValue;

                _totalAmount = Math.Round(tot).ToString();
            }
        }
        public bool isResubmitting { get; set; }
    }
}
