﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class ScreenData
    {
        public string APPLICATION_ID { get; set; } 

        public string REG_NO { get; set; } 

        public string USERNAME { get; set; } 

        public string SCREEN_ID { get; set; } 

        public string MENU_ID { get; set; } 

        public string CAPTION { get; set; } 

        public int POSITION { get; set; } 

        public string PARRENT_ID { get; set; } 

        public string PATH { get; set; }

        public string ROLE_ID { get; set; }

        public string ROLE_NAME { get; set; } 

        
        public List<ScreenData> ChildList { get; set; } 
    }
}
