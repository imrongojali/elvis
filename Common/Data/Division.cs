﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class Division
    {
        public string DivisionID { set; get; }
        public string DivisionName { set; get; }
        public string DivisionFullName { set; get; }
        public int ProductionFlag { set; get; } 
    }
}
