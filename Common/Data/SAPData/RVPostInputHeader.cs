﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class RVPostInputHeader
    {
        public string RV_NO { get; set; }
        public string RV_YEAR { get; set; }
        public string SEQ_NO { get; set; }
        public string RV_DATE { get; set; }
        public string RV_TYPE { get; set; }
        public string RV_NO_SUSPENSE { get; set; }
        public string TRANS_TYPE { get; set; }
        public string VENDOR { get; set; }
        public string VENDOR_GROUP { get; set; }
        public string INVOICE_NO { get; set; }
        public string TAX_NO { get; set; }
        public string PAYMENT_TERM { get; set; }
        public string PAYMENT_METHOD { get; set; }
        public string PLAN_PAYMENT_DATE { get; set; }
        public string POSTING_DATE { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string CURRENCY_CD { get; set; }
        public string TAX_CD { get; set; }
        public string HEADER_TEXT { get; set; }
        public string BANK_TYPE { get; set; }
        public string STATUS { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_YEAR { get; set; }
    }
}
