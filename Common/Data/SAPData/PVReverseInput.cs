﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PVReverseInput
    {
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }
        public string ITEM_NO { get; set; }
        public string POSTING_DT { get; set; }
        public string STATUS { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_YEAR { get; set; } 
    }
}
