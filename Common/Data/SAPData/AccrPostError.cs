﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class AccrSAPError
    {
        public string TABLE_NAME = "T_RETURN";
        public string TABLE_TYPE = "BAPIRET2"; 
        public string TYPE { get; set; }
        public string ID { get; set; }
        public string NUMBER { get; set; }
        public string MESSAGE { get; set; }
        public string LOG_NO { get; set; }
        public string LOG_MSG_NO { get; set; }
        public string MESSAGE_V1 { get; set; }
        public string MESSAGE_V2 { get; set; }
        public string MESSAGE_V3 { get; set; }
        public string MESSAGE_V4 { get; set; }
        public string PARAMETER { get; set; }
        public string ROW { get; set; }
        public string FIELD { get; set; }
        public string SYSTEM { get; set; }
        public string PROCESS_NAME { get; set; }

    }
}
