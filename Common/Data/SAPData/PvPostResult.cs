﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class PVPostResult
    {
        public string PV_NO { get; set; }
        public string PV_YEAR { get; set; }
        public string ITEM_NO { get; set; }
        public string MESSAGE_TEXT { get; set; }
    }
}
