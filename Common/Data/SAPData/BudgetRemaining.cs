﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data.SAPData
{
    [Serializable]
    public class BudgetRemaining
    {
        public string BUDGET_NO { get; set; }
        public decimal REMAINING_AMT { get; set; }
        public string REMAINING_STR { get; set; }
    }
}
