﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._20MasterData
{
    [Serializable]
    public class BankAccountData
    {
        public string vendor_cd { get; set; }
        public string vendor_name { get; set; }
        public string vendor_group_cd { get; set; }
        public string vendor_group_name { get; set; }
        public string search_terms { get; set; }
        public string name_of_bank { get; set; }
        public string bank_account { get; set; }
        public string status { get; set; }
        public string created_by { get; set; }
        public DateTime? created_dt { get; set; }

        public string beneficiaries { get; set; }
        public string directory { get; set; }
        public string file_name { get; set; }
        public string guid_filename { get; set; }
        public string url { get; set; }
    }
}
