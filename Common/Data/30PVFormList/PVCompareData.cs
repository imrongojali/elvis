﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVList
{
    [Serializable]
    public class PVCompareData
    {
        public Int64 RowNum { get; set; }
        public String ValidasiData { get; set; }
        public String INVOICE_NO { get; set; }
        public String TAX_NO { get; set; }
        public Decimal? DPP { get; set; }
        public Decimal? PPN { get; set; }
        public Decimal? PPH { get; set; }
        public String ValidasiMessage { get; set; }
    }
}
