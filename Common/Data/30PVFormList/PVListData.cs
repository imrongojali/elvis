﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._30PVList
{
    [Serializable]
    public class PVListData
    {
        readonly private string green = "#00ff00";
        readonly private string red = "#ff0000";
        readonly private string yellow = "#fffb00";

        private string settlement_status;
        public string SETTLEMENT_STATUS {
            get
            {
                return settlement_status;
            }
            set
            {
                settlement_status = value;
                if (settlement_status.Equals("0"))
                {
                    SETTLEMENT_STATUS_COLOR = red;
                }
                else if (settlement_status.Equals("1"))
                {
                    SETTLEMENT_STATUS_COLOR = green;
                }
            }
        }
        public string SETTLEMENT_STATUS_COLOR { get; set; }
        private string workflow_status;
        public string WORKFLOW_STATUS {
            get
            {
                return workflow_status;
            }
            set
            {
                workflow_status = value;
                if (workflow_status.Equals("0"))
                {
                    WORKFLOW_STATUS_COLOR = red;
                }
                else if (workflow_status.Equals("1"))
                {
                    WORKFLOW_STATUS_COLOR = green;
                }
            }
        }
        public string WORKFLOW_STATUS_COLOR { get; set; }

        //fid.pras 29/06/2018
        public string BUDGET_NO_FORMAT { get; set; }
        
        public string PV_NO { get; set; }
        public string PV_TYPE_CD { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public string REFF { get; set; }
        public string HOLD_BY_NAME { get; set; }
        public int? HOLD_FLAG { get; set; }
        public int? PRINT_TICKET_FLAG { get; set; }
        public string PRINT_TICKET_BY { get; set; } 
        public DateTime PV_DATE { get; set; }
        public DateTime? PAID_DATE { get; set; }
        public DateTime? PLANNING_PAYMENT_DATE { get; set; }
        public DateTime? SUBMIT_DATE { get; set; }
        public string AMOUNT_IDR { get; set; }
        public string AMOUNT_USD { get; set; }
        public string AMOUNT_JPY { get; set; }
        public string AMOUNT_SGD { get; set; }
        public string AMOUNT { get; set; }
        public ArrayList AMOUNT_OTHER { get; set; }
        public string AMOUNT_QUANTITY { get; set; }
        public decimal? TOTAL_AMOUNT_IDR { get; set; }
        public int? BANK_TYPE { get; set; }
        public string ISSUING_DIVISION { get; set; }
        public string DIVISION_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public string STATUS_CD { get; set; }
        public string LAST_STATUS { get; set; }
        // public bool NOTICE_FLAG { get; set; }
        public int? NOTICES { get; set; }
        public string SET_NO_PV { get; set; }
        public string SET_NO_RV { get; set; }
        public string VENDOR_CD { get; set; }
        public string VENDOR_NAME { get; set; }
        public string TRANSACTION_CD { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public string PAY_METHOD_CD { get; set; }
        public string PAY_METHOD_NAME { get; set; }
        //public string CALCULATE_TAX { get; set; }
        public string LT_STATUS_USER { get; set; }
        public string SUSPENSE_NO { get; set; }
        public string SUSPENSE_YEAR { get; set;  }
        // public DateTime ACTIVITY_DT { get; set; }
        public DateTime? ACTIVITY_DATE_FROM {get; set; }
        public DateTime? ACTIVITY_DATE_TO { get; set; } 
        public string LT_STATUS_FINANCE { get; set; }
        public string TOTAL_AMOUNT { get; set; }
        public string SAP_DOC_NO { get; set; }
        public string SAP_DOC_NO_LIST { get; set; }
        //public ArrayList SAP_DOC_NO_DETAIL { get; set; }
        public string SAP_DOC_NO_QUANTITY { get; set; }
        public string BUDGET_NO { get; set; }
        public string BUDGET_DESCRIPTION { get; set; } 
        public string BUDGET_NO_LIST { get; set; }
        //public ArrayList BUDGET_DETAIL { get; set; }
        //public string BUDGET_QUANTITY { get; set; }
        public string ATTACHMENT { get; set; }
        //public ArrayList ATTACHMENT_DETAIL { get; set; }
        public string ATTACHMENT_QUANTITY { get; set; }
        public DateTime? SUBMIT_HC_DOC_DATE { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CHANGED_DATE { get; set; }
        public string CHANGED_BY { get; set; }
        public string PV_YEAR { get; set; }
        public DateTime? REG_ACTUAL_DT { get; set; }

        public DateTime? PLAN_DT_SH { get; set; }
        public DateTime? ACTUAL_DT_SH { get; set; }
        public decimal? PLAN_LT_SH { get; set; }
        private decimal? actual_lt_sh;
        public decimal? ACTUAL_LT_SH { get; set;  }
        
        public DateTime? PLAN_DT_DPH { get; set; }
        public DateTime? ACTUAL_DT_DPH { get; set; }
        public decimal? PLAN_LT_DPH { get; set; }
        public decimal? ACTUAL_LT_DPH { get; set; } 
        
        public DateTime? PLAN_DT_DH { get; set; }
        public DateTime? ACTUAL_DT_DH { get; set; }
        public decimal? PLAN_LT_DH { get; set; }
        public decimal? ACTUAL_LT_DH {get; set;} 
        
        public DateTime? PLAN_DT_DIRECTOR { get; set; }
        public DateTime? ACTUAL_DT_DIRECTOR { get; set; }
        public decimal? PLAN_LT_DIRECTOR { get; set; }
        public decimal? ACTUAL_LT_DIRECTOR { get; set; } 
        
        public DateTime? PLAN_DT_VP { get; set; }
        public DateTime? ACTUAL_DT_VP { get; set; }
        public decimal? PLAN_LT_VP { get; set; }
        public decimal? ACTUAL_LT_VP { get; set; } 

        public DateTime? PLAN_DT_VERIFIED_COUNTER { get; set; }
        public DateTime? ACTUAL_DT_VERIFIED_COUNTER { get; set; }
        public decimal? PLAN_LT_VERIFIED_COUNTER { get; set; }
        public decimal? ACTUAL_LT_VERIFIED_COUNTER { get; set; } 
   
        public DateTime? PLAN_DT_VERIFIED_FINANCE { get; set; }
        public DateTime? ACTUAL_DT_VERIFIED_FINANCE { get; set; }
        public decimal? PLAN_LT_VERIFIED_FINANCE { get; set; }
        public decimal? ACTUAL_LT_VERIFIED_FINANCE { get; set; } 
        
        public DateTime? PLAN_DT_APPROVED_FINANCE_SH { get; set; }
        public DateTime? ACTUAL_DT_APPROVED_FINANCE_SH { get; set; }
        public decimal? PLAN_LT_APPROVED_FINANCE_SH { get; set; }
        public decimal? ACTUAL_LT_APPROVED_FINANCE_SH { get; set; } 

        public DateTime? PLAN_DT_APPROVED_FINANCE_DPH { get; set; }
        public DateTime? ACTUAL_DT_APPROVED_FINANCE_DPH { get; set; }
        public decimal? PLAN_LT_APPROVED_FINANCE_DPH { get; set; }
        public decimal? ACTUAL_LT_APPROVED_FINANCE_DPH { get; set; } 

        public DateTime? PLAN_DT_APPROVED_FINANCE_DH { get; set; }
        public DateTime? ACTUAL_DT_APPROVED_FINANCE_DH { get; set; }
        public decimal? PLAN_LT_APPROVED_FINANCE_DH { get; set; }
        public decimal? ACTUAL_LT_APPROVED_FINANCE_DH { get; set; } 

        public DateTime? PLAN_DT_APPROVED_FINANCE_DIRECTOR { get; set; }
        public DateTime? ACTUAL_DT_APPROVED_FINANCE_DIRECTOR { get; set; }
        public decimal? PLAN_LT_APPROVED_FINANCE_DIRECTOR { get; set; }
        public decimal? ACTUAL_LT_APPROVED_FINANCE_DIRECTOR { get; set; } 

        public DateTime? PLAN_DT_POSTED_TO_SAP { get; set; }
        public DateTime? ACTUAL_DT_POSTED_TO_SAP { get; set; }
        public decimal? PLAN_LT_POSTED_TO_SAP { get; set; }
        public decimal? ACTUAL_LT_POSTED_TO_SAP { get; set; } 

        public DateTime? PLAN_DT_PAID { get; set; }
        public DateTime? ACTUAL_DT_PAID { get; set; }
        public decimal? PLAN_LT_PAID { get; set; }
        public decimal? ACTUAL_LT_PAID { get; set; } 

        public DateTime? PLAN_DT_CHECKED { get; set; }
        public DateTime? ACTUAL_DT_CHECKED { get; set; }
        public decimal? PLAN_LT_CHECKED { get; set; }
        public decimal? ACTUAL_LT_CHECKED { get; set; } 

        public string INVALID_PRINT_COVER_STATUS { get; set; }
        public int? VERSION { get; set; }
        public string DESCRIPTION { get; set; }
        public string NEXT_APPROVER { get; set; }

        public bool DELETED { get; set; } 
        public string ERR { get; set; }
        
    }
}
