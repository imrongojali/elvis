﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class CashierPolicyList
    {

        private List<CashierPolicyData> data;
        public List<CashierPolicyData> Data
        {
            get
            {
                return data;
            }

            set
            {
                data = value;
            }
        }

        public CashierPolicyList()
        {
            data = new List<CashierPolicyData>();
        }

        public void Add(string currency, decimal amount, decimal rate)
        {
            int seq = 0;
            if (data.Count > 0) 
                seq = (from d in data select d.Seq).Max();

            data.Add(new CashierPolicyData()
            {
                Curr = currency,
                Seq = seq + 1,
                Rate = rate, 
                Policy = 0,
                Amount = amount,
                Total = amount*rate
            });
        }
    }
}
