﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class PVHeaderData
    {
        public int PV_NO;
        public int PV_YEAR;
        public int STATUS_CD;
        public string PAY_METHOD_CD;
        public int PV_TYPE_CD;
        public DateTime? ACTIVITY_DATE_FROM;
        public DateTime? ACTIVITY_DATE_TO;
        public DateTime? PLANNING_PAYMENT_DATE;
        public string VENDOR_CD;
        public int? VENDOR_GROUP_CD;
        public int? BANK_TYPE;
    }
}
