﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._30PVFormList
{
    [Serializable]
    public class DistributionStatusData
    {
        public decimal REFF_NO { get; set; }
        public int STATUS_CD { get; set; }
        public string PIC { get; set; }
        public DateTime? PLAN_DT { get; set; }
        public DateTime? ACTUAL_DT { get; set; }
        public decimal? ACTUAL_LT { get; set; }
        public string ACTUAL_UOM { get; set; }
        public int? SLA_UNIT { get; set; }
        public string SLA_UOM { get; set; }
        public string PROCCED_BY { get; set; }
        public byte CLASS { get; set; }
        public int? SKIP_FLAG { get; set; }

    }
}
