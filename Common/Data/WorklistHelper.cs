﻿using System;

namespace Common.Data
{
    [Serializable]
    public class WorklistHelper
    {
        public string Folio { get; set; }
        public string SN { get; set; }
        public string FullName { get; set; }
        public DateTime StartDate { get; set; }
        public string UserID { get; set; }
    }

}
