﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class AmountData
    {
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal? TOTAL_AMOUNT { get; set; }
        public decimal? EXCHANGE_RATE { get; set; }
        public DateTime CREATED_DATE { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime? CHANGED_DATE { get; set; }
        public string CHANGED_BY { get; set; } 

    }
}
