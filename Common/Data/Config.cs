﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class Config : BaseModel
    {
        public string ConfigKey { get; set; }
        public string ConfigValue { get; set; }

    }
}
