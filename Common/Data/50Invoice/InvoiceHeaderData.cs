﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._50Invoice
{
    [Serializable]
    public class InvoiceData
    {
        public string INVOICE_NO { get; set; }
        public DateTime INVOICE_DATE { get; set; }
        public string VENDOR_CD { get; set; }
        public string DESCRIPTION { get; set; }
        public string VENDOR_NAME { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public string CURRENCY_CD { get; set; }
        public decimal? IDR { get; set; }
        public decimal? USD { get; set; }
        public decimal? JPY { get; set; }
        public decimal? SGD { get; set; }
        public decimal? EURO { get; set; }
        //public string AMOUNT { get; set; }
        public int? OTHERS { get; set; }
        
        public ArrayList AMOUNT_OTHER_LIST { get; set; }
        //public string AMOUNT_QUANTITY { get; set; }
        public decimal AMOUNT { get; set; }
        public int? PV_NO { get; set; }
        public int? STATUS_CD { get; set; }
        public string INVOICE_STATUS { get; set; }
        public int? PV_YEAR { get; set; }
        public int ITEM_TRANSACTION_CD { get; set; }
        public string ITEM_DESCRIPTION { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string WBS_NO { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public int? CONVERT_FLAG { get; set; }
        public int SEQ_NO { get; set; }
        public int? TRANSACTION_CD { get; set; }
        public string TRANSACTION_NAME { get; set; }
        public int FT_CD { get; set; }
        public int DOC_NO { get; set; }
        public int DOC_YEAR { get; set; }
        public string OTHER_CURRENY { get; set; }
        public decimal? OTHER_AMOUNT { get; set; }
        public string TAX_NO { get; set; }
        public DateTime? INVOICE_TAX_DATE { get; set; }
        public string TAX_CD { get; set; }
        public int SEQNO { get; set; }
        public String TMMIN_INVOICE {get;set;}
        public String REMARK { get; set; }
        public int CONT_QTY_20 { get; set; }
        public int CONT_QTY_40 { get; set; }
        public DateTime? UPLOAD_DATE { get; set; }
        public DateTime? SUBMIT_DATE { get; set; }
        public DateTime? RECEIVE_DATE { get; set; }
        public DateTime? CONVERT_DATE { get; set; }
        public DateTime? PLANNING_PAYMENT_DATE { get; set; } 
        public string errMsg { get; set; } 
    }

}
