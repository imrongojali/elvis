﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class Result
    {
        public bool ResultCode { get; set; }
        public string ResultDesc { get; set; }
        public string ResultDescs { get; set; }
    }
}
