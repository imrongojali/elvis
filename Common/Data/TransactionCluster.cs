﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class TransactionCluster
    {
        public string TrCode { get; set; }
        public string TrName { get; set; }
        public string TrLevel { get; set; } 
        public string ClusterId { get; set; } 
    }
}
