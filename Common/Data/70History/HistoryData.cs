﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Web.SessionState;
using System.Web;
using System.ComponentModel;

namespace Common.Data._70History
{
    [Serializable]
    public class HistoryData
    {
        public decimal REFF_NO { get; set; }
        // public int DOC_YEAR { get; set; }
        // public string MODULE_DESC { get; set; }
        public string STATUS_NAME { get; set; }
        public string USERNAME { get; set; }
        public DateTime? ACTUAL_DT { get; set; }
        // public string CREATED_BY { get; set; }
        // public DateTime CREATED_DT { get; set; }
        // public string CHANGED_BY { get; set; }
        // public DateTime? CHANGED_DT { get; set; }
    }
}
