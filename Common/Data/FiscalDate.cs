﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    public class FiscalDate
    {
        public int DateFrom { set; get; }
        public int MonthFrom { set; get; }
        public int DateTo { set; get; }
        public int MonthTo { set; get; }
    }
}
