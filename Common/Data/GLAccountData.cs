﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class GLAccountData
    {
        public string GL_ACCOUNT { get; set; }
        public string GL_ACCOUNT_NAME { get; set; } 
    }
}
