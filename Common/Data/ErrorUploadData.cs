﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class ErrorUploadData
    {
        public int SEQ_NO { get; set; }
        public string ERRMSG { get; set; }
    }
}
