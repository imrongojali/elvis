﻿using System;

namespace Common.Data
{
    [Serializable]
    public class SystemMaster
    {
        public string Key { set; get; }
        public string Code { set; get; }
        
        public object Value { set; get; }
        
        public string ValueTxt { set; get; }
        public int? ValueNum { set; get; }
        public string ValueDt { set; get; }
        
        public string CreatedBy { set; get; }
        public DateTime CreatedDt { set; get; }
        public string ChangedBy { set; get; }
        public DateTime? ChangedDt { set; get; }

        public int ID { set; get; }
    }
}
