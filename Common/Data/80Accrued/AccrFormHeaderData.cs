﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccrFormHeaderData
    {
        public string ACCRUED_NO { get; set; }
        public string ISSUING_DIV { get; set; }
        public float USD_TO_IDR { get; set; }
        public float JPY_TO_IDR { get; set; }
        public string PIC_CUR { get; set; }
        public string PIC_NEXT { get; set; }
        public float TOTAL_AMOUNT { get; set; }
        public string SUBMISSION_STS { get; set; }
        public string WORKFLOW_STS { get; set; }

        public int SUBMISSION_STS_CD { get; set; }
        public int WORKFLOW_STS_CD { get; set; }
       
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
