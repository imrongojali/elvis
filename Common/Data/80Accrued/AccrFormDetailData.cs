﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccrFormDetailData
    {
        public int SEQ_NO { get; set; }

        public string BOOKING_NO { get; set; }
        public string WBS_NO { get; set; }
        public string WBS_DESC { get; set; }
        public string SUSPENSE_NO { get; set; }
        public string PV_TYPE { get; set; }
        public string ACTIVITY { get; set; }
        public string COST_CENTER_CD { get; set; }
        public string CURRENCY_CD { get; set; }
        public float AMOUNT { get; set; }
        public float AMOUNT_IDR { get; set; }
        public float AMOUNT_AVAILABLE { get; set; }
        public float AMOUNT_REMAINING { get; set; }
        
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
    }
}
