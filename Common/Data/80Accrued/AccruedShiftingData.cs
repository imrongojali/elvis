﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data._80Accrued
{
    [Serializable]
    public class AccruedShiftingData
    {
        readonly private string green = "#00ff00";
        readonly private string red = "#ff0000";
        readonly private string yellow = "#fffb00";

        private string workflow_status;
        public string WORKFLOW_STATUS
        {
            get
            {
                return workflow_status;
            }
            set
            {
                workflow_status = value;

                if (workflow_status == null)
                {
                    return;
                }

                if (workflow_status.Equals("0"))
                {
                    WORKFLOW_STATUS_COLOR = red;
                }
                else if (workflow_status.Equals("1"))
                {
                    WORKFLOW_STATUS_COLOR = green;
                }
            }
        }
        public string KEYS { get; set; }
        public string WORKFLOW_STATUS_COLOR { get; set; }
        public int? DisplaySequenceNumber { get; set; }
		//public int? SEQ_NO { get; set; }
        public string SHIFTING_NO { get; set; }
        public string EXTEND_NO { get; set; }
        public string BOOKING_NO { get; set; }
        public string BOOKING_NO_PR { get; set; }
        public string BOOKING_NO_DIRECT { get; set; }
        public string BOOKING_NO_OLD { get; set; }
        public string DIVISION_ID { get; set; }
        public string DIVISION_NAME { get; set; }
        public int STATUS_CD { get; set; }
        public string SUBMISSION_STATUS { get; set; }
        public decimal TOT_AMOUNT { get; set; }
        public decimal? REMAINING_PR { get; set; }
        public decimal? REMAINING_DIRECT { get; set; }
        public int SHIFTING_FROM { get; set; }
        public decimal SHIFTING_AMT { get; set; }
        public string PIC_CURRENT { get; set; }
        public string PIC_NEXT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public string MODULE_CD { get; set; }
        public int BUDGET_YEAR { get; set; }
        public string PV_TYPE_NAME { get; set; }
        public int? PV_TYPE_CD { get; set; }

        //add by FID.Arri on 8 May 2018 for AccrExtendReport
        public string WBS_NO_OLD { get; set; }
        public string WBS_NO_PR { get; set; }
        public string WBS_DESC_OLD { get; set; }
        public string WBS_DESC_PR { get; set; }
        public int? SUSPENSE_NO_OLD { get; set; }
        public int? SUSPENSE_NO_PR { get; set; }
        public decimal BLOCKING_AMT { get; set; }
        public decimal AVAILABLE_AMT { get; set; }
        public string EXTEND_TYPE { get; set; }
        public string SHIFTING_TYPE { get; set; }
        public int SHIFTING_TYPE_CD { get; set; }
        public decimal? RECLAIMABLE_AMT { get; set; }
        public decimal? EXTENDABLE_AMT { get; set; }
        public decimal? EXTEND_AMT { get; set; }
        public string ACTIVITY_DES { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public string REFF_NO { get; set; }
        public string SYSTEM_VALUE_TXT { get; set; }
        public string WbsNumber { get; set; }
        public bool Editable { get; set; }
        public bool Editing { get; set; }
        public bool Saved { get; set; }
        public string SAP_DOC_NO { get; set; }
        public bool CanUploadDetail { get; set; }
        private bool openEmpty;
        public bool OpenEmpty
        {
            get
            {
                return openEmpty;
            }
            set
            {
                openEmpty = value;
                openExisting = !openEmpty;
                Editable = openExisting;
            }
        }
        private bool openExisting;
        public bool OpenExisting
        {
            get
            {
                return openExisting;
            }
            set
            {
                openExisting = value;
                openEmpty = !openExisting;
                // Editing = !value;
            }
        }
    }
}
