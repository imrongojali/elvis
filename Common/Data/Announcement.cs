﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class Announcement
    {
        public int Code { set; get; }
        public string Title { set; get; }
        public string Description { set; get; }
        public string CreatedBy { set; get; }
        public DateTime? ReleaseDate { get; set; }
        public DateTime? ExpireDate { get; set; } 
    }
}
