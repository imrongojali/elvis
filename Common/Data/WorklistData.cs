﻿using System;

namespace Common.Data
{
    [Serializable]
    public class WorklistData
    {
        public string Folio { get; set; }
        public string SN { get; set; }
        public string DocStatus { get; set; }
        public string DocNumber { get; set; }
        public string DocYear { get; set; }
        public string WorklistTme { get; set; }
        public DateTime DocDate { get; set; }
        public int DocAgeSeconds { get; set; }
        public string CreatedDate { get; set; }
        public string DivName { get; set; }
        public string UserID { get; set; }
        public string Description { get; set; }
        public int Notices { get; set; }
        public DateTime? PaidDate { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public int? HoldFlag { get; set; }
        public string HypValue { get; set; }
        public decimal? TotalAmount { get; set; }
        public int NeedReply { get; set; }
        public int Rejected { get; set; }
        public int? STATUS_CD { get; set; }
        public string StatusName { get; set; }
        public string PvTypeName { get; set; }
        public string ActivityDateFrom { get; set; }
        public string ActivityDateTo { get; set; }
        public string PayMethodName { get; set; }
        public string BudgetNo { get; set; }
        public bool WorklistNotice { get; set; }
        // Add by FID.Arri on May 11, 2018 for new Dashboard
        //public string TransType { get; set; }
        public bool IsAccrued { get; set; }
        public string Type { get; set; }

        // FID.Ridwan 11072018
        public int TransCd { get; set; }
    }
}
