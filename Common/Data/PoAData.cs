﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable]
    public class PoAData
    {
        public string POA_NUMBER { get; set; }
        public string GRANTOR { get; set; }
        public string ATTORNEY { get; set; }
        public string REASON { get; set; }
        public bool OFFICE_STATUS { get; set; }
        public DateTime? VALID_FROM { get; set; }
        public DateTime? VALID_TO { get; set; } 
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public DateTime? CHANGED_DT { get; set; }
        public DateTime? GRANTED_DATE { get; set; }
        public DateTime? REVOKED_DATE { get; set; } 
    }
}
