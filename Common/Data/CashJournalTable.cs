﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.Data
{
    [Serializable] 
    public class CashJournalTable
    {
        public string BusinessTransaction { get; set; }
        public decimal? Amount { get; set; }
        public string Text { get; set; }
        public string Vendor { get; set; }
        public string Customer { get; set; }
        public DateTime? PostingDate { get; set; }
        public DateTime? DocDate { get; set; }
        public string Reference { get; set; }
        public string DocNo { get; set; }
        public string InternalDocNo { get; set; }
        public string Assignment { get; set; }
        public string DocumentStatus { get; set; }
        public string Reason { get; set; } 
    }
}
