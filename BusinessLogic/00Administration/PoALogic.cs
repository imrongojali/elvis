using BusinessLogic;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Function;
using Dapper;
using DataLayer.Model;
using K2.Helper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Text;
using System.Web;

namespace BusinessLogic.PoA
{
	public class PoALogic : LogicBase
	{
		public const string WS_MESG_SUCCESS = "SUCCESS";

		public const string WS_MESG_ERROR = "ERROR";

		protected StringBuilder _err = new StringBuilder("");

		private ErrorData ed = new ErrorData();

		private LogicFactory logic = LogicFactory.Get();

		public string emailTemplateDir = "";

		public bool AttorneyCanGrant
		{
			get
			{
				bool r = true;
				string canGrant = this.logic.Sys.GetText("PoA", "ATTORNEY_CAN_GRANT", "");
				if (!canGrant.isEmpty())
				{
					r = canGrant.Int(1) == 1;
				}
				return r;
			}
		}

		public bool UseShareWorkList
		{
			get
			{
				string share = this.logic.Sys.GetText("PoA", "SHARE", "");
				return share.Int(0) > 0;
			}
		}

		public PoALogic()
		{
		}

		public bool AddPOA(ref string POANumber, string Grantor, string Attorney, string Reason, string OfficeStatus, DateTime ValidFrom, DateTime ValidTo, string UserName, ref ErrorData Err)
		{
			bool Kode = false;
			if (this.FuturePoACount(Grantor) > 0)
			{
				ErrorData errorDatum = new ErrorData()
				{
					ErrID = 2,
					ErrMsgID = "MSTD00141ERR",
					Data = new string[0],
					ErrMsg = this.logic.Msg.Message("MSTD00141ERR", new object[0])
				};
				Err = errorDatum;
				return Kode;
			}
			if (this.CheckOOFStatus(Grantor))
			{
				ErrorData errorDatum1 = new ErrorData()
				{
					ErrID = 2,
					ErrMsgID = "MSTD00025ERR",
					Data = new string[0],
					ErrMsg = this.logic.Msg.Message("MSTD00025ERR", new object[0])
				};
				Err = errorDatum1;
				return Kode;
			}
			TB_R_POA GrantorAsAttorney = (
				from a in base.db.TB_R_POA
				where (a.ATTORNEY == Grantor) && !a.OFFICE_STATUS
				select a).FirstOrDefault<TB_R_POA>();
			if (GrantorAsAttorney != null && !this.AttorneyCanGrant)
			{
				ErrorData errorDatum2 = new ErrorData()
				{
					ErrID = 2,
					ErrMsgID = "MSTD00002ERR"
				};
				string[] strArrays = new string[1];
				string[] fullName = new string[] { this.GetFullName(Grantor), " cannot Grant PoA when acting as Attorney for ", this.GetFullName(GrantorAsAttorney.GRANTOR), " referenced by PoA number :", GrantorAsAttorney.POA_NUMBER };
				strArrays[0] = string.Concat(fullName);
				errorDatum2.Data = strArrays;
				errorDatum2.ErrMsg = this.logic.Msg.Message("MSTD00002ERR", new object[0]);
				Err = errorDatum2;
				return Kode;
			}
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					TB_R_POA _header = new TB_R_POA();
					if (POANumber.isEmpty())
					{
						POANumber = this.GeneratePoACode();
					}
					_header.CREATED_BY = UserName;
					_header.CREATED_DT = DateTime.Now;
					_header.POA_NUMBER = POANumber;
					_header.GRANTOR = Grantor;
					_header.ATTORNEY = Attorney;
					_header.REASON = Reason;
					DateTime? nullable = null;
					_header.OFFICE_STATUS = !PoALogic.isActive(new DateTime?(ValidFrom), new DateTime?(ValidTo), new DateTime?(DateTime.Now), nullable);
					_header.VALID_FROM = new DateTime?(ValidFrom);
					_header.VALID_TO = new DateTime?(ValidTo);
					co.db.AddToTB_R_POA(_header);
					co.db.SaveChanges();
					Kode = true;
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref Err);
				}
			}
			return Kode;
		}

		public int AttorneyGrantorOOFCount(string attorney)
		{
			int r = 0;
			r = (
				from poa in base.db.TB_R_POA
				where !poa.OFFICE_STATUS && poa.ATTORNEY.Equals(attorney)
				select poa).Count<TB_R_POA>();
			return r;
		}

		public bool canChangeGrantor(string user)
		{
			bool flag;
			using (ContextWrap co = new ContextWrap())
			{
				ELVIS_DBEntities db = co.db;
				string adminRole = this.logic.Sys.GetText("ROLE", "GRANTORS", "");
				adminRole = (!string.IsNullOrEmpty(adminRole) ? adminRole.Trim() : "ELVIS_ADMIN,ELVIS_SECRETARY");
				string userRole = (
					from urh in db.vw_User_Role_Header
					where urh.USERNAME == user
					select urh.ROLE_ID).FirstOrDefault<string>();
				userRole = (!string.IsNullOrEmpty(userRole) ? userRole.Trim() : "");
				flag = (adminRole.Length <= 0 || userRole.Length <= 0 ? false : adminRole.IndexOf(userRole, StringComparison.OrdinalIgnoreCase) >= 0);
			}
			return flag;
		}

		public bool canEdit(string poaNumber)
		{
			int canE = (
				from p in base.db.TB_R_POA
				where (p.POA_NUMBER == poaNumber) && (p.REVOKED_DATE == (DateTime?)null) && (p.OFFICE_STATUS && (p.GRANTED_DATE == (DateTime?)null) || !p.OFFICE_STATUS && (p.GRANTED_DATE != (DateTime?)null))
				select p).Count<TB_R_POA>();
			return canE > 0;
		}

		public bool CheckOOFStatus(string username)
		{
			int PoAOutOfOfficeCount = (
				from poa in base.db.TB_R_POA
				where !poa.OFFICE_STATUS && poa.GRANTOR.Equals(username)
				select poa).Count<TB_R_POA>();
			return PoAOutOfOfficeCount > 0;
		}

		public bool DeactivatePoa(string POANumber, string userName)
		{
			bool Ok = false;
			DbTransaction TX = null;
			using (ContextWrap kon = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = kon.db;
					TX = db.Connection.BeginTransaction();
					IQueryable<TB_R_POA> q = 
						from p in db.TB_R_POA
						where p.POA_NUMBER.Equals(POANumber)
						select p;
					if (q.Any<TB_R_POA>())
					{
						TB_R_POA x = q.FirstOrDefault<TB_R_POA>();
						if (!PoALogic.isActive(x.VALID_FROM, x.VALID_TO, x.GRANTED_DATE, x.REVOKED_DATE))
						{
							if (x.GRANTED_DATE.HasValue || x.REVOKED_DATE.HasValue)
							{
								x.OFFICE_STATUS = true;
								x.REVOKED_DATE = new DateTime?(DateTime.Now);
							}
							else
							{
								db.DeleteObject(x);
							}
							db.SaveChanges();
						}
						else
						{
							this.logic.k2.Open();
							this.RevokeWorklist(db, POANumber, userName);
							this.logic.k2.Close();
							db.SaveChanges();
						}
					}
					TX.Commit();
					Ok = true;
				}
				catch (Exception exception)
				{
					LoggingLogic.err(exception);
				}
			}
			return Ok;
		}

		public bool DeletePOA(string POANumber, ref ErrorData Err)
		{
			bool Kode = false;
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = co.db;
					IQueryable<TB_R_POA> q = 
						from p in db.TB_R_POA
						where p.POA_NUMBER.Contains(POANumber)
						select p;
					foreach (TB_R_POA p in q)
					{
						db.DeleteObject(p);
					}
					db.SaveChanges();
					Kode = true;
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref Err);
				}
			}
			return Kode;
		}

		public bool EditPOA(PoAData poaEdit, ref ErrorData Err, UserData u)
		{
			bool Kode = false;
			using (ContextWrap co = new ContextWrap())
			{
				DbTransaction tx = null;
				try
				{
					ELVIS_DBEntities db = co.db;
					TB_R_POA q = (
						from p in db.TB_R_POA
						where p.POA_NUMBER == poaEdit.POA_NUMBER
						select p).FirstOrDefault<TB_R_POA>();
					tx = db.Connection.BeginTransaction();
					TB_R_POA poa = q;
					if (poa == null)
					{
						Err = new ErrorData()
						{
							ErrID = 2,
							ErrMsgID = ""
						};
					}
					else
					{
						poa.CHANGED_BY = u.USERNAME;
						poa.CHANGED_DT = new DateTime?(DateTime.Now);
						DateTime? nullable = null;
						poa.OFFICE_STATUS = !PoALogic.isActive(poaEdit.VALID_FROM, poaEdit.VALID_TO, new DateTime?(DateTime.Now), nullable);
						poa.GRANTOR = poaEdit.GRANTOR;
						poa.ATTORNEY = poaEdit.ATTORNEY;
						poa.VALID_FROM = poaEdit.VALID_FROM;
						poa.VALID_TO = poaEdit.VALID_TO;
						db.SaveChanges();
						tx.Commit();
					}
					Kode = true;
				}
				catch (Exception exception)
				{
					Exception ex = exception;
					tx.Rollback();
					this.Handle(ex, ref Err);
				}
			}
			return Kode;
		}

		public bool EditStatusPOA(string POANumber, bool status, ref ErrorData Err, UserData u)
		{
			bool Kode = false;
			using (ContextWrap co = new ContextWrap())
			{
				DbTransaction tx = co.db.Connection.BeginTransaction();
				try
				{
					IQueryable<TB_R_POA> q = 
						from p in co.db.TB_R_POA
						where p.POA_NUMBER == POANumber
						select p;
					if (!q.Any<TB_R_POA>())
					{
						Err = new ErrorData()
						{
							ErrID = 2,
							ErrMsgID = ""
						};
					}
					else
					{
						TB_R_POA poa = q.First<TB_R_POA>();
						poa.CHANGED_BY = u.USERNAME;
						poa.CHANGED_DT = new DateTime?(DateTime.Now);
						poa.OFFICE_STATUS = status;
						base.db.SaveChanges();
					}
					Kode = true;
					tx.Commit();
				}
				catch (Exception exception)
				{
					Exception ex = exception;
					tx.Rollback();
					this.Handle(ex, ref Err);
				}
			}
			return Kode;
		}

		public int FuturePoACount(string grantor)
		{
			int r = 0;
			DateTime d = DateTime.Now;
			DateTime dateTime = new DateTime(d.Year, d.Month, d.Day);
			r = (
				from p in base.db.TB_R_POA
				where p.GRANTOR != null && p.GRANTOR.Equals(grantor) && (p.VALID_FROM != (DateTime?)null) && (p.VALID_FROM > (DateTime?)dateTime) && (p.VALID_TO != (DateTime?)null) && (p.VALID_TO > (DateTime?)dateTime) && (p.REVOKED_DATE == (DateTime?)null)
				select p).Count<TB_R_POA>();
			return r;
		}

		public string GeneratePoACode()
		{
			string PoACode = string.Empty;
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = co.db;
					string CurrentYear = DateTime.Now.Year.ToString();
					TB_R_POA q = (
						from i in db.TB_R_POA
						orderby i.POA_NUMBER descending
						select i).FirstOrDefault<TB_R_POA>();
					if (q == null)
					{
						PoACode = string.Concat(CurrentYear, "0001");
					}
					else
					{
						PoACode = q.POA_NUMBER;
						int length = PoACode.Length;
						string PoAYear = PoACode.Substring(0, 4);
						int PoANumber = int.Parse(PoACode.Substring(4, 4));
						if (PoAYear != CurrentYear)
						{
							PoANumber = 1;
						}
						else
						{
							PoANumber++;
						}
						int Max = 4 - PoANumber.ToString().Length;
						PoACode = CurrentYear;
						while (Max > 0)
						{
							PoACode = string.Concat(PoACode, "0");
							Max--;
						}
						PoACode = string.Concat(PoACode, PoANumber);
					}
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref this.ed);
				}
			}
			return PoACode;
		}

		public List<string> getAttorneyUsernamePoa(string user)
		{
			List<string> _retVal = new List<string>();
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ObjectResult<vw_User_Data_Detail> q = co.db.GetAtorneyList(user);
					if (q != null)
					{
						List<vw_User_Data_Detail> UserDataList = q.ToList<vw_User_Data_Detail>();
						_retVal = new List<string>();
						foreach (vw_User_Data_Detail vwUserDataDetail in UserDataList)
						{
							if ((
								from i in _retVal
								where i == vwUserDataDetail.USERNAME
								select i).ToList<string>().Count != 0)
							{
								continue;
							}
							_retVal.Add(vwUserDataDetail.USERNAME);
						}
					}
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref this.ed);
				}
			}
			return _retVal;
		}

		public string GetFullName(string uid)
		{
			return (
				from a in base.db.vw_User
				where a.USERNAME == uid
				select (a.FIRST_NAME + " ") + a.LAST_NAME).FirstOrDefault<string>();
		}

		public List<PoAData> GetGrantorByAttorney(string attorney, bool isAvailable)
		{
			List<PoAData> list;
			try
			{
				int? nullable = null;
				CommandType? nullable1 = null;
				list = base.Db.Query<PoAData>("GetGrantorByAttorney", new { ATTORNEY = attorney, OFFICE_STATUS = isAvailable }, null, true, nullable, nullable1).ToList<PoAData>();
			}
			catch (Exception exception)
			{
				this.Handle(exception, ref this.ed);
				return null;
			}
			return list;
		}

		public List<string> getGrantorUsernamePoa(string user)
		{
			List<string> _retVal = new List<string>();
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ObjectResult<vw_User_Data_Detail> q = co.db.GetGrantorList(user);
					if (q != null)
					{
						List<vw_User_Data_Detail> UserDataList = q.ToList<vw_User_Data_Detail>();
						_retVal = new List<string>();
						foreach (vw_User_Data_Detail vwUserDataDetail in UserDataList)
						{
							if ((
								from i in _retVal
								where i == vwUserDataDetail.USERNAME
								select i).ToList<string>().Count != 0)
							{
								continue;
							}
							_retVal.Add(vwUserDataDetail.USERNAME);
						}
					}
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref this.ed);
				}
			}
			return _retVal;
		}

		public List<string> getUsernamePoaByDiv(string user)
		{
			List<string> _retVal = new List<string>();
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = co.db;
					vw_User_Data divID = (
						from p in db.vw_User_Data
						where p.USERNAME == user
						select p).FirstOrDefault<vw_User_Data>();
					string dIVISIONID = divID.DIVISION_ID;
					List<string> q = (
						from i in db.vw_User_Data
						where (i.DIVISION_ID == dIVISIONID) && (i.USERNAME != user)
						select i.USERNAME).ToList<string>();
					if (q != null && q.Count > 0)
					{
						foreach (string data in q)
						{
							_retVal.Add(data);
						}
					}
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref this.ed);
				}
			}
			return _retVal;
		}

		private void GrantWorklist(ELVIS_DBEntities db, string POANumber, string userName)
		{
			List<string> docs = null;
			string loc = string.Format("GrantWorkList('{0}', '{1}')", POANumber, userName);
			try
			{
				PoAData poa = (
					from a in db.TB_R_POA
					where a.POA_NUMBER == POANumber
					select a into b
					select new PoAData()
					{
						POA_NUMBER = b.POA_NUMBER,
						GRANTOR = b.GRANTOR,
						ATTORNEY = b.ATTORNEY,
						REASON = b.REASON,
						OFFICE_STATUS = b.OFFICE_STATUS,
						VALID_FROM = b.VALID_FROM,
						VALID_TO = b.VALID_TO,
						GRANTED_DATE = b.GRANTED_DATE,
						REVOKED_DATE = b.REVOKED_DATE,
						CREATED_BY = b.CREATED_BY,
						CREATED_DT = b.CREATED_DT,
						CHANGED_BY = b.CHANGED_BY,
						CHANGED_DT = b.CHANGED_DT
					}).FirstOrDefault<PoAData>();
				if (poa != null)
				{
					this.logic.Say(loc, string.Concat("poa GRANT from: ", poa.GRANTOR, " to: ", poa.ATTORNEY), new object[0]);
					List<WorklistHelper> w = this.logic.k2.GetWorklistCompleteByProcess(poa.GRANTOR, "");
					docs = (
						from x in w
						select x.Folio).ToList<string>();
					this.logic.Say(loc, "Redirect documents:", new object[0]);
					foreach (string str in docs)
					{
						LogicFactory logicFactory = this.logic;
						object[] objArray = new object[] { str };
						logicFactory.Say(loc, "\t{0}", objArray);
					}
					List<string> redirected = this.logic.k2.Redirect(docs, poa.GRANTOR, poa.ATTORNEY);
					foreach (string doc in redirected)
					{
						int num = 0;
						int num1 = 0;
						if (!this.SplitReffNo(doc, ref num, ref num1))
						{
							continue;
						}
						IQueryable<TB_R_POA_RECORD> px = 
							from c in db.TB_R_POA_RECORD
							where (c.POA_NUMBER == POANumber) && c.DOC_NO == num && c.DOC_YEAR == num1
							select c;
						if (!px.Any<TB_R_POA_RECORD>())
						{
							TB_R_POA_RECORD tBRPOARECORD = new TB_R_POA_RECORD()
							{
								POA_NUMBER = POANumber,
								DOC_NO = num,
								DOC_YEAR = num1,
								CREATED_BY = poa.GRANTOR,
								CREATED_DT = DateTime.Now
							};
							db.AddToTB_R_POA_RECORD(tBRPOARECORD);
						}
						else
						{
							TB_R_POA_RECORD poax = px.FirstOrDefault<TB_R_POA_RECORD>();
							if (poax != null)
							{
								poax.CHANGED_BY = poa.GRANTOR;
								poax.CHANGED_DT = new DateTime?(DateTime.Now);
							}
						}
						db.SaveChanges();
						db.SwapApproval(doc, poa.GRANTOR, poa.ATTORNEY, new bool?(false));
					}
					if (this.emailTemplateDir.isEmpty() && HttpContext.Current != null && HttpContext.Current.Server != null)
					{
						this.emailTemplateDir = HttpContext.Current.Server.MapPath(this.logic.Sys.GetText("EMAIL_TEMPLATE", "DIR", ""));
					}
					EmailFunction e = new EmailFunction(this.emailTemplateDir);
					e.ComposePoa(poa.GRANTOR, poa.ATTORNEY, POANumber, poa.VALID_FROM, poa.VALID_TO, userName);
					IQueryable<TB_R_POA> q = 
						from p in db.TB_R_POA
						where p.POA_NUMBER == POANumber
						select p;
					if (q.Any<TB_R_POA>())
					{
						TB_R_POA tbRPoA = q.First<TB_R_POA>();
						tbRPoA.CHANGED_BY = userName;
						tbRPoA.CHANGED_DT = new DateTime?(DateTime.Now);
						tbRPoA.GRANTED_DATE = new DateTime?(DateTime.Now);
						tbRPoA.OFFICE_STATUS = false;
						db.SaveChanges();
					}
				}
			}
			catch (Exception exception)
			{
				LoggingLogic.err(exception);
				throw;
			}
		}

		protected void Handle(Exception ex, ref ErrorData Err)
		{
			if (ex != null)
			{
				this._err.AppendLine(ex.Message);
				if (ex.InnerException != null)
				{
					this._err.AppendLine(ex.InnerException.Message);
				}
				this._err.AppendLine(ex.StackTrace);
				if (Err != null)
				{
					Err = new ErrorData()
					{
						ErrID = 2,
						ErrMsgID = "",
						ErrMsg = ex.Message
					};
				}
			}
		}

		public static bool isActive(DateTime? validFrom, DateTime? validTo, DateTime? Granted, DateTime? Revoked)
		{
			DateTime today = DateTime.Now;
			DateTime? nullable = validFrom;
			DateTime vfrom = (nullable.HasValue ? nullable.GetValueOrDefault() : today);
			DateTime? nullable1 = validTo;
			DateTime vto = (nullable1.HasValue ? nullable1.GetValueOrDefault() : today);
			DateTime? granted = Granted;
			if (granted.HasValue)
			{
				granted.GetValueOrDefault();
			}
			DateTime? revoked = Revoked;
			if (revoked.HasValue)
			{
				revoked.GetValueOrDefault();
			}
			return (!(today.Date >= vfrom.Date) || !(today.Date <= vto.Date) ? false : !Revoked.HasValue);
		}

		public bool isFuturePoA(string poaNumber)
		{
			DateTime d = DateTime.Now;
			DateTime t0 = new DateTime(d.Year, d.Month, d.Day);
			TB_R_POA poa = (
				from p in base.db.TB_R_POA
				where (p.POA_NUMBER == poaNumber) && (p.REVOKED_DATE == (DateTime?)null)
				select p).FirstOrDefault<TB_R_POA>();
			if (poa == null)
			{
				return false;
			}
			if (poa.VALID_FROM.HasValue)
			{
				DateTime? vALIDFROM = poa.VALID_FROM;
				DateTime dateTime = t0;
				if ((vALIDFROM.HasValue ? vALIDFROM.GetValueOrDefault() > dateTime : false) && poa.VALID_TO.HasValue)
				{
					DateTime? vALIDTO = poa.VALID_TO;
					DateTime dateTime1 = t0;
					if (!vALIDTO.HasValue)
					{
						return false;
					}
					return vALIDTO.GetValueOrDefault() > dateTime1;
				}
			}
			return false;
		}

		public bool isInOfficeStatus(string POANumber)
		{
			bool? officeStatus = new bool?((
				from a in base.db.TB_R_POA
				where a.POA_NUMBER == POANumber
				select a.OFFICE_STATUS).FirstOrDefault<bool>());
			bool? nullable = officeStatus;
			if (!nullable.HasValue)
			{
				return false;
			}
			return nullable.GetValueOrDefault();
		}

		public void ProcessAllUnprocessPoA(string userName)
		{
			DateTime date = DateTime.Now.Date;
			List<string> listPoANumber = null;
			DbTransaction tx = null;
			string loc = string.Concat("ProcessAllUnprocessPoA(", userName, ")");
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					try
					{
						this.logic.k2.Open();
						ELVIS_DBEntities db = co.db;
						listPoANumber = (
							from a in db.TB_R_POA
							where (a.GRANTED_DATE == (DateTime?)null) && (a.VALID_FROM != (DateTime?)null) && (a.VALID_TO != (DateTime?)null)
							select a into b
							where ((DateTime?)date >= b.VALID_FROM) && ((DateTime?)date <= b.VALID_TO)
							select b into c
							select c.POA_NUMBER).ToList<string>();
						if (listPoANumber != null)
						{
							LogicFactory logicFactory = this.logic;
							object[] objArray = new object[] { listPoANumber.Count<string>() };
							logicFactory.Say(loc, "Granting {0} PoA", objArray);
							foreach (string POANumber in listPoANumber)
							{
								try
								{
									this.logic.Say(loc, string.Concat("Grant Worklist PoA ", POANumber), new object[0]);
									tx = db.Connection.BeginTransaction();
									this.GrantWorklist(db, POANumber, userName);
									tx.Commit();
									this.logic.Say(loc, string.Concat("Grant Worklist PoA ", POANumber, " done"), new object[0]);
								}
								catch (Exception exception)
								{
									Exception ex = exception;
									tx.Rollback();
									this.logic.Say(loc, string.Concat("Error Grant Worklist PoA ", POANumber, ", Error : ", ex.Message), new object[0]);
									LoggingLogic.err(ex);
								}
							}
						}
						listPoANumber = (
							from a in db.TB_R_POA
							where (a.REVOKED_DATE == (DateTime?)null) && (a.VALID_FROM != (DateTime?)null) && (a.VALID_TO != (DateTime?)null) && !(((DateTime?)date >= a.VALID_FROM) && ((DateTime?)date <= a.VALID_TO)) && ((DateTime?)date >= a.VALID_FROM)
							select a.POA_NUMBER).ToList<string>();
						if (listPoANumber != null)
						{
							this.logic.Say(loc, string.Concat("Revoking ", listPoANumber.Count<string>(), " PoA"), new object[0]);
							foreach (string POANumber in listPoANumber)
							{
								try
								{
									this.logic.Say(loc, string.Concat("Revoke Worklist PoA ", POANumber), new object[0]);
									tx = db.Connection.BeginTransaction();
									this.RevokeWorklist(db, POANumber, userName);
									tx.Commit();
									this.logic.Say(loc, string.Concat("Revoke Worklist PoA ", POANumber, " done"), new object[0]);
								}
								catch (Exception exception1)
								{
									Exception ex = exception1;
									tx.Rollback();
									this.logic.Say(loc, string.Concat("Error Revoke Worklist PoA ", POANumber, ", Error : ", ex.Message), new object[0]);
									LoggingLogic.err(ex);
								}
							}
						}
					}
					catch (Exception exception2)
					{
						LoggingLogic.err(exception2);
						throw;
					}
				}
				finally
				{
					if (this.logic.k2 != null)
					{
						this.logic.k2.Close();
					}
				}
			}
		}

		public void RedirectWorklist(string POANumber, string userName, bool? inOffice = false)
		{
			try
			{
				try
				{
					this.logic.k2.Open();
					bool? nullable = inOffice;
					if ((nullable.HasValue ? nullable.GetValueOrDefault() : false))
					{
						this.RevokeWorklist(base.db, POANumber, userName);
					}
					else
					{
						this.GrantWorklist(base.db, POANumber, userName);
					}
				}
				catch (Exception exception)
				{
					LoggingLogic.err(exception);
					throw;
				}
			}
			finally
			{
				this.logic.k2.Close();
			}
		}

		private void RevokeWorklist(ELVIS_DBEntities db, string POANumber, string userName)
		{
			List<string> docs = null;
			string loc = string.Format("RevokeWorklist('{0}', '{1}')", POANumber, userName);
			try
			{
				PoAData poa = (
					from a in db.TB_R_POA
					where a.POA_NUMBER == POANumber
					select a into b
					select new PoAData()
					{
						POA_NUMBER = b.POA_NUMBER,
						GRANTOR = b.GRANTOR,
						ATTORNEY = b.ATTORNEY,
						REASON = b.REASON,
						OFFICE_STATUS = b.OFFICE_STATUS,
						CREATED_BY = b.CREATED_BY,
						CREATED_DT = b.CREATED_DT,
						CHANGED_BY = b.CHANGED_BY,
						CHANGED_DT = b.CHANGED_DT
					}).FirstOrDefault<PoAData>();
				if (poa != null)
				{
					List<WorklistHelper> w = this.logic.k2.GetWorklistCompleteByProcess(poa.ATTORNEY, "");
					docs = (
						from x in w
						select x.Folio).ToList<string>();
					IQueryable<PoARecordData> poar = 
						from pr in db.TB_R_POA_RECORD
						where pr.POA_NUMBER == POANumber
						select pr into g
						select new PoARecordData()
						{
							POA_NUMBER = g.POA_NUMBER,
							DOC_NO = g.DOC_NO,
							DOC_YEAR = g.DOC_YEAR,
							CREATED_BY = g.CREATED_BY,
							CREATED_DT = g.CREATED_DT,
							CHANGED_BY = g.CHANGED_BY,
							CHANGED_DT = g.CHANGED_DT
						};
					if (poar.Any<PoARecordData>())
					{
						List<string> poardocs = (
							from r in poar.ToList<PoARecordData>()
							select string.Concat(r.DOC_NO.ToString(), r.DOC_YEAR.ToString())).ToList<string>();
						List<string> target = poardocs.AsEnumerable<string>().Intersect<string>(docs.AsEnumerable<string>()).ToList<string>();
						List<string> results = this.logic.k2.Redirect(target, poa.ATTORNEY, poa.GRANTOR);
						this.logic.Say(loc, string.Concat("poa REVOKE back to: ", poa.GRANTOR, " from: ", poa.ATTORNEY), new object[0]);
						this.logic.Say(loc, "Revoke Documents : ", new object[0]);
						foreach (string doc in results)
						{
							LogicFactory logicFactory = this.logic;
							object[] objArray = new object[] { doc };
							logicFactory.Say(loc, "\t{0}", objArray);
							db.SwapApproval(doc, poa.GRANTOR, poa.ATTORNEY, new bool?(true));
						}
					}
					IQueryable<TB_R_POA> q = 
						from p in db.TB_R_POA
						where p.POA_NUMBER == POANumber
						select p;
					if (q.Any<TB_R_POA>())
					{
						TB_R_POA tbRPoA = q.First<TB_R_POA>();
						tbRPoA.CHANGED_BY = userName;
						tbRPoA.CHANGED_DT = new DateTime?(DateTime.Now);
						tbRPoA.REVOKED_DATE = new DateTime?(DateTime.Now);
						tbRPoA.OFFICE_STATUS = true;
						db.SaveChanges();
					}
				}
			}
			catch (Exception exception)
			{
				LoggingLogic.err(exception);
				throw;
			}
		}

		public static bool roleAllowedEdit(UserRoleHeader uroh)
		{
			if (uroh.ROLE_ID.Contains("_SECRETARY"))
			{
				return true;
			}
			return uroh.ROLE_ID.Contains("ELVIS_ADMIN");
		}

		public List<TB_R_POA> searchPoA(string PoANumber, string Grantor, string Attorney, string OfficeStatus, DateTime? ValidFrom, DateTime? ValidTo, string user, ref ErrorData Err)
		{
			bool? nullable;
			List<TB_R_POA> retVal = new List<TB_R_POA>();
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = co.db;
					if (string.IsNullOrEmpty(OfficeStatus))
					{
						nullable = null;
					}
					else
					{
						nullable = new bool?(OfficeStatus.Equals("1"));
					}
					bool? inOffice = nullable;
					ObjectResult<TB_R_POA> q = db.SearchPoa(user, PoANumber, Grantor, Attorney, inOffice, ValidFrom, ValidTo);
					foreach (TB_R_POA h in q)
					{
						retVal.Add(h);
					}
				}
				catch (Exception exception)
				{
					Exception ex = exception;
					LoggingLogic.err(ex);
					this.Handle(ex, ref Err);
				}
			}
			return retVal;
		}

		public void ShareWorklist(string fromUser, string toUser = "", int inOffice = 1)
		{
			this.logic.k2.Open();
			this.logic.k2.Impersonate(fromUser);
			this.logic.k2.ShareWorklist(DateTime.Now, toUser, inOffice);
		}

		private bool SplitReffNo(string r, ref int no, ref int yy)
		{
			no = 0;
			yy = 0;
			if (!r.isEmpty() && r.Length > 4)
			{
				int j = r.Length - 4;
				no = r.Substring(0, j).Int(0);
				yy = r.Substring(j, 4).Int(0);
			}
			if (no <= 0)
			{
				return false;
			}
			return yy > 0;
		}

		public List<CodeConstant> UserNameFull(List<string> g)
		{
			List<CodeConstant> l = new List<CodeConstant>();
			using (ContextWrap co = new ContextWrap())
			{
				try
				{
					ELVIS_DBEntities db = co.db;
					for (int i = 0; i < g.Count; i++)
					{
						string item = g[i];
						vw_User n = (
							from a in db.vw_User
							where a.USERNAME == item
							select a).FirstOrDefault<vw_User>();
						if (n != null)
						{
							CodeConstant codeConstant = new CodeConstant()
							{
								Code = n.USERNAME,
								Description = string.Concat(n.FIRST_NAME, " ", n.LAST_NAME)
							};
							l.Add(codeConstant);
						}
						else if (item.isEmpty())
						{
							CodeConstant codeConstant1 = new CodeConstant()
							{
								Code = "",
								Description = ""
							};
							l.Add(codeConstant1);
						}
					}
				}
				catch (Exception exception)
				{
					this.Handle(exception, ref this.ed);
				}
			}
			return l;
		}
	}
}