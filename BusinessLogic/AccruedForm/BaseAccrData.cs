﻿using BusinessLogic.VoucherForm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.AccruedForm
{
    public class BaseAccrData
    {
        public string AccruedNo { get; set; }
        public string ReffNo { get; set; }
        public string DivisionID { get; set; }
        public int? StatusCode { get; set; }
        public DateTime CreatedDt { get; set; }
        public List<FormNote> Notes { get; set; }
        public AttachmentTable AttachmentTable { get; set; }
    }
}
