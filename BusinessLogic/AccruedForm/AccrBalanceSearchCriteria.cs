﻿/*
 * Created by fid.Taufik H
 * 02/05/2018
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace BusinessLogic.AccruedForm
{
    public class AccrBalanceSearchCriteria
    {
        public string issuingDivision, bookingNo, wbsNoOld, wbsNoNew, sBookingNoPre, sBookingNoPost;
        public int pvType, openStatus, extend, shifted, suspenseNoOld, suspenseNoNew, iBookingNo, txtBDYear;
        public DateTime expiryDtFrom, expiryDtTo;
        public bool ignoreBookingNo, ignoreIssuingDivision
            , ignoreDateFrom, ignoreDateTo, ignoreYear, ignorewbsNoOld, ignorewbsNoNew
            , ignorePvType, ignoreOpenStatus, ignoreExtend, ignoreShifted, ignoreSuspenseNoOld, ignoreSuspenseNoNew;
        public string[] divs;

        private void init()
        {
            issuingDivision = "";
            bookingNo = "";
            wbsNoOld = "";
            wbsNoNew = "";
            txtBDYear = -1;
            pvType = -1;
            openStatus = -1;
            extend = -1;
            shifted = -1;
            suspenseNoOld = -1;
            suspenseNoNew = -1;

            expiryDtFrom = new DateTime();
            expiryDtTo = new DateTime();

            ignoreYear = true;
            ignoreBookingNo = true;
            ignorewbsNoOld = true;
            ignoreIssuingDivision = true;
            ignoreDateFrom = true;
            ignoreDateTo = true;
            ignorewbsNoNew = true;
            ignorePvType = true;
            ignoreOpenStatus = true;
            ignoreExtend = true;
            ignoreShifted = true;
            ignoreSuspenseNoOld = true;
            ignoreSuspenseNoNew = true;
        }

        public AccrBalanceSearchCriteria()
        {
            init();
        }

        public AccrBalanceSearchCriteria(
            string _issuingDivision, string _openStatus, string _wbsNoOld,
            string _bookingNo, string _extend, string _wbsNoNew, 
            string _pvType, string _shifted, string _suspenseNoOld,
            string _createdDtFrom, string _createdDtTo, string _txtBDYear, string _suspenseNoNew)
        {
            //default ignore all searching criteria
            init();

            #region build searching criteria

            if (!_issuingDivision.isEmpty()) //   !string.IsNullOrEmpty(_issuingDivision) && Convert.ToInt16(_issuingDivision) != 0)
            {
                ignoreIssuingDivision = false;

                issuingDivision = _issuingDivision;

                if (!issuingDivision.isEmpty() && issuingDivision.Contains(";"))
                {
                    divs = issuingDivision.Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
                }
            }

            if (!string.IsNullOrEmpty(_createdDtFrom))
            {
                ignoreDateFrom = false;
                expiryDtFrom = DateTime.ParseExact(_createdDtFrom, "dd/MM/yyyy", null);
            }
            if (!string.IsNullOrEmpty(_createdDtTo))
            {
                ignoreDateTo = false;
                expiryDtTo = DateTime.ParseExact(_createdDtTo, "dd/MM/yyyy", null);
            }

            if(!string.IsNullOrEmpty(_wbsNoOld)){
                wbsNoOld = _wbsNoOld;
                ignorewbsNoOld = false;
            }

            if (!string.IsNullOrEmpty(_wbsNoNew))
            {
                wbsNoNew = _wbsNoNew;
                ignorewbsNoNew = false;
            }

            //if (!string.IsNullOrEmpty(_bookingNo))
            //{
            //    bookingNo = _bookingNo;
            //    ignoreBookingNo = false;
            //}

            if (!string.IsNullOrEmpty(_bookingNo))
            {
                bookingNo = _bookingNo;
                iBookingNo = LikeLogic.ignoreWhat(_bookingNo, ref sBookingNoPre, ref sBookingNoPost);
                ignoreBookingNo = false;
            }

            if (!string.IsNullOrEmpty(_txtBDYear))
            {
                txtBDYear = Convert.ToInt32(_txtBDYear);
                ignoreYear = false;
            }
          
            if (!string.IsNullOrEmpty(_openStatus))
            {
                openStatus = Convert.ToInt32(_openStatus);
                ignoreOpenStatus = false;
            }

            if (!string.IsNullOrEmpty(_extend))
            {
                extend = Convert.ToInt32(_extend);
                ignoreExtend = false;
            }

            if (!string.IsNullOrEmpty(_shifted))
            {
                shifted = Convert.ToInt32(_shifted);
                ignoreShifted = false;
            }

            if (!string.IsNullOrEmpty(_pvType))
            {
                pvType = Convert.ToInt32(_pvType);
                ignorePvType = false;
            }

            if (!string.IsNullOrEmpty(_suspenseNoOld))
            {
                suspenseNoOld = Convert.ToInt32(_suspenseNoOld);
                ignoreSuspenseNoOld = false;
            }

            if (!string.IsNullOrEmpty(_suspenseNoNew))
            {
                suspenseNoNew = Convert.ToInt32(_suspenseNoNew);
                ignoreSuspenseNoNew = false;
            }
            #endregion

        }
    }
}
