﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common;
using System.Collections;

namespace BusinessLogic
{
    public class TextTickels : ITick
    {
        private string fn = "";
        private int tabs = 0;
        private DateTime LastSpin = DateTime.Now;
        public void In(string w, params object[] x)
        {
            if (string.IsNullOrEmpty(fn))
            {
                int ia  = w.IndexOfAny(new char[] {' ', '[', '(', ')', '[', '\t', '\r', '\n', '_', '`', '\\', '|', '/'}) ;
                fn = w.Substring(0, (ia > 1) ? ia : w.Length);
            }            
            Op(tabs, w, x);
            tabs++;
        }

        public int Level
        {
            get
            {
                return tabs; 
            }
            set
            {
                tabs = value;                
            }
        }

        public string Op(int op = 0, string x = null)
        {
            Op(op, x, null);
            return null;
        }

        public string Op(int op = 0, string x = null, params object[] xx)
        {
            TimeSpan t = (DateTime.Now - LastSpin);
            if (t.TotalSeconds > 10) {
                int tos = Convert.ToInt32( Math.Round(t.TotalSeconds, 0));
                string s = "";
                int r = tos % 60;
                int a = 2;
                s = s + r.ToString("00");
                tos = tos / 60;
                if (tos > 0)
                {
                    r = tos % 60;
                    s = r.ToString("00") + ":" + s;
                    tos = tos / 60;
                    --a;
                    if (tos > 0)
                    {
                        s = tos.ToString("00") + ":" + s;
                        --a;
                    }
                }
                LoggingLogic.say(fn, "\r\n{0}{1}", new string('.',a*3), s);
            }
            LastSpin = DateTime.Now;
            LoggingLogic.say(fn, new string(' ', 2 * op) + x, xx);
            return null;
        }

        public void Out(string w, params object[] x)
        {
            tabs--;
            Op(tabs, w, x);             
            if (tabs <= 0)
                fn = "";
        }

        public void Spin(string w, params object[] x)
        {
            Op(tabs, w, x);
        }

        public void Trace(string w)
        {
            Op(tabs, w + "{0}", LoggingLogic.Trace(null, ".TextTickels,.InOutMark,.ITick,.Ticker"));
        }
    }
}
