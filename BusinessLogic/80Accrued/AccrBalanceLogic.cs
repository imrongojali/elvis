﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using System.Data.Objects;
using System.IO;
using System.Data.Objects.SqlClient;

using Common;
using Common.Data;
using BusinessLogic.CommonLogic;
using DataLayer.Model;
using Common.Data._80Accrued;
using BusinessLogic.VoucherForm;
using Common.Function;
using Dapper;
using BusinessLogic.AccruedForm;

namespace BusinessLogic._80Accrued
{
    public class AccrBalanceLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        protected LoggingLogic log = new LoggingLogic();
        protected readonly string _INF = "MSTD00001INF";
        protected List<string> errs = new List<string>();

        public IQueryable<AccruedListDetail> Search(AccrBalanceSearchCriteria crit)
        {
            db.CommandTimeout = 3600;
            var q = (from trab in db.vw_AccrBalance
                     select new AccruedListDetail()
                     {
                         DIVISION_ID = trab.DIVISION_ID,
                         BOOKING_NO = trab.BOOKING_NO,
                         PV_TYPE_NAME = trab.PV_TYPE_NAME,
                         BUDGET_YEAR = trab.BUDGET_YEAR,
                         PV_TYPE_CD = trab.PV_TYPE_CD,
                         WBS_NO_OLD = trab.WBS_NO_OLD,
                         WBS_NO_PR = trab.WBS_NO_PR,
                         SUSPENSE_NO_OLD = trab.PV_TYPE_CD == 2 ? trab.SUSPENSE_NO_OLD : null,
                         SUSPENSE_NO_PR = trab.SUSPENSE_NO_PR,
                         INIT_AMT = trab.INIT_AMT,
                         SHIFTED_AMT = trab.SHIFTED_AMT,
                         SPENT_AMT = trab.SPENT_AMT,
                         AVAILABLE_AMT = trab.AVAILABLE_AMT,
                         OUTSTANDING_AMT = trab.OUTSTANDING_AMT,
                         BLOCKED_AMT = trab.BLOCKED_AMT,
                         REMAIN_SHIFTING = trab.REMAIN_SHIFTING,
                         REMAIN_EXTEND = trab.REMAIN_EXTEND,
                         EXPIRE_DT = trab.EXPIRE_DT,
                         BOOKING_STS = trab.BOOKING_STS,
                         OPEN_STATUS = trab.OPEN_STATUS,
                         STATUS_CD_SHIFTED = trab.STATUS_CD_SHIFTED,
                         STATUS_CD_EXTEND = trab.STATUS_CD_EXTEND,
                         SAP_DOC_NO_CLOSE = trab.SAP_DOC_NO_CLOSE
                     }).Distinct();

            if (!crit.ignoreIssuingDivision)
            {
                if (!crit.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == crit.issuingDivision));
                else
                {
                    q = q.Where(p => crit.divs.Contains(p.DIVISION_ID));
                }
            }

            if (!crit.ignorewbsNoOld)
            {
                q = q.Where(p => p.WBS_NO_OLD == crit.wbsNoOld);
            }

            if (!crit.ignorewbsNoNew)
            {
                q = q.Where(p => p.WBS_NO_PR == crit.wbsNoNew);
            }

            if (!crit.ignoreDateFrom)
            {
                q = q.Where(p => p.EXPIRE_DT != null && p.EXPIRE_DT >= crit.expiryDtFrom);
            }
            if (!crit.ignoreDateTo)
            {
                q = q.Where(p => p.EXPIRE_DT != null && p.EXPIRE_DT <= crit.expiryDtTo);
            }

            if (!crit.ignoreSuspenseNoOld)
            {
                q = q.Where(p => p.SUSPENSE_NO_OLD == crit.suspenseNoOld);
            }

            if (!crit.ignoreSuspenseNoNew)
            {
                q = q.Where(p => p.SUSPENSE_NO_PR == crit.suspenseNoNew);
            }

            if (!crit.ignorePvType)
            {
                q = q.Where(p => p.PV_TYPE_CD == crit.pvType);
            }

            if (!crit.ignoreYear)
            {
                q = q.Where(p => p.BUDGET_YEAR == crit.txtBDYear);
            }

            if (!crit.ignoreOpenStatus)
            {
                q = q.Where(p => p.BOOKING_STS == crit.openStatus);
            }

            //if (!crit.ignoreBookingNo)
            //{
            //    q = q.Where(p => p.BOOKING_NO == crit.bookingNo);
            //}

            if (!crit.ignoreBookingNo)
            {
                switch (crit.iBookingNo)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.BOOKING_NO.Contains(crit.sBookingNoPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.BOOKING_NO.StartsWith(crit.sBookingNoPre) && p.BOOKING_NO.EndsWith(crit.sBookingNoPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.BOOKING_NO.Contains(crit.bookingNo));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }



            if (!crit.ignoreExtend)
            {
                q = q.Where(p => p.STATUS_CD_EXTEND == crit.extend);
            }

            if (!crit.ignoreShifted)
            {
                q = q.Where(p => p.STATUS_CD_SHIFTED == crit.shifted);
            }
            
            return q;
        }

        public List<AccruedListDetail> SearchDetail()
        {

            List<AccruedListDetail> listAccrBalance = new List<AccruedListDetail>();

            try
            {
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.WBS_NO_OLD as WbsNumber, v.[Description] \r\n" +
                            "  FROM TB_R_ACCR_BALANCE ab left join dbo.vw_WBS v ON ab.WBS_NO_OLD = v.WbsNumber \r\n" +
                            " "

                            );


                listAccrBalance = Db.Query<AccruedListDetail>(sql.ToString()).ToList();

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return listAccrBalance;
        }

        public IQueryable<AccruedListDetail> GetHeader(string accruedNo)
        {
            db.CommandTimeout = 3600;
            var q = (from tralh in db.TB_R_ACCR_LIST_H
                     join trald in db.TB_R_ACCR_LIST_D on tralh.ACCRUED_NO equals trald.ACCRUED_NO into a
                     from trald in a.DefaultIfEmpty()
                     join tmapt in db.TB_M_ACCR_PV_TYPE on trald.PV_TYPE_CD equals tmapt.PV_TYPE_CD into b
                     from tmapt in b.DefaultIfEmpty()
                     join vd in db.vw_Division on tralh.DIVISION_ID equals vd.DIVISION_ID into e
                     from vd in e.DefaultIfEmpty()
                     select new AccruedListDetail()
                     {
                         ACCRUED_NO = tralh.ACCRUED_NO,
                         DIVISION_NAME = vd.DIVISION_NAME,
                         UPDATED_DT = tralh.CHANGED_DT,
                         PV_TYPE_NAME = tmapt.PV_TYPE_NAME
                     });


            if (!String.IsNullOrEmpty(accruedNo))
            {
                q = q.Where(p => p.ACCRUED_NO == accruedNo);
            }

            return q;
        }

        public List<WBSStructure> getWbsNoOld(string _IssuingDivision ="")
        {
            if (_IssuingDivision.isEmpty())
                return null;

            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.WBS_NO_OLD as WbsNumber, (SELECT top 1 [Description] FROM vw_Wbs WHERE WbsNumber = ab.WBS_NO_OLD) AS [Description] \r\n" +
                            " FROM TB_R_ACCR_BALANCE ab" +
                            " WHERE DIVISION_ID IN (SELECT DIVISION_ID FROM vw_Division WHERE DIVISION_NAME IN (" + _IssuingDivision + ")) "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );


                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }


        public List<SuspenseComboStructure> getSuspenseNew(string _IssuingDivision = "")
        {
            if (_IssuingDivision.isEmpty()) 
                return null;

            List<SuspenseComboStructure> lstsuspense = new List<SuspenseComboStructure>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.SUSPENSE_NO_NEW as SuspenseNo, ab.WBS_NO_OLD as WbsNumber,(SELECT top 1 [Description] FROM vw_Wbs WHERE WbsNumber = ab.WBS_NO_OLD) AS [Description] \r\n" +
                            " FROM TB_R_ACCR_BALANCE ab " +
                            " WHERE SUSPENSE_NO_NEW IS NOT NULL AND DIVISION_ID IN (SELECT DIVISION_ID FROM vw_Division WHERE DIVISION_NAME IN (" + _IssuingDivision + ")) "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );


                lstsuspense = Db.Query<SuspenseComboStructure>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstsuspense;
        }

        public List<SuspenseComboStructure> getSuspenseOld(string _IssuingDivision = "")
        {
            if (_IssuingDivision.isEmpty())
                return null;

            List<SuspenseComboStructure> lstsuspense = new List<SuspenseComboStructure>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.SUSPENSE_NO_OLD as SuspenseNo, ab.WBS_NO_OLD as WbsNumber, (SELECT top 1 [Description] FROM vw_Wbs WHERE WbsNumber = ab.WBS_NO_OLD) AS [Description]  \r\n" +
                            " FROM TB_R_ACCR_BALANCE ab" +
                            " WHERE SUSPENSE_NO_OLD IS NOT NULL AND DIVISION_ID IN (SELECT DIVISION_ID FROM vw_Division WHERE DIVISION_NAME IN (" + _IssuingDivision + ")) "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );


                lstsuspense = Db.Query<SuspenseComboStructure>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstsuspense;
        }

        public List<ComboClassData> getComboData(string query, string options = "")
        {
            var lstWbsResult = new List<ComboClassData>();
            try
            {
                if (string.IsNullOrEmpty(options))
                    lstWbsResult.Add(new ComboClassData {COMBO_CD = "", COMBO_NAME = "All"});
                lstWbsResult.AddRange(Db.Query<ComboClassData>((new StringBuilder(query)).ToString()).ToList());
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }

        public List<WBSStructure> getWbsNoNew(string _IssuingDivision = "")
        {
            if (_IssuingDivision.isEmpty())
                return null;

            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                //string fWbs = "wbs_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
                StringBuilder sql = new StringBuilder(
                            "SELECT DISTINCT ab.WBS_NO_PR as WbsNumber, (SELECT top 1 [Description] FROM vw_Wbs WHERE WbsNumber = ab.WBS_NO_OLD) AS [Description]  \r\n" +
                            "  FROM TB_R_ACCR_BALANCE ab \r\n" +
                            " WHERE DIVISION_ID IN (SELECT DIVISION_ID FROM vw_Division WHERE DIVISION_NAME IN (" + _IssuingDivision + ")) "
                    // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );


                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
                //LoggingLogic.say(fWbs, sql.ToString());

            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }

        public bool UpdateDetail(List<AccruedListDetail> ListItem, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();


                    foreach (AccruedListDetail d in ListItem)
                    {
                        if (d.BOOKING_NO != null && d.WBS_NO_PR != null)
                        {
                            var set_d = (from i in db.TB_R_ACCR_BALANCE
                                         where i.BOOKING_NO == d.BOOKING_NO
                                         select i).FirstOrDefault();
                            if (set_d != null)
                            {
                                set_d.WBS_NO_PR = d.WBS_NO_PR;
                                set_d.CHANGED_BY = userData.USERNAME;
                                set_d.CHANGED_DT = DateTime.Now;

                                db.SaveChanges();
                            }
                        }
                    }
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;

        }

        public bool CloseAccrued(string bookingNo)
        {
            bool ret = false;
            try
            {
                var q = Qx<string>("CloseAccrOperation", new { bookingNo = bookingNo });
                ret = true;
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return ret;
        }


        //add by FID.Arri on 8 May 2018 for AccrBalanceReport
        public string GenerateListAccrBalanceResult(
            string _username,
            string templateFile,
            string newFile,
            AccrBalanceSearchCriteria c,
            UserData userData,
            string pdfExport = "")
        {

            if (!File.Exists(templateFile))
            {
                log.Log("MSTD00002ERR", "Template file : '" + templateFile + "' not found");
                return null;
            }

            ExcelWriter x = new ExcelWriter(templateFile);
            string _sheetName = "ACCR";

            x.MarkPage(_sheetName);

            //Get data from search criteria
            var qq = Search(c);
            List<AccruedListDetail> _list =
                qq.ToList();
            _list.Sort(AccruedListDetail.RuleOrder);

            //Set value Header
            string titleReport = "LIST ACCRUED BALANCE TAHUN " + DateTime.Now.ToString("yyyy");

            var divCode = (from vDiv in db.vw_Division
                           select new Division()
                           {
                               DivisionID = vDiv.DIVISION_ID,
                               DivisionName = vDiv.DIVISION_NAME,
                               DivisionFullName = vDiv.FULL_NAME,
                           }).Distinct();
            if (!c.issuingDivision.Contains(";"))
                divCode = divCode.Where(p => (p.DivisionID == c.issuingDivision));
            else
            {
                divCode = divCode.Where(p => c.divs.Contains(p.DivisionID));
            }
            //divCode = divCode.Where(p => p.DivisionID.Contains(c.issuingDivision));
            string listDiv = "";
            foreach (var div in divCode)
            {
                listDiv = listDiv + div.DivisionFullName + ", ";
            }

            string divisionName = String.IsNullOrEmpty(listDiv) ? "All" : listDiv;
            string bookingNo = String.IsNullOrEmpty(c.bookingNo) ? "All" : c.bookingNo;
            string pvType = c.pvType.Equals(-1) ? "All" : c.pvType.Equals(1) ? "Direct" : c.pvType.Equals(2) ? "Suspense" : "PR";
            string expiryDtFr = c.expiryDtFrom != DateTime.MinValue ? c.expiryDtFrom.ToString("dd-MM-yyyy") : "";
            string expiryDtTo = c.expiryDtTo != DateTime.MinValue ? c.expiryDtTo.ToString("dd-MM-yyyy") : "";

            string openSts = c.openStatus.Equals(-1) ? "All" : c.openStatus.Equals(1) ? "Closed" : "Open";
            string extended = c.extend.Equals(-1) ? "All" : c.extend.Equals(1) ? "Yes" : "No";
            string shifted = c.shifted.Equals(-1) ? "All" : c.shifted.Equals(1) ? "Yes" : "No";

            string printedBy = userData.FIRST_NAME + " " + userData.LAST_NAME;
            string printedDt = DateTime.Now.ToString("dd-MM-yyyy");

            decimal? totalInitial = 0;
            decimal? totalSpent = 0;
            decimal? totalAvailable = 0;
            decimal? totalOutstanding = 0;
            decimal? totalBlocked = 0;
            decimal? totalPV = 0;
            decimal? totalExtend = 0;

            foreach (AccruedListDetail hAccr in _list)
            {
                totalInitial = totalInitial + hAccr.INIT_AMT;
                totalSpent = totalSpent + hAccr.SPENT_AMT;
                totalAvailable = totalAvailable + hAccr.AVAILABLE_AMT;
                totalOutstanding = totalOutstanding + hAccr.OUTSTANDING_AMT;
                totalBlocked = totalBlocked + hAccr.BLOCKED_AMT;
                totalPV = totalPV + hAccr.REMAIN_SHIFTING;
                totalExtend = totalExtend + hAccr.REMAIN_EXTEND;
            }

            x.MarkCells("[TITLE_REPORT],[DIVISION],[BOOKING_NO],[PV_TYPE],[EXP_DATE_FR],[EXP_DATE_TO],[OPEN_STS],[EXTENDED],[SHIFTED],[PRINTED_BY],[PRINTED_DT],[TOTAL_INITIAL],[TOTAL_SPENT],[TOTAL_AVAILABLE],[TOTAL_OUTSTANDING],[TOTAL_BLOCKED],[TOTAL_PV],[TOTAL_EXT]");
            x.PutCells(titleReport, divisionName, bookingNo, pvType, expiryDtFr, expiryDtTo, openSts, extended, shifted, printedBy, printedDt, totalInitial.fmt(2).ToString(), totalSpent.fmt(2).ToString(), totalAvailable.fmt(2).ToString(), totalOutstanding.fmt(2).ToString(), totalBlocked.fmt(2).ToString(), totalPV.fmt(2).ToString(), totalExtend.fmt(2).ToString());
            //End set value Header

            //Set Value Detail
            x.MarkRow("[BOOKING_NO],[BUDGET_YEAR],[EXPIRY_DATE],[OPEN_STS],[ACTIVITY],[PV_TYPE_REC],[WBS_NO_OLD],"
                + "[WBS_NO_NEW],[SUSPENSE_NO_OLD],[SUSPENSE_NO_NEW],[INITIAL],"
                + "[SHIFTED],[SPENT],[AVAILABLE],[OUTSTANDING],[BLOCKED],[REMAIN_PV],"
                + "[REMAIN_EXT],[CLOSING_SAP_NO]");

            int j = 0;

            List<AccruedListDetail> act = null;
            Dictionary<int, string> doctype = logic.Look.GetDocType();
            foreach (AccruedListDetail dx in _list)
            {
                string activity = "";
                act = GetViewActivity(dx.BOOKING_NO);
                if (act.Count() != 0)
                {
                    activity = act.FirstOrDefault().ACTIVITY_DES;
                }
                else
                {
                    activity = "";
                }
                dx.EXPIRE_DT_STR = dx.EXPIRE_DT.HasValue ? dx.EXPIRE_DT.Value.ToString("dd-MM-yyyy") : "";
                //Input Data to list detail
                x.PutRowInsert(dx.BOOKING_NO, dx.BUDGET_YEAR == 0 ? 0 : dx.BUDGET_YEAR, dx.EXPIRE_DT_STR, dx.OPEN_STATUS, activity, dx.PV_TYPE_NAME, dx.WBS_NO_OLD,
                        dx.WBS_NO_PR, dx.SUSPENSE_NO_OLD, dx.SUSPENSE_NO_PR, dx.INIT_AMT.fmt(2),
                        dx.SHIFTED_AMT.fmt(2), dx.SPENT_AMT.fmt(2), dx.AVAILABLE_AMT.fmt(2), dx.OUTSTANDING_AMT.fmt(2), dx.BLOCKED_AMT.fmt(2)
                        , dx.REMAIN_SHIFTING, dx.REMAIN_EXTEND, String.IsNullOrEmpty(dx.SAP_DOC_NO_CLOSE) ? "" : dx.SAP_DOC_NO_CLOSE);

                j++;
            }

            if (j < 1)
            {
                x.PutRow("", "", "", "", "",
                         "", "", "", "", "",
                         "", "", "", "");
            }
            //End set value Detail

            x.PutTail();
            string _strTempDir = LoggingLogic.GetTempPath("");
            string filePath = System.IO.Path.Combine(_strTempDir, newFile);
            if (!Directory.Exists(_strTempDir))
                Directory.CreateDirectory(_strTempDir);
            x.Write(filePath);

            return filePath;
            //if (String.IsNullOrEmpty(pdfExport))
            //{
            //    return filePath;
            //}
            //else
            //{
            //    string pdfPath = System.IO.Path.Combine(_strTempDir, pdfExport);
            //    bool rslt = x.ExportWorkbookToPdf(filePath, pdfPath);
            //    if (rslt)
            //    {
            //        return pdfPath;
            //    }
            //    else
            //    {
            //        return filePath;
            //    }

            //}
        }
		
		//fid.pras
        public List<AccruedListDetail> GetViewActivity(string bookingNo) //, string bookingNo)
        {
            List<AccruedListDetail> returnData = new List<AccruedListDetail>();
            returnData = Qx<AccruedListDetail>("GetActivityAccr", new { BOOKING_NO = bookingNo }).ToList(); //, BOOKING_NO = bookingNo

            return returnData;
        }

        public bool Submit(AccrFormData FormData, UserData userData)
        {
            DbTransaction TX = null;
            bool result = false;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var db = co.db;
                    TX = db.Connection.BeginTransaction();

                    var ListBooking = FormData.Details
                                        .GroupBy(x => x.BookingNo)
                                        .Select(x => new AccrFormDetail { 
                                            BookingNo = x.First().BookingNo,
                                            AmountIdr = x.Sum(a => a.AmountIdr),
                                            PVTypeCd = x.First().PVTypeCd,
                                            SuspenseNo = x.First().SuspenseNo,
                                            WbsNumber = x.First().WbsNumber
                                        }
                                        );
                    string divId = FormData.IssuingDiv;

                    foreach (AccrFormDetail d in ListBooking)
                    {
                        if (d.BookingNo != null)
                        {
                            var bal = (from i in db.TB_R_ACCR_BALANCE
                                         where i.BOOKING_NO == d.BookingNo
                                         select i).FirstOrDefault();
                            if (bal != null)
                            {
                                bal.INIT_AMT = bal.INIT_AMT + d.AmountIdr;
                                bal.AVAILABLE_AMT = bal.AVAILABLE_AMT + d.AmountIdr;
                                bal.BUDGET_AMT = bal.BUDGET_AMT + d.AmountIdr;
                                bal.POSTING_FLAG = 0;
                                bal.CHANGED_BY = userData.USERNAME;
                                bal.CHANGED_DT = DateTime.Now;

                                db.SaveChanges();
                            }
                            else
                            {
                                int nxYr = DateTime.Now.Year + 1;
                                DateTime expDt = new DateTime(nxYr, 3, 20);
                                string strExpDt = logic.Sys.GetText("ACCR_EXPIRY", "DEFAULT");
                                if (strExpDt.isNotEmpty() && strExpDt.Contains("/"))
                                {
                                    var spl = strExpDt.Split('/');
                                    try
                                    {
                                        expDt = new DateTime(nxYr, spl[1].Int(), spl[0].Int());
                                    }
                                    catch (Exception e)
                                    {
                                        logic.Say("Insert/Update Balance", "Failed to use expired date from system master : {0}", strExpDt);
                                        Handle(e);
                                    }
                                }

                                bal = new TB_R_ACCR_BALANCE();

                                bal.BOOKING_NO = d.BookingNo;
                                bal.WBS_NO_OLD = d.WbsNumber;
                                bal.PV_TYPE_CD = d.PVTypeCd;
                                bal.DIVISION_ID = divId;
                                bal.INIT_AMT = d.AmountIdr;
                                bal.SPENT_AMT = 0;
                                bal.AVAILABLE_AMT = d.AmountIdr;
                                bal.BOOKING_STS = 0;
                                bal.BLOKING_AMT = 0;
                                bal.CREATED_DT = DateTime.Now;
                                bal.CREATED_BY = userData.USERNAME;
                                bal.OUTSTANDING_AMT = 0;
                                bal.BUDGET_AMT = d.AmountIdr;
                                bal.SUSPENSE_NO_OLD = d.SuspenseNo.isNotEmpty() ? (int?)d.SuspenseNo.Int() : null;
                                bal.EXPIRE_DT = expDt;
                                bal.POSTING_FLAG = 0;

                                db.TB_R_ACCR_BALANCE.AddObject(bal);
                                db.SaveChanges();

                                logic.AccrForm.InsertAccrReffNo(d.BookingNo, 6);
                            }
                        }
                    }
                    TX.Commit();
                    result = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    if (TX != null)
                        TX.Rollback();
                    result = false;
                }
            }

            return result;
        }

        public List<TB_R_ACCR_BALANCE> GetBalanceByAccruedNo(string accrNo, int accrType, ELVIS_DBEntities DB = null)
        {
            if (DB == null) DB = db;

            return (from b in DB.TB_R_ACCR_BALANCE
                    join d in DB.TB_R_ACCR_LIST_D
                    on b.BOOKING_NO equals d.BOOKING_NO
                    where d.ACCRUED_NO == accrNo
                         && d.PV_TYPE_CD == accrType
                    select b).ToList();
        }
        public AccruedBalanceData GetBalanceByBookingNo(string bookingNo)
        {
            return GetBalancesByBookingNo(new List<string>() { bookingNo }).FirstOrDefault();
        }
        public List<AccruedBalanceData> GetBalancesByBookingNo(List<string> bookingNo)
        {
            var bal = (from b in db.TB_R_ACCR_BALANCE
                       join a in db.TB_R_ACCR_DOC_NO on b.BOOKING_NO equals a.ACCR_NO into ba
                       from a in ba.DefaultIfEmpty()
                       where bookingNo.Contains(b.BOOKING_NO)
                       select new AccruedBalanceData
                       {
                           BOOKING_NO = b.BOOKING_NO,
                           REFF_NO = SqlFunctions.StringConvert(a.ACCR_DOC_NO).Trim(),
                           INIT_AMT = b.INIT_AMT,
                           SPENT_AMT = b.SPENT_AMT,
                           AVAILABLE_AMT = b.AVAILABLE_AMT,
                           BLOKING_AMT = b.BLOKING_AMT,
                           OUTSTANDING_AMT = b.OUTSTANDING_AMT,
                           BUDGET_AMT = b.BUDGET_AMT,
                           POSTING_FLAG = b.POSTING_FLAG,
                           BOOKING_STS = b.BOOKING_STS,
                           EXPIRED_DT = b.EXPIRE_DT,
                           CREATED_DT = b.CREATED_DT
                       }).ToList();

            return bal;
        }

        public bool isClosed(string bookingNo)
        {
            var sts = (from b in db.TB_R_ACCR_BALANCE
                       where b.BOOKING_NO == bookingNo
                       select b.BOOKING_STS).FirstOrDefault();

            return sts == 1;
        }
        public bool isAnyClosed(List<string> bookingNo) 
        {
            var bal = (from b in db.TB_R_ACCR_BALANCE
                       where bookingNo.Contains(b.BOOKING_NO)
                       select b.BOOKING_STS);

            return bal.Any(x => x == 1);
        }
    }
}
