﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Resources;

namespace BusinessLogic.CommonLogic
{
    public class GlobalResourceLogic
    {
        public string GetResxObject(string ClassName, string ResourceKey)
        {
            string _strBaseName = "BusinessLogic.App_GlobalResource." + ClassName;
            ResourceManager _rescManager = new ResourceManager(_strBaseName, Assembly.GetExecutingAssembly());
            return _rescManager.GetString(ResourceKey);
        }
    }
}
