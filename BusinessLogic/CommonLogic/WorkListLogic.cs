﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Model;
using DataLayer;
using BusinessLogic.PoA;
using System.Web;
using K2.Helper;
using Common;
using Common.Data;
using Common.Function;
using System.IO;

namespace BusinessLogic.CommonLogic
{
    public class WorkListLogic
    {
        LogicFactory logic = LogicFactory.Get();
        private string _UserName = null;

        public readonly string WID = "User.Worklist";
        public List<WorklistHelper> Data
        {
            get
            {
                List<WorklistHelper> l = null;
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    l = HttpContext.Current.Session[WID] as List<WorklistHelper>;
                }
                return l;
            }

            set
            {
                if ((HttpContext.Current != null) && (HttpContext.Current.Session != null))
                {
                    HttpContext.Current.Session[WID] = value;
                }
            }
        }

        public void Remove(string folio)
        {
            if (Data != null && Data.Count > 0)
            {
                int j = -1;
                StringBuilder b = new StringBuilder("");
                for (int i = 0; i < Data.Count; i++)
                {
                    WorklistHelper w = Data[i];
                    // b.AppendFormat("{0}\t'{1}'\t'{2}'\t'{3}'\t'{4}'\r\n", w.Folio, w.SN, w.FullName, w.StartDate, w.UserID);
                    if (Data[i].Folio.Equals(folio)) { j = i; break; }
                }
                b.Append("Search: " + folio + " @ " + j + "\r\n");
                // System.IO.File.WriteAllText(Common.Util.GetTmp(LoggingLogic.GetPath(), "worklist", ".txt"), b.ToString());
                if (j >= 0) Data.RemoveAt(j);
            }
        }

        public void Reload(string userName)
        {
            List<WorklistHelper> worklist = new List<WorklistHelper>();
            if (_UserName.isEmpty() && !userName.isEmpty())
                _UserName = userName;

            var blPOA = new PoALogic();
            if (HttpContext.Current != null && HttpContext.Current.Server != null)
                blPOA.emailTemplateDir = HttpContext.Current.Server.MapPath(logic.Sys.GetText("EMAIL_TEMPLATE", "DIR"));

            try
            {
                logic.k2.Open();
                // logic.Say("WorklistLogic", "K2Helper.GetWorklist({0})", _UserName);
                var myWorklist = logic.k2.GetWorklist(_UserName);
                Ticker.In("GetGrantorByAttorney");
                var lstGrantor = blPOA.GetGrantorByAttorney(_UserName, false);
                Ticker.Out();
                foreach (var kvp in myWorklist)
                {
                    WorklistHelper wh = new WorklistHelper();
                    wh.Folio = kvp.Key;
                    wh.SN = kvp.Value;
                    worklist.Add(wh);
                }
                if (lstGrantor != null)
                {
                    foreach (var item in lstGrantor)
                    {
                        // LoggingLogic.say(_UserName, "k2.GetWorklist({0})", item.GRANTOR);
                        var grantorWorklist = logic.k2.GetWorklist(item.GRANTOR);

                        foreach (var kvp in grantorWorklist)
                        {
                            WorklistHelper wh = new WorklistHelper();
                            wh.Folio = kvp.Key;
                            wh.SN = kvp.Value;
                            worklist.Add(wh);
                        }
                    }
                }
                logic.k2.Close();
            }
            catch (Exception e)
            {
                LoggingLogic.err(e);
            }
            Data = worklist;

        }

        public WorkListLogic()
        {
        }

        public WorkListLogic(string _UserName)
        {
            logic.Say("WorklistLogic", "new WorklistLogic({0})", _UserName);

            if (Data != null && Data.Count > 0)
                return;

            Ticker.In("new WorkListLogic " + _UserName);
            Reload(_UserName);
            Ticker.Out();
        }

        public bool isUserHaveWorklist(string _Folio)
        {
            List<WorklistHelper> l = null;
           
            Common.Data.UserData u = logic.User.SessionUserData(HttpContext.Current);
            bool hasWorklist = false;
            if (u == null || string.IsNullOrEmpty(u.USERNAME))
                return false;
            
            try
            {
                logic.k2.Open();
                l = logic.k2.GetWorklistFiltered(u.USERNAME, "folio", _Folio);
                hasWorklist = (l != null && l.Count > 0 && l.Where(f => f.Folio == _Folio).Any());
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            finally
            {
                logic.k2.Close();
            }
            
            return hasWorklist;
        }

    }
}
