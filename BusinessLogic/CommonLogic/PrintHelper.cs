﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Function;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace BusinessLogic.CommonLogic
{
    public class PrintHelper
    {
        public static bool WidthsParse(Ini i, string id, ref float[] f)
        {
            string s = i[id];
            if (!s.isEmpty())
            {
                f = s.Split(',')
                    .Select(x => (float)Convert.ToDouble(x))
                    .ToArray();
                return (f != null && f.Length > 0);
            }

            return false;
        }

        public static int padMF = 1;
        public static void coverMFPadding(PdfPCell p)
        {
            p.PaddingTop = padMF;
            p.PaddingBottom = padMF;
        }

        public static void coverMFRow(PdfPTable p, PdfPCell d, string x)
        {
            PdfPCell c0 = new PdfPCell(new Phrase(x, new Font(Font.FontFamily.HELVETICA, 8)));
            c0.BorderWidthTop = 0;
            c0.BorderWidthBottom = 0;
            c0.BorderWidthRight = 0;
            c0.BorderWidthLeft = 0;

            coverMFPadding(c0);
            p.AddCell(c0);

            d.PaddingTop = 0;
            d.PaddingBottom = 0;

            if (x.Contains("SAP"))
            {
                d.BorderWidthTop = 0;
                d.BorderWidthBottom = 0;
                d.BorderWidthRight = 0;
                d.BorderWidthLeft = 0;
            }
            p.AddCell(d);
        }

    }
}
