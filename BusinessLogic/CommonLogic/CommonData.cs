﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessLogic.CommonLogic;
using DataLayer;
using DataLayer.Model;
using System.Data.EntityClient;
using System.Data;
using Common.Function;
using Dapper;

namespace BusinessLogic
{
    public static class CommonData
    {
        private static string[] roundCurrencies = new string[] { };

        public static string[] RoundCurrencies
        {
            get
            {
                if (roundCurrencies.Length < 1)
                {
                    SystemSettingLogic _set = new SystemSettingLogic();

                    string rc = _set.GetText("INVOICE_UPLOAD", "NO_FRACTION_CURRENCY");
                    if (string.IsNullOrEmpty(rc))
                    {
                        roundCurrencies = new string[] { "IDR", "JPY" };
                    }
                    else
                    {
                        roundCurrencies = rc.Split(',');
                    }
                }
                return roundCurrencies;
            }
        }


        private static List<CodeConstant> paymentMethod = null;
        public static List<CodeConstant> PaymentMethod
        {
            get
            {
                if (paymentMethod == null)
                {
                    LogicFactory lo = LogicFactory.Get();
                    paymentMethod= lo.Base.hQu<CodeConstant>("GetPaymentMethods").ToList();
                }
                return paymentMethod;
            }
        }

        public static List<CodeConstant> GetPVTypes(int exclude)
        {
            return LogicFactory.Get().Base.hQu<CodeConstant>("GetPvTypes", exclude).ToList();
        }

        private static List<CodeConstant> pvTypes = null;
        public static List<CodeConstant> PVTypes
        {
            get
            {
                if (pvTypes == null)
                {
                    pvTypes = GetPVTypes(3);
                }
                return pvTypes;
            }
        }


        public static List<CodeConstant> GetRVTypes(int exclude)
        {
            return LogicFactory.Get().Base.hQu<CodeConstant>("GetRvTypes", exclude).ToList();
        }

        private static List<CodeConstant> rvTypes = null;
        public static List<CodeConstant> RVTypes
        {
            get
            {
                if (rvTypes == null)
                {
                    rvTypes = GetRVTypes(2);
                }
                return rvTypes;
            }
        }

        //fid.Hadid
        public static List<CodeConstant> GetAccrPVTypes(int exclude)
        {
            return LogicFactory.Get().Base.Qu<CodeConstant>("GetAccrPvTypes", exclude).ToList();
        }

        private static List<CodeConstant> accrPvTypes = null;
        public static List<CodeConstant> AccrPVTypes
        {
            get
            {
                if (accrPvTypes == null)
                {
                    accrPvTypes = GetAccrPVTypes(0);
                }
                return accrPvTypes;
            }
        }

        public static List<CodeConstant> pvTypeCodes = GetPVTypes(0);
        //end fid.Hadid

        public static List<CodeConstant> rvTypeCodes = GetRVTypes(0);
        public static List<CodeConstant> accrPvTypeCodes = GetAccrPVTypes(0);

        // Rinda Rahayu 20160404
        public static List<CodeConstant> GetTransactionTypesSkipApproval()
        {
            return LogicFactory.Get().Base.hQu<CodeConstant>("GetTransactionTypesSkipApproval", null).ToList();
        }
        
    }
}
