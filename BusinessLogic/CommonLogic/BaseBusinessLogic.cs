﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.EntityClient;
using System.Linq;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using Common.Function;
using DataLayer;
using DataLayer.Model;
using System.Web;
using Dapper;

namespace BusinessLogic.CommonLogic
{
    public class BaseBusinessLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        #region isNotExceedMaxLength
        public bool isNotExceedMaxLength(string StrInput, int maxLength)
        {
            try
            {
                if (StrInput.Length <= maxLength)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
        }
        #endregion

        #region isInteger
        public bool IsNotInteger(string StrInput)
        {
            try
            {
                Convert.ToInt32(StrInput);
                return false;
            }
            catch
            {
                return true;
            }
        }
        #endregion

        #region Reference Codes
        public bool isWbs(string Wbs)
        {
            return (hQx<int>("GetIsWbs", new { Wbs = Wbs }).FirstOrDefault() > 0);
        }

        public bool isVendorCode(string vendorCd)
        {
            return (hQx<int>("GetIsVendor", new { vendorCd = vendorCd }).FirstOrDefault() > 0);
        }
        public bool AllowedVendorGroup(string vendorCd)
        {
            return (hQx<int>("GetIsAllowedVendorGroup", new { vendorCd = vendorCd }).FirstOrDefault() > 0);           
        }

        public List<TransactionType> GetTransactionTypes()
        {
            return (hQx<TransactionType>("GetTransactionTypes", new {code = 0}).ToList());
        }

        public TransactionType GetTransactionType(int code)
        {
            return (hQx<TransactionType>("GetTransactionTypes", new { code = code }).FirstOrDefault());

        }

        public List<int> GetTransactionCodes()
        {
            return hQu<int>("GetTransactionCodes").ToList();                       
        }
        public List<string> GetTaxCodes()
        {
            return Qu<string>("GetTaxCodes").ToList();
        }
        public List<ItemTransactionData> GetTransactionItems()
        {
            return Qu<ItemTransactionData>("GetItemTransactions").ToList();
        }

        public List<string> GetCurrencies()
        {
            return Qu<string>("GetCurrencies").ToList();
        }

        public List<string> GetDivisionIds()
        {
            return Qu<string>("GetDivisionIds").ToList();
        }
        #endregion

        public List<ApprovalData> GetApprovalOverview(string txtPVNo, string txtPVYear, int aHead, UserData u = null, int StatusCd = 0)
        {
          
            List<ApprovalData> ad = new List<ApprovalData>();
            decimal rNo = (txtPVNo + txtPVYear).Dec();

            if (rNo < 1000) return ad; // don't bother 

            try
            {
                var q = Qu<ApprovalData>("ApprovalOverview", rNo, txtPVNo, txtPVYear, aHead).ToList();
               
                if (q.Any())
                {
                    int StatusX = StatusCd;

                    if (StatusCd > 0)
                    {
                        StatusX = logic.Look.GetApproveStatus(StatusCd);
                    }
                    foreach (var al in q)
                    {
                        int status = 0;
                        if (al.ACTUAL_DT != null)
                            status = 1;

                        //string fullName = al.PROCEED_BY;
                        string pos = al.CLASS.str();

                      
                        DateTime? actual = al.ACTUAL_DT;
                        if (StatusX > 0)
                        {
                            if (actual == null)
                            {
                                if (al.STATUS_CD <= StatusX && StatusCd == StatusX)
                                {
                                    status = 1;
                                    actual = DateTime.Now;
                                }
                                else if (al.STATUS_CD == StatusX && StatusCd != StatusX)
                                {
                                    status = 2;
                                }
                            }
                        }

                        ad.Add(new ApprovalData()
                        {
                            CODE = al.CODE,
                            FULL_NAME = al.FULL_NAME,
                            ACTUAL_DT = actual,
                            STATUS = status,
                            SKIP_FLAG = al.SKIP_FLAG,
                            STATUS_CD = al.STATUS_CD,
                            PROCEED_BY = al.PROCEED_BY,
                            CLASS = al.CLASS
                        });

                    }
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }

            return ad;

        }

        public List<AmountData> GetAmounts(int no, int yy)
        {
            return Qx<AmountData>("GetAmountData", new { no = no, yy = yy }).ToList();          
        }

        public List<OriginalAmountData> GetOriginal(int no, int yy)
        {
            return Qx<OriginalAmountData>("GetOriginal", new { no = no, yy = yy }).ToList();
        }

        public List<string> GetContextItems()
        {
            List<string> r = new List<string>();
            foreach (string k in HttpContext.Current.Items.Keys)
            {
                r.Add(k);
            }
            return r;
        }

        public static void CopyWorklistData(WorklistData a, WorklistData b)
        {
            a.Description = b.Description;
            a.HypValue = b.HypValue;
            a.TotalAmount = b.TotalAmount;
            a.PaidDate = b.PaidDate;
            a.HoldFlag = b.HoldFlag;
            a.Notices = b.Notices;
            a.NeedReply = b.NeedReply;
            a.DocDate = b.DocDate;
            a.DivName = b.DivName;
            a.Rejected = b.Rejected;
            a.STATUS_CD = b.STATUS_CD;
            a.StatusName = b.StatusName;
            a.PvTypeName = b.PvTypeName;
            a.ActivityDateFrom = b.ActivityDateFrom;
            a.ActivityDateTo = b.ActivityDateTo;
            a.PayMethodName = b.PayMethodName;
            a.BudgetNo = b.BudgetNo;
            a.WorklistNotice = b.WorklistNotice;
            a.TransCd = b.TransCd;
        }

        #region 2018.08.07 wot.daniel + Mass Act CR 
        /// <summary>
        /// fill DocCd and StatusCd field of each items in list
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <returns>List ApproveRejectData filled StatusCd and DocType</returns>
        public List<ApproveRejectData> GetStatusCd(List<ApproveRejectData> a)
        {
            string folio = "";
            if (a.Any()) {
                folio = string.Join(",", a.Select(r=>r.Folio).ToList());
            }
            
            List<ApproveRejectData> ret = Qx<ApproveRejectData>("GetFoliosStatusCdAccrued", new { folios = folio }).ToList();
            if (!ret.Any())
                ret = new List<ApproveRejectData>();
            return ret;
        }
        #endregion

    }
}
