﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Data.OleDb;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using DataLayer.Model;
using Common;

namespace BusinessLogic.CommonLogic
{
    public delegate string LogWrite(int _pid, string _id, string _location, string _uid, params object[] x);

    public class CommonExcelUpload
    {
        #region initializing
        
        DataTable _dt = null;
        OleDbConnection oco = null;
        OleDbCommand ocmd = null;
        OleDbDataAdapter oda = null;
        private int _pid = 0;
        private string _uid = "";

        private readonly string FN_CEXLUP = "9.1.1";
        #endregion

        #region Upload Excel File to Staging Table
        /**
         * _strSheetRangeHeader = "B6:E" leave blank for rows count
         * _strSheetRangeDetail = "F6:I" leave blank for rows count
         * 
         **/

        public bool doUploadExcelFile(string _FunctionName, string _FileType, string _Path,
            string _strSheetRangeData, int _ProcessID, ref string _ErrorMessage)
        {
            return doUploadExcelFile(_FunctionName, _Path, _strSheetRangeData, _pid, ref _ErrorMessage); 
        }
        #endregion


        #region new doUpload
        public bool doUploadExcelFile(string _FunctionName, string _Path,
            string _strSheetRangeData, int _ProcessID, ref string _ErrorMessage)
        {
            bool result = false;
            string _StrConn = GetConnectionString(_Path);
            string[] _SheetName = GetExcelSheetNames();
            _pid = _ProcessID;
            _uid = "";
            say("SheetNames = {0}", string.Join(",", _SheetName));
            
            string templateSheetName = null;
            if (_SheetName.Length < 2)
                templateSheetName = _SheetName[0];


            if (string.IsNullOrEmpty(templateSheetName))
            {
                say("no data");
                return false;
            }

            DataTable _dt = null;

            try
            {
                int _rowsCountDetail = GetRowsCount(templateSheetName);
                _strSheetRangeData = _strSheetRangeData + Convert.ToString(_rowsCountDetail);
                _StrConn = GetConnectionString(_Path);
                _dt = GetTable(_FunctionName, templateSheetName, _strSheetRangeData);

                string fn = Convert.ToString(_ProcessID);

                LoggingLogic l = new LoggingLogic();
                int rownum = 0;
                foreach (DataRow r in _dt.Rows)
                {
                    ++rownum;
                    for (int i = 0; i < _dt.Columns.Count; i++)
                    {
                        say(fn, "[{0}, {1}] {2}", rownum, i, r[i]);
                    }
                }
            }
            catch (Exception e)
            {
                _ErrorMessage = e.Message;
                return false;
            }
            finally
            {
                del();

            }

            return result;
        }


        #endregion

        #region get connection string
        public string GetConnectionString(string strFileType, string strNewPath)
        {
            string _connString = null;
            if (strFileType.Trim() == ".xls")
            {
                _connString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"", strNewPath);
            }
            else if (strFileType.Trim() == ".xlsx")
            {
                _connString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"", strNewPath);
            }
            oco = new OleDbConnection(_connString);
            return _connString;
        }

        public string GetConnectionString(string ExcelFileName)
        {
            string ext = Path.GetExtension(ExcelFileName).ToString().ToLower();
            string _connString = null;
            if (ext == ".xls")
            {
                _connString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\"", ExcelFileName);
            }
            else if (ext == ".xlsx")
            {
                _connString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;IMEX=1;HDR=NO;'", ExcelFileName);
            }
            oco = new OleDbConnection(_connString);
            return _connString;
        }
        #endregion


        #region Excel Common Reader
        readonly string me = "CommonExcelUpload";
        public const string DTL = "_#rows#_";
        private string PageName = "";
        private string DataRange = "";
        private List<Marker> CellPos = new List<Marker>();
        private List<Marker> ColPos = new List<Marker>();
        private List<string> _err = new List<string>();
        Dictionary<string, object> _d = new Dictionary<string, object>();

        private string[] _cells;
        private string[] _columns;

        private int _ruleCount;
        private int _top, _left, _bottom, _right;

        private LoggingLogic _log = null;

        public LoggingLogic Log
        {
            get
            {
                if (_log == null) _log = new LoggingLogic();
                return _log;
            }
            set
            {
                _log = value;
            }
        }

        private LogWrite lw;
        public LogWrite LogWriter {
            get
            {
                if (lw != null)
                    return lw;
                else
                    return LogIt;
            }
            set
            {
                lw = value;
            }
        }

        

        public string[] Cells
        {
            get
            {
                return _cells;
            }
        }

        public string[] Columns
        {
            get
            {
                return Columns;
            }
        }

        public string this[string index]
        {
            get
            {
                if (_d.ContainsKey(index))
                {
                    return Convert.ToString(_d[index]);
                }
                else
                    return "";
            }

            set
            {
                if (_d.ContainsKey(index))
                {
                    _d[index] = value;
                }
            }
        }

        public DataTable Rows
        {
            get
            {
                if (_d.ContainsKey(DTL))
                {
                    return _d[DTL] as DataTable;
                }
                else
                    return null;
            }
        }

        public List<string> ReadError
        {
            get
            {
                return _err;
            }
        }

        private bool _errorLogging = false;
        public bool ErrorLogging
        {
            get
            {
                return _errorLogging;
            }
            set
            {
                _errorLogging = value;
            }
        }

        public string LogIt(int _pid, string _id, string _location, string _uid, params object[] x)
        {
            return "";
        }

        public string UserName
        {
            get { return _uid;  }
            set { _uid = value; } 
        }


        public int PID
        {
            get { return _pid; } 
            set { _pid = value; } 
        }

        public static int Value26(string x)
        {
            int a = 0;
            int m = 1;
            for (int i = x.Length - 1; i >= 0; i--)
            {
                int n = (int)x[i];
                a += (n - 64) * m;
                m *= 26;
            }
            return a;
        }

        public static void ShowMatches(MatchCollection c, Regex x, string matchName)
        {
            if (c == null) return;
            int i = 0;
            LoggingLogic.say("matches", "{0}={1}", matchName, x);
                
            foreach (Match m in c)
            {
                LoggingLogic.say("matches", "  [{0}] {1}", i, m.Value);
                i++;
                int j = 0;
                foreach (Group g in m.Groups)
                {
                    LoggingLogic.say("matches", "     [{0}] {1}", j, g.Value);
                    j++;
                }
            }
        }

        private void SayData()
        {
            say("PageName=" + PageName);
            say("DataRange=" + DataRange);
            say("Cell");
            foreach (Marker m in CellPos)
            {
                say("\t{0} {1}", m.pos, m.name);
            }
            say("Col");
            foreach (Marker m in ColPos)
            {
                say("\t{0} {1}", m.pos, m.name);
            }
        }

        private void Burn(MatchCollection x, Regex r, string nm)
        {
            say("{0} = {1}", nm, r.ToString());
            if (x.Count > 0)
            {
                for (int i = 0; i < x.Count; i++)
                {
                    Match m = x[i];
                    say("    [{0}] {1}", i, m.Value);
                    for (int j = 0; j < m.Groups.Count; j++)
                    {
                        say("        [{0}] {1}", j, m.Groups[j].Value);
                    }
                }
            }
            else
            {
                say("{0} no matches", nm);
            }
        }

        public void ParseMeta(string MetaData)
        {
            if (string.IsNullOrEmpty(MetaData)) return;

            PageName = "";
            DataRange = "";
            CellPos.Clear();
            ColPos.Clear();

            _ruleCount = 0;

            string[] l = MetaData.Split(new string[] { "\r\n", "|" },
                    StringSplitOptions.RemoveEmptyEntries);
            // string rulePattern = "(\\s[A-Z](\\([0-9\\-]+\\))*[*]*)*";
            string rulePattern = "(\\s+.*)*";
            Regex rxPage = new Regex("(@)([\\w\\d]+)");
            Regex rxCell = new Regex("\\[([A-Za-z]+[0-9]+)\\]\\s+(\\w+)" + rulePattern);
            Regex rxRange = new Regex("\\[(\\w+\\d+\\:\\w+\\d+)\\]");
            Regex rxColumn = new Regex("\\[([A-Za-z]+)\\]\\s+(\\w+)" + rulePattern);

            bool hasPage = false;
            bool hasRange = false;
            for (int i = 0; i < l.Length; i++)
            {
                string s = l[i].Trim();

                // ignore comments 
                if (s.IndexOfAny(new char[] { '#', '/', '*' }) == 0)
                    continue;

                // match Page 
                if (!hasPage)
                {
                    MatchCollection xPage = rxPage.Matches(s);

                    if (xPage.Count > 0 && xPage[0].Groups.Count > 0)
                    {
                        // Burn(xPage, rxPage, "Page");
                        PageName = xPage[0].Groups[2].Value;
                        hasPage = true;
                    }
                }

                // match Cell
                MatchCollection xCell = rxCell.Matches(s);

                if (xCell.Count > 0 && xCell[0].Groups.Count > 2)
                {
                    // Burn(xCell, rxCell, "Cell");
                    string pattern = "";

                    if (xCell[0].Groups.Count > 3)
                    {
                        pattern = xCell[0].Groups[3].Value.Trim();
                        ++_ruleCount;
                    }
                    CellPos.Add(new Marker(
                        xCell[0].Groups[1].Value,
                        xCell[0].Groups[2].Value,
                        pattern));
                }


                // match Range 
                if (!hasRange)
                {
                    MatchCollection xRange = rxRange.Matches(s);
                    if (xRange.Count > 0 && xRange[0].Groups.Count > 0)
                    {
                        // Burn(xRange, rxRange, "Range");
                        DataRange = xRange[0].Groups[1].Value;
                        hasRange = true;
                    }
                }

                // match Column 
                MatchCollection xCol = rxColumn.Matches(s);
                if (xCol.Count > 0 && xCol[0].Groups.Count > 2)
                {
                    string pattern = "";
                    // Burn(xCol, rxColumn, "Column");
                    if (xCol[0].Groups.Count > 3)
                    {
                        pattern = xCol[0].Groups[3].Value.Trim();
                        ++_ruleCount;
                    }
                    ColPos.Add(new Marker(xCol[0].Groups[1].Value,
                        xCol[0].Groups[2].Value,
                        pattern));
                }
            }

            if (CellPos.Count > 0)
            {
                _cells = new string[CellPos.Count];
                for (int i = 0; i < CellPos.Count; i++)
                {
                    _cells[i] = CellPos[i].name;
                }
            }

            if (ColPos.Count > 0)
            {
                GetBox();
                _columns = new string[ColPos.Count];
                for (int i = 0; i < ColPos.Count; i++)
                {
                    _columns[i] = ColPos[i].name;
                    int col = Value26(ColPos[i].pos);
                    if ((col < _left) || (col > _right))
                        _err.Add("Column " + ColPos[i].pos);
                }
                if (_err.Count > 0)
                    throw new Exception("Detail column outside Range ["  + DataRange + "] :" + string.Join(";", _err.ToArray()));
            }
        }

        private void GetBox()
        {
            Regex pRange = new Regex("([A-Z]+)([0-9]+):([A-Z]+)([0-9]+)");
            _left = 0;
            _top = 0;
            _right = 0;
            _bottom = 0;
            MatchCollection c = pRange.Matches(DataRange);
            if (c.Count > 0 && c[0].Groups.Count > 4)
            {
                _left = Value26(c[0].Groups[1].Value);
                _top = Convert.ToInt32(c[0].Groups[2].Value);
                _right = Value26(c[0].Groups[3].Value);
                _bottom = Convert.ToInt32(c[0].Groups[4].Value);
            }
        }

        /**
        read Excel De OleDb
        provided predefined  Metadata  
        adding logical structure to unstructured data in excel
        to output in Dictionary
        giving logical access towards excel data 

        Input:
          - Meta Data 
          - Excel File

        Output:
          Dictionary of <string, object>
          pointing to each cell
          and one special cell[DTL] 
              contains object [rowCount,colCount] array
              rowCount and colCount determined by OleDb

        Excel Upload Metadata example

        # any line stared with these :
            # hash        
            / slash 
            * asterisk
        # is comment 
 
        @tOLE
          [C2] NAME C(10)
          [C3] REG_DATE D*
          [C4] ACTIVE C(1)
          [C5] DESCRIPTION
          [A7:E15]
              [A] NO N 
              [B] PRODUCT C(5)*
              [C] QTY N
              [D] PRICE N(15)
              [E] TOTAL N 

          Available Metadata class :
          a. Sheet Name / Page Name (*Mandatory )
              @<sheet_name> : source to import

          b. Cell Location (optional)

          c. Data Range (optional: when no detail row needed) 

          d. Column position for data range (only when range specified) 

          square bracket marks location metadata
              divided into three class :
              1. Cell
                  format : [<column><row>] <CELL_NAME>
                  ex : [C2]
                  mark position to read from - for header marking
              2. Range
                  format : [<column_start><row_start>:<column_end><row_end>]
                  ex : [A7:E15]
                  mark detail range of allowed data input
              3. Column
                  format: [<column>] <COLUMN_NAME>
                  ex: [A]
                  mark column position for data input

          every Cell and Column Marker
          can contain constraint expression 
          for checking data length and type 
            <data_type>[(min-max)][*] 
            data_type: C | D | N 
            min-max = can be range or single number equivalent to 0-max
            * marks that field is mandatory
         
        Whitespace before each metadata class is not necessary
        as long as each data class is separated by newline
        or pipe (|) character

        Order is not important, provided  mandatory element is contained inside metadata 

        * 
        */
        public Dictionary<string, object> Read(string MetaExcel, string ExcelFilename)
        {
            _pid = Log.Log("MSTD00001INF", "Read " + Path.GetFileName(ExcelFilename), me, UserName, FN_CEXLUP, PID);
            
            ParseMeta(MetaExcel);
            string _StrConn = GetConnectionString(ExcelFilename);

            string[] _SheetName = GetExcelSheetNames();
            if (_SheetName.Length == 1)
                PageName = _SheetName[0];

            _d.Clear();

            try
            {
                init();
                foreach (Marker m in CellPos)
                {
                    string getCell = string.Format("SELECT * FROM [{0}${1}:{1}]",
                            PageName, m.pos);
                    oda = new OleDbDataAdapter(getCell, oco);
                    DataTable t = new DataTable();

                    try
                    {
                        oda.Fill(t);

                        if (t.Rows.Count > 0 && t.Columns.Count > 0)
                        {
                            DataRow row = t.Rows[0];
                            string k, v;
                            k = m.name;
                            v = Convert.ToString(row[0]);
                            if (!_d.ContainsKey(k))
                                _d.Add(k, v);
                            else
                                _d[k] = v;
                        }

                    }
                    catch (Exception ex)
                    {
                        _err.Add(ex.Message);
                        Log.Log("MSTD00002ERR", LoggingLogic.Trace(ex), "CommonExcelUpload.Read", UserName, FN_CEXLUP, PID);
                    }
                    finally
                    {
                        t.Dispose();
                        oda.Dispose();
                        oda = null;
                    }

                }


                if (DataRange.Length > 0)
                {
                    // Determine selected column(s) from difference 
                    // from column format and top left column in range 

                    int[] colNum = new int[ColPos.Count];
                    GetBox();

                    for (int i = 0; i < ColPos.Count; i++)
                    {
                        colNum[i] = Value26(ColPos[i].pos) - _left + 1;
                    }
                    DataTable d = new DataTable();
                    string selected = "";
                    for (int i = 0; i < ColPos.Count; i++)
                    {
                        if (i > 0) selected += ", ";
                        selected += string.Format("F{0} AS {1}", colNum[i].ToString(), ColPos[i].name);
                        Type t = typeof(string); 
                        switch(ColPos[i].rule.dt) 
                        {
                            case "N": t = typeof(decimal); break;
                            case "C": t = typeof(string); break;
                            case "D": t= typeof(string); break;
                            default: t = typeof(string); break;
                        }
                        d.Columns.Add(ColPos[i].name, t);

                        
                    }

                    // take only what you need 
                    string getRow = string.Format("SELECT {0} FROM [{1}${2}]"
                        , selected, PageName, DataRange);
                    oda = new OleDbDataAdapter(getRow, oco);

                    
                    
                    try
                    {
                        ReadParam = true;
                        oda.Fill(d);
                    }
                    catch (Exception ex)
                    {
                        _err.Add(ex.Message);
                        Log.Log("MSTD00002ERR", LoggingLogic.Trace(ex), "CommonExcelUpload.Read", UserName, FN_CEXLUP, PID);
                        ReadParam = false;
                    }
                    finally
                    {
                        oda.Dispose();
                    }

                    // remove empty rows
                    for (int i = d.Rows.Count - 1; i > 0; i--)
                    {
                        bool isEmpty = true;
                        for (int j = 0; j < d.Columns.Count; j++)
                        {
                            DataRow r = d.Rows[i];
                            if (!string.IsNullOrEmpty(r[j].ToString()))
                            {
                                isEmpty = false;
                                break;
                            }
                        }
                        if (isEmpty)
                        {
                            d.Rows.RemoveAt(i);
                        }
                    }

                    if (!_d.ContainsKey(DTL))
                        _d.Add(DTL, d);
                    else
                        _d[DTL] = d;
                }
            }
            catch (Exception ex)
            {
                _err.Add(ex.Message);
                Log.Log("MSTD00002ERR", LoggingLogic.Trace(ex), me, UserName, FN_CEXLUP, PID);
            }
            finally
            {
                del();
            }
            return _d;
        }

        private static bool ReadParam;
        public bool ReadDataError()
        {
            return ReadParam;
        }

        private void AddErr(Marker m, string msg, int row = -1)
        {
            string rowPos = (row > 0) ? row.ToString() : "";
            _err.Add("@ " + m.pos + rowPos + " " + m.name + " " + msg);
        }

        private void AddErr(Marker m, int row = -1)
        {
            string msg = m.rule.msg;
            string rowPos = (row > 0) ? row.ToString() : "";
            string id = "MSTD00"+ Convert.ToString(m.rule.msgId) + "ERR";
            string cell = m.pos + rowPos;
            switch (m.rule.msgId)
            {
                case Rule.ERR_EMPTY:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name});
                    break;
                case Rule.ERR_LEN_EXCEED:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name, m.rule.max });
                    break;
                case Rule.ERR_LEN_OUTSIDE:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name, m.rule.min, m.rule.max });
                    break;
                case Rule.ERR_LEN_WRONG:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name, m.rule.max });
                    break;
                case Rule.ERR_NOT_DATE:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name});
                    break;
                case Rule.ERR_NOT_NUM:
                    msg = lw(_pid, id, me, _uid, new object[] { cell, m.name });
                    break;
                default: 
                    break;
            }

            _err.Add(msg);
        }

        public int AddErrorReasonCol(DataTable x)
        {
            int errcol = x.Columns.IndexOf("ERRMSG");
            if (errcol < 0)
            {
                x.Columns.Add("ERRMSG", System.Type.GetType("System.String"));
                errcol = x.Columns.IndexOf("ERRMSG");
            }
            return errcol;
        }

        /**
         Given input _d
         
         Check size and type constrait specified in metadata  
         */
        public bool Check()
        {
            if (_ruleCount < 1) return true;
            _err.Clear();

            // check header 
            foreach (Marker mc in CellPos)
            {
                if (mc.rule != null)
                {
                    string v = "";

                    if (_d.ContainsKey(mc.name))
                        v = Convert.ToString(_d[mc.name]);
                    if (!mc.rule.Valid(ref v))
                    {
                        AddErr(mc);
                        if (mc.rule.msgId == Rule.ERR_LEN_EXCEED)
                        {
                            _d[mc.name] = v;
                        }
                    }
                    else
                    {
                        if (mc.rule.dt.Equals("C"))
                            _d[mc.name] = v;
                    }

                }
            }

            // check detail
            if (_d.ContainsKey(DTL))
            {
                DataTable x = _d[DTL] as DataTable;
                
                
                if (x != null)
                {
                    int errcol =  AddErrorReasonCol(x); 

                    // List<Rule> chk = new List<Rule>();
                    bool[] chk = new bool[ColPos.Count];
                   
                    for (int i = 0; i < ColPos.Count; i++)
                    {
                        chk[i] = ColPos[i].rule != null;
                    }

                    GetBox();
                    int cols = x.Columns.Count - 1;
                    for (int i = 0; i < cols; i++)
                    {
                        if (!chk[i]) continue;
                        for (int n = 0; n < x.Rows.Count; n++)
                        {
                            DataRow r = x.Rows[n];

                            string v = Convert.ToString(r[i]);
                            Rule c = ColPos[i].rule;
                            if (!ColPos[i].rule.Valid(ref v))
                            {
                                AddErr(ColPos[i], _top + n);
                                if (r[errcol] == null)
                                {
                                    r[errcol] = _err[_err.Count - 1];
                                }
                                else
                                {
                                    r[errcol] = r[errcol] + "\r\n" + _err[_err.Count - 1];
                                }
                            }
                            else
                            {
                                if (ColPos[i].rule.dt.Equals("C"))
                                    r[i] = v;
                            }
                            
                        }
                    }

                }
            }

            Log.Log("MSTD00001INF", "Check=" + ((_err.Count < 1) ? "Ok" : "NG"), me, FN_CEXLUP, FN_CEXLUP, PID);
            LogErrors();
            return _err.Count < 1;
        }

        private void LogErrors()
        {
            if (!ErrorLogging || _err.Count < 1) return;

            foreach (string err in _err)
            {
                Log.Log("MSTD00002ERR", err, me, UserName, FN_CEXLUP, PID);
            }
        }



        #endregion

        #region pre post 
        private void init()
        {
            if (oco.State == ConnectionState.Closed)
            {
                oco.Open();
            }
        }

        private void del(IDisposable x)
        {
            if (x != null)
            {
                x.Dispose();
                x = null;
            }
        }

        private void del()
        {
            del(ocmd);
            del(oda);
            if (oco != null && oco.State == ConnectionState.Open)
            {
                oco.Close();
                del(oco);
            }
        }
        #endregion

        #region Excel Data
        public DataTable GetTable(string strTableName, string _strSheetName, string _strSheetRange)
        {
            try
            {
                //Open and query
                
                init();
                if (!SetSheetQuerySelect(_strSheetName, _strSheetRange)) 
                    return null;

                //Fill table
                _dt = new DataTable();
                oda = new OleDbDataAdapter(ocmd);
                
                DataTable dt = new DataTable(strTableName);
                oda.FillSchema(dt, SchemaType.Source);
                oda.Fill(dt);

                if (_strSheetRange.IndexOf(":") > 0)
                {
                    string FirstCol = _strSheetRange.Substring(0, _strSheetRange.IndexOf(":") - 1);
                    int intCol = ColNumber(FirstCol);
                    for (int intI = 0; intI < dt.Columns.Count; intI++)
                    {
                        dt.Columns[intI].Caption = ColName(intCol + intI);
                    }
                }
                
                dt.DefaultView.AllowDelete = false;

                
                return dt;

            }
            finally
            {
                del();
            }
        }

        public int GetRowsCount(string SheetName)
        {
            int _rowsCount = 0;
            try
            {
                init();
                string _query = String.Format("SELECT * FROM [{0}$]", SheetName);
                ocmd = new OleDbCommand(_query, oco);
                oda = new OleDbDataAdapter(ocmd);
                _dt = new DataTable();
                oda.Fill(_dt);
                

                //result
                _rowsCount = _dt.Rows.Count + 1;
            }
            finally
            {
                del();
            }
            return _rowsCount;
        }

        public DataTable GetTable2(string strTableName, string _strSheetName, string _strSheetRange)
        {
            try
            {
                //Open and query
                if (oco.State == ConnectionState.Closed)
                {
                    oco.Open();
                }
                if (SetSheetQuerySelect(_strSheetName, _strSheetRange) == false) return null;

                //Fill table
                _dt = new DataTable();
                oda = new OleDbDataAdapter();
                oda.SelectCommand = ocmd;
                DataTable dt = new DataTable(strTableName);
                oda.FillSchema(dt, SchemaType.Source);
                oda.Fill(dt);

                if (_strSheetRange.IndexOf(":") > 0)
                {
                    string FirstCol = _strSheetRange.Substring(0, _strSheetRange.IndexOf(":") - (_strSheetRange.IndexOf(":") - 1));
                    int intCol = ColNumber(FirstCol);
                    for (int intI = 0; intI < dt.Columns.Count; intI++)
                    {
                        dt.Columns[intI].Caption = ColName(intCol + intI);
                    }
                }

                dt.DefaultView.AllowDelete = false;
                
                return dt;

            }
            finally
            {
                del();
            }
        }

        private bool SetSheetQuerySelect(string _strSheetName, string _strSheetRange)
        {
            if (oco == null || oco.State == ConnectionState.Closed)
                throw new Exception("Connection is unassigned or closed.");

            if (_strSheetName.Length == 0)
                throw new Exception("Sheetname was not assigned.");

            ocmd = new OleDbCommand(
                @"SELECT * FROM ["
                + _strSheetName
                + "$" + _strSheetRange
                + "]", oco);

            return true;            
        }

        public String[] GetExcelSheetNames()
        {
            System.Data.DataTable dt = null;
            try
            {
                init();
                // Get the data table containing the schema
                dt = oco.GetOleDbSchemaTable(OleDbSchemaGuid.Tables,
                    new object[] { null, null, null, "TABLE" });

                if (dt == null)
                {
                    return null;
                }

                String[] excelSheets = new String[dt.Rows.Count];
                int i = 0;

                // Add the sheet name to the string array.
                foreach (DataRow row in dt.Rows)
                {
                    string strSheetTableName = row["TABLE_NAME"].ToString();
                    excelSheets[i] = strSheetTableName.Substring(0, strSheetTableName.Length - 1);
                    i++;
                }


                return excelSheets;
            }
            finally
            {
                del(dt);                
            }
        }
        #endregion

        #region is excel format
        public bool IsExcelFormat(string f)
        {
            string fx = f.Trim();
            return (fx.Trim() == ".xls" || fx.Trim() == ".xlsx");                
        }

        public static bool IsExcelFile(string f)
        {
            if (f == null) return false;
            string ext = Path.GetExtension(f).ToString().ToLower();
            return (ext == ".xls" || ext == ".xlsx");
        }

        public int ColNumber(string strCol)
        {
            strCol = strCol.ToUpper();
            int intColNumber = 0;
            if (strCol.Length > 1)
            {
                intColNumber = Convert.ToInt16(Convert.ToByte(strCol[1]) - 65);
                intColNumber += Convert.ToInt16(Convert.ToByte(strCol[1]) - 64) * 26;
            }
            else
                intColNumber = Convert.ToInt16(Convert.ToByte(strCol[0]) - 65);
            return intColNumber;
        }

        public string ColName(int intCol)
        {
            string sColName = "";
            if (intCol < 26)
                sColName = Convert.ToString(Convert.ToChar((Convert.ToByte((char)'A') + intCol)));
            else
            {
                int intFirst = ((int)intCol / 26);
                int intSecond = ((int)intCol % 26);
                sColName = Convert.ToString(Convert.ToByte((char)'A') + intFirst);
                sColName += Convert.ToString(Convert.ToByte((char)'A') + intSecond);
            }
            return sColName;
        }

        #endregion

        public void say(string word, params object[] o)
        {
            _pid = Log.Log("MSTD00001INF", (o == null || o.Length <1) ? word: string.Format(word, o), me, _uid, FN_CEXLUP, _pid);
        }


    }

    public class Marker
    {
        public string pos;
        public string name;
        public Rule rule;

        public Marker()
        {
            rule = null;
            name = "";
            pos = "";
        }

        public Marker(string Pos, string Name, string pattern = "")
        {
            pos = Pos;
            name = Name;
            if (!string.IsNullOrEmpty(pattern))
            {
                rule = new Rule(pattern.Trim());
            }
        }
    }

    public class Rule
    {
        public string dt;
        public int min, max;
        public bool mandatory;        
        public string msg;
        public int msgId;
        public object par;
        public const int ERR_NONE = 0;
        public const int ERR_EMPTY = 201;
        public const int ERR_NOT_DATE = 202;
        public const int ERR_NOT_NUM = 203;
        public const int ERR_LEN_EXCEED = 204;
        public const int ERR_LEN_OUTSIDE = 205;
        public const int ERR_LEN_WRONG = 206;

        public Rule(string rule)
        {
            Regex pattern = new Regex("([A-Z])(\\(([0-9\\-]+|[0-9]+\\-[0-9]+)\\))*([*]*)");
            dt = "";
            min = 0;
            max = 0;
            msgId = ERR_NONE;
            mandatory = false;

            MatchCollection mx = pattern.Matches(rule);

            if (mx.Count > 0 && mx[0].Groups.Count > 1)
            {
                CommonExcelUpload.ShowMatches(mx, pattern, "Rule");
                dt = mx[0].Groups[1].Value;                
                mandatory = String.Compare(mx[0].Groups[4].Value, "*") == 0;
                LoggingLogic.say("matches", mx[0].Groups[4].Value);
                string len = (mx[0].Groups.Count > 3) ? mx[0].Groups[3].Value : "";
                if (len.Length > 0)
                {
                    if (len.Contains("-"))
                    {
                        string[] lenRange = len.Split('-');

                        if (!Int32.TryParse(lenRange[0], out min)) min = 0;
                        if (!Int32.TryParse(lenRange[1], out max)) max = -1;

                    }
                    else
                    {
                        if (!Int32.TryParse(len, out max)) max = -1;
                    }
                }
                else
                    max = -1;
            }
        }

        private bool checkLen(ref string v, bool truncate)
        {
            if (max > 0)
            {
                if (min > 0 && max > 0)
                {

                    int n = v.Length;
                    if (n < min || n > max)
                    {
                        if (min < max)
                        {
                            msg = " length must be between " + min
                                 + " to " + max + " chars";
                            msgId = ERR_LEN_OUTSIDE;
                        }
                        else
                        {
                            msg = " length must be " + max + " chars";
                            msgId = ERR_LEN_WRONG;
                        }
                    }
                }
                else
                {
                    if (v.Length > max && truncate)
                    {
                        msg = " truncated. Max " + max + " chars";
                        msgId = ERR_LEN_EXCEED;
                        v = v.Substring(0, max);
                    }
                }
            }
            return (msg.Length < 1);
        }


        public static bool TryStrToDateFmt(object a, string fmt, ref DateTime r)
        {
            return DateTime.TryParseExact(Convert.ToString(a), fmt, CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out r);
        }


        public bool Valid(ref string v)
        {
            bool isEmpty = string.IsNullOrEmpty(v);

            if (isEmpty && mandatory)
            {
                msg = " must be filled";
                msgId = ERR_EMPTY;                
                return false;
            }
            msg = "";
            msgId = 0;
            if (!mandatory && isEmpty) { return true; }

            DateTime datum = DateTime.Now;
            Decimal dec;

            switch (dt)
            {
                case "D":
                    if (!TryStrToDateFmt(v, AppSetting.UPLOAD_DATE_FORMAT, ref datum))
                    {
                        msg = " is not in Date Format";
                        msgId = ERR_NOT_DATE;
                    }
                    break;
                case "C":
                    if (!string.IsNullOrEmpty(v)) v = v.Trim();
                    checkLen(ref v, true);
                    break;
                case "N":
                    checkLen(ref v, false);
                    if (!Decimal.TryParse(v, out dec))
                    {
                        msg = "not in Number Format";
                        msgId = ERR_NOT_NUM;
                    }
                    break;
                default:
                    msg = "";
                    msgId = ERR_NONE;
                    break;
            }
            return (msg.Length < 1);
        }
    }

}
