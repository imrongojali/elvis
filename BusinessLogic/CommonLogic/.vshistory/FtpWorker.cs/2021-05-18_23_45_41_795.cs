﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Diagnostics;
using Common;
using Bytescout.BarCodeReader;

namespace BusinessLogic.CommonLogic
{
    public class FtpWorker : IDisposable
    {
        readonly string UserID = AppSetting.FTP_USERID;
        readonly string Password = AppSetting.FTP_PASS;

        private FtpWebRequest f = null;
        private List<string> reply = new List<string>();
        private List<string> errs = new List<string>();

        // fid.Hadid 20190325. Could set auth from outside config
        public string __UserID { get; set; }
        public string __Password { get; set; }

        public List<string> Responses
        {
            get
            {
                return reply;
            }
        }

        public List<string> Errors
        {
            get { return errs; }
        }

        private void err(Exception ex)
        {
            errs.Add(ex.Message);
        }

        private void err(string s, params object[] x)
        {
            if (x!=null) 
                errs.Add(string.Format(s, x));
            else
                errs.Add(s);
        }


        public void Dispose()
        {

        }

        private void Connect(string uri, bool KeepAlive = true)
        {
            f = (FtpWebRequest)FtpWebRequest.Create(uri);

            if (!String.IsNullOrEmpty(__UserID) && !String.IsNullOrEmpty(__Password))
                f.Credentials = new NetworkCredential(__UserID, __Password);
            else
                f.Credentials = new NetworkCredential(UserID, Password);

            f.KeepAlive = KeepAlive;
            f.UsePassive = false;
            f.UseBinary = true;
            f.Proxy = new WebProxy();
        }

        private bool Fetch()
        {
            bool r = false;
            string line;
            reply.Clear();
            try
            {
                using (WebResponse response = f.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            reply.Add(line);
                        }
                    }
                }
                r = true;
            }
            catch (WebException ex)
            {
                WebExceptionStatus[] l = new WebExceptionStatus[] {
                    WebExceptionStatus.ConnectFailure, 
                    WebExceptionStatus.ConnectionClosed,
                    WebExceptionStatus.Timeout,
                    WebExceptionStatus.TrustFailure,
                    WebExceptionStatus.ProxyNameResolutionFailure
                };
                if (!l.Contains(ex.Status))
                    r = true;
                err(ex);
            }
            catch (Exception ex)
            {
                err(ex);
            }
            return r;
        }

        private bool? Has(string uri, string filename = "")
        {
            bool? r = null;
            string go = uri;
            if (!uri.Substring(uri.Length - 1, 1).Equals("/")) go = uri + "/";
            try
            {
                Connect(go);
                f.Method = WebRequestMethods.Ftp.ListDirectory;

                if (Fetch())
                {
                    if (String.IsNullOrEmpty(filename) && reply.Count > 0) r = true;
                    else
                    {
                        r = false;
                        foreach (string s in reply)
                        {
                            if (String.Compare(s, filename, true) == 0)
                            {
                                r = true;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                err(ex);
            }
            go = go + filename;
            
            return r;
        }

        private bool MD(string uri)
        {
            bool r = false;
            try
            {
                
                Connect(uri);
                f.Method = WebRequestMethods.Ftp.MakeDirectory;

                r = Fetch();
            }
            catch (Exception ex)
            {
                err(ex);
            }
            return r;
        }

        public bool MakeDirs(string uri)
        {
            bool r = false;
            int j = uri.Length;
            

            if (Has(uri) ?? false)
                return true;

            int firstSlash = uri.IndexOf("/", 7);
            int nextSlash = uri.IndexOf("/", firstSlash + 1);
            int lastSlash = uri.LastIndexOf("/", j - 1);
            bool justMadeIt = false;
            while ((nextSlash < j) && (nextSlash > 0))
            {
                string url = uri.Substring(0, firstSlash);
                string fn = uri.Substring(firstSlash + 1, nextSlash - firstSlash - 1);
                if (justMadeIt || (!Has(url, fn) ?? false))
                {
                    r = MD(uri.Substring(0, nextSlash));
                    justMadeIt = true;
                }
                firstSlash = nextSlash;
                nextSlash = uri.IndexOf("/", nextSlash + 1);
            }
            if (justMadeIt || (!Has(uri) ?? false))
                MD(uri);
            
            return r;
        }

        public bool Upload(byte[] fileBytes, string filename, string uploadPath, ref bool result, ref string errMessage)
        {
            result = false;

            string uri = uploadPath;
            int lastDelim = filename.LastIndexOf(Path.DirectorySeparatorChar);
            string filepath = (lastDelim > 0) ? filename.Substring(0, lastDelim) : "";
            string justname = (lastDelim > 0) ? filename.Substring(lastDelim + 1, filename.Length - lastDelim - 1) : filename;
            bool? x = Has(uri, justname);
            result = !(x ?? false);
            if (x == null)
            {
                err("Connect failed");
                result = false;
            }
            else
            {
                if (result)
                {
                    MakeDirs(uri);

                    string ftpfullpath = uri + "/" + justname;

                    Connect(ftpfullpath);
                    f.Method = WebRequestMethods.Ftp.UploadFile;

                    Stream ftpstream = f.GetRequestStream();
                    ftpstream.Write(fileBytes, 0, fileBytes.Length);
                    ftpstream.Close();

                    result = Fetch();
                }
                else
                {
                    err("File {0} already exists", filename);
                }

            }

            return result;
        }


        public string GetValueQRCode( string dir, string file)
        {
          //  QRPDFFiles = Path.GetFullPath(SourcePath + fileName);

            string ftpfullpath = Path.GetFullPath(dir + "/" + file);

            Connect(ftpfullpath);


            Reader barcodeReader = new Reader("1_DEVELOPER_LICENSE_FOR_DESKTOP_DEVELOPER_LICENSE_2D_BARCODES_ONLY_DESKTOP_APPS_ONLY_MAHENDRA.SONDAY@EMERIOCORP.COM_SUPPORT_AND_UPDATES_UNTIL_APRIL_25_2020", "E702-4936-D399-2BC9-8239-D07D-27B");


            // barcodeReader.PDFRenderingResolution = 200;
            barcodeReader.ImagePreprocessingFilters.AddScale(0.75d);


            barcodeReader.BarcodeTypesToFind.QRCode = true;

            barcodeReader.FastMode = true;
            // FtpWebRequest reqFTP = null;


          
            var _indexPages = barcodeReader.GetPdfPageCount(ftpfullpath);


            return ftpfullpath;
        }



        public bool Upload(string filename, string uploadPath, ref bool result, ref string errMessage)
        {
            FileStream fs = File.OpenRead(filename);
            byte[] buffer = new byte[fs.Length];
            fs.Read(buffer, 0, buffer.Length);
            fs.Close();
            return Upload(buffer, filename, uploadPath, ref result, ref errMessage);
        }
    }
}
