﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.SessionState;
using System.Data.Common;
using System.Data.EntityClient;
using Common.Data;
using Common.Function;
using DataLayer.Model;
using Dapper;

namespace BusinessLogic
{
    public class UserLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        protected string[] AuthorizedRoles = new string[] { };

        public delegate bool isAuth(UserData u);

        public UserData LogIn(string UserName, out string SESSION_ID, string IPNum = "")
        {
            UserData _userData = null;

            List<UserData> QuData = Qu<UserData>("GetViewUserData", UserName).ToList();

            SESSION_ID = "";
            if (QuData.Any())
            {
                List<RoleAcessData> _data = new List<RoleAcessData>();

                _data = Qu<RoleAcessData>("GetRoleAccessRight", UserName.ToUpper()).ToList();

                bool isSecretary = false;
                foreach (var item in _data)
                {
                    if (item.ROLE_ID.Contains("SECRETARY")) isSecretary = true;
                }

                string[] divs = (from d in QuData select d.DIV_CD.ToString()).ToArray();
                int[] divisions = divs.Select(a => a.Int(0)).ToArray();

                int userDivision;
                if (isSecretary)
                    userDivision = divisions.Max();
                else
                    userDivision = divisions.FirstOrDefault();


                string uDivID = userDivision.str();

                var x = (from a in QuData
                         where (userDivision != 0 && a.DIV_CD.ToString() == uDivID)
                         || (userDivision == 0 && a.USERNAME.Equals(UserName))
                         select a).FirstOrDefault();
                if (x != null)
                {
                    _userData = new UserData()
                    {
                        REG_NO = x.REG_NO,
                        USERNAME = x.USERNAME,
                        TITLE = x.TITLE,
                        FIRST_NAME = x.FIRST_NAME,
                        LAST_NAME = x.LAST_NAME,
                        EMAIL = x.EMAIL,
                        DIV_CD = x.DIV_CD,
                        DIV_NAME = x.DIV_NAME,
                        DEPARTMENT_ID = x.DEPARTMENT_ID,
                        DEPARTMENT_NAME = x.DEPARTMENT_NAME,
                        GROUP_CD = x.GROUP_CD
                    };
                    _userData._RoleAcessData = _data;
                }
                else
                    _userData = null;

                List<UserPositionData> P = Qu<UserPositionData>("GetUserPositionData", UserName).ToList();

                if (_userData != null && P != null)
                {
                    foreach (UserPositionData p in P.Distinct())
                    {
                        _userData.Positions.Add(p);
                    }
                    _userData.isAuthorizedCreator = isAuthorizedCreatorRole(_userData);
                    SESSION_ID = "";
                    //System.Data.Objects.ObjectParameter Param = new System.Data.Objects.ObjectParameter("SESSION_ID", " ");

                    //db.NewLogin(Common.AppSetting.ApplicationID, _userData.USERNAME, Param);

                    DynamicParameters d = new DynamicParameters();
                    d.Add("@ApplicationId", Common.AppSetting.ApplicationID);
                    d.Add("@Username", _userData.USERNAME);
                    d.Add("@SESSION_ID", SESSION_ID, DbType.String, ParameterDirection.Output);

                    Db.Execute("sp_NewLogin", d, null, null, CommandType.StoredProcedure);
                    SESSION_ID = d.Get<string>("@SESSION_ID");

                    // SESSION_ID = Convert.ToString(Param.Value);
                }
            }


            return _userData;
        }


        public UserData SessionUserData(HttpContext c)
        {
            if (c == null || c.Session == null) return null;
            UserData u = c.Session["userData"] as UserData;
            string SESSION_ID = Convert.ToString(c.Session["SESSION_ID"]);

            if (u == null || !isValidSessionLogin(u.USERNAME, SESSION_ID, c.Request == null ? "" : c.Request.UserHostAddress))
                u = null;

            if (u == null)
            {
                c.Session.RemoveAll();
            }
            return u;
        }

        public bool isValidSessionLogin(string Username, string SESSION_ID, string IPNum = "")
        {
            bool isValidLogin = false;
            if (logic.ValidSession == null)
            {
                DynamicParameters d = new DynamicParameters();
                d.Add("@ApplicationId", Common.AppSetting.ApplicationID);
                d.Add("@Username", Username);
                d.Add("@SESSION_ID", SESSION_ID);
                d.Add("@retVal", null, DbType.Boolean, ParameterDirection.Output);

                Db.Execute("sp_isValidSessionLogin", d, null, null, CommandType.StoredProcedure);
                isValidLogin = d.Get<bool>("@retVal");

                logic.ValidSession = isValidLogin;
            }
            else
            {
                isValidLogin = logic.ValidSession ?? false;
            }

            if (!isValidLogin)
            {
                logic.Log.Log("MSTD00002ERR",
                    "Invalid SessionID=" + SESSION_ID, "isValidSessionLogin", Username, "", 0, IPNum);
            }

            return isValidLogin;
        }

        public string GetUserLoginBySessionID(string SESSION_ID)
        {
            string retVal = "";
            System.Data.Objects.ObjectParameter UserName = new System.Data.Objects.ObjectParameter("Username", " ");

            db.getUsrLoginBySessionId(Common.AppSetting.ApplicationID, SESSION_ID, UserName);
            retVal = Convert.ToString(UserName.Value);
            return retVal;
        }

        private bool? _authorizedFinance = null;
        public bool IsAuthorizedFinance
        {
            get
            {
                if (_authorizedFinance == null)
                {
                    _authorizedFinance = isAuthorizedFinance(logic.GetUserData());
                }
                return (_authorizedFinance ?? false);
            }
        }

        public bool isAuthorizedFinance(UserData u)
        {
            if (u == null || u._RoleAcessData == null) return false;
            bool roleAllow = false;
            string[] allowed_role = logic.Sys.GetArray("ROLES", "AUTHORIZED_FINANCE");
            string[] roles = (from c in u._RoleAcessData select c.ROLE_ID).ToArray();
            roleAllow = roles.Intersect(allowed_role).Count() > 0;
            return roleAllow;
        }

        private bool? _authorizedCreator = null;
        public bool IsAuthorizedCreatorRole
        {
            get
            {
                if (_authorizedCreator == null)
                {
                    _authorizedCreator = isAuthorizedCreatorRole(logic.GetUserData());
                }
                return (_authorizedCreator ?? false);
            }
        }

        public bool isAuthorizedCreatorRole(UserData u)
        {
            if (u == null || u._RoleAcessData == null)
                return false;
            bool isAllowed = false;
            if (AuthorizedRoles.Length < 1) AuthorizedRoles = logic.Sys.GetArray("PVForm", "AuthorizedCreatorRole");

            string[] roles = (from c in u._RoleAcessData select c.ROLE_ID).ToArray();
            isAllowed = roles.Intersect(AuthorizedRoles).Count() > 0;
            return isAllowed;
        }

        private bool? _directorUp = null;
        public bool IsDirectorUp
        {
            get
            {
                if (_directorUp == null)
                {
                    _directorUp = isDirectorUp(logic.GetUserData());
                }
                return (_directorUp ?? false);
            }
        }

        public bool isDirectorUp(UserData u)
        {
            if (u == null || u.Positions == null || u.Positions.Count < 1) return false;
            int? pos = u.Positions.Where(p => p.CLASS >= 8).Select(x => x.CLASS).FirstOrDefault();
            return (pos ?? 0) >= 8;
        }

    }
}
