﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data.Common.Data;

namespace BusinessLogic
{
    public class ErrorStatusLogic
    {
        public List<ErrorStatusData> GetErrorStatusList()
        {
            List<ErrorStatusData> retVal = new List<ErrorStatusData>();
            ErrorStatusData ErrBlank = new ErrorStatusData();
            ErrBlank.Status_CD = -1;
            ErrBlank.Status_Name = "";
            retVal.Add(ErrBlank);

            ErrorStatusData Err0 = new ErrorStatusData();
            Err0.Status_CD = 0;
            Err0.Status_Name = Convert.ToString(Err0.Status_CD) + ":Success";
            retVal.Add(Err0);
            ErrorStatusData Err1 = new ErrorStatusData();
            Err1.Status_CD = 1;
            Err1.Status_Name = Convert.ToString(Err1.Status_CD) + ":Error (Business)";
            retVal.Add(Err1);
            ErrorStatusData Err2 = new ErrorStatusData();
            Err2.Status_CD = 2;
            Err2.Status_Name = Convert.ToString(Err2.Status_CD) + ":Error (System)";
            retVal.Add(Err2);
            return retVal;
        }
    }
}
