﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._40RVFormList;
using Common.Function;
using DataLayer.Model;
using Dapper;

namespace BusinessLogic._40RVFormList
{
    public class RVApprovalLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();
        #region Search Inquiry
        public List<RVApprovalData> SearchDetail(string pvNoParam, string pvYearParam)
        {
            List<RVApprovalData> result = new List<RVApprovalData>();

            //default ignore all searching criteria

            int pvNo = pvNoParam.Int();
            int pvYear = pvYearParam.Int();

            var qr = Qu<RVApprovalData>("RvApprovalDetail", pvNo, pvYear).ToList();
            if (qr != null && qr.Count > 0)
            {
                foreach (var d in qr)
                {
                    RVApprovalData p = d.Clone();
                    p.AMOUNT = CommonFunction.Eval_Curr(p.CURRENCY_CD, p.AMOUNT_VALUE);
                    result.Add(p);
                }
            }

            return result;
        }



        public RVApprovalData Search(string _No, string _Year)
        {
            RVApprovalData r = null;


            List<RVApprovalData> l = Qu<RVApprovalData>("RvApprovalHeader", _No, _Year).ToList();
            if ((l != null) && (l.Count > 0))
                r = l[0];

            return r;

        }

        public List<AttachmentData> SearchAttachmentInqury(String rvNoParam, String rvYearParam)
        {
            List<AttachmentData> returnData = new List<AttachmentData>();

            returnData = Qx<AttachmentData>("GetAttachmentInquiry", new { refNo = String.Concat(rvNoParam, rvYearParam) }).ToList();
            if (returnData != null)
            {
                foreach (AttachmentData r in returnData)
                {
                    r.URL = CommonFunction.CombineUrl(AppSetting.FTP_DOWNLOADHTTP, r.DIRECTORY, r.FILE_NAME);
                }
            }
            return returnData;
        }



        #endregion

        #region hold button
        public bool ChangeHoldValue(String rvNoParam,
                                    String rvYearParam,
                                    String userNameParam,
                                    ref ErrorData err)
        {

            Int32 rvNo;
            Int16 rvYear;
            //Int32 holdFlag;
            bool flag = true;

            #region build searching criteria
            if (!string.IsNullOrEmpty(rvNoParam))
                rvNo = Int32.Parse(rvNoParam);
            else
                rvNo = 0;

            if (!string.IsNullOrEmpty(rvYearParam))
                rvYear = Int16.Parse(rvYearParam);
            else
                rvYear = 0;

            #endregion
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var data = (from i in db.TB_R_RV_H
                            where i.RV_NO == rvNo &&
                                  i.RV_YEAR == rvYear
                            select i).FirstOrDefault();

                Int32 holdFlag = data.HOLD_FLAG ?? 0;

                if (holdFlag == 0)
                {
                    data.HOLD_FLAG = 1;
                    data.HOLD_BY = userNameParam;
                    data.CHANGED_DATE = DateTime.Now;
                    data.CHANGED_BY = userNameParam;
                    flag = false;
                }
                else if (holdFlag == 1)
                {
                    data.HOLD_FLAG = 0;
                    data.HOLD_BY = userNameParam;
                    data.CHANGED_DATE = DateTime.Now;
                    data.CHANGED_BY = userNameParam;
                    //flag = false;
                }

                db.SaveChanges();
            }
            return flag;
        }
        #endregion

        public void SetReceivedDate(int docno, int docYear)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var q = co.db.TB_R_RV_H.Where(a => a.RV_NO == docno && a.RV_YEAR == docYear).FirstOrDefault();
                if (q != null)
                {
                    q.RECEIVED_DATE = DateTime.Now;
                    co.db.SaveChanges();
                }
            }
        }

        public void InsertTableHistory(string pvNoParam, string pvYearParam)
        {
            Int32 pvNo;
            Int16 pvYear;
            String[] retVal = new String[] { "", "" };

            #region build searching criteria
            if (!string.IsNullOrEmpty(pvNoParam))
                pvNo = Int32.Parse(pvNoParam);
            else
                pvNo = 0;

            if (!string.IsNullOrEmpty(pvYearParam))
                pvYear = Int16.Parse(pvYearParam);
            else
                pvYear = 0;
            #endregion

            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var header = (from i in db.TB_R_PV_H
                              where i.PV_NO == pvNo &&
                                    i.PV_YEAR == pvYear
                              select i);

                var detail = (from i in db.TB_R_PV_D
                              where i.PV_NO == pvNo &&
                                    i.PV_YEAR == pvYear
                              select i);

                int maxVer = 0;
                var history = db.TB_H_PV_H.Where(i => i.PV_NO == pvNo && i.PV_YEAR == pvYear);

                if (history.Any())
                {
                    maxVer = history.Max(i => i.VERSION);
                    maxVer++;
                }
                else
                {
                    maxVer++;
                }

                foreach (var data in header)
                {
                    TB_H_PV_H _header = new TB_H_PV_H();
                    _header.ACTIVITY_DATE_FROM = data.ACTIVITY_DATE_FROM;
                    _header.ACTIVITY_DATE_TO = data.ACTIVITY_DATE_TO;
                    _header.BANK_TYPE = data.BANK_TYPE;
                    _header.BUDGET_NO = data.BUDGET_NO;
                    _header.CHANGED_BY = data.CHANGED_BY;
                    _header.CHANGED_DATE = data.CHANGED_DATE;
                    _header.CREATED_BY = data.CREATED_BY;
                    _header.CREATED_DATE = data.CREATED_DATE;
                    _header.DELETED = data.DELETED;
                    _header.DIVISION_ID = data.DIVISION_ID;
                    _header.HOLD_BY = data.HOLD_BY;
                    _header.HOLD_FLAG = data.HOLD_FLAG;
                    //_header.LOCATION_CD = data.LOCATION_CD;
                    _header.PAID_DATE = data.PAID_DATE;
                    _header.PAY_METHOD_CD = data.PAY_METHOD_CD;
                    _header.PLANNING_PAYMENT_DATE = data.PLANNING_PAYMENT_DATE;
                    _header.POSTING_DATE = data.POSTING_DATE;
                    _header.PV_DATE = data.PV_DATE;
                    _header.PV_NO = data.PV_NO;
                    _header.PV_TYPE_CD = data.PV_TYPE_CD;
                    _header.PV_YEAR = data.PV_YEAR;
                    _header.REFFERENCE_NO = data.REFFERENCE_NO;
                    _header.SET_NO_PV = data.SET_NO_PV;
                    _header.SET_NO_RV = data.SET_NO_RV;
                    _header.SETTLEMENT_STATUS = data.SETTLEMENT_STATUS;
                    _header.STATUS_CD = data.STATUS_CD;
                    _header.SUBMIT_DATE = data.SUBMIT_DATE;
                    _header.SUBMIT_HC_DOC_DATE = data.SUBMIT_HC_DOC_DATE;
                    _header.SUSPENSE_NO = data.SUSPENSE_NO;
                    _header.TRANSACTION_CD = data.TRANSACTION_CD;
                    _header.TOTAL_AMOUNT = data.TOTAL_AMOUNT;
                    _header.VENDOR_CD = data.VENDOR_CD;
                    _header.VENDOR_GROUP_CD = data.VENDOR_GROUP_CD;
                    _header.VERSION = maxVer;
                    _header.WORKFLOW_STATUS = data.WORKFLOW_STATUS;

                    db.AddToTB_H_PV_H(_header);
                }

                foreach (var data in detail)
                {
                    TB_H_PV_D _detail = new TB_H_PV_D();
                    _detail.AMOUNT = data.AMOUNT;
                    _detail.CHANGED_BY = data.CHANGED_BY;
                    _detail.CHANGED_DT = data.CHANGED_DT;
                    _detail.COST_CENTER_CD = data.COST_CENTER_CD;
                    _detail.CREATED_BY = data.CREATED_BY;
                    _detail.CREATED_DT = data.CREATED_DT;
                    _detail.CURRENCY_CD = data.CURRENCY_CD;
                    _detail.DESCRIPTION = data.DESCRIPTION;
                    _detail.INVOICE_NO = data.INVOICE_NO;
                    _detail.ITEM_TRANSACTION_CD = data.ITEM_TRANSACTION_CD;
                    _detail.PV_NO = data.PV_NO;
                    _detail.PV_YEAR = data.PV_YEAR;
                    _detail.SEQ_NO = data.SEQ_NO;
                    _detail.TAX_CD = data.TAX_CD;
                    _detail.TAX_NO = data.TAX_NO;
                    _detail.VERSION = maxVer;
                    // _detail.WBS_NO = data.WBS_NO;

                    db.AddToTB_H_PV_D(_detail);
                }

                db.SaveChanges();
            }
        }

        public string[] GetNewStatus(string docNo, string docYear)
        {
            String[] retVal = new string[] { "", "", "" };
            int no, yy;
            no = docNo.Int(0);
            yy = docYear.Int(0);
            string sql = string.Format(logic.SQL["RvStatusData"], no, yy);


            var q = Qu<NewStatusData>("RvStatusData", no, yy).ToList();
            if (q != null && q.Count > 0)
                retVal = new string[] { q[0].STATUS_CD.str(), q[0].STATUS_NAME, q[0].PAY_METHOD_CD };

            return retVal;
        }

        public void UpdateSettlement(string rvNoParam, string rvYearParam)
        {
            Int32 rvNo;
            Int16 rvYear;

            #region build searching criteria
            if (!string.IsNullOrEmpty(rvNoParam))
                rvNo = Int32.Parse(rvNoParam);
            else
                rvNo = 0;

            if (!string.IsNullOrEmpty(rvYearParam))
                rvYear = Int16.Parse(rvYearParam);
            else
                rvYear = 0;
            #endregion
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from i in db.TB_R_RV_H
                         where i.RV_NO == rvNo &&
                               i.RV_YEAR == rvYear
                         select i).FirstOrDefault();

                if (q != null)
                {
                    if (q.RV_TYPE_CD == 2)
                    {
                        var r = (from i in db.vw_SettlementStatusRV
                                 where i.RV_NO == rvNo &&
                                       i.RV_YEAR == rvYear
                                 select i).FirstOrDefault();

                        if (r != null)
                        {
                            if (r.SUSPENSE_STATUS > 0)
                            {
                                var s = (from j in db.TB_R_PV_H
                                         where j.SUSPENSE_NO == (from i in db.TB_R_PV_H
                                                                 where i.PV_NO == r.SET_NO_PV &&
                                                                       i.PV_YEAR == rvYear
                                                                 select i.SUSPENSE_NO).FirstOrDefault()
                                         select j).FirstOrDefault();

                                s.SETTLEMENT_STATUS = 1;
                                db.SaveChanges();
                            }



                        }
                    }
                }
            }
        }

        public bool isCanDirect(string rvNoParam, string rvYearParam, string userName)
        {
            bool retVal = false;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                try
                {
                    decimal reffNo = Decimal.Parse(String.Concat(rvNoParam, rvYearParam));
                    var q = db.TB_R_DISTRIBUTION_STATUS
                        .Where(a => a.REFF_NO == reffNo && a.ACTUAL_DT == null)
                        .OrderBy(a => a.PLAN_DT)
                        .ToList();
                    if (q.Any())
                    {
                        var s = q.Where(a => a.PROCCED_BY == userName).FirstOrDefault();
                        if (s != null)
                        {
                            if (s.STATUS_CD > 59 && q.FirstOrDefault().STATUS_CD > 59)
                            {
                                retVal = true;
                            }
                            else if (s.STATUS_CD < 60 && q.FirstOrDefault().STATUS_CD < 60)
                            {
                                retVal = true;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }
            }
            return retVal;
        }

        public bool isCanHold(string pvNoParam, string pvYearParam, string userName, string statusParam)
        {
            bool retVal = false;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                try
                {
                    Int16 status = Int16.Parse(statusParam);
                    decimal reffNo = Decimal.Parse(String.Concat(pvNoParam, pvYearParam));

                    var q = db.TB_R_DISTRIBUTION_STATUS
                            .Where(a => a.REFF_NO == reffNo
                                && a.PROCCED_BY == userName
                                && (a.STATUS_CD > 19 && a.STATUS_CD < 30));

                    if (q.Any())
                    {
                        if ((status > 59 && status < 70) ||
                            (status > 80 && status < 90))
                        {
                            retVal = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }
            }
            return retVal;
        }

        public bool Verified(int docNo, int docYear, UserData u)
        {
            int r = UQx<int>("SetVerifiedRV", new { docNo = docNo, docYear = docYear, username = u.USERNAME }).FirstOrDefault();

            return r != 0;
            /*
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from p in db.TB_R_RV_H
                         where p.RV_NO == docNo && p.RV_YEAR == docYear
                         select p).FirstOrDefault();
                if (q != null)
                {
                    if (q.HOLD_FLAG != null && q.HOLD_FLAG > 0)
                    {
                        FormPersistence per = new FormPersistence();
                        string holdbyRole = per.getUserRoleID(q.HOLD_BY);
                        if (holdbyRole.Equals("ELVIS_COUNTER"))
                        {
                            q.HOLD_FLAG = 0;
                        }
                    }
                    q.SUBMIT_HC_DOC_DATE = DateTime.Now;
                    q.COUNTER_FLAG = 1;
                    q.CHANGED_BY = u.USERNAME;
                    q.CHANGED_DATE = DateTime.Now;
                    db.SaveChanges();

                    return true;
                }
            }
            return false;
             */
        }

    }
}
