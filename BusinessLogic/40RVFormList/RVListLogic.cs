﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Linq;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common;
using Common.Data;
using Common.Data._40RVFormList;
using Common.Data._40RVList;
using Common.Data.SAPData;
using Common.Function;
using DataLayer.Model;
using Dapper;

namespace BusinessLogic._40RVList
{
    public static class RVListOp
    {
        public const byte RV_RECEIVED_BY_CASHIER = 1;
        public const byte RV_CHECK_BY_ACCOUNTING = 2;
    }

    public partial class RVListLogic : VoucherListLogic
    {
        public const byte RV_RECEIVED_BY_CASHIER = 1;
        public const byte RV_CHECK_BY_ACCOUNTING = 2;
        public IQueryable<vw_RV_List> SearchListIqueryable(
            VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;

            var q = (from p in db.vw_RV_List
                     select p);
            if (!c.ignoreDate)
                q = q.Where(p => p.RV_DATE >= c.docDateFrom && p.RV_DATE <= c.docDateTo);

            if (!c.ignoreNum)
                q = q.Where(p => (p.RV_NO >= c.numFrom && p.RV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.RV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.RECEIVED_DATE != null && p.RECEIVED_DATE >= c.transDateFrom && p.RECEIVED_DATE <= c.transDateTo)));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => ((p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.SUBMIT_HC_DOC_DATE <= c.verifiedDateTo)));

            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.RV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.RV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.RV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q.AsQueryable<vw_RV_List>();
        }

        public IQueryable<vw_RV_List_Status_User> SearchListStatusUserIqueryable(
          VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;

            var q = (from p in db.vw_RV_List_Status_User
                     select p);
            if (!c.ignoreDate)
                q = q.Where(p => p.RV_DATE >= c.docDateFrom && p.RV_DATE <= c.docDateTo);
            if (!c.ignoreNum)
                q = q.Where(p => (p.RV_NO >= c.numFrom && p.RV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.RV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.RECEIVED_DATE != null && p.RECEIVED_DATE >= c.transDateFrom && p.RECEIVED_DATE <= c.transDateTo)));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => ((p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.SUBMIT_HC_DOC_DATE <= c.verifiedDateTo)));

            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.RV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.RV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.RV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q.AsQueryable<vw_RV_List_Status_User>();
        }

        public IQueryable<vw_RV_List_Status_Finance> SearchListStatusFinanceIqueryable(
           VoucherSearchCriteria c)
        {
            db.CommandTimeout = 3600;

            var q = (from p in db.vw_RV_List_Status_Finance
                     select p);
            if (!c.ignoreDate)
                q = q.Where(p => p.RV_DATE >= c.docDateFrom && p.RV_DATE <= c.docDateTo);

            if (!c.ignoreNum)
                q = q.Where(p => (p.RV_NO >= c.numFrom && p.RV_NO <= c.numTo));
            if (!c.ignoreType)
                q = q.Where(p => (p.RV_TYPE_CD == c.docType));
            if (!c.ignoreVendorCode)
                q = q.Where(p => (p.VENDOR_CD == c.vendorCode));
            if (!c.ignoreIssuingDivision)
            {
                if (!c.issuingDivision.Contains(";"))
                    q = q.Where(p => (p.DIVISION_ID == c.issuingDivision));
                else
                {
                    q = q.Where(p => c.divs.Contains(p.DIVISION_ID));
                }
            }
            if (!c.ignoreLastStatus)
                q = q.Where(p => c.lastStatus.Contains(p.STATUS_CD));
            if (c.notice >= 0)
                q = q.Where(p => ((c.notice == 0 && p.NOTICES == 0) || (c.notice > 0 && p.NOTICES > 0)));
            if (!c.ignoreTransactionType)
                q = q.Where(p => (p.TRANSACTION_CD == c.transactionType));
            if (!c.ignorePaymentMethod)
                q = q.Where(p => (p.PAY_METHOD_CD == c.paymentMethod));
            if (!c.ignoreVendorGroup)
                q = q.Where(p => (p.VENDOR_GROUP_CD == c.vendorGroup));
            if (!c.ignoreActivityDate)
                q = q.Where(p => ((p.ACTIVITY_DATE_TO >= c.activityDateFrom && p.ACTIVITY_DATE_FROM <= c.activityDateTo)));
            if (!c.ignoreSubmitDate)
                q = q.Where(p => ((p.SUBMIT_DATE != null && p.SUBMIT_DATE >= c.submitDateFrom && p.SUBMIT_DATE <= c.submitDateTo)));
            if (!c.ignoreTransDate)
                q = q.Where(p => ((p.RECEIVED_DATE != null && p.RECEIVED_DATE >= c.transDateFrom && p.RECEIVED_DATE <= c.transDateTo)));
            if (!c.ignoreVerifiedDate)
                q = q.Where(p => ((p.SUBMIT_HC_DOC_DATE != null && p.SUBMIT_HC_DOC_DATE >= c.verifiedDateFrom && p.SUBMIT_HC_DOC_DATE <= c.verifiedDateTo)));

            if (c.holdFlag >= 0)
                q = q.Where(p => (p.HOLD_FLAG == c.holdFlag));
            if (!c.ignoreNumEndsWith)
                q = q.Where(p => (p.RV_NO_STRING.EndsWith(c.sItemPost)));
            if (!c.ignoreNumStartsWith)
                q = q.Where(p => (p.RV_NO_STRING.StartsWith(c.sItemPre)));
            if (!c.ignoreNumContains)
                q = q.Where(p => (p.RV_NO_STRING.Contains(c.sItemPre)));
            if (c.workflowStatus >= 0)
                q = q.Where(p => p.WORKFLOW_STATUS == c.workflowStatus);
            if (c.isCanceled)
                q = q.Where(p => p.CANCEL_FLAG.HasValue && p.CANCEL_FLAG != 0);
            else
                q = q.Where(p => !p.CANCEL_FLAG.HasValue || p.CANCEL_FLAG == 0);
            if (!c.ignoreBy)
            {
                switch (c.iBy)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sByPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.CREATED_BY.StartsWith(c.sByPre) && p.CREATED_BY.EndsWith(c.sByPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.CREATED_BY.Contains(c.sBy));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (!c.ignoreNextApprover)
            {
                switch (c.iNext)
                {
                    case LikeLogic.igFIRST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre));
                        break;
                    case LikeLogic.igFIRSTLAST:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNextPre));
                        break;
                    case LikeLogic.igMIDDLE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.StartsWith(c.sNextPre) && p.NEXT_APPROVER_NAME.EndsWith(c.sNextPost));
                        break;
                    case LikeLogic.igNONE:
                        q = q.Where(p => p.NEXT_APPROVER_NAME.Contains(c.sNext));
                        break;
                    case LikeLogic.igALL:
                        break;
                    default:
                        break;
                }
            }
            if (c.isDeleted)
                q = q.Where(p => p.DELETED.HasValue && !(p.DELETED == 0));
            else
                q = q.Where(p => !p.DELETED.HasValue || (p.DELETED == 0));
            return q.AsQueryable<vw_RV_List_Status_Finance>();
        }

        private bool ValidateActualOnTime(DateTime? planDate, DateTime? actualDate)
        {
            bool isValid = true;
            if (planDate != null)
            {
                if (actualDate == null)
                {
                    DateTime now = DateTime.Now;
                    if (now.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
                else
                {
                    if (actualDate.Value.CompareTo(planDate) > 0)
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }

        public bool Delete(List<RVFormData> listOfRVNo, string username)
        {
            bool _result = false;

            DbTransaction T1 = null;
            using (ContextWrap co = new ContextWrap())
            {
                try
                {
                    var DB = co.db;
                    T1 = DB.Connection.BeginTransaction();

                    foreach (RVFormData RVNo in listOfRVNo)
                    {
                        int rvNo = RVNo.RVNumber;
                        int rvYear = RVNo.RVYear;
                        // Get TB_R_RV_H
                        var q = (from i in DB.TB_R_RV_H
                                 where i.RV_NO == rvNo
                                 where i.RV_YEAR == rvYear
                                 select i).FirstOrDefault();
                        if (q == null) continue;

                        string reff = rvNo.str() + rvYear.str();
                        decimal REFF_NO = reff.Dec();

                        //decimal reffNo = Convert.ToDecimal((q.REFFERENCE_NO ?? 0));
                        if (q.REFFERENCE_NO != null)
                        {
                            //string reff = Convert.ToString(q.REFFERENCE_NO);
                            // Delete TB_R_ATTACHMENT
                            var att = (from a in DB.TB_R_ATTACHMENT
                                       where a.REFERENCE_NO == reff
                                       select a).ToList();
                            if (att.Any())
                            {
                                foreach (var tmp in att)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }
                        }

                        // Delete TB_R_NOTICE
                        var not = (from n in DB.TB_R_NOTICE
                                   where n.DOC_NO == rvNo
                                   where n.DOC_YEAR == rvYear
                                   select n).ToList();
                        if (not.Any())
                        {
                            foreach (var tmp in not)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_DISTRIBUTION_STATUS
                        var dis = (from d in DB.TB_R_DISTRIBUTION_STATUS
                                   where d.REFF_NO == REFF_NO
                                   select d).ToList();
                        if (dis.Any())
                        {
                            foreach (var tmp in dis)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Delete TB_R_PVRV_AMOUNT
                        var rvrv = (from r in DB.TB_R_PVRV_AMOUNT
                                    where r.DOC_NO == rvNo
                                    where r.DOC_YEAR == rvYear
                                    select r).ToList();
                        if (rvrv.Any())
                        {
                            foreach (var tmp in rvrv)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        // Get TB_R_RV_DETAIL for Invoice checking and re-enable conversion
                        var rvdI = (from pi in DB.TB_R_RV_D
                                    where pi.RV_NO == rvNo
                                    where pi.RV_YEAR == rvYear
                                    select pi).ToList();
                        if (rvdI.Any())
                        {
                            foreach (var pInv in rvdI)
                            {
                                var v = (from inv in DB.TB_R_INVOICE_H
                                         where inv.PV_NO == pInv.RV_NO
                                         where inv.PV_YEAR == pInv.RV_YEAR
                                         select inv).FirstOrDefault();
                                if (v != null)
                                {
                                    v.STATUS_CD = -1;
                                    v.CONVERT_FLAG = 0;
                                    v.CHANGED_BY = username;
                                    v.CHANGED_DT = DateTime.Now;
                                }
                            }

                        }

                        // Delete TB_R_RV_DETAIL
                        var rvd = (from p in DB.TB_R_RV_D
                                   where p.RV_NO == rvNo
                                   where p.RV_YEAR == rvYear
                                   select p).ToList();
                        if (rvd.Any())
                        {
                            foreach (var tmp in rvd)
                            {
                                DB.DeleteObject(tmp);
                                DB.SaveChanges();
                            }
                        }

                        if (q.RV_TYPE_CD == 2)
                        {
                            // Get the previous RV Form if any
                            int susNo = q.SUSPENSE_NO ?? 0;
                            int susYear = q.SUSPENSE_YEAR ?? 0;

                            var preP = (from v in DB.TB_R_PV_H
                                        where v.PV_NO ==  susNo && v.PV_YEAR == susYear
                                        select v).FirstOrDefault();
                            if (preP != null)
                            {
                                // Get the RV (if any)
                                int pvSettle = preP.SET_NO_PV ?? 0;
                                var newR = (from r in DB.TB_R_PV_H
                                            where r.SUSPENSE_NO == susNo
                                            where r.SUSPENSE_YEAR == susYear
                                            select r).FirstOrDefault();

                                if (newR != null)
                                {
                                    // Delete the PVRV Amount of the particular RV
                                    var newrv = (from n in DB.TB_R_PVRV_AMOUNT
                                                 where n.DOC_NO == newR.PV_NO
                                                 where n.DOC_YEAR == newR.PV_YEAR
                                                 select n).ToList();
                                    if (newrv.Any())
                                    {
                                        foreach (var tmp in newrv)
                                        {
                                            DB.DeleteObject(tmp);
                                            DB.SaveChanges();
                                        }
                                    }

                                    // Delete RV Detail
                                    var newRD = (from rd in DB.TB_R_PV_D
                                                 where rd.PV_NO == newR.PV_NO
                                                 where rd.PV_YEAR == newR.PV_YEAR
                                                 select rd).ToList();
                                    if (newRD.Any())
                                    {
                                        foreach (var tmp in newRD)
                                        {
                                            DB.DeleteObject(tmp);
                                            DB.SaveChanges();
                                        }
                                    }
                                    newR.DELETED = 1;
                                    newR.CHANGED_BY = username;
                                    newR.CHANGED_DATE = DateTime.Now;
                                    //DB.DeleteObject(newR);
                                    
                                    DB.SaveChanges();
                                }

                                // Get the settlement of the previous RV and re-enable conversion
                                var prevSetH = (from ps in DB.TB_R_SET_FORM_H
                                                where ps.PV_SUS_NO == susNo
                                                where ps.PV_YEAR == susYear
                                                select ps).FirstOrDefault();
                                if (prevSetH != null)
                                {
                                    prevSetH.CONVERT_FLAG = 0;
                                    prevSetH.CHANGED_BY = username;
                                    prevSetH.CHANGED_DT = DateTime.Now;
                                    DB.SaveChanges();
                                }
                                preP.SET_NO_PV = null;
                                preP.SET_NO_RV = null;
                                preP.CHANGED_BY = username;
                                preP.CHANGED_DATE = DateTime.Now;
                                DB.SaveChanges();
                            }

                            // Delete TB_R_SET_FORM_D
                            var setD = (from d in DB.TB_R_SET_FORM_D
                                        where d.PV_SUS_NO == rvNo
                                        where d.PV_YEAR == rvYear
                                        select d).ToList();
                            if (setD.Any())
                            {
                                foreach (var tmp in setD)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }

                            // Delete TB_R_SET_FORM_H
                            var setH = (from h in DB.TB_R_SET_FORM_H
                                        where h.PV_SUS_NO == rvNo
                                        where h.PV_YEAR == rvYear
                                        select h).ToList();
                            if (setH.Any())
                            {
                                foreach (var tmp in setH)
                                {
                                    DB.DeleteObject(tmp);
                                    DB.SaveChanges();
                                }
                            }
                        }
                        // DB.DeleteObject(q);
                        q.NEXT_APPROVER = "";
                        q.DELETED = 1;
                        q.CHANGED_DATE = DateTime.Now;
                        q.CHANGED_BY = username;

                        logic.k2.deleteprocess(q.RV_NO.ToString() + q.RV_YEAR.ToString());

                        DB.SaveChanges();
                    }
                    _result = true;
                    T1.Commit();
                }
                catch (Exception Ex)
                {
                    T1.Rollback();
                    LoggingLogic.err(Ex);
                    throw;
                }

            }
            return _result;
        }


        public String getCashierName()
        {
            String cashierName = "";

            var q = (from p in db.vw_User_Role_Detail
                     where p.ROLE_ID == "ELVIS_CASHIER"
                     where p.ROLE_NAME == "ELVIS CASHIER"
                     select new
                     {
                         p.FIRST_NAME,
                         p.LAST_NAME
                     }).ToList();

            if (q.Any())
            {
                cashierName = String.Format("{0} {1}", q.First().FIRST_NAME, q.First().LAST_NAME);
            }
            return cashierName;
        }

        #region Get String Builder

        private String DateTimeToString(DateTime? dateTimeParam, String format)
        {
            if (dateTimeParam != null)
            {
                DateTime dateTimeParam1 = (DateTime)dateTimeParam;
                return dateTimeParam1.ToString(format);
            }
            else
            {
                return null;
            }
        }

        public byte[] Get(string _username, string _imagePath,
                VoucherSearchCriteria c)
        {
            //String dateTimeNoTime = "dd MMM yyyy";
            //String dateTimeWithTime = "dd MMM yyyy HH:mm";

            ExcelWriter x = new ExcelWriter();
            try
            {
                int sizeForAll = SearchListIqueryable(c).Count();

                string header1 = "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                                "Downloaded Date : " + DateTime.Now.DATUM() + ExcelWriter.COL_SEP +
                                "Total Record Download : " + sizeForAll;
                string header2 = "No|RV No|Vendor Name|Transaction Type|Total Amount IDR|Last Status|" + //"Amount{IDR|USD|JPY|Other}|" +
                                "RV Date|Submit Date|Paid Date|" +
                                "Issuing Division|RV Type|Suspense No|" +
                                "RV Settlement No|RV Settlement No|Vendor Code|" +
                                "Payment Method|Budget No|SAP Doc No|" +
                                "Attachment|Submit HC Doc Date|" +
                                "Activity Date{From|To}|" +
                                "Hold By|Version|" +
                                "Created{By|Date}|Changed{By|Date}";
                x.PutPage("General Info");
                x.PutHeader("RV List General Info", header1, header2, _imagePath);
                int rownum = 0;
                int pageSize = ExcelWriter.xlRowLimit;
                // the first eleven rows is used as header
                pageSize = pageSize - 11;
                int isNewPage = 1;
                int counter = 1;

                List<vw_RV_List> _list = SearchListIqueryable(c).ToList();
                foreach (vw_RV_List dx in _list)
                {
                    if (counter >= pageSize)
                    {
                        counter = 1;
                        x.PutTail();
                        x.PutPage("General Info " + ++isNewPage);
                        x.PutHeader("RV List General Info", header1, header2, _imagePath);
                    }
                    x.Put(
                        ++rownum,
                        dx.RV_NO, dx.VENDOR_NAME, dx.TRANSACTION_NAME, dx.TOTAL_AMOUNT_IDR.fmt(), dx.LAST_STATUS,
                        dx.RV_DATE.fmt(StrTo.dateTimeNoTime),
                        dx.SUBMIT_DATE.minute(),
                        dx.RECEIVED_DATE.minute(),
                        dx.DIVISION_NAME, dx.RV_TYPE_CD, dx.SUSPENSE_NO,
                        dx.SET_NO_RV, dx.SET_NO_RV, dx.VENDOR_CD,
                        dx.PAY_METHOD_NAME, dx.BUDGET_NO, dx.SAP_DOC_NO,
                        dx.ATTACHMENT_QUANTITY, dx.SUBMIT_HC_DOC_DATE.DATUM(),
                        //dx.LT_STATUS_USER, dx.LT_STATUS_FINANCE,
                        dx.ACTIVITY_DATE_FROM.minute(),
                        dx.ACTIVITY_DATE_TO.minute(),
                        dx.HOLD_BY_NAME, dx.VERSION,
                        dx.CREATED_BY, dx.CREATED_DATE.minute(), dx.CHANGED_BY, dx.CHANGED_DATE.minute());
                    counter++;
                }

                x.PutTail();

                x.PutPage("Status User");
                header2 = "No|RV No|RV Year|Related Division{Registered{Actual}|Approved by SH{Plan|Actual|Pln L/T|Act L/T}|" +
                    "Approved by DpH{Plan|Actual|Pln L/T|Act L/T}|Approved by DH{Plan|Actual|Pln L/T|Act L/T}}"
                    ;
                x.PutHeader("RV List Status User", header1, header2, _imagePath);
                rownum = 0;
                isNewPage = 1;
                counter = 1;

                List<vw_RV_List_Status_User> _listUsr = SearchListStatusUserIqueryable(c).ToList();
                foreach (vw_RV_List_Status_User dx in _listUsr)
                {
                    if (counter >= pageSize)
                    {
                        counter = 1;
                        x.PutTail();
                        x.PutPage("Status User " + ++isNewPage);
                        x.PutHeader("RV List Status User", header1, header2, _imagePath);
                    }

                    x.Put(
                        ++rownum,
                        dx.RV_NO, dx.RV_YEAR,
                        dx.REG_ACTUAL_DT.minute(),
                        dx.PLAN_DT_SH.minute(),
                        dx.ACTUAL_DT_SH.minute(), dx.PLAN_LT_SH, dx.ACTUAL_LT_SH,
                        dx.PLAN_DT_DPH.minute(),
                        dx.ACTUAL_DT_DPH.minute(),
                        dx.PLAN_LT_DPH, dx.ACTUAL_LT_DPH,
                        dx.PLAN_DT_DH.minute(),
                        dx.ACTUAL_DT_DH.minute(),
                        dx.PLAN_LT_DH, dx.ACTUAL_LT_DH);
                    counter++;
                }

                x.PutTail();

                x.PutPage("Status Finance");
                header2 = "No|RV No|RV Year|Finance Division" +
                    "{Receipt Cashier{Plan|Actual|Pln L/T|Act L/T}|" +
                    "Verified by FD Staff{Plan|Actual|Pln L/T|Act L/T}|" +
                    "Approved by SH{Plan|Actual|Pln L/T|Act L/T}|" +
                   "Approved by FD DpH{Plan|Actual|Pln L/T|Act L/T}|" +
                    "Closing{Plan|Actual|Pln L/T|Act L/T}}";
                x.PutHeader("RV List Status Finance", header1, header2, _imagePath);
                rownum = 0;
                isNewPage = 1;
                counter = 1;

                List<vw_RV_List_Status_Finance> _listFin = SearchListStatusFinanceIqueryable(c).ToList();
                foreach (vw_RV_List_Status_Finance dx in _listFin)
                {
                    if (counter >= pageSize)
                    {
                        counter = 1;
                        x.PutTail();
                        x.PutPage("Status Finance " + ++isNewPage);
                        x.PutHeader("RV List Status Finance", header1, header2, _imagePath);
                    }
                    /*int otherCount = 0;
                            if (dx.AMOUNT_OTHER != null)
                                otherCount = dx.AMOUNT_OTHER.Count;*/
                    x.Put(
                            ++rownum,
                            dx.RV_NO, dx.RV_YEAR,

                            dx.PLAN_DT_RECEIVED.minute(),
                            dx.ACTUAL_DT_RECEIVED.minute(),
                            dx.PLAN_LT_RECEIVED,
                            dx.ACTUAL_LT_RECEIVED,

                            dx.PLAN_DT_VERIFIED_FINANCE.minute(),
                            dx.ACTUAL_DT_VERIFIED_FINANCE.minute(),
                            dx.PLAN_LT_VERIFIED_FINANCE,
                            dx.ACTUAL_LT_VERIFIED_FINANCE,

                            dx.PLAN_DT_APPROVED_FINANCE_SH.minute(),
                            dx.ACTUAL_DT_APPROVED_FINANCE_SH.minute(),
                            dx.PLAN_LT_APPROVED_FINANCE_SH,
                            dx.ACTUAL_LT_APPROVED_FINANCE_SH,

                            dx.PLAN_DT_APPROVED_FINANCE_DPH.minute(),
                            dx.ACTUAL_DT_APPROVED_FINANCE_DPH.minute(),
                            dx.PLAN_LT_APPROVED_FINANCE_DPH,
                            dx.ACTUAL_LT_APPROVED_FINANCE_DPH,

                            dx.PLAN_DT_CLOSING.minute(),
                            dx.ACTUAL_DT_CLOSING.minute(),
                            dx.PLAN_LT_CLOSING,
                            dx.ACTUAL_LT_CLOSING
                          );
                    counter++;
                }

                x.PutTail();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }

            return x.GetBytes();
        }


        public string getInvalidPrintStatusCover(string rv_no, string rv_year)
        {
            string status = null;
            decimal reffNo = Convert.ToDecimal(rv_no + rv_year);

            var q = (from p in db.TB_R_DISTRIBUTION_STATUS
                     where p.REFF_NO == reffNo
                     && p.ACTUAL_DT == null
                     select p).FirstOrDefault();
            int statusCd = 0;

            if (q != null)
            {
                List<int> r = (from p in db.TB_R_DISTRIBUTION_STATUS
                               where p.REFF_NO == reffNo
                               && p.STATUS_CD != null // && (p.STATUS_CD >= 50 && p.STATUS_CD < 60)
                               && p.ACTUAL_DT == null
                               select p.STATUS_CD).ToList();
                if (r != null && r.Count > 0)
                {
                    r = r.Where(x => x >= 50 && x < 60).ToList();
                    if (r != null && r.Count > 0)
                        statusCd = r.Max();
                }
            }
            else
            {
                statusCd = 10;
            }
            if (statusCd > 0)
            {
                var s = (from p in db.TB_M_STATUS
                         where p.STATUS_CD == statusCd
                         select p).FirstOrDefault();
                if (s != null)
                {
                    status = s.STATUS_NAME;
                }
            }
            return status;
        }


        #endregion

        public override bool PostToSap(List<string> keys, int _ProcessId, ref string _Message, UserData u, int? transType = null)
        {
            base.PostToSap(keys, _ProcessId, ref _Message, u);
            return RVPostToSap(keys, _ProcessId, ref _Message, u.USERNAME);
        }

        ///Added by Akhmad Nuryanto, 6/6/2012
        ///Post to SAP
        public bool RVPostToSap(List<string> _ListofRVListData, int _ProcessId, ref string _Message, string uid)
        {
            // changed to appropriate status code
            int postToSAPStatus = 68;
            int[] alreadyPosted = new int[] { 68, 69, 70 };
            bool result = false;
            List<RVPostInputHeader> _ListInputHeader = new List<RVPostInputHeader>();
            List<RVPostInputDetail> _ListInputDetail = new List<RVPostInputDetail>();
            List<RVPostSAPResult> _ListResult = new List<RVPostSAPResult>();
            List<string> _isPosted = new List<string>();
            bool isError = false;
            List<string> _isNotReadyToBePosted = new List<string>();
            List<string> _FailedDocuments = new List<string>();
            List<string> _SucceedDocuments = new List<string>();

            this["OK"] = _SucceedDocuments;
            this["NG"] = _FailedDocuments;
            this["notready"] = _isNotReadyToBePosted;
            this["done"] = _isPosted;

            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    #region validation
                    foreach (string item in _ListofRVListData)
                    {
                        string reffNo = item.Replace(":", "");
                        System.Data.Objects.ObjectParameter IsReady = new System.Data.Objects.ObjectParameter("IS_READY", "");
                        db.isReadyToPost(reffNo, IsReady);
                        if (IsReady == null || IsReady.Value.Equals("") || IsReady.Value.Equals("0"))
                        {
                            isError = true;
                            if (!_isNotReadyToBePosted.Contains(item))
                            {
                                _isNotReadyToBePosted.Add(item);
                            }
                        }
                    }
                    #endregion

                    if (!isError)
                    {
                        var q = (from p in db.vw_RVPostInputHeader
                                 where _ListofRVListData.Contains(p.RV_NO + ":" + p.RV_YEAR)
                                 select p).ToList();

                        if (q.Any())
                        {

                            List<string> _ListofRVDetail = new List<string>();

                            foreach (var data in q)
                            {
                                int i = 1;
                                int j = 1;
                                RVPostInputHeader _InputHeader = new RVPostInputHeader();
                                // if (data.isExistRecord != null)
                                if (alreadyPosted.Contains(data.STATUS_CD))
                                {
                                    if (!_isPosted.Contains(_InputHeader.RV_NO + _InputHeader.RV_YEAR))
                                    {
                                        _isPosted.Add(_InputHeader.RV_NO + _InputHeader.RV_YEAR);
                                    }
                                }
                                else
                                {
                                    _InputHeader.RV_NO = Convert.ToString(data.RV_NO);
                                    _InputHeader.RV_YEAR = Convert.ToString(data.RV_YEAR);
                                    _InputHeader.SEQ_NO = Convert.ToString(i);
                                    _InputHeader.RV_DATE = data.RV_DATE.ToString("dd.MM.yyyy");
                                    _InputHeader.RV_TYPE = Convert.ToString(data.RV_TYPE_CD);
                                    _InputHeader.RV_NO_SUSPENSE = Convert.ToString(data.SUSPENSE_NO);
                                    _InputHeader.TRANS_TYPE = Convert.ToString(data.TRANSACTION_CD);
                                    _InputHeader.VENDOR = data.VENDOR_CD;
                                    _InputHeader.VENDOR_GROUP = Convert.ToString(data.VENDOR_GROUP_CD);
                                    _InputHeader.INVOICE_NO = data.INVOICE_NO;
                                    _InputHeader.TAX_NO = data.TAX_NO;
                                    _InputHeader.PAYMENT_TERM = data.PAYMENT_TERM;
                                    _InputHeader.PAYMENT_METHOD = data.PAY_METHOD_CD;

                                    //_InputHeader.PLAN_PAYMENT_DATE = ((DateTime)data.PLANNING_PAYMENT_DATE).ToString("dd.MM.yyyy");
                                    _InputHeader.POSTING_DATE = DateTime.Now.ToString("dd.MM.yyyy");
                                    _InputHeader.TOTAL_AMOUNT = ((decimal)data.AMOUNT).ToString("#.##");
                                    _InputHeader.CURRENCY_CD = data.CURRENCY_CD;
                                    _InputHeader.TAX_CD = data.TAX_CD;
                                    _InputHeader.HEADER_TEXT = data.TAX_NO;
                                    //_InputHeader.BANK_TYPE = Convert.ToString(data.BANK_TYPE);

                                    _ListInputHeader.Add(_InputHeader);
                                    i++;

                                    if (!_ListofRVDetail.Contains(data.RV_NO + data.RV_YEAR))
                                    {
                                        var details = (from p in db.vw_RVPostInputDetail
                                                       where _ListofRVListData.Contains(p.RV_NO + ":" + p.RV_YEAR)
                                                       orderby p.RV_YEAR, p.RV_NO, p.INVOICE_NO
                                                       select p).ToList();

                                        if (details.Any())
                                        {
                                            foreach (var dataDetail in details)
                                            {
                                                RVPostInputDetail _InputDetail = new RVPostInputDetail();
                                                _InputDetail.RV_NO = Convert.ToString(dataDetail.RV_NO);
                                                _InputDetail.RV_YEAR = Convert.ToString(dataDetail.RV_YEAR);
                                                _InputDetail.SEQ_NO = Convert.ToString(j);
                                                _InputDetail.GL_ACCOUNT = Convert.ToString(dataDetail.GL_ACCOUNT);

                                                if (data.RV_TYPE_CD == 3)
                                                {
                                                    _InputDetail.AMOUNT = ((decimal)dataDetail.SPENT_AMOUNT).ToString("#.##");
                                                }
                                                else
                                                {
                                                    _InputDetail.AMOUNT = ((decimal)dataDetail.AMOUNT).ToString("#.##");
                                                }

                                                _InputDetail.COST_CENTER = Convert.ToString(dataDetail.COST_CENTER_CD);
                                                _InputDetail.WBS_ELEMENT = dataDetail.WBS_NO;
                                                _InputDetail.ITEM_TEXT = dataDetail.STD_WORDING;

                                                _ListInputDetail.Add(_InputDetail);
                                                j++;
                                            }
                                        }
                                        _ListofRVDetail.Add(data.RV_NO + data.RV_YEAR);
                                    }
                                }
                            }

                            if (_isPosted.Count > 0)
                            {
                                _Message = m.Message("MSTD00075ERR", CommonFunction.CommaJoin(_isPosted));
                                log.Log("MSTD00075ERR", _Message, "RVPostToSap()", uid, "", _ProcessId);
                                return false;
                            }
                        }

                        if (_ListInputHeader.Count > 0 && _ListInputDetail.Count > 0)
                        {
                            _ListResult = new List<RVPostSAPResult>(); //not yet implemented new SAPNco3Logic().GetResultPostRVToSAP(_ListInputHeader, _ListInputDetail, _ProcessId);
                            if (_ListResult.Count > 0)
                            {
                                try
                                {
                                    foreach (RVPostSAPResult item in _ListResult)
                                    {
                                        if (item.MESSAGE == "S")
                                        {
                                            TB_R_SAP_DOC_NO _TB_R_SAP_DOC_NO = new TB_R_SAP_DOC_NO();

                                            _TB_R_SAP_DOC_NO.DOC_NO = Convert.ToInt32(item.RV_NO);
                                            _TB_R_SAP_DOC_NO.DOC_YEAR = Convert.ToInt32(item.RV_YEAR);
                                            _TB_R_SAP_DOC_NO.ITEM_NO = Convert.ToInt32(item.ITEM_NO);
                                            _TB_R_SAP_DOC_NO.CURRENCY_CD = item.CURRENCY_CD;
                                            _TB_R_SAP_DOC_NO.INVOICE_NO = item.INVOICE_NO;
                                            _TB_R_SAP_DOC_NO.SAP_DOC_NO = item.SAP_DOC_NO;
                                            _TB_R_SAP_DOC_NO.SAP_DOC_YEAR = item.SAP_DOC_YEAR == "" ? 0 : Convert.ToInt32(item.SAP_DOC_YEAR);

                                            db.AddToTB_R_SAP_DOC_NO(_TB_R_SAP_DOC_NO);
                                            db.SaveChanges();
                                            if (!_SucceedDocuments.Contains(item.RV_NO + item.RV_YEAR))
                                            {
                                                _SucceedDocuments.Add(item.RV_NO + item.RV_YEAR);
                                                int intRVNo = Convert.ToInt32(item.RV_NO);
                                                int intRVYear = Convert.ToInt32(item.RV_YEAR);
                                                TB_R_RV_H _TB_R_RV_H = (from p in db.TB_R_RV_H
                                                                        where p.RV_NO == intRVNo && p.RV_YEAR == intRVYear
                                                                        select p).FirstOrDefault();
                                                if (_TB_R_RV_H != null)
                                                {
                                                    _TB_R_RV_H.STATUS_CD = postToSAPStatus;
                                                    _TB_R_RV_H.POSTING_DATE = DateTime.Now;
                                                    db.SaveChanges();
                                                }
                                            }
                                            result = true;
                                        }
                                        else
                                        {
                                            if (!_FailedDocuments.Contains(item.RV_NO + item.RV_YEAR))
                                            {
                                                _FailedDocuments.Add(item.RV_NO + item.RV_YEAR);
                                            }
                                        }
                                    }


                                    if (_FailedDocuments.Count > 0 && _SucceedDocuments.Count > 0)
                                    {
                                        _Message = m.Message("MSTD00079INF",
                                            CommonFunction.CommaJoin(_SucceedDocuments),
                                            CommonFunction.CommaJoin(_FailedDocuments));
                                        log.Log("MSTD00079INF", _Message, "RVPostToSap()", uid, "", _ProcessId);
                                    }
                                    else if (_FailedDocuments.Count > 0)
                                    {
                                        _Message = m.Message("MSTD00068ERR", "RV");
                                        log.Log("MSTD00068ERR", _Message, "RVPostToSap()", uid, "", _ProcessId);

                                    }
                                    else
                                    {
                                        _Message = m.Message("MSTD00067INF", "RV");
                                        log.Log("MSTD00067INF", _Message, "RVPostToSap()", uid, "", _ProcessId);
                                    }

                                }
                                catch (Exception e)
                                {
                                    _Message = e.Message;
                                    log.Log("MSTD00076ERR", _Message, "RVPostToSap()", uid, "", _ProcessId);
                                    //throw e;
                                }
                            }
                        }
                    }
                    else
                    {
                        _Message = m.Message("MSTD00078ERR", CommonFunction.CommaJoin(_isNotReadyToBePosted));
                        log.Log("MSTD00078ERR", _Message, "RVPostToSap()", uid, "", _ProcessId);
                    }
                }
                catch (Exception ex)
                {
                    _Message = ex.Message;
                    log.Log("MSTD00076ERR", _Message, "RVPostToSap()", uid, "", _ProcessId);
                    //throw ex;
                }

            return result;
        }
        ///end of addition by Akhmad Nuryanto



        private void SetHistoryData(RVFormHistoryData d, RVFormHistoryData x)
        {
            d.RVNumber = x.RVNumber;
            d.RVYear = x.RVYear;
            d.Version = x.Version;
            d.StatusCode = x.StatusCode;
            d.PaymentMethodCode = x.PaymentMethodCode;
            d.VendorGroupCode = x.VendorGroupCode;
            d.VendorCode = x.VendorCode;
            d.RVTypeCode = x.RVTypeCode;
            d.TransactionCode = x.TransactionCode;
            d.SuspenseNumber = x.SuspenseNumber;
            d.BudgetNumber = x.BudgetNumber;
            d.ActivityDate = x.ActivityDate;
            d.ActivityDateTo = x.ActivityDateTo;
            d.DivisionID = x.DivisionID;
            d.RVDate = x.RVDate;
            d.TaxCode = x.TaxCode;
            d.TaxInvoiceNumber = x.TaxInvoiceNumber;
            d.PostingDate = x.PostingDate;
            //d.PlanningPaymentDate = x.PlanningPaymentDate;
            d.BankType = x.BankType;
            d.TaxCalculated = x.TaxCalculated;
            d.SingleWbsUsed = x.SingleWbsUsed;
            d.Version = x.Version;
            d.UpdatedDt = x.UpdatedDt;
            d.UpdatedBy = x.UpdatedBy;
            d.RVTypeName = x.RVTypeName;
            d.VendorGroupName = x.VendorGroupName;
            d.PaymentMethodName = x.PaymentMethodName;
            d.StatusName = x.StatusName;
            d.VendorName = x.VendorName;
            d.TransactionName = x.TransactionName;
            d.SetNoPv = x.SetNoPv;
            d.SetNoRv = x.SetNoRv;
            d.SubmitDate = x.SubmitDate;
            d.PaidDate = x.PaidDate;
            d.WorkflowStatus = x.WorkflowStatus;
            //d.SettlementStatus = x.SettlementStatus;
            d.ReferenceNo = x.ReferenceNo;
            d.LocationCode = x.LocationCode;
            d.SubmitHcDocDate = x.SubmitHcDocDate;
            d.HoldFlag = x.HoldFlag;
            d.HoldBy = x.HoldBy;
            d.ModifiedBy = x.ModifiedBy;
            d.TotalAmount = x.TotalAmount;
            d.Details = GetDetailHistory(x.Details, false);
        }

        private static List<RVFormHistoryDetail> GetDetailHistory(List<RVFormHistoryDetail> x, bool forDelete)
        {
            if (x != null)
            {
                List<RVFormHistoryDetail> y = new List<RVFormHistoryDetail>();
                foreach (var z in x)
                {
                    RVFormHistoryDetail dt = new RVFormHistoryDetail()
                    {
                        SequenceNumber = z.SequenceNumber,
                        RVNumber = z.RVNumber,
                        RVYear = z.RVYear,
                        Version = z.Version,
                        CostCenterCode = z.CostCenterCode,
                        CostCenterCodeChange = z.CostCenterCodeChange,
                        CostCenterName = z.CostCenterName,
                        CostCenterNameChange = z.CostCenterNameChange,
                        Description = z.Description,
                        DescriptionChange = z.DescriptionChange,
                        CurrencyCode = z.CurrencyCode,
                        CurrencyCodeChange = z.CurrencyCodeChange,
                        Amount = z.Amount,
                        AmountChange = z.AmountChange,
                        TaxCode = z.TaxCode,
                        TaxCodeChange = z.TaxCodeChange,
                        TaxNumber = z.TaxNumber,
                        TaxNumberChange = z.TaxNumberChange,
                        ItemTransactionCode = z.ItemTransactionCode,
                        ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                        ItemTransactionName = z.ItemTransactionName,
                        ItemTransactionNameChange = z.ItemTransactionNameChange,
                        //WbsNumber = z.WbsNumber,
                        //WbsNumberChange = z.WbsNumberChange,
                        InvoiceNumber = z.InvoiceNumber,
                        InvoiceNumberChange = z.InvoiceNumberChange,
                        IsDeleted = forDelete
                    };

                    y.Add(dt);
                }

                return y;
            }
            return null;
        }

        private static RVFormHistoryDetail GetDetailHistoryObj(RVFormHistoryDetail z)
        {
            if (z != null)
            {
                RVFormHistoryDetail dt = new RVFormHistoryDetail()
                {
                    SequenceNumber = z.SequenceNumber,
                    RVNumber = z.RVNumber,
                    RVYear = z.RVYear,
                    Version = z.Version,
                    CostCenterCode = z.CostCenterCode,
                    CostCenterCodeChange = z.CostCenterCodeChange,
                    CostCenterName = z.CostCenterName,
                    CostCenterNameChange = z.CostCenterNameChange,
                    Description = z.Description,
                    DescriptionChange = z.DescriptionChange,
                    CurrencyCode = z.CurrencyCode,
                    CurrencyCodeChange = z.CurrencyCodeChange,
                    Amount = z.Amount,
                    AmountChange = z.AmountChange,
                    TaxCode = z.TaxCode,
                    TaxCodeChange = z.TaxCodeChange,
                    TaxNumber = z.TaxNumber,
                    TaxNumberChange = z.TaxNumberChange,
                    ItemTransactionCode = z.ItemTransactionCode,
                    ItemTransactionCodeChange = z.ItemTransactionCodeChange,
                    ItemTransactionName = z.ItemTransactionName,
                    ItemTransactionNameChange = z.ItemTransactionNameChange,
                    //WbsNumber = z.WbsNumber,
                    //WbsNumberChange = z.WbsNumberChange,
                    InvoiceNumber = z.InvoiceNumber,
                    InvoiceNumberChange = z.InvoiceNumberChange,
                    IsDeleted = z.IsDeleted
                };

                return dt;
            }
            return null;
        }

        private static List<RVFormHistoryDetail> GetChangedDetailValue(List<RVFormHistoryDetail> det)
        {
            List<RVFormHistoryDetail> tmp = null;
            if (det != null && det.Any())
            {
                tmp = new List<RVFormHistoryDetail>();
                foreach (var d in det)
                {
                    d.CostCenterCodeChange = true;
                    d.CostCenterNameChange = true;
                    d.DescriptionChange = true;
                    d.CurrencyCodeChange = true;
                    d.AmountChange = true;
                    d.TaxCodeChange = true;
                    d.TaxNumberChange = true;
                    d.ItemTransactionCodeChange = true;
                    d.ItemTransactionNameChange = true;
                    //d.WbsNumberChange = true;
                    d.InvoiceNumberChange = true;
                    tmp.Add(d);
                }
            }
            return tmp;
        }

        private static RVFormHistoryDetail GetChangedDetailValue(RVFormHistoryDetail d)
        {
            if (d != null)
            {
                d.CostCenterCodeChange = true;
                d.CostCenterNameChange = true;
                d.DescriptionChange = true;
                d.CurrencyCodeChange = true;
                d.AmountChange = true;
                d.TaxCodeChange = true;
                d.TaxNumberChange = true;
                d.ItemTransactionCodeChange = true;
                d.ItemTransactionNameChange = true;
                //d.WbsNumberChange = true;
                d.InvoiceNumberChange = true;
            }
            return d;
        }

        public List<RVFormHistoryData> GetHistoryData(String rvNoParam, String rvYearParam)
        {
            List<RVFormHistoryData> returnData = new List<RVFormHistoryData>();
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    
                    int rvNo = Convert.ToInt32(rvNoParam);
                    int rvYear = Convert.ToInt16(rvYearParam);

                    var qq = from p in db.TB_H_RV_H
                             join ve in db.vw_Vendor on p.VENDOR_CD equals ve.VENDOR_CD into vc
                             from vcItem in vc.DefaultIfEmpty()
                             join vg in db.TB_M_VENDOR_GROUP on p.VENDOR_GROUP_CD equals vg.VENDOR_GROUP_CD into vgGroup
                             from vgItem in vgGroup.DefaultIfEmpty()
                             join t in db.TB_M_TRANSACTION_TYPE on p.TRANSACTION_CD equals t.TRANSACTION_CD into tty
                             from ttyItem in tty.DefaultIfEmpty()
                             join pm in db.TB_M_PAYMENT_METHOD on p.PAY_METHOD_CD equals pm.PAY_METHOD_CD into pmtd
                             from pmtdItem in pmtd.DefaultIfEmpty()
                             join s in db.TB_M_STATUS on p.STATUS_CD equals s.STATUS_CD into sts
                             from stsItem in sts.DefaultIfEmpty()
                             join rvt in db.TB_M_RV_TYPE on p.RV_TYPE_CD equals rvt.RV_TYPE_CD into pty
                             from ptyItem in pty.DefaultIfEmpty()
                             join u in db.vw_User on p.CHANGED_BY equals u.USERNAME

                             where (p.RV_NO.Equals(rvNo) && p.RV_YEAR.Equals(rvYear))
                             orderby p.VERSION
                             select new
                             {
                                 p.VERSION,
                                 p.CHANGED_DATE,
                                 p.CHANGED_BY,
                                 u.LAST_NAME,
                                 p.RV_DATE,
                                 p.RV_YEAR,
                                 p.RV_NO,
                                 p.REFFERENCE_NO,
                                 p.ACTIVITY_DATE_FROM,
                                 p.ACTIVITY_DATE_TO,
                                 p.POSTING_DATE,
                                 p.RV_TYPE_CD,
                                 ptyItem.RV_TYPE_NAME,
                                 p.DIVISION_ID,
                                 p.VENDOR_GROUP_CD,
                                 vgItem.VENDOR_GROUP_NAME,
                                 //p.BANK_TYPE,
                                 //p.PLANNING_PAYMENT_DATE,
                                 p.STATUS_CD,
                                 stsItem.STATUS_NAME,
                                 p.TRANSACTION_CD,
                                 ttyItem.TRANSACTION_NAME,
                                 p.VENDOR_CD,
                                 vcItem.VENDOR_NAME,
                                 p.PAY_METHOD_CD,
                                 pmtdItem.PAY_METHOD_NAME,
                                 p.BUDGET_NO,
                                 p.SUSPENSE_NO,
                                 p.SET_NO_PV,
                                 p.SET_NO_RV,
                                 p.SUBMIT_DATE,
                                 p.RECEIVED_DATE,
                                 p.WORKFLOW_STATUS,
                                 //p.SETTLEMENT_STATUS,
                                 p.LOCATION_CD,
                                 p.SUBMIT_HC_DOC_DATE,
                                 p.HOLD_BY,
                                 p.HOLD_FLAG,
                                 p.TOTAL_AMOUNT
                             };

                    RVFormHistoryData old = null;
                    foreach (var q in qq)
                    {
                        RVFormHistoryData _new = new RVFormHistoryData();
                        RVFormHistoryData x = new RVFormHistoryData();

                        _new.Version = q.VERSION;
                        _new.RVNumber = q.RV_NO;
                        _new.RVYear = q.RV_YEAR;
                        _new.UpdatedDt = q.CHANGED_DATE;
                        _new.UpdatedBy = q.CHANGED_BY;
                        _new.RVDate = q.RV_DATE;
                        _new.DivisionID = q.DIVISION_ID;

                        _new.RVTypeCode = q.RV_TYPE_CD;
                        _new.RVTypeName = q.RV_TYPE_NAME;
                        _new.VendorGroupCode = q.VENDOR_GROUP_CD;
                        _new.VendorGroupName = q.VENDOR_GROUP_NAME;
                        _new.PaymentMethodCode = q.PAY_METHOD_CD;
                        _new.PaymentMethodName = q.PAY_METHOD_NAME;
                        _new.StatusCode = q.STATUS_CD;
                        _new.StatusName = q.STATUS_NAME;
                        _new.VendorCode = q.VENDOR_CD;
                        _new.VendorName = q.VENDOR_NAME;
                        _new.TransactionCode = q.TRANSACTION_CD;
                        _new.TransactionName = q.TRANSACTION_NAME;
                        _new.ActivityDate = q.ACTIVITY_DATE_FROM;
                        _new.ActivityDateTo = q.ACTIVITY_DATE_TO;
                        _new.PostingDate = q.POSTING_DATE;
                        //_new.PlanningPaymentDate = q.PLANNING_PAYMENT_DATE;
                        //_new.BankType = q.BANK_TYPE;
                        _new.BudgetNumber = q.BUDGET_NO;
                        _new.SetNoPv = q.SET_NO_PV;
                        _new.SetNoRv = q.SET_NO_RV;
                        _new.SubmitDate = q.SUBMIT_DATE;
                        
                        _new.ReceivedDate = q.RECEIVED_DATE;
                        _new.WorkflowStatus = q.WORKFLOW_STATUS;
                        //_new.SettlementStatus = q.SETTLEMENT_STATUS;
                        _new.ReferenceNo = q.REFFERENCE_NO;
                        _new.LocationCode = q.LOCATION_CD;
                        _new.SubmitHcDocDate = q.SUBMIT_HC_DOC_DATE;
                        _new.HoldBy = q.HOLD_BY;
                        _new.HoldFlag = q.HOLD_FLAG;
                        _new.ModifiedBy = q.LAST_NAME;
                        _new.TotalAmount = q.TOTAL_AMOUNT;

                        // get detail
                        List<RVFormHistoryDetail> historyDetail = null;
                        var dd = from d in db.TB_H_RV_D
                                 join c in db.vw_CostCenter on d.COST_CENTER_CD equals c.COST_CENTER into cctr
                                 from subCctr in cctr.DefaultIfEmpty()
                                 join it in db.TB_M_ITEM_TRANSACTION on d.ITEM_TRANSACTION_CD equals it.ITEM_TRANSACTION_CD into itTrans
                                 from subIt in itTrans.DefaultIfEmpty()
                                 where (d.RV_NO.Equals(rvNo) && d.RV_YEAR.Equals(rvYear) && d.VERSION.Equals(_new.Version))
                                 orderby d.SEQ_NO
                                 select new
                                 {
                                     d.SEQ_NO,
                                     d.VERSION,
                                     d.RV_YEAR,
                                     d.RV_NO,
                                     d.COST_CENTER_CD,
                                     COST_CENTER_NAME = subCctr.DESCRIPTION,
                                     d.CURRENCY_CD,
                                     d.DESCRIPTION,
                                     d.AMOUNT,
                                     // d.WBS_NO,
                                     d.INVOICE_NO,
                                     d.ITEM_TRANSACTION_CD,
                                     subIt.ITEM_TRANSACTION_DESC,
                                     d.TAX_CD,
                                     d.TAX_NO
                                 };

                        if (dd.Any())
                        {
                            historyDetail = new List<RVFormHistoryDetail>();
                            foreach (var d in dd)
                            {
                                RVFormHistoryDetail dt = new RVFormHistoryDetail()
                                {
                                    SequenceNumber = d.SEQ_NO,
                                    RVNumber = d.RV_NO,
                                    RVYear = d.RV_YEAR,
                                    Version = d.VERSION,
                                    CostCenterCode = d.COST_CENTER_CD,
                                    CostCenterCodeChange = false,
                                    CostCenterName = d.COST_CENTER_NAME,
                                    CostCenterNameChange = false,
                                    Description = d.DESCRIPTION,
                                    DescriptionChange = false,
                                    CurrencyCode = d.CURRENCY_CD,
                                    CurrencyCodeChange = false,
                                    Amount = d.AMOUNT,
                                    AmountChange = false,
                                    TaxCode = d.TAX_CD,
                                    TaxCodeChange = false,
                                    TaxNumber = d.TAX_NO,
                                    TaxNumberChange = false,
                                    ItemTransactionCode = d.ITEM_TRANSACTION_CD,
                                    ItemTransactionCodeChange = false,
                                    ItemTransactionName = d.ITEM_TRANSACTION_DESC,
                                    ItemTransactionNameChange = false,
                                    //WbsNumber = d.WBS_NO,
                                    //WbsNumberChange = false,
                                    InvoiceNumber = d.INVOICE_NO,
                                    InvoiceNumberChange = false,
                                    IsDeleted = false
                                };

                                historyDetail.Add(dt);
                            }

                            _new.Details = historyDetail;
                        }

                        SetHistoryData(x, _new);

                        if (old != null)
                        {
                            x.Version = _new.Version;
                            x.RVNumber = _new.RVNumber;
                            x.RVYear = _new.RVYear;

                            if (_new.RVDate.Equals(old.RVDate))
                                x.RVDate = null;

                            if (_new.DivisionID == null || _new.DivisionID.Equals(old.DivisionID))
                                x.DivisionID = null;

                            if (_new.RVTypeCode.Equals(old.RVTypeCode))
                            {
                                x.RVTypeCode = null;
                                x.RVTypeName = null;
                            }

                            if (_new.VendorGroupCode.Equals(old.VendorGroupCode))
                            {
                                x.VendorGroupCode = null;
                                x.VendorGroupName = null;
                            }

                            if (_new.PaymentMethodCode == null || _new.PaymentMethodCode.Equals(old.PaymentMethodCode))
                            {
                                x.PaymentMethodCode = null;
                                x.PaymentMethodName = null;
                            }

                            if (_new.StatusCode.Equals(old.StatusCode))
                            {
                                x.StatusCode = null;
                                x.StatusName = null;
                            }

                            if (_new.VendorCode == null || _new.VendorCode.Equals(old.VendorCode))
                            {
                                x.VendorCode = null;
                                x.VendorName = null;
                            }

                            if (_new.TransactionCode.Equals(old.TransactionCode))
                            {
                                x.TransactionCode = null;
                                x.TransactionName = null;
                            }

                            if (_new.ActivityDate.Equals(old.ActivityDate)) x.ActivityDate = null;
                            if (_new.ActivityDateTo.Equals(old.ActivityDateTo)) x.ActivityDateTo = null;
                            if (_new.PostingDate.Equals(old.PostingDate)) x.PostingDate = null;
                            //if (_new.PlanningPaymentDate.Equals(old.PlanningPaymentDate)) x.PlanningPaymentDate = null;
                            if (_new.BankType.Equals(old.BankType)) x.BankType = null;
                            if (_new.BudgetNumber == null || _new.BudgetNumber.Equals(old.BudgetNumber)) x.BudgetNumber = null;
                            if (_new.SuspenseNumber.Equals(old.SuspenseNumber)) x.SuspenseNumber = null;
                            if (_new.SetNoPv.Equals(old.SetNoPv)) x.SetNoPv = null;
                            if (_new.SetNoRv.Equals(old.SetNoRv)) x.SetNoRv = null;
                            if (_new.SubmitDate.Equals(old.SubmitDate)) x.SubmitDate = null;
                            if (_new.PaidDate.Equals(old.PaidDate)) x.PaidDate = null;
                            if (_new.WorkflowStatus.Equals(old.WorkflowStatus)) x.WorkflowStatus = null;
                            //if (_new.SettlementStatus.Equals(old.SettlementStatus)) x.SettlementStatus = null;
                            if (_new.ReferenceNo.Equals(old.ReferenceNo)) x.ReferenceNo = null;
                            if (_new.LocationCode.Equals(old.LocationCode))
                            {
                                x.LocationCode = null;
                            }
                            if (_new.SubmitHcDocDate.Equals(old.SubmitHcDocDate)) x.SubmitHcDocDate = null;
                            if (_new.HoldBy == null || _new.HoldBy.Equals(old.HoldBy)) x.HoldBy = null;
                            if (_new.HoldFlag.Equals(old.HoldFlag)) x.HoldFlag = null;
                            if (_new.TotalAmount.Equals(old.TotalAmount)) x.TotalAmount = null;

                            // Compare details
                            if (_new.Details == null && old.Details != null)
                                x.Details = GetDetailHistory(old.Details, true);
                            else if (_new.Details != null && old.Details == null)
                                x.Details = GetChangedDetailValue(_new.Details);
                            else if (_new.Details != null && old.Details != null)
                            {
                                List<RVFormHistoryDetail> tmp = new List<RVFormHistoryDetail>();
                                RVFormHistoryDetail detTmp = new RVFormHistoryDetail();
                                foreach (var n in _new.Details)
                                {
                                    bool isAdded = true;
                                    foreach (var o in old.Details)
                                    {
                                        if (o.SequenceNumber.Equals(n.SequenceNumber))
                                        {
                                            isAdded = false;
                                            bool removeFromList = true;
                                            detTmp = GetDetailHistoryObj(n);
                                            if ((n.CostCenterCode == null && o.CostCenterCode != null) ||
                                                (n.CostCenterCode != null && !n.CostCenterCode.Equals(o.CostCenterCode)))
                                            {
                                                detTmp.CostCenterCodeChange = true;
                                                detTmp.CostCenterNameChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.Description == null && o.Description != null) ||
                                                (n.Description != null && !n.Description.Equals(o.Description)))
                                            {
                                                detTmp.DescriptionChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.StandardDescriptionWording == null && o.StandardDescriptionWording != null) ||
                                                (n.StandardDescriptionWording != null && !n.StandardDescriptionWording.Equals(o.StandardDescriptionWording)))
                                            {
                                                detTmp.StandardDescriptionWordingChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.CurrencyCode == null && o.CurrencyCode != null) ||
                                                (n.CurrencyCode != null && !n.CurrencyCode.Equals(o.CurrencyCode)))
                                            {
                                                detTmp.CurrencyCodeChange = true;
                                                removeFromList = false;
                                            }

                                            if (!n.Amount.Equals(o.Amount))
                                            {
                                                detTmp.AmountChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.TaxCode == null && o.TaxCode != null) ||
                                                (n.TaxCode != null && !n.TaxCode.Equals(o.TaxCode)))
                                            {
                                                detTmp.TaxCodeChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.TaxNumber == null && o.TaxNumber != null) ||
                                                (n.TaxNumber != null && !n.TaxNumber.Equals(o.TaxNumber)))
                                            {
                                                detTmp.TaxNumberChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.ItemTransactionCode == null && o.ItemTransactionCode != null) ||
                                                (n.ItemTransactionCode != null && !n.ItemTransactionCode.Equals(o.ItemTransactionCode)))
                                            {
                                                detTmp.ItemTransactionCodeChange = true;
                                                detTmp.ItemTransactionNameChange = true;
                                                removeFromList = false;
                                            }

                                            //if ((n.WbsNumber == null && o.WbsNumber != null) ||
                                            //    (n.WbsNumber != null && !n.WbsNumber.Equals(o.WbsNumber)))
                                            //{
                                            //    detTmp.WbsNumberChange = true;
                                            //    removeFromList = false;
                                            //}

                                            if ((n.InvoiceNumber == null && o.InvoiceNumber != null) ||
                                                (n.InvoiceNumber != null && !n.InvoiceNumber.Equals(o.InvoiceNumber)))
                                            {
                                                detTmp.InvoiceNumberChange = true;
                                                removeFromList = false;
                                            }

                                            if ((n.InvoiceNumberUrl == null && o.InvoiceNumberUrl != null) ||
                                                (n.InvoiceNumberUrl != null && !n.InvoiceNumberUrl.Equals(o.InvoiceNumberUrl)))
                                            {
                                                detTmp.InvoiceNumberUrlChange = true;
                                                removeFromList = false;
                                            }

                                            if (!removeFromList)
                                                tmp.Add(detTmp);
                                        }
                                    }
                                    if (isAdded)
                                    {
                                        detTmp = GetDetailHistoryObj(n);
                                        tmp.Add(GetChangedDetailValue(detTmp));
                                    }
                                }
                                foreach (var o in old.Details)
                                {
                                    bool isDeleted = true;
                                    foreach (var n in _new.Details)
                                    {
                                        if (o.SequenceNumber.Equals(n.SequenceNumber))
                                        {
                                            isDeleted = false;
                                            break;
                                        }
                                    }
                                    if (isDeleted)
                                    {
                                        detTmp = GetDetailHistoryObj(o);
                                        detTmp.IsDeleted = true;
                                        tmp.Add(detTmp);
                                    }
                                }

                                x.Details = tmp;
                            }
                        }
                        else
                        {
                            old = new RVFormHistoryData();
                        }
                        returnData.Add(x);
                        SetHistoryData(old, _new);
                    }

                    returnData.Reverse();
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                    throw;
                }

            return returnData;
        }

        

        public bool isCanDirect(string rvNoParam, string rvYearParam, string userName, decimal div)
        {
            bool retVal = false;
            decimal reffNo = Decimal.Parse(rvNoParam + rvYearParam);
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = db.TB_R_DISTRIBUTION_STATUS.Where(a => a.REFF_NO == reffNo && a.ACTUAL_DT == null).OrderBy(a => a.STATUS_CD).ToList();
                if (q.Any())
                {
                    var s = q.Where(a => a.PROCCED_BY == userName).FirstOrDefault();
                    if (s != null)
                    {
                        if (s.STATUS_CD > 61 && q.FirstOrDefault().STATUS_CD > 61)
                        {
                            retVal = true;
                        }
                        else if (s.STATUS_CD < 60 && q.FirstOrDefault().STATUS_CD < 60)
                        {
                            retVal = true;
                        }
                    }
                }
            }

            return retVal;
        }

        public List<CodeConstant> getDocumentStatus()
        {
            return base.getDocumentStatus("2");
        }

        public void CloseDocumentAtLast(int no, int yy)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from r in db.TB_R_RV_H
                         where r.RV_NO == no && r.RV_YEAR == yy
                         select r).FirstOrDefault();
                if (q != null)
                {
                    if ((q.RV_TYPE_CD == 2 && q.STATUS_CD == 69)
                        || (q.RV_TYPE_CD == 1 && q.STATUS_CD == 68))
                    {
                        q.STATUS_CD = 99;
                        q.WORKFLOW_STATUS = 1;
                        db.SaveChanges();
                    }
                }
            }
        }

        public override bool SetNextApprover(int _no, int _yy, string by)
        {
            bool r = false;
            using (ContextWrap co = new ContextWrap())
                try
                {
                    var db = co.db;
                    var o = db.TB_R_RV_H
                        .Where(a => a.RV_NO == _no && a.RV_YEAR == _yy)
                        .FirstOrDefault();
                    if (o != null)
                    {
                        o.NEXT_APPROVER = by;
                    }
                    db.SaveChanges();
                    r = true;
                }
                catch (Exception ex)
                {
                    LoggingLogic.err(ex);
                }

            return r;
        }


        public bool Fill(RVListData z)
        {
            int _no = z.RV_NO.Int();
            int _yy = z.RV_YEAR.Int();
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var q = (from v in db.vw_RV_List select v).
                    Where(l => l.RV_NO == _no && l.RV_YEAR == _yy)
                    .FirstOrDefault();
                if (q != null)
                {
                    z.STATUS_CD = q.STATUS_CD.str();
                    z.RV_TYPE_CD = q.RV_TYPE_CD.str();
                    z.RV_DATE = (DateTime)q.RV_DATE;
                    z.DIVISION_NAME = q.DIVISION_NAME;
                    z.DIVISION_ID = q.DIVISION_ID;
                    z.VENDOR_CD = q.VENDOR_CD;
                    z.VENDOR_NAME = q.VENDOR_NAME;
                    z.TRANSACTION_CD = q.TRANSACTION_CD.str();
                    z.TRANSACTION_NAME = q.TRANSACTION_NAME;
                    z.TOTAL_AMOUNT_IDR = q.TOTAL_AMOUNT_IDR;
                    z.PAY_METHOD_CD = q.PAY_METHOD_CD;
                    z.PAY_METHOD_NAME = q.PAY_METHOD_NAME;
                    z.BUDGET_NO = q.BUDGET_NO;
                    z.BUDGET_DESCRIPTION = (from b in db.vw_WBS
                                            where b.WbsNumber == q.BUDGET_NO
                                            select b.Description).FirstOrDefault();
                    List<OtherAmountData> oad = logic.Look.GetAmounts(_no, _yy);
                    OtherAmountData oa = null;
                    oa = oad.Where(c => c.CURRENCY_CD == "IDR").FirstOrDefault();
                    z.AMOUNT_IDR = (oa != null) ? CommonFunction.Eval_Curr("IDR", oa.TOTAL_AMOUNT) : "";
                    oa = oad.Where(c => c.CURRENCY_CD == "JPY").FirstOrDefault();
                    z.AMOUNT_JPY = (oa != null) ? CommonFunction.Eval_Curr("JPY", oa.TOTAL_AMOUNT) : "";
                    oa = oad.Where(c => c.CURRENCY_CD == "USD").FirstOrDefault();
                    z.AMOUNT_USD = (oa != null) ? CommonFunction.Eval_Curr("USD", oa.TOTAL_AMOUNT) : "";

                    var qd = (from d in db.TB_R_RV_D
                              orderby d.SEQ_NO
                              where d.RV_NO == q.RV_NO && d.RV_YEAR == q.RV_YEAR
                              select d).FirstOrDefault();
                    if (qd != null)
                    {
                        z.DESCRIPTION = qd.DESCRIPTION;
                    }
                    z.NEXT_APPROVER = q.NEXT_APPROVER;
                    z.DELETED = (q.DELETED??0)==1;
                }
                return q != null;
            }
        }

    }
}

