﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class DonationHeaderTemplate
    {
        public string JENIS_SUMBANGAN { get; set; }

        public string BENTUK_SUMBANGAN { get; set; }

        public string NILAI_SUMBANGAN { get; set; }

        public string TANGGAL_SUMBANGAN { get; set; }

        public string NAMA_LEMBAGA_PENERIMA { get; set; }

        public string NPWP_LEMBAGA_PENERIMA { get; set; }

        public string ALAMAT_LEMBAGA { get; set; }

        public string NO_TELP_LEMBAGA { get; set; }

        public string SARANA_PRASARANA_INFRASTRUKTUR { get; set; }

        public string LOKASI_INFRASTRUKTUR { get; set; }

        public string BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }

        public string IZIN_MENDIRIKAN_BANGUNAN { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }
    }
}
