﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class PV_ENOM
    {
        public string CATEGORY_CODE { get; set; }
        public string ReferenceNoDataField { get; set; }
        public string ENOM_NO { get; set; }
        public string VENDOR_CD { get; set; }
        public Int32 VENDOR_GROUP_CD { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
    }
}
