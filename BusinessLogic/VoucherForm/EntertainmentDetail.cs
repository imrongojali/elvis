﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class EntertainmentDetail
    {
        public Int64 SEQ_NO { get; set; }
        public Int64 SEQ_NO_DETAIL { get; set; }
        public string CATEGORY_CODE { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
        public string REFERENCE_NO { get; set; }
        public string ENOM_NO { get; set; }
        public Int32 PV_NO { get; set; }
        public Int32 NO { get; set; }
        public string TANGGAL { get; set; }
        public string ALAMAT { get; set; }
        public string TIPE_ENTERTAINMENT { get; set; }
        public string JUMLAH { get; set; }
        public string NAMA_RELASI { get; set; }
        public string JABATAN { get; set; }
        public string ALAMAT_PERUSAHAAN { get; set; }
        public string TIPE_BISNIS { get; set; }
        public string KETERANGAN { get; set; }


        //public DateTime TANGGAL { get; set; }

        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }

    }
}
