﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class Entertainment
    {
        public Int64 SEQ_NO { get; set; }
        public Int64 SEQ_NO_DETAIL { get; set; }
        public string CATEGORY_CODE { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
        public string REFERENCE_NO { get; set; }
        public string ENOM_NO { get; set; }
        public Int32 PV_NO { get; set; }
        public Int32 NO { get; set; }
        public DateTime TANGGAL { get; set; }
        public string TEMPAT { get; set; }
        public string ALAMAT { get; set; }
        public string TIPE_ENTERTAINMENT { get; set; }
        public decimal JUMLAH { get; set; }
        //public string NAMA_RELASI { get; set; }
        //public string JABATAN { get; set; }
        //public string ALAMAT_PERUSAHAAN { get; set; }
        //public string TIPE_BISNIS { get; set; }
        //public string KETERANGAN { get; set; }
        public string FormType { get; set; }

        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public int THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }
    }
}
