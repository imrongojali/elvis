﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
   public class SelectedCategory
    {
        //TODO : Imron Model Selected Category
        public string CategoryCode { get; set; }
        public string NominativeType { get; set; }
        public string JointGroup { get; set; }
        public int GroupSeq { get; set; }
    }
}
