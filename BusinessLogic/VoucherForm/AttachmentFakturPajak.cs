﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class AttachmentFakturPajak
    {
        public Int64 ID { get; set; }
        public string REFERENCE_NO { get; set; }
        public string REF_SEQ_NO { get; set; }
        public string DESCRIPTION { get; set; }
        public string DIRECTORY { get; set; }
        public string FILE_NAME { get; set; }
        public string STATUS { get; set; }
        public string URL { get; set; }

        public string URL_DIRECTORY { get; set; }
        public string ERR_MESSAGE { get; set; }
        public string FormType { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime CREATED_DT { get; set; }

        public Int32 Sequence_number { get; set; }
    }
}
