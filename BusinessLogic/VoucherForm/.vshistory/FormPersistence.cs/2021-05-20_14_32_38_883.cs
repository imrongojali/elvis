﻿using BusinessLogic.AccruedForm;
using BusinessLogic.CommonLogic;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using Common.Data._80Accrued;
using Common.Data.SAPData;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using Common.Function;
using Dapper;
using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.Reporting.WebForms;
using QRCoder;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Xml;
using System.Xml.Serialization;

namespace BusinessLogic.VoucherForm
{
    public class FormPersistence : LogicBase
    {
        protected LogicFactory logic = LogicFactory.Get();
        protected DateTime today = DateTime.Now;
        protected string logi;

        private string _UserName;

        protected List<string> _errs;

        public CommonExcelUpload ceu = null;

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }


        protected NumberFormatInfo numberFormatInfo;
        public int[] suspenseTransactionCd;
        public int financeDirectorate = 0;
        public int BoardOfDirector = 0;

        public FormPersistence()
        {
            NumberFormatInfo formatInfo = CultureInfo.CurrentCulture.NumberFormat;
            numberFormatInfo = (NumberFormatInfo)formatInfo.Clone();
            numberFormatInfo.CurrencySymbol = "";
            _errs = new List<string>();

            string sWBSValid = "WBS_VALIDATION";
            financeDirectorate = logic.Sys.GetValue(sWBSValid, "FINANCE_DIR_DIV") ?? 0;
            BoardOfDirector = logic.Sys.GetValue(sWBSValid, "BOD_DIV") ?? 0;
            suspenseTransactionCd = (logic.Sys.GetText("PvForm", "SUSPENSE_TRANSACTION_CD") ?? "")
                .Split(',')
                .Select(x => x.Int(0))
                .Distinct()
                .ToArray();
            logi = "";
        }


        public virtual string getDocType(int? code)
        {
            return "";
        }

        public virtual List<CodeConstant> getDocTypes()
        {
            return new List<CodeConstant>();
        }
        public TransactionType GetTransactionType(int code)
        {
            return logic.Base.GetTransactionType(code);
        }

        protected List<GlAccountMappingData> _glAccountMapping = null;
        protected List<GlAccountMappingData> glAccountMapping
        {
            get
            {
                if (_glAccountMapping == null)
                {
                    _glAccountMapping = hQu<GlAccountMappingData>("GetGLAccountMapping").ToList();
                }
                return _glAccountMapping;
            }
        }


        protected int[] DivNoProductionFlagCheck()
        {
            string[] divs = logic.Sys.GetArray("FORM_AUTH", "SKIP_PRODUCTION_FLAG_CHECK_DIV");
            int[] r;
            if (divs == null || divs.Length < 1)
                r = new int[] { 5, 8 };
            else
            {
                r = new int[divs.Length];

                for (int i = 0; i < divs.Length; i++)
                {
                    r[i] = divs[i].Int();
                }
            }
            return r;
        }

        public int? GLAccount(int transactionCd, int itemTransactionCd)
        {
            int? r = null;
            if ((transactionCd > 0) && (itemTransactionCd > 0))
                r = glAccountMapping
                        .Where(g => g.TRANSACTION_CD == transactionCd && g.ITEM_TRANSACTION_CD == itemTransactionCd)
                        .Select(a => a.GL_ACCOUNT)
                        .FirstOrDefault();
            return r;
        }

        public int? GLAccount(string CostCenterCd, int transactionCd, int? itemTransactionCd = null)
        {
            IEnumerable<int> gli = hQx<int>("GetGLAccount", new { cc = CostCenterCd, tx = transactionCd, itx = itemTransactionCd });
            if (gli != null && gli.Any())
                return gli.FirstOrDefault();
            else
            {
                logic.Say("GLAcc", "GLAccount ({0},{1},{2}) not found", CostCenterCd, transactionCd.str(), itemTransactionCd.str() ?? "NULL");
                return null;
            }
        }

        public List<TransactionType> getTransactionTypes(
            UserData u = null,
            int Tag = -1,
            int VendorGroup = -1,
            bool isSuspense = false,
            bool hasDetail = true,
            bool isAuthorizedFinance = false,
            bool userDivisionCanUploadDetail = false,
            string _screenID = "")
        {
            bool isSecretary = false;
            List<TransactionType> l = new List<TransactionType>();
            List<int> divs = new List<int>();
            string divCd = "";
            if (u != null)
            {
                isSecretary = u.Roles.Contains("ELVIS_SECRETARY");
                int d = 0;
                divs = u.Divisions
                        .Where(a => int.TryParse(a, out d))
                        .Select(x => x.Int())
                        .ToList();
                divCd = Convert.ToString(u.DIV_CD);
            }
            string divst = string.Join(",", divs.ToArray());
            int isPVFormList = _screenID.Equals("ELVIS_PVFormList") ? 1 : 0;

            return hQx<TransactionType>("sp_GetTransactionTypesAccr",
                new
                {
                    divCd = divCd,
                    isPVFormList = isPVFormList,
                    userDIVs = divst,
                    userDivisionCanUploadDetail = ((userDivisionCanUploadDetail) ? 1 : 0),
                    isSecretary = ((isSecretary) ? 1 : 0),
                    tag = Tag,
                    isSuspense = ((isSuspense) ? 1 : 0)
                }, null, true, null, CommandType.StoredProcedure).ToList();
        }

        public string getPaymentMethodName(string code)
        {
            return hQx<string>("getPaymentMethodName", new { code = code }).FirstOrDefault();

        }

        public CodeConstant getVendorGroupNameByVendor(string vendorCode)
        {
            return hQu<CodeConstant>("getVendorGroupNameByVendor", StrTo.RemoveQuotes(vendorCode)).FirstOrDefault();
        }
        public List<CodeConstant> getCurrencyCodeList()
        {
            return hQu<CodeConstant>("getCurrencyCodeList").ToList();
        }
        public List<CodeConstant> getCurrencyCodeListWOEurSgd()
        {
            return hQu<CodeConstant>("getCurrencyCodeListWOEurSgd").ToList();
        }
        public string getInitialDocumentStatus()
        {
            return getDocumentStatus(0);
        }
        public int getDocumentStatusCode(string status)
        {
            return hQx<int>("getDocumentStatusCode", new { statusName = status }).FirstOrDefault();

        }
        public string getDocumentStatus(int code)
        {
            return hQx<string>("GetDocumentStatusName", new { code = code }).FirstOrDefault();

        }
        public string getUserRoleID(string username)
        {
            return hQx<string>("getUserRoleID", new { username = username }).FirstOrDefault();

        }
        public string getUserRoleName(string username)
        {
            return hQx<string>("getUserRoleName", new { username = username }).FirstOrDefault();

        }
        public VendorBank GetVendorBank(string vendorCode, string key)
        {
            return hQx<VendorBank>("GetVendorBank", new { vendorCode = vendorCode, key = key }).FirstOrDefault();
        }

        public IQueryable<VendorBank> GetVendorBanks(string vendorCode, string key = "")
        {
            return hQx<VendorBank>("GetVendorBanks", new { VendorCode = vendorCode, Key = key }).Select(v => v).AsQueryable();
        }

        public List<WBSStructure> WbsNumbers(string bookingID = "")
        {
            return Qx<WBSStructure>("GetWbsNumberFromAccrBalance", new { bookingID = bookingID }).ToList();
        }

        public List<WBSStructure> GetWbsNumbers(UserData userData, PVFormData f)
        {
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null)
            {
                return new List<WBSStructure>(0);
            }
            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                int fiscalYear = logic.Sys.FiscalYear(DateTime.Now);
                StringBuilder sql = new StringBuilder(
                            "-- getWbsNumbers\r\n\r\n" +
                            "-- DIV_CD: " + division + " TRANSCATION_TYPE: " + txCode.str() + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.vw_WBS v \r\n" +
                            " WHERE (v.Year = " + fiscalYear.str() + ") \r\n"
                            // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;
                int tt = f.TransactionCode ?? 0;

                bool isGlAccountProject = hQu<int>("GetIsGLAccountProject", tt).FirstOrDefault() > 0;

                string divCondition = "";

                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " v.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " v.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " v.Division = '" + division + "'";
                    }
                }

                sql.AppendLine(
                          "   AND ( \r\n"
                        + ((isGlAccountProject)
                            ? "   -- is     GL ACCOUNT PROJECT \r\n       "
                            : "   -- is NOT GL ACCOUNT PROJECT \r\n     " + divCondition + "\r\n      AND NOT\r\n       ")
                        + "      (LEFT(WbsNumber, 1) IN ('P', 'S')) \r\n"
                        + "   )\r\n");


                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }

        public void DebugWbs(string mark, List<WBSStructure> w, string Username)
        {
            string fWbs = "wbs_" + CommonFunction.CleanFilename(Username).Replace(".", "_");
            StringBuilder so = new StringBuilder("");
            foreach (var o in w)
            {
                so.AppendFormat("{0}\t{1}\r\n", o.WbsNumber, o.Description);
            }
            LoggingLogic.say(fWbs + "_" + mark + ".txt", so.ToString());
        }

        public List<WBSStructure> getWbsNumbers(UserData userData, PVFormData f)
        {
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null)
            {
                return new List<WBSStructure>(0);
            }
            List<WBSStructure> wbsNumbers = new List<WBSStructure>();

            try
            {
                int fiscalYear = logic.Sys.FiscalYear(DateTime.Now);
                decimal BOD = BoardOfDirector;
                int tt = f.TransactionCode ?? 0;
                bool isGlAccountProject = hQu<int>("GetIsGLAccountProject", tt).FirstOrDefault() > 0;

                wbsNumbers = hQx<WBSStructure>("sp_GetWbsNumbers",
                        new
                        {
                            fiscalYear = fiscalYear
                        ,
                            isFinanceDirectorate = ((userData.DIV_CD >= financeDirectorate) ? 1 : 0)
                        ,
                            division = division
                        ,
                            isProject = (isGlAccountProject ? 1 : 0)
                        ,
                            transactionCode = tt
                        }
                        , null, true, null, CommandType.StoredProcedure).ToList();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return wbsNumbers;
        }

        //fid.Hadid
        public List<WBSStructure> getWbsNumbersAccrued(UserData userData)
        {
            string fWbs = "wbsByUser_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
            string division = userData.DIV_CD.ToString();

            List<WBSStructure> lstWbsResult = new List<WBSStructure>();

            try
            {
                // List<string> GlAccProjects = logic.Sys.GetList("GL_ACCOUNT_PROJECT");

                //int fiscalYear = DateTime.Now.Year - 1; // cuman buat testing tar ilangin -1 nya
                int fiscalYear = DateTime.Now.Year;
                StringBuilder sql = new StringBuilder(
                            "-- getWbsNumbersByUser\r\n\r\n" +
                            "-- DIV_CD: " + division + " \r\n" +
                            // "-- GL_ACCOUNT_PROJECT: " + string.Join(";", GlAccProjects.ToArray()) + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.vw_WBS v \r\n" +
                            " WHERE (v.Year = " + (fiscalYear).str() + ") \r\n"
                            // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;

                string divCondition = "";
                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " AND v.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " AND v.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " AND v.Division = '" + division + "'";
                    }
                }
                sql.AppendLine(divCondition);

                lstWbsResult = Db.Query<WBSStructure>(sql.ToString()).ToList();


            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstWbsResult;
        }
        public List<BookingNoStructure> getBookingNo(UserData userData, PVFormData f)
        {
            string fWbs = "bookingNo_" + CommonFunction.CleanFilename(userData.USERNAME).Replace(".", "_") + ".txt";
            int? txCode = f.TransactionCode;
            string division = userData.DIV_CD.ToString();
            if (txCode == null || txCode != logic.Sys.GetNum("TRANS_TYPE/ACCR_YE", 219))
            {
                return new List<BookingNoStructure>(0);
            }
            List<BookingNoStructure> lstBookingNo = new List<BookingNoStructure>();

            try
            {
                StringBuilder sql = new StringBuilder(
                            "-- getBookingNo\r\n\r\n" +
                            "-- DIV_CD: " + division + " TRANSCATION_TYPE: " + txCode.str() + "\r\n" +
                            // "-- GL_ACCOUNT_PROJECT: " + string.Join(";", GlAccProjects.ToArray()) + "\r\n" +
                            "\r\n" +
                            "SELECT DISTINCT b.BOOKING_NO BookingNo, v.WbsNumber, v.[Description] \r\n" +
                            "  FROM dbo.TB_R_ACCR_BALANCE b \r\n" +
                            "  JOIN vw_WBS v on b.WBS_NO_OLD = v.WbsNumber \r\n" +
                            "  JOIN TB_R_ACCR_LIST_D d on b.BOOKING_NO = d.BOOKING_NO \r\n" +
                            "  JOIN TB_R_ACCR_LIST_H h on d.ACCRUED_NO = h.ACCRUED_NO \r\n" +
                            " WHERE (b.BOOKING_STS = 0) \r\n" +
                            "   AND h.STATUS_CD = 105  AND b.PV_TYPE_CD = '1' \r\n"
                            // "   AND (LEFT(v.WbsNumber, 1) <> 'I') \r\n" // 2014.01.21 wot.daniel - no check this condition in TAM- MOVED TO VIEW
                            );

                decimal BOD = BoardOfDirector;

                string divCondition = "";
                if (userData.DIV_CD >= financeDirectorate)
                {
                    sql.AppendLine("   -- Finance Directorate " + BOD.str());
                    divCondition = " b.Division = " + BOD.str();
                }
                else
                {
                    StringMap pad = logic.Sys.GetDict("PAD_BUDGET_MAPPING");
                    if (pad.Has(division))
                    {
                        sql.AppendLine("   -- IN PAD BUDGET MAPPING" + pad.KV());
                        string padDivisions = Quoten(pad.Get(division));

                        divCondition = " b.Division IN (" + padDivisions + ") ";
                    }
                    else
                    {
                        sql.AppendLine("   -- NOT IN PAD BUDGET MAPPING");
                        divCondition = " b.DIVISION_ID = '" + division + "'";
                    }
                }
                sql.AppendLine("\r\n  AND");
                sql.AppendLine(divCondition);

                lstBookingNo = Db.Query<BookingNoStructure>(sql.ToString()).ToList();


            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }
            return lstBookingNo;
        }
        //end fid.Hadid

        public string Quoten(string nonquoted, string splitChar = ",", string quoteChar = "'")
        {
            if (nonquoted.isEmpty())
                return nonquoted;

            string[] splitz = nonquoted.Split(new string[] { splitChar }, StringSplitOptions.None);
            for (int i = 0; i < splitz.Length; i++)
            {
                string v = splitz[i].Trim();

                if (!(v.StartsWith(quoteChar) && v.EndsWith(quoteChar)))
                {
                    v = quoteChar + v + quoteChar;
                }
                else if (!v.StartsWith(quoteChar))
                {
                    v = quoteChar + v;
                }
                else if (!v.EndsWith(quoteChar))
                {
                    v = v + quoteChar;
                }
                splitz[i] = v;
            }
            return string.Join(splitChar, splitz);
        }

        public string getDescriptionDefaultWording(int? transactionTypeCode)
        {
            if (transactionTypeCode.HasValue)
            {
                return hQx<string>("getDescriptionDefaultWording", new { tt = @transactionTypeCode }).FirstOrDefault();
            }

            return null;
        }
        public List<VendorData> getVendorCodes()
        {
            return hQx<VendorData>("getVendorCodes", new { EXCLUDE_EXTERNAL_VENDOR = Common.AppSetting.EXCLUDE_EXTERNAL_VENDOR }).ToList();
        }
        public string getVendorCodeName(string code)
        {
            return hQx<string>("GetVendorNameByCode", new { code = code }).FirstOrDefault();
        }
        public string getVendorCodeByName(string name)
        {
            return Qx<string>("GetVendorCodeByName", new { name = name }).FirstOrDefault();
        }

        public List<CodeConstant> getCostCenterCodes(int productionFlag = 0, string div = "")
        {
            if (div.Int() >= financeDirectorate) div = BoardOfDirector.str(); // when director login assume from board of director division

            return hQx<CodeConstant>("GetCostCenterCodesByProdFlagDiv", new { @div = div, @prodFlag = productionFlag }).ToList();
        }
        public List<CostCenterCdData> GetCostCenters()
        {
            return hQu<CostCenterCdData>("GetCostCenters").ToList();
        }
        public CostCenterCdData getCostCenterByCode(string code, string div = "")
        {
            return Qx<CostCenterCdData>("GetCostCentersByCodeDiv", new { code = code, div = div }).FirstOrDefault();
        }
        public List<SAPDocumentNumber> getSAPDocumentNumbers(PVFormData formData)
        {
            return Qx<SAPDocumentNumber>("getSAPDocumentNumbers", new { docNo = formData.PVNumber ?? 0, docYear = formData.PVYear ?? 0 }).ToList();
        }

        public string getAttachmentCategoryName(string code)
        {
            return hQx<string>("GetAttachmentCategoryName", new { code = code }).FirstOrDefault();
        }
        public List<CodeConstant> getAttachmentCategories()
        {
            return hQu<CodeConstant>("getAttachmentCategories").ToList();
        }
        public string getCostCenterDescription(string code)
        {
            return hQx<string>("getCostCenterDescription", new { code = StrTo.RemoveQuotes(code) }).FirstOrDefault();
        }
        public List<ExchangeRate> getExchangeRates()
        {
            return (logic.Look.getExchangeRates());
        }
        public List<ExchangeRate> getExchangeRatesPerNo(int num, int year)
        {
            return Qx<ExchangeRate>("getExchangeRatesPerNo", new { anum = num, ayear = year }).ToList();
        }


        public IQueryable<BankKeysData> GetBankKeys()
        {
            return hQx<BankKeysData>("GetBankKey", null).AsQueryable();
        }

        public string getDivisionName(string divisionId)
        {
            return hQx<string>("getDivisionName", new { DivisionId = divisionId }).FirstOrDefault();
        }

        public List<Division> getDivisions()
        {
            return hQu<Division>("getDivisions").ToList();
        }


        public int? getPreviousDocumentStatus(PVFormData formData)
        {
            decimal referenceNumber = decimal.Parse(formData.PVNumber.ToString() + formData.PVYear.ToString());
            return hQu<int?>("getPreviousDistributionHistorySeq", referenceNumber).FirstOrDefault();

        }
        public void fetchAttachment(PVFormData formData, int pid = 0)
        {
            string pvNumber = formData.PVNumber.ToString();
            string pvYear = (formData.PVYear ?? DateTime.Now.Year).str();
            string refNumber = String.Concat(pvNumber, pvYear);
            if ((formData.PVNumber ?? 0) < 1)
            {
                refNumber = "T" + pid.str() + pvYear;
            }
            AttachmentTable attachmentTable = formData.AttachmentTable;

            fetchAttachment(refNumber, attachmentTable, pid);


        }

        //fid.Hadid
        public void fetchAttachment(string refNumber, AttachmentTable attachmentTable, int pid = 0)
        {


            List<FormAttachment> lstAttachment = attachmentTable.Attachments;
            lstAttachment.Clear();

            List<FormAttachment> ax = Qx<FormAttachment>("GetAttachmentByReff", new { REFERENCE_NO = refNumber }).ToList();
            foreach (var a in ax)
            {
                a.Blank = false;
                a.Url = CommonFunction.CombineUrl(Common.AppSetting.FTP_DOWNLOADHTTP, a.PATH, a.FileName);
            }
            lstAttachment.AddRange(ax);
            attachmentTable.addBlankRow();
        }
        //End fid.Hadid

        public void deleteAttachmentInfo(FormAttachment formAttachment)
        {
            Do("deleteAttachmentInfo", new { reffNo = formAttachment.ReferenceNumber, Filename = formAttachment.FileName });

        }
        public void saveAttachmentInfo(FormAttachment formAttachment, UserData userData)
        {
            Do("saveAttachmentInfo",
                    new
                    {
                        ReferenceNumber = formAttachment.ReferenceNumber,
                        FileName = formAttachment.FileName,
                        CategoryCode = formAttachment.CategoryCode,
                        Description = formAttachment.Description,
                        PATH = formAttachment.PATH,
                        UserName = userData.USERNAME
                    });

        }

        public bool postToK2(PVFormData formData, UserData userData, bool payingVoucher)
        {
            ITick its = Ticker.Tick;
            its.In("postToK2");
            bool postResult = false;
            try
            {
                string moduleCode = "1";
                moduleCode = (payingVoucher) ? "1" : "2";

                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", Common.AppSetting.ApplicationID);
                submitData.Add("ApprovalLevel", "1");
                submitData.Add("ApproverClass", "1");
                submitData.Add("ApproverCount", "1");
                submitData.Add("CurrentDivCD", userData.DIV_CD.ToString());
                submitData.Add("CurrentPIC", userData.USERNAME);
                submitData.Add("EmailAddress", userData.EMAIL);
                string entertain = "0";
                int? transactionCode = formData.TransactionCode;
                List<int?> lstEntertainmentTransactions = getEntertainmentTransactionCodes();
                if (lstEntertainmentTransactions != null)
                {
                    foreach (int? c in lstEntertainmentTransactions)
                    {
                        if (transactionCode == c)
                        {
                            entertain = "1";
                            break;
                        }
                    }
                }
                submitData.Add("Entertain", entertain);

                submitData.Add("K2Host", "");
                submitData.Add("LimitClass", "0");
                submitData.Add("ModuleCD", moduleCode);
                submitData.Add("NextClass", "0");
                submitData.Add("PIC", userData.DIV_CD.ToString());
                submitData.Add("Reff_No", formData.PVNumber.ToString() + formData.PVYear.ToString());
                string registerDate = string.Format("{0:G}", DateTime.Now);
                submitData.Add("RegisterDt", registerDate);

                decimal? prevDocStatus = getPreviousDocumentStatus(formData);
                string val = "0";
                if ((prevDocStatus != null) && (prevDocStatus.Value == 0))
                {
                    val = "1";
                }
                submitData.Add("RejectedFinance", val);
                submitData.Add("StatusCD", formData.StatusCode.ToString());
                submitData.Add("StepCD", "");
                submitData.Add("VendorCD", formData.VendorCode);

                decimal totalAmount = 0;
                PVFormTable formTable = formData.FormTable;
                if (formTable != null)
                {
                    totalAmount = formData.GetTotalAmount();
                    totalAmount = Math.Round(totalAmount);
                }
                string amt = totalAmount.ToString();
                // prevent overflow, K2 receive only int data type
                if (totalAmount > int.MaxValue) amt = int.MaxValue.ToString();
                if (totalAmount < int.MinValue) amt = int.MinValue.ToString();

                submitData.Add("TotalAmount", amt);

                //added by Akhmad Nuryanto, delete worklist if resubmitting form after rejected
                if (formData.isResubmitting)
                {

                    logic.k2.deleteprocess(formData.PVNumber.ToString() + formData.PVYear.ToString());

                }
                //end of addition by Akhmad Nuryanto

                its.In("K2SendCommandWithCheckWorklist");
                if (formData.PostK2ByWorklistChecking)
                {

                    postResult = logic.WorkFlow.K2SendCommandWithCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                    userData, formData.K2Command, submitData);

                }
                else
                {

                    postResult = logic.WorkFlow.K2SendCommandWithoutCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                        userData, formData.K2Command, submitData);
                }
                its.Out();
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
                throw;
            }
            finally
            {
                its.Out();
            }
            formData.PostedK2 = postResult;
            return postResult;
        }

        public List<int?> getEntertainmentTransactionCodes()
        {
            return hQu<int?>("getEntertainmentTransactionCodes").ToList();
        }

        protected string me = "";

        public bool Upload(string xlsFilename, string metaFile,
            List<PVFormDetail> dd,
            ref string[][] derr,
            int TransactionCd, PVFormData datas,
            UserData ux, int pid)
        {
            bool Ok = false;
            me = "FormPersistence.Upload";

            // bool isBudget = isBudgeted(TransactionCd);
            int[] noProdCheck = DivNoProductionFlagCheck();

            _errs.Clear();

            if (!System.IO.File.Exists(xlsFilename))
            {
                _errs.Add("Uploaded File not found");
            }

            string _meta = System.IO.File.ReadAllText(metaFile);
            if (string.IsNullOrEmpty(_meta))
            {
                _errs.Add("Metadata not found");
            }
            if (_errs.Count > 0) return false;

            ceu = new CommonExcelUpload();
            ceu.UserName = ux.USERNAME;
            ceu.PID = pid;

            ceu.LogWriter = logic.Msg.WriteMessage;
            Dictionary<string, object> _data;
            _data = ceu.Read(_meta, xlsFilename);

            if (_data.Keys.Count < 1)
            {
                _errs.Add("Empty file");
                return false;
            }

            // validate mandatory field and field length  
            // => this is *very* critical because process will fail 
            // when inserted data is not valid
            Ok = ceu.Check();

            // validate Detail 
            DataTable x = ceu.Rows;

            if (!Ok)
            {
                ShowFeedback(ceu, x, ref derr);
                return false;

                /// FOR TESTING OVERRIDE 
                /// Ok = true;
            }

            PutUpload(x, TransactionCd, datas.VendorCode, pid, xlsFilename, ux, ref dd, ref derr); /// formerly use steps in PutData

            Ok = (_errs.Count < 1);

            return Ok;
        }


        public bool ShowFeedback(CommonExcelUpload ceu, DataTable x, ref string[][] derr)
        {
            foreach (string s in ceu.ReadError)
            {
                _errs.Add(s);
            }

            if (x.Rows.Count > 0)
            {
                derr = new string[x.Rows.Count][];
                int ri = 0;
                int colCount = x.Columns.Count;
                foreach (DataRow r in x.Rows)
                {
                    derr[ri] = new string[colCount];

                    for (int i = 0; i < colCount; i++)
                    {
                        derr[ri][i] = Convert.ToString(r[i]);
                    }
                    ri++;
                }
            }
            else
            {
                derr = new string[1][];
                derr[0] = new string[5];
                for (int i = 0; i < 5; i++)
                {
                    derr[0][i] = "";
                }
                derr[0][15] = CommonFunction.CommaJoin(ceu.ReadError, "\r\n");
            }

            return (_errs.Count < 1);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="x">DataTable as input data, structures as described in metadata, must have same column count with user defined data table </param>
        /// <param name="TransactionCd">required param for header </param>
        /// <param name="VendorCd">required param for header</param>
        /// <param name="pid"> process id related to upload, for monitoring and atomicity</param>
        /// <param name="xlsFilename">uploaded file name</param>
        /// <param name="ux">User object</param>
        /// <param name="dd">Output data structure containing processed uploaded contents </param>
        /// <param name="derr">Output data structure containing errors </param>
        /// <returns></returns>
        public bool PutUpload(DataTable x
                , int TransactionCd
                , string VendorCd
                , int pid
                , string xlsFilename
                , UserData ux
                // output
                , ref List<PVFormDetail> dd
                , ref string[][] derr)
        {
            string spName = Qx<string>("GetSpFromTx",
                    new { tt = TransactionCd.str() }).FirstOrDefault();

            Exec("sp_PutUploadH",
                    new
                    {
                        pid = pid,
                        transactionCd = TransactionCd,
                        vendorCd = VendorCd,
                        uploadFileName = Path.GetFileName(xlsFilename),
                        uid = ux.USERNAME
                    },
                        null, null, CommandType.StoredProcedure);

            if (!spName.isEmpty() && x != null)
            {
                SqlBulkCopy bulk = new SqlBulkCopy(Db as SqlConnection);

                DecorateUpload(x, pid, ux.USERNAME);
                bulk.DestinationTableName = "TB_T_UPLOAD_D";
                bulk.ColumnMappings.Clear();
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    bulk.ColumnMappings.Add(x.Columns[i].ColumnName, x.Columns[i].ColumnName);
                }
                PreExecuteTest(x, ux, pid);

                bulk.WriteToServer(x);

                Exec(spName, new { pid = pid, uid = ux.USERNAME });
            }

            dd.AddRange(Qx<PVFormDetail>("GetLastUpload", new { pid = pid }).ToList());
            List<ErrorUploadData> ErrUploads = Qx<ErrorUploadData>("GetErrorUploadData", new { pid = pid }).ToList();
            if (ErrUploads != null && ErrUploads.Count > 0)
                _errs.AddRange(ErrUploads.Select(e => e.ERRMSG));
            return true;
        }

        public void DecorateUpload(DataTable x, int pid, string createdBy)
        {
            if (x.Columns.IndexOf("PROCESS_ID") < 0)
            {
                x.Columns.Add("PROCESS_ID", typeof(int));
            }
            if (x.Columns.IndexOf("CREATED_BY") < 0)
            {
                x.Columns.Add("CREATED_BY", typeof(string));
            }
            if (x.Columns.IndexOf("CREATED_DT") < 0)
            {
                x.Columns.Add("CREATED_DT", typeof(DateTime));
            }
            if (x.Columns.IndexOf("SEQ_NO") < 0)
            {
                x.Columns.Add("SEQ_NO", typeof(int));
            }
            List<int> existingNum = new List<int>();
            List<int> d = new List<int>();
            int maxseq = 0;
            for (int i = 0; i < x.Rows.Count; i++)
            {
                DataRow r = x.Rows[i];
                int seq = (r["SEQ_NO"] as int?) ?? 0;
                if (seq > maxseq) maxseq = seq;
                if (seq <= 0)
                    d.Add(i);
                else
                {
                    if (existingNum.Contains(seq))
                    {
                        d.Add(i);
                    }
                    else
                        existingNum.Add(seq);
                }

            }

            if (d.Count > 0)
            {
                for (int i = 0; i < d.Count; i++)
                {
                    DataRow r = x.Rows[d[i]];
                    maxseq++;
                    r["SEQ_NO"] = maxseq;
                }
            }

            for (int i = 0; i < x.Rows.Count; i++)
            {
                DataRow r = x.Rows[i];

                r["PROCESS_ID"] = pid;
                r["CREATED_BY"] = createdBy;
                r["CREATED_DT"] = DateTime.Now;
            }
        }


        public void PreExecuteTest(DataTable x, UserData ux, int pid)
        {
            StringBuilder stru = new StringBuilder("DataTableStructures\r\n");

            if (logic.Sys.GetNum("test.CommonExcelUpload/DataTableStru") == 1)
            {
                for (int i = 0; i < x.Columns.Count; i++)
                {
                    DataColumn c = x.Columns[i];
                    stru.AppendFormat("[{0}]\t{1}\t{2}\t{3}\r\n", i, c.ColumnName, c.DataType.ToString(), c.MaxLength.ToString());
                }
                logic.Log.Log("MSTD00001INF", stru.ToString(), "DataTableStru", ux.USERNAME, "9.1.1", pid);
            }

            if (logic.Sys.GetNum("test.CommonExcelUpload/PreExecuteTest") == 1)
            {
                StringBuilder b = new StringBuilder("");
                for (int j = 0; j < x.Columns.Count; j++)
                {
                    if (j > 0) b.Append(";");
                    b.Append(x.Columns[j].ColumnName);
                }
                b.Append("\r\n");
                for (int i = 0; i < x.Rows.Count; i++)
                {
                    DataRow r = x.Rows[i];
                    for (int j = 0; j < x.Columns.Count; j++)
                    {
                        if (j > 0) b.Append(";");
                        string s = r[j].str();
                        if (!s.isEmpty() && (s.Contains('\r') || s.Contains('\n')))
                        {
                            s = s.Replace("\r", " ");
                            s = s.Replace("\n", " ");
                            b.Append(s);
                        }
                        else
                            b.Append(r[j]);

                    }
                    b.Append("\r\n");
                }
                logic.Log.Log("MSTD00001INF", b.ToString(), "DataContents", ux.USERNAME, "9.1.1", pid);
            }


        }

        public bool GetGlAccountCostCenterFlag(int gla)
        {
            var x = Qx<int?>("GetGlAccountCostCenterFlag", new { GlAccount = gla });

            return (x.FirstOrDefault() ?? 0) == 1;


        }

        public bool isAccrDoc(string doc)
        {
            string code = doc.Substring(0, 2);
            if (code == "LA" || code == "BS" || code == "AE")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string getAccrCode(string doc)
        {
            string code = doc.Substring(0, 2);
            return code;
        }

        public List<WorklistData> GetWorklistByDoc(string doc, string sql)
        {
            string k = sql + "(" + doc + ")";
            string SQL = string.Format(logic.SQL[sql], logic.SQL["GetSplitByComma"]);
            List<WorklistData> h = Heap.Get<List<WorklistData>>(k);
            List<WorklistData> _q = new List<WorklistData>();
            if (h != null)
                return h;
            try
            {
                IDbConnection _db = logic.Cx.DB;
                if (!doc.isEmpty())
                {
                    DynamicParameters dp = new DynamicParameters();
                    dp.Add("@p1", doc);
                    _q = _db.Query<WorklistData>(SQL, dp, null, true, null, CommandType.Text).ToList();
                    Heap.Set<List<WorklistData>>(k, _q, Heap.Duration * 5);
                }
            }
            catch (Exception e)
            {
                LoggingLogic.say(sql, SQL + "\r\n@p1='{0}'\r\n", doc);
                LoggingLogic.err(e);
            }
            return _q;
        }

        public void FillWorklistData(List<WorklistData> w)
        {
            List<string> pv = new List<string>();
            List<string> rv = new List<string>();
            List<string> accr = new List<string>();
            List<string> accrList = new List<string>();
            List<string> accrShifting = new List<string>();
            List<string> accrExtend = new List<string>();

            for (int i = 0; i < w.Count; i++)
            {
                string reff = w[i].Folio.Trim();

                if (!isAccrDoc(w[i].Folio)) // if folio is not a accrued document
                {
                    int docNo = reff.Substring(0, reff.Length - 4).Int(0);
                    string _no = docNo.str();
                    if (docNo > 0 && docNo < 600000000)
                    {
                        pv.Add(_no);
                    }
                    else if (docNo >= 600000000)
                    {
                        rv.Add(_no);
                    }
                }
                else
                {
                    switch (getAccrCode(reff))
                    {
                        case "LA":
                            accrList.Add(reff);
                            break;
                        case "BS":
                            accrShifting.Add(reff);
                            break;
                        case "AE":
                            accrExtend.Add(reff);
                            break;
                        default:
                            accr.Add(reff);
                            break;
                    }
                }
            }
            string pvs = string.Join(",", pv.Distinct().ToArray());
            string rvs = string.Join(",", rv.Distinct().ToArray());

            string accrLists = string.Join(",", accrList.Distinct().ToArray());
            string accrShiftings = string.Join(",", accrShifting.Distinct().ToArray());
            string accrExtends = string.Join(",", accrExtend.Distinct().ToArray());

            List<WorklistData> q = new List<WorklistData>();

            q.AddRange(GetWorklistByDoc(pvs, "WorklistPV"));
            q.AddRange(GetWorklistByDoc(rvs, "WorklistRV"));

            q.AddRange(GetWorklistByDoc(accrLists, "WorklistAccruedList"));
            q.AddRange(GetWorklistByDoc(accrShiftings, "WorklistAccruedShifting"));
            q.AddRange(GetWorklistByDoc(accrExtends, "WorklistAccruedExtend"));

            if (q != null && q.Count > 0)
            {
                for (int i = 0; i < w.Count; i++)
                {
                    WorklistData d = q.Where(a => a.Folio == w[i].Folio).FirstOrDefault();
                    if (d != null)
                    {
                        BaseBusinessLogic.CopyWorklistData(w[i], d);
                    }
                }
            }

            for (int i = w.Count - 1; i >= 0; i--)
            {
                if (string.IsNullOrEmpty(w[i].StatusName))
                    w.RemoveAt(i);
            }
        }

        public string GetDivName(string folio, UserData userData)
        {
            string divName = "";

            string code = folio.Substring(0, 2);
            divName = logic.AccrList.GetDivNameFromFolio(code, folio, userData);

            return divName;
        }
        public string DownloadError(int _processId, string uploadedFile, string sourceFile, string targetDirectory, string[][] e)
        {
            string fileName =
                    Util.GetTmp(targetDirectory,
                    Path.GetFileNameWithoutExtension(CommonFunction.SanitizeFilename(uploadedFile))
                            + "_" + _processId.str() + "_error", ".txt");

            File.WriteAllText(fileName, string.Join("\r\n", _errs));

            return Path.GetFileName(fileName);
        }

        public DateTime GetDocDate(string no, string year)
        {
            DateTime DocDate = new DateTime();
            DateTime? dt = Qx<DateTime?>("GetDocDate", new { docNo = no, docYear = year }).FirstOrDefault();
            if (dt != null) DocDate = dt ?? new DateTime();

            return DocDate;
        }


        public long GetDocNo(byte DocCd, string username)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@doc_cd", DocCd);
            d.Add("@user", username);
            d.Add("@NO", null, DbType.Int32, ParameterDirection.Output);

            Exec("sp_GetDocNumber", d);

            long ID = d.Get<int>("@NO");
            return ID;
        }


        /* Start Rinda Rahayu 20160411 */
        public DateTime GetPlanningPaymentDate(int pvCode, int year)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@REFF_NO", pvCode + "" + year);
            d.Add("@MODULE_CD", null);
            d.Add("@PlanningPaymentDate", null, DbType.DateTime, ParameterDirection.Output);

            Exec("sp_GetPlanningPaymentDate", d);

            DateTime planningPaymentDate = d.Get<DateTime>("@PlanningPaymentDate");

            return planningPaymentDate;
        }
        /* End Rinda Rahayu 20160411 */

        public void fetchNote(PVFormData formData)
        {
            List<FormNote> l = Qu<FormNote>("GetListNoticeByDocNoYear", formData.PVNumber, formData.PVYear).ToList();
            if (l.Any())
            {
                formData.Notes.Clear();
                l.ForEach(x => formData.Notes.Add(x));
            }
        }

        //fid.Hadid
        public void fetchNote(BaseAccrData formData)
        {
            List<FormNote> l = Qu<FormNote>("GetListNoticeByDocNoYear", formData.ReffNo, formData.CreatedDt.Year).ToList();
            if (l.Any())
            {
                formData.Notes.Clear();
                l.ForEach(x => formData.Notes.Add(x));
            }
            //fid.pras
            else formData.Notes.Clear();
        }
        //end fid.Hadid
        public List<FormAttachment> getAttachments(string referenceNo)
        {
            return Qu<FormAttachment>("getAttachments", referenceNo).ToList();
        }

        public bool MoveAttachment(string src, decimal seq, string dst, string yy)
        {
            string olde = src + yy;
            string neo = dst + yy;
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;

                var q = (from a in db.TB_R_ATTACHMENT
                         where a.REFERENCE_NO == olde && a.REF_SEQ_NO == seq
                         select a).FirstOrDefault();
                if (q != null)
                {
                    string adir = "";
                    string[] a = q.DIRECTORY.Split('/');
                    if (a.Length > 0)
                    {
                        a[a.Length - 1] = dst;
                        adir = string.Join("/", a, 0, a.Length);
                    }

                    db.AddToTB_R_ATTACHMENT(new TB_R_ATTACHMENT()
                    {
                        REFERENCE_NO = neo,
                        REF_SEQ_NO = q.REF_SEQ_NO,
                        ATTACH_CD = q.ATTACH_CD,
                        DESCRIPTION = q.DESCRIPTION,
                        DIRECTORY = adir,
                        FILE_NAME = q.FILE_NAME,
                        CREATED_DT = DateTime.Now,
                        CREATED_BY = q.CREATED_BY
                    });
                    db.DeleteObject(q);

                    db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
        }

        public virtual PVFormData search(int pvNumber, int pvYear)
        {
            return null;
        }
        public virtual AccrFormData search(string refNo)
        {
            return null;
        }

        protected virtual List<PVFormDetail> GetDetail(PVFormData f, ELVIS_DBEntities db)
        {
            return new List<PVFormDetail>();
        }

        public void LoadOri(PVFormData f)
        {
            f.FormTable.DataList = GetOri(f, db);
        }

        protected List<PVFormDetail> GetOri(PVFormData f, ELVIS_DBEntities db)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailResult = logic.Base.GetOriginal(_no, _yy);

            if (!detailResult.Any())
                return l;

            PVFormTable formTable = f.FormTable;
            List<CodeConstant> lstCostCenterCodes = getCostCenterCodes();
            string standardDescriptionWording = null;
            PVFormDetail formDetail;
            int seqNumber = 0;

            string vendorName = getVendorCodeName(f.VendorCode);
            if (vendorName != null)
            {
                vendorName = vendorName.Trim();
            }
            string divisionName = getDivisionName(f.DivisionID);
            foreach (var detail in detailResult)
            {
                if (standardDescriptionWording == null)
                {
                    standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
                }

                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = Math.Round(detail.AMOUNT),
                    CostCenterCode = detail.COST_CENTER_CD,
                    CostCenterName = getCostCenterDescription(detail.COST_CENTER_CD),
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    //WbsNumber = detail.WBS_NO,
                    InvoiceNumber = detail.INVOICE_NO,
                    GlAccount = detail.GL_ACCOUNT.str(),

                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD,

                    #region EFB
                    WHT_TAX_CODE = detail.WHT_TAX_CODE,
                    WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF,
                    WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO,
                    WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT
                    #endregion
                };

                if (formDetail.ItemTransaction == null)
                {
                    formDetail.ItemTransaction = 1;
                }

                formDetail.InvoiceNumberUrl = string.Format(
                    "/50Invoice/InvoiceListDet.aspx?invoiceNo={0}"
                    + "&invoiceDate={1}&vendorCode={2}&vendorName={3}"
                    + "&division={4}&invoiceStatus={5}&pvNo={6}&pvYear={7}",
                    formDetail.InvoiceNumber,
                    string.Format("{0:dd MMM yyyy}", detail.INVOICE_DATE),
                    f.VendorCode, vendorName, divisionName, f.StatusCode,
                    f.PVNumber, f.PVYear);

                formDetail.CostCenterName = (from a in lstCostCenterCodes
                                             where a.Code == formDetail.CostCenterCode
                                             select a.Description).FirstOrDefault();
                l.Add(formDetail);
            }
            return l;
        }

        public virtual bool save(PVFormData formData, UserData userData, bool financeHeader)
        {
            string fn = ((formData.PVNumber ?? 0) == 0 || (formData.PVYear ?? 0) == 0) ?
                    formData.ScreenType + "_%TIME%_%MS%" + "save"
                    : string.Format("{0}{1}", formData.PVNumber, formData.PVYear);

            System.IO.File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), fn, ".txt")
                , formData.dump());

            return true;
        }
        public virtual bool save(AccrFormData formData, UserData userData)
        {
            string fn = (formData.AccruedNo.isEmpty() ?
                    "Accrued_%TIME%_%MS%" + "save"
                    : string.Format("{0}", formData.AccruedNo));

            System.IO.File.WriteAllText(Util.GetTmp(LoggingLogic.GetPath(), fn, ".txt")
                , formData.dump());

            return true;
        }

        public virtual Dictionary<string, decimal> GetSuspenseAmounts(int pvNo, int pvYear)
        {
            return null;
        }

        protected string cat(string x, string y)
        {
            return ((x ?? "") + " " + (y ?? "")).Trim();
        }

        public virtual string GetSummary(UserData u, string _imagePath, string template, int? num, int? year)
        {
            return null;
        }

        public virtual string WriteSummary(string imagePath, string outputPath,
                              UserData _UserData,
                              string contentTemp)
        {
            return null;
        }

        public virtual bool Reverse(string uid, PVFormData f)
        {
            return true;
        }

        public virtual bool Reverse(string uid, AccrFormData f)
        {
            return true;
        }

        public virtual bool BudgetCheckReverse(string uid, PVFormData d, ref string m)
        {
            if (Common.AppSetting.SKIP_BUDGET_CHECK || !Common.AppSetting.BUDGET_CHECK_REVERSE)
            {
                return true;
            }
            if (d.BudgetNumber.isEmpty()) return true;
            int fiscalYear = logic.Sys.FiscalYear(d.PVDate ?? new DateTime(StrTo.NULL_YEAR, 1, 1));
            //BudgetCheckInput iBu = new BudgetCheckInput()
            //{
            //    DOC_NO = d.PVNumber ?? 0,
            //    //DOC_YEAR = d.PVYear ?? 0,
            //    DOC_YEAR = fiscalYear,
            //    DOC_STATUS = (int)BudgetCheckStatus.Reverse,
            //    WBS_NO = d.BudgetNumber,
            //    AMOUNT = Math.Round((decimal)d.GetTotalAmount())
            //};
            BudgetPostInput iBu = new BudgetPostInput()
            {
                TransactionNo = d.PVNumber.ToString() ?? "0",
                ItemNo = 1,
                Qty = 0,
                //Description = x.PV_TYPE_NAME + "-" + x.TRANSACTION_NAME,
                Description = d.Notes.ToString(),
                WBS = d.BudgetNumber,
                Year = fiscalYear.ToString(),
                Amount = Math.Round((decimal)d.GetTotalAmount()),
                System = "ELvis",
                Reference = "",
                ReferenceItemNo = "",
                Status = "Reverse"
            };
            //BudgetCheckResult o = logic.SAP.BudgetCheck(iBu, UserName);
            BudgetCheckResult o = logic.SAP.BudgetReverseTalend(iBu, UserName);
            m = o.MESSAGE;
            return (o.STATUS.Equals("S"));
        }

        public bool WaitStatusChanged(int no, int year, int DocCd, int Status, int timeout = 10)
        {
            int status = 0;
            int x = 0;
            if (no == 0 || year == 0 || timeout > 60000) return false;

            status = Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();

            while (status == Status && x < timeout)
            {
                status = Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();

                System.Threading.Thread.Sleep(500);
                x++;
            }

            return status != Status;
        }

        public UserData Sup(UserData x)
        {
            string DCD = x.DIV_CD.str();
            string DpCD = x.DEPARTMENT_ID;
            string uid = x.USERNAME;
            int CLASS = 1;
            string SID = null;
            UserData o = new UserData();

            var q = (from u in db.vw_User
                     where u.USERNAME != null && u.USERNAME == uid
                     select u).FirstOrDefault();
            if (q != null)
            {
                CLASS = q.CLASS ?? 0;
                SID = q.SECTION_ID;
            }

            var r = (from y in db.vw_User
                     where (SID == null || (y.SECTION_ID != null && y.SECTION_ID == SID))
                     && (DpCD == null || (y.DEPARTMENT_ID != null && y.DEPARTMENT_ID == DpCD))
                     && (DCD == null || (y.DIVISION_ID != null && y.DIVISION_ID == DCD))
                     && (y.CLASS != null && y.CLASS > CLASS)
                     select y).FirstOrDefault();


            if (r != null)
            {
                o.FIRST_NAME = r.FIRST_NAME;
                o.LAST_NAME = r.LAST_NAME;
                o.USERNAME = r.USERNAME;
                o.DIV_CD = r.DIVISION_ID.Dec();
                o.DEPARTMENT_ID = r.DEPARTMENT_ID;
                o.TITLE = r.TITLE;
            }

            return o;
        }


        public void save(int no, int yy, OneTimeVendorData otv)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var q = from o in db.TB_R_ONE_TIME_VENDOR
                        where o.PV_NO == no
                        && o.PV_YEAR == yy
                        select o;

                TB_R_ONE_TIME_VENDOR v = null;
                bool isNew = false;
                if (q.Any())
                {
                    v = q.FirstOrDefault();
                }
                else
                {
                    isNew = true;
                    v = new TB_R_ONE_TIME_VENDOR()
                    {
                        PV_NO = no,
                        PV_YEAR = yy,
                    };
                }
                v.TITLE = otv.TITLE;
                v.NAME1 = otv.NAME1;
                v.NAME2 = otv.NAME2;
                v.STREET = otv.STREET;
                v.CITY = otv.CITY;

                v.BANK_KEY = otv.BANK_KEY;

                v.BANK_ACCOUNT = otv.BANK_ACCOUNT;
                if (isNew)
                {
                    db.AddToTB_R_ONE_TIME_VENDOR(v);
                }
                db.SaveChanges();
            }
        }

        public void saveOri(PVFormData formData, UserData userData, ELVIS_DBEntities db)
        {
            // get Existing 
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;
            var q = from oa in db.TB_R_ORIGINAL_AMOUNT
                    where oa.DOC_NO == _no && oa.DOC_YEAR == _yy
                    select oa;

            if (q.Any())
            {
                List<OriginalAmountData> oa = new List<OriginalAmountData>();
                List<OriginalAmountData> oadd = new List<OriginalAmountData>();

                foreach (var o in q.ToList())
                {
                    PVFormDetail fd = formData.Details
                        .Where(a => a.SequenceNumber == o.SEQ_NO)
                        .FirstOrDefault();

                    if (fd == null)
                    {
                        oa.Add(new OriginalAmountData()
                        {
                            DOC_NO = o.DOC_NO,
                            DOC_YEAR = o.DOC_YEAR,
                            SEQ_NO = o.SEQ_NO
                        });
                    }
                }

                // remove existing 
                foreach (OriginalAmountData o in oa)
                {
                    var d = (from ox in db.TB_R_ORIGINAL_AMOUNT
                             where ox.DOC_YEAR == _yy
                             && ox.DOC_NO == _no
                             && ox.SEQ_NO == o.SEQ_NO
                             select ox).FirstOrDefault();
                    if (d != null)
                        db.DeleteObject(d);
                }
            }

            List<ExchangeRate> le = getExchangeRates();

            foreach (PVFormDetail d in formData.Details)
            {
                int _seq = d.SequenceNumber;

                var x = (from ox in db.TB_R_ORIGINAL_AMOUNT
                         where ox.DOC_YEAR == _yy
                         && ox.DOC_NO == _no
                         && ox.SEQ_NO == _seq
                         select ox).FirstOrDefault();

                decimal er = (decimal)(from e in le
                                       where e.CurrencyCode == d.CurrencyCode
                                       select e.Rate).FirstOrDefault();
                LoggingLogic.say(_no.str(), "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
                     _no,
                     _yy,
                     _seq,
                     d.CurrencyCode,
                     er,
                    Math.Round(d.Amount),
                     0,
                     0,
                     d.CostCenterCode,
                     d.Description,
                     d.InvoiceNumber,
                     d.ItemTransaction,
                     d.GlAccount,
                     d.TaxNumber,
                     d.TaxCode,
                     userData.USERNAME,
                     today,
                     d.WHT_TAX_CODE,
                     d.WHT_TAX_TARIFF,
                     d.WHT_TAX_ADDITIONAL_INFO,
                     d.WHT_DPP_PPh_AMOUNT
                    );
                if (x != null)
                {
                    x.CURRENCY_CD = d.CurrencyCode;
                    x.EXCHANGE_RATE = er;
                    x.AMOUNT = Math.Round((decimal)d.Amount);
                    x.SPENT_AMOUNT = 0;
                    x.SUSPENSE_AMOUNT = 0;

                    x.COST_CENTER_CD = d.CostCenterCode;
                    x.DESCRIPTION = d.Description;
                    x.INVOICE_NO = d.InvoiceNumber;
                    x.ITEM_TRANSACTION_CD = d.ItemTransaction;
                    x.GL_ACCOUNT = d.GlAccount.isEmpty() ?
                            GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0)
                            : d.GlAccount.Int();
                    x.TAX_NO = d.TaxNumber;
                    x.TAX_CD = d.TaxCode;

                    x.CHANGED_BY = userData.USERNAME;
                    x.CHANGED_DT = today;
                    #region EFB
                    x.WHT_TAX_CODE = d.WHT_TAX_CODE;
                    x.WHT_TAX_TARIFF = d.WHT_TAX_TARIFF;
                    x.WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO;
                    x.WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT;
                    #endregion
                    db.SaveChanges();
                }
                else
                {
                    db.AddToTB_R_ORIGINAL_AMOUNT(
                        new TB_R_ORIGINAL_AMOUNT()
                        {
                            DOC_NO = _no,
                            DOC_YEAR = _yy,
                            SEQ_NO = _seq,
                            CURRENCY_CD = d.CurrencyCode,
                            EXCHANGE_RATE = er,
                            AMOUNT = Math.Round((decimal)d.Amount),
                            SPENT_AMOUNT = 0,
                            SUSPENSE_AMOUNT = 0,
                            COST_CENTER_CD = d.CostCenterCode,
                            DESCRIPTION = d.Description,
                            INVOICE_NO = d.InvoiceNumber,
                            ITEM_TRANSACTION_CD = d.ItemTransaction,
                            GL_ACCOUNT = d.GlAccount.isEmpty() ?
                                GLAccount(formData.TransactionCode ?? 0, d.ItemTransaction ?? 0)
                                : d.GlAccount.Int(),
                            TAX_NO = d.TaxNumber,
                            TAX_CD = d.TaxCode,
                            CREATED_BY = userData.USERNAME,
                            CREATED_DT = today,

                            #region EFB
                            WHT_TAX_CODE = d.WHT_TAX_CODE,
                            WHT_TAX_TARIFF = d.WHT_TAX_TARIFF,
                            WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO,
                            WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT
                            #endregion
                        });
                }
                db.SaveChanges();
            }
        }


        public virtual List<BudgetCheckInput> GetBudgetNumberHistory(int _no, int _year, string BudgetNo)
        {
            return new List<BudgetCheckInput>();
        }

        public List<PVFormDetail> GetPV(PVFormData f, ELVIS_DBEntities db)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailQuery = from t in db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
            var detailResult = detailQuery.ToList();
            if (!detailResult.Any())
            {
                _yy--;
                detailQuery = from t in db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
                detailResult = detailQuery.ToList();
                if (!detailResult.Any())
                {
                    _yy += 2;
                    detailQuery = from t in db.TB_R_PV_D
                                  where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                                  select t;
                    detailResult = detailQuery.ToList();
                }
            }
            if (!detailResult.Any()) return l;

            PVFormTable formTable = f.FormTable;
            string standardDescriptionWording = null;
            PVFormDetail formDetail;
            int seqNumber = 0;

            string vendorName = getVendorCodeName(f.VendorCode);
            if (vendorName != null)
            {
                vendorName = vendorName.Trim();
            }
            string divisionName = getDivisionName(f.DivisionID);
            foreach (var detail in detailResult)
            {
                if (standardDescriptionWording == null)
                {
                    standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
                }

                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = detail.AMOUNT,
                    CostCenterCode = detail.COST_CENTER_CD,
                    CostCenterName = getCostCenterDescription(detail.COST_CENTER_CD),
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    InvoiceNumber = detail.INVOICE_NO,
                    GlAccount = detail.GL_ACCOUNT.str(),
                    FromCurr = detail.FROM_CURR,
                    FromSeq = detail.FROM_SEQ,
                    ItemNo = detail.ITEM_NO,
                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    InvoiceDate = detail.INVOICE_DATE,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD,
                    #region EFB
                    WHT_TAX_CODE = detail.WHT_TAX_CODE,
                    WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF,
                    WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO,
                    WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT
                    #endregion
                };

                if (formDetail.ItemTransaction == null)
                {
                    formDetail.ItemTransaction = 1;
                }

                formDetail.InvoiceNumberUrl = string.Format(
                    "/50Invoice/InvoiceListDet.aspx?invoiceNo={0}"
                    + "&invoiceDate={1}&vendorCode={2}&vendorName={3}"
                    + "&division={4}&invoiceStatus={5}&pvNo={6}&pvYear={7}",
                    formDetail.InvoiceNumber,
                    string.Format("{0:dd MMM yyyy}", detail.INVOICE_DATE),
                    f.VendorCode, vendorName, divisionName, f.StatusCode,
                    f.PVNumber, f.PVYear);
                //formDetail.CostCenterName = getCostCenterDescription(formDetail.CostCenterCode);
                l.Add(formDetail);
            }

            return l;
        }

        private string DescCurrRate(string desc, string curr, decimal Rate)
        {
            string d = "";
            if (!desc.isEmpty())
                d = desc;
            if (d.Length > 20)
                d = d.Substring(0, 20);
            d += " | " + curr + " " + CommonFunction.Eval_Curr("IDR", Rate);
            return d;
        }

        private string DescCashierPolicy(string curr, decimal original, decimal rate)
        {
            return string.Format("{0} {1} | Rate:{2:N2}", curr, CommonFunction.Eval_Curr(curr, original), rate);
        }

        public void CashierPolicy(List<PVFormDetail> d,
            bool leaveZero = false,
            int docNo = 0, int docYear = 0,
            List<ExchangeRate> exchangeRates = null,
            int MaxSeq = 0,
            int exRateDocType = 2,
            int PVTypeCode = 1)
        {
            if (d == null || d.Count < 1) // never work on empty list
                return;

            List<ExchangeRate> rates = null;
            if (exchangeRates == null)
                rates = logic.Look.GetCashierPolicyExchangeRates(exRateDocType);
            else
                rates = exchangeRates;

            Dictionary<string, decimal> amo = FormTableHelper.AmountByCurrency(d);
            Dictionary<string, decimal> capo = new Dictionary<string, decimal>();
            CashierPolicyList capol = new CashierPolicyList();
            string[] RoundCurrency = logic.Sys.RoundCurrencies();
            foreach (string k in amo.Keys)
            {
                ExchangeRate rate = (from e in rates select e)
                        .Where(x => x.CurrencyCode == k)
                        .FirstOrDefault();
                if (rate != null && rate.AmountMin != null && rate.AmountMin > 0)
                {
                    decimal amt = amo[k];
                    decimal dn = Math.Floor(amt / rate.AmountMin ?? 1);
                    decimal dr = amt - dn * (rate.AmountMin ?? 1);

                    capol.Add(k, amt - dr, 1);
                    capol.Add(k, dr, rate.Rate);
                }
                else
                {
                    capol.Add(k, amo[k], 1);
                }
            }

            var qCp = from x in capol.Data
                      select new
                      {
                          Currency = (x.Rate != 1) ? "IDR" : x.Curr,
                          Amount = (x.Rate == 1) ? x.Amount : (x.Amount * x.Rate)
                      };
            if (qCp.Any())
            {
                var qCpSum = qCp.GroupBy(a => a.Currency)
                    .Select(b => new
                    {
                        CURRENCY_CD = b.Key,
                        AMOUNT = b.Sum(x => x.Amount)
                    });
                if (qCpSum.Any())
                {
                    foreach (var cps in qCpSum)
                    {
                        capo[cps.CURRENCY_CD] = cps.AMOUNT;
                    }
                }
            }
            /// do value comparison from summarized 
            /// apply policy
            List<CashierPolicyData> Rp = (from g in capol.Data where g.Rate != 1 select g).ToList();
            List<PVFormDetail> neuD = new List<PVFormDetail>();
            int maxSeq = (from dx in d select dx.SequenceNumber).Max();
            if (MaxSeq > 0)
                maxSeq = MaxSeq;

            List<int> delSeq = new List<int>();

            foreach (CashierPolicyData rp in Rp)
            {
                foreach (PVFormDetail de in
                                      d.Where(x => x.CurrencyCode == rp.Curr)
                                        .OrderByDescending(xx => xx.Amount))
                {
                    if (rp.Amount <= 0) break;
                    decimal diff = 0;
                    int fseq = de.SequenceNumber;
                    //if (skipList != null && skipList.Contains(fseq))
                    //    continue;
                    decimal originalAmount = rp.Amount;

                    if (de.Amount > rp.Amount)
                    {
                        diff = de.Amount - rp.Amount;
                        de.Amount = diff;

                        rp.Policy = rp.Policy + rp.Amount;
                        rp.Amount = 0;
                    }
                    else if (de.Amount == rp.Amount)
                    {
                        diff = de.Amount;
                        de.Amount = 0;
                        rp.Policy = rp.Policy + diff;
                        rp.Amount = 0;

                        delSeq.Add(de.SequenceNumber);
                    }
                    else
                    {
                        diff = de.Amount;
                        de.Amount = 0;

                        delSeq.Add(de.SequenceNumber);
                        rp.Policy = rp.Policy + diff;
                        rp.Amount = rp.Amount - diff;
                    }

                    if (PVTypeCode != 3)
                        rp.Total = Math.Round(rp.Policy * rp.Rate); // 20140102 wot.daniel + /// rounding to prevent error saving 
                    else
                        rp.Total = rp.Policy * rp.Rate;  /// 20140117 wot.daniel + do not round for Settlement - prevent posting error 

                    ++maxSeq;
                    de.FromSeq = fseq;
                    de.Persisted = false;
                    neuD.Add(new PVFormDetail()
                    {
                        SequenceNumber = maxSeq,
                        FromSeq = fseq,
                        CurrencyCode = "IDR",
                        FromCurr = rp.Curr,
                        Amount = rp.Total,
                        Description = DescCashierPolicy(rp.Curr, originalAmount, rp.Rate),
                        CostCenterCode = de.CostCenterCode,
                        CostCenterName = de.CostCenterName,
                        GlAccount = de.GlAccount,
                        ItemTransaction = de.ItemTransaction,
                        InvoiceNumber = de.InvoiceNumber,
                        StandardDescriptionWording = de.StandardDescriptionWording,
                        TaxCode = de.TaxCode,
                        TaxNumber = de.TaxNumber,
                        Persisted = false
                    });
                }
            }
            delSeq = delSeq.Distinct().ToList();
            d.AddRange(neuD);
            if (!leaveZero)
                for (int i = d.Count - 1; i >= 0; i--)
                {
                    if (delSeq.Contains(d[i].SequenceNumber))
                    {
                        d.RemoveAt(i);
                    }
                }
        }

        public void ReSequence(List<PVFormDetail> d, List<PVFormDetail> suspenses, List<PVFormDetail> spent = null)
        {
            int lastSu = suspenses.Select(a => a.SequenceNumber).Max();
            int lastSe = 0;
            if (spent != null)
            {
                lastSe = spent.Select(b => b.SequenceNumber).Max();
            }
            int seqMax = Math.Max(lastSu, lastSe);

            for (int i = 0; i < d.Count; i++)
            {
                PVFormDetail se = d[i];
                if (se.FromSeq != null && se.SequenceNumber != se.FromSeq)
                {
                    PVFormDetail su = (from x in suspenses
                                       where x.FromSeq != null
                                       && x.FromSeq == se.FromSeq
                                       && x.SequenceNumber != x.FromSeq
                                       select x).FirstOrDefault();

                    if (su != null)
                    {
                        if (su.SequenceNumber != se.SequenceNumber) /// skip already correct 
                        {

                            /// find similar sequence number on d 
                            /// when found assign other sequence number on sequence when it is not found on s 

                            PVFormDetail lastD = (from y in d where y.SequenceNumber == su.SequenceNumber select y).FirstOrDefault();
                            if (lastD != null)
                            {
                                seqMax++;
                                lastD.SequenceNumber = seqMax;
                            }
                            if (!se.Equals(lastD))
                            {
                                se.SequenceNumber = su.SequenceNumber;
                            }
                        }
                    }
                    else if (spent != null)
                    {
                        PVFormDetail stl = (from u in spent
                                            where u.FromSeq != null
                                            && u.FromSeq == se.FromSeq
                                            && u.SequenceNumber != u.FromSeq
                                            select u).FirstOrDefault();
                        if (stl != null)
                        {
                            if (stl.SequenceNumber != se.SequenceNumber) // skip already correct seq no
                            {
                                /// find similar sequence number on d 
                                /// when found assign other sequence number on sequence when it is not found on s 

                                PVFormDetail lu = (from z in d where z.SequenceNumber == stl.SequenceNumber select z).FirstOrDefault();
                                if (lu != null)
                                {
                                    seqMax++;

                                    lu.SequenceNumber = seqMax; /// move non available to new ly assigned number 
                                }
                                if (!se.Equals(lu))
                                {
                                    se.SequenceNumber = stl.SequenceNumber;
                                }
                            }
                        }
                        else
                        {
                            /// just put it into new seq num 
                            seqMax++;
                            se.SequenceNumber = seqMax;
                        }
                    }
                }
            }
        }

        public bool FillNonCashierPolicySettlement(List<PVFormDetail> setl, List<PVFormDetail> sus)
        {
            if (setl == null || setl.Count < 1 || sus == null || sus.Count < 1)
                return false;

            /// find settlement that has original currency but has no idr cashier policy amount 
            List<int> toAdd = new List<int>();
            List<int> SusSeq = new List<int>();

            for (int i = 0; i < setl.Count; i++)
            {
                PVFormDetail a = setl[i];
                if (!a.FromCurr.isEmpty() || a.CurrencyCode == "IDR")
                    continue;
                bool hasCashierPolicy = false;
                for (int j = i + 1; j < setl.Count; j++)
                {
                    if (setl[j].FromCurr == a.CurrencyCode && setl[j].FromSeq == a.SequenceNumber)
                    {
                        hasCashierPolicy = true;
                        break;
                    }
                }
                if (!hasCashierPolicy)
                {
                    /// find if suspense has Cashier policy, if it does, add 
                    var SuspenseSeq = sus.Where(s => s.FromCurr == a.CurrencyCode && s.FromSeq == a.SequenceNumber);
                    if (SuspenseSeq.Any())
                    {
                        toAdd.Add(i);
                        SusSeq.Add(SuspenseSeq.FirstOrDefault().SequenceNumber);
                    }
                }
            }
            /// fill from suspense 
            if (toAdd.Count > 0)
            {
                for (int x = 0; x < toAdd.Count; x++)
                {
                    PVFormDetail ax = setl[toAdd[x]];
                    PVFormDetail fd = new PVFormDetail();
                    fd.Copy(ax);

                    fd.CurrencyCode = "IDR";
                    fd.Amount = 0;
                    fd.FromCurr = ax.CurrencyCode;
                    fd.FromSeq = ax.SequenceNumber;
                    ax.FromSeq = ax.SequenceNumber;
                    ax.Persisted = false;
                    fd.SequenceNumber = SusSeq[x];
                    fd.Persisted = false;
                    setl.Add(fd);
                }
            }
            return true;

        }

        protected int GetSettlementMaxSeqNo(ELVIS_DBEntities db, int suspenseNo, int suspenseYear)
        {
            var q = from a in db.TB_R_SET_FORM_D
                    where a.PV_SUS_NO == suspenseNo && a.PV_YEAR == suspenseYear
                    select a.SEQ_NO;
            int r = 0;
            if (q.Any())
            {
                r = q.Max();
            }
            return r;
        }

        protected int ReplaceSettlement(ELVIS_DBEntities db,
                int? suspenseNo, int? suspenseYear,
                int docNo, int docYear,
                List<PVFormDetail> leSettle,
                string userName)
        {
            DateTime today = DateTime.Now;
            int maxSeq = 0;

            foreach (var d in leSettle)
            {

                if (d.SequenceNumber > maxSeq)
                    maxSeq = d.SequenceNumber;

                var q = from sde in db.TB_R_SET_FORM_D
                        where sde.PV_SUS_NO == suspenseNo
                          && sde.PV_YEAR == suspenseYear
                          && sde.SEQ_NO == d.SequenceNumber
                        select sde;
                if (q.Any())
                {
                    var x = q.FirstOrDefault();
                    if (x != null)
                    {
                        x.FROM_SEQ = d.FromSeq;
                        x.SPENT_AMOUNT = d.Amount;
                        x.SETTLEMENT_NO = docNo;
                        x.SETTLEMENT_YEAR = docYear;
                        x.CHANGED_BY = userName;
                        x.CHANGED_DT = today;
                    }
                }
                else
                {
                    db.AddToTB_R_SET_FORM_D(new TB_R_SET_FORM_D()
                    {
                        PV_SUS_NO = suspenseNo ?? 0,
                        PV_YEAR = suspenseYear ?? 0,
                        SEQ_NO = d.SequenceNumber,
                        FROM_SEQ = d.FromSeq,
                        SETTLEMENT_YEAR = docYear,
                        SETTLEMENT_NO = docNo,
                        SPENT_AMOUNT = d.Amount,
                        CREATED_DT = today,
                        CREATED_BY = userName
                    });
                }
            }
            db.SaveChanges();
            return maxSeq;
        }

        #region simulate

        protected readonly string notBalance = "Total Amount Header & Detail is not balance";
        protected readonly string noWitholding = "This transaction should have witholding tax";
        protected readonly string notVerified = "Hardcopy Document has not been Verified by Counter";


        public virtual List<PVFormSimulationData> GetSimulationData(String docNo, String docYear)
        {
            return null;
        }
        public virtual List<AccrFormSimulationData> GetSimulationData(String refNo)
        {
            return null;
        }

        #endregion

        public virtual void updateBalanceOutstanding(PVFormData formData, UserData userData, bool isSubmit)
        {
            return;
        }

        public virtual void updateBalanceVoucherPosting(PVFormData formData, UserData userData)
        {
            return;
        }

        public int GetLastStatusDocument(int no, int year, int DocCd)
        {
            return Qx<int>("GetLastStatusByDocNo", new { docCd = DocCd, no = no, yy = year }).FirstOrDefault();
        }

        public void SetCancelFlag(string accrNo, UserData userData, int code)
        {
            Qx<int>("SetCancelFlagAccr", new { code = code, UserName = userData.USERNAME, accrNo = accrNo }).First();
        }

        public List<OutstandingSuspense> GetUnsettledByDivision(string DivisionId)
        {
            return Qu<OutstandingSuspense>("GetUnsettledByDivision", DivisionId).ToList();
        }


        //TODO : Imron SP Custom
        //public Int64 saveAttachmentFakturPajakInfo(FormAttachmentFakturPajak formAttachment, UserData userData)
        //{
        //    Int64 result = 0;
        //    try
        //    {


        //        Int64 saveResult = 0;

        //        DynamicParameters d = new DynamicParameters();
        //        d.Add("@ReferenceNumber", formAttachment.ReferenceNumber);
        //        d.Add("@Description", formAttachment.Description);
        //        d.Add("@PATH", formAttachment.PATH);
        //        d.Add("@FileName", formAttachment.FileName);
        //        d.Add("@Status", formAttachment.Status);
        //        d.Add("@Url", formAttachment.UrlXml);
        //        d.Add("@ErrMessage", formAttachment.ErrMessage);
        //        d.Add("@UserName", userData.USERNAME);
        //        d.Add("@ID", saveResult, DbType.Int64, ParameterDirection.Output);

        //        Exec("sp_UploadFakturPajak", d);

        //        saveResult = d.Get<Int64>("@ID");
        //        result = saveResult;


        //    }
        //    catch (Exception ex)
        //    {
        //        result = 0;
        //        Handle(ex);
        //    }
        //    return result;
        //}

        //public void deleteAttachmentFakturPajakInfo(FormAttachmentFakturPajak formAttachment)
        //{
        //    Do("deleteAttachmentFakturPajakInfo", new { reffNo = formAttachment.ReferenceNumber, Filename = formAttachment.FileName });

        //}
        //public List<AttachmentType> GetAttachmentType(int _TRANSACTION_CD, string _DIVISION, int _groupSeq)
        //{
        //    List<AttachmentType> l = new List<AttachmentType>();
        //    DynamicParameters d = new DynamicParameters();
        //    d.Add("@TRANSACTION_CD", _TRANSACTION_CD);
        //    d.Add("@DIVISION", _DIVISION);
        //    d.Add("@groupSeq", _groupSeq);
        //    l = executeProcedureQuery<AttachmentType>("sp_GetAttachmentType", d).ToList();
        //    return l;
        //}



        public List<SelectedCategory> GetSelectedCategory(int division, int? transactionCode = 0)
        {
            List<SelectedCategory> l = new List<SelectedCategory>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@transactionCode", transactionCode);
            d.Add("@division", division);

            l = executeProcedureQuery<SelectedCategory>("sp_GetSelectedCategory", d).ToList();
            return l;
        }

        public List<CategoryNominativeData> GetSelectedCategoryNominative(string ReferenceNo, int? transactionCode = 0)
        {
            List<CategoryNominativeData> l = new List<CategoryNominativeData>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@transactionCode", transactionCode);
            d.Add("@referenceNo", ReferenceNo);

            l = executeProcedureQuery<CategoryNominativeData>("sp_GetSelectedCategoryNominative", d).ToList();
            return l;
        }


        public List<Question> GetQuestion(string CategoryCode, Int32 TransactionCode, string ReferenceNo)
        {
            List<Question> l = new List<Question>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@categoryCode", CategoryCode);
            d.Add("@transactionCode", TransactionCode);
            d.Add("@referenceNo", ReferenceNo);

            l = executeProcedureQuery<Question>("sp_GetQuestion", d).ToList();
            return l;
        }


        public void AddQuestion(string CategoryCode, Int32 TransactionCode, string ReferenceNo, IList<QuestionDetail> questionDetail, DateTime createDt, string username)
        {
            //https://medium.com/dapper-net/sql-server-specific-features-2773d894a6ae
            //https://dev.to/zoltanhalasz/bulk-insert-with-dapper-with-table-type-parameter-45mp

            foreach (var val in questionDetail)
            {
                DynamicParameters d = new DynamicParameters();
                d.Add("@categoryCode", CategoryCode);
                d.Add("@transactionCode", TransactionCode);
                d.Add("@referenceNo", ReferenceNo);
                //d.Add("@tpQuestion", createDetailTable(questionDetail).at);
                // d.Add("@tpQuestion",   createDetailTable(questionDetail));
                d.Add("@questionId", val.QuestionId);
                d.Add("@isYes", val.IsYes);
                d.Add("@createDt", createDt);
                d.Add("@createBy", username);

                ExecData("sp_Add_Question", d);
            }


        }


        public List<Promotion> GetPromotion(string CategoryCode, Int32 TransactionCode, string ReferenceNo)
        {
            List<Promotion> l = new List<Promotion>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@categoryCode", CategoryCode);
            d.Add("@transactionCode", TransactionCode);
            d.Add("@referenceNo", ReferenceNo);

            l = executeProcedureQuery<Promotion>("sp_GetPromotion", d).ToList();
            return l;
        }

        public List<PromotionReport> GetPromotionReport(string CategoryCode, Int32 TransactionCode, string ReferenceNo)
        {
            List<PromotionReport> l = new List<PromotionReport>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@categoryCode", CategoryCode);
            d.Add("@transactionCode", TransactionCode);
            d.Add("@referenceNo", ReferenceNo);

            l = executeProcedureQuery<PromotionReport>("sp_GetPromotionReport", d).ToList();
            return l;
        }

        public List<PromotionReportXls> GetPromotionReportXls(string CategoryCode, Int32 TransactionCode, string ReferenceNo)
        {
            List<PromotionReportXls> l = new List<PromotionReportXls>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@categoryCode", CategoryCode);
            d.Add("@transactionCode", TransactionCode);
            d.Add("@referenceNo", ReferenceNo);

            l = executeProcedureQuery<PromotionReportXls>("sp_GetPromotionReportXls", d).ToList();
            return l;
        }

        public List<PromotionDetail> GetPromotionDetail(Int64? seqNo = 0)
        {
            List<PromotionDetail> l = new List<PromotionDetail>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@seqNo", seqNo);


            l = executeProcedureQuery<PromotionDetail>("sp_GetPromotionDetail", d).ToList();
            return l;
        }

        public Int64 AddPromotion(Promotion promotion)
        {
            Int64 result = 0;
            try
            {
                Int64 saveResult = 0;
                DynamicParameters d = new DynamicParameters();
                d.Add("@CATEGORY_CODE", promotion.CATEGORY_CODE);
                d.Add("@TRANSACTION_CD", promotion.TRANSACTION_CD);
                d.Add("@REFERENCE_NO", promotion.REFERENCE_NO);
                d.Add("@NO", promotion.NO);
                d.Add("@NAMA", promotion.NAMA);
                d.Add("@NPWP", promotion.NPWP);
                d.Add("@ALAMAT", promotion.ALAMAT);

                d.Add("@CREATED_DT", promotion.CREATED_DT);
                d.Add("@CREATED_BY", promotion.CREATED_BY);
                d.Add("@SeqNo", saveResult, DbType.Int64, ParameterDirection.Output);
                ExecData("sp_CreatePromotion", d);
                saveResult = d.Get<Int64>("@SeqNo");
                result = saveResult;


            }
            catch (Exception ex)
            {
                result = 0;
                Handle(ex);
            }
            return result;
        }

        public Int64 AddPromotionMasterDetail(Promotion promotion, IList<PromotionDetail> promotionDetails)
        {
            Int64 result = 0;
            try
            {
                Int64 saveResult = 0;
                DynamicParameters d = new DynamicParameters();
                d.Add("@CATEGORY_CODE", promotion.CATEGORY_CODE);
                d.Add("@TRANSACTION_CD", promotion.TRANSACTION_CD);
                d.Add("@REFERENCE_NO", promotion.REFERENCE_NO);
                d.Add("@NO", promotion.NO);
                d.Add("@NAMA", promotion.NAMA);
                d.Add("@NPWP", promotion.NPWP);
                d.Add("@ALAMAT", promotion.ALAMAT);

                d.Add("@CREATED_DT", promotion.CREATED_DT);
                d.Add("@CREATED_BY", promotion.CREATED_BY);
                d.Add("@SeqNo", saveResult, DbType.Int64, ParameterDirection.Output);
                ExecData("sp_CreatePromotion", d);
                saveResult = d.Get<Int64>("@SeqNo");
                foreach (PromotionDetail item in promotionDetails.Where(x => x.NO == promotion.NO).ToList())
                {
                    item.SEQ_NO = saveResult;
                    item.CREATED_DT = promotion.CREATED_DT;
                    item.CREATED_BY = promotion.CREATED_BY;
                    AddPromotionDetail(item);
                }
                result = saveResult;


            }
            catch (Exception ex)
            {
                result = 0;
                Handle(ex);
            }
            return result;
        }

        public void AddPromotionDetail(PromotionDetail promotionDetail)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@seqNo", promotionDetail.SEQ_NO);
            d.Add("@TANGGAL", promotionDetail.TANGGAL);
            d.Add("@BENTUK_DAN_JENIS_BIAYA", promotionDetail.BENTUK_DAN_JENIS_BIAYA);
            d.Add("@JUMLAH", promotionDetail.JUMLAH);
            d.Add("@JUMLAH_GROSS_UP", promotionDetail.JUMLAH_GROSS_UP);
            d.Add("@KETERANGAN", promotionDetail.KETERANGAN);
            d.Add("@JUMLAH_PPH", promotionDetail.JUMLAH_PPH);
            d.Add("@JENIS_PPH", promotionDetail.JENIS_PPH);
            d.Add("@JUMLAH_NET", promotionDetail.JUMLAH_NET);
            d.Add("@NOMOR_REKENING", promotionDetail.NOMOR_REKENING);
            d.Add("@NAMA_REKENING_PENERIMA", promotionDetail.NAMA_REKENING_PENERIMA);
            d.Add("@NAMA_BANK", promotionDetail.NAMA_BANK);
            d.Add("@NO_KTP", promotionDetail.NO_KTP);
            d.Add("@CREATED_DT", promotionDetail.CREATED_DT);
            d.Add("@CREATED_BY", promotionDetail.CREATED_BY);
            ExecData("sp_CreatePromotionDetail", d);
        }

        public List<string> getDataFieldName(string _categoryCode, string _fieldNameByTable)
        {
            return executeProcedureQuery<string>("sp_GetDataFieldName", new { categoryCode = _categoryCode, fieldNameByTable = _fieldNameByTable }).ToList();

        }

        public List<DataFieldTemplate> getDataFieldNameTemplate(string _categoryCode)
        {
            return executeProcedureQuery<DataFieldTemplate>("sp_GetDataFieldNameByExcel", new { categoryCode = _categoryCode }).ToList();

        }

        public bool IsValidationVendorGroup(string _categoryCode, string _fieldNameByTable, int? _VENDOR_GROUP_CD = 0)
        {
            return executeProcedureQuery<bool>("sp_GetDataFieldValidationVendor", new { categoryCode = _categoryCode, fieldNameByTable = _fieldNameByTable, VENDOR_GROUP_CD = _VENDOR_GROUP_CD }).FirstOrDefault();

        }

        public void EditPromotion(Promotion promotion)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@SEQ_NO", promotion.SEQ_NO);
            d.Add("@NAMA", promotion.NAMA);
            d.Add("@NPWP", promotion.NPWP);
            d.Add("@ALAMAT", promotion.ALAMAT);

            d.Add("@CREATED_DT", promotion.CREATED_DT);
            d.Add("@CREATED_BY", promotion.CREATED_BY);
            ExecData("sp_UpdatePromotion", d);
        }
        public void EditPromotionDetail(PromotionDetail promotionDetail)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@SEQ_NO_DETAIL", promotionDetail.SEQ_NO_DETAIL);

            d.Add("@TANGGAL", promotionDetail.TANGGAL);
            d.Add("@BENTUK_DAN_JENIS_BIAYA", promotionDetail.BENTUK_DAN_JENIS_BIAYA);
            d.Add("@JUMLAH", promotionDetail.JUMLAH);
            d.Add("@JUMLAH_GROSS_UP", promotionDetail.JUMLAH_GROSS_UP);
            d.Add("@KETERANGAN", promotionDetail.KETERANGAN);
            d.Add("@JUMLAH_PPH", promotionDetail.JUMLAH_PPH);
            d.Add("@JENIS_PPH", promotionDetail.JENIS_PPH);
            d.Add("@JUMLAH_NET", promotionDetail.JUMLAH_NET);
            d.Add("@NOMOR_REKENING", promotionDetail.NOMOR_REKENING);
            d.Add("@NAMA_REKENING_PENERIMA", promotionDetail.NAMA_REKENING_PENERIMA);
            d.Add("@NAMA_BANK", promotionDetail.NAMA_BANK);
            d.Add("@NO_KTP", promotionDetail.NO_KTP);
            d.Add("@CREATED_DT", promotionDetail.CREATED_DT);
            d.Add("@CREATED_BY", promotionDetail.CREATED_BY);
            ExecData("sp_UpdatePromotionDetail", d);
        }
        public void DeletePromotion(Int64? _seqno = 0)
        {
            DynamicParameters d = new DynamicParameters();

            d.Add("@SEQ_NO", _seqno);
            ExecData("sp_DeletePromotion", d);
        }

        public void DeletePromotionDetail(Int64? _seqnoDetail = 0)
        {
            DynamicParameters d = new DynamicParameters();

            d.Add("@SEQ_NO_DETAIL", _seqnoDetail);
            ExecData("sp_DeletePromotionDetail", d);
        }

        public List<EnomVendor> GetEnomVendor(string _VENDOR_CD)
        {
            List<EnomVendor> l = new List<EnomVendor>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@VENDOR_CD", _VENDOR_CD);

            l = executeProcedureQuery<EnomVendor>("sp_GetVendor", d).ToList();
            return l;
        }

        public bool UpdateHeaderPromotion(DataFieldHeader _dataFieldHeader)
        {
            bool result = false;
            try
            {


                bool saveResult = false;

                DynamicParameters d = new DynamicParameters();
                d.Add("@categoryCode", _dataFieldHeader.CATEGORY_CODE);
                d.Add("@transactionCode", _dataFieldHeader.TRANSACTION_CD);
                d.Add("@referenceNo", _dataFieldHeader.REFERENCE_NO);
                d.Add("@thnPajak", _dataFieldHeader.THN_PAJAK);
                d.Add("@tmpPenandatangan", _dataFieldHeader.TMP_PENANDATANGAN);
                d.Add("@tglPenandatangan", _dataFieldHeader.TGL_PENANDATANGAN);
                d.Add("@namaPenadatangan", _dataFieldHeader.NAMA_PENANDATANGAN);
                d.Add("@jabatanPenandatangan", _dataFieldHeader.JABATAN_PENANDATANGAN);
                d.Add("@IsStatus", saveResult, DbType.Boolean, ParameterDirection.Output);

                Exec("sp_UpdateHeaderPromotion", d);

                saveResult = d.Get<bool>("@IsStatus");
                result = saveResult;


            }
            catch (Exception ex)
            {
                result = false;
                Handle(ex);
            }
            return result;
        }

        public static string FormatNPWP(string value)
        {

            string a = value.Substring(0, 2);
            string b = value.Substring(2, 3);
            string c = value.Substring(5, 3);
            string d = value.Substring(8, 1);
            string e = value.Substring(9, 3);
            string f = value.Substring(12, 3);

            return a + "." + b + "." + c + "." + d + "-" + e + "." + f;
        }

        public String getData(ISheet _sheet, int row, int column)
        {

            var cell = _sheet.GetRow(row).GetCell(column);
            String data = null;
            if (cell != null)
            {
                CellType cellType = cell.CellType;
                if (cellType.ToString() == "STRING")
                {
                    data = cell.StringCellValue.ToString().Trim();
                }
                else if (cellType.ToString() == "NUMERIC")
                {
                    data = cell.NumericCellValue.ToString().Trim();
                }
                else if (cellType.ToString() == "FORMULA")
                {
                    try
                    {
                        data = cell.NumericCellValue.ToString().Trim();
                    }
                    catch
                    {

                        data = cell.StringCellValue.ToString().Trim();
                    }

                    //data = cell.CellFormula.ToString().Trim();
                }
                else if (cellType.ToString() == "BOOLEAN")
                {
                    data = cell.BooleanCellValue.ToString().Trim();
                }
                //else if (cellType.toString() == "ERROR")
                //{
                //    // let data be null or assign some value like this
                //    data = "ERROR in sheet nr. " + sheetnumber + " row nr. " + row + " column nr. " + column;
                //}
                //else if (cellType.toString() == "BLANK")
                //{
                //    // let data be null or assign some value like this
                //    data = "BLANK cell in sheet nr. " + sheetnumber + " row nr. " + row + " column nr. " + column;
                //}
                //else
                //{
                //    // let data be null or assign some value like this
                //    data = "UNKNOWN cell type in sheet nr. " + sheetnumber + " row nr. " + row + " column nr. " + column;
                //}
            }
            return data;
        }


        public bool UploadFileTemplatePromotion(
            string CategoryCode,
            Int32 TransactionCode,
            string ReferenceNo,
            string _VendorGroup,
            DateTime createddt,
            string createdby,
            string xlsFilename)
        {
            bool Ok = false;
            _errs.Clear();

            if (!System.IO.File.Exists(xlsFilename))
            {
                _errs.Add("Uploaded File not found");
            }
            FileStream file = new FileStream(xlsFilename, FileMode.OpenOrCreate, FileAccess.ReadWrite);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");

            Int32 number = 0;

            int row = 0;

            file.Close();


            bool IS_NAMA = IsValidationVendorGroup(CategoryCode, "NAMA", Convert.ToInt32(_VendorGroup));
            bool IS_NPWP = IsValidationVendorGroup(CategoryCode, "NPWP", Convert.ToInt32(_VendorGroup));
            bool IS_ALAMAT = IsValidationVendorGroup(CategoryCode, "ALAMAT", Convert.ToInt32(_VendorGroup));
            bool IS_TANGGAL = IsValidationVendorGroup(CategoryCode, "TANGGAL", Convert.ToInt32(_VendorGroup));
            bool IS_BENTUK_DAN_JENIS_BIAYA = IsValidationVendorGroup(CategoryCode, "BENTUK_DAN_JENIS_BIAYA", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH = IsValidationVendorGroup(CategoryCode, "JUMLAH", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_GROSS_UP = IsValidationVendorGroup(CategoryCode, "JUMLAH_GROSS_UP", Convert.ToInt32(_VendorGroup));
            bool IS_KETERANGAN = IsValidationVendorGroup(CategoryCode, "KETERANGAN", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_PPH = IsValidationVendorGroup(CategoryCode, "JUMLAH_PPH", Convert.ToInt32(_VendorGroup));
            bool IS_JENIS_PPH = IsValidationVendorGroup(CategoryCode, "JENIS_PPH", Convert.ToInt32(_VendorGroup));
            bool IS_JUMLAH_NET = IsValidationVendorGroup(CategoryCode, "JUMLAH_NET", Convert.ToInt32(_VendorGroup));
            bool IS_NOMOR_REKENING = IsValidationVendorGroup(CategoryCode, "NOMOR_REKENING", Convert.ToInt32(_VendorGroup));
            bool IS_NAMA_REKENING_PENERIMA = IsValidationVendorGroup(CategoryCode, "NAMA_REKENING_PENERIMA", Convert.ToInt32(_VendorGroup));
            bool IS_NAMA_BANK = IsValidationVendorGroup(CategoryCode, "NAMA_BANK", Convert.ToInt32(_VendorGroup));
            bool IS_NO_KTP = IsValidationVendorGroup(CategoryCode, "NO_KTP", Convert.ToInt32(_VendorGroup));



            string str_NAMA = getDataFieldName(CategoryCode, "NAMA").FirstOrDefault();
            string str_NPWP = getDataFieldName(CategoryCode, "NPWP").FirstOrDefault();
            string str_ALAMAT = getDataFieldName(CategoryCode, "ALAMAT").FirstOrDefault();
            string str_TANGGAL = getDataFieldName(CategoryCode, "TANGGAL").FirstOrDefault();
            string str_BENTUK_DAN_JENIS_BIAYA = getDataFieldName(CategoryCode, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault();
            string str_JUMLAH = getDataFieldName(CategoryCode, "JUMLAH").FirstOrDefault();
            string str_JUMLAH_GROSS_UP = getDataFieldName(CategoryCode, "JUMLAH_GROSS_UP").FirstOrDefault();
            string str_KETERANGAN = getDataFieldName(CategoryCode, "KETERANGAN").FirstOrDefault();
            string str_JUMLAH_PPH = getDataFieldName(CategoryCode, "JUMLAH_PPH").FirstOrDefault();
            string str_JENIS_PPH = getDataFieldName(CategoryCode, "JENIS_PPH").FirstOrDefault();
            string str_JUMLAH_NET = getDataFieldName(CategoryCode, "JUMLAH_NET").FirstOrDefault();
            string str_NOMOR_REKENING = getDataFieldName(CategoryCode, "NOMOR_REKENING").FirstOrDefault();
            string str_NAMA_REKENING_PENERIMA = getDataFieldName(CategoryCode, "NAMA_REKENING_PENERIMA").FirstOrDefault();
            string str_NAMA_BANK = getDataFieldName(CategoryCode, "NAMA_BANK").FirstOrDefault();
            string str_NO_KTP = getDataFieldName(CategoryCode, "NO_KTP").FirstOrDefault();
            IList<Promotion> promotion = new List<Promotion>();
            IList<PromotionDetail> promotionDetail = new List<PromotionDetail>();
            int errCount = 0;
            Decimal decimalType;
            //https://www.programmersought.com/article/38154063930/
            for (row = 4; row <= sheet.LastRowNum; row++) // Data row start at row 5, index 4 on excel template
            {
                Int32 _NO = 0;
                string _NAMA = "";
                string _NPWP = "";
                string _ALAMAT = "";
                // Nullable<DateTime> _Tanggal;
                DateTime? _Tanggal = null;

                string _BENTUK_DAN_JENIS_BIAYA = "";
                Decimal _Jml = 0;
                Decimal _Jmlgu = 0;
                string _KETERANGAN = "";
                Decimal _Jmlpph = 0;
                string _JenisPph = "";
                Decimal _JmlNet = 0;
                string _NoRek = "";
                string _NamaRekeningPenerima = "";
                string _NamaBank = "";
                string _NomorKtp = "";
                int errrow = 0;
                StringBuilder builder = new StringBuilder();
                builder.Append("Excel Row : " + Convert.ToString(row + 1) + "  ");
                bool isMergedCell = sheet.GetRow(row).GetCell(0).IsMergedCell;
                if (!string.IsNullOrEmpty(getData(sheet, row, 0)))
                {
                    if ((string.IsNullOrEmpty(getData(sheet, row, 0))))
                    {

                        builder.Append("No Is Mandatory;");
                        errrow++;
                        //errCount++;
                    }
                    else
                    {
                        _NO = Convert.ToInt32(getData(sheet, row, 0));
                    }
                    if ((string.IsNullOrEmpty(getData(sheet, row, 1))) && (IS_NAMA == true))
                    {

                        builder.Append(str_NAMA + " Is Mandatory;");
                        errrow++;
                        //errCount++;
                    }
                    else
                    {
                        _NAMA = getData(sheet, row, 1);
                    }
                    if ((string.IsNullOrEmpty(getData(sheet, row, 2))) && (IS_NPWP == true))
                    {
                        builder.Append(str_NPWP + " Is Mandatory;");
                        errrow++;
                    }
                    else
                    {
                        if (getData(sheet, row, 2).Length != 15)
                        {
                            builder.Append(str_NPWP + " Harus 16 Charcter;");
                            errrow++;
                        }
                        else
                        {
                            _NPWP = getData(sheet, row, 2);
                        }
                    }


                    if ((string.IsNullOrEmpty(getData(sheet, row, 3))) && (IS_ALAMAT == true))
                    {
                        builder.Append(str_ALAMAT + " Is Mandatory;");
                        errrow++;
                    }
                    else
                    {
                        _ALAMAT = getData(sheet, row, 3);
                    }
                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 4))) && (IS_TANGGAL == true))
                {
                    builder.Append(str_TANGGAL + " Is Mandatory;");
                    errrow++;

                }
                else
                {
                    var _Date = getData(sheet, row, 4).Split('.');
                    if (_Date.Count() > 1) // when the excel column is a text type
                    {
                        if (_Date[0].Length == 2 && _Date[1].Length == 2 && _Date[2].Length == 4)
                        {
                            _Tanggal = DateTime.ParseExact(getData(sheet, row, 4).Replace(".", "/"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        }
                        else
                        {
                            builder.Append(str_TANGGAL + " Format must be: dd.mm.yyyy;");
                            errrow++;

                        }
                    }

                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 5))) && (IS_BENTUK_DAN_JENIS_BIAYA == true))
                {
                    builder.Append(str_BENTUK_DAN_JENIS_BIAYA + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _BENTUK_DAN_JENIS_BIAYA = getData(sheet, row, 5);
                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 6))) && (IS_JUMLAH == true))
                {
                    builder.Append(str_JUMLAH + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    bool resultjml = Decimal.TryParse(getData(sheet, row, 6), out decimalType);
                    if (resultjml)
                    {
                        _Jml = Decimal.Parse(getData(sheet, row, 6));

                    }
                    else
                    {
                        builder.Append(str_JUMLAH + " must be Decimal Data;");
                        errrow++;
                    }

                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 7))) && (IS_JUMLAH_GROSS_UP == true))
                {
                    builder.Append(str_JUMLAH_GROSS_UP + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    bool resultjmlgu = Decimal.TryParse(getData(sheet, row, 7), out decimalType);
                    if (resultjmlgu)
                    {
                        _Jmlgu = Decimal.Parse(getData(sheet, row, 7));

                    }
                    else
                    {
                        builder.Append(str_JUMLAH_GROSS_UP + " must be Decimal Data;");
                        errrow++;
                    }

                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 8))) && (IS_KETERANGAN == true))
                {
                    builder.Append(str_KETERANGAN + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _KETERANGAN = getData(sheet, row, 8);
                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 9))) && (IS_JUMLAH_PPH == true))
                {
                    builder.Append(str_JUMLAH_PPH + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    bool resultjmlpph = Decimal.TryParse(getData(sheet, row, 9), out decimalType);
                    if (resultjmlpph)
                    {
                        _Jmlpph = Decimal.Parse(getData(sheet, row, 9));

                    }
                    else
                    {
                        builder.Append(str_JUMLAH_PPH + " must be Decimal Data;");
                        errrow++;
                    }

                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 10))) && (IS_JENIS_PPH == true))
                {
                    builder.Append(str_JENIS_PPH + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _JenisPph = getData(sheet, row, 10);
                }


                if ((string.IsNullOrEmpty(getData(sheet, row, 11))) && (IS_JUMLAH_NET == true))
                {
                    builder.Append(str_JUMLAH_NET + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    bool resultjmlNet = Decimal.TryParse(getData(sheet, row, 11), out decimalType);
                    if (resultjmlNet)
                    {
                        _JmlNet = Decimal.Parse(getData(sheet, row, 11));

                    }
                    else
                    {
                        builder.Append(str_JUMLAH_NET + " must be Decimal Data;");
                        errrow++;
                    }

                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 12))) && (IS_NOMOR_REKENING == true))
                {
                    builder.Append(str_NOMOR_REKENING + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _NoRek = getData(sheet, row, 12);
                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 13))) && (IS_NAMA_REKENING_PENERIMA == true))
                {
                    builder.Append(str_NAMA_REKENING_PENERIMA + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _NamaRekeningPenerima = getData(sheet, row, 13);
                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 14))) && (IS_NAMA_BANK == true))
                {
                    builder.Append(str_NAMA_BANK + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _NamaBank = getData(sheet, row, 14);
                }

                if ((string.IsNullOrEmpty(getData(sheet, row, 15))) && (IS_NO_KTP == true))
                {
                    builder.Append(str_NO_KTP + " Is Mandatory;");
                    errrow++;
                }
                else
                {
                    _NomorKtp = getData(sheet, row, 15);
                }
                if (errrow > 0)
                {
                    errCount = +errrow;
                    _errs.Add(builder.ToString());
                }
                else
                {


                    if (!string.IsNullOrEmpty(getData(sheet, row, 0)))
                    {
                        number = _NO;
                        promotion.Add(new Promotion
                        {
                            CATEGORY_CODE = CategoryCode,
                            TRANSACTION_CD = TransactionCode,
                            REFERENCE_NO = ReferenceNo,
                            NO = _NO,
                            NAMA = _NAMA,
                            NPWP = FormatNPWP(_NPWP),
                            ALAMAT = _ALAMAT,
                            //TANGGAL = (DateTime)_Tanggal,
                            //BENTUK_DAN_JENIS_BIAYA = _BENTUK_DAN_JENIS_BIAYA,
                            //JUMLAH = (Decimal)_Jml,
                            //JUMLAH_GROSS_UP = _Jmlgu,
                            //KETERANGAN = _KETERANGAN,
                            //JUMLAH_PPH = _Jmlpph,
                            //JENIS_PPH = _JenisPph,
                            //JUMLAH_NET = _JmlNet,
                            //NOMOR_REKENING = _NoRek,
                            //NAMA_REKENING_PENERIMA = _NamaRekeningPenerima,
                            //NAMA_BANK = _NamaBank,
                            //NO_KTP = _NomorKtp,
                            CREATED_DT = createddt,
                            CREATED_BY = createdby,


                        });
                    }


                    promotionDetail.Add(new PromotionDetail
                    {

                        NO = number,
                        TANGGAL = (DateTime)_Tanggal,
                        BENTUK_DAN_JENIS_BIAYA = _BENTUK_DAN_JENIS_BIAYA,
                        JUMLAH = (Decimal)_Jml,
                        JUMLAH_GROSS_UP = _Jmlgu,
                        KETERANGAN = _KETERANGAN,
                        JUMLAH_PPH = _Jmlpph,
                        JENIS_PPH = _JenisPph,
                        JUMLAH_NET = _JmlNet,
                        NOMOR_REKENING = _NoRek,
                        NAMA_REKENING_PENERIMA = _NamaRekeningPenerima,
                        NAMA_BANK = _NamaBank,
                        NO_KTP = _NomorKtp
                    });


                }
            }
            if (errCount > 0)
            {
                Ok = false;
            }
            else
            {
                if (promotion.Count() > 0)
                {
                    foreach (Promotion item in promotion.ToList())
                    {
                        AddPromotionMasterDetail(item, promotionDetail);
                    }
                    Ok = true;
                }
            }
            return Ok;
        }


        public byte[] ExportXlsPromotion(string _CategoryCodeProm, string _TransactionCodeProm, string _ReferenceNoProm, string templatePathSource, string templatePath)
        {

            List<PromotionReportXls> _listData = new List<PromotionReportXls>();
            _listData = GetPromotionReportXls(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);
            //  string templatePathSource = System.IO.Path.Combine(Server.MapPath(logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotion.xls");
            // string templatePath = System.IO.Path.Combine(Server.MapPath(def.logic.Sys.GetText("EXCEL_TEMPLATE", "DIR")), "DataFieldPromotionTest.xls");
            //string excelName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".xls";
            //string pdfName = "List_Accrued_Report" + DateTime.Now.ToString("ddMMyyyy_HHmmss") + ".pdf";
            FileStream file = new FileStream(templatePathSource, FileMode.OpenOrCreate, FileAccess.Read);

            HSSFWorkbook hssfwb = new HSSFWorkbook(file);
            ISheet sheet = hssfwb.GetSheet("Default");
            // sheet.GetRow(3).GetCell(0).StringCellValue.ToString().Trim();

            int i = 1;

            if (_listData.Count() > 0)
            {

                foreach (PromotionReportXls item in _listData.ToList())
                {

                    var row = sheet.CreateRow(i);
                    row.CreateCell(0).SetCellValue(item.NO);
                    row.CreateCell(1).SetCellValue(item.NAMA);
                    row.CreateCell(2).SetCellValue(item.NPWP);
                    row.CreateCell(3).SetCellValue(item.ALAMAT);
                    row.CreateCell(4).SetCellValue(item.TANGGAL);
                    row.CreateCell(5).SetCellValue(item.BENTUK_DAN_JENIS_BIAYA);
                    row.CreateCell(6).SetCellValue(item.JUMLAH.ToString());
                    row.CreateCell(7).SetCellValue(item.JUMLAH_GROSS_UP.ToString());
                    row.CreateCell(8).SetCellValue(item.KETERANGAN.ToString());
                    row.CreateCell(9).SetCellValue(item.JUMLAH_PPH.ToString());
                    row.CreateCell(10).SetCellValue(item.JENIS_PPH.ToString());
                    row.CreateCell(11).SetCellValue(item.JUMLAH_NET.ToString());
                    row.CreateCell(12).SetCellValue(item.NOMOR_REKENING);
                    row.CreateCell(13).SetCellValue(item.NAMA_REKENING_PENERIMA);
                    row.CreateCell(14).SetCellValue(item.NAMA_BANK);
                    row.CreateCell(15).SetCellValue(item.NO_KTP);

                    i++;
                }
            }

            //using (FileStream fs = new FileStream(templatePath, FileMode.Create, FileAccess.Write))
            //{

            //    hssfwb.Write(fs);
            //    fs.Close();
            //    file.Close();
            //}
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            hssfwb.Write(ms);
            return ms.ToArray();

            //byte[] bytes = Convert.FromBase64String(templatePathSource);
            //using (MemoryStream fs = new MemoryStream(bytes))
            //{

            //    hssfwb.Write(fs);
            //    fs.Close();
            //    file.Close();
            //}
            //return bytes;
        }

        public byte[] ExportPdfPromotion(string _CategoryCodeProm,
            string _TransactionCodeProm,
            string _ReferenceNoProm,


            string ReportPath)
        {
            List<PromotionReport> _listData = new List<PromotionReport>();
            _listData = GetPromotionReport(_CategoryCodeProm, Convert.ToInt32(_TransactionCodeProm), _ReferenceNoProm);

            List<PromotionHeaderTemplateReport> dataField = new List<PromotionHeaderTemplateReport>();
            dataField.Add(new PromotionHeaderTemplateReport
            {
                NO = getDataFieldName(_CategoryCodeProm, "NO").FirstOrDefault(),
                NAMA = getDataFieldName(_CategoryCodeProm, "NAMA").FirstOrDefault(),
                NPWP = getDataFieldName(_CategoryCodeProm, "NPWP").FirstOrDefault(),
                ALAMAT = getDataFieldName(_CategoryCodeProm, "ALAMAT").FirstOrDefault(),
                TANGGAL = getDataFieldName(_CategoryCodeProm, "TANGGAL").FirstOrDefault(),
                BENTUK_DAN_JENIS_BIAYA = getDataFieldName(_CategoryCodeProm, "BENTUK_DAN_JENIS_BIAYA").FirstOrDefault(),
                JUMLAH = getDataFieldName(_CategoryCodeProm, "JUMLAH").FirstOrDefault(),
                JUMLAH_GROSS_UP = getDataFieldName(_CategoryCodeProm, "JUMLAH_GROSS_UP").FirstOrDefault(),
                KETERANGAN = getDataFieldName(_CategoryCodeProm, "KETERANGAN").FirstOrDefault(),
                JUMLAH_PPH = getDataFieldName(_CategoryCodeProm, "JUMLAH_PPH").FirstOrDefault(),
                JENIS_PPH = getDataFieldName(_CategoryCodeProm, "JENIS_PPH").FirstOrDefault(),
                JUMLAH_NET = getDataFieldName(_CategoryCodeProm, "JUMLAH_NET").FirstOrDefault(),
                NOMOR_REKENING = getDataFieldName(_CategoryCodeProm, "NOMOR_REKENING").FirstOrDefault(),
                NAMA_REKENING_PENERIMA = getDataFieldName(_CategoryCodeProm, "NAMA_REKENING_PENERIMA").FirstOrDefault(),
                NAMA_BANK = getDataFieldName(_CategoryCodeProm, "NAMA_BANK").FirstOrDefault(),
                NO_KTP = getDataFieldName(_CategoryCodeProm, "NO_KTP").FirstOrDefault()
            });


            List<ImageData> imagedata = new List<ImageData>();
            string data = _CategoryCodeProm + "-" + _TransactionCodeProm + "-" + _ReferenceNoProm;


            imagedata.Add(new ImageData { QrCode = GenerateQrCode(data) });


            string extension;
            string encoding;
            string mimeType;
            string[] streams;
            Warning[] warnings;

            LocalReport report = new LocalReport();



            // report.ReportPath = Server.MapPath("~/Report/RDLC/DataFieldPromotion.rdlc");
            report.ReportPath = ReportPath;
            ReportDataSource rds1 = new ReportDataSource();
            rds1.Name = "DataSetPromotionHeader";
            rds1.Value = dataField;
            report.DataSources.Add(rds1);
            ReportDataSource rds2 = new ReportDataSource();
            rds2.Name = "DataSetPromotionDetail";
            rds2.Value = _listData;
            report.DataSources.Add(rds2);

            ReportDataSource rds3 = new ReportDataSource();
            rds3.Name = "DataSetQrCode";
            rds3.Value = imagedata;
            report.DataSources.Add(rds3);

            Byte[] mybytes = report.Render("PDF", null,
                            out extension, out encoding,
                            out mimeType, out streams, out warnings); //for exporting to PDF  
            return mybytes;
        }

        public string GenerateQrCode(string text)
        {
            byte[] imgArray;
            // string name = "</? xml version = \"1.0\" encoding = \"UTF - 8\" ?>< PrintLetterBarcodeData uid = \"358655541160\" name = \"St. Paul Premkumar\" gender = \"MALE\" yob =  \"1981\" co = \"S/O Steepan\" lm = \"SN.Nagar\" loc = \"null\" vtc = \"Anna Nagar\" po = \"Anna Nagar West\" dist = \"Chennai\" state = \"Tamil Nadu\" pc = \"601502\" dob = \"12-06-1981\" />";

            QRCodeGenerator qrGenerator = new QRCodeGenerator();
            QRCodeData qrCodeData = qrGenerator.CreateQrCode(text, QRCodeGenerator.ECCLevel.Q);
            QRCoder.QRCode qrCode = new QRCoder.QRCode(qrCodeData);
            Bitmap qrCodeImage = qrCode.GetGraphic(20);

            using (MemoryStream imageStream = new MemoryStream())
            {
                qrCodeImage.Save(imageStream, ImageFormat.Png);
                imgArray = new byte[imageStream.Length];
                imageStream.Seek(0, SeekOrigin.Begin);
                imageStream.Read(imgArray, 0, (int)imageStream.Length);
            }
            // return imgArray;
            return Convert.ToBase64String(imgArray);
        }

        public List<AttachmentCategoryNominative> GetAttachmentCategoryNominative(string CategoryCode, string ReferenceNo, int? vendorgroup = 0)
        {
            List<AttachmentCategoryNominative> lstAttachmentEnom = new List<AttachmentCategoryNominative>();
            DynamicParameters d = new DynamicParameters();
            d.Add("@categoryCode", CategoryCode);
            d.Add("@referenceNo", ReferenceNo);
            d.Add("@VENDOR_GROUP_CD", vendorgroup);

            List<AttachmentCategoryNominative> lstAttEnom = executeProcedureQuery<AttachmentCategoryNominative>("sp_GetAttachmentCategoryNominative", d).ToList();
            foreach (var a in lstAttEnom)
            {
                a.Blank = false;
                a.Url = CommonFunction.CombineUrl(Common.AppSetting.FTP_DOWNLOADHTTP, a.PATH, a.FileName);
            }
            lstAttachmentEnom.AddRange(lstAttEnom);
            return lstAttachmentEnom;
        }
        public string GetCategoryNominative(string _categoryCode)
        {
            return executeProcedureQuery<string>("usp_GetCategoryNominative", new { enominativeGroup = _categoryCode }).FirstOrDefault();

        }

        public void AddAttachmenCategoryNominative(AttachmentCategoryNominative _attachmentCategoryNominative)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@referenceNumber", _attachmentCategoryNominative.ReferenceNumber);
            d.Add("@fileName", _attachmentCategoryNominative.FileName);
            d.Add("@categoryCode", _attachmentCategoryNominative.ATTACH_CD);
            d.Add("@description", _attachmentCategoryNominative.Description);
            d.Add("@path", _attachmentCategoryNominative.PATH);
            d.Add("@fileType", _attachmentCategoryNominative.FILE_TYPE);
            d.Add("@enominativeGroup", _attachmentCategoryNominative.CategoryCode);
            d.Add("@isEnominative", _attachmentCategoryNominative.IS_ENOMINATIVE);
            d.Add("@username", _attachmentCategoryNominative.CreatedBy);
            ExecData("usp_SaveAttachmenCategoryNominative", d);
        }

        public void DeleteAttachmenCategoryNominative(string _referenceNumber, decimal? seqNumber = 0)
        {
            DynamicParameters d = new DynamicParameters();

            d.Add("@referenceNumber", _referenceNumber);
            d.Add("@seqNumber", seqNumber);
            ExecData("usp_DeleteAttachmenCategoryNominative", d);
        }

        public List<AttachmentFakturPajak> GetAttachmentFakturPajak(string ReferenceNo)
        {
            List<AttachmentFakturPajak> lstAttachmentFaktur = new List<AttachmentFakturPajak>();
            DynamicParameters d = new DynamicParameters();

            d.Add("@referenceNo", ReferenceNo);

            List<AttachmentFakturPajak> lstAttFaktur = executeProcedureQuery<AttachmentFakturPajak>("sp_GetEnomFakturPajak", d).ToList();
            foreach (var a in lstAttFaktur)
            {

                a.URL_DIRECTORY = CommonFunction.CombineUrl(Common.AppSetting.FTP_DOWNLOADHTTP, a.DIRECTORY, a.FILE_NAME);
            }
            lstAttachmentFaktur.AddRange(lstAttFaktur);
            return lstAttachmentFaktur;
        }
        public Int64 CreateAttachmentFakturPajak(AttachmentFakturPajak _attachmentFakturPajak)
        {
            Int64 result = 0;
            try
            {
                Int64 saveResult = 0;
                DynamicParameters d = new DynamicParameters();
                d.Add("@referenceNo", _attachmentFakturPajak.REFERENCE_NO);
                d.Add("@description", _attachmentFakturPajak.DESCRIPTION);
                d.Add("@directory", _attachmentFakturPajak.DIRECTORY);
                d.Add("@fileName", _attachmentFakturPajak.FILE_NAME);
                d.Add("@status", _attachmentFakturPajak.STATUS);
                d.Add("@url", _attachmentFakturPajak.URL);
                d.Add("@errMessage", _attachmentFakturPajak.ERR_MESSAGE);
                d.Add("@createdBy", _attachmentFakturPajak.CREATED_BY);
                d.Add("@createdDt", _attachmentFakturPajak.CREATED_DT);
                d.Add("@attFakturPajakId", saveResult, DbType.Int64, ParameterDirection.Output);
                ExecData("sp_CreateAttFakturPajak", d);
                saveResult = d.Get<Int64>("@attFakturPajakId");
                result = saveResult;


            }
            catch (Exception ex)
            {
                result = 0;
                Handle(ex);
            }
            return result;
        }

        public void UpdateAttachmentFakturPajak(AttachmentFakturPajak _attachmentFakturPajak)
        {
            DynamicParameters d = new DynamicParameters();
            d.Add("@attFakturPajakId", _attachmentFakturPajak.ID);

            d.Add("@description", _attachmentFakturPajak.DESCRIPTION);
            d.Add("@status", _attachmentFakturPajak.STATUS);
            d.Add("@url", _attachmentFakturPajak.URL);
            d.Add("@errMessage", _attachmentFakturPajak.ERR_MESSAGE);
            d.Add("@createdBy", _attachmentFakturPajak.CREATED_BY);

            ExecData("sp_UpdateAttFakturPajak", d);
        }


        public Config GetByConfigKey(String ConfigKey)
        {
            return executeProcedureQuery<Config>("TAM_EFAKTUR.dbo.usp_GetConfigurationDataByConfigKey", new { ConfigKey = ConfigKey }).FirstOrDefault();
        }

        public string NoFakturXML(string valueXML)
        {
            string _nf = "";
            var response = new HttpResponseMessage();
            resValidateFakturPm VATInXML = new resValidateFakturPm();
            if (!string.IsNullOrEmpty(valueXML))
            {
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                    Uri serviceUri = new Uri(valueXML, UriKind.Absolute);

                    XmlDocument doc = new XmlDocument();
                    response = client.GetAsync(serviceUri).Result;

                    var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                    var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                    using (var reader = XmlReader.Create(stream))
                    {
                        VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                        if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                        {
                            _nf = "";
                        }
                        else
                        {
                            _nf = VATInXML.nomorFaktur;
                        }
                    }
                }
            }
            return _nf;
        }

        public void GenerateXML(string valueXML, Int64 _id)
        {

            var response = new HttpResponseMessage();
            VATInManualInputView model = new VATInManualInputView();
            resValidateFakturPm VATInXML = new resValidateFakturPm();
            try
            {
                if (!string.IsNullOrEmpty(valueXML))
                {
                    using (HttpClient client = new HttpClient())
                    {
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/xml", 0.9));
                        Uri serviceUri = new Uri(valueXML, UriKind.Absolute);

                        XmlDocument doc = new XmlDocument();
                        response = client.GetAsync(serviceUri).Result;

                        var stream = new System.IO.MemoryStream(response.Content.ReadAsByteArrayAsync().Result);

                        var serializer = new XmlSerializer(typeof(resValidateFakturPm));
                        using (var reader = XmlReader.Create(stream))
                        {
                            VATInXML = (resValidateFakturPm)serializer.Deserialize(reader);
                            if (string.IsNullOrEmpty(VATInXML.nomorFaktur))
                            {
                                throw new Exception("Invalid URL, Invoice not found");
                            }
                        }
                        string InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                        DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);
                        DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                       

                        model.InvoiceNumber = VATInXML.nomorFaktur;
                        model.InvoiceNumberFull = VATInXML.nomorFaktur.FormatNomorFakturGabungan(VATInXML.kdJenisTransaksi, VATInXML.fgPengganti);
                        model.KDJenisTransaksi = VATInXML.kdJenisTransaksi;
                        model.FGPengganti = VATInXML.fgPengganti;
                        model.InvoiceDate = VATInXML.tanggalFakturDateFormat.FormatSQLDate();
                        model.ExpireDate = endOfMonth.ToShortDateString();
                        model.SupplierNPWP = VATInXML.npwpPenjual.FormatNPWP();
                        model.SupplierName = VATInXML.namaPenjual;

                        model.SupplierAddress = VATInXML.alamatPenjual;

                        model.NPWPLawanTransaksi = VATInXML.npwpLawanTransaksi.FormatNPWP();
                        model.NamaLawanTransaksi = VATInXML.namaLawanTransaksi;
                        model.AlamatLawanTransaksi = VATInXML.alamatLawanTransaksi;
                        model.StatusApprovalXML = VATInXML.statusApproval;
                        model.StatusFakturXML = VATInXML.statusFaktur;
                        model.VATBaseAmount = VATInXML.jumlahDpp;
                        model.VATAmount = VATInXML.jumlahPpn;
                        model.JumlahPPnBM = VATInXML.jumlahPpnBm;
                        model.FakturType = "eFaktur";
                        model.VATInDetails = new List<VATInDetailManualInputView>();

                        foreach (resValidateFakturPmDetailTransaksi detail in VATInXML.detailTransaksi)
                        {
                            VATInDetailManualInputView detailModel = new VATInDetailManualInputView();

                            detailModel.UnitName = detail.nama;
                            detailModel.UnitPrice = detail.hargaSatuan;
                            detailModel.Quantity = detail.jumlahBarang;
                            detailModel.TotalPrice = detail.hargaTotal;
                            detailModel.Discount = detail.diskon;
                            detailModel.DPP = detail.dpp;
                            detailModel.PPN = detail.ppn;
                            detailModel.PPNBM = detail.ppnbm;
                            detailModel.TarifPPNBM = detail.tarifPpnbm;

                            model.VATInDetails.Add(detailModel);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            try
            {
                if (model.FakturType == "eFaktur" && !string.IsNullOrEmpty(valueXML))
                {
                    #region Validasi VAT In khusus scan
                    //validasi npwp
                    var config = GetByConfigKey("CompanyNPWP");
                    var configbatam = GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi status faktur
                    if (model.StatusFakturXML == "Faktur Dibatalkan")
                    {
                        throw new Exception("Cannot save Data with Status Faktur Dibatalkan");
                    }

                    //validasi expire date
                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = GetByConfigKey("VATInDaysExpired");

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    //validasi Faktur pengganti
                    Result validateResult = executeProcedureQuery<Result>("TAM_EFAKTUR.dbo.usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate }).FirstOrDefault();

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }

                    #endregion
                }
                else if (model.FakturType == "eFaktur" && string.IsNullOrEmpty(valueXML))
                {
                    #region Validasi VATin efaktur manual

                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = GetByConfigKey("VATInDaysExpired");

                    model.ExpireDate = tanggalexpired;

                    //validasi npwp
                    var config = GetByConfigKey("CompanyNPWP");
                    var configbatam = GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi expiredate

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    //validasi faktur pengganti
                    Result validateResult = executeProcedureQuery<Result>("TAM_EFAKTUR.dbo.usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate }).FirstOrDefault();

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }
                    #endregion
                }
                else
                {
                    #region Validasi VATin nonEfaktur

                    string InvoiceDate = model.InvoiceDate;

                    DateTime expdate = Convert.ToDateTime(InvoiceDate).AddDays(1).AddMonths(3).AddDays(-1);//
                    DateTime endOfMonth = new DateTime(expdate.Year, expdate.Month, 1).AddMonths(1).AddDays(-1);
                    string tanggalexpired = endOfMonth.ToShortDateString();
                    int SelisihHari = (Convert.ToDateTime(tanggalexpired) - DateTime.Now.Date.AddDays(-1)).Days;//Convert.ToDateTime(tanggalexpired)).Days;
                    var NilaiSelisihHari = GetByConfigKey("VATInDaysExpired");

                    model.ExpireDate = tanggalexpired;

                    //validasi npwp
                    var config = GetByConfigKey("CompanyNPWP");
                    var configbatam = GetByConfigKey("CompanyNPWPBatam");
                    if (config == null)
                    {
                        throw new Exception("Please define the CompanyNPWP Config");
                    }

                    if (configbatam == null)
                    {
                        throw new Exception("Please define the CompanyNPWPBatam Config");
                    }

                    if (!model.NPWPLawanTransaksi.Equals(config.ConfigValue) && !model.NPWPLawanTransaksi.Equals(configbatam.ConfigValue))
                    {
                        throw new Exception("Buyer NPWP '" + model.NPWPLawanTransaksi + "' does not match with Company NPWP");
                    }

                    //validasi tanggal expired

                    if (SelisihHari < Convert.ToInt32(NilaiSelisihHari.ConfigValue))
                    {
                        if (SelisihHari <= 0)
                        {
                            SelisihHari = 0;
                        }

                        throw new Exception("Tax Invoice can’t be saved. Tax Invoice EXPIRED is Already " + SelisihHari + " Days Before Expired.");
                    }

                    Result validateResult = executeProcedureQuery<Result>("TAM_EFAKTUR.dbo.usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate }).FirstOrDefault();

                    if (!validateResult.ResultCode)
                    {
                        throw new Exception(validateResult.ResultDesc);
                    }

                    #endregion
                }
               
                Nullable<Guid> SyncId = null;
                string FakturType = model.FakturType;
             
                #region Master - VATIn
                //generate new Guid for Id - VATIn
                Guid VATInID = Guid.NewGuid();
                // model.ExpireDate.Trim().Split('/');
                DateTime _ExpireDate = Convert.ToDateTime(model.ExpireDate.Trim());

                //   DateTime dataExpireDate =Convert.ToDateTime(_ExpireDate[2] + "-" + _ExpireDate[1] + "-" + _ExpireDate[0]);
                DynamicParameters d = new DynamicParameters();

              
                d.Add("@Id", VATInID);
                d.Add("@SyncId", SyncId);
                d.Add("@InvoiceNumber", model.InvoiceNumber);
                d.Add("@InvoiceNumberFull", model.InvoiceNumberFull);
                d.Add("@KDJenisTransaksi", model.KDJenisTransaksi);
                d.Add("@FGPengganti", model.FGPengganti);
                d.Add("@InvoiceDate", model.InvoiceDate);

                d.Add("@ExpireDate", _ExpireDate);
                d.Add("@SupplierNPWP", model.SupplierNPWP);
                d.Add("@SupplierName", model.SupplierName);
                d.Add("@SupplierAddress", model.SupplierAddress);

                d.Add("@StatusApprovalXML", model.StatusApprovalXML);
                d.Add("@StatusFakturXML", model.StatusFakturXML);
                d.Add("@FakturType", FakturType);
                d.Add("@VATBaseAmount", model.VATBaseAmount);
                d.Add("@VATAmount", model.VATAmount);
                d.Add("@JumlahPPNBM", model.JumlahPPnBM);
                d.Add("@EventDate", DateTime.Now.FormatSQLDateTime());
                d.Add("@EventActor", "ELVIS");
                 d.Add("@IdNuminative", _id);
               
              ExecData("TAM_EFAKTUR.dbo.usp_InsertCustomEnom_TB_R_VATIn", d);
                #endregion
                //kondisi
                #region Detail - VATInDetail
                foreach (VATInDetailManualInputView detailModel in model.VATInDetails)
                {
                    //dynamic argsDetail = new
                    //{
                    DynamicParameters detail = new DynamicParameters();
                    detail.Add("@Id", Guid.NewGuid());
                    detail.Add("@VATInId", VATInID);
                    detail.Add("@UnitName", detailModel.UnitName);
                    detail.Add("@UnitPrice", detailModel.UnitPrice);
                    detail.Add("@Quantity", detailModel.Quantity);
                    detail.Add("@TotalPrice", detailModel.TotalPrice);
                    detail.Add("@Discount", detailModel.Discount);
                    detail.Add("@DPP", detailModel.DPP);
                    detail.Add("@PPN", detailModel.PPN);
                    detail.Add("@TarifPPNBM", detailModel.TarifPPNBM);
                    detail.Add("@PPNBM", detailModel.PPNBM);
                    detail.Add("@EventDate", DateTime.Now.FormatSQLDateTime());
                    detail.Add("@EventActor", "ELVIS");
                    //};
                   ExecData("TAM_EFAKTUR.dbo.usp_InsertCustom_TB_R_VATInDetail", detail);
                }
                #endregion
                //db.CommitTransaction();
             
            }
            catch (Exception e)
            {
               
                throw e;
            }
        }
    

        public Result GetValidateVATInInvoiceNumber(String ConfigKey)
        {
            return executeProcedureQuery<Result>("TAM_EFAKTUR.dbo.usp_ValidateVATInInvoiceNumber", new { ConfigKey = ConfigKey }).FirstOrDefault();
        }
       // Result validateResult = db.SingleOrDefault<Result>("VATIn/usp_ValidateVATInInvoiceNumber", new { NomorFakturGabungan = model.InvoiceNumberFull, NomorFaktur = model.InvoiceNumber, FGPengganti = model.FGPengganti, FakturType = model.FakturType, NPWPPenjual = model.SupplierNPWP, InvoiceDate = model.InvoiceDate });
        //--------------------------------------------------------


    }
}