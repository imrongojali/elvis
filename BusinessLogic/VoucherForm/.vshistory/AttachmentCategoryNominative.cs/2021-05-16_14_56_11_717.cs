﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class AttachmentCategoryNominative
    {
        public int SequenceNumber { get; set; }
        public string ReferenceNumber { get; set; }

        public string CategoryCode { get; set; }
        public string CategoryName { get; set; }
        public string PATH { get; set; }
        public string FileName { get; set; }
        public string DisplayFilename { get; set; }
        public string Description { get; set; }
        public string FILE_TYPE { get; set; }
        public string ValidateFileType { get; set; }
        public bool ValidateVendorGroup { get; set; }
        public string CreatedBy { get; set; }
    }
}
