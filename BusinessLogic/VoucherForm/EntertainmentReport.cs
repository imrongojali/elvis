﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class EntertainmentReport
    {

        public string CompanyName { get; set; }
        public string CompanyNPWP { get; set; }
        public string CompanyAddress { get; set; }
        public string TMP_PENANDATANGAN { get; set; }
        public DateTime TGL_PENANDATANGAN { get; set; }
        public string NAMA_PENANDATANGAN { get; set; }
        public string JABATAN_PENANDATANGAN { get; set; }
        public Int32 THN_PAJAK { get; set; }
        public string REFERENCE_NO { get; set; }
        public string ENOM_NO { get; set; }
        public Int32 PV_NO { get; set; }

        public Int64 SEQ_NO { get; set; }
        public string CATEGORY_CODE { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
        public string NO { get; set; }
        public DateTime TANGGAL { get; set; }
        public string TEMPAT { get; set; }
        public string ALAMAT { get; set; }
        public string TIPE_ENTERTAINMENT { get; set; }
        public decimal JUMLAH { get; set; }
        public string NAMA_RELASI { get; set; }
        public string JABATAN { get; set; }
        public string ALAMAT_PERUSAHAAN { get; set; }
        public string TIPE_BISNIS { get; set; }
        public string KETERANGAN { get; set; }
        public string BENTUK_DAN_JENIS_ENTERTAINMENT { get; set; }
        public string POSISI_RELASI { get; set; }
        public string PERUSAHAAN_RELASI { get; set; }
        public string JENIS_USAHA_RELASI { get; set; }
        public decimal TOTAL { get; set; }
    }
}
