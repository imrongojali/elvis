﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class Question
    {
        public int RowNumber { get; set; }
        public string CategoryCode { get; set; }
        public string NominativeType { get; set; }
        public Int64 QuestionId { get; set; }
        public string DescriptionQuestion { get; set; }
        public bool IsYes { get; set; }
        public string WarningStatusYes { get; set; }
        public bool IsNo { get; set; }
        public string WarningStatusNo { get; set; }
        public bool ValueCheckedYes { get; set; }
        public bool ValueCheckedNo { get; set; }
        public bool EnableChk { get; set; }

    }
}
