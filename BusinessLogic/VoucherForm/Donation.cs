﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class Donation
    {
        public Int64 SEQ_NO { get; set; }
        public string CATEGORY_CODE { get; set; }
        public Int32 TRANSACTION_CD { get; set; }
        public string REFERENCE_NO { get; set; }
        public string ENOM_NO { get; set; }
        public Int32 PV_NO { get; set; }
        public Int32 NO { get; set; }
        public string JENIS_SUMBANGAN { get; set; }
        public string BENTUK_SUMBANGAN { get; set; }
        public Decimal NILAI_SUMBANGAN { get; set; }
        public DateTime TANGGAL_SUMBANGAN { get; set; }
        public string NAMA_LEMBAGA_PENERIMA { get; set; }
        public string NPWP_LEMBAGA_PENERIMA { get; set; }
        public string ALAMAT_LEMBAGA { get; set; }
        public string NO_TELP_LEMBAGA { get; set; }
        public string SARANA_PRASARANA_INFRASTRUKTUR { get; set; }
        public string LOKASI_INFRASTRUKTUR { get; set; }
        public string IZIN_MENDIRIKAN_BANGUNAN { get; set; }
        public string NOMOR_REKENING { get; set; }
        public string NAMA_REKENING_PENERIMA { get; set; }
        public string NAMA_BANK { get; set; }
        public string NO_KTP { get; set; }
        public Decimal BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }
        public string FormType { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public int THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }
       
    }
}
