﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    public class PromotionReportXls
    {
        public Int32 NO { get; set; }
        public Int32 SplitNomor { get; set; }
        public string NAMA { get; set; }
        public string NPWP { get; set; }
        public string ALAMAT { get; set; }

        public DateTime? TANGGAL { get; set; }
        public string BENTUK_DAN_JENIS_BIAYA { get; set; }
        //public decimal JUMLAH { get; set; }
        public string JUMLAH { get; set; }
        //public decimal JUMLAH_GROSS_UP { get; set; }
        public string JUMLAH_GROSS_UP { get; set; }
        public string KETERANGAN { get; set; }
        //public decimal JUMLAH_PPH { get; set; }
        public string JUMLAH_PPH { get; set; }
        public string JENIS_PPH { get; set; }
        public string JUMLAH_NET { get; set; }
        //public decimal JUMLAH_NET { get; set; }
        public string NOMOR_REKENING { get; set; }
        public string NAMA_REKENING_PENERIMA { get; set; }
        public string NAMA_BANK { get; set; }
        public string NO_KTP { get; set; }
    }
}
