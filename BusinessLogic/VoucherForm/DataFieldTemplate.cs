﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class DataFieldTemplate
    {
        public string FieldNameByTable { get; set; }
        public string FieldNameByExcel { get; set; }
    }
}
