﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class DonationReport
    {

        public string DONATION_ID { get; set; }
        public string CompanyName { get; set; }
        public string CompanyNPWP { get; set; }
        public string CompanyAddress { get; set; }

        public string REFERENCE_NO { get; set; }
        public string PV_NO { get; set; }
        //public int? THN_PAJAK { get; set; }

        public string TMP_PENANDATANGAN { get; set; }

        public DateTime? TGL_PENANDATANGAN { get; set; }

        public string NAMA_PENANDATANGAN { get; set; }

        public string JABATAN_PENANDATANGAN { get; set; }

        public string JENIS_SUMBANGAN { get; set; }

        public string BENTUK_SUMBANGAN { get; set; }

        public decimal? NILAI_SUMBANGAN { get; set; }

        public DateTime? TANGGAL_SUMBANGAN { get; set; }

        public string NAMA_LEMBAGA_PENERIMA { get; set; }

        public string NPWP_LEMBAGA_PENERIMA { get; set; }

        public string ALAMAT_LEMBAGA { get; set; }

        public string NO_TELP_LEMBAGA { get; set; }

        public string SARANA_PRASARANA_INFRASTRUKTUR { get; set; }

        public string LOKASI_INFRASTRUKTUR { get; set; }

        public decimal? BIAYA_PEMBANGUNAN_INFRASTRUKTUR { get; set; }

        public string IZIN_MENDIRIKAN_BANGUNAN { get; set; }

        public string NOMOR_REKENING { get; set; }

        public string NAMA_REKENING_PENERIMA { get; set; }

        public string NAMA_BANK { get; set; }
    }
}
