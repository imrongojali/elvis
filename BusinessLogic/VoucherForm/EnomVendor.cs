﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class EnomVendor
    {
       public string VENDOR_CD { get; set; }
        public string  VENDOR_NAME { get; set; }
        public Int32 VENDOR_GROUP_CD { get; set; }
        public string NPWP { get; set; }
        public string NpwpAddress { get; set; }
        
    }
}
