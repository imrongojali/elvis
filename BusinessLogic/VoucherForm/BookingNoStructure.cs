﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class BookingNoStructure
    {
        public string BookingNo { set; get; }
        public string WbsNumber { set; get; }
        public string Description { set; get; }
        public string CATEGORY_CODE { set; get; }
    }
}
