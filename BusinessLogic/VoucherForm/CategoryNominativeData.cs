﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.VoucherForm
{
    [Serializable]
    public class CategoryNominativeData
    {
        public string CategoryCode { get; set; }
        public int GroupSeq { get; set; }
    }
}
