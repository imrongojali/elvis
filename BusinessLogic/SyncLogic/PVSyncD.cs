﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.SyncLogic
{
    [Serializable]
    public class PVSyncD
    {
        public int SyncId { get; set; }
        public string PVTowass { get; set; }
        public int PVYear { get; set; }
        public int? PVNo { get; set; }
        public int SeqNo { get; set; }
        public string CostCenter { get; set; }
        public string CurrCode { get; set; }
        public string Description { get; set; }
        public Decimal? Amount { get; set; }
        public Decimal? PPNAmount { get; set; }
        public Decimal? DPPAmount { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public string TaxNo { get; set; }
        public string TaxCd { get; set; }
        public string TaxAssignment { get; set; }
    }
}
