﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BusinessLogic.SyncLogic
{
    [Serializable]
    public class PVSyncAtt
    {
        public int SyncId { get; set; }
        public string PVTowass { get; set; }
        public int PVYear { get; set; }
        public int? PVNo { get; set; }
        public int SeqNo { get; set; }
        public string FileName { get; set; }
    }
}
