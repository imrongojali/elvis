﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.Data._20MasterData;
using Common.Function;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data;
using DataLayer.Model;
using Common.Data;
using System.IO;
using BusinessLogic.CommonLogic;
using System.Diagnostics;
using System.Data.Objects.SqlClient;

namespace BusinessLogic._20MasterData
{
    public class ItemTransactionLogic : LogicBase
    {

        #region Search Inquiry
        public List<ItemTransactionData> SearchInqury(string _txCd, string _itemTransCd, string _itemTransDesc)
        {
            List<ItemTransactionData> result = new List<ItemTransactionData>();
            using (ContextWrap co = new ContextWrap()) 
            try
            {
                var _db = co.db;
                //default ignore all searching criteria
                bool ignoreTransactionCd = true;
                bool ignoreTransactionDesc = true;
                bool ignoreItemDescFirstLast = true;
                bool ignoreItemDescFirst = true;
                bool ignoreItemDescLast = true;
                bool ignoreItemDescMiddle = true;

                
                int TrCode = 0;
                if (!string.IsNullOrEmpty(_itemTransCd) && Convert.ToInt32(_itemTransCd) != 0)
                {
                    ignoreTransactionCd = false;
                    TrCode = Convert.ToInt16(_itemTransCd);
                }

                int TxCode = 0;
                if (!_txCd.isEmpty())
                {
                    TxCode = _txCd.Int();
                }

                string itemDescFirst = "";
                string itemDescLast = "";
                if (!string.IsNullOrEmpty(_itemTransDesc))
                {
                    if (_itemTransDesc.Contains('*'))
                    {
                        if (_itemTransDesc.StartsWith("*") && _itemTransDesc.EndsWith("*"))
                        {
                            _itemTransDesc = _itemTransDesc.Replace('*', ' ');
                            ignoreItemDescFirstLast = false;
                        }
                        else
                        {
                            if (_itemTransDesc.StartsWith("*"))
                            {
                                _itemTransDesc = _itemTransDesc.Replace('*', ' ');
                                ignoreItemDescLast = false;
                            }
                            else if (_itemTransDesc.EndsWith("*"))
                            {
                                _itemTransDesc = _itemTransDesc.Replace('*', ' ');
                                ignoreItemDescFirst = false;
                            }
                            else
                            {
                                int idx = _itemTransDesc.IndexOf('*');
                                _itemTransDesc = _itemTransDesc.Trim();
                                itemDescFirst = _itemTransDesc.Substring(0, idx - 1);
                                itemDescLast = _itemTransDesc.Substring(idx + 1);
                                ignoreItemDescMiddle = false;
                            }
                        }
                    }
                    else
                    {
                        ignoreTransactionDesc = false;
                    }
                }

                var q = (from p in _db.TB_M_ITEM_TRANSACTION select p);
                if (TxCode != 0)
                {
                    q = q.Where(a => a.TRANSACTION_CD == TxCode);
                }
                if (!ignoreTransactionCd) 
                    q = q.Where(p=>p.ITEM_TRANSACTION_CD == TrCode);
                _itemTransDesc = _itemTransDesc.Trim();
                if (!ignoreTransactionDesc) 
                    q = q.Where(p=> p.ITEM_TRANSACTION_DESC.Contains(_itemTransDesc));
                if (!ignoreItemDescFirstLast)
                    q = q.Where(p=>p.ITEM_TRANSACTION_DESC.Contains(_itemTransDesc));
                if (!ignoreItemDescFirst) q = q.Where(p => p.ITEM_TRANSACTION_DESC.StartsWith(_itemTransDesc));
                if (!ignoreItemDescLast) q= q.Where(p => p.ITEM_TRANSACTION_DESC.EndsWith(_itemTransDesc));
                if  (!ignoreItemDescMiddle) q =  q.Where(p => p.ITEM_TRANSACTION_DESC.StartsWith(itemDescFirst) && p.ITEM_TRANSACTION_DESC.EndsWith(itemDescLast));
                         

                if (q.Any())
                {
                    q = q.OrderBy(a => a.TRANSACTION_CD).ThenBy(a=> a.ITEM_TRANSACTION_CD)
                        .ThenByDescending(a => a.CREATED_DT);
                    foreach (var data in q)
                    {
                        ItemTransactionData _data = new ItemTransactionData();
                        _data.TRANSACTION_CD = data.TRANSACTION_CD;
                        _data.ITEM_TRANSACTION_CD = data.ITEM_TRANSACTION_CD;
                        _data.ITEM_TRANSACTION_DESC = data.ITEM_TRANSACTION_DESC;
                        _data.DPP_FLAG = data.DPP_FLAG;
                        _data.CREATED_BY = data.CREATED_BY;
                        _data.CREATED_DT = data.CREATED_DT;
                        _data.CHANGED_BY = data.CHANGED_BY;
                        _data.CHANGED_DT = data.CHANGED_DT;
                        result.Add(_data);
                    }
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }

            return result;
        }


        #endregion

        #region Delete Item Transaction
        public bool Delete(List<string> listOfTransCode)
        {
            bool _result = false;

            DbTransaction T1 = null;
            using (ContextWrap co = new ContextWrap()) 
            {
                try
                {
                    var _db = co.db;

                    T1 = _db.Connection.BeginTransaction();

                    foreach (string itemTransCode in listOfTransCode)
                    {
                        if (!itemTransCode.Contains(':')) continue;
                        string [] a = itemTransCode.Split(':');
                        if (a.Length < 2) continue;
                        int txCd = a[0].Int();
                        int itemTransCd = a[1].Int();
                        
                        var q = (from i in _db.TB_M_ITEM_TRANSACTION 
                                 where i.ITEM_TRANSACTION_CD == itemTransCd 
                                 && i.TRANSACTION_CD == txCd
                                 select i).FirstOrDefault();
                        if (q != null)
                        {
                            _db.DeleteObject(q);
                            _db.SaveChanges();
                        }
                    }
                    _result = true;
                    T1.Commit();
                }
                catch (Exception Ex)
                {
                    T1.Rollback();
                    Handle(Ex);
                }

            }
            return _result;
        }
        #endregion

        #region Add Item Transaction
        public bool InsertItemTransaction(string txCode, string itemTransCode, string itemTransDesc, int dppFlag, UserData userData)
        {
            bool result = false;
            try
            {
                using (ContextWrap co = new ContextWrap()) 
                {
                    var db = co.db;
                    TB_M_ITEM_TRANSACTION _header = new TB_M_ITEM_TRANSACTION();
                    _header.TRANSACTION_CD = txCode.Int();
                    _header.ITEM_TRANSACTION_CD = Convert.ToByte(itemTransCode);
                    _header.ITEM_TRANSACTION_DESC = itemTransDesc;
                    _header.DPP_FLAG = dppFlag;
                    _header.CREATED_BY = userData.USERNAME;
                    _header.CREATED_DT = DateTime.Now;

                    db.AddToTB_M_ITEM_TRANSACTION(_header);
                    db.SaveChanges();

                    result = true;
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }
        #endregion

        #region Edit Item Transaction Type
        public bool EditItemTransaction(string txCode, string itemTransCode, string itemTransDesc, int dppFlag, UserData userData)
        {
            bool result = false;
            try
            {
                using (ContextWrap co = new ContextWrap()) 
                {
                    var db = co.db;
                    byte trCode = Convert.ToByte(itemTransCode);
                    int trxCode = txCode.Int(0); 
                    var _header = (from i in db.TB_M_ITEM_TRANSACTION
                                   where i.ITEM_TRANSACTION_CD == trCode && i.TRANSACTION_CD == trxCode
                                   select i).FirstOrDefault();

                    if (_header != null)
                    {
                        _header.ITEM_TRANSACTION_DESC = itemTransDesc;
                        _header.DPP_FLAG = dppFlag;
                        _header.CHANGED_BY = userData.USERNAME;
                        _header.CHANGED_DT = DateTime.Now;

                        db.SaveChanges();
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Handle(ex);
            }
            return result;
        }
        #endregion


        public string TxName(int code)
        {
            using (ContextWrap co = new ContextWrap())
            {
                var _db = co.db;
                var q = (from t in _db.TB_M_TRANSACTION_TYPE
                         where t.TRANSACTION_CD == code
                         select t.TRANSACTION_NAME).FirstOrDefault();
                return q ?? code.str();
            }
        }

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<ItemTransactionData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Item_Transaction");
            x.PutHeader("Item Transaction",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Transaction Type|Item Code|Item Transaction Description|DPP|Created{Date|By}|Changed{Date|By}",
                _imagePath);
            int rownum = 0;
            foreach (ItemTransactionData dx in _list)
            {
                x.Put(++rownum,
                    TxName(dx.TRANSACTION_CD),
                    dx.ITEM_TRANSACTION_CD,
                    dx.ITEM_TRANSACTION_DESC,
                    ((dx.DPP_FLAG??0) != 0 ? "yes": "no"),
                    dx.CREATED_DT.fmt(StrTo.TO_MINUTE),
                    dx.CREATED_BY,
                    dx.CHANGED_DT.fmt(StrTo.TO_MINUTE),
                    dx.CHANGED_BY
                    );
            }
            x.PutTail();
            return x.GetBytes();
        }

        #endregion

    }
}
