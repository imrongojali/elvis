﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.EntityClient;
using System.Data.Objects;
using Common.Data;
using Common.Data._20MasterData;
using Common.Function;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Diagnostics;
using DataLayer.Model;
using BusinessLogic.CommonLogic;
using BusinessLogic;
using Common.Data.SAPData;
using Dapper;

namespace BusinessLogic._20MasterData
{
    public class BankAccountLogic : LogicBase
    {
        #region Search Inquiry

        public List<BankAccountData> Search(string _vendorCd, int _vendorGroupCd, string _searchTerms, string _name)
        {
            return Qx<BankAccountData>("GetBankAccountDataSearch",
                    new
                    {
                        code = _vendorCd,
                        group = _vendorGroupCd,
                        terms = _searchTerms,
                        name = _name
                    }
                    ).ToList();
        }
        #endregion

        public string InternalGroup()
        {
            return Logic.Sys.GetText("VENDOR_GROUP_CODE", "VENDOR_GROUP_INTERNAL");
        }

        #region Get String Builder

        public byte[] Get(string _username, string _imagePath, List<BankAccountData> _list)
        {
            ExcelWriter x = new ExcelWriter();
            x.PutPage("Bank Account");
            x.PutHeader("Bank Account",
                "Downloaded By : " + _username + ExcelWriter.COL_SEP +
                "Downloaded Date : " + DateTime.Now.ToString("dd MMM yyyy") + ExcelWriter.COL_SEP +
                "Total Record Download : " + _list.Count,
                "No|Vendor Code|Vendor Name|Vendor Group|Search Terms|Bank|Account|Beneficiaries|Status|" +
                "Created{Date|By}",
                _imagePath);
            int rownum = 0;

            foreach (BankAccountData dx in _list)
            {
                x.Put(++rownum,
                    dx.vendor_cd,
                    dx.vendor_name,
                    dx.vendor_group_name,
                    dx.search_terms,
                    dx.name_of_bank,
                    dx.bank_account,
                    dx.beneficiaries,
                    dx.status,
                    dx.created_dt.fmt(StrTo.TO_MINUTE), dx.created_by
                    );
            }
            
            x.PutTail();
            return x.GetBytes();
        }

        #endregion
    }
}
