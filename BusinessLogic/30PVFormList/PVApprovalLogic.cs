﻿using System;
using System.Collections.Generic;
using System.Linq;
using BusinessLogic._30PVList;
using Common;
using Common.Data;
using Common.Data._20MasterData;
using Common.Data._30PVFormList;
using Common.Function;

namespace BusinessLogic._30PVFormList
{
    public class PVApprovalLogic : LogicBase
    {
        private LogicFactory logic = LogicFactory.Get();

        #region Search Inquiry
        public decimal GetPVTotalAmount(int pvNo, int pvYear)
        {
            decimal? r = Qx<decimal>("GetTotalAmountByReff", new { no = pvNo, year = pvYear }).FirstOrDefault();

            return r ?? 0;
        }

        public void setCancel(int pvNo, int pvYear, int v, string userName = "setCancel")
        {
            int r = UQx<int>("SetCancelPV", new { docNo = pvNo, docYear = pvYear, cancel = v , username = userName }).FirstOrDefault();
        }

        public List<PVApprovalData> SearchDetail(string pvNoParam, string pvYearParam)
        {
            List<PVApprovalData> result = new List<PVApprovalData>();

            //default ignore all searching criteria

            int pvNo = pvNoParam.Int();
            int pvYear = pvYearParam.Int();

            var qr = Qu<PVApprovalData>("PvApprovalDetail", pvNo, pvYear).ToList();
            if (qr != null && qr.Count > 0)
            {
                foreach (var d in qr)
                {
                    PVApprovalData p = d.Clone();
                    p.AMOUNT = CommonFunction.Eval_Curr(p.CURRENCY_CD, p.AMOUNT_VALUE);
                    result.Add(p);
                }
            }
            return result;
        }

        public int? SearchStatusFromDistribution(string pvNoParam, string pvYearParam, string userName)
        {
            decimal reffNo = (pvNoParam + pvYearParam).Dec();
            int? resultData = null;
            resultData = Qx<int?>("GetStatusFromDistribution", new { reffNo = reffNo, username = userName }).First();

            return resultData;
        }

        public decimal? GetSuspenseAmount(string pvNo, string pvYear)
        {
            int _pv_no, _pv_year;
            decimal? totalSuspense = null;

            _pv_no = pvNo.Int();
            _pv_year = pvYear.Int();
            totalSuspense = Qx<decimal?>("GetSuspenseAmountBySettlement", new { no = _pv_no, year = _pv_year }).FirstOrDefault();

            return totalSuspense;
        }

        public PVApprovalData Search(string aNo, string aYear)
        {
            PVApprovalData r = null;

            int docNo = aNo.Int();
            int docYear = aYear.Int();

            List<PVApprovalData> l = Qu<PVApprovalData>("PvApprovalHeader", docNo, docYear).ToList();
            if ((l != null) && (l.Count > 0))
                r = l[0];

            return r;
        }

        public int SuspenseCountByDiv(int DivCd)
        {
            return Qx<int>("GetSuspenseCountByDiv", new { DivCd = DivCd }).FirstOrDefault();
        }

        public int GetUnsettledSuspenseByDiv(int DivCd)
        {
            int? r = Qu<int>("UnsettledSuspenseByDiv", DivCd).FirstOrDefault();

            return r ?? 0;
        }

        public List<VendorSuspenses> GetVendorSuspensesByDiv(int DivCd)
        {
            return Qx<VendorSuspenses>("GetVendorSuspensesByDiv", new { DivCd = DivCd }).ToList();
        }

        public List<AttachmentData> SearchAttachmentInqury(String pvNoParam, String pvYearParam)
        {
            string refNo = String.Concat(pvNoParam, pvYearParam);
            List<AttachmentData> returnData = new List<AttachmentData>();

            returnData = Qx<AttachmentData>("GetAttachmentInquiry", new { refNo = refNo }).ToList();
            if (returnData != null)
            {
                foreach (AttachmentData r in returnData)
                {
                    r.URL = CommonFunction.CombineUrl(AppSetting.FTP_DOWNLOADHTTP, r.DIRECTORY, r.FILE_NAME);
                }
            }
            return returnData;
        }

        #endregion

        #region hold button

        public bool ChangeHoldValue(String pvNoParam,
                                    String pvYearParam,
                                    String userNameParam,
                                    ref ErrorData err,
                                    int? HoldFlag = null,
                                    int HoldValue = 0)
        {
            int r = UQx<int>("SetHoldValuePV", new { docNo = pvNoParam, docYear = pvYearParam, username = userNameParam, holdFlag = HoldFlag, holdValue = HoldValue }).FirstOrDefault();
            return (r != 0);
        }

        /* changed to use sql
        public bool ChangeHoldValue(String pvNoParam,
                                    String pvYearParam,
                                    String userNameParam,
                                    ref ErrorData err,
                                    int? HoldFlag = null,
                                    int HoldValue = 0)
        {
            Int32 pvNo;
            Int16 pvYear;
            //Int32 holdFlag;
            bool flag = true;

            #region build searching criteria
            if (!string.IsNullOrEmpty(pvNoParam))
                pvNo = Int32.Parse(pvNoParam);
            else
                pvNo = 0;

            if (!string.IsNullOrEmpty(pvYearParam))
                pvYear = Int16.Parse(pvYearParam);
            else
                pvYear = 0;

            #endregion
            using (ContextWrap co = new ContextWrap())
            {
                var db = co.db;
                var data = (from i in db.TB_R_PV_H
                            where i.PV_NO == pvNo &&
                                  i.PV_YEAR == pvYear
                            select i).FirstOrDefault();

                Int32 holdFlag = data.HOLD_FLAG ?? 0;
                if (HoldFlag == null)
                {
                    if (holdFlag == 0)
                    {
                        data.HOLD_FLAG = ((HoldValue > 0) ? HoldValue : 1);

                        flag = false;
                    }
                    else if (holdFlag != 0)
                    {
                        data.HOLD_FLAG = (data.HOLD_FLAG ?? 0) * -1;
                    }
                }
                else
                {
                    if (HoldFlag > 0)
                    {
                        data.HOLD_FLAG = ((HoldValue > 0) ? HoldValue : 1);
                    }
                    else
                        data.HOLD_FLAG = (data.HOLD_FLAG ?? 0) * -1;

                    flag = HoldFlag <= 0;
                }

                data.HOLD_BY = userNameParam;
                data.CHANGED_DATE = DateTime.Now;
                data.CHANGED_BY = userNameParam;

                db.SaveChanges();
            }
            return flag;
        }*/

        public static int HoldValueOf(Common.Enum.ApprovalEnum a)
        {
            int r = 0;
            switch (a)
            {
                case Common.Enum.ApprovalEnum.Hold: r = 1; break;
                case Common.Enum.ApprovalEnum.UnHold: r = 0; break;
                default: r = 0; break;
            }
            return r;
        }

        #endregion

        public bool GetUserInfo(string userName, ref string email, ref string roleId)
        {
            bool ret = false;
            UserRoleData q = Qx<UserRoleData>("GetUserInfoByName", new { USERNAME = userName }).FirstOrDefault();

            if (q != null)
            {
                email = q.EMAIL;
                roleId = q.ROLE_ID;
                ret = true;
            }
            return ret;
        }

        // wo.apiyudin 05-06-2020
        public bool IsRejectedPVEBilling(string pvNo)
        {
            bool ret = false;
            int q = Qx<int>("GetIsRejectedPVEBilling", new { PV_NO = pvNo }).FirstOrDefault();

            if (q != 1)
            {
                ret = false;
            }
            else
            {
                ret = true;
            }

            return ret;
        }

        public HashSet<string> GetEmailUserDealer(string pvNo, string pvYear)
        {
            HashSet<string> ret = null;

            List<string> q = Qx<string>("GetEmailUserDealer", new { PV_NO = pvNo, PV_YEAR = pvYear }).ToList();

            if (q.Count > 0)
            {
                ret = new HashSet<string>(q);
            }

            return ret;
        }

        public string GetValBySystemCd(string systemCd)
        {
            string ret = null;
            string q = Qx<string>("GetValBySystemCd", new { SYSTEM_CD = systemCd }).FirstOrDefault();

            if (q != null)
            {
                ret = q;
            }
            return ret;
        }        
        //end

        public void SetPaidDate(string pvNo, string pvYear, string UserName = "")
        {
            Do("SetPaidDate", new { docno = pvNo, docyear = pvYear, username = UserName });
        }

        public void InsertTableHistory(string pvNoParam, string pvYearParam)
        {
            UQx<int>("SetHistoryPV", new { docNo = pvNoParam, docYear = pvYearParam });
        }

        public string[] GetNewStatus(string pvNoParam, string pvYearParam)
        {
            String[] retVal = new string[] { "", "", "" };
            int no, yy;
            no = pvNoParam.Int(0);
            yy = pvYearParam.Int(0);

            var q = Qu<NewStatusData>("PvStatusDAta", no, yy).ToList();
            if (q != null && q.Count > 0)
                retVal = new string[] { q[0].STATUS_CD.str(), q[0].STATUS_NAME, q[0].PAY_METHOD_CD };

            return retVal;
        }

        public void UpdateSettlement(string pvNoParam, string pvYearParam, string username)
        {
            UQx<int>("SetSettlementPV", new { docNo = @pvNoParam, docYear = @pvYearParam, username = username });
        }

        public string GetFirstUser(string _no, string _year)
        {
            string reffNo = String.Concat(_no, _year);

            return Qx<string>("GetFirstUser", new { reffNo = reffNo }).FirstOrDefault();
        }

        public bool isCanHold(string pvNoParam, string pvYearParam, string userName, string statusParam)
        {
            bool retVal = false;

            try
            {

                if (!string.IsNullOrEmpty(pvNoParam) & !string.IsNullOrEmpty(pvYearParam) && !string.IsNullOrEmpty(statusParam))
                {
                    Int16 status = Int16.Parse(statusParam);
                    decimal reffNo = Decimal.Parse(String.Concat(pvNoParam, pvYearParam));

                    int cCan = Qx<int>("GetCanHoldStatusByReffNoUsername", new { reffNo = reffNo, USERNAME = userName }).First();

                    if (cCan > 0)
                    {
                        retVal = ((status > 20 && status <= 26) ||
                            (status > 43 && status <= 47));
                    }
                }
            }
            catch (Exception ex)
            {
                LoggingLogic.err(ex);
            }

            return retVal;
        }

        public bool isCanHold(string pvNoParam, string pvYearParam, string userName, string statusParam, string btnText)
        {
            int canHold = 0;
            if (!string.IsNullOrEmpty(pvNoParam)
              && !string.IsNullOrEmpty(pvYearParam)
              && !string.IsNullOrEmpty(statusParam)
              && !string.IsNullOrEmpty(btnText))
            {
                Int16 status = Int16.Parse(statusParam);
                decimal reffNo = Decimal.Parse(String.Concat(pvNoParam, pvYearParam));
                int isHOLD = btnText.Trim().ToUpper().Equals("HOLD") ? 1 : 0;
                canHold = Qu<int>("GetCanHold", reffNo, userName, isHOLD, status).FirstOrDefault();
            }
            return (canHold != 0);
        }

        public bool PVPaid(string txtPVNo, string txtPVYear, int _ProcessId, ref string _Message)
        {
            List<string> pvs = new List<string>();
            pvs.Add(txtPVNo + ":" + txtPVYear);
            return logic.PVList.PVCheck(pvs, _ProcessId, ref _Message, "", PVListLogic.SAP_PAID_BY_CASHIER);
        }

        public bool PVCheck(string txtPVNo, string txtPVYear, int _ProcessId, ref string _Message)
        {
            List<string> pvs = new List<string>();
            pvs.Add(txtPVNo + ":" + txtPVYear);
            return logic.PVList.PVCheck(pvs, _ProcessId, ref _Message);
        }

        public string GetBudgetName(string WBSNo)
        {
            return hQx<string>("GetBudgetName", new { WbsNo = WBSNo }).FirstOrDefault();
        }

        public bool Verified(int docNo, int docYear, UserData u)
        {
            int r = UQx<int>("SetVerifiedPV", new { docNo = docNo, docYear = docYear, username = u.USERNAME }).FirstOrDefault();
            
            return (r > 0);
        }

        public List<string> GetTotalAmounts(int docNo, int docYear)
        {
            List<string> r = new List<string>();
            List<AmountData> q = Qx<AmountData>("GetTotalAmounts", new { no = docNo, year = docYear }).ToList();

            if (q.Any())
            {
                foreach (var x in q)
                {
                    r.Add(CommonFunction.Eval_Curr(x.CURRENCY_CD, x.TOTAL_AMOUNT, 1));
                }
            }

            return r;
        }
    }
}
