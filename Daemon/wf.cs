﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace ElvisWorkflowStatusUpdater
{
    public class WF
    {
        public const int DO_PV_WORKFLOW = 1;
        public const int DO_RV_WORKFLOW = 2;
        public const int DO_DONE_PV = 4;
        public const int DO_DONE_RV = 8;
        public const int DO_SETTLEMENT_STATUS = 16;
        public const int DO_VENDOR_SUSPENSE_COUNT = 32;
        public static DateTime lastLogDate = new DateTime(1856, 07, 10);
        public const string logname = "Workflow.log";
        public const string REFF_NUMBER = "@ReffNumber";
        public const string DOC_NO = "@Number";
        public const string DOC_YEAR = "@Year";
        public const string WORKFLOW_STATUS = "@WorkflowStatus";
        public const string UPPER_BOUND = "@UpperBound";
        public const string LOWER_BOUND = "@LowerBound";

        // public const int MAX_THREAD = AppSetting.MaxThread ;
        // public const int THREADING_MIN_DATA = 100;
        public delegate void Say(string s, params object[] x);
        public static Say say = Talker(AppSetting.Output);

        public static string _path = GetPath();
        public static string _file = Path.Combine(GetPath(), logname);


        public static Say Talker(int x)
        {
            switch (x)
            {
                case 0:
                    return WF.cono;
                   
                case 1:
                    return WF.debo;
                    
                case 2:
                    {
                       
                        return WF.logo;                        
                    }
                default:
                    return WF.logo;
            }
        }

        public static void Init()
        {
            if (AppSetting.Output == 2)
            {
                ForceDirectories(GetPath());
                ReMoveFile(Path.Combine(GetPath(), logname));
            }                 
        }

        public static string ExceptionMessageTrace(Exception x)
        {
            return x.Message +
                ((x.InnerException != null) ? "\r\n\t" + x.InnerException.Message : "")
                + "\r\n\t" + x.StackTrace;
        }

        public static string GetPath()
        {
            DateTime n = DateTime.Now;
            return Path.Combine(Environment.GetEnvironmentVariable("ALLUSERSPROFILE")
                , AppSetting.LdapDomain, AppSetting.ApplicationId, "log"
                , n.Year.ToString()
                , n.Month.ToString()
                , n.Day.ToString());            
        }
     
        public static string Base26(long l)
        {
            string x = "";
            long n;
            while (l > 0)
            {
                n = (l - 1) % 26;
                x = Convert.ToChar(n + 65).ToString() + x;
                l = (l - n) / 26;
            }
            return x;
        }


        public static void ReMoveFile(string n)
        {
            if (File.Exists(n))
            {
                DateTime d = File.GetCreationTime(n);
                string ft = d.ToString("yyyyMMdd_hhmmss");
                int i = 0;
                string newname;
                do
                {
                    newname = Path.Combine(
                        Path.GetDirectoryName(n), 
                        Path.GetFileNameWithoutExtension(n) 
                        + "_" + ft + ((i>0)? Base26(i):"") 
                        + Path.GetExtension(n));
                    i++;
                } while (File.Exists(newname));
                File.Move(n, newname);
            }
        }

        public static bool ForceDirectories(string d)
        {
            bool r = false;
            try
            {
                if (!Directory.Exists(d))
                {
                    Directory.CreateDirectory(d);
                }
                r = true;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            return r;
        }

        public static string tick(string s, params object[] x)
        {
            DateTime d = DateTime.Now;
            string ts = "        ";
            bool last = false;
            if (!s.isEmpty() && s.StartsWith("\r\n"))
            {
                last = true;
                s = s.Substring(2);
            }
            if ((d - lastLogDate).TotalSeconds >= 1 || last)
            {
                lastLogDate = d;
                ts = d.ToString("HH:mm:ss");
            }
            return ts + "\t" + string.Format(s, x);
        } 

        public static void logo(string s, params object[] x)
        {
            File.AppendAllText(_file, "\r\n" + tick(s, x));
        }

        public static void debo(string s, params object[] x)
        {
            Debug.WriteLine(string.Format(s, x));
        }

        public static void cono(string s, params object[] x)
        {
            Console.WriteLine(string.Format(s, x));
        }

        public static void err(Exception ex)
        {
            say(ExceptionMessageTrace(ex));
        }

    }
}
