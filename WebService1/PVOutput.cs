﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ELVISServices
{
    public class PVOutput
    {
        public int PV_NO { get; set; }
        public int PV_YEAR { get; set; }
        public string STATUS_PV { get; set; }
        public string ERR_MESSAGE { get; set; }
    }
}