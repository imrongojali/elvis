﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using BusinessLogic;
using BusinessLogic.CommonLogic;
using BusinessLogic.VoucherForm;
using Common.Function;
using Common.Data;
using DataLayer.Model;
using Common;

namespace ELVISServices
{
    [Serializable]
    public class PVData
    {
        //protected const string __ELVISCONNECTION_STRING = "ELVIS_DBConnectionString";
        //protected const string __SAPCONNECTION_STRING = "SAP_DBConnectionString";
        protected ELVIS_DBEntities elvisdb = new ELVIS_DBEntities();
        
        //protected readonly TMMIN_ROLEEntities tmmindb;
        //protected readonly SAP_DBEntities sapdb;
        protected WorkFlowLogic workflow = new WorkFlowLogic();
        private Common.Function.GlobalResourceData _resource = new Common.Function.GlobalResourceData();
        
        private static PVFormLogic GetLogic()
        {
            PVFormLogic l = new PVFormLogic();
            return l;
        }

        public PVOutput save(
            string PaymentMethodCode, string VendorCode, int PVTypeCode, 
            int TransactionCode, string BudgetNumber, 
            string ActivityDate, string ActivityDateTo, 
            string DivisionID, DateTime PostingDate, 
            int BankType, decimal ReffNo, 
            int approveLevel, string userName, 
            string SuspenceNo, bool SingleWbsUsed, 
            bool financeHeader, bool submitting, 
            PVDetailData[] detailData)
        {
            #region Init
            
            bool headerSaved = false;            
            string SaveStatus = "S";
            string MessageSts = string.Empty;
            int VendorGroupCode = 1;
            int ProcessID = 0;
            PVDetailData pvd = new PVDetailData();
            PVFormTable pvtab = new PVFormTable();
            //int pvNumber = PVNumber;
            PVFormLogic _pvFormLogic = GetLogic();
            int pvNumber = _pvFormLogic.getDocumentNo();
            //int pvYear = PVYear;
            int pvYear = int.Parse(string.Format("{0:yyyy}", DateTime.Now));
            DateTime today = DateTime.Now;
            //elvisdb = new ELVIS_DBEntities();
            int StatusCode = 0;
            submitting = true;
            decimal totalAmount = getTotalAmountInterface(detailData);
            int procid = 0;
            
            #endregion


            LockingLogic llock = new LockingLogic();
            LoggingLogic llog = new LoggingLogic();
            CheckDoublePost.DoublePost doub = new CheckDoublePost.DoublePost();

            bool isDouble = doub.isDoublePost(TransactionCode, ReffNo);
            
            procid = llog.Log("INF", "", "PVData.Save",userName ,"SAP_HR", procid, "PV from SAP");
            ProcessID = procid;
            
            
            bool isLocking = llock.IsLocked("SAP_HR_Int", TransactionCode.ToString() + "|" + ReffNo.ToString());

            if (isLocking == true)
            {
                pvNumber = 0;
                pvYear = 0;
                SaveStatus = "E";
                MessageSts = "Data has been lock by another process";
            }
            else
            {

                llock.DoLock(ProcessID, "SAP_HR_Int", TransactionCode.ToString() + "|" + ReffNo.ToString(), userName);



                /* Header */
                TB_R_PV_H tblHeader = null;

                try
                {
                    List<TB_R_PV_H> existingHeaders = (from t in elvisdb.TB_R_PV_H
                                                        where (t.PV_NO == pvNumber) && (t.PV_YEAR == pvYear)
                                                        select t).ToList();


                    if (existingHeaders.Any())
                    {
                        tblHeader = existingHeaders[0];
                    }
                    else
                    {
                        tblHeader = new TB_R_PV_H();
                        elvisdb.TB_R_PV_H.AddObject(tblHeader);
                    }



                    if (submitting)
                    {
                        StatusCode = 10;
                    }

                    if (!string.IsNullOrEmpty(VendorCode))
                    {
                        string fmt = "0000000000.##";
                        int vnd = Convert.ToInt32(VendorCode);
                        string vendors = vnd.ToString(fmt);
                        //CodeConstant vendorGroup = logic.getVendorGroupNameByVendor(VendorCode); 
                        CodeConstant vendorGroup = _pvFormLogic.getVendorGroupNameByVendor(vendors);
                        if (vendorGroup.Code == string.Empty)
                        {
                            MessageSts = "Vendor not found in Master Vendor";
                        }
                        VendorGroupCode = Convert.ToInt32(vendorGroup.Code);
                        VendorCode = vendors;
                    }
                    else
                    {
                        VendorCode = null;
                    }

                    if (string.IsNullOrEmpty(ActivityDate) || ActivityDate == "0000-00-00")
                    {
                        ActivityDate = string.Format("{0:G}", DateTime.Now);
                    }
                    if (string.IsNullOrEmpty(ActivityDateTo) || ActivityDateTo == "0000-00-00")
                    {
                        ActivityDateTo = string.Format("{0:G}", DateTime.Now);
                    }

                    if (financeHeader)
                    {
                        StatusCode = 21;
                        tblHeader.POSTING_DATE = PostingDate;
                        //tblHeader.PLANNING_PAYMENT_DATE = PlanningPaymentDate;
                        tblHeader.BANK_TYPE = BankType;
                    }
                    else
                    {
                        tblHeader.PV_NO = pvNumber;
                        tblHeader.PV_YEAR = pvYear;
                        tblHeader.PAY_METHOD_CD = PaymentMethodCode;
                        tblHeader.VENDOR_GROUP_CD = VendorGroupCode;
                        tblHeader.VENDOR_CD = VendorCode;
                        tblHeader.PV_TYPE_CD = PVTypeCode;
                        tblHeader.TRANSACTION_CD = TransactionCode;
                        tblHeader.BUDGET_NO = BudgetNumber;
                        tblHeader.ACTIVITY_DATE_FROM = DateTime.Parse(ActivityDate);
                        tblHeader.ACTIVITY_DATE_TO = DateTime.Parse(ActivityDateTo);
                        tblHeader.DIVISION_ID = DivisionID;
                        tblHeader.PV_DATE = today; //PVDate;
                        tblHeader.REFFERENCE_NO = ReffNo;
                        tblHeader.BANK_TYPE = BankType;
                        tblHeader.POSTING_DATE = PostingDate;
                        tblHeader.CREATED_DATE = today;
                        tblHeader.CREATED_BY = userName; //"SAP_ADMIN";
                        tblHeader.CHANGED_DATE = today;
                        tblHeader.CHANGED_BY = userName; //"SAP_ADMIN";
                        tblHeader.TOTAL_AMOUNT = totalAmount; //(decimal)getTotalAmountInterface(detailData); //(decimal)pvtab.getTotalAmount();

                        if (SuspenceNo != "")
                        {
                            int suspenceNo = Convert.ToInt32(SuspenceNo);
                            tblHeader.SUSPENSE_NO = suspenceNo;
                        }
                    }
                    tblHeader.STATUS_CD = StatusCode;

                    if (isDouble)
                    {
                        pvNumber = 0;
                        pvYear = 0;
                        SaveStatus = "E";
                        MessageSts = "Double Post PV with Transaction Code: " + TransactionCode.ToString() + " Refference No: " + ReffNo.ToString();

                    }

                    else
                    {
                        elvisdb.SaveChanges();
                        headerSaved = true;
                    }
                        
                    //change logic get pv number by tori 20122012
                    //if (headerSaved)
                    //{
                    //    //logic.incrementDocumentNo(); updated by tori 20122012
                    //    logic.getDocumentNo();
                        
                    //}

                }
                catch (Exception ex)
                {
                    headerSaved = false;
                    SaveStatus = "E";
                    pvNumber = 0;
                    pvYear = 0;
                    if (MessageSts != string.Empty)
                    {
                        MessageSts += ex.Message;
                    }
                    else
                    {
                        MessageSts = ex.Message;
                    }
                    
                    llock.DoUnlock(ProcessID);
                    //throw ex;
                }

                try
                {

                    if (!financeHeader && headerSaved)
                    {
                        //List<PVFormDetail> lstDetail = formData.Details;
                        List<PVFormDetail> lstDetail = new List<PVFormDetail>();

                        int count = detailData.Count();



                        /* Detail */
                        List<TB_R_PV_D> existingDetails = (from t in elvisdb.TB_R_PV_D
                                                            where (t.PV_NO == pvNumber) && (t.PV_YEAR == pvYear)
                                                            select t).ToList();
                        List<TB_M_GL_ACCOUNT_MAPPING> lstGLMapping = (from t in elvisdb.TB_M_GL_ACCOUNT_MAPPING select t).ToList();

                        bool syncBudgetNo = (BudgetNumber != null) && (!BudgetNumber.Equals(""));
                        int maxSequenceNumber = 1;
                        var seqNum = (from t in elvisdb.TB_R_PV_D select t.SEQ_NO);
                        if (seqNum.Any())
                        {
                            maxSequenceNumber = seqNum.Max();
                        }

                        TB_R_PV_D tblDetail;
                        for (int xx = 0; xx < count; xx++)
                        {
                            maxSequenceNumber++;
                            pvd = new PVDetailData();
                            pvd = detailData[xx];
                            tblDetail = new TB_R_PV_D()
                            {
                                SEQ_NO = maxSequenceNumber,
                                PV_NO = pvNumber,
                                PV_YEAR = pvYear,
                                AMOUNT = (decimal)pvd.Amount,
                                COST_CENTER_CD = pvd.CostCenterCode,
                                CURRENCY_CD = pvd.CurrencyCode,
                                TAX_CD = null, //pvd.TaxCode,
                                TAX_NO = null, //pvd.TaxNumber,
                                ITEM_TRANSACTION_CD = 1, //pvd.ItemTransaction,
                                DESCRIPTION = pvd.Description,
                                CREATED_BY = userName, //"SAP_ADMIN",
                                CREATED_DT = today,
                                CHANGED_DT = today,
                                CHANGED_BY = userName //"SAP_ADMIN"
                            };

                            foreach (TB_M_GL_ACCOUNT_MAPPING gl in lstGLMapping)
                            {
                                if ((tblHeader.TRANSACTION_CD == gl.TRANSACTION_CD) &&
                                    (tblDetail.ITEM_TRANSACTION_CD == gl.ITEM_TRANSACTION_CD))
                                {
                                    tblDetail.GL_ACCOUNT = gl.GL_ACCOUNT;
                                    break;
                                }
                            }
                            if (syncBudgetNo)
                            {
                                // tblDetail.WBS_NO = BudgetNumber;
                            }

                            elvisdb.TB_R_PV_D.AddObject(tblDetail);
                        }

                        PVFormData formData = new PVFormData();
                        /* Amount */
                        List<TB_R_PVRV_AMOUNT> existingAmounts = (from t in elvisdb.TB_R_PVRV_AMOUNT
                                                                  where (t.DOC_NO == pvNumber) && (t.DOC_YEAR == pvYear)
                                                                  select t).ToList();
                        OrderedDictionary mapTotalAmount = createTotalAmountMap(detailData);
                        if (existingAmounts.Any())
                        {
                            object objAmount;
                            foreach (TB_R_PVRV_AMOUNT d in existingAmounts)
                            {
                                objAmount = mapTotalAmount[d.CURRENCY_CD];
                                if (objAmount == null)
                                {
                                    elvisdb.TB_R_PVRV_AMOUNT.DeleteObject(d);
                                }
                                else
                                {
                                    d.TOTAL_AMOUNT = Convert.ToDecimal(objAmount);
                                    d.CHANGED_BY = userName;//userData.USERNAME;
                                    d.CHANGED_DATE = today;
                                }
                            }
                            elvisdb.SaveChanges();
                        }

                        FormPersistence formPers = new FormPersistence();
                        List<ExchangeRate> exchangeRates = formPers.getExchangeRates();
                        existingAmounts = (from t in elvisdb.TB_R_PVRV_AMOUNT
                                           where (t.DOC_NO == pvNumber) && (t.DOC_YEAR == pvYear)
                                           select t).ToList();
                        bool processed;
                        TB_R_PVRV_AMOUNT tblAmount;
                        ICollection lstKey = mapTotalAmount.Keys;
                        foreach (string key in lstKey)
                        {
                            processed = false;
                            foreach (TB_R_PVRV_AMOUNT amount in existingAmounts)
                            {
                                if (key.Equals(amount.CURRENCY_CD))
                                {
                                    processed = true;
                                    break;
                                }
                            }

                            if (!processed)
                            {
                                tblAmount = new TB_R_PVRV_AMOUNT()
                                {
                                    CHANGED_BY = userName, //userData.USERNAME,
                                    CHANGED_DATE = today,
                                    CREATED_BY = userName, //userData.USERNAME,
                                    CREATED_DATE = today,
                                    CURRENCY_CD = key,
                                    DOC_NO = pvNumber,
                                    DOC_YEAR = pvYear,
                                    TOTAL_AMOUNT = Convert.ToDecimal(mapTotalAmount[key])
                                };

                                foreach (ExchangeRate xrate in exchangeRates)
                                {
                                    if (xrate.CurrencyCode.Equals(key))
                                    {
                                        tblAmount.EXCHANGE_RATE = (decimal)xrate.Rate;
                                        break;
                                    }
                                }

                                elvisdb.TB_R_PVRV_AMOUNT.AddObject(tblAmount);
                            }
                        }

                        elvisdb.SaveChanges();
                    }
                }

                catch (Exception ex)
                {
                    SaveStatus = "E";
                    MessageSts = ex.Message;
                    llock.DoUnlock(ProcessID);
                    //throw ex;
                }

                postToK2Interface(pvNumber, pvYear, DivisionID, userName, TransactionCode, StatusCode, VendorCode, totalAmount, true, approveLevel);
                llock.DoUnlock(ProcessID);
            }
            
            PVOutput retVal = new PVOutput();
            retVal.PV_NO = pvNumber;
            retVal.PV_YEAR = pvYear;
            retVal.STATUS_PV = SaveStatus;
            retVal.ERR_MESSAGE = MessageSts;

            return retVal;
        }

        public decimal getTotalAmountInterface(PVDetailData[] detail)
        {
            PVFormLogic Pvl = new PVFormLogic();
            PVDetailData pvd = new PVDetailData();
            List<ExchangeRate> lstExchangeRate = Pvl.getExchangeRates();
            decimal totalIDR = 0;
            decimal amount;

            int count = detail.Count();

            for (int i = 0; i < count; i++)
            {
                pvd = new PVDetailData();
                pvd = detail[i];
                amount = Convert.ToDecimal(pvd.Amount);

                if (pvd.CurrencyCode.ToUpper().Equals("IDR"))
                {
                    totalIDR += amount;
                }

                else
                {
                    foreach (ExchangeRate rate in lstExchangeRate)
                    {
                        if (rate.CurrencyCode.Equals(pvd.CurrencyCode))
                        {
                            totalIDR += amount * rate.Rate;
                            break;
                        }
                    }
                }
            };                

            return totalIDR;
        }

        public OrderedDictionary createTotalAmountMap(PVDetailData[] detail)
        {
            OrderedDictionary map = new OrderedDictionary();
            PVDetailData pvd = new PVDetailData();
            object objAmount;
            decimal totalAmount = 0;
            int count = detail.Count();
            for (int i = 0; i < count;i++ )
            {
                pvd = new PVDetailData();
                pvd = detail[i];

                if (!string.IsNullOrEmpty(pvd.CurrencyCode))
                {
                    objAmount = map[pvd.CurrencyCode];
                    if (objAmount == null)
                    {
                        totalAmount = 0;
                    }
                    else
                    {
                        totalAmount = objAmount.Dec();
                    }

                    map[pvd.CurrencyCode] = totalAmount + pvd.Amount;
                }
            }

            return map;
        }

        #region K2

        public bool postToK2Interface(
            int PVNumber, int PVYear, 
            string divisionCD, 
            string UserName, 
            int TransCode, 
            int statusCD, 
            string vendorCD, 
            decimal totalAmount, 
            bool payingVoucher, 
            int appLevel)
        {
            try
            {
                string moduleCode = "1";
                bool k2success = false;
                if (!payingVoucher)
                {
                    moduleCode = "2";
                }
                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", "ELVIS"); //Common.AppSetting.ApplicationID);
                submitData.Add("ApprovalLevel", "1");
                submitData.Add("ApproverClass", "1");
                submitData.Add("ApproverCount", "1");
                submitData.Add("CurrentDivCD", divisionCD);
                submitData.Add("CurrentPIC", UserName);

                string entertain = "0";
                int? transactionCode = TransCode;
                List<int?> lstEntertainmentTransactions = getEntertainmentTransactionCodes();
                if (lstEntertainmentTransactions != null)
                {
                    foreach (int? c in lstEntertainmentTransactions)
                    {
                        if (transactionCode == c)
                        {
                            entertain = "1";
                            break;
                        }
                    }
                }
                submitData.Add("Entertain", entertain);

                submitData.Add("K2Host", "");
                submitData.Add("LimitClass", "0");
                submitData.Add("ModuleCD", moduleCode);
                submitData.Add("NextClass", "0");
                submitData.Add("PIC", divisionCD);
                submitData.Add("Reff_No", PVNumber.ToString() + PVYear.ToString());
                string registerDate = string.Format("{0:G}", DateTime.Now);
                submitData.Add("RegisterDt", registerDate);

                PVFormData formData = new PVFormData();
                formData.PVNumber = PVNumber;
                formData.PVYear = PVYear;

                decimal? prevDocStatus = getPreviousDocumentStatus(formData);
                string val = "0";
                if ((prevDocStatus != null) && (prevDocStatus.Value == 0))
                {
                    val = "1";
                }
                submitData.Add("RejectedFinance", val);
                submitData.Add("StatusCD", statusCD.ToString());
                submitData.Add("StepCD", "");
                submitData.Add("VendorCD", vendorCD);               
                submitData.Add("TotalAmount", totalAmount.ToString());

                UserData userData = new UserData();
                userData.USERNAME = UserName;
                userData.DIV_CD = decimal.Parse(divisionCD);
                string commands = _resource.GetResxObject("K2ProcID", "Form_Registration_Submit_New");
                

                
                k2success =  workflow.K2SendCommandWithoutCheckWorklist(PVNumber.ToString() + PVYear.ToString(),
                                                                userData, commands, submitData);

                System.Threading.Thread.Sleep(5000);
                k2success = pvApprovalInterface(PVNumber.ToString() + PVYear.ToString(), UserName, divisionCD, appLevel);

                return k2success;

                
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return false;
        }

        public List<int?> getEntertainmentTransactionCodes()
        {
            var q = from t in elvisdb.TB_M_SYSTEM
                    where t.SYSTEM_TYPE.Equals("K2_POSTING") && t.SYSTEM_CD.Equals("ENTERTAINMENT_TRANSACTION")
                    select t.SYSTEM_VALUE_NUM;
            if (q != null)
            {
                return (List<int?>)q.ToList();
            }

            return null;
        }
        public int? getPreviousDocumentStatus(PVFormData formData)
        {
            decimal referenceNumber = decimal.Parse(formData.PVNumber.ToString() + formData.PVYear.ToString());
            var seqNumber = (from t in elvisdb.TB_H_DISTRIBUTION_STATUS
                             where (t.REFF_NO == referenceNumber)
                             select t.SEQ_NO);
            if (seqNumber.Any())
            {
                int? statusCode = (from t in elvisdb.TB_H_DISTRIBUTION_STATUS
                                   where (t.REFF_NO == referenceNumber) && (t.SEQ_NO == seqNumber.Max())
                                   select t.SEQ_NO).Max();
                return statusCode;
            }

            return null;
        }

        public bool pvApprovalInterface(string _DocNo, string userName, string divCD, int ApproveLvl)
        {
            bool _RetVal = false;
            string posID = string.Empty;
            string _Command = _resource.GetResxObject("K2ProcID", "Form_Registration_Approval");
            int i = 2;
            
            //for (int i = 2; i <= ApproveLvl; i++)
            while(i <= ApproveLvl)
            {

                if (i == 2)
                {
                    posID = "SH";
                }
                else if (i == 3)
                {
                    posID = "DPH";
                }
                else if (i == 4)
                {
                    posID = "DH";
                }
                else if (i == 5)
                {
                    posID = "SGM";
                }

                

                var userApprove = (from v in elvisdb.vw_User
                                   where (v.DIVISION_ID == divCD) && (v.POSITION_ID == posID)
                                   select v.USERNAME).FirstOrDefault();

                UserData userData = new UserData();
                userData.USERNAME = userApprove;

                try
                {
                    Dictionary<string, string> _DataField = new Dictionary<string, string>();
                    _DataField.Add("REFF_NO", _DocNo);
                    //_DataField.Add("Comment", _Comment);
                    //_DataField.Add("EmailFrom", _UserData.EMAIL);
                    //_DataField.Add("PICFullName", _UserData.FIRST_NAME + " " + _UserData.LAST_NAME);
                    _DataField.Add("CurrentPIC", userData.USERNAME);
                    _RetVal = new WorkFlowLogic().K2SendCommandWithCheckWorklist(_DocNo, userData, _Command, _DataField);
                }
                catch (Exception Ex)
                {
                    throw Ex;
                }

                System.Threading.Thread.Sleep(2000);
                i++;

            }
            return _RetVal;

        }



        #endregion
       

    }
        
}