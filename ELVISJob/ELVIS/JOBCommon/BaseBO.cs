﻿using DataLayer.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using BusinessLogic.SyncLogic;
using Dapper;
using BusinessLogic.CommonLogic;
using Common.Function;

namespace ELVISJob.ELVIS.JOBCommon
{
    public class BaseBO : BaseClass
    {
        protected JobFunctions jo = new JobFunctions();
        protected MessageLogic meLo = new MessageLogic();
        protected ELVIS_DBEntities _db = new ELVIS_DBEntities();
        protected readonly GlobalResourceData resx = new GlobalResourceData();

        public void SetProcessId(int pid)
        {
            ProcessId = pid;
        }
        public void SetFuncId(string fid)
        {
            FuncId = fid;
        }
        public string GetFuncId()
        {
            return FuncId;
        }

        private IDbConnection _DB = null;
        protected IDbConnection DB
        {
            get
            {
                if (_DB == null)
                {
                    _DB = new SqlConnection(ConfigurationManager.ConnectionStrings["ELVIS_DBConnectionString"].ConnectionString);
                }
                return _DB;
            }
        }
        public int PutLog(string msgType, string msg, string loc, int ID = 0)
        {
            loc = FuncId + " " + loc;

            if (ID < 1 && ProcessId > 0)
                ID = ProcessId;
            else if (ID > 0)
                ProcessId = ID;

            return Log(msgType, msg, loc, FuncId, ID);
        }
        public int Log(
            string _msgId,
            string _errMessage,
            string _errLocation = "",
            string _functionID = "",
            int ID = 0,
            string _remarks = null,
            string _type = null,
            string _sts = null)
        {

            DynamicParameters dyn = new DynamicParameters();
            dyn.Add("@what", _errMessage);
            dyn.Add("@user", UID);
            dyn.Add("@where", _errLocation);
            dyn.Add("@pid", ID, DbType.Int32, ParameterDirection.InputOutput);
            dyn.Add("@id", _msgId);
            dyn.Add("@type", _type);
            dyn.Add("@func", _functionID);
            dyn.Add("@sts", _sts);
            dyn.Add("@rem", _remarks);

            DB.Execute("sp_PutLog", dyn, null, null, CommandType.StoredProcedure);
            ID = dyn.Get<int>("@pid");

            return ID;
        }
        public int Exec(
                  string spName
                , dynamic param = null
                , IDbTransaction transaction = null
                , int? commandTimeout = null
                , CommandType? commandType = CommandType.StoredProcedure)
        {
            return DB.Execute(spName, param as object, transaction, commandTimeout, commandType);
        }

        public List<PVSyncH> GetUnsyncPVH(int syncAction)
        {
            List<PVSyncH> listPVH = new List<PVSyncH>();

            var hd = _db.vw_UnsyncPVH;

            if (hd.Any())
            {
                foreach (var data in hd)
                {
                    var pvH = new PVSyncH();

                    pvH.SyncId = data.SYNC_ID;
                    pvH.PVTowass = data.PV_TOWASS;
                    pvH.PVNo = data.PV_NO;
                    pvH.PVYear = data.PV_YEAR ?? -1;
                    pvH.PayMethodCode = data.PAY_METHOD_CD;
                    pvH.VendorCode = data.VENDOR_CD;
                    pvH.PVType = data.PV_TYPE_CD;
                    pvH.TransCode = data.TRANSACTION_CD;
                    pvH.BudgetNo = data.BUDGET_NO;
                    pvH.DivCd = data.DIVISION_ID;
                    pvH.PVDate = data.PV_DATE;
                    pvH.NeedCompare = data.NEED_COMPARE;
                    pvH.TotalAmount = data.TOTAL_AMOUNT;
                    pvH.CreatedBy = data.CREATED_BY;
                    pvH.CreatedDt = data.CREATED_DT;
                    pvH.SyncAction = data.SYNC_ACTION;

                    if (syncAction == SYNC_PV_DRAFT)
                    {
                        pvH.details = GetUnsyncPVD(data.SYNC_ID);
                        pvH.attachments = GetUnsyncAtt(data.SYNC_ID);
                    }

                    listPVH.Add(pvH);
                }
            }

            return listPVH;
        }

        public List<PVSyncD> GetUnsyncPVD(int syncId)
        {
            List<PVSyncD> listPVD = new List<PVSyncD>();

            var det = _db.vw_UnsyncPVD
                    .Where(x => x.SYNC_ID == syncId);

            if (det.Any())
            {
                foreach (var data in det)
                {
                    var pvD = new PVSyncD();

                    pvD.SyncId = data.SYNC_ID;
                    pvD.PVTowass = data.PV_TOWASS;
                    pvD.PVYear = data.PV_YEAR ?? -1;
                    pvD.PVNo = data.PV_NO;
                    pvD.SeqNo = data.SEQ_NO;
                    pvD.CostCenter = data.COST_CENTER_CD;
                    pvD.CurrCode = data.CURRENCY_CD;
                    pvD.Description = data.DESCRIPTION;
                    pvD.Amount = data.AMOUNT;
                    pvD.PPNAmount = data.PPN_AMOUNT;
                    pvD.DPPAmount = data.DPP_AMOUNT;
                    pvD.InvoiceNo = data.INVOICE_NO;
                    pvD.InvoiceDate = data.INVOICE_DATE;
                    pvD.TaxNo = data.TAX_NO;
                    pvD.TaxCd = data.TAX_CD;
                    pvD.TaxAssignment = data.TAX_ASSIGNMENT;

                    listPVD.Add(pvD);
                }
            }

            return listPVD;
        }

        public List<PVSyncAtt> GetUnsyncAtt(int syncId)
        {
            List<PVSyncAtt> listAtt = new List<PVSyncAtt>();

            var det = _db.vw_UnsyncAtt
                    .Where(x => x.SYNC_ID == syncId);

            if (det.Any())
            {
                foreach (var data in det)
                {
                    var att = new PVSyncAtt();

                    att.SyncId = data.SYNC_ID;
                    att.PVTowass = data.PV_TOWASS;
                    att.PVYear = data.PV_YEAR ?? -1;
                    att.PVNo = data.PV_NO;
                    att.SeqNo = data.SEQ_NO;
                    att.FileName = data.FILE_NAME;

                    listAtt.Add(att);
                }
            }

            return listAtt;
        }

        public void CloseSyncFlag(List<PVSyncH> allUnsyncPVH)
        {
            if (allUnsyncPVH == null || allUnsyncPVH.Count == 0)
                return;

            allUnsyncPVH = allUnsyncPVH.Where(x => x.SyncStatus == 1).ToList();

            foreach (var syncPVH in allUnsyncPVH)
            {
                string sql = string.Format(@"update [ELVIS_SYNC_DB].dbo.TB_SYNC_PV_H
                                            set SYNC_STATUS = 1
                                            where SYNC_ID = {0}", syncPVH.SyncId);
                DB.Execute(sql);
            }
        }
    }
}
