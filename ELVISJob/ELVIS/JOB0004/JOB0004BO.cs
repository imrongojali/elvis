﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELVISJob.ELVIS.JOBCommon;
using BusinessLogic.VoucherForm;
using Common.Data;
using DataLayer.Model;
using Dapper;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Data.Common;
using BusinessLogic;
using BusinessLogic.SyncLogic;
using System.IO;
using BusinessLogic.CommonLogic;
using Common.Data.SAPData;

namespace ELVISJob.ELVIS.JOB0004
{
    class JOB0004BO : BaseBO
    {
        public void SyncBudget()
        {
            var listWbs = (from d in _db.TB_R_ACCR_BALANCE
                       where d.PV_TYPE_CD == 4
                       select new SyncSAPData 
                       { 
                           BUDGET_NO = d.WBS_NO_PR,
                           ACTUAL_AMT = d.SPENT_AMT ?? 0,
                           COMMITMENT_AMT = d.OUTSTANDING_AMT ?? 0,
                           AVAILABLE_AMT = d.AVAILABLE_AMT ?? 0
                       }).ToList();

            WriteLog(listWbs, true);

            var sapLogic = new SAPNco3Logic();
            var listWbsSAP = sapLogic.SynchronizeBudget(listWbs, "JOB0004");

            WriteLog(listWbsSAP, false);

            foreach (var wbsSAP in listWbsSAP)
            { 
                var w = (from d in _db.TB_R_ACCR_BALANCE
                             where d.WBS_NO_PR == wbsSAP.BUDGET_NO
                             select d).FirstOrDefault();

                w.SPENT_AMT = wbsSAP.ACTUAL_AMT;
                w.OUTSTANDING_AMT = wbsSAP.COMMITMENT_AMT;
                w.AVAILABLE_AMT = wbsSAP.AVAILABLE_AMT;

                _db.SaveChanges();
            }
        }

        public void WriteLog(List<SyncSAPData> listWbs, bool isOut)
        { 
            if(isOut)
                Say(FuncId, "============= Param Out =============");
            else
                Say(FuncId, "============= Param In =============");

            foreach(var wbs in listWbs)
                Say(FuncId, "Budget: {0}\t Spent: {1::0.00}\t Outstanding: {2::0.00}\t Available: {3::0.00}\t"
                    , wbs.BUDGET_NO
                    , wbs.ACTUAL_AMT
                    , wbs.COMMITMENT_AMT
                    , wbs.AVAILABLE_AMT);
        }
    }
}
