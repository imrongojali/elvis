﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ELVISJob.ELVIS.JOBCommon;
using BusinessLogic.VoucherForm;
using Common.Data;
using DataLayer.Model;
using Dapper;
using System.Data;
using System.Collections.Specialized;
using System.Collections;
using System.Data.Common;
using BusinessLogic;
using BusinessLogic.SyncLogic;
using System.IO;
using BusinessLogic.CommonLogic;

namespace ELVISJob.ELVIS.JOB0003
{
    class JOB0003BO : BaseBO
    {
        DateTime today = DateTime.Now;
        protected int[] NonCostCenterGlAccounts = new int[] { 0 };

        public string[] GetRoundCurrency()
        {
            string roundCurr = jo.GetSysText("INVOICE_UPLOAD", "NO_FRACTION_CURRENCY");

            if (!string.IsNullOrEmpty(roundCurr.Trim()))
            {
                return roundCurr.Split(',');
            }
            else
            {
                return new string[] { "IDR", "JPY" };
            }
        }
        public int getPreviousDocumentStatus(string reff_no)
        {
            string sql = string.Format(@"SELECT COALESCE(MAX(seq_no), 0) max_seq_no
                                        FROM tb_h_distribution_status
                                        WHERE reff_no = {0}
                                        GROUP BY reff_no", reff_no);

            return DB.Query<int>(sql).FirstOrDefault();
        }
        public string getDescriptionDefaultWording(int? transCd)
        {
            if (transCd.HasValue)
            {
                var q = (from m in _db.TB_M_TRANSACTION_TYPE
                         where m.TRANSACTION_CD == transCd
                         select m.STD_WORDING).FirstOrDefault();
                return q;
            }

            return null;
        }

        public string getCostCenterDescription(string code)
        {
            if (code == null)
                return null;

            var q = (from m in _db.vw_CostCenter
                     where m.COST_CENTER == code
                     select m.DESCRIPTION).FirstOrDefault();

            return q;
        }
        public PVFormData searchPV(int pvNo, int pvYear, string userName)
        {
            var p = (from a in _db.TB_R_PV_H
                     where a.PV_NO == pvNo && a.PV_YEAR == pvYear
                     select a).FirstOrDefault();

            PVFormData pv = null;
            if (p != null)
            {
                pv = new PVFormData();

                pv.PVNumber = p.PV_NO;
                pv.PVYear = p.PV_YEAR;

                pv.ActivityDate = p.ACTIVITY_DATE_FROM;
                pv.ActivityDateTo = p.ACTIVITY_DATE_TO;
                pv.BudgetNumber = p.BUDGET_NO;
                pv.DivisionID = p.DIVISION_ID;
                pv.PaymentMethodCode = p.PAY_METHOD_CD;
                pv.PVDate = p.PV_DATE;
                pv.PVTypeCode = p.PV_TYPE_CD;
                pv.StatusCode = p.STATUS_CD;
                pv.SuspenseNumber = p.SUSPENSE_NO;
                pv.SuspenseYear = p.SUSPENSE_YEAR;
                pv.TransactionCode = p.TRANSACTION_CD;
                pv.VendorCode = p.VENDOR_CD;
                pv.VendorGroupCode = p.VENDOR_GROUP_CD;
                pv.HoldingUser = p.HOLD_BY;
                pv.OnHold = (p.HOLD_FLAG.HasValue
                    && p.HOLD_FLAG > 0
                    );
                pv.HOLD_FLAG = p.HOLD_FLAG;
                pv.COUNTER_FLAG = p.COUNTER_FLAG;
                pv.PostingDate = p.POSTING_DATE;
                pv.PlanningPaymentDate = p.PLANNING_PAYMENT_DATE;
                pv.BankType = p.BANK_TYPE;
                pv.TaxCalculated = pv.TaxCode != null;
                pv.SubmitHC = p.SUBMIT_HC_DOC_DATE;
                pv.BookingNo = p.STATUS_COMPARE; // numpang properti euy sori. biar cepet
                pv.SubmitDate = p.SUBMIT_DATE;
                pv.ReferenceNo = p.REFFERENCE_NO;
                pv.Canceled = (p.CANCEL_FLAG ?? 0) == 1;
                pv.FormTable.DataList = GetPVDetail(pv);
                pv.UserName = userName;
                FillPVAttachment(pv);
            }

            return pv;
        }

        public List<PVFormDetail> GetPVDetail(PVFormData f)
        {
            int _no, _yy;
            _no = f.PVNumber ?? 0;
            _yy = f.PVYear ?? DateTime.Now.Year;
            List<PVFormDetail> l = new List<PVFormDetail>();

            var detailQuery = from t in _db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
            var detailResult = detailQuery.ToList();
            if (!detailResult.Any())
            {
                _yy--;
                detailQuery = from t in _db.TB_R_PV_D
                              where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                              select t;
                detailResult = detailQuery.ToList();
                if (!detailResult.Any())
                {
                    _yy += 2;
                    detailQuery = from t in _db.TB_R_PV_D
                                  where (t.PV_YEAR == _yy) && (t.PV_NO == _no)
                                  select t;
                    detailResult = detailQuery.ToList();
                }
            }
            if (!detailResult.Any()) return l;

            PVFormTable formTable = f.FormTable;
            string standardDescriptionWording = getDescriptionDefaultWording(f.TransactionCode);
            PVFormDetail formDetail;
            int seqNumber = 0;

            foreach (var detail in detailResult)
            {
                seqNumber++;
                formDetail = new PVFormDetail()
                {
                    Persisted = true,
                    SequenceNumber = detail.SEQ_NO,
                    DisplaySequenceNumber = seqNumber,
                    Amount = detail.AMOUNT,
                    CostCenterCode = detail.COST_CENTER_CD,
                    CostCenterName = getCostCenterDescription(detail.COST_CENTER_CD),
                    CurrencyCode = detail.CURRENCY_CD,
                    Description = detail.DESCRIPTION,
                    StandardDescriptionWording = standardDescriptionWording,
                    InvoiceNumber = detail.INVOICE_NO,
                    GlAccount = detail.GL_ACCOUNT.HasValue ? null : detail.GL_ACCOUNT.ToString(),
                    FromCurr = detail.FROM_CURR,
                    FromSeq = detail.FROM_SEQ,
                    ItemNo = detail.ITEM_NO,
                    TaxCode = detail.TAX_CD,
                    TaxNumber = detail.TAX_NO,
                    InvoiceDate = detail.INVOICE_DATE,
                    ItemTransaction = detail.ITEM_TRANSACTION_CD,
                    #region EFB
                    //WHT_TAX_CODE = detail.WHT_TAX_CODE,
                    //WHT_TAX_TARIFF = detail.WHT_TAX_TARIFF,
                    //WHT_TAX_ADDITIONAL_INFO = detail.WHT_TAX_ADDITIONAL_INFO,
                    //WHT_DPP_PPh_AMOUNT = detail.WHT_DPP_PPh_AMOUNT
                    #endregion
                };

                if (formDetail.ItemTransaction == null)
                {
                    formDetail.ItemTransaction = 1;
                }

                l.Add(formDetail);
            }

            return l;
        }

        public void FillPVAttachment(PVFormData f)
        {
            string reffNo = string.Format("{0}{1}", f.PVNumber, f.PVYear);

            var data = (from a in _db.TB_R_ATTACHMENT
                        where a.REFERENCE_NO == reffNo
                        select a).ToList();

            FormAttachment att;
            if (data.Any())
            {
                List<FormAttachment> listAtt = f.AttachmentTable.Attachments;
                foreach (var d in data)
                {
                    att = new FormAttachment()
                    {
                        ReferenceNumber = d.REFERENCE_NO,
                        SequenceNumber = d.REF_SEQ_NO,
                        CategoryCode = d.ATTACH_CD,
                        Description = d.DESCRIPTION,
                        FileName = d.FILE_NAME,
                        PATH = d.DIRECTORY,
                        Blank = false
                    };

                    listAtt.Add(att);
                }
            }
        }

        private bool ValidateDateRange(DateTime? d)
        {
            return (d == null || ((DateTime)d).Year > 1969 && ((DateTime)d).Year <= 9999);
        }

        private bool ValidateActivityDate(DateTime activityDate, DateTime activityDateTo, int docType, List<string> errMsgs)
        {
            if(ValidateDateRange(activityDate))
            {
                errMsgs.Add(meLo.Message("MSTD00107ERR", "Activity Date From", "1970", "9999"));
                return false;
            }
            if (ValidateDateRange(activityDateTo))
            {
                errMsgs.Add(meLo.Message("MSTD00107ERR", "Activity Date To", "1970", "9999"));
                return false;
            }
            if (DateTime.Compare(activityDate, activityDateTo) > 0)
            { 
                errMsgs.Add(meLo.Message("MSTD00083ERR", "Activity Date From", "Activity Date To"));
                return false;
            }
            else if (docType == 2)
            {
                if (DateTime.Compare(DateTime.Now.Date, activityDate) > 0)
                {
                    errMsgs.Add(meLo.Message("MSTD00094ERR", "Activity Date From", "Current date"));
                    return false;
                }
            }

            return true;
        }

        public bool performNonSubmissionValidation(PVFormData FormData, List<string> errMsgs)
        {
            List<PVFormDetail> details = FormData.Details.OrderByDescending(x => x.CostCenterCode).ToList();
            decimal amount;

            string[] roundCurrs = GetRoundCurrency();
            string roundCurrencies = string.Join(" and ", roundCurrs, 0, roundCurrs.Length);
            foreach (PVFormDetail d in details)
            {
                if (!string.IsNullOrEmpty(d.CurrencyCode) && roundCurrs.Contains(d.CurrencyCode))
                {
                    amount = d.Amount;

                    decimal frac = amount - decimal.Truncate(amount);

                    if (frac > 0 && FormData.PVTypeCode != 3)
                    {
                        errMsgs.Add(meLo.Message("MSTD00102ERR", roundCurrencies));

                        return false;
                    }
                }
            }

            return true;
        }

        public bool validateGeneralData(PVFormData FormData, List<string> errMsgs)
        {
            string msgMandatory = "MSTD00017WRN";

            int? docType = FormData.PVTypeCode;
            if (docType == null)
            {
                errMsgs.Add(meLo.Message(msgMandatory, "PV Type"));
            }
            if (string.IsNullOrEmpty(FormData.PaymentMethodCode))
            {
                errMsgs.Add(meLo.Message(msgMandatory, "Payment Method"));
            }
            if (string.IsNullOrEmpty(FormData.VendorCode) || string.IsNullOrWhiteSpace(FormData.VendorCode))
            {
                errMsgs.Add(meLo.Message(msgMandatory, "Vendor Code"));
            }
            if (FormData.TransactionCode == null)
            {
                errMsgs.Add(meLo.Message(msgMandatory, "Transaction Type"));
            }
            if (docType != 1)
            {
                if ((FormData.ActivityDate == null) || (FormData.ActivityDateTo == null))
                {
                    errMsgs.Add(meLo.Message(msgMandatory, "Activity Date"));
                }

                ValidateActivityDate(FormData.ActivityDate ?? DateTime.Now, FormData.ActivityDateTo ?? DateTime.Now, FormData.PVTypeCode ?? 1, errMsgs);
            }
            if (FormData.Budgeted)
            {
                if (string.IsNullOrEmpty(FormData.BudgetNumber))
                {
                    errMsgs.Add(meLo.Message(msgMandatory, "Budget Number"));
                }
            }

            List<PVFormDetail> lstDetail = FormData.FormTable.getBlankRowCleanedDetails().OrderByDescending(x => x.CostCenterCode).ToList();
            if (lstDetail.Count == 0)
            {
                errMsgs.Add(meLo.Message(msgMandatory, "Details"));
                FormData.Details = lstDetail;
            }
            else
            {
                bool ignoreMinusAmount = ((jo.GetSysText("PvForm", "AllowNegativeAmount") ?? "0") == "1");

                var Tabel = FormData.FormTable;

                if (Tabel.hasInvalidDetail(ignoreMinusAmount)) // ignore zeroes when mode is view
                {
                    errMsgs.Add(meLo.Message(msgMandatory, "Currency Code and Amount of details"));
                }
                if (Tabel.hasEmptyDescription())
                {
                    errMsgs.Add(meLo.Message(msgMandatory, "Description"));
                }

                List<string> ecc = new List<string>();
                if (FormData._TransactionType != null && !Tabel.validCostCenter(NonCostCenterGlAccounts, FormData._TransactionType.CostCenterFlag, ecc))
                {
                    for (int ei = ecc.Count - 1; ei >= 0; ei--)
                    {
                        errMsgs.Add(meLo.Message("MSTD00002ERR", ecc[ei]));
                    }
                    errMsgs.Add(meLo.Message("MSTD00002ERR", "Invalid Cost Center"));
                }
                if (FormData._TransactionType != null && FormData._TransactionType.CostCenterFlag == true &&
                    (lstDetail[0].CostCenterCode == null || lstDetail[0].CostCenterCode.Equals("")))
                {
                    errMsgs.Add(meLo.Message(msgMandatory, "Cost Center"));
                }
                if (FormData._TransactionType != null && !Tabel.validTxInGlAccount(FormData._TransactionType))
                {
                    errMsgs.Add(meLo.Message("MSTD00002ERR", "Please choose correct transaction type related to PRD/Non PRD"));
                }
            }

            if (FormData.AttachmentRequired)
            {
                int attachments = 0;
                foreach (var a in FormData.AttachmentTable.Attachments)
                {
                    if (!string.IsNullOrEmpty(a.DisplayFilename) || !string.IsNullOrEmpty(a.CategoryCode))
                        attachments++;
                }
                if (attachments < 1)
                    errMsgs.Add(meLo.Message("MSTD00103ERR"));
                else
                {
                    const string ENTERTAINMENT_MANDATORY_ATTACH = "3";
                    int[] EntertainmentTransactionTypes = jo.getEntertainmentTransactionCodes();

                    if (EntertainmentTransactionTypes.Contains(FormData.TransactionCode ?? 0))
                    {
                        var hasXls = FormData.AttachmentTable.Attachments
                            .Where(a => a.CategoryCode == ENTERTAINMENT_MANDATORY_ATTACH
                            && (a.FileName != null
                                && (a.FileName.EndsWith(".xls") || a.FileName.EndsWith(".xlsx"))
                            ));

                        if (!hasXls.Any())
                        {
                            errMsgs.Add(meLo.Message(
                                "MSTD00002ERR",
                                "Entertainment Transaction must have "
                                + jo.getAttachCatName(ENTERTAINMENT_MANDATORY_ATTACH)
                                + " in Excel format"));
                        }
                    }
                }
            }

            return errMsgs.Count == 0;
        }

        public bool postToK2(PVFormData formData, UserData userData, List<string> errMsgs)
        {
            LogicFactory logic = LogicFactory.Get();

            bool postResult = false;
            try
            {
                string moduleCode = "1";
                moduleCode = "1";

                Dictionary<string, string> submitData = new Dictionary<string, string>();
                submitData.Add("ApplicationID", Common.AppSetting.ApplicationID);
                submitData.Add("ApprovalLevel", "1");
                submitData.Add("ApproverClass", "1");
                submitData.Add("ApproverCount", "1");
                submitData.Add("CurrentDivCD", userData.DIV_CD.ToString());
                submitData.Add("CurrentPIC", userData.USERNAME);
                submitData.Add("EmailAddress", userData.EMAIL);
                string entertain = "0";
                int? transactionCode = formData.TransactionCode;
                List<int> lstEntertainmentTransactions = jo.getEntertainmentTransactionCodes().ToList();
                if (lstEntertainmentTransactions != null)
                {
                    foreach (int? c in lstEntertainmentTransactions)
                    {
                        if (transactionCode == c)
                        {
                            entertain = "1";
                            break;
                        }
                    }
                }
                submitData.Add("Entertain", entertain);

                submitData.Add("K2Host", "");
                submitData.Add("LimitClass", "0");
                submitData.Add("ModuleCD", moduleCode);
                submitData.Add("NextClass", "0");
                submitData.Add("PIC", userData.DIV_CD.ToString());
                submitData.Add("Reff_No", formData.PVNumber.ToString() + formData.PVYear.ToString());
                string registerDate = string.Format("{0:G}", DateTime.Now);
                submitData.Add("RegisterDt", registerDate);

                decimal? prevDocStatus = getPreviousDocumentStatus(string.Format("{0}{1}", formData.PVNumber, formData.PVYear));
                string val = "0";
                if ((prevDocStatus != null) && (prevDocStatus.Value == 0))
                {
                    val = "1";
                }
                submitData.Add("RejectedFinance", val);
                submitData.Add("StatusCD", formData.StatusCode.ToString());
                submitData.Add("StepCD", "");
                submitData.Add("VendorCD", formData.VendorCode);

                decimal totalAmount = 0;
                //PVFormTable formTable = formData.FormTable;
                //if (formTable != null)
                //{
                //    totalAmount = formData.GetTotalAmount();
                //    totalAmount = Math.Round(totalAmount);
                //}
                totalAmount = formData.Details.Sum(x => x.Amount); // assume the currency from TOWASS always IDR 
                totalAmount = Math.Round(totalAmount);
                string amt = totalAmount.ToString();
                // prevent overflow, K2 receive only int data type
                if (totalAmount > int.MaxValue) amt = int.MaxValue.ToString();
                if (totalAmount < int.MinValue) amt = int.MinValue.ToString();

                submitData.Add("TotalAmount", amt);

                //added by Akhmad Nuryanto, delete worklist if resubmitting form after rejected
                if ((formData.StatusCode ?? 0) != 0)
                {

                    logic.k2.deleteprocess(formData.PVNumber.ToString() + formData.PVYear.ToString());

                }
                //end of addition by Akhmad Nuryanto

                string command = resx.K2ProcID("Form_Registration_Submit_New");
                if (formData.PostK2ByWorklistChecking)
                {

                    postResult = logic.WorkFlow.K2SendCommandWithCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                    userData, command, submitData);

                }
                else
                {

                    postResult = logic.WorkFlow.K2SendCommandWithoutCheckWorklist(formData.PVNumber.ToString() + formData.PVYear.ToString(),
                                                                        userData, command, submitData);
                }
            }
            catch (Exception ex)
            {
                string exMsg = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                errMsgs.Add(exMsg);
                Handle(ex);

                return false;
            }
            formData.PostedK2 = postResult;
            return postResult;
        }

        public void saveSubmit(PVFormData formData, UserData userData, List<string> msgErr)
        { 
            DbTransaction TX = null;
            using (ContextWrap eco = new ContextWrap())
            {
                ELVIS_DBEntities db = eco.db;
                try
                {
                    TX = db.Connection.BeginTransaction();

                    int version = saveHistoryHeader(formData, userData, db);
                    saveHistoryDetail(formData, userData, version, db);
                    saveOri(formData, userData, db);

                    TX.Commit();
                }
                catch (Exception e)
                {
                    TX.Rollback();
                    Handle(e);
                    string imsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                    msgErr.Add(imsg);
                }
            }
        }
        public int saveHistoryHeader(PVFormData f, UserData u, ELVIS_DBEntities DB)
        {

            var Vers = DB.TB_H_PV_H
                         .Where(h =>
                                h.PV_NO == f.PVNumber
                             && h.PV_YEAR == f.PVYear)
                         .Select(h => h.VERSION)
                         .ToList();
            int lastVersion = 1;
            if (Vers != null && Vers.Count > 0)
            {
                lastVersion = Vers.Max() + 1;
            }

            TB_H_PV_H hHistory = new TB_H_PV_H()
            {
                POSTING_DATE = f.PostingDate,
                PLANNING_PAYMENT_DATE = f.PlanningPaymentDate,
                BANK_TYPE = f.BankType,
                STATUS_CD = f.StatusCode.Value,
                PV_NO = f.PVNumber.Value,
                PV_YEAR = f.PVYear.Value,
                PAY_METHOD_CD = f.PaymentMethodCode,
                VENDOR_GROUP_CD = f.VendorGroupCode,
                VENDOR_CD = f.VendorCode,
                PV_TYPE_CD = f.PVTypeCode,
                TRANSACTION_CD = f.TransactionCode,
                BUDGET_NO = f.BudgetNumber,
                ACTIVITY_DATE_FROM = f.ActivityDate,
                ACTIVITY_DATE_TO = f.ActivityDateTo,
                DIVISION_ID = f.DivisionID,
                PV_DATE = f.PVDate.Value,
                TOTAL_AMOUNT = f.Details.Sum(x => x.Amount),
                CREATED_DATE = today,
                CREATED_BY = u.USERNAME,
                VERSION = lastVersion
            };

            DB.TB_H_PV_H.AddObject(hHistory);
            DB.SaveChanges();
            return lastVersion;
        }
        public void saveHistoryDetail(PVFormData f, UserData u, int lastVersion, ELVIS_DBEntities DB)
        {
            /* Detail History */
            int lastSeqNo = 1;

            foreach (PVFormDetail detail in f.Details)
            {
                lastSeqNo++;
                int seq = lastSeqNo;
                if (detail.SequenceNumber != 0) seq = detail.SequenceNumber;

                DB.TB_H_PV_D.AddObject(new TB_H_PV_D()
                {
                    SEQ_NO = seq,
                    VERSION = lastVersion,
                    PV_NO = f.PVNumber.Value,
                    PV_YEAR = f.PVYear.Value,
                    AMOUNT = (detail.CurrencyCode == "IDR" ? Math.Round(detail.Amount) : detail.Amount),
                    COST_CENTER_CD = detail.CostCenterCode,
                    CURRENCY_CD = detail.CurrencyCode,
                    TAX_CD = detail.TaxCode,
                    TAX_NO = detail.TaxNumber,
                    ITEM_TRANSACTION_CD = detail.ItemTransaction,
                    DESCRIPTION = detail.Description,
                    INVOICE_NO = detail.InvoiceNumber,
                    CREATED_BY = u.USERNAME,
                    CREATED_DT = today
                });
                DB.SaveChanges();
            }
        }

        public void saveOri(PVFormData formData, UserData userData, ELVIS_DBEntities db)
        {
            int _no, _yy;
            _no = formData.PVNumber ?? 0;
            _yy = formData.PVYear ?? 0;
            var q = from oa in db.TB_R_ORIGINAL_AMOUNT
                    where oa.DOC_NO == _no && oa.DOC_YEAR == _yy
                    select oa;

            if (q.Any())
            {
                List<OriginalAmountData> oa = new List<OriginalAmountData>();
                List<OriginalAmountData> oadd = new List<OriginalAmountData>();

                foreach (var o in q.ToList())
                {
                    PVFormDetail fd = formData.Details
                        .Where(a => a.SequenceNumber == o.SEQ_NO)
                        .FirstOrDefault();

                    if (fd == null)
                    {
                        oa.Add(new OriginalAmountData()
                        {
                            DOC_NO = o.DOC_NO,
                            DOC_YEAR = o.DOC_YEAR,
                            SEQ_NO = o.SEQ_NO
                        });
                    }
                }

                // remove existing 
                foreach (OriginalAmountData o in oa)
                {
                    var d = (from ox in db.TB_R_ORIGINAL_AMOUNT
                             where ox.DOC_YEAR == _yy
                             && ox.DOC_NO == _no
                             && ox.SEQ_NO == o.SEQ_NO
                             select ox).FirstOrDefault();
                    if (d != null)
                        db.DeleteObject(d);
                }
            }

            List<ExchangeRate> le = jo.getExchangeRates();

            foreach (PVFormDetail d in formData.Details)
            {
                int _seq = d.SequenceNumber;

                var x = (from ox in db.TB_R_ORIGINAL_AMOUNT
                         where ox.DOC_YEAR == _yy
                         && ox.DOC_NO == _no
                         && ox.SEQ_NO == _seq
                         select ox).FirstOrDefault();

                decimal er = (decimal)(from e in le
                                       where e.CurrencyCode == d.CurrencyCode
                                       select e.Rate).FirstOrDefault();
                int? glacc = null;

                if (string.IsNullOrEmpty(d.GlAccount))
                {
                    glacc = jo.GLAccount(d.CostCenterCode, formData.TransactionCode ?? 0, d.ItemTransaction);
                }
                else
                {
                    int t;
                    if (Int32.TryParse(d.GlAccount.Trim(), out t))
                    {
                        glacc = t;
                    }
                }

                if (x != null)
                {
                    x.CURRENCY_CD = d.CurrencyCode;
                    x.EXCHANGE_RATE = er;
                    x.AMOUNT = Math.Round((decimal)d.Amount);
                    x.SPENT_AMOUNT = 0;
                    x.SUSPENSE_AMOUNT = 0;

                    x.COST_CENTER_CD = d.CostCenterCode;
                    x.DESCRIPTION = d.Description;
                    x.INVOICE_NO = d.InvoiceNumber;
                    x.ITEM_TRANSACTION_CD = d.ItemTransaction;
                    x.GL_ACCOUNT = glacc;
                    x.TAX_NO = d.TaxNumber;
                    x.TAX_CD = d.TaxCode;

                    x.CHANGED_BY = userData.USERNAME;
                    x.CHANGED_DT = today;
                    #region EFB
                    x.WHT_TAX_CODE = d.WHT_TAX_CODE;
                    x.WHT_TAX_TARIFF = d.WHT_TAX_TARIFF;
                    x.WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO;
                    x.WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT;
                    #endregion
                    db.SaveChanges();
                }
                else
                {
                    db.AddToTB_R_ORIGINAL_AMOUNT(
                        new TB_R_ORIGINAL_AMOUNT()
                        {
                            DOC_NO = _no,
                            DOC_YEAR = _yy,
                            SEQ_NO = _seq,
                            CURRENCY_CD = d.CurrencyCode,
                            EXCHANGE_RATE = er,
                            AMOUNT = Math.Round((decimal)d.Amount),
                            SPENT_AMOUNT = 0,
                            SUSPENSE_AMOUNT = 0,
                            COST_CENTER_CD = d.CostCenterCode,
                            DESCRIPTION = d.Description,
                            INVOICE_NO = d.InvoiceNumber,
                            ITEM_TRANSACTION_CD = d.ItemTransaction,
                            GL_ACCOUNT = glacc,
                            TAX_NO = d.TaxNumber,
                            TAX_CD = d.TaxCode,
                            CREATED_BY = userData.USERNAME,
                            CREATED_DT = today,

                            #region EFB
                            WHT_TAX_CODE = d.WHT_TAX_CODE,
                            WHT_TAX_TARIFF = d.WHT_TAX_TARIFF,
                            WHT_TAX_ADDITIONAL_INFO = d.WHT_TAX_ADDITIONAL_INFO,
                            WHT_DPP_PPh_AMOUNT = d.WHT_DPP_PPh_AMOUNT
                            #endregion
                        });
                }
                db.SaveChanges();
            }
        }

        public void returnPVSubmit(List<PVSyncH> allUnsyncPVH)
        {
            if (allUnsyncPVH == null)
                return;

            var ret = allUnsyncPVH
                        .Where(x => x.ResultSts != null)
                        .ToList();

            if (ret.Count == 0)
                return;

            try
            {
                foreach (var up in ret)
                {
                    //switch to SSIS method
                    //DynamicParameters d = new DynamicParameters();
                    //d.Add("@pv_no_elvis", up.PVNo);
                    //d.Add("@submit_date", up.SubmitDate);
                    //d.Add("@return_sts", up.ResultSts);
                    //d.Add("@message", up.Message);

                    //Exec("sp_wossync_receive_submit", d);

                    DynamicParameters d = new DynamicParameters();
                    d.Add("@sync_id", up.SyncId);
                    d.Add("@sync_action", up.SyncAction);
                    d.Add("@pv_no_towass", null);
                    d.Add("@pv_no_elvis", up.PVNo);
                    d.Add("@pv_year", up.PVYear);
                    d.Add("@pv_amount", null);
                    d.Add("@vendor_cd", null);
                    d.Add("@vendor_nm", null);
                    d.Add("@pv_cover_file", null);
                    d.Add("@submit_date", up.SubmitDate);
                    d.Add("@cancel_date", null);
                    d.Add("@result_sts", up.ResultSts);
                    d.Add("@message", up.Message);

                    Exec("sp_wos_send_result", d);
                }
            }
            catch (Exception e)
            {
                Handle(e);
            }
        }
    }
}
