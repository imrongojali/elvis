﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic.VoucherForm;
using BusinessLogic.SyncLogic;
using BusinessLogic.CommonLogic;
using ELVISJob.ELVIS.JOBCommon;
using Common.Data;
using System.IO;

namespace ELVISJob.ELVIS.JOB0001
{
    class JOB0001 : BaseJob
    {
        public override void Run()
        {
            JobFunctions jo = new JobFunctions();
            JOB0001BO bo = new JOB0001BO();
            bo.SetFuncId("JOB0001");

            List<PVSyncH> allUnsyncPV = null;

            try
            {
                allUnsyncPV = bo.GetUnsyncPVH(SYNC_PV_DRAFT);
                allUnsyncPV = bo.ClearUnsyncPVDraft(allUnsyncPV);

                bool dataExists = allUnsyncPV
                                    .Exists(x => x.SyncAction == SYNC_PV_DRAFT 
                                                && x.SyncStatus == 0);

                if (dataExists)
                {
                    ProcessId = bo.PutLog(MSG_INF, "Start Process", "Start");
                    bo.SetProcessId(ProcessId);

                    bool success = false;
                    List<string> errMsg;

                    foreach (var up in allUnsyncPV
                                        .Where(x => x.SyncAction == SYNC_PV_DRAFT)
                                        .ToList())
                    {
                        if (up.SyncStatus == 1)
                            continue;

                        errMsg = new List<string>();
                        UserData u = new UserData();
                        bo.PutLog(MSG_INF, "Sync PV Draft TOWASS : " + up.PVTowass, "Sync");

                        // do validation here
                        var pv = bo.ParsingUnsycPVFormData(up, ref errMsg);

                        if (pv != null)
                        {
                            u = jo.GetUserData(pv.UserName);
                            if (u == null)
                                errMsg.Add(string.Format("ELVIS User {0} does not exist", pv.UserName));

                            if (pv.VendorCode == null)
                                errMsg.Add(string.Format("Vendor should not be empty"));

                            if (pv._TransactionType == null)
                                errMsg.Add(string.Format("Transaction Code should not be empty or unrecognized"));

                            if (pv.DivisionID == null)
                                errMsg.Add(string.Format("Division ID should not be empty"));
                        }

                        success = (errMsg.Count == 0 && pv != null);
                        
                        if (success)
                        {
                            success = bo.SaveData(pv, u, errMsg, up.NeedCompare);
                        }

                        // fill property to be returned
                        if (success)
                        {
                            bo.PutLog(MSG_INF, "Sync PV Draft Successfully", "Sync");

                            string _no = (pv.PVNumber ?? 0).ToString();
                            string _yy = (pv.PVYear ?? 0).ToString();

                            // create PV Cover
                            up.PVCover = bo.printCover(_no, _yy, u);

                            up.ResultSts = 1;
                            up.PVNo = pv.PVNumber;
                            up.TotalAmount = pv.Details.Sum(x => x.Amount); // assume amount from TOWASS always in IDR
                            up.VendorNm = pv.BookingNo; //numpang property orang. XP

                        }
                        else
                        {
                            string msg = string.Join("<br/>", errMsg);
                            bo.PutLog(MSG_ERR, "Sync PV Draft Failed: " + msg, "Sync");

                            up.ResultSts = 0;
                            up.Message = msg;
                        }

                        allUnsyncPV.ForEach(x =>
                        {
                            if (x.SyncAction == SYNC_PV_DRAFT && x.PVTowass.Equals(up.PVTowass))
                            {
                                x.SyncStatus = 1;
                            }
                        });
                    }

                    bo.returnPVDraft(allUnsyncPV);
                    //bo.returnPVCover(allUnsyncPV);

                    bo.PutLog(MSG_INF, "Finish Process", "Finish");
                }

                bo.CloseSyncFlag(allUnsyncPV);
            }
            catch (Exception e)
            {
                string exMsg = e.InnerException != null ? e.InnerException.Message : e.Message;
                bo.PutLog(MSG_ERR, "Sync PV Draft Exception: " + exMsg, "Sync");
                Handle(e);
            }
        }
    }
}
